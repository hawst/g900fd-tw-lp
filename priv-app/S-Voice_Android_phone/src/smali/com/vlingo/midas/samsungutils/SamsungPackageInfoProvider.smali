.class public Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;
.super Lcom/vlingo/core/internal/util/CorePackageInfoProvider;
.source "SamsungPackageInfoProvider.java"


# static fields
.field public static final ALARM:[Ljava/lang/String;

.field public static final CALL:[Ljava/lang/String;

.field public static final CAR_MODE:[Ljava/lang/String;

.field public static final CN_BAIDU_MAPS:[Ljava/lang/String;

.field public static final CN_NAV_MAPS:[Ljava/lang/String;

.field public static final DEFAULT_CAR_MODE_PACKAGE:Ljava/lang/String;

.field public static final ENAVI:[Ljava/lang/String;

.field public static final EVENT:[Ljava/lang/String;

.field public static final KOREAN_NAV:[Ljava/lang/String;

.field public static final MAP:[Ljava/lang/String;

.field public static final MEMO:[Ljava/lang/String;

.field public static final MESSAGING:[Ljava/lang/String;

.field public static final MUSIC:[Ljava/lang/String;

.field public static final NAV:[Ljava/lang/String;

.field public static final RADIO:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field public static final TASK:[Ljava/lang/String;

.field public static final TIMER:[Ljava/lang/String;

.field public static final UWA:[Ljava/lang/String;

.field public static final VOICERECORDER:[Ljava/lang/String;

.field public static final WORLDCLOCK_TAB:[Ljava/lang/String;

.field public static mDeviceType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    const-class v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->TAG:Ljava/lang/String;

    .line 31
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "com.android.phone"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->CALL:[Ljava/lang/String;

    .line 32
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "com.samsung.android.app.memo"

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.sec.android.app.memo"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.sec.android.widgetapp.diotek.smemo"

    aput-object v1, v0, v4

    const-string/jumbo v1, "com.sec.android.app.snotebook"

    aput-object v1, v0, v5

    const-string/jumbo v1, "com.samsung.android.snote"

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->MEMO:[Ljava/lang/String;

    .line 33
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "com.google.android.apps.maps"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->MAP:[Ljava/lang/String;

    .line 34
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.clockpackage"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->ALARM:[Ljava/lang/String;

    .line 35
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.android.calendar"

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.nttdocomo.android.schedulememo"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->EVENT:[Ljava/lang/String;

    .line 36
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "com.android.calendar"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->TASK:[Ljava/lang/String;

    .line 37
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.music"

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.samsung.music"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->MUSIC:[Ljava/lang/String;

    .line 38
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.fm"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->RADIO:[Ljava/lang/String;

    .line 39
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.voicerecorder"

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.sec.android.app.voicenote"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->VOICERECORDER:[Ljava/lang/String;

    .line 40
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.clockpackage.timer"

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.sec.android.app.clockpackage"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->TIMER:[Ljava/lang/String;

    .line 41
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "com.android.mms"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->MESSAGING:[Ljava/lang/String;

    .line 42
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.worldclock"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->WORLDCLOCK_TAB:[Ljava/lang/String;

    .line 43
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "com.google.android.apps.maps"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->NAV:[Ljava/lang/String;

    .line 44
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "com.baidu.BaiduMap"

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.baidu.BaiduMap.pad"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.baidu.BaiduMap.samsung"

    aput-object v1, v0, v4

    const-string/jumbo v1, "com.autonavi.xmgd.navigator.keyboard"

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->CN_NAV_MAPS:[Ljava/lang/String;

    .line 45
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "com.baidu.BaiduMap"

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.baidu.BaiduMap.pad"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.baidu.BaiduMap.samsung"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->CN_BAIDU_MAPS:[Ljava/lang/String;

    .line 46
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "com.pdager"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->ENAVI:[Ljava/lang/String;

    .line 47
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "com.skt.skaf.l001mtm091"

    aput-object v1, v0, v2

    const-string/jumbo v1, "kt.navi"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.mnsoft.lgunavi"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->KOREAN_NAV:[Ljava/lang/String;

    .line 48
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.automotive.carmode"

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.sec.android.automotive.drivelink"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->CAR_MODE:[Ljava/lang/String;

    .line 49
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "com.uplus.uwa.handsfree"

    aput-object v1, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->UWA:[Ljava/lang/String;

    .line 50
    sget-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->CAR_MODE:[Ljava/lang/String;

    aget-object v0, v0, v2

    sput-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->DEFAULT_CAR_MODE_PACKAGE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CorePackageInfoProvider;-><init>()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CorePackageInfoProvider;-><init>()V

    .line 58
    return-void
.end method

.method public static getCarModePackageName()Ljava/lang/String;
    .locals 9

    .prologue
    .line 380
    const/4 v6, 0x0

    .line 381
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->CAR_MODE:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v0, v1, v4

    .line 383
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 384
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const/16 v8, 0x80

    invoke-virtual {v7, v0, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 388
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 392
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_2

    iget-object v7, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    :goto_2
    return-object v7

    .line 385
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 386
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 381
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 392
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    sget-object v7, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->DEFAULT_CAR_MODE_PACKAGE:Ljava/lang/String;

    goto :goto_2
.end method

.method public static hasAlarm()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 109
    const/4 v6, 0x0

    .line 110
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->ALARM:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 112
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 113
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 117
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 121
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 114
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 115
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 110
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 121
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasApp([Ljava/lang/String;)Z
    .locals 10
    .param p0, "packages"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 343
    const/4 v6, 0x0

    .line 344
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    move-object v1, p0

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 346
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 347
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 351
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 355
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 348
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 349
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 344
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 355
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasAutonavi()Z
    .locals 2

    .prologue
    .line 359
    const-string/jumbo v0, "com.autonavi.xmgd.navigator.keyboard"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static hasBaiduMaps()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 363
    const/4 v6, 0x0

    .line 364
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->CN_BAIDU_MAPS:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 366
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 367
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 371
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 375
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 368
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 369
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 364
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 375
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasCallEnabled()Z
    .locals 11

    .prologue
    const/4 v8, 0x1

    .line 324
    const/4 v6, 0x0

    .line 325
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->CALL:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v1, v4

    .line 327
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 328
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const/16 v9, 0x80

    invoke-virtual {v7, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 333
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_0

    .line 334
    sget-object v7, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "hasCallEnabled() - true"

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    .end local v0    # "app":Ljava/lang/String;
    :goto_2
    return v8

    .line 329
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 330
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v7, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "hasCallEnabled() - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    const/4 v6, 0x0

    goto :goto_1

    .line 325
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 338
    .end local v0    # "app":Ljava/lang/String;
    :cond_1
    sget-object v9, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "hasCallEnabled() - "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    const-string/jumbo v7, "true"

    :goto_3
    invoke-static {v9, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    if-eqz v6, :cond_3

    move v7, v8

    :goto_4
    move v8, v7

    goto :goto_2

    .line 338
    :cond_2
    const-string/jumbo v7, "false"

    goto :goto_3

    .line 339
    :cond_3
    const/4 v7, 0x0

    goto :goto_4
.end method

.method public static hasChineseNav()Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 302
    const-string/jumbo v9, "FAKE_CHINESE_NAVIGATION_APP"

    invoke-static {v9, v8}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 303
    const-string/jumbo v9, "CHINESE_NAVIGATION_BAIDU"

    invoke-static {v9, v8}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_1

    const-string/jumbo v9, "CHINESE_NAVIGATION_AUTONAVI"

    invoke-static {v9, v8}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_1

    move v7, v8

    .line 320
    .local v1, "arr$":[Ljava/lang/String;
    .local v4, "i$":I
    .local v5, "len$":I
    .local v6, "navApp":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return v7

    .line 308
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "navApp":Landroid/content/pm/ApplicationInfo;
    :cond_1
    const/4 v6, 0x0

    .line 309
    .restart local v6    # "navApp":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->CN_NAV_MAPS:[Ljava/lang/String;

    .restart local v1    # "arr$":[Ljava/lang/String;
    array-length v5, v1

    .restart local v5    # "len$":I
    const/4 v4, 0x0

    .restart local v4    # "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 311
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 312
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const/16 v10, 0x80

    invoke-virtual {v9, v0, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 316
    .end local v2    # "context":Landroid/content/Context;
    :goto_2
    if-nez v6, :cond_0

    .line 309
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 313
    :catch_0
    move-exception v3

    .line 314
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_2

    .line 320
    .end local v0    # "app":Ljava/lang/String;
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    if-nez v6, :cond_0

    move v7, v8

    goto :goto_0
.end method

.method public static hasEvent()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 125
    const/4 v6, 0x0

    .line 126
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->EVENT:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 128
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 129
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 133
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 137
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 130
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 131
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 126
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 137
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasKoreanSpecialNavigation()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 61
    const/4 v6, 0x0

    .line 62
    .local v6, "mNav":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->KOREAN_NAV:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 64
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 65
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 69
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 73
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 66
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 67
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 62
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 73
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasMaps()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 93
    const/4 v6, 0x0

    .line 94
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->MAP:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 96
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 97
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 101
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 105
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 98
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 99
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 94
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 105
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasMemo()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 77
    const/4 v6, 0x0

    .line 78
    .local v6, "mMemo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->MEMO:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 80
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 81
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 85
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 89
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 82
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 83
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 78
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 89
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasMessaging()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 241
    const/4 v6, 0x0

    .line 243
    .local v6, "messaging":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->MESSAGING:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 245
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 246
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 250
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 254
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 247
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 248
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 243
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 254
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasMusic()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 157
    const/4 v6, 0x0

    .line 158
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->MUSIC:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 160
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 161
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 165
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 169
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 162
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 163
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 158
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 169
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasNav()Z
    .locals 9

    .prologue
    .line 285
    const/4 v6, 0x0

    .line 286
    .local v6, "mActInfo":Landroid/content/pm/ActivityInfo;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 287
    .local v3, "context":Landroid/content/Context;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->NAV:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v1, v4

    .line 290
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    new-instance v2, Landroid/content/ComponentName;

    const-string/jumbo v7, "com.google.android.maps.driveabout.app.DestinationActivity"

    invoke-direct {v2, v0, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    .local v2, "component":Landroid/content/ComponentName;
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const/16 v8, 0x80

    invoke-virtual {v7, v2, v8}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 294
    .end local v2    # "component":Landroid/content/ComponentName;
    :goto_1
    if-eqz v6, :cond_0

    .line 295
    const/4 v7, 0x1

    .line 298
    .end local v0    # "app":Ljava/lang/String;
    :goto_2
    return v7

    .line 287
    .restart local v0    # "app":Ljava/lang/String;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 298
    .end local v0    # "app":Ljava/lang/String;
    :cond_1
    const/4 v7, 0x0

    goto :goto_2

    .line 292
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v7

    goto :goto_1
.end method

.method public static hasRadio()Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 173
    const/4 v7, 0x0

    .line 174
    .local v7, "radio":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->RADIO:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_3

    aget-object v0, v1, v5

    .line 176
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 177
    .local v3, "context":Landroid/content/Context;
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/16 v11, 0x80

    invoke-virtual {v10, v0, v11}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 182
    .end local v3    # "context":Landroid/content/Context;
    :goto_1
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 183
    .local v2, "build_model":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 194
    .end local v0    # "app":Ljava/lang/String;
    .end local v2    # "build_model":Ljava/lang/String;
    :cond_0
    :goto_2
    return v9

    .line 178
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 179
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v7, 0x0

    goto :goto_1

    .line 185
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "build_model":Ljava/lang/String;
    :cond_1
    const-string/jumbo v10, "GT-I9195"

    invoke-virtual {v2, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string/jumbo v10, "DGT-I9195"

    invoke-virtual {v2, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string/jumbo v10, "GT-I9192"

    invoke-virtual {v2, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 190
    if-eqz v7, :cond_2

    move v9, v8

    .line 191
    goto :goto_2

    .line 174
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 194
    .end local v0    # "app":Ljava/lang/String;
    .end local v2    # "build_model":Ljava/lang/String;
    :cond_3
    if-eqz v7, :cond_4

    :goto_3
    move v9, v8

    goto :goto_2

    :cond_4
    move v8, v9

    goto :goto_3
.end method

.method public static hasTask()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 141
    const/4 v6, 0x0

    .line 142
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->TASK:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 144
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 145
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 149
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 153
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 146
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 147
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 142
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 153
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasTimer()Z
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 214
    const/4 v6, 0x0

    .line 216
    .local v6, "mTimer":Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isBMode()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 237
    :cond_0
    :goto_0
    return v7

    .line 220
    :cond_1
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->TIMER:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_5

    aget-object v0, v1, v4

    .line 222
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 223
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const/16 v10, 0x80

    invoke-virtual {v9, v0, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    .line 227
    if-eqz v6, :cond_2

    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasWorldClock()Z

    move-result v9

    if-nez v9, :cond_3

    :cond_2
    if-eqz v6, :cond_4

    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->isTabletClockPackageAvailable()Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    if-eqz v9, :cond_4

    :cond_3
    move v7, v8

    .line 228
    goto :goto_0

    .line 230
    .end local v2    # "context":Landroid/content/Context;
    :catch_0
    move-exception v3

    .line 231
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    .line 233
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_4
    if-nez v6, :cond_0

    .line 220
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 237
    .end local v0    # "app":Ljava/lang/String;
    :cond_5
    if-nez v6, :cond_0

    move v7, v8

    goto :goto_0
.end method

.method public static hasUWA()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 396
    const/4 v1, 0x0

    .line 397
    .local v1, "appInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v2, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->UWA:[Ljava/lang/String;

    .local v2, "arr$":[Ljava/lang/String;
    array-length v6, v2

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_2

    aget-object v0, v2, v5

    .line 399
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 400
    .local v3, "context":Landroid/content/Context;
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 404
    .end local v3    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v1, :cond_1

    .line 408
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 401
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 402
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_1

    .line 397
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 408
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v1, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasVoiceRecorder()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 198
    const/4 v6, 0x0

    .line 199
    .local v6, "voiceRecorder":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->VOICERECORDER:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 201
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 202
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 206
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 210
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 203
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 204
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 199
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 210
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasWorldClock()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 258
    const/4 v6, 0x0

    .line 259
    .local v6, "mWorldClock":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->WORLDCLOCK_TAB:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 261
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 262
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 266
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 270
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 263
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 264
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 259
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 270
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static isTabletClockPackageAvailable()Z
    .locals 7

    .prologue
    .line 274
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 275
    .local v1, "intent":Landroid/content/Intent;
    new-instance v4, Landroid/content/ComponentName;

    const-string/jumbo v5, "com.sec.android.app.clockpackage"

    const-string/jumbo v6, "com.sec.android.app.clockpackage.TabletClockPackage"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 277
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 278
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 279
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const/high16 v4, 0x10000

    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 281
    .local v2, "list":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
