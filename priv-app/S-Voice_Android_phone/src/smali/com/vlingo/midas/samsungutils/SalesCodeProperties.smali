.class public Lcom/vlingo/midas/samsungutils/SalesCodeProperties;
.super Ljava/lang/Object;
.source "SalesCodeProperties.java"


# static fields
.field private static final CHINESE_APP_CHANNEL:Ljava/lang/String; = "Preinstall Free China"

.field private static instance:Lcom/vlingo/midas/samsungutils/SalesCodeProperties;


# instance fields
.field protected fakeAppChannel:Ljava/lang/String;

.field protected mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->instance:Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->instance:Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    .line 28
    return-void
.end method

.method public static getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->instance:Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    invoke-direct {v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->instance:Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    .line 23
    :cond_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->instance:Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    return-object v0
.end method

.method public static getPhoneSalesCode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "value":Ljava/lang/String;
    const-string/jumbo v1, "fake_sales_code"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    const-string/jumbo v1, "faked_sales_code_value"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    :cond_0
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "ro.csc.sales_code"

    invoke-virtual {v1, v2}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 129
    :cond_1
    return-object v0
.end method

.method private getRealCountryCodeProperty()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v0

    const-string/jumbo v1, "ro.csc.countryiso_code"

    invoke-virtual {v0, v1}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRealSalesCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v0

    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-virtual {v0, v1}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSalesCodeProperty()Ljava/lang/String;
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "fake_sales_code"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "faked_sales_code_value"

    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getRealSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->fakeAppChannel:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->fakeAppChannel:Ljava/lang/String;

    const-string/jumbo v1, "Preinstall Free China"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    const-string/jumbo v0, "CHN"

    goto :goto_0

    .line 57
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getRealSalesCode()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public init(Ljava/lang/String;)V
    .locals 0
    .param p1, "appChannel"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->fakeAppChannel:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public initContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method public isAmericanPhone()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 83
    const-string/jumbo v2, "ATT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "BST"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "SPR"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "VMU"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "XAS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "TMB"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "USC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "VZW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 86
    :cond_1
    return v1
.end method

.method public isChatONVPhone()Z
    .locals 3

    .prologue
    .line 61
    const/4 v0, 0x1

    .line 62
    .local v0, "isChatONV":Z
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 64
    const-string/jumbo v2, "VZW"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "SPR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CHN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CHM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CHU"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "KTT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "LGT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "SKT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "DCM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 69
    :cond_0
    const/4 v0, 0x0

    .line 72
    :cond_1
    return v0
.end method

.method public isChinesePhone()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 97
    const-string/jumbo v2, "CHN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CHM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CHU"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CTC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "CHC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "LHS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "PAP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getRealCountryCodeProperty()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "CN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 102
    :cond_1
    return v1
.end method

.method public isJapanesePhone()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v0

    .line 107
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 108
    const-string/jumbo v2, "DCM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "KDI"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 110
    :cond_1
    return v1
.end method

.method public isKDDIPhone()Z
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v0

    .line 115
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 116
    const-string/jumbo v1, "KDI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 118
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
