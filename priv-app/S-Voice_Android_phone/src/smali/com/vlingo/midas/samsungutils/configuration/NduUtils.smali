.class public Lcom/vlingo/midas/samsungutils/configuration/NduUtils;
.super Ljava/lang/Object;
.source "NduUtils.java"


# static fields
.field private static final HOST_SETTINGS_UTILS:[Ljava/lang/String;

.field private static final SHARED_PREFS_NAME:Ljava/lang/String; = "lmttutility"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "com.nuance.nuancedebugutil"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "com.vlingo.midas.lmttutility"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->HOST_SETTINGS_UTILS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getNduContext()Landroid/content/Context;
    .locals 6

    .prologue
    .line 37
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 38
    .local v1, "context":Landroid/content/Context;
    sget-object v0, Lcom/vlingo/midas/samsungutils/configuration/NduUtils;->HOST_SETTINGS_UTILS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 40
    .local v4, "packageName":Ljava/lang/String;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 46
    .end local v4    # "packageName":Ljava/lang/String;
    :goto_1
    return-object v5

    .line 41
    .restart local v4    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 38
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 46
    .end local v4    # "packageName":Ljava/lang/String;
    :cond_0
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static getNduHost(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "hostKey"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 19
    move-object v0, p1

    .line 33
    .local v0, "host":Ljava/lang/String;
    return-object v0
.end method
