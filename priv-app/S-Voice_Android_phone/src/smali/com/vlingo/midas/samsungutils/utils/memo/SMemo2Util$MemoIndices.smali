.class public Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;
.super Ljava/lang/Object;
.source "SMemo2Util.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "MemoIndices"
.end annotation


# instance fields
.field final KEY_CONTENT_COL:I

.field final KEY_DATE_COL:I

.field final KEY_ID_COL:I

.field final KEY_NAME_COL:I

.field final KEY_TAG_CONTENT_COL:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 323
    const-string/jumbo v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;->KEY_ID_COL:I

    .line 324
    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;->KEY_NAME_COL:I

    .line 325
    const-string/jumbo v0, "ModifiedTime"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;->KEY_DATE_COL:I

    .line 326
    const-string/jumbo v0, "content"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;->KEY_CONTENT_COL:I

    .line 327
    const-string/jumbo v0, "Tag_Content"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;->KEY_TAG_CONTENT_COL:I

    .line 328
    return-void
.end method
