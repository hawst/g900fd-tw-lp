.class public final Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;
.super Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;
.source "SMemo2Util.java"

# interfaces
.implements Lcom/vlingo/core/internal/memo/IMemoUtil;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;
    }
.end annotation


# static fields
.field private static final KEY_CONTENT:Ljava/lang/String; = "content"

.field private static final KEY_DATE:Ljava/lang/String; = "ModifiedTime"

.field private static final KEY_HAS_VOICERECORDER:Ljava/lang/String; = "HasVoiceRecord"

.field private static final KEY_ID:Ljava/lang/String; = "_id"

.field private static final KEY_NAME:Ljava/lang/String; = "name"

.field private static final KEY_TAG_CONTENT:Ljava/lang/String; = "Tag_Content"

.field private static final KEY_TEMPLATE_TIME:Ljava/lang/String; = "TemplateType"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SORT_ORDER:Ljava/lang/String; = "ModifiedTime DESC"

.field private static instance:Lcom/vlingo/core/internal/memo/IMemoUtil; = null

.field private static final strAudio:Ljava/lang/String; = "voice"

.field private static final strContent:Ljava/lang/String; = "content"

.field private static final strId:Ljava/lang/String; = "id"

.field private static final strTitle:Ljava/lang/String; = "title"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "ModifiedTime"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "content"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "Tag_Content"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->PROJECTION:[Ljava/lang/String;

    .line 62
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;-><init>()V

    .line 65
    return-void
.end method

.method private getFilePathFromId(Landroid/content/Context;J)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J

    .prologue
    .line 199
    const/4 v7, 0x0

    .line 200
    .local v7, "path":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 201
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-direct {p0, p2, p3}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v1

    .line 202
    .local v1, "itemUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 204
    .local v6, "cur":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 205
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 206
    const-string/jumbo v2, "path"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 209
    :cond_0
    if-eqz v6, :cond_1

    .line 210
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 213
    :cond_1
    return-object v7

    .line 209
    :catchall_0
    move-exception v2

    if-eqz v6, :cond_2

    .line 210
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v2
.end method

.method public static getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;
    .locals 1

    .prologue
    .line 154
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    if-nez v0, :cond_0

    .line 155
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;

    invoke-direct {v0}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;-><init>()V

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    .line 158
    :cond_0
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->instance:Lcom/vlingo/core/internal/memo/IMemoUtil;

    return-object v0
.end method

.method private getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "indices"    # Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;

    .prologue
    .line 75
    new-instance v0, Lcom/vlingo/core/internal/memo/Memo;

    invoke-direct {v0}, Lcom/vlingo/core/internal/memo/Memo;-><init>()V

    .line 76
    .local v0, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;->KEY_NAME_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setTitle(Ljava/lang/String;)V

    .line 77
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;->KEY_CONTENT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setContent(Ljava/lang/String;)V

    .line 78
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;->KEY_CONTENT_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setText(Ljava/lang/String;)V

    .line 81
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;->KEY_ID_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setId(I)V

    .line 82
    iget v1, p2, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;->KEY_DATE_COL:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/memo/Memo;->setDate(Ljava/lang/String;)V

    .line 84
    return-object v0
.end method

.method public static getPackageVersionCode(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 147
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 148
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v1, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    .end local v0    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v1

    .line 149
    :catch_0
    move-exception v1

    .line 151
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private getUriFromId(J)Landroid/net/Uri;
    .locals 2
    .param p1, "id"    # J

    .prologue
    .line 217
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getUpdateContentProviderUri()Landroid/net/Uri;

    move-result-object v0

    .line 221
    .local v0, "contentUri":Landroid/net/Uri;
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static isAppInstalled(Ljava/lang/String;I)Z
    .locals 10
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "minVersionCode"    # I

    .prologue
    const/4 v8, 0x0

    .line 114
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 115
    .local v2, "ctxt":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 116
    .local v6, "pm":Landroid/content/pm/PackageManager;
    new-instance v5, Landroid/content/Intent;

    const-string/jumbo v9, "android.intent.action.MAIN"

    invoke-direct {v5, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 117
    .local v5, "intent":Landroid/content/Intent;
    const-string/jumbo v9, "android.intent.category.LAUNCHER"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    invoke-virtual {v6, v5, v8}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 119
    .local v1, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 122
    .local v0, "actList":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/pm/ResolveInfo;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 123
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    .line 124
    .local v7, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    .line 125
    .local v3, "flag1":Z
    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 126
    .local v4, "flag2":Z
    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    if-eqz v9, :cond_0

    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 139
    const/4 v8, 0x1

    .line 142
    .end local v3    # "flag1":Z
    .end local v4    # "flag2":Z
    .end local v7    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_1
    return v8
.end method

.method public static isInstalled()Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 93
    const-string/jumbo v6, "com.sec.android.widgetapp.diotek.smemo"

    invoke-static {v6, v5}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v3

    .line 94
    .local v3, "smemo_installed":Z
    const-string/jumbo v6, "com.sec.android.app.memo"

    invoke-static {v6, v5}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v2

    .line 98
    .local v2, "memo_installed":Z
    if-nez v3, :cond_0

    if-eqz v2, :cond_1

    .line 100
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 101
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string/jumbo v7, "com.sec.android.provider.snote"

    const/16 v8, 0x2000

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v4, v5

    .line 108
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    :goto_0
    return v4

    .line 103
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method


# virtual methods
.method public deleteMemo(Landroid/content/Context;J)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 232
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getIntentNameDelete()Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 236
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "id"

    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 238
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->isBroadcast()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 239
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public getCreateMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    const-string/jumbo v0, "com.sec.android.memo.CREATE_TMEMO"

    return-object v0
.end method

.method protected getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 88
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;-><init>(Landroid/database/Cursor;)V

    .line 89
    .local v0, "indices":Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->getMemo(Landroid/database/Cursor;Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util$MemoIndices;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method public getMemos(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255
    const/4 v6, 0x0

    .line 256
    .local v6, "cur":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 259
    .local v7, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getContentProviderUri()Landroid/net/Uri;

    move-result-object v1

    .line 260
    .local v1, "contentUri":Landroid/net/Uri;
    const/4 v3, 0x0

    .line 261
    .local v3, "SELECTION":Ljava/lang/String;
    if-nez p2, :cond_2

    .line 262
    const-string/jumbo v3, "IsFolder=\'0\' and Islocked=\'0\' and deleted=\'0\'"

    .line 271
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 272
    if-nez v6, :cond_4

    .line 288
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 289
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 292
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v3    # "SELECTION":Ljava/lang/String;
    :cond_1
    :goto_2
    return-object v7

    .line 264
    .restart local v1    # "contentUri":Landroid/net/Uri;
    .restart local v3    # "SELECTION":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 265
    const-string/jumbo v3, "IsFolder=\'0\' and Islocked=\'0\' and deleted=\'0\'"

    goto :goto_0

    .line 267
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " and "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "IsFolder=\'0\' and Islocked=\'0\' and deleted=\'0\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 275
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 281
    .end local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .local v8, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :cond_5
    :try_start_2
    invoke-virtual {p0, v6}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->getMemo(Landroid/database/Cursor;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-nez v0, :cond_5

    move-object v7, v8

    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_1

    .line 284
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v3    # "SELECTION":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 288
    :goto_3
    if-eqz v6, :cond_1

    .line 289
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 288
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v6, :cond_6

    .line 289
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 288
    .end local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v1    # "contentUri":Landroid/net/Uri;
    .restart local v3    # "SELECTION":Ljava/lang/String;
    .restart local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :catchall_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_4

    .line 284
    .end local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :catch_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .restart local v7    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_3
.end method

.method public getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    .line 248
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    .line 249
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "ModifiedTime DESC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v1

    return-object v1
.end method

.method protected getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method public getViewMemoAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    const-string/jumbo v0, "com.sec.android.memo.OPEN_ID"

    return-object v0
.end method

.method public saveMemoData(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->getIntentNameCreate()Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 177
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "title"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    const-string/jumbo v2, "content"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;->isBroadcast()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 180
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 184
    :goto_0
    return-void

    .line 182
    :cond_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public searchMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    .line 227
    .local v0, "fields":[Ljava/lang/String;
    const-string/jumbo v1, "ModifiedTime DESC"

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->searchMemos(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public updateMemo(Landroid/content/Context;Lcom/vlingo/core/internal/memo/Memo;Lcom/vlingo/core/internal/memo/Memo;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "originalMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .param p3, "changedMemo"    # Lcom/vlingo/core/internal/memo/Memo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/core/internal/memo/MemoUtilException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 188
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 189
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "name"

    invoke-virtual {p3}, Lcom/vlingo/core/internal/memo/Memo;->getContent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p2}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v3

    int-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->getUriFromId(J)Landroid/net/Uri;

    move-result-object v1

    .line 192
    .local v1, "updateUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 193
    .local v0, "rowsUpdated":I
    const/4 v3, 0x1

    if-eq v0, v3, :cond_0

    .line 194
    new-instance v3, Lcom/vlingo/core/internal/memo/MemoUtilException;

    const-string/jumbo v4, "Error in updating memo."

    invoke-direct {v3, v4}, Lcom/vlingo/core/internal/memo/MemoUtilException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 196
    :cond_0
    return-void
.end method
