.class public Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;
.super Ljava/lang/Object;
.source "MemoAudioUtil.java"


# static fields
.field private static final AMR_HEADER:Ljava/lang/String; = "!#AMR\n"

.field private static final TAG:Ljava/lang/String; = "MemoAudioUtil"

.field private static mediaCodec:Landroid/media/MediaCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static destroyMediaCodec()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 40
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    .line 42
    return-void
.end method

.method public static getInputByteArray()[B
    .locals 8

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    .local v0, "audioToMemoPCM":Ljava/io/RandomAccessFile;
    const/4 v4, 0x0

    .line 49
    .local v4, "input":[B
    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "driving_mode_audio."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "raw"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "r"

    invoke-direct {v1, v5, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    .end local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .local v1, "audioToMemoPCM":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v5

    long-to-int v5, v5

    new-array v4, v5, [B

    .line 54
    invoke-virtual {v1, v4}, Ljava/io/RandomAccessFile;->read([B)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 60
    if-eqz v1, :cond_2

    .line 62
    :try_start_2
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 69
    .end local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .restart local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    :cond_0
    :goto_0
    return-object v4

    .line 63
    .end local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .restart local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v2

    .line 64
    .local v2, "e":Ljava/io/IOException;
    const-string/jumbo v5, "MemoAudioUtil"

    const-string/jumbo v6, "IOExceptioin throw while closing audioToMemoPCM"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 65
    .end local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .restart local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    goto :goto_0

    .line 55
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 56
    .local v3, "e1":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    if-eqz v0, :cond_0

    .line 62
    :try_start_4
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 63
    :catch_2
    move-exception v2

    .line 64
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "MemoAudioUtil"

    const-string/jumbo v6, "IOExceptioin throw while closing audioToMemoPCM"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 57
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "e1":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 58
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 60
    if-eqz v0, :cond_0

    .line 62
    :try_start_6
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 63
    :catch_4
    move-exception v2

    .line 64
    const-string/jumbo v5, "MemoAudioUtil"

    const-string/jumbo v6, "IOExceptioin throw while closing audioToMemoPCM"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 60
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v0, :cond_1

    .line 62
    :try_start_7
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 65
    :cond_1
    :goto_4
    throw v5

    .line 63
    :catch_5
    move-exception v2

    .line 64
    .restart local v2    # "e":Ljava/io/IOException;
    const-string/jumbo v6, "MemoAudioUtil"

    const-string/jumbo v7, "IOExceptioin throw while closing audioToMemoPCM"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 60
    .end local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .restart local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    goto :goto_3

    .line 57
    .end local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .restart local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .restart local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 55
    .end local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .restart local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    :catch_7
    move-exception v3

    move-object v0, v1

    .end local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .restart local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    goto :goto_1

    .end local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .restart local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    :cond_2
    move-object v0, v1

    .end local v1    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    .restart local v0    # "audioToMemoPCM":Ljava/io/RandomAccessFile;
    goto :goto_0
.end method

.method private static initMediaCodec(I)V
    .locals 6
    .param p0, "maxInputSize"    # I

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x1f40

    const/4 v3, 0x1

    .line 27
    const-string/jumbo v1, "audio/3gpp"

    invoke-static {v1}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v1

    sput-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    .line 28
    const-string/jumbo v1, "audio/3gpp"

    invoke-static {v1, v4, v3}, Landroid/media/MediaFormat;->createAudioFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    .line 29
    .local v0, "mediaFormat":Landroid/media/MediaFormat;
    const-string/jumbo v1, "bitrate"

    const/16 v2, 0x3200

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 30
    const-string/jumbo v1, "sample-rate"

    invoke-virtual {v0, v1, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 31
    const-string/jumbo v1, "channel-count"

    invoke-virtual {v0, v1, v3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 34
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1, v0, v5, v5, v3}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 35
    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->start()V

    .line 36
    return-void
.end method

.method public static prepareMemoAudioFile([B)Ljava/io/File;
    .locals 23
    .param p0, "input"    # [B

    .prologue
    .line 73
    new-instance v9, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    const-string/jumbo v4, "driving_mode_audio.amr"

    invoke-direct {v9, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 76
    .local v9, "audioToMemoAMR":Ljava/io/File;
    move-object/from16 v0, p0

    array-length v2, v0

    const-string/jumbo v4, "!#AMR\n"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v2, v4

    invoke-static {v2}, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->initMediaCodec(I)V

    .line 77
    new-instance v10, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v10}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 78
    .local v10, "bufferInfo":Landroid/media/MediaCodec$BufferInfo;
    const/4 v13, 0x0

    .line 79
    .local v13, "fos":Ljava/io/FileOutputStream;
    const/4 v15, 0x1

    .line 80
    .local v15, "hasMoreData":Z
    const v2, 0xbb80

    new-array v0, v2, [B

    move-object/from16 v21, v0

    .line 81
    .local v21, "readBuffer":[B
    const v2, 0xbb80

    new-array v0, v2, [B

    move-object/from16 v22, v0

    .line 83
    .local v22, "writeBuffer":[B
    new-instance v18, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 86
    .local v18, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v14, Ljava/io/FileOutputStream;

    invoke-direct {v14, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .local v14, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    const-string/jumbo v2, "!#AMR\n"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/4 v4, 0x0

    const-string/jumbo v6, "!#AMR\n"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v14, v2, v4, v6}, Ljava/io/FileOutputStream;->write([BII)V

    .line 89
    :cond_0
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v17

    .line 90
    .local v17, "inputBuffers":[Ljava/nio/ByteBuffer;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v20

    .line 91
    .local v20, "outputBuffers":[Ljava/nio/ByteBuffer;
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    const-wide/16 v6, -0x1

    invoke-virtual {v2, v6, v7}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v3

    .line 92
    .local v3, "inputBufferIndex":I
    if-ltz v3, :cond_3

    .line 93
    aget-object v16, v17, v3

    .line 94
    .local v16, "inputBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 96
    const/4 v2, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v4

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2, v4}, Ljava/io/InputStream;->read([BII)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    .line 97
    .local v5, "bytesRead":I
    const/4 v2, -0x1

    if-ne v5, v2, :cond_2

    .line 121
    .end local v5    # "bytesRead":I
    .end local v16    # "inputBuffer":Ljava/nio/ByteBuffer;
    :goto_0
    if-eqz v14, :cond_7

    .line 123
    :try_start_2
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v13, v14

    .line 129
    .end local v3    # "inputBufferIndex":I
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .end local v17    # "inputBuffers":[Ljava/nio/ByteBuffer;
    .end local v20    # "outputBuffers":[Ljava/nio/ByteBuffer;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    :cond_1
    :goto_1
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->destroyMediaCodec()V

    .line 130
    return-object v9

    .line 100
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "inputBufferIndex":I
    .restart local v5    # "bytesRead":I
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "inputBuffer":Ljava/nio/ByteBuffer;
    .restart local v17    # "inputBuffers":[Ljava/nio/ByteBuffer;
    .restart local v20    # "outputBuffers":[Ljava/nio/ByteBuffer;
    :cond_2
    :try_start_3
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    if-lt v5, v2, :cond_4

    const/4 v15, 0x1

    .line 101
    :goto_2
    const/4 v2, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2, v5}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 103
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    if-eqz v15, :cond_5

    const/4 v8, 0x0

    :goto_3
    invoke-virtual/range {v2 .. v8}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 107
    .end local v5    # "bytesRead":I
    .end local v16    # "inputBuffer":Ljava/nio/ByteBuffer;
    :cond_3
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    const-wide/16 v6, -0x1

    invoke-virtual {v2, v10, v6, v7}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v19

    .line 109
    .local v19, "outputBufferIndex":I
    aget-object v2, v20, v19

    iget v4, v10, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 110
    aget-object v2, v20, v19

    const/4 v4, 0x0

    iget v6, v10, Landroid/media/MediaCodec$BufferInfo;->size:I

    move-object/from16 v0, v22

    invoke-virtual {v2, v0, v4, v6}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 111
    const/4 v2, 0x0

    iget v4, v10, Landroid/media/MediaCodec$BufferInfo;->size:I

    move-object/from16 v0, v22

    invoke-virtual {v14, v0, v2, v4}, Ljava/io/FileOutputStream;->write([BII)V

    .line 112
    aget-object v2, v20, v19

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 114
    sget-object v2, Lcom/vlingo/midas/samsungutils/utils/memo/MemoAudioUtil;->mediaCodec:Landroid/media/MediaCodec;

    const/4 v4, 0x0

    move/from16 v0, v19

    invoke-virtual {v2, v0, v4}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 115
    iget v2, v10, Landroid/media/MediaCodec$BufferInfo;->flags:I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v4, 0x4

    if-ne v2, v4, :cond_0

    goto :goto_0

    .line 100
    .end local v19    # "outputBufferIndex":I
    .restart local v5    # "bytesRead":I
    .restart local v16    # "inputBuffer":Ljava/nio/ByteBuffer;
    :cond_4
    const/4 v15, 0x0

    goto :goto_2

    .line 103
    :cond_5
    const/4 v8, 0x4

    goto :goto_3

    .line 124
    .end local v5    # "bytesRead":I
    .end local v16    # "inputBuffer":Ljava/nio/ByteBuffer;
    :catch_0
    move-exception v11

    .line 125
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    move-object v13, v14

    .line 126
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 116
    .end local v3    # "inputBufferIndex":I
    .end local v11    # "e":Ljava/io/IOException;
    .end local v17    # "inputBuffers":[Ljava/nio/ByteBuffer;
    .end local v20    # "outputBuffers":[Ljava/nio/ByteBuffer;
    :catch_1
    move-exception v12

    .line 117
    .local v12, "e1":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_4
    invoke-virtual {v12}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 121
    if-eqz v13, :cond_1

    .line 123
    :try_start_5
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 124
    :catch_2
    move-exception v11

    .line 125
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 118
    .end local v11    # "e":Ljava/io/IOException;
    .end local v12    # "e1":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v11

    .line 119
    .restart local v11    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_6
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 121
    if-eqz v13, :cond_1

    .line 123
    :try_start_7
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_1

    .line 124
    :catch_4
    move-exception v11

    .line 125
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 121
    .end local v11    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    :goto_6
    if-eqz v13, :cond_6

    .line 123
    :try_start_8
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 126
    :cond_6
    :goto_7
    throw v2

    .line 124
    :catch_5
    move-exception v11

    .line 125
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 121
    .end local v11    # "e":Ljava/io/IOException;
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v2

    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto :goto_6

    .line 118
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v11

    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 116
    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v12

    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .end local v13    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "inputBufferIndex":I
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v17    # "inputBuffers":[Ljava/nio/ByteBuffer;
    .restart local v20    # "outputBuffers":[Ljava/nio/ByteBuffer;
    :cond_7
    move-object v13, v14

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_1
.end method
