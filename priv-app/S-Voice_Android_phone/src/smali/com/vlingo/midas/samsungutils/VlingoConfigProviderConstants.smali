.class public Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;
.super Ljava/lang/Object;
.source "VlingoConfigProviderConstants.java"


# static fields
.field public static final SVOICE_CONTENT_PROVIDER_AUTHORITY:Ljava/lang/String;

.field public static final SVOICE_CONTENT_PROVIDER_URI:Ljava/lang/String;

.field public static final SVOICE_CONTENT_PROVIDER_VERSION:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getSettingsProviderName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_AUTHORITY:Ljava/lang/String;

    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_AUTHORITY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_URI:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
