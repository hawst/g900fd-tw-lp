.class public final Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;
.super Ljava/lang/Object;
.source "SettingsInitializer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$2;,
        Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;,
        Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;,
        Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;
    }
.end annotation


# static fields
.field private static final SETTINGS_FOR_APPLICATION:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

.field private static final SETTINGS_FOR_APPLICATION_PROPOSED:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

.field private static final SETTINGS_FOR_SERVICE:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

.field private static final TAG:Ljava/lang/String;

.field private static followType:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

.field private static settingsInUse:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 27
    const-class v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    .line 52
    const/16 v0, 0x54

    new-array v0, v0, [Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "language"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "audiofilelog_enabled"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "phrasespot_waveformlogging_enabled"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "driving_mode_audio_files"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v8

    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "location_enabled"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v9

    const/4 v1, 0x5

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "custom_tone_encoding"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "calendar.default_calendar_key"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "dm.username"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "FORCE_NON_DM"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_voice_prompt_confirm_with_user"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "contacts.use_other_names"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "auto_dial"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "auto_endpointing"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "CARRIER"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "CARRIER_COUNTRY"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVER_NAME"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVICES_HOST_NAME"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "EVENTLOG_HOST_NAME"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "HELLO_HOST_NAME"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "LMTT_HOST_NAME"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.asr_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.hello_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.lmtt_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.log_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.tts_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.vcs_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.rollout_groupid"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "https.rollout_percentage"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "calendar.app_package"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "calendar.preference_filename"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "new_contact_match_algo"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "processing_tone_fadeout_period"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "profanity_filter"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tos_accepted"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tos_accepted_date"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x23

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tos_accepted_version"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x24

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "iux_complete"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "samsung_disclaimer_accepted"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "car_iux_tts_cacheing_required"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x27

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_tts_fallback_engine"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x28

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_ignore_use_speech_rate"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x29

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_required_engine"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_audiotrack_tone_player"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_default_phone"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_hidden_calendars"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_mediasync_tone_approach"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_network_tts"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_non_j_audio_sources"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x30

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "validate_launch_intent_version"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x31

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "vcs.timeout.ms"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x32

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "weather_use_vp_location"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x33

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_voice_prompt"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x34

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "auto_punctuation"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x35

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "car_nav_home_address"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x36

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tos_launched_for_tos_other_app"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x37

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "former_tos_acceptance_state"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x38

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "uuid"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x39

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "zip_timestamp_"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "asr.manager"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "asr.http.keep_alive"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "asr.http.timeout.connect_ms"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "asr.http.timeout.read_ms"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "stats.enable"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "acceptedtext.enable"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x40

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "activitylog.enable"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x41

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "appstate.first_run.calypso"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x42

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "DEVICE_MODEL"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x43

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "dynamic_config_disabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x44

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time.speech.long"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x45

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time.speech.long.msg"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x46

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time.speech.medium"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x47

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time.speech.short"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x48

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time_withoutspeech"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x49

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "endpoint.time_withspeech"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "hello_request_complete"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "max_audio_time"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "nothing_recognized_reprompt.count"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "nothing_recognized_reprompt.max_value"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVER_RESONSE_FILE"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVER_RESONSE_LOGGGING"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x50

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "speex.complexity"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x51

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "speex.quality"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x52

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "speex.variable_bitrate"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x53

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "speex.voice_activity_detection"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_SERVICE:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .line 166
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "language"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "iux_complete"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "tos_accepted"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "tos_accepted_date"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v8

    new-instance v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v2, "tos_accepted_version"

    sget-object v3, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v1, v2, v3, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v1, v0, v9

    const/4 v1, 0x5

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "processing_tone_fadeout_period"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->INTEGER:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_audiotrack_tone_player"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_voice_prompt_confirm_with_user"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "new_contact_match_algo"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "car_iux_tts_cacheing_required"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_tts_fallback_engine"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_ignore_use_speech_rate"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "tts_local_required_engine"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "use_default_phone"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "audiofilelog_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "phrasespot_waveformlogging_enabled"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "driving_mode_audio_files"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "CARRIER"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "CARRIER_COUNTRY"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "profanity_filter"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->BOOLEAN:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "car_nav_home_address"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "uuid"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v5}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "DEVICE_MODEL"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVER_RESONSE_FILE"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-instance v2, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    const-string/jumbo v3, "SERVER_RESONSE_LOGGGING"

    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->STRING:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    invoke-direct {v2, v3, v4, v6}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;-><init>(Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;Z)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_APPLICATION_PROPOSED:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .line 338
    sget-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_APPLICATION_PROPOSED:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    sput-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_APPLICATION:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$500(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    .prologue
    .line 26
    invoke-static {p0, p1, p2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->setValue(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V

    return-void
.end method

.method public static initSettings(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;

    .prologue
    .line 347
    const/4 v0, 0x1

    .line 348
    .local v0, "amAService":Z
    invoke-static {p0, p1, v0}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->initSettings(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 349
    return-void
.end method

.method public static initSettings(Landroid/content/Context;Ljava/lang/String;IZ)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;
    .param p2, "providerVersion"    # I
    .param p3, "amAService"    # Z

    .prologue
    .line 372
    if-nez p3, :cond_0

    .line 376
    invoke-static {p0, p1}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->isReady(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 377
    const-string/jumbo v5, "applicationQueryManager returned null, ConfigProvider not ready?"

    .line 378
    .local v5, "msg":Ljava/lang/String;
    new-instance v9, Ljava/lang/IllegalStateException;

    invoke-direct {v9, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 382
    .end local v5    # "msg":Ljava/lang/String;
    :cond_0
    if-lez p2, :cond_1

    .line 391
    :cond_1
    if-eqz p3, :cond_2

    .line 392
    sget-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->SERVICE:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    sput-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->followType:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    .line 396
    :goto_0
    const-string/jumbo v9, "appstate.first_run.calypso"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 397
    .local v2, "firstRun":Z
    const-string/jumbo v9, "appstate.first_run.calypso"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 398
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 399
    .local v0, "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    sget-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->followType:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    sget-object v10, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->SERVICE:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    if-ne v9, v10, :cond_3

    .line 400
    sget-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_SERVICE:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    sput-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->settingsInUse:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .line 404
    :goto_1
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->settingsInUse:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .local v1, "arr$":[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_2
    if-ge v3, v4, :cond_7

    aget-object v7, v1, v3

    .line 405
    .local v7, "setting":Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    if-eqz v2, :cond_4

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->firstRunInitOnly:Z
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$000(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 406
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentProviderPath:Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$100(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 407
    .local v6, "newSettingValue":Ljava/lang/String;
    sget-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "First run initializes "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$200(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " to "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$200(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v9

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->type:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$300(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    move-result-object v10

    invoke-static {v9, v6, v10}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->setValue(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V

    .line 404
    .end local v6    # "newSettingValue":Ljava/lang/String;
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 394
    .end local v0    # "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    .end local v1    # "arr$":[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    .end local v2    # "firstRun":Z
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v7    # "setting":Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    :cond_2
    sget-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;->APPLICATION:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    sput-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->followType:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$FOLLOW_TYPE;

    goto :goto_0

    .line 402
    .restart local v0    # "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    .restart local v2    # "firstRun":Z
    :cond_3
    sget-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->SETTINGS_FOR_APPLICATION:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    sput-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->settingsInUse:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    goto :goto_1

    .line 409
    .restart local v1    # "arr$":[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    .restart local v7    # "setting":Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    :cond_4
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->firstRunInitOnly:Z
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$000(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 410
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentProviderPath:Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$100(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 411
    .restart local v6    # "newSettingValue":Ljava/lang/String;
    sget-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Initializing "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$200(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " to "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " and adding content observer."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$200(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v9

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->type:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$300(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    move-result-object v10

    invoke-static {v9, v6, v10}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->setValue(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V

    .line 413
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$400(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 414
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$400(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 417
    :cond_5
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentProviderPath:Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$100(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 418
    .local v8, "uri":Landroid/net/Uri;
    new-instance v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;

    new-instance v10, Landroid/os/Handler;

    invoke-direct {v10}, Landroid/os/Handler;-><init>()V

    invoke-direct {v9, v10, v7, v0, p1}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;-><init>(Landroid/os/Handler;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;Ljava/lang/String;)V

    # setter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v7, v9}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$402(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;Landroid/database/ContentObserver;)Landroid/database/ContentObserver;

    .line 425
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const/4 v10, 0x1

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$400(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;

    move-result-object v11

    invoke-virtual {v9, v8, v10, v11}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto/16 :goto_3

    .line 427
    .end local v6    # "newSettingValue":Ljava/lang/String;
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_6
    sget-object v9, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Setting "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$200(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " is skipped since it\'s updated on the first run only."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 430
    .end local v7    # "setting":Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    :cond_7
    return-void
.end method

.method public static initSettings(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;
    .param p2, "amAService"    # Z

    .prologue
    .line 360
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->initSettings(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 361
    return-void
.end method

.method public static isReady(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 478
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 479
    .local v0, "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    const-string/jumbo v4, "SETTINGS/tos_accepted"

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 480
    .local v2, "tosAcceptedStringValue":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 481
    const-string/jumbo v1, "applicationQueryManager returned null, ConfigProvider not ready?"

    .line 482
    .local v1, "msg":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    .end local v1    # "msg":Ljava/lang/String;
    :goto_0
    return v3

    .line 485
    :cond_0
    const-string/jumbo v4, "true"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 486
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "TOS not yet accepted, value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 487
    .restart local v1    # "msg":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 490
    .end local v1    # "msg":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static isTosAccepted(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "applicationContentProviderBase"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 497
    new-instance v0, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 498
    .local v0, "applicationQueryManager":Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;
    const-string/jumbo v4, "SETTINGS/tos_accepted"

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 499
    .local v2, "tosAcceptedStringValue":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 500
    const-string/jumbo v1, "applicationQueryManager returned null, ConfigProvider not ready?"

    .line 501
    .local v1, "msg":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    .end local v1    # "msg":Ljava/lang/String;
    :goto_0
    return v3

    .line 504
    :cond_0
    const-string/jumbo v4, "true"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 505
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "TOS not yet accepted, value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 506
    .restart local v1    # "msg":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 509
    .end local v1    # "msg":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private static setValue(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V
    .locals 4
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    .prologue
    .line 445
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Copying value for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": Value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 448
    :try_start_0
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$2;->$SwitchMap$com$vlingo$midas$samsungutils$configuration$SettingsInitializer$SETTING_TYPE:[I

    invoke-virtual {p2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 462
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    :goto_0
    return-void

    .line 450
    :pswitch_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {p0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 465
    :catch_0
    move-exception v0

    .line 466
    .local v0, "ex":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 453
    .end local v0    # "ex":Ljava/lang/NumberFormatException;
    :pswitch_1
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 456
    :pswitch_2
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setLong(Ljava/lang/String;J)V

    goto :goto_0

    .line 459
    :pswitch_3
    invoke-static {p1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {p0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setFloat(Ljava/lang/String;F)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 469
    :cond_0
    sget-object v1, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Failed to update from master "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 448
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static uninitSettings(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 436
    sget-object v0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->settingsInUse:[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    .local v0, "arr$":[Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 437
    .local v3, "setting":Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$400(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 438
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$400(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Landroid/database/ContentObserver;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 439
    const/4 v4, 0x0

    # setter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentObserver:Landroid/database/ContentObserver;
    invoke-static {v3, v4}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$402(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;Landroid/database/ContentObserver;)Landroid/database/ContentObserver;

    .line 436
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 442
    .end local v3    # "setting":Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;
    :cond_1
    return-void
.end method
