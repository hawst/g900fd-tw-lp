.class public Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;
.super Lcom/vlingo/core/internal/dialogmanager/StateController;
.source "MemoController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private memoSelection:Lcom/vlingo/core/internal/memo/Memo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;-><init>()V

    return-void
.end method


# virtual methods
.method public deleteMemoAction(Lcom/vlingo/core/internal/memo/Memo;)V
    .locals 2
    .param p1, "memo"    # Lcom/vlingo/core/internal/memo/Memo;

    .prologue
    .line 191
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteMemoInterface;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteMemoInterface;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteMemoInterface;->memo(Lcom/vlingo/core/internal/memo/Memo;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteMemoInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteMemoInterface;->queue()V

    .line 194
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 15
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 101
    invoke-super/range {p0 .. p2}, Lcom/vlingo/core/internal/dialogmanager/StateController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 103
    invoke-interface/range {p1 .. p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "LPAction"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 106
    const/4 v9, 0x0

    .line 187
    :goto_0
    return v9

    .line 109
    :cond_0
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->memoSelection:Lcom/vlingo/core/internal/memo/Memo;

    .line 110
    const-string/jumbo v9, "Name"

    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 111
    .local v6, "name":Ljava/lang/String;
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 113
    .local v1, "actionName":Ljava/lang/String;
    const-string/jumbo v9, "Which"

    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 114
    .local v7, "ordinal":Ljava/lang/String;
    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 115
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 122
    :goto_1
    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    iget-object v9, p0, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->memoSelection:Lcom/vlingo/core/internal/memo/Memo;

    if-nez v9, :cond_3

    .line 123
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 131
    .local v5, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :goto_2
    if-nez v5, :cond_5

    iget-object v9, p0, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->memoSelection:Lcom/vlingo/core/internal/memo/Memo;

    if-nez v9, :cond_5

    .line 134
    if-eqz v6, :cond_4

    .line 135
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved_about:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-static {v10, v11}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 139
    :goto_3
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 187
    :cond_1
    :goto_4
    const/4 v9, 0x0

    goto :goto_0

    .line 117
    .end local v5    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :cond_2
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v5

    .line 118
    .restart local v5    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 119
    .local v8, "pageMemos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    invoke-static {v8, v7}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/memo/Memo;

    iput-object v9, p0, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->memoSelection:Lcom/vlingo/core/internal/memo/Memo;

    goto :goto_1

    .line 125
    .end local v5    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .end local v8    # "pageMemos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/MemoManager;->getMemoUtil()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v4

    .line 128
    .local v4, "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v9

    invoke-interface {v4, v9, v6}, Lcom/vlingo/core/internal/memo/IMemoUtil;->searchMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .restart local v5    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_2

    .line 137
    .end local v4    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v10, v11}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto :goto_3

    .line 140
    :cond_5
    if-eqz v5, :cond_9

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_6

    iget-object v9, p0, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->memoSelection:Lcom/vlingo/core/internal/memo/Memo;

    if-eqz v9, :cond_9

    .line 145
    :cond_6
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_7

    .line 146
    const/4 v9, 0x0

    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/memo/Memo;

    .line 147
    .local v3, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iput-object v3, p0, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->memoSelection:Lcom/vlingo/core/internal/memo/Memo;

    .line 151
    :goto_5
    const-string/jumbo v9, "MemoDelete"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 154
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    const/4 v10, 0x0

    sget-object v11, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_confirm_delete:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-virtual {v3, v14}, Lcom/vlingo/core/internal/memo/Memo;->getMemoName(Z)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_DELETEPROMPT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v12}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v12

    invoke-virtual {v9, v10, v11, v12}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 156
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Memo:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v9, v10, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_4

    .line 149
    .end local v3    # "memo":Lcom/vlingo/core/internal/memo/Memo;
    :cond_7
    iget-object v3, p0, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->memoSelection:Lcom/vlingo/core/internal/memo/Memo;

    .restart local v3    # "memo":Lcom/vlingo/core/internal/memo/Memo;
    goto :goto_5

    .line 157
    :cond_8
    const-string/jumbo v9, "MemoLookup"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 160
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Memo:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v9, v10, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 163
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    goto/16 :goto_4

    .line 169
    .end local v3    # "memo":Lcom/vlingo/core/internal/memo/Memo;
    :cond_9
    if-eqz v5, :cond_b

    .line 174
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->getDisplayCount(I)I

    move-result v9

    move-object/from16 v0, p2

    invoke-static {v0, v5, v9}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;I)V

    .line 176
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_DELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v9}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    .line 177
    .local v2, "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    const-string/jumbo v9, "MemoLookup"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 178
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_LOOKUPCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v9}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    .line 181
    :cond_a
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 182
    .restart local v8    # "pageMemos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MemoList:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v9, v10, v8, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto/16 :goto_4

    .line 184
    .end local v2    # "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .end local v8    # "pageMemos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :cond_b
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v10, v11}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto/16 :goto_4
.end method

.method public getRuleMappings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRules()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 44
    move-object v1, p2

    check-cast v1, Lcom/vlingo/core/internal/memo/Memo;

    .line 45
    .local v1, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iput-object v1, p0, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->memoSelection:Lcom/vlingo/core/internal/memo/Memo;

    .line 46
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 47
    .local v0, "actionName":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    if-eqz v0, :cond_0

    .line 56
    const-string/jumbo v2, "MemoDelete"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 57
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 58
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_confirm_delete:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1, v6}, Lcom/vlingo/core/internal/memo/Memo;->getMemoName(Z)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_DELETEPROMPT:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v4}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-virtual {v2, v7, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 61
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Memo:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v2, v3, v7, v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0

    .line 62
    :cond_2
    const-string/jumbo v2, "MemoLookup"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 63
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Memo:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v2, v3, v7, v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 65
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishTurn()V

    .line 66
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->reset()V

    goto :goto_0

    .line 70
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 2
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v0, 0x0

    .line 89
    const-string/jumbo v1, "execute"

    invoke-static {p1, v1, v0, v0}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->memoSelection:Lcom/vlingo/core/internal/memo/Memo;

    if-eqz v1, :cond_0

    .line 90
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->memoSelection:Lcom/vlingo/core/internal/memo/Memo;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->deleteMemoAction(Lcom/vlingo/core/internal/memo/Memo;)V

    .line 91
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 92
    const/4 v0, 0x1

    .line 94
    :cond_0
    return v0
.end method
