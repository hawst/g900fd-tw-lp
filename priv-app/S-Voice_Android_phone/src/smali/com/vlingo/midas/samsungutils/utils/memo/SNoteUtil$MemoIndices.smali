.class public Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;
.super Ljava/lang/Object;
.source "SNoteUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "MemoIndices"
.end annotation


# instance fields
.field public final KEY_DATE_COL:I

.field final KEY_ID_COL:I

.field final KEY_NAME_COL:I

.field final KEY_PATH_COL:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    const-string/jumbo v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;->KEY_ID_COL:I

    .line 309
    const-string/jumbo v0, "path"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;->KEY_PATH_COL:I

    .line 310
    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;->KEY_NAME_COL:I

    .line 311
    const-string/jumbo v0, "ModifiedTime"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil$MemoIndices;->KEY_DATE_COL:I

    .line 312
    return-void
.end method
