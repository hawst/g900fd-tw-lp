.class public final Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;
.super Ljava/lang/Object;
.source "WidgetUtils.java"


# static fields
.field private static final cappingItems:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10
    const-string/jumbo v0, "multi.widget.client.capped"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->cappingItems:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDisplayCount(I)I
    .locals 4
    .param p0, "naturalSize"    # I

    .prologue
    .line 15
    move v1, p0

    .line 16
    .local v1, "toReturn":I
    sget-boolean v3, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->cappingItems:Z

    if-nez v3, :cond_0

    move v2, v1

    .line 23
    .end local v1    # "toReturn":I
    .local v2, "toReturn":I
    :goto_0
    return v2

    .line 19
    .end local v2    # "toReturn":I
    .restart local v1    # "toReturn":I
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->getMultiWidgetItemsInitialMax()I

    move-result v0

    .line 20
    .local v0, "capSize":I
    if-le p0, v0, :cond_1

    .line 21
    move v1, v0

    :cond_1
    move v2, v1

    .line 23
    .end local v1    # "toReturn":I
    .restart local v2    # "toReturn":I
    goto :goto_0
.end method

.method public static getMultiWidgetItemsInitialMax()I
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getMultiWidgetItemsInitialMax()I

    move-result v0

    return v0
.end method
