.class public Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;
.super Ljava/lang/Object;
.source "TMemoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "MemoIndices"
.end annotation


# instance fields
.field final KEY_CONTENT_COL:I

.field final KEY_CREATE_COL:I

.field final KEY_ID_COL:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    const-string/jumbo v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;->KEY_ID_COL:I

    .line 180
    const-string/jumbo v0, "Content"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;->KEY_CONTENT_COL:I

    .line 181
    const-string/jumbo v0, "create_t"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil$MemoIndices;->KEY_CREATE_COL:I

    .line 182
    return-void
.end method
