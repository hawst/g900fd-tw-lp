.class public Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;
.super Ljava/lang/Object;
.source "KMemoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "MemoIndices"
.end annotation


# instance fields
.field final KEY_CONTENT_COL:I

.field public final KEY_DATE_COL:I

.field final KEY_ID_COL:I

.field public final KEY_TEXT_COL:I

.field final KEY_TITLE_COL:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    const-string/jumbo v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_ID_COL:I

    .line 212
    const-string/jumbo v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_TITLE_COL:I

    .line 213
    const-string/jumbo v0, "strippedContent"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_CONTENT_COL:I

    .line 214
    const-string/jumbo v0, "strippedContent"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_TEXT_COL:I

    .line 215
    const-string/jumbo v0, "lastModifiedAt"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$MemoIndices;->KEY_DATE_COL:I

    .line 216
    return-void
.end method
