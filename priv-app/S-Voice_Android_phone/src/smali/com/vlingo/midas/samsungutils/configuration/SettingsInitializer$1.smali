.class final Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;
.super Landroid/database/ContentObserver;
.source "SettingsInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->initSettings(Landroid/content/Context;Ljava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$applicationContentProviderBase:Ljava/lang/String;

.field final synthetic val$applicationQueryManager:Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

.field final synthetic val$setting:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;


# direct methods
.method constructor <init>(Landroid/os/Handler;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;Ljava/lang/String;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Handler;

    .prologue
    .line 418
    iput-object p2, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;->val$setting:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    iput-object p3, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;->val$applicationQueryManager:Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    iput-object p4, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;->val$applicationContentProviderBase:Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .prologue
    .line 421
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;->val$setting:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->name:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$200(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;->val$applicationQueryManager:Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;->val$applicationContentProviderBase:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;->val$setting:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->contentProviderPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$100(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/associatedservice/ApplicationQueryManager;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$1;->val$setting:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;

    # getter for: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->type:Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;
    invoke-static {v2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;->access$300(Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$Setting;)Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;

    move-result-object v2

    # invokes: Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->setValue(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V
    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer;->access$500(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/midas/samsungutils/configuration/SettingsInitializer$SETTING_TYPE;)V

    .line 422
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 423
    return-void
.end method
