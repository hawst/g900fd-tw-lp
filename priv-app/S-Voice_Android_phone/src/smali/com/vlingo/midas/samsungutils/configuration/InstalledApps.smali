.class public Lcom/vlingo/midas/samsungutils/configuration/InstalledApps;
.super Ljava/lang/Object;
.source "InstalledApps.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generateConfigPairs()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 30
    .local v0, "pairs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasMessaging()Z

    move-result v1

    if-nez v1, :cond_0

    .line 33
    const-string/jumbo v1, "hasMessageApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasKoreanSpecialNavigation()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    const-string/jumbo v1, "hasKoreanSpecialNavigationApp"

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasTimer()Z

    move-result v1

    if-nez v1, :cond_2

    .line 46
    const-string/jumbo v1, "hasTimerApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasVoiceRecorder()Z

    move-result v1

    if-nez v1, :cond_3

    .line 51
    const-string/jumbo v1, "hasRecordVoiceApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasDialing()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasCallEnabled()Z

    move-result v1

    if-nez v1, :cond_5

    .line 56
    :cond_4
    const-string/jumbo v1, "hasPhoneApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasTelephony()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasCallEnabled()Z

    move-result v1

    if-nez v1, :cond_7

    .line 61
    :cond_6
    const-string/jumbo v1, "hasTelephony"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasRadio()Z

    move-result v1

    if-nez v1, :cond_8

    .line 67
    const-string/jumbo v1, "hasRadioApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    :cond_8
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasAlarm()Z

    move-result v1

    if-nez v1, :cond_9

    .line 72
    const-string/jumbo v1, "hasAlarmApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    :cond_9
    const-string/jumbo v1, "CTC"

    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getPhoneSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 77
    sget-object v1, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->ENAVI:[Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasApp([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 80
    const-string/jumbo v1, "hasNavigationApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    :cond_a
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasMemo()Z

    move-result v1

    if-nez v1, :cond_b

    .line 102
    const-string/jumbo v1, "hasMemoApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    :cond_b
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasMusic()Z

    move-result v1

    if-nez v1, :cond_c

    .line 107
    const-string/jumbo v1, "hasMusicApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    :cond_c
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasTask()Z

    move-result v1

    if-nez v1, :cond_d

    .line 117
    const-string/jumbo v1, "hasTaskApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    :cond_d
    return-object v0

    .line 83
    :cond_e
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->isChinesePhone()Z

    move-result v1

    if-nez v1, :cond_f

    .line 86
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasNav()Z

    move-result v1

    if-nez v1, :cond_a

    .line 89
    const-string/jumbo v1, "hasNavigationApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 92
    :cond_f
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasChineseNav()Z

    move-result v1

    if-nez v1, :cond_a

    .line 95
    const-string/jumbo v1, "hasNavigationApp"

    const-string/jumbo v2, "false"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
