.class public Lcom/vlingo/midas/samsungutils/utils/memo/MemoManager;
.super Ljava/lang/Object;
.source "MemoManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMemoUtil()Lcom/vlingo/core/internal/memo/IMemoUtil;
    .locals 2

    .prologue
    .line 9
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->isInstalled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v0

    .line 22
    .local v0, "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    :goto_0
    return-object v0

    .line 11
    .end local v0    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->isInstalled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 12
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v0

    .restart local v0    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    goto :goto_0

    .line 13
    .end local v0    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->isInstalled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 14
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v0

    .restart local v0    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    goto :goto_0

    .line 15
    .end local v0    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->isInstalled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 16
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/TMemoUtil;->getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v0

    .restart local v0    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    goto :goto_0

    .line 17
    .end local v0    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->isInstalled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 18
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SNoteUtil;->getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v0

    .restart local v0    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    goto :goto_0

    .line 20
    .end local v0    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemoUtil;->getInstance()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v0

    .restart local v0    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    goto :goto_0
.end method
