.class public Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;
.super Ljava/lang/Object;
.source "SNote2Util.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "MemoIndices"
.end annotation


# instance fields
.field final KEY_CONTENT_COL:I

.field final KEY_DATE_COL:I

.field final KEY_ID_COL:I

.field final KEY_NAME_COL:I

.field final KEY_TAG_CONTENT_COL:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300
    const-string/jumbo v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;->KEY_ID_COL:I

    .line 301
    const-string/jumbo v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;->KEY_NAME_COL:I

    .line 302
    const-string/jumbo v0, "ModifiedTime"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;->KEY_DATE_COL:I

    .line 303
    const-string/jumbo v0, "content"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;->KEY_CONTENT_COL:I

    .line 304
    const-string/jumbo v0, "Tag_Content"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util$MemoIndices;->KEY_TAG_CONTENT_COL:I

    .line 305
    return-void
.end method
