.class Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$1;
.super Ljava/lang/Object;
.source "KMemoUtil.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$1;->this$0:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$1;->this$0:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    invoke-static {p2}, Lcom/samsung/android/app/memo/MemoServiceIF$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/app/memo/MemoServiceIF;

    move-result-object v1

    # setter for: Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;
    invoke-static {v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->access$002(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;Lcom/samsung/android/app/memo/MemoServiceIF;)Lcom/samsung/android/app/memo/MemoServiceIF;

    .line 268
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$1;->this$0:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    # invokes: Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->createPendingMemos()V
    invoke-static {v0}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->access$100(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;)V

    .line 269
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil$1;->this$0:Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->mMemoSvcIF:Lcom/samsung/android/app/memo/MemoServiceIF;
    invoke-static {v0, v1}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->access$002(Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;Lcom/samsung/android/app/memo/MemoServiceIF;)Lcom/samsung/android/app/memo/MemoServiceIF;

    .line 274
    return-void
.end method
