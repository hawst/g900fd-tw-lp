.class public Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SaveMemoAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/MemoInterface;


# instance fields
.field private memo:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->memo:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected execute()V
    .locals 4

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->memo:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    const-string/jumbo v1, "missing memo"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 54
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;->getInstance()Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/CheckPhoneResourceUtil;->getExternalAvailableSpaceInMB()J

    move-result-wide v0

    const-wide/16 v2, 0x32

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 39
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    const-string/jumbo v1, "Device Storage is not enough for memo"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 42
    new-instance v0, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction$1;-><init>(Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;)V

    const-wide/16 v1, 0x5dc

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public memo(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/MemoInterface;
    .locals 0
    .param p1, "memoParam"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;->memo:Ljava/lang/String;

    .line 25
    return-object p0
.end method
