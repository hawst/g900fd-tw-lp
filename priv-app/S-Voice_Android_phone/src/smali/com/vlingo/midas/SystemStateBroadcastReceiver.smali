.class public Lcom/vlingo/midas/SystemStateBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SystemStateBroadcastReceiver.java"


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

.field private isAttached:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/vlingo/midas/SystemStateBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/SystemStateBroadcastReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/SystemStateBroadcastReceiver;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 30
    iget-object v5, p0, Lcom/vlingo/midas/SystemStateBroadcastReceiver;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    if-nez v5, :cond_0

    .line 32
    invoke-static {p1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/SystemStateBroadcastReceiver;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    .line 35
    :cond_0
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "action":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/midas/SystemStateBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "onReceive(), action="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    const-string/jumbo v5, "com.vlingo.client.app.action.APPLICATION_STATE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 43
    const-string/jumbo v5, "com.vlingo.client.app.extra.STATE"

    const/4 v6, -0x1

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 46
    .local v4, "newState":I
    if-ne v4, v8, :cond_5

    .line 49
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    .line 50
    .local v2, "isBtHeadsetConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v1

    .line 51
    .local v1, "isBluetoothAudioSupported":Z
    if-ne v2, v8, :cond_1

    if-eq v1, v8, :cond_4

    .line 52
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isMusicEchoCancellerSupported()Z

    move-result v5

    if-nez v5, :cond_2

    .line 53
    iget-object v5, p0, Lcom/vlingo/midas/SystemStateBroadcastReceiver;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    const/4 v6, 0x3

    invoke-virtual {v5, v6, v10}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 58
    :cond_2
    :goto_0
    invoke-static {v9}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->considerRightBeforeForeground(Z)V

    .line 90
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "isBluetoothAudioSupported":Z
    .end local v2    # "isBtHeadsetConnected":Z
    .end local v4    # "newState":I
    :cond_3
    :goto_1
    return-void

    .line 56
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "isBluetoothAudioSupported":Z
    .restart local v2    # "isBtHeadsetConnected":Z
    .restart local v4    # "newState":I
    :cond_4
    iget-object v5, p0, Lcom/vlingo/midas/SystemStateBroadcastReceiver;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    const/4 v6, 0x6

    invoke-virtual {v5, v6, v10}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    goto :goto_0

    .line 60
    .end local v1    # "isBluetoothAudioSupported":Z
    .end local v2    # "isBtHeadsetConnected":Z
    :cond_5
    if-nez v4, :cond_3

    .line 63
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isMusicEchoCancellerSupported()Z

    move-result v5

    if-nez v5, :cond_3

    .line 64
    iget-object v5, p0, Lcom/vlingo/midas/SystemStateBroadcastReceiver;->audioFocusManager:Lcom/vlingo/core/internal/audio/AudioFocusManager;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    goto :goto_1

    .line 68
    .end local v4    # "newState":I
    :cond_6
    const-string/jumbo v5, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 69
    const-string/jumbo v5, "launch_voicetalk"

    invoke-static {v5, v8}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 70
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v5

    if-nez v5, :cond_3

    .line 74
    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/VlingoApplication;->getMainActivityClass()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v3, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 75
    .local v3, "newIntent":Landroid/content/Intent;
    const/high16 v5, 0x10000000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 77
    const-string/jumbo v5, "isSecure"

    invoke-virtual {p2, v5, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_7

    .line 78
    const-string/jumbo v5, "AUTO_LISTEN"

    const-string/jumbo v6, "AUTO_LISTEN"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 80
    :cond_7
    const-string/jumbo v5, "isThisComeFromHomeKeyDoubleClickConcept"

    const-string/jumbo v6, "isThisComeFromHomeKeyDoubleClickConcept"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 81
    const-string/jumbo v5, "CHECK_SCHEDULE_ENABLED"

    const-string/jumbo v6, "CHECK_SCHEDULE_ENABLED"

    invoke-virtual {p2, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 83
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
