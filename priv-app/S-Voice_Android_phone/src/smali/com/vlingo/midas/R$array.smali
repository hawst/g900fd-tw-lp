.class public final Lcom/vlingo/midas/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final asrServers:I = 0x7f090004

.field public static final auto_dial_preference:I = 0x7f09000f

.field public static final auto_dial_preference_values:I = 0x7f090010

.field public static final core_russian_weekdays:I = 0x7f090036

.field public static final dayMatchers:I = 0x7f090033

.field public static final debug_mimic_mode:I = 0x7f090011

.field public static final debug_mimic_mode_values:I = 0x7f090012

.field public static final helloServers:I = 0x7f090008

.field public static final languages_iso:I = 0x7f09000d

.field public static final languages_iso_cn:I = 0x7f09000e

.field public static final languages_names:I = 0x7f09000b

.field public static final languages_names_cn:I = 0x7f09000c

.field public static final lmttServers:I = 0x7f090009

.field public static final logServers:I = 0x7f090007

.field public static final monthMatchers:I = 0x7f090003

.field public static final servicesServers:I = 0x7f090006

.field public static final svox_big_numbers:I = 0x7f090002

.field public static final svox_symbols:I = 0x7f090000

.field public static final svox_timezones:I = 0x7f090001

.field public static final training:I = 0x7f090035

.field public static final ttsServers:I = 0x7f090005

.field public static final wake_up_time:I = 0x7f090034

.field public static final wcis_alarm_examples:I = 0x7f090027

.field public static final wcis_alarm_lookup_examples:I = 0x7f090028

.field public static final wcis_check_weather_examples:I = 0x7f09002d

.field public static final wcis_driving_mode_examples:I = 0x7f090020

.field public static final wcis_findcontact_examples:I = 0x7f09002c

.field public static final wcis_get_an_answers_examples:I = 0x7f09002e

.field public static final wcis_local_listings_examples:I = 0x7f090031

.field public static final wcis_local_listings_examples_naver:I = 0x7f090038

.field public static final wcis_memo_examples:I = 0x7f09002a

.field public static final wcis_memo_in_car_examples:I = 0x7f09002b

.field public static final wcis_movies_examples_naver:I = 0x7f090039

.field public static final wcis_music_examples:I = 0x7f09001a

.field public static final wcis_music_examples_no_radio:I = 0x7f09001b

.field public static final wcis_nav_examples:I = 0x7f090023

.field public static final wcis_news_examples:I = 0x7f090021

.field public static final wcis_open_app_examples:I = 0x7f090018

.field public static final wcis_open_app_examples_american:I = 0x7f090019

.field public static final wcis_recordvoice_examples:I = 0x7f090022

.field public static final wcis_schedule_examples:I = 0x7f090024

.field public static final wcis_schedule_in_car_examples:I = 0x7f090025

.field public static final wcis_schedule_lookup_examples:I = 0x7f090026

.field public static final wcis_search_examples:I = 0x7f090016

.field public static final wcis_search_examples_cn:I = 0x7f090017

.field public static final wcis_search_examples_naver_add:I = 0x7f090037

.field public static final wcis_simple_setting_controls_examples:I = 0x7f09002f

.field public static final wcis_simple_setting_controls_examples_for_chinese:I = 0x7f090030

.field public static final wcis_sms_examples:I = 0x7f090015

.field public static final wcis_social_update_examples:I = 0x7f09001e

.field public static final wcis_social_update_examples_cn:I = 0x7f09001f

.field public static final wcis_task_examples:I = 0x7f09001c

.field public static final wcis_task_lookup_examples:I = 0x7f09001d

.field public static final wcis_timer_examples:I = 0x7f090029

.field public static final wcis_voice_dial_car_examples:I = 0x7f090013

.field public static final wcis_voice_dial_car_examples_us:I = 0x7f090014

.field public static final wcis_world_clock_examples:I = 0x7f090032

.field public static final weather_clear:I = 0x7f09005c

.field public static final weather_clear_today:I = 0x7f09005d

.field public static final weather_cold:I = 0x7f090058

.field public static final weather_cold_today:I = 0x7f090059

.field public static final weather_duststorm:I = 0x7f090063

.field public static final weather_duststorm_today:I = 0x7f090064

.field public static final weather_flurries:I = 0x7f09004c

.field public static final weather_flurries_today:I = 0x7f09004d

.field public static final weather_foggy:I = 0x7f090040

.field public static final weather_foggy_today:I = 0x7f090041

.field public static final weather_haze:I = 0x7f090061

.field public static final weather_haze_today:I = 0x7f090062

.field public static final weather_hot:I = 0x7f090056

.field public static final weather_hot_today:I = 0x7f090057

.field public static final weather_ice:I = 0x7f090052

.field public static final weather_ice_today:I = 0x7f090053

.field public static final weather_mixed_rain_and_snow:I = 0x7f090054

.field public static final weather_mixed_rain_and_snow_today:I = 0x7f090055

.field public static final weather_moslty_clear:I = 0x7f09005e

.field public static final weather_moslty_clear_today:I = 0x7f09005f

.field public static final weather_mostly_cloudy:I = 0x7f09003e

.field public static final weather_mostly_cloudy_night:I = 0x7f090060

.field public static final weather_mostly_cloudy_today:I = 0x7f09003f

.field public static final weather_mostly_cloudy_with_flurries:I = 0x7f09004e

.field public static final weather_mostly_cloudy_with_flurries_today:I = 0x7f09004f

.field public static final weather_mostly_cloudy_with_thunder_showers:I = 0x7f090048

.field public static final weather_mostly_cloudy_with_thunder_showers_today:I = 0x7f090049

.field public static final weather_partly_sunny:I = 0x7f09003c

.field public static final weather_partly_sunny_today:I = 0x7f09003d

.field public static final weather_partly_sunny_with_showers:I = 0x7f090044

.field public static final weather_partly_sunny_with_showers_today:I = 0x7f090045

.field public static final weather_phenomenon:I = 0x7f090065

.field public static final weather_rainy:I = 0x7f09004a

.field public static final weather_rainy_today:I = 0x7f09004b

.field public static final weather_showers:I = 0x7f090042

.field public static final weather_showers_today:I = 0x7f090043

.field public static final weather_snow:I = 0x7f090050

.field public static final weather_snow_today:I = 0x7f090051

.field public static final weather_sunny:I = 0x7f09003a

.field public static final weather_sunny_today:I = 0x7f09003b

.field public static final weather_thunderstorms:I = 0x7f090046

.field public static final weather_thunderstorms_today:I = 0x7f090047

.field public static final weather_wind_direction:I = 0x7f090066

.field public static final weather_wind_force:I = 0x7f090067

.field public static final weather_windy:I = 0x7f09005a

.field public static final weather_windy_today:I = 0x7f09005b

.field public static final web_search_engine_preference_values:I = 0x7f09000a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
