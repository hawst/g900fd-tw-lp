.class public final Lcom/vlingo/midas/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final alarm_active_day_color:I = 0x7f0b0009

.field public static final answer_question_text_color:I = 0x7f0b001b

.field public static final answer_question_title_color:I = 0x7f0b001a

.field public static final color_qasubsection:I = 0x7f0b0008

.field public static final contact_border_color:I = 0x7f0b0019

.field public static final contact_img_1_bg_color:I = 0x7f0b001f

.field public static final contact_img_2_bg_color:I = 0x7f0b0020

.field public static final contact_img_3_bg_color:I = 0x7f0b0021

.field public static final contact_img_4_bg_color:I = 0x7f0b0022

.field public static final contact_img_5_bg_color:I = 0x7f0b0023

.field public static final cursor:I = 0x7f0b0011

.field public static final drivingmode_background_color:I = 0x7f0b000b

.field public static final drivingmode_news_cp_color:I = 0x7f0b000d

.field public static final edit_text_color_dark_theme:I = 0x7f0b000c

.field public static final edit_text_color_santos_theme:I = 0x7f0b000e

.field public static final help_about_bg_color:I = 0x7f0b0013

.field public static final inner_bg:I = 0x7f0b0014

.field public static final item_application_list_divider:I = 0x7f0b000a

.field public static final iux_text_color:I = 0x7f0b0024

.field public static final iux_text_color_disclaimer:I = 0x7f0b0025

.field public static final link_text_color_disclaimer:I = 0x7f0b0026

.field public static final path_textview_blue_color_selected:I = 0x7f0b000f

.field public static final path_textview_blue_color_unselected:I = 0x7f0b0010

.field public static final prompt_bubble:I = 0x7f0b0005

.field public static final prompt_bubble_wakeup:I = 0x7f0b0006

.field public static final search_button_color:I = 0x7f0b0004

.field public static final settings_preference_category:I = 0x7f0b001c

.field public static final settings_preference_title:I = 0x7f0b001d

.field public static final slide_text_color:I = 0x7f0b0002

.field public static final solid_black:I = 0x7f0b0001

.field public static final solid_white:I = 0x7f0b0000

.field public static final text_color_white:I = 0x7f0b0012

.field public static final training_1:I = 0x7f0b0018

.field public static final training_2:I = 0x7f0b0017

.field public static final training_3:I = 0x7f0b0016

.field public static final training_4:I = 0x7f0b0015

.field public static final user_bubble:I = 0x7f0b0007

.field public static final welcome_page_bottom_layout_clr:I = 0x7f0b001e

.field public static final widget_button_color:I = 0x7f0b0003


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
