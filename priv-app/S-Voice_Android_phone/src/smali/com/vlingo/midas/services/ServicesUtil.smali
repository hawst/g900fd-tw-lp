.class public abstract Lcom/vlingo/midas/services/ServicesUtil;
.super Ljava/lang/Object;
.source "ServicesUtil.java"


# static fields
.field public static final START_SERVICES:Ljava/lang/String; = "com.vlingo.midas.services.START_ALL"


# instance fields
.field private mAudioPlaybackInitialized:Z

.field private mSafeReaderInitialized:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-boolean v0, p0, Lcom/vlingo/midas/services/ServicesUtil;->mSafeReaderInitialized:Z

    .line 19
    iput-boolean v0, p0, Lcom/vlingo/midas/services/ServicesUtil;->mAudioPlaybackInitialized:Z

    .line 24
    return-void
.end method


# virtual methods
.method protected abstract getContext()Landroid/content/Context;
.end method

.method public startAudioPlaybackService()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 27
    iget-boolean v0, p0, Lcom/vlingo/midas/services/ServicesUtil;->mAudioPlaybackInitialized:Z

    if-nez v0, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/vlingo/midas/services/ServicesUtil;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->init(Landroid/content/Context;Z)V

    .line 29
    iput-boolean v1, p0, Lcom/vlingo/midas/services/ServicesUtil;->mAudioPlaybackInitialized:Z

    .line 31
    :cond_0
    return v1
.end method

.method public startLmttVoconService(Z)Z
    .locals 3
    .param p1, "sendUpdate"    # Z

    .prologue
    const/4 v0, 0x0

    .line 59
    sget-object v1, Lcom/vlingo/sdk/recognition/RecognitionMode;->CLOUD:Lcom/vlingo/sdk/recognition/RecognitionMode;

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSDKRecoMode()Lcom/vlingo/sdk/recognition/RecognitionMode;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/sdk/recognition/RecognitionMode;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    if-eqz p1, :cond_1

    .line 64
    invoke-static {v0}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;->sendUpdate(Z)Z

    move-result v0

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 66
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/lmttvocon/VoconLmttServiceHelper;->notifyApplicationActivity()Z

    move-result v0

    goto :goto_0
.end method

.method public startLocalServices()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/services/ServicesUtil;->startRequiredServices(Z)Z

    move-result v0

    return v0
.end method

.method public startRequiredServices(Z)Z
    .locals 1
    .param p1, "toReturn"    # Z

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/vlingo/midas/services/ServicesUtil;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/vlingo/midas/services/ServicesUtil;->startAudioPlaybackService()Z

    move-result v0

    and-int/2addr p1, v0

    .line 90
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/services/ServicesUtil;->startSafeReaderService(Z)Z

    move-result v0

    and-int/2addr p1, v0

    .line 92
    :cond_0
    return p1
.end method

.method public startSafeReaderService(Z)Z
    .locals 2
    .param p1, "playAnnouncement"    # Z

    .prologue
    const/4 v1, 0x1

    .line 37
    iget-boolean v0, p0, Lcom/vlingo/midas/services/ServicesUtil;->mSafeReaderInitialized:Z

    if-nez v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/services/ServicesUtil;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->init(Landroid/content/Context;)V

    .line 39
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->safeReadingInit(Lcom/vlingo/core/internal/safereader/ISafeReaderServiceEngine;)V

    .line 40
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 41
    iput-boolean v1, p0, Lcom/vlingo/midas/services/ServicesUtil;->mSafeReaderInitialized:Z

    .line 43
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->startSafeReading()V

    .line 49
    :cond_1
    return v1
.end method

.method public stopSafereaderService(Z)V
    .locals 1
    .param p1, "playAnnouncement"    # Z

    .prologue
    .line 53
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->stopSafeReading()V

    .line 54
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->deinit()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/services/ServicesUtil;->mSafeReaderInitialized:Z

    .line 56
    return-void
.end method

.method protected supportsSVoiceAssociatedServiceOnly()Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return v0
.end method
