.class public Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;
.super Ljava/lang/Object;
.source "JProjectBackgroundHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
.implements Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;


# static fields
.field private static final FOCUS_DEFAULT_BACKGROUND:I = 0x0

.field private static final FOCUS_FOREGROUND:I = 0x1

.field private static isInProcess:Z

.field private static mFocus:I

.field private static mInstance:Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;


# instance fields
.field private alerts:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field private isSilentMode:Z

.field private listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    sput-boolean v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isInProcess:Z

    .line 41
    sput v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->mFocus:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isSilentMode:Z

    .line 58
    iput-object p0, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    invoke-static {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->registerSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 60
    return-void
.end method

.method private generateVerboseMultiSenderSpoken(Ljava/util/LinkedList;I)Ljava/lang/String;
    .locals 9
    .param p2, "totalMessages"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "senders":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    const-string/jumbo v3, ""

    .line 200
    .local v3, "names":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v1

    .line 203
    .local v1, "max":I
    const/4 v0, 0x0

    .line 204
    .local v0, "index":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 205
    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 206
    if-eqz v0, :cond_0

    .line 207
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 208
    add-int/lit8 v4, v1, -0x1

    if-ne v0, v4, :cond_0

    .line 209
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 211
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_and:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 214
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 215
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 217
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 221
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 223
    .local v2, "multiSenderSpokenText":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 224
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 226
    return-object v2
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;
    .locals 2

    .prologue
    .line 50
    const-class v1, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->mInstance:Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;

    invoke-direct {v0}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;-><init>()V

    sput-object v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->mInstance:Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;

    .line 52
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->mFocus:I

    .line 54
    :cond_0
    sget-object v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->mInstance:Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isDrivingModeOffered()Z
    .locals 1

    .prologue
    .line 316
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v0

    return v0
.end method

.method private isKeygaurdSecureActive()Z
    .locals 3

    .prologue
    .line 311
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "keyguard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 312
    .local v0, "keyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private readAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V
    .locals 9
    .param p1, "alert"    # Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 245
    if-eqz p1, :cond_3

    .line 246
    sput-boolean v8, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isInProcess:Z

    .line 250
    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "MMS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "SMS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move-object v0, p1

    .line 251
    check-cast v0, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 252
    .local v0, "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getSenderName()Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "messageSender":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getMessageText()Ljava/lang/String;

    move-result-object v2

    .line 255
    .local v2, "msgTxt":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->formatPhoneNumberForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 258
    invoke-direct {p0}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isKeygaurdSecureActive()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isDrivingModeOffered()Z

    move-result v4

    if-nez v4, :cond_2

    .line 259
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v7

    aput-object v2, v6, v8

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 263
    .local v3, "prompt":Ljava/lang/String;
    :goto_0
    invoke-static {v3}, Lcom/vlingo/core/internal/audio/TTSRequest;->getMessageReadback(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;

    move-result-object v4

    invoke-static {v4, p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->play(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 271
    .end local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v1    # "messageSender":Ljava/lang/String;
    .end local v2    # "msgTxt":Ljava/lang/String;
    .end local v3    # "prompt":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 261
    .restart local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .restart local v1    # "messageSender":Ljava/lang/String;
    .restart local v2    # "msgTxt":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v1, v6, v7

    invoke-interface {v4, v5, v6}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "prompt":Ljava/lang/String;
    goto :goto_0

    .line 269
    .end local v0    # "message":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v1    # "messageSender":Ljava/lang/String;
    .end local v2    # "msgTxt":Ljava/lang/String;
    .end local v3    # "prompt":Ljava/lang/String;
    :cond_3
    sput-boolean v7, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isInProcess:Z

    goto :goto_1
.end method

.method private readAlerts()V
    .locals 3

    .prologue
    .line 232
    iget-object v1, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    if-eqz v1, :cond_0

    .line 233
    const/4 v1, 0x1

    sput-boolean v1, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isInProcess:Z

    .line 234
    iget-object v1, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->generateVerboseMultiSenderSpoken(Ljava/util/LinkedList;I)Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "prompt":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 236
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/TTSRequest;->getMessageReadback(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->play(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 240
    .end local v0    # "prompt":Ljava/lang/String;
    :goto_0
    return-void

    .line 238
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isInProcess:Z

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 69
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    sput v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->mFocus:I

    .line 70
    invoke-static {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->unregisterSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 71
    invoke-static {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->registerSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 72
    iput-object p1, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    monitor-exit p0

    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public handleAlert(Ljava/util/LinkedList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    const/4 v1, 0x0

    .line 113
    .local v1, "focusIsForeground":Z
    monitor-enter p0

    .line 114
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isFocusForeground()Z

    move-result v1

    .line 115
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    if-nez v1, :cond_6

    invoke-virtual {p0}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isSilentMode()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 120
    if-eqz p1, :cond_3

    .line 124
    iget-object v4, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    if-nez v4, :cond_0

    .line 128
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    iput-object v4, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    .line 131
    :cond_0
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    .line 132
    .local v0, "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    iget-object v4, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 135
    iget-object v4, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    .end local v0    # "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 139
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/PhoneUtil;->phoneInUse(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-boolean v4, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isInProcess:Z

    if-nez v4, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 143
    invoke-direct {p0}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isKeygaurdSecureActive()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-direct {p0}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isDrivingModeOffered()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v4

    if-nez v4, :cond_4

    .line 144
    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    .local v3, "launchIntent":Landroid/content/Intent;
    const-string/jumbo v4, "ACTION_SAFEREADER_LAUNCH"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 147
    const-string/jumbo v4, "EXTRA_MESSAGE_LIST"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 148
    const-string/jumbo v4, "EXTRA_TIME_SENT"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 149
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 150
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    .line 174
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "launchIntent":Landroid/content/Intent;
    :cond_3
    :goto_1
    return-void

    .line 152
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_4
    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_5

    .line 153
    invoke-direct {p0}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->readAlerts()V

    goto :goto_1

    .line 155
    :cond_5
    iget-object v4, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-direct {p0, v4}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->readAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V

    goto :goto_1

    .line 163
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_6
    if-eqz v1, :cond_3

    goto :goto_1
.end method

.method public handleAlert(Ljava/util/LinkedList;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V
    .locals 0
    .param p2, "fieldId"    # Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<+",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;",
            ")V"
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "alertList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<+Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->handleAlert(Ljava/util/LinkedList;)V

    .line 180
    return-void
.end method

.method public isFocusForeground()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 84
    sget v1, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->mFocus:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSilentMode()Z
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    return v0
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 306
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isInProcess:Z

    .line 307
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 308
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->alerts:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->readAlert(Lcom/vlingo/core/internal/safereader/SafeReaderAlert;)V

    .line 290
    :goto_0
    return-void

    .line 287
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isInProcess:Z

    .line 288
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    goto :goto_0
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 297
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->isInProcess:Z

    .line 298
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 299
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 277
    return-void
.end method

.method public readoutDelay()J
    .locals 3

    .prologue
    .line 184
    const-string/jumbo v0, "safereader.delay"

    const-wide/16 v1, 0x1b58

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    invoke-static {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->unregisterSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 64
    iput-object v1, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .line 65
    sput-object v1, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->mInstance:Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;

    .line 66
    return-void
.end method

.method public declared-synchronized releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 76
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->mFocus:I

    .line 77
    invoke-static {p1}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->unregisterSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 78
    invoke-static {p0}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->registerSafeReaderListener(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 79
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereaderimpl/SafeReaderEngine;->stopSafeReading()V

    .line 80
    iput-object p0, p0, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->listener:Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSilentMode(Z)V
    .locals 0
    .param p1, "isSilentMode"    # Z

    .prologue
    .line 194
    return-void
.end method
