.class public Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;
.super Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;
.source "MidasNotificationPopUpContainer.java"


# instance fields
.field private currentStatus:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;


# direct methods
.method public constructor <init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V
    .locals 1
    .param p1, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    .line 25
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->NONE_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    iput-object v0, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->currentStatus:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    .line 21
    return-void
.end method


# virtual methods
.method public getNextNotification()Lcom/vlingo/core/internal/notification/NotificationPopUp;
    .locals 9

    .prologue
    .line 28
    const/4 v0, 0x0

    .line 35
    .local v0, "notification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDisclaimerAccepted()Z

    move-result v7

    if-nez v7, :cond_4

    .line 37
    sget-object v7, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->DISCLAIMER_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    iput-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->currentStatus:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    .line 39
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/LocaleUtils;->getBRLocale()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "pt_BR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->tos_vlingo_terms_samsung_for_pt_br:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 42
    .local v2, "title":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/LocaleUtils;->getBRLocale()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "pt_BR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    sget v6, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_samsung_for_pt_br:I

    .line 43
    .local v6, "resID":I
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 44
    .local v3, "notificationText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/LocaleUtils;->getBRLocale()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "pt_BR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->tos_accept_samsung_for_pt_br:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 47
    .local v5, "positiveButton":Ljava/lang/String;
    :goto_2
    new-instance v0, Lcom/vlingo/midas/notification/DisclaimerNotification;

    .end local v0    # "notification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    invoke-direct {v0, v2, v3, v5}, Lcom/vlingo/midas/notification/DisclaimerNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "notificationText":Ljava/lang/String;
    .end local v5    # "positiveButton":Ljava/lang/String;
    .end local v6    # "resID":I
    .restart local v0    # "notification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    :cond_0
    :goto_3
    return-object v0

    .line 39
    :cond_1
    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->tos_vlingo_terms_samsung:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 42
    .restart local v2    # "title":Ljava/lang/String;
    :cond_2
    sget v6, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_samsung:I

    goto :goto_1

    .line 44
    .restart local v3    # "notificationText":Ljava/lang/String;
    .restart local v6    # "resID":I
    :cond_3
    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->tos_accept_samsung:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 48
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "notificationText":Ljava/lang/String;
    .end local v6    # "resID":I
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v7

    if-nez v7, :cond_c

    .line 49
    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->incomingTOSNotification:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    if-nez v7, :cond_b

    .line 51
    sget-object v7, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->TOS_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    iput-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->currentStatus:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    .line 53
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/LocaleUtils;->getBRLocale()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "pt_BR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->tos_vlingo_terms_for_pt_br:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 58
    .restart local v2    # "title":Ljava/lang/String;
    :goto_4
    invoke-static {}, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->isLaunchedForTosAcceptOtherApp()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 59
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/LocaleUtils;->getBRLocale()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "pt_BR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    sget v6, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_cc_for_pt_br:I

    .line 63
    .restart local v6    # "resID":I
    :goto_5
    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    invoke-virtual {v7, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 66
    .restart local v3    # "notificationText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/LocaleUtils;->getBRLocale()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "pt_BR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->tos_accept_for_pt_br:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 69
    .restart local v5    # "positiveButton":Ljava/lang/String;
    :goto_6
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/LocaleUtils;->getBRLocale()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "pt_BR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->tos_decline_for_pt_br:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 72
    .local v4, "negativeButton":Ljava/lang/String;
    :goto_7
    const/4 v1, 0x1

    .line 73
    .local v1, "version":I
    new-instance v0, Lcom/vlingo/midas/notification/TermsOfServiceNotification;

    .end local v0    # "notification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    invoke-direct/range {v0 .. v5}, Lcom/vlingo/midas/notification/TermsOfServiceNotification;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .restart local v0    # "notification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    goto/16 :goto_3

    .line 53
    .end local v1    # "version":I
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "notificationText":Ljava/lang/String;
    .end local v4    # "negativeButton":Ljava/lang/String;
    .end local v5    # "positiveButton":Ljava/lang/String;
    .end local v6    # "resID":I
    :cond_5
    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->tos_vlingo_terms:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 59
    .restart local v2    # "title":Ljava/lang/String;
    :cond_6
    sget v6, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_cc:I

    goto :goto_5

    .line 61
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/LocaleUtils;->getBRLocale()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "pt_BR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    sget v6, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_old_for_pt_br:I

    .restart local v6    # "resID":I
    :goto_8
    goto :goto_5

    .end local v6    # "resID":I
    :cond_8
    sget v6, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_old:I

    goto :goto_8

    .line 66
    .restart local v3    # "notificationText":Ljava/lang/String;
    .restart local v6    # "resID":I
    :cond_9
    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->tos_accept:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    .line 69
    .restart local v5    # "positiveButton":Ljava/lang/String;
    :cond_a
    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->context:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->tos_decline:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_7

    .line 75
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "notificationText":Ljava/lang/String;
    .end local v5    # "positiveButton":Ljava/lang/String;
    .end local v6    # "resID":I
    :cond_b
    sget-object v7, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->INCOMING_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    iput-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->currentStatus:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    .line 76
    iget-object v0, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->incomingTOSNotification:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    goto/16 :goto_3

    .line 78
    :cond_c
    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->incomingNotifications:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->incomingNotifications:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 80
    sget-object v7, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->INCOMING_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    iput-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->currentStatus:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    .line 82
    iget-object v7, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->incomingNotifications:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "notification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    check-cast v0, Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .restart local v0    # "notification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    goto/16 :goto_3
.end method

.method public needNextNotification()Z
    .locals 2

    .prologue
    .line 88
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->NONE_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    .line 89
    .local v0, "s":Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDisclaimerAccepted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->DISCLAIMER_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    .line 101
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->currentStatus:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    if-eq v1, v0, :cond_4

    .line 102
    const/4 v1, 0x1

    .line 103
    :goto_1
    return v1

    .line 91
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 92
    iget-object v1, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->incomingTOSNotification:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    if-nez v1, :cond_2

    .line 93
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->TOS_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    goto :goto_0

    .line 95
    :cond_2
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->INCOMING_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    goto :goto_0

    .line 97
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->incomingNotifications:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->incomingNotifications:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 98
    sget-object v0, Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;->INCOMING_S:Lcom/vlingo/core/internal/notification/NotificationPopUpContainer$Status;

    goto :goto_0

    .line 103
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public removeNotificationFromContainer(Lcom/vlingo/core/internal/notification/NotificationPopUp;)V
    .locals 2
    .param p1, "notification"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .prologue
    .line 107
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getType()Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->ALERT:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->incomingNotifications:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 110
    :cond_0
    return-void
.end method
