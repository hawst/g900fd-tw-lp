.class public Lcom/vlingo/midas/notification/NotificationPopUpManager;
.super Ljava/lang/Object;
.source "NotificationPopUpManager.java"

# interfaces
.implements Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/notification/NotificationPopUpManager$KeyListener;,
        Lcom/vlingo/midas/notification/NotificationPopUpManager$CancelListener;,
        Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;,
        Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;
    }
.end annotation


# static fields
.field public static final ACCEPTED:I = 0x1

.field public static final CANCELED:I = 0x3

.field public static final DECLINED:I = 0x2

.field private static container:Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;


# instance fields
.field private activityHandler:Landroid/os/Handler;

.field private context:Landroid/app/Activity;

.field private currentDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V
    .locals 1
    .param p1, "ctx"    # Landroid/app/Activity;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    .line 35
    iput-object p2, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->activityHandler:Landroid/os/Handler;

    .line 36
    iput-object p1, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    .line 37
    new-instance v0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;

    invoke-direct {v0, p3}, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    sput-object v0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->container:Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/notification/NotificationPopUpManager;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/notification/NotificationPopUpManager;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100()Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->container:Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;

    return-object v0
.end method

.method private buildAlertNotificationDialog(Lcom/vlingo/core/internal/notification/NotificationPopUp;)Landroid/app/AlertDialog;
    .locals 4
    .param p1, "ntfc"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .prologue
    .line 154
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 155
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 156
    new-instance v1, Lcom/vlingo/midas/notification/NotificationDialogView;

    iget-object v2, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getNotificationText()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/vlingo/midas/notification/NotificationDialogView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 157
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getPositiveButton()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 158
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getPositiveButton()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;

    invoke-direct {v2, p0, p1}, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;-><init>(Lcom/vlingo/midas/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 160
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getNegativeButton()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 161
    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->getNegativeButton()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;

    invoke-direct {v2, p0, p1}, Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;-><init>(Lcom/vlingo/midas/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 163
    :cond_1
    new-instance v1, Lcom/vlingo/midas/notification/NotificationPopUpManager$CancelListener;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/midas/notification/NotificationPopUpManager$CancelListener;-><init>(Lcom/vlingo/midas/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 164
    new-instance v1, Lcom/vlingo/midas/notification/NotificationPopUpManager$KeyListener;

    invoke-direct {v1}, Lcom/vlingo/midas/notification/NotificationPopUpManager$KeyListener;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 165
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method private setShowingNotifications(Z)Z
    .locals 1
    .param p1, "showingNotifications"    # Z

    .prologue
    .line 66
    const-string/jumbo v0, "showing_notifications"

    invoke-static {v0, p1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 67
    return p1
.end method


# virtual methods
.method public dismissNotifications()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    .line 149
    :cond_0
    return-void
.end method

.method public existDialog()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    :cond_0
    const/4 v0, 0x0

    .line 73
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasNotifications(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)Z
    .locals 1
    .param p1, "factory"    # Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;

    .prologue
    .line 41
    new-instance v0, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    sput-object v0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->container:Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;

    .line 42
    invoke-virtual {p0}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->needNotifications()Z

    move-result v0

    return v0
.end method

.method public needNotifications()Z
    .locals 2

    .prologue
    .line 49
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDisclaimerAccepted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAllNotificationsAccepted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->isNeedToRemindOfPriorAcceptanceMaster()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 57
    .local v0, "showingNotifications":Z
    :goto_0
    invoke-direct {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    .line 58
    return v0

    .line 49
    .end local v0    # "showingNotifications":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendResult(I)V
    .locals 1
    .param p1, "result"    # I

    .prologue
    .line 140
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    .line 141
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->activityHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 142
    return-void
.end method

.method public setContext(Landroid/app/Activity;)V
    .locals 0
    .param p1, "ctx"    # Landroid/app/Activity;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    .line 47
    return-void
.end method

.method public showNextNotification()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 84
    iget-object v2, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 87
    invoke-direct {p0, v4}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    .line 121
    :goto_0
    return-void

    .line 91
    :cond_0
    sget-object v2, Lcom/vlingo/midas/notification/NotificationPopUpManager;->container:Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;

    invoke-virtual {v2}, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->needNextNotification()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->existDialog()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 94
    invoke-direct {p0, v3}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    goto :goto_0

    .line 98
    :cond_1
    sget-object v2, Lcom/vlingo/midas/notification/NotificationPopUpManager;->container:Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;

    invoke-virtual {v2}, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->getNextNotification()Lcom/vlingo/core/internal/notification/NotificationPopUp;

    move-result-object v1

    .line 101
    .local v1, "nextNotification":Lcom/vlingo/core/internal/notification/NotificationPopUp;
    if-eqz v1, :cond_2

    .line 102
    invoke-direct {p0, v3}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    .line 103
    invoke-direct {p0, v1}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->buildAlertNotificationDialog(Lcom/vlingo/core/internal/notification/NotificationPopUp;)Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    .line 105
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    .line 111
    invoke-direct {p0, v4}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    goto :goto_0

    .line 119
    .end local v0    # "e":Landroid/view/WindowManager$BadTokenException;
    :cond_2
    invoke-virtual {p0, v3}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->sendResult(I)V

    goto :goto_0
.end method

.method public showNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp;Landroid/app/Activity;)V
    .locals 2
    .param p1, "notification"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 124
    iput-object p2, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->context:Landroid/app/Activity;

    .line 125
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    .line 126
    invoke-direct {p0, p1}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->buildAlertNotificationDialog(Lcom/vlingo/core/internal/notification/NotificationPopUp;)Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    .line 128
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    return-void

    .line 129
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;

    .line 134
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->setShowingNotifications(Z)Z

    goto :goto_0
.end method

.method public showingNotifications()Z
    .locals 2

    .prologue
    .line 62
    const-string/jumbo v0, "showing_notifications"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
