.class public final enum Lcom/vlingo/midas/notification/NotificationPopUp$Type;
.super Ljava/lang/Enum;
.source "NotificationPopUp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/notification/NotificationPopUp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/notification/NotificationPopUp$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/notification/NotificationPopUp$Type;

.field public static final enum ALERT:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

.field public static final enum DISCLAIMER:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

.field public static final enum INFORMATION:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

.field public static final enum TOS:Lcom/vlingo/midas/notification/NotificationPopUp$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    const-string/jumbo v1, "INFORMATION"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/notification/NotificationPopUp$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->INFORMATION:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    new-instance v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    const-string/jumbo v1, "DISCLAIMER"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/notification/NotificationPopUp$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->DISCLAIMER:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    new-instance v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    const-string/jumbo v1, "TOS"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/notification/NotificationPopUp$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->TOS:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    new-instance v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    const-string/jumbo v1, "ALERT"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/notification/NotificationPopUp$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->ALERT:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    .line 92
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    sget-object v1, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->INFORMATION:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->DISCLAIMER:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->TOS:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->ALERT:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->$VALUES:[Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/notification/NotificationPopUp$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 92
    const-class v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/notification/NotificationPopUp$Type;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->$VALUES:[Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    invoke-virtual {v0}, [Lcom/vlingo/midas/notification/NotificationPopUp$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    return-object v0
.end method
