.class public Lcom/vlingo/midas/notification/AlertNotification;
.super Lcom/vlingo/core/internal/notification/NotificationPopUp;
.source "AlertNotification.java"


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 9
    .param p1, "version"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "notificationText"    # Ljava/lang/String;
    .param p4, "negativeButton"    # Ljava/lang/String;
    .param p5, "positiveButton"    # Ljava/lang/String;
    .param p6, "exitOnDecline"    # Z
    .param p7, "actionRequired"    # Z

    .prologue
    .line 11
    sget-object v1, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->ALERT:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/vlingo/core/internal/notification/NotificationPopUp;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 13
    return-void
.end method


# virtual methods
.method public accept()V
    .locals 4

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/vlingo/midas/notification/AlertNotification;->getVersion()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 18
    .local v1, "version":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->isNotificationsAccepted(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 21
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setNotificationAccepted(Ljava/lang/String;Z)V

    .line 22
    const-string/jumbo v2, "notification_counter_local"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 23
    .local v0, "counter":I
    const-string/jumbo v2, "notification_counter_local"

    add-int/lit8 v0, v0, 0x1

    invoke-static {v2, v0}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 25
    .end local v0    # "counter":I
    :cond_0
    return-void
.end method

.method public decline()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public isAccepted()Z
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/vlingo/midas/notification/AlertNotification;->getVersion()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isNotificationsAccepted(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
