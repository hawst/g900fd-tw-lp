.class Lcom/vlingo/midas/notification/NotificationPopUpParser;
.super Lcom/vlingo/core/internal/xml/SimpleXmlParser;
.source "NotificationPopUpParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/notification/NotificationPopUpParser$1;
    }
.end annotation


# instance fields
.field private final ATTR_ACTION_REQUIRED:I

.field private final ATTR_EXIT_ON_DECLINE:I

.field private final ATTR_LNKRES:I

.field private final ATTR_NAME:I

.field private final ATTR_STRRES:I

.field private final ATTR_VAL:I

.field private final ELEM_ALERT_DIALOG:I

.field private final ELEM_LINK:I

.field private final ELEM_LNKRES:I

.field private final ELEM_LOCALE:I

.field private final ELEM_LOCALIZED:I

.field private final ELEM_NEGATIVE_BUTTON:I

.field private final ELEM_NOTIFICATION_TEXT:I

.field private final ELEM_POSITIVE_BUTTON:I

.field private final ELEM_STRRES:I

.field private final ELEM_TITLE:I

.field private final ELEM_TYPE:I

.field private final ELEM_VERSION:I

.field private actionRequired:Ljava/lang/Boolean;

.field private exitOnDecline:Ljava/lang/Boolean;

.field private final hrefBase:Ljava/lang/String;

.field private i:I

.field private links:[Ljava/lang/String;

.field private lnkResources:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private localizedResources:Z

.field private name:Ljava/lang/String;

.field private negativeButton:Ljava/lang/String;

.field private notificationText:Ljava/lang/String;

.field private final notificationType:Ljava/lang/String;

.field private notifications:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/notification/NotificationPopUp;",
            ">;"
        }
    .end annotation
.end field

.field private positiveButton:Ljava/lang/String;

.field private size:I

.field private strResources:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private title:Ljava/lang/String;

.field private final tosType:Ljava/lang/String;

.field private type:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

.field private version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vlingo/core/internal/xml/SimpleXmlParser;-><init>()V

    .line 37
    const-string/jumbo v0, "TOS"

    iput-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->tosType:Ljava/lang/String;

    .line 38
    const-string/jumbo v0, "Notification"

    iput-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notificationType:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->localizedResources:Z

    .line 50
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->lnkResources:Ljava/util/HashMap;

    .line 55
    const-string/jumbo v0, "<a href=\"%s\">%s</a>"

    iput-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->hrefBase:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    .line 61
    const-string/jumbo v0, "AlertDialog"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_ALERT_DIALOG:I

    .line 62
    const-string/jumbo v0, "type"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_TYPE:I

    .line 63
    const-string/jumbo v0, "version"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_VERSION:I

    .line 64
    const-string/jumbo v0, "Localized"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_LOCALIZED:I

    .line 65
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_LOCALE:I

    .line 66
    const-string/jumbo v0, "StrRes"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_STRRES:I

    .line 67
    const-string/jumbo v0, "LnkRes"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_LNKRES:I

    .line 68
    const-string/jumbo v0, "title"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_TITLE:I

    .line 69
    const-string/jumbo v0, "notificationText"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_NOTIFICATION_TEXT:I

    .line 70
    const-string/jumbo v0, "positiveButtonText"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_POSITIVE_BUTTON:I

    .line 71
    const-string/jumbo v0, "negativeButtonText"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_NEGATIVE_BUTTON:I

    .line 72
    const-string/jumbo v0, "link"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerElement(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_LINK:I

    .line 74
    const-string/jumbo v0, "exitOnDecline"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_EXIT_ON_DECLINE:I

    .line 75
    const-string/jumbo v0, "actionRequired"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_ACTION_REQUIRED:I

    .line 76
    const-string/jumbo v0, "name"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_NAME:I

    .line 77
    const-string/jumbo v0, "value"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_VAL:I

    .line 78
    const-string/jumbo v0, "strRes"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_STRRES:I

    .line 79
    const-string/jumbo v0, "lnkRes"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/notification/NotificationPopUpParser;->registerAttribute(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_LNKRES:I

    .line 80
    return-void
.end method


# virtual methods
.method public beginElement(ILcom/vlingo/core/internal/xml/XmlAttributes;[CI)V
    .locals 11
    .param p1, "elementType"    # I
    .param p2, "attributes"    # Lcom/vlingo/core/internal/xml/XmlAttributes;
    .param p3, "cData"    # [C
    .param p4, "elementEndPosition"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v7, 0x0

    .line 91
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_ALERT_DIALOG:I

    if-ne v6, p1, :cond_1

    .line 94
    iput-object v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->type:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    .line 95
    iput-object v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->version:Ljava/lang/Integer;

    .line 96
    iput-object v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->title:Ljava/lang/String;

    .line 97
    iput-object v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    .line 98
    iput-object v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->positiveButton:Ljava/lang/String;

    .line 99
    iput-object v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->negativeButton:Ljava/lang/String;

    .line 100
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_EXIT_ON_DECLINE:I

    invoke-virtual {p2, v6}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "true"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->exitOnDecline:Ljava/lang/Boolean;

    .line 101
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_ACTION_REQUIRED:I

    invoke-virtual {p2, v6}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "true"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->actionRequired:Ljava/lang/Boolean;

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_TYPE:I

    if-ne v6, p1, :cond_3

    .line 107
    invoke-static {p3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "s":Ljava/lang/String;
    const-string/jumbo v6, "TOS"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 109
    sget-object v6, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->TOS:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->type:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    goto :goto_0

    .line 110
    :cond_2
    const-string/jumbo v6, "Notification"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 111
    sget-object v6, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->ALERT:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->type:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    goto :goto_0

    .line 115
    .end local v4    # "s":Ljava/lang/String;
    :cond_3
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_VERSION:I

    if-ne v6, p1, :cond_4

    .line 118
    invoke-static {p3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->version:Ljava/lang/Integer;

    goto :goto_0

    .line 121
    :cond_4
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_LOCALIZED:I

    if-ne v6, p1, :cond_5

    .line 122
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    goto :goto_0

    .line 123
    :cond_5
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_LOCALE:I

    if-ne v6, p1, :cond_6

    .line 124
    iput-boolean v10, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->localizedResources:Z

    goto :goto_0

    .line 127
    :cond_6
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_STRRES:I

    if-ne v6, p1, :cond_7

    iget-boolean v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->localizedResources:Z

    if-eqz v6, :cond_7

    .line 130
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_NAME:I

    invoke-virtual {p2, v6}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v3

    .line 131
    .local v3, "name":Ljava/lang/String;
    invoke-static {p3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v5

    .line 132
    .local v5, "value":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    invoke-virtual {v6, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 135
    .end local v3    # "name":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/String;
    :cond_7
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_LNKRES:I

    if-ne v6, p1, :cond_8

    iget-boolean v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->localizedResources:Z

    if-eqz v6, :cond_8

    .line 138
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_NAME:I

    invoke-virtual {p2, v6}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->name:Ljava/lang/String;

    .line 139
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_VAL:I

    invoke-virtual {p2, v6}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->size:I

    .line 140
    iput v9, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->i:I

    .line 141
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->size:I

    new-array v6, v6, [Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->links:[Ljava/lang/String;

    goto/16 :goto_0

    .line 144
    :cond_8
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_LINK:I

    if-ne v6, p1, :cond_9

    iget-boolean v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->localizedResources:Z

    if-eqz v6, :cond_9

    .line 147
    new-instance v0, Ljava/util/Formatter;

    invoke-direct {v0}, Ljava/util/Formatter;-><init>()V

    .line 148
    .local v0, "formatter":Ljava/util/Formatter;
    const-string/jumbo v6, "<a href=\"%s\">%s</a>"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget v8, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_VAL:I

    invoke-virtual {p2, v8}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    iget v8, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_NAME:I

    invoke-virtual {p2, v8}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v0, v6, v7}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "href":Ljava/lang/String;
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->i:I

    iget v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->size:I

    if-ge v6, v7, :cond_0

    .line 153
    iget-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->links:[Ljava/lang/String;

    iget v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->i:I

    aput-object v1, v6, v7

    .line 154
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->i:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->i:I

    goto/16 :goto_0

    .line 156
    .end local v0    # "formatter":Ljava/util/Formatter;
    .end local v1    # "href":Ljava/lang/String;
    :cond_9
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_TITLE:I

    if-ne v6, p1, :cond_a

    .line 159
    iget-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    iget v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_STRRES:I

    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 162
    :cond_a
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_NOTIFICATION_TEXT:I

    if-ne v6, p1, :cond_b

    .line 165
    iget-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    iget v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_STRRES:I

    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    .line 166
    iget-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->lnkResources:Ljava/util/HashMap;

    iget v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_LNKRES:I

    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 167
    .local v2, "linkRes":[Ljava/lang/String;
    if-eqz v2, :cond_0

    array-length v6, v2

    if-lez v6, :cond_0

    .line 168
    new-instance v0, Ljava/util/Formatter;

    invoke-direct {v0}, Ljava/util/Formatter;-><init>()V

    .line 169
    .restart local v0    # "formatter":Ljava/util/Formatter;
    iget-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    check-cast v2, [Ljava/lang/Object;

    .end local v2    # "linkRes":[Ljava/lang/String;
    invoke-virtual {v0, v6, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    goto/16 :goto_0

    .line 173
    .end local v0    # "formatter":Ljava/util/Formatter;
    :cond_b
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_POSITIVE_BUTTON:I

    if-ne v6, p1, :cond_c

    .line 176
    iget-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    iget v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_STRRES:I

    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->positiveButton:Ljava/lang/String;

    goto/16 :goto_0

    .line 179
    :cond_c
    iget v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_NEGATIVE_BUTTON:I

    if-ne v6, p1, :cond_0

    .line 182
    iget-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->strResources:Ljava/util/HashMap;

    iget v7, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ATTR_STRRES:I

    invoke-virtual {p2, v7}, Lcom/vlingo/core/internal/xml/XmlAttributes;->lookup(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->negativeButton:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public endDocument()V
    .locals 2

    .prologue
    .line 213
    invoke-super {p0}, Lcom/vlingo/core/internal/xml/SimpleXmlParser;->endDocument()V

    .line 216
    iget-object v1, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/notification/NotificationPopUp;

    goto :goto_0

    .line 220
    :cond_0
    return-void
.end method

.method public endElement(II)V
    .locals 3
    .param p1, "elementType"    # I
    .param p2, "elementStartPosition"    # I

    .prologue
    .line 190
    iget v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_ALERT_DIALOG:I

    if-ne v0, p1, :cond_1

    .line 191
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notificationText:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    sget-object v0, Lcom/vlingo/midas/notification/NotificationPopUpParser$1;->$SwitchMap$com$vlingo$midas$notification$NotificationPopUp$Type:[I

    iget-object v1, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->type:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    invoke-virtual {v1}, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 209
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 204
    :cond_1
    iget v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_LOCALE:I

    if-ne v0, p1, :cond_2

    .line 205
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->localizedResources:Z

    goto :goto_0

    .line 206
    :cond_2
    iget v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->ELEM_LNKRES:I

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->localizedResources:Z

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->lnkResources:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->links:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 192
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getFirstNotification()Lcom/vlingo/midas/notification/NotificationPopUp;
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/notification/NotificationPopUp;

    .line 229
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNotifications()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/notification/NotificationPopUp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpParser;->notifications:Ljava/util/ArrayList;

    return-object v0
.end method

.method public onParseBegin([C)V
    .locals 0
    .param p1, "xml"    # [C

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/xml/SimpleXmlParser;->onParseBegin([C)V

    .line 87
    return-void
.end method
