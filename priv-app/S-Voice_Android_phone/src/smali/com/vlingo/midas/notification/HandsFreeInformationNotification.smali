.class public Lcom/vlingo/midas/notification/HandsFreeInformationNotification;
.super Lcom/vlingo/midas/notification/NotificationPopUp;
.source "HandsFreeInformationNotification.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "notificationText"    # Ljava/lang/String;
    .param p3, "negativeButton"    # Ljava/lang/String;
    .param p4, "positiveButton"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 11
    sget-object v1, Lcom/vlingo/midas/notification/NotificationPopUp$Type;->INFORMATION:Lcom/vlingo/midas/notification/NotificationPopUp$Type;

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/vlingo/midas/notification/NotificationPopUp;-><init>(Lcom/vlingo/midas/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 12
    return-void
.end method


# virtual methods
.method public accept()V
    .locals 1

    .prologue
    .line 16
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isHandsFreeInformationAccepted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->setHandsFreeInformationAccepted(Z)V

    .line 21
    :cond_0
    return-void
.end method

.method public decline()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->setHandsFreeInformationAccepted(Z)V

    .line 28
    return-void
.end method

.method public isAccepted()Z
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isHandsFreeInformationAccepted()Z

    move-result v0

    return v0
.end method
