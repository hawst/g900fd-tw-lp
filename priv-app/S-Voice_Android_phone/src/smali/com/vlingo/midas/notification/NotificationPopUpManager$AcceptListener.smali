.class Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;
.super Ljava/lang/Object;
.source "NotificationPopUpManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/notification/NotificationPopUpManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AcceptListener"
.end annotation


# instance fields
.field private log:Lcom/vlingo/core/internal/logging/Logger;

.field private notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

.field final synthetic this$0:Lcom/vlingo/midas/notification/NotificationPopUpManager;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V
    .locals 1
    .param p2, "notificationPopUp"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;->this$0:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    const-class v0, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 174
    iput-object p2, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .line 175
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 180
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->accept()V

    .line 181
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;->this$0:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    invoke-virtual {v0}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->existDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;->this$0:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    # getter for: Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->access$000(Lcom/vlingo/midas/notification/NotificationPopUpManager;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 184
    :cond_0
    # getter for: Lcom/vlingo/midas/notification/NotificationPopUpManager;->container:Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;
    invoke-static {}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->access$100()Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->removeNotificationFromContainer(Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    .line 185
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$AcceptListener;->this$0:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    invoke-virtual {v0}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->showNextNotification()V

    .line 186
    return-void
.end method
