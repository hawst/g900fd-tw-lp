.class public Lcom/vlingo/midas/notification/DisclaimerNotification;
.super Lcom/vlingo/core/internal/notification/NotificationPopUp;
.source "DisclaimerNotification.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "notificationText"    # Ljava/lang/String;
    .param p3, "positiveButton"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 18
    sget-object v1, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->DISCLAIMER:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/vlingo/core/internal/notification/NotificationPopUp;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 19
    return-void
.end method


# virtual methods
.method public accept()V
    .locals 4

    .prologue
    .line 23
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDisclaimerAccepted()Z

    move-result v2

    if-nez v2, :cond_0

    .line 26
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 28
    :cond_0
    const-string/jumbo v2, "all_notifications_accepted"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 29
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 30
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 31
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 34
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method public decline()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 41
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isHandsFreeInformationAccepted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setHandsFreeInformationAccepted(Z)V

    .line 44
    :cond_0
    return-void
.end method

.method public isAccepted()Z
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDisclaimerAccepted()Z

    move-result v0

    return v0
.end method
