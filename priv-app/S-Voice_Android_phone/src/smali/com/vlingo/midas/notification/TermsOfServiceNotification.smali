.class public Lcom/vlingo/midas/notification/TermsOfServiceNotification;
.super Lcom/vlingo/core/internal/notification/NotificationPopUp;
.source "TermsOfServiceNotification.java"


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "version"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "notificationText"    # Ljava/lang/String;
    .param p4, "negativeButton"    # Ljava/lang/String;
    .param p5, "positiveButton"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 18
    sget-object v1, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->TOS:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/vlingo/core/internal/notification/NotificationPopUp;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 19
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 9
    .param p1, "version"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "notificationText"    # Ljava/lang/String;
    .param p4, "negativeButton"    # Ljava/lang/String;
    .param p5, "positiveButton"    # Ljava/lang/String;
    .param p6, "exitOnDecline"    # Z
    .param p7, "actionRequired"    # Z

    .prologue
    .line 21
    sget-object v1, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->TOS:Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/vlingo/core/internal/notification/NotificationPopUp;-><init>(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 22
    return-void
.end method


# virtual methods
.method public accept()V
    .locals 4

    .prologue
    .line 26
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v2

    if-nez v2, :cond_0

    .line 29
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->setTOSAccepted(Z)V

    .line 30
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onTosAccepted()V

    .line 31
    const-string/jumbo v2, "tos_notification_counter_local"

    const/4 v3, -0x1

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 32
    .local v0, "counter":I
    const-string/jumbo v2, "tos_notification_counter_local"

    add-int/lit8 v0, v0, 0x1

    invoke-static {v2, v0}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 33
    const-string/jumbo v2, "tos_accepted_version"

    invoke-virtual {p0}, Lcom/vlingo/midas/notification/TermsOfServiceNotification;->getVersion()I

    move-result v3

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 34
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 38
    const/4 v1, 0x1

    .line 39
    .local v1, "sendUpdate":Z
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/ServiceManager;->startLocalServices()Z

    .line 41
    .end local v0    # "counter":I
    .end local v1    # "sendUpdate":Z
    :cond_0
    return-void
.end method

.method public decline()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 47
    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->setTOSAccepted(Z)V

    .line 48
    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 49
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isHandsFreeInformationAccepted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->setHandsFreeInformationAccepted(Z)V

    .line 52
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->tos_must_accept_to_use:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 55
    return-void
.end method

.method public isAccepted()Z
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v0

    return v0
.end method
