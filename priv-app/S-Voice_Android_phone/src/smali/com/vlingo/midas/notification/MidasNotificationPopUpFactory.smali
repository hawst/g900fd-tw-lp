.class public Lcom/vlingo/midas/notification/MidasNotificationPopUpFactory;
.super Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;
.source "MidasNotificationPopUpFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/notification/MidasNotificationPopUpFactory$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;-><init>()V

    .line 20
    return-void
.end method


# virtual methods
.method public getNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/vlingo/core/internal/notification/NotificationPopUp;
    .locals 8
    .param p1, "type"    # Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;
    .param p2, "version"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "notificationText"    # Ljava/lang/String;
    .param p5, "negativeButton"    # Ljava/lang/String;
    .param p6, "positiveButton"    # Ljava/lang/String;
    .param p7, "exitOnDecline"    # Z
    .param p8, "actionRequired"    # Z

    .prologue
    .line 13
    invoke-super/range {p0 .. p8}, Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;->getNotification(Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .line 14
    sget-object v0, Lcom/vlingo/midas/notification/MidasNotificationPopUpFactory$1;->$SwitchMap$com$vlingo$core$internal$notification$NotificationPopUp$Type:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/notification/NotificationPopUp$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 22
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 16
    :pswitch_0
    new-instance v0, Lcom/vlingo/midas/notification/TermsOfServiceNotification;

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/vlingo/midas/notification/TermsOfServiceNotification;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 18
    :pswitch_1
    new-instance v0, Lcom/vlingo/midas/notification/AlertNotification;

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/vlingo/midas/notification/AlertNotification;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 14
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
