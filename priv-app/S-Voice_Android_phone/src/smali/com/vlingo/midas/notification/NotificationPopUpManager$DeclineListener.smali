.class Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;
.super Ljava/lang/Object;
.source "NotificationPopUpManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/notification/NotificationPopUpManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeclineListener"
.end annotation


# instance fields
.field private notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

.field final synthetic this$0:Lcom/vlingo/midas/notification/NotificationPopUpManager;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/notification/NotificationPopUpManager;Lcom/vlingo/core/internal/notification/NotificationPopUp;)V
    .locals 0
    .param p2, "notificationPopUp"    # Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;->this$0:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    iput-object p2, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    .line 195
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 200
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->decline()V

    .line 201
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/notification/NotificationPopUp;->exitOnDecline()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;->this$0:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->sendResult(I)V

    .line 208
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;->this$0:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    # getter for: Lcom/vlingo/midas/notification/NotificationPopUpManager;->currentDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->access$000(Lcom/vlingo/midas/notification/NotificationPopUpManager;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 205
    # getter for: Lcom/vlingo/midas/notification/NotificationPopUpManager;->container:Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;
    invoke-static {}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->access$100()Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;->notificationPopUp:Lcom/vlingo/core/internal/notification/NotificationPopUp;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/notification/MidasNotificationPopUpContainer;->removeNotificationFromContainer(Lcom/vlingo/core/internal/notification/NotificationPopUp;)V

    .line 206
    iget-object v0, p0, Lcom/vlingo/midas/notification/NotificationPopUpManager$DeclineListener;->this$0:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    invoke-virtual {v0}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->showNextNotification()V

    goto :goto_0
.end method
