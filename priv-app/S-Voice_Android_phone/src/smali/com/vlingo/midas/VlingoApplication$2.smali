.class Lcom/vlingo/midas/VlingoApplication$2;
.super Ljava/lang/Object;
.source "VlingoApplication.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/VlingoApplication;->initClientSpecificSettings(Landroid/content/SharedPreferences$Editor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/VlingoApplication;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/VlingoApplication;)V
    .locals 0

    .prologue
    .line 685
    iput-object p1, p0, Lcom/vlingo/midas/VlingoApplication$2;->this$0:Lcom/vlingo/midas/VlingoApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1, "args"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 689
    if-nez p2, :cond_0

    .line 694
    :goto_0
    return-void

    .line 690
    :cond_0
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->SERVER_URL_KEYS:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 691
    invoke-static {}, Lcom/vlingo/midas/util/ServerDetails;->getInstance()Lcom/vlingo/midas/util/ServerDetails;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/VlingoAndroidCore;->updateServerInfo(Lcom/vlingo/core/internal/util/CoreServerInfo;)V

    .line 693
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/VlingoApplication$2;->this$0:Lcom/vlingo/midas/VlingoApplication;

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/vlingo/midas/provider/VlingoConfigProvider;->CONTENT_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "SETTINGS/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method
