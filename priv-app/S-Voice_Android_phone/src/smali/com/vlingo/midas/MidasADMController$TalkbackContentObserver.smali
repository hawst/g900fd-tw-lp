.class Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;
.super Landroid/database/ContentObserver;
.source "MidasADMController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/MidasADMController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TalkbackContentObserver"
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field final synthetic this$0:Lcom/vlingo/midas/MidasADMController;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/MidasADMController;Landroid/os/Handler;)V
    .locals 1
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    .line 157
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 154
    const-class v0, Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;->TAG:Ljava/lang/String;

    .line 158
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onChange() : selfChange="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v0, p0, Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # invokes: Lcom/vlingo/midas/MidasADMController;->processOnChange()V
    invoke-static {v0}, Lcom/vlingo/midas/MidasADMController;->access$200(Lcom/vlingo/midas/MidasADMController;)V

    .line 164
    return-void
.end method
