.class public Lcom/vlingo/midas/DeviceConfigurationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DeviceConfigurationReceiver.java"


# static fields
.field public static final ACTION_CHECK_DOUBLE_TAP_CONFIG:Ljava/lang/String; = "com.vlingo.client.app.action.CHECK_DOUBLE_TAP_CONFIG"

.field private static final NOT_SET:I = -0x1

.field private static final SYSTEM_DOUBLE_TAP_SETTING:Ljava/lang/String; = "double_tab_launch"

.field private static final TAG:Ljava/lang/String; = "DeviceConfigurationReceiver"

.field private static degaswifibmw:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v0

    const-string/jumbo v1, "ro.product.name"

    invoke-virtual {v0, v1}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/DeviceConfigurationReceiver;->degaswifibmw:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private doCheckDoubleTapConfig(Landroid/content/Context;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 119
    const/4 v2, 0x1

    .line 120
    .local v2, "ret":Z
    const/4 v0, 0x1

    .line 123
    .local v0, "doubleTapConfig":Z
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v6, "double_tab_launch"

    invoke-static {v3, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 126
    .local v1, "previousConfig":I
    if-ne v1, v7, :cond_4

    .line 128
    invoke-direct {p0}, Lcom/vlingo/midas/DeviceConfigurationReceiver;->getDeviceDoubleTapConfig()Z

    move-result v0

    .line 131
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "double_tab_launch"

    if-eqz v0, :cond_2

    move v3, v4

    :goto_0
    invoke-static {v6, v7, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v2

    .line 134
    if-nez v2, :cond_0

    .line 135
    const-string/jumbo v3, "DeviceConfigurationReceiver"

    const-string/jumbo v6, "double_tab_launch set is failed. Try to set again."

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    const-wide/16 v6, 0x1f4

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 140
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v6, "double_tab_launch"

    if-eqz v0, :cond_3

    :goto_1
    invoke-static {v3, v6, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 150
    :cond_0
    :goto_2
    if-eqz v2, :cond_1

    .line 152
    const-string/jumbo v3, "launch_voicetalk"

    invoke-static {v3, v0}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 155
    :cond_1
    return v2

    :cond_2
    move v3, v5

    .line 131
    goto :goto_0

    :cond_3
    move v4, v5

    .line 140
    goto :goto_1

    .line 147
    :cond_4
    if-ne v1, v4, :cond_5

    move v0, v4

    :goto_3
    goto :goto_2

    :cond_5
    move v0, v5

    goto :goto_3

    .line 142
    :catch_0
    move-exception v3

    goto :goto_2
.end method

.method private doCheckVoiceInputControl(Landroid/content/Context;)Z
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x0

    const/4 v13, -0x1

    .line 56
    const/4 v7, 0x1

    .line 58
    .local v7, "ret":Z
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control"

    invoke-static {v11, v12, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 59
    .local v1, "previousConfig_all":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_incomming_calls"

    invoke-static {v11, v12, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 60
    .local v2, "previousConfig_call":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_chatonv"

    invoke-static {v11, v12, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 61
    .local v4, "previousConfig_chatonv":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_alarm"

    invoke-static {v11, v12, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 62
    .local v0, "previousConfig_alarm":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_camera"

    invoke-static {v11, v12, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 63
    .local v3, "previousConfig_camera":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_music"

    invoke-static {v11, v12, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 64
    .local v5, "previousConfig_music":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_radio"

    invoke-static {v11, v12, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 69
    .local v6, "previousConfig_radio":I
    const-string/jumbo v11, "DeviceConfigurationReceiver"

    const-string/jumbo v12, "doCheckVoiceInputControl() is called"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 71
    const-string/jumbo v11, "DeviceConfigurationReceiver"

    const-string/jumbo v12, "doCheckVoiceInputControl >> isKitkatPhoneGUI() :TRUE"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v9, 0x1

    .line 73
    .local v9, "voice_control_master":I
    const/4 v8, 0x0

    .line 80
    .local v8, "voice_control_item":I
    :goto_0
    if-ne v1, v13, :cond_2

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control"

    invoke-static {v11, v12, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v7

    .line 82
    if-nez v7, :cond_2

    .line 115
    :cond_0
    :goto_1
    return v10

    .line 75
    .end local v8    # "voice_control_item":I
    .end local v9    # "voice_control_master":I
    :cond_1
    const-string/jumbo v11, "DeviceConfigurationReceiver"

    const-string/jumbo v12, "doCheckVoiceInputControl >> isKitkatPhoneGUI() :FALSE"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const/4 v9, 0x0

    .line 77
    .restart local v9    # "voice_control_master":I
    const/4 v8, 0x1

    .restart local v8    # "voice_control_item":I
    goto :goto_0

    .line 85
    :cond_2
    if-ne v2, v13, :cond_3

    .line 86
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_incomming_calls"

    invoke-static {v11, v12, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v7

    .line 87
    if-eqz v7, :cond_0

    .line 90
    :cond_3
    if-ne v4, v13, :cond_4

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_chatonv"

    invoke-static {v11, v12, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v7

    .line 92
    if-eqz v7, :cond_0

    .line 95
    :cond_4
    if-ne v0, v13, :cond_5

    .line 96
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_alarm"

    invoke-static {v11, v12, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v7

    .line 97
    if-eqz v7, :cond_0

    .line 100
    :cond_5
    if-ne v3, v13, :cond_6

    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_camera"

    invoke-static {v11, v12, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v7

    .line 102
    if-eqz v7, :cond_0

    .line 105
    :cond_6
    if-ne v5, v13, :cond_7

    .line 106
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_music"

    invoke-static {v11, v12, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v7

    .line 107
    if-eqz v7, :cond_0

    .line 110
    :cond_7
    if-ne v6, v13, :cond_8

    .line 111
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "voice_input_control_radio"

    invoke-static {v11, v12, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v7

    .line 112
    if-eqz v7, :cond_0

    .line 115
    :cond_8
    const/4 v10, 0x1

    goto :goto_1
.end method

.method private getDeviceDoubleTapConfig()Z
    .locals 6

    .prologue
    .line 159
    const/4 v1, 0x1

    .line 161
    .local v1, "ret":Z
    const-string/jumbo v2, ""

    .line 162
    .local v2, "sales_code":Ljava/lang/String;
    const/4 v0, 0x0

    .line 165
    .local v0, "get_sales_code":Ljava/lang/Process;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    const-string/jumbo v5, "getprop ro.csc.sales_code"

    invoke-virtual {v4, v5}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v0

    .line 166
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 168
    .local v3, "sales_code_buffer_reader":Ljava/io/BufferedReader;
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 169
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 171
    if-eqz v2, :cond_0

    const-string/jumbo v4, "VZW"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 172
    :cond_1
    const/4 v1, 0x0

    .line 174
    :cond_2
    sget-object v4, Lcom/vlingo/midas/DeviceConfigurationReceiver;->degaswifibmw:Ljava/lang/String;

    if-eqz v4, :cond_3

    sget-object v4, Lcom/vlingo/midas/DeviceConfigurationReceiver;->degaswifibmw:Ljava/lang/String;

    const-string/jumbo v5, "degaswifibmw"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 175
    const/4 v1, 0x0

    .line 179
    :cond_3
    if-eqz v2, :cond_5

    .line 180
    const-string/jumbo v4, "ILO"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string/jumbo v4, "CEL"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string/jumbo v4, "PCL"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string/jumbo v4, "PTR"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string/jumbo v4, "MIR"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_5

    .line 183
    :cond_4
    const/4 v1, 0x0

    .line 191
    .end local v3    # "sales_code_buffer_reader":Ljava/io/BufferedReader;
    :cond_5
    :goto_0
    return v1

    .line 188
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method private turnOffApplicationPackage(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 196
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 198
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-eqz v0, :cond_0

    .line 199
    sget-object v1, Lcom/vlingo/midas/DeviceConfigurationReceiver;->degaswifibmw:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/midas/DeviceConfigurationReceiver;->degaswifibmw:Ljava/lang/String;

    const-string/jumbo v2, "degaswifibmw"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    const-string/jumbo v1, "com.vlingo.midas"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 201
    const-string/jumbo v1, "DeviceConfigurationReceiver"

    const-string/jumbo v2, "Turn off S Voice package."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    :cond_0
    return v3
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 26
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v5, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "com.vlingo.client.app.action.CHECK_DOUBLE_TAP_CONFIG"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 30
    :cond_0
    const/4 v4, 0x1

    .line 32
    .local v4, "set_completed":Z
    invoke-direct {p0, p1}, Lcom/vlingo/midas/DeviceConfigurationReceiver;->doCheckDoubleTapConfig(Landroid/content/Context;)Z

    move-result v5

    and-int/2addr v4, v5

    .line 34
    invoke-direct {p0, p1}, Lcom/vlingo/midas/DeviceConfigurationReceiver;->doCheckVoiceInputControl(Landroid/content/Context;)Z

    move-result v5

    and-int/2addr v4, v5

    .line 36
    invoke-direct {p0, p1}, Lcom/vlingo/midas/DeviceConfigurationReceiver;->turnOffApplicationPackage(Landroid/content/Context;)Z

    move-result v5

    and-int/2addr v4, v5

    .line 39
    if-eqz v4, :cond_4

    .line 41
    const-string/jumbo v5, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v2, 0x0

    .line 43
    .local v2, "flags":I
    :goto_0
    if-nez v2, :cond_1

    .line 44
    const-string/jumbo v5, "DeviceConfigurationReceiver"

    const-string/jumbo v6, "Device configuration is succeed. S Voice will be force killed. It is not a bug."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 48
    .local v3, "pm":Landroid/content/pm/PackageManager;
    new-instance v1, Landroid/content/ComponentName;

    const-class v5, Lcom/vlingo/midas/DeviceConfigurationReceiver;

    invoke-direct {v1, p1, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    .local v1, "componentName":Landroid/content/ComponentName;
    const/4 v5, 0x2

    invoke-virtual {v3, v1, v5, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 54
    .end local v1    # "componentName":Landroid/content/ComponentName;
    .end local v2    # "flags":I
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    .end local v4    # "set_completed":Z
    :cond_2
    :goto_1
    return-void

    .line 41
    .restart local v4    # "set_completed":Z
    :cond_3
    const/4 v2, 0x1

    goto :goto_0

    .line 51
    :cond_4
    const-string/jumbo v5, "DeviceConfigurationReceiver"

    const-string/jumbo v6, "Device configuration is failed. It will be try to set again at next booting time."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
