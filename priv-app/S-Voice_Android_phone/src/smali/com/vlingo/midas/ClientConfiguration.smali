.class public final enum Lcom/vlingo/midas/ClientConfiguration;
.super Ljava/lang/Enum;
.source "ClientConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/ClientConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/ClientConfiguration;

.field public static final ANSWERS:I = 0x5

.field private static final HOME_KEY_DOUBLE:Ljava/lang/String; = "1"

.field public static final enum LMTT_NOSPACES:Lcom/vlingo/midas/ClientConfiguration;

.field public static final LOCALSEARCH:I = 0xa

.field public static final NAVER:I = 0xf

.field private static final PROPERTY_KEY:Ljava/lang/String; = "ro.vlingo.launch.key"

.field private static final SEARCH_KEY_LONG:Ljava/lang/String; = "2"


# instance fields
.field private supportedLanguages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 26
    new-instance v0, Lcom/vlingo/midas/ClientConfiguration;

    const-string/jumbo v1, "LMTT_NOSPACES"

    sget-object v2, Lcom/vlingo/midas/settings/MidasSettings;->LANGUAGES_CHINESE_ONLY:Ljava/util/Set;

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/midas/ClientConfiguration;-><init>(Ljava/lang/String;ILjava/util/Set;)V

    sput-object v0, Lcom/vlingo/midas/ClientConfiguration;->LMTT_NOSPACES:Lcom/vlingo/midas/ClientConfiguration;

    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/vlingo/midas/ClientConfiguration;

    sget-object v1, Lcom/vlingo/midas/ClientConfiguration;->LMTT_NOSPACES:Lcom/vlingo/midas/ClientConfiguration;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ClientConfiguration;->$VALUES:[Lcom/vlingo/midas/ClientConfiguration;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p3, "supportedLangs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/ClientConfiguration;->supportedLanguages:Ljava/util/Set;

    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/ClientConfiguration;->supportedLanguages:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 38
    return-void
.end method

.method public static getDefaultBrowser()Ljava/lang/String;
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 47
    .local v1, "res":Landroid/content/res/Resources;
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 48
    sget v2, Lcom/vlingo/midas/R$array;->web_search_engine_preference_values:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "browsers":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    .line 50
    const/4 v2, 0x0

    aget-object v2, v0, v2

    .line 52
    :goto_0
    return-object v2

    :cond_0
    const-string/jumbo v2, "google"

    goto :goto_0
.end method

.method public static getSalesCodeProperty()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isAmericanPhone()Z
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->isAmericanPhone()Z

    move-result v0

    return v0
.end method

.method public static isChatONVPhone()Z
    .locals 1

    .prologue
    .line 97
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->isChatONVPhone()Z

    move-result v0

    return v0
.end method

.method public static isChinesePhone()Z
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->isChinesePhone()Z

    move-result v0

    return v0
.end method

.method public static isChinesePhone(Ljava/lang/String;)Z
    .locals 1
    .param p0, "fakeAppChannel"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->init(Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v0

    return v0
.end method

.method public static isEnabled(I)Z
    .locals 3
    .param p0, "flag"    # I

    .prologue
    .line 69
    const/4 v0, 0x0

    .line 71
    .local v0, "toReturn":Z
    sparse-switch p0, :sswitch_data_0

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 73
    :sswitch_0
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "en-US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "en-GB"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 75
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 77
    :cond_2
    const/4 v0, 0x0

    .line 79
    goto :goto_0

    .line 81
    :sswitch_1
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "en-US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 82
    if-nez v0, :cond_3

    .line 83
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "ko-KR"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ko-KR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 84
    :cond_3
    if-nez v0, :cond_0

    .line 85
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "zh-CN"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "zh-CN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 88
    :sswitch_2
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "ko-KR"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ko-KR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_1
        0xf -> :sswitch_2
    .end sparse-switch
.end method

.method public static isJapanesePhone()Z
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->isJapanesePhone()Z

    move-result v0

    return v0
.end method

.method public static isKDDIPhone()Z
    .locals 1

    .prologue
    .line 121
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->isKDDIPhone()Z

    move-result v0

    return v0
.end method

.method public static isUCameraModel()Z
    .locals 2

    .prologue
    .line 137
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 138
    .local v0, "build_model":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string/jumbo v1, "EK-GN120"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "EK-GN100_CHN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "EK-GC200"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 140
    :cond_0
    const/4 v1, 0x1

    .line 142
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static supportsSVoiceAssociatedServiceOnly()Z
    .locals 2

    .prologue
    .line 109
    const-string/jumbo v0, "is_svoice_for_gear"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static useSearchLongPress()Z
    .locals 4

    .prologue
    .line 133
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "ro.vlingo.launch.key"

    const-string/jumbo v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "ret":Ljava/lang/String;
    const-string/jumbo v1, "2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/ClientConfiguration;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/vlingo/midas/ClientConfiguration;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/ClientConfiguration;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/ClientConfiguration;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/vlingo/midas/ClientConfiguration;->$VALUES:[Lcom/vlingo/midas/ClientConfiguration;

    invoke-virtual {v0}, [Lcom/vlingo/midas/ClientConfiguration;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/ClientConfiguration;

    return-object v0
.end method


# virtual methods
.method public isSupported()Z
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/midas/ClientConfiguration;->supportedLanguages:Ljava/util/Set;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
