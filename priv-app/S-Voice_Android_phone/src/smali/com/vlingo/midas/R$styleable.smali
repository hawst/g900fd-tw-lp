.class public final Lcom/vlingo/midas/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CustomIUXButton:[I

.field public static final CustomIUXButton_buttonText:I = 0x0

.field public static final CustomIUXButton_textColor:I = 0x1

.field public static final CustomIUXButton_textDrawable:I = 0x2

.field public static final SplitView:[I

.field public static final SplitView_Leftweight:I = 0x2

.field public static final SplitView_Rightweight:I = 0x3

.field public static final SplitView_splitterBackground:I = 0x1

.field public static final SplitView_splitterSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4412
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vlingo/midas/R$styleable;->CustomIUXButton:[I

    .line 4465
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/vlingo/midas/R$styleable;->SplitView:[I

    return-void

    .line 4412
    nop

    :array_0
    .array-data 4
        0x7f010004
        0x7f010005
        0x7f010006
    .end array-data

    .line 4465
    :array_1
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
