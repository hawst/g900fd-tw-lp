.class public Lcom/vlingo/midas/music/SearchMusic;
.super Ljava/lang/Object;
.source "SearchMusic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/music/SearchMusic$1;
    }
.end annotation


# static fields
.field private static MAX_COUNT:I

.field private static final SEARCH_MUSIC_MAX:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x5

    sput v0, Lcom/vlingo/midas/music/SearchMusic;->MAX_COUNT:I

    .line 47
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->getMultiWidgetItemsUltimateMax()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sput v0, Lcom/vlingo/midas/music/SearchMusic;->SEARCH_MUSIC_MAX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 757
    return-void
.end method

.method private static augmentQuery(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p0, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 899
    .local p1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 900
    .local v2, "words":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 901
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 902
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/util/StringUtils;->containsString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 903
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 907
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 909
    :cond_2
    return-object v2
.end method

.method private static augmentWords([Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .param p0, "words"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 913
    .local p1, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 914
    .local v1, "augmentedWords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 915
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 916
    .local v4, "word":Ljava/lang/String;
    invoke-static {v4, p1}, Lcom/vlingo/midas/music/SearchMusic;->augmentQuery(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 915
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 920
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "word":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .end local v1    # "augmentedWords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    return-object v1
.end method

.method private static buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;
    .locals 7
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .param p3, "searchType"    # Lcom/vlingo/midas/util/PlayMusicType;

    .prologue
    .line 733
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->stringIsNonAsciiWithCase(Ljava/lang/String;)Z

    move-result v1

    .line 734
    .local v1, "isNonAsciiWithCase":Z
    const/4 v2, 0x0

    .line 736
    .local v2, "nameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 737
    sget-object v5, Lcom/vlingo/midas/music/SearchMusic$1;->$SwitchMap$com$vlingo$midas$util$PlayMusicType:[I

    invoke-virtual {p3}, Lcom/vlingo/midas/util/PlayMusicType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 755
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "nameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 756
    .restart local v2    # "nameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 762
    :cond_0
    :goto_0
    if-eqz p2, :cond_3

    .line 763
    if-eqz v1, :cond_2

    .line 764
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v5, v2}, Lcom/vlingo/midas/music/SearchMusic;->augmentWords([Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 765
    .local v4, "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 766
    invoke-static {p0, v4}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    .line 783
    .end local v4    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    return-object v5

    .line 739
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getAlbumNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 740
    goto :goto_0

    .line 743
    :pswitch_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getArtistNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 744
    goto :goto_0

    .line 747
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getPlaylistNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 748
    goto :goto_0

    .line 751
    :pswitch_3
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSongNameList()Ljava/util/ArrayList;

    move-result-object v2

    .line 752
    goto :goto_0

    .line 768
    .restart local v4    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 771
    .end local v4    # "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-static {p0, p1}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 774
    :cond_3
    const-string/jumbo v5, " "

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 775
    .local v3, "parts":[Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 776
    invoke-static {v3, v2}, Lcom/vlingo/midas/music/SearchMusic;->augmentWords([Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 777
    .local v0, "augmentedParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 778
    invoke-static {p0, v0}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 780
    :cond_4
    invoke-static {p0, v3}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 783
    .end local v0    # "augmentedParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    invoke-static {p0, v3}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 737
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static byAlbum(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/vlingo/midas/music/SearchMusic;->getAlbumList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 235
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "album"

    sget-object v1, Lcom/vlingo/midas/util/PlayMusicType;->ALBUM:Lcom/vlingo/midas/util/PlayMusicType;

    invoke-static {v0, p0, p1, v1}, Lcom/vlingo/midas/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/midas/util/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static byArtist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "artistName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/vlingo/midas/music/SearchMusic;->getArtistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 221
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "artist"

    sget-object v1, Lcom/vlingo/midas/util/PlayMusicType;->ARTIST:Lcom/vlingo/midas/util/PlayMusicType;

    invoke-static {v0, p0, p1, v1}, Lcom/vlingo/midas/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/midas/util/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static byEverything(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "isLastAttempt"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    const/4 v11, 0x0

    .line 147
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v15, 0x0

    .line 148
    .local v15, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    const/4 v2, 0x5

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v6, "album_id"

    aput-object v6, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v6, "artist"

    aput-object v6, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v6, "title"

    aput-object v6, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v6, "album"

    aput-object v6, v4, v2

    .line 149
    .local v4, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "artist"

    const/4 v7, 0x1

    sget-object v20, Lcom/vlingo/midas/util/PlayMusicType;->ARTIST:Lcom/vlingo/midas/util/PlayMusicType;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v6, v0, v7, v1}, Lcom/vlingo/midas/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, " OR "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "title"

    const/4 v7, 0x1

    sget-object v20, Lcom/vlingo/midas/util/PlayMusicType;->TITLE:Lcom/vlingo/midas/util/PlayMusicType;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v6, v0, v7, v1}, Lcom/vlingo/midas/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, " OR "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "album"

    const/4 v7, 0x1

    sget-object v20, Lcom/vlingo/midas/util/PlayMusicType;->ALBUM:Lcom/vlingo/midas/util/PlayMusicType;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v6, v0, v7, v1}, Lcom/vlingo/midas/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 151
    .local v5, "where":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v13

    .line 152
    .local v13, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v17, 0x0

    .line 153
    .local v17, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v13, :cond_0

    .line 155
    :try_start_0
    invoke-virtual {v13}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v17, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 163
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 164
    .local v3, "musicUri":Landroid/net/Uri;
    if-eqz v17, :cond_5

    .line 165
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v3

    .line 168
    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const-string/jumbo v7, "title_key"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 170
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .local v16, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    if-eqz v11, :cond_2

    :try_start_2
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 174
    const-string/jumbo v9, ""

    .line 175
    .local v9, "albumUri":Ljava/lang/String;
    if-eqz v17, :cond_6

    .line 176
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 180
    :cond_1
    :goto_2
    const-string/jumbo v2, "_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    .line 181
    .local v18, "songId":Ljava/lang/Long;
    const-string/jumbo v2, "album_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 182
    .local v8, "albumId":Ljava/lang/String;
    const-string/jumbo v2, "artist"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 183
    .local v10, "artist":Ljava/lang/String;
    const-string/jumbo v2, "title"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 184
    .local v19, "title":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 185
    .local v14, "imageUri":Landroid/net/Uri;
    new-instance v2, Lcom/vlingo/midas/music/MusicDetails;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v2, v0, v1, v10, v14}, Lcom/vlingo/midas/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-nez v2, :cond_1

    .line 199
    .end local v8    # "albumId":Ljava/lang/String;
    .end local v9    # "albumUri":Ljava/lang/String;
    .end local v10    # "artist":Ljava/lang/String;
    .end local v14    # "imageUri":Landroid/net/Uri;
    .end local v18    # "songId":Ljava/lang/Long;
    .end local v19    # "title":Ljava/lang/String;
    :cond_2
    if-eqz v11, :cond_8

    .line 200
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    move-object/from16 v15, v16

    .line 204
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_3
    :goto_3
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_4

    if-eqz p2, :cond_4

    .line 208
    :cond_4
    return-object v15

    .line 156
    .end local v3    # "musicUri":Landroid/net/Uri;
    :catch_0
    move-exception v12

    .line 157
    .local v12, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v12}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_0

    .line 158
    .end local v12    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v12

    .line 159
    .local v12, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v12}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_0

    .line 167
    .end local v12    # "e":Ljava/lang/IllegalAccessException;
    .restart local v3    # "musicUri":Landroid/net/Uri;
    :cond_5
    :try_start_3
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 178
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v9    # "albumUri":Ljava/lang/String;
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_6
    :try_start_4
    const-string/jumbo v9, "content://media/external/audio/albumart/"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 195
    .end local v9    # "albumUri":Ljava/lang/String;
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_2
    move-exception v2

    .line 199
    :goto_4
    if-eqz v11, :cond_3

    .line 200
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 199
    :catchall_0
    move-exception v2

    :goto_5
    if-eqz v11, :cond_7

    .line 200
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v2

    .line 199
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object/from16 v15, v16

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_5

    .line 195
    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_3
    move-exception v2

    move-object/from16 v15, v16

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_4

    .end local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_8
    move-object/from16 v15, v16

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v15    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_3
.end method

.method public static byPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/vlingo/midas/music/SearchMusic;->getPlaylistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 259
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/vlingo/midas/music/SearchMusic;->searchSongForPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static byTitle(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 246
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/vlingo/midas/music/SearchMusic;->getTitleList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getAlbumList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 435
    const/4 v14, 0x0

    .line 436
    .local v14, "cursor":Landroid/database/Cursor;
    const/16 v17, 0x0

    .line 437
    .local v17, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v6, "album"

    aput-object v6, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v6, "artist"

    aput-object v6, v4, v2

    .line 438
    .local v4, "projection":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v5, 0x0

    .line 439
    .local v5, "where":Ljava/lang/String;
    :goto_0
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v16

    .line 440
    .local v16, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v19, 0x0

    .line 441
    .local v19, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v16, :cond_0

    .line 443
    :try_start_0
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v19, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 451
    :cond_0
    :goto_1
    const/4 v3, 0x0

    .line 452
    .local v3, "albumUri":Landroid/net/Uri;
    if-eqz v19, :cond_6

    .line 453
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUM:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v3

    .line 459
    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const-string/jumbo v7, "album_key"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 461
    if-eqz v14, :cond_3

    .line 465
    const-string/jumbo v9, ""

    .line 466
    .local v9, "albumArtUristring":Ljava/lang/String;
    if-eqz v19, :cond_7

    .line 467
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 470
    :goto_3
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    .line 471
    const/4 v13, 0x0

    .line 472
    .local v13, "count":I
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 474
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .local v18, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_1
    :try_start_2
    const-string/jumbo v2, "_id"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    .line 475
    .local v11, "albumId":Ljava/lang/Long;
    const-string/jumbo v2, "album"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 476
    .local v12, "albumName":Ljava/lang/String;
    const-string/jumbo v2, "artist"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 477
    .local v10, "albumArtist":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 482
    .local v8, "albumArtUri":Landroid/net/Uri;
    new-instance v2, Lcom/vlingo/midas/music/MusicDetails;

    invoke-direct {v2, v11, v12, v10, v8}, Lcom/vlingo/midas/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 483
    add-int/lit8 v13, v13, 0x1

    .line 484
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Lcom/vlingo/midas/music/SearchMusic;->SEARCH_MUSIC_MAX:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-lt v13, v2, :cond_1

    :cond_2
    move-object/from16 v17, v18

    .line 490
    .end local v8    # "albumArtUri":Landroid/net/Uri;
    .end local v9    # "albumArtUristring":Ljava/lang/String;
    .end local v10    # "albumArtist":Ljava/lang/String;
    .end local v11    # "albumId":Ljava/lang/Long;
    .end local v12    # "albumName":Ljava/lang/String;
    .end local v13    # "count":I
    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_3
    if-eqz v14, :cond_4

    .line 491
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 495
    :cond_4
    :goto_4
    return-object v17

    .line 438
    .end local v3    # "albumUri":Landroid/net/Uri;
    .end local v5    # "where":Ljava/lang/String;
    .end local v16    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v19    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :cond_5
    const-string/jumbo v2, "album"

    sget-object v6, Lcom/vlingo/midas/util/PlayMusicType;->ALBUM:Lcom/vlingo/midas/util/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v2, v0, v1, v6}, Lcom/vlingo/midas/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 444
    .restart local v5    # "where":Ljava/lang/String;
    .restart local v16    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v19    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_0
    move-exception v15

    .line 445
    .local v15, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v15}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_1

    .line 446
    .end local v15    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v15

    .line 447
    .local v15, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v15}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_1

    .line 455
    .end local v15    # "e":Ljava/lang/IllegalAccessException;
    .restart local v3    # "albumUri":Landroid/net/Uri;
    :cond_6
    :try_start_3
    sget-object v3, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto/16 :goto_2

    .line 469
    .restart local v9    # "albumArtUristring":Ljava/lang/String;
    :cond_7
    const-string/jumbo v9, "content://media/external/audio/albumart/"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 486
    .end local v9    # "albumArtUristring":Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 490
    :goto_5
    if-eqz v14, :cond_4

    .line 491
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 490
    :catchall_0
    move-exception v2

    :goto_6
    if-eqz v14, :cond_8

    .line 491
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v2

    .line 490
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v9    # "albumArtUristring":Ljava/lang/String;
    .restart local v13    # "count":I
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object/from16 v17, v18

    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_6

    .line 486
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_3
    move-exception v2

    move-object/from16 v17, v18

    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_5
.end method

.method public static getArtistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346
    const/16 v18, 0x0

    .line 347
    .local v18, "cursor":Landroid/database/Cursor;
    const/16 v19, 0x0

    .line 348
    .local v19, "cursorAlbumArt":Landroid/database/Cursor;
    const/16 v23, 0x0

    .line 349
    .local v23, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v6, "artist"

    aput-object v6, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v6, "artist_key"

    aput-object v6, v4, v2

    .line 350
    .local v4, "projection":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v5, 0x0

    .line 351
    .local v5, "where":Ljava/lang/String;
    :goto_0
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v21

    .line 352
    .local v21, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v25, 0x0

    .line 353
    .local v25, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v21, :cond_0

    .line 355
    :try_start_0
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v25, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 362
    :cond_0
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v6, "ko-KR"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 363
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ") OR ("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "REPLACE(`artist`,\' \',\'\')"

    sget-object v9, Lcom/vlingo/midas/util/PlayMusicType;->ARTIST:Lcom/vlingo/midas/util/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v6, v0, v1, v9}, Lcom/vlingo/midas/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ")"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 366
    :cond_1
    const/4 v3, 0x0

    .line 367
    .local v3, "artistUri":Landroid/net/Uri;
    if-eqz v25, :cond_a

    .line 368
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ARTIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v3

    .line 371
    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const-string/jumbo v7, "artist_key"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 373
    if-eqz v18, :cond_6

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 377
    const/16 v17, 0x0

    .line 378
    .local v17, "count":I
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 379
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .local v24, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :try_start_2
    const-string/jumbo v13, ""

    .line 380
    .local v13, "albumUri":Ljava/lang/String;
    if-eqz v25, :cond_b

    .line 381
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    .line 385
    :cond_2
    :goto_3
    const-string/jumbo v2, "_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 386
    .local v15, "artistId":Ljava/lang/Long;
    const-string/jumbo v2, "artist"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 387
    .local v14, "artist":Ljava/lang/String;
    const-string/jumbo v2, "artist_key"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 394
    .local v16, "artistKey":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "album_id"

    aput-object v6, v8, v2

    .line 395
    .local v8, "projectionAlbumArt":[Ljava/lang/String;
    const-string/jumbo v2, "artist_key"

    const/4 v6, 0x1

    sget-object v9, Lcom/vlingo/midas/util/PlayMusicType;->ARTIST:Lcom/vlingo/midas/util/PlayMusicType;

    move-object/from16 v0, v16

    invoke-static {v2, v0, v6, v9}, Lcom/vlingo/midas/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    .line 396
    sget-object v22, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 397
    .local v22, "imageUri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 398
    .local v7, "musicUri":Landroid/net/Uri;
    if-eqz v25, :cond_c

    .line 399
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v7

    .line 402
    :goto_4
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v10, 0x0

    const-string/jumbo v11, "artist_key"

    move-object v9, v5

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 404
    if-eqz v19, :cond_4

    .line 405
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 406
    const-string/jumbo v2, "album_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 407
    .local v12, "albumId":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    .line 412
    .end local v12    # "albumId":Ljava/lang/String;
    :cond_3
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 415
    :cond_4
    new-instance v2, Lcom/vlingo/midas/music/MusicDetails;

    move-object/from16 v0, v22

    invoke-direct {v2, v15, v14, v14, v0}, Lcom/vlingo/midas/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    add-int/lit8 v17, v17, 0x1

    .line 417
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    sget v2, Lcom/vlingo/midas/music/SearchMusic;->SEARCH_MUSIC_MAX:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move/from16 v0, v17

    if-lt v0, v2, :cond_2

    :cond_5
    move-object/from16 v23, v24

    .line 423
    .end local v7    # "musicUri":Landroid/net/Uri;
    .end local v8    # "projectionAlbumArt":[Ljava/lang/String;
    .end local v13    # "albumUri":Ljava/lang/String;
    .end local v14    # "artist":Ljava/lang/String;
    .end local v15    # "artistId":Ljava/lang/Long;
    .end local v16    # "artistKey":Ljava/lang/String;
    .end local v17    # "count":I
    .end local v22    # "imageUri":Landroid/net/Uri;
    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_6
    if-eqz v18, :cond_7

    .line 424
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 426
    :cond_7
    if-eqz v19, :cond_8

    .line 427
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 431
    :cond_8
    :goto_5
    return-object v23

    .line 350
    .end local v3    # "artistUri":Landroid/net/Uri;
    .end local v5    # "where":Ljava/lang/String;
    .end local v21    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v25    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :cond_9
    const-string/jumbo v2, "artist"

    sget-object v6, Lcom/vlingo/midas/util/PlayMusicType;->ARTIST:Lcom/vlingo/midas/util/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v2, v0, v1, v6}, Lcom/vlingo/midas/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 356
    .restart local v5    # "where":Ljava/lang/String;
    .restart local v21    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v25    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_0
    move-exception v20

    .line 357
    .local v20, "e":Ljava/lang/InstantiationException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_1

    .line 358
    .end local v20    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v20

    .line 359
    .local v20, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_1

    .line 370
    .end local v20    # "e":Ljava/lang/IllegalAccessException;
    .restart local v3    # "artistUri":Landroid/net/Uri;
    :cond_a
    :try_start_3
    sget-object v3, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 383
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v13    # "albumUri":Ljava/lang/String;
    .restart local v17    # "count":I
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_b
    :try_start_4
    const-string/jumbo v13, "content://media/external/audio/albumart/"

    goto/16 :goto_3

    .line 401
    .restart local v7    # "musicUri":Landroid/net/Uri;
    .restart local v8    # "projectionAlbumArt":[Ljava/lang/String;
    .restart local v14    # "artist":Ljava/lang/String;
    .restart local v15    # "artistId":Ljava/lang/Long;
    .restart local v16    # "artistKey":Ljava/lang/String;
    .restart local v22    # "imageUri":Landroid/net/Uri;
    :cond_c
    sget-object v7, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_4

    .line 419
    .end local v7    # "musicUri":Landroid/net/Uri;
    .end local v8    # "projectionAlbumArt":[Ljava/lang/String;
    .end local v13    # "albumUri":Ljava/lang/String;
    .end local v14    # "artist":Ljava/lang/String;
    .end local v15    # "artistId":Ljava/lang/Long;
    .end local v16    # "artistKey":Ljava/lang/String;
    .end local v17    # "count":I
    .end local v22    # "imageUri":Landroid/net/Uri;
    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_2
    move-exception v2

    .line 423
    :goto_6
    if-eqz v18, :cond_d

    .line 424
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 426
    :cond_d
    if-eqz v19, :cond_8

    .line 427
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 423
    :catchall_0
    move-exception v2

    :goto_7
    if-eqz v18, :cond_e

    .line 424
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 426
    :cond_e
    if-eqz v19, :cond_f

    .line 427
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :cond_f
    throw v2

    .line 423
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "count":I
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object/from16 v23, v24

    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_7

    .line 419
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_3
    move-exception v2

    move-object/from16 v23, v24

    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_6
.end method

.method private static getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/midas/util/PlayMusicType;)Ljava/util/List;
    .locals 6
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchCondition"    # Ljava/lang/String;
    .param p3, "searchType"    # Lcom/vlingo/midas/util/PlayMusicType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vlingo/midas/util/PlayMusicType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 264
    invoke-static {p0, p1, p2, v5, p3}, Lcom/vlingo/midas/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    .line 265
    .local v0, "firstResult":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 277
    .end local v0    # "firstResult":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :goto_0
    return-object v0

    .line 269
    .restart local v0    # "firstResult":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_0
    const-string/jumbo v3, " "

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 270
    .local v1, "parts":[Ljava/lang/String;
    array-length v3, v1

    if-le v3, v5, :cond_1

    .line 271
    const-string/jumbo v3, " "

    const-string/jumbo v4, "%"

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 272
    invoke-static {p0, p1, p2, v5, p3}, Lcom/vlingo/midas/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/util/List;

    move-result-object v2

    .line 273
    .local v2, "secondResult":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    move-object v0, v2

    .line 274
    goto :goto_0

    .line 277
    .end local v2    # "secondResult":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_1
    const/4 v3, 0x0

    invoke-static {p0, p1, p2, v3, p3}, Lcom/vlingo/midas/music/SearchMusic;->getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static getDataFromSongsList(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/util/List;
    .locals 22
    .param p0, "columnName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchCondition"    # Ljava/lang/String;
    .param p3, "exactSearch"    # Z
    .param p4, "searchType"    # Lcom/vlingo/midas/util/PlayMusicType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/vlingo/midas/util/PlayMusicType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    const/4 v13, 0x0

    .line 282
    .local v13, "cursor":Landroid/database/Cursor;
    const/16 v17, 0x0

    .line 283
    .local v17, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    const/4 v4, 0x4

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v8, "_id"

    aput-object v8, v6, v4

    const/4 v4, 0x1

    const-string/jumbo v8, "album_id"

    aput-object v8, v6, v4

    const/4 v4, 0x2

    const-string/jumbo v8, "artist"

    aput-object v8, v6, v4

    const/4 v4, 0x3

    const-string/jumbo v8, "title"

    aput-object v8, v6, v4

    .line 284
    .local v6, "projection":[Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p4

    invoke-static {v0, v1, v2, v3}, Lcom/vlingo/midas/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;

    move-result-object v7

    .line 285
    .local v7, "where":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v4}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v15

    .line 286
    .local v15, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v19, 0x0

    .line 287
    .local v19, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v15, :cond_0

    .line 289
    :try_start_0
    invoke-virtual {v15}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v19, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 297
    :cond_0
    :goto_0
    const/4 v5, 0x0

    .line 298
    .local v5, "musicUri":Landroid/net/Uri;
    if-eqz v19, :cond_5

    .line 299
    :try_start_1
    sget-object v4, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v5

    .line 302
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v8, 0x0

    const-string/jumbo v9, "title_key"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 304
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 305
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .local v18, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    if-eqz v13, :cond_2

    :try_start_2
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 308
    const-string/jumbo v11, ""

    .line 309
    .local v11, "albumUri":Ljava/lang/String;
    if-eqz v19, :cond_6

    .line 310
    sget-object v4, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 314
    :cond_1
    :goto_2
    const-string/jumbo v4, "_id"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    .line 315
    .local v20, "songId":Ljava/lang/Long;
    const-string/jumbo v4, "album_id"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 316
    .local v10, "albumId":Ljava/lang/String;
    const-string/jumbo v4, "artist"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 317
    .local v12, "artist":Ljava/lang/String;
    const-string/jumbo v4, "title"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 318
    .local v21, "title":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 319
    .local v16, "imageUri":Landroid/net/Uri;
    new-instance v4, Lcom/vlingo/midas/music/MusicDetails;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v16

    invoke-direct {v4, v0, v1, v12, v2}, Lcom/vlingo/midas/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v4

    if-nez v4, :cond_1

    .line 333
    .end local v10    # "albumId":Ljava/lang/String;
    .end local v11    # "albumUri":Ljava/lang/String;
    .end local v12    # "artist":Ljava/lang/String;
    .end local v16    # "imageUri":Landroid/net/Uri;
    .end local v20    # "songId":Ljava/lang/Long;
    .end local v21    # "title":Ljava/lang/String;
    :cond_2
    if-eqz v13, :cond_8

    .line 334
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    move-object/from16 v17, v18

    .line 337
    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_3
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_4

    .line 342
    :cond_4
    return-object v17

    .line 290
    .end local v5    # "musicUri":Landroid/net/Uri;
    :catch_0
    move-exception v14

    .line 291
    .local v14, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v14}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_0

    .line 292
    .end local v14    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v14

    .line 293
    .local v14, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v14}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_0

    .line 301
    .end local v14    # "e":Ljava/lang/IllegalAccessException;
    .restart local v5    # "musicUri":Landroid/net/Uri;
    :cond_5
    :try_start_3
    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 312
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v11    # "albumUri":Ljava/lang/String;
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_6
    :try_start_4
    const-string/jumbo v11, "content://media/external/audio/albumart/"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 329
    .end local v11    # "albumUri":Ljava/lang/String;
    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_2
    move-exception v4

    .line 333
    :goto_4
    if-eqz v13, :cond_3

    .line 334
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 333
    :catchall_0
    move-exception v4

    :goto_5
    if-eqz v13, :cond_7

    .line 334
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v4

    .line 333
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catchall_1
    move-exception v4

    move-object/from16 v17, v18

    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_5

    .line 329
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_3
    move-exception v4

    move-object/from16 v17, v18

    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_4

    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_8
    move-object/from16 v17, v18

    .end local v18    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_3
.end method

.method public static getGenericList(Landroid/content/Context;Ljava/lang/String;Z)Landroid/util/Pair;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Landroid/util/Pair",
            "<",
            "Lcom/vlingo/midas/util/PlayMusicType;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 80
    invoke-static {p0, p1, p2}, Lcom/vlingo/midas/music/SearchMusic;->getPlaylistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 81
    .local v0, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 82
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/vlingo/midas/util/PlayMusicType;->PLAYLIST:Lcom/vlingo/midas/util/PlayMusicType;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 99
    :goto_0
    return-object v1

    .line 86
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/vlingo/midas/music/SearchMusic;->getTitleList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 88
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/vlingo/midas/util/PlayMusicType;->TITLE:Lcom/vlingo/midas/util/PlayMusicType;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 92
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/vlingo/midas/music/SearchMusic;->getAlbumList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 94
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/vlingo/midas/util/PlayMusicType;->ALBUM:Lcom/vlingo/midas/util/PlayMusicType;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :cond_2
    invoke-static {p0, p1, p2}, Lcom/vlingo/midas/music/SearchMusic;->getArtistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 99
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/vlingo/midas/util/PlayMusicType;->ARTIST:Lcom/vlingo/midas/util/PlayMusicType;

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static getPlaylistList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 664
    const/4 v9, 0x0

    .line 665
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 666
    .local v12, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v6, "name"

    aput-object v6, v4, v2

    .line 668
    .local v4, "projection":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v5, 0x0

    .line 669
    .local v5, "where":Ljava/lang/String;
    :goto_0
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v11

    .line 670
    .local v11, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v14, 0x0

    .line 671
    .local v14, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v11, :cond_0

    .line 673
    :try_start_0
    invoke-virtual {v11}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v14, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 681
    :cond_0
    :goto_1
    const/4 v3, 0x0

    .line 682
    .local v3, "playlistUri":Landroid/net/Uri;
    if-eqz v14, :cond_7

    .line 683
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v14, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v3

    .line 686
    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const-string/jumbo v7, "name"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 689
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 693
    const/4 v8, 0x0

    .line 694
    .local v8, "count":I
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 696
    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .local v13, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_1
    :try_start_2
    const-string/jumbo v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 698
    .local v15, "playlistId":Ljava/lang/Long;
    const-string/jumbo v2, "name"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 700
    .local v16, "playlistName":Ljava/lang/String;
    const-string/jumbo v2, "Quick list"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 701
    const-string/jumbo v2, "en-GB"

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 702
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    sget v6, Lcom/vlingo/midas/R$string;->playlists_quicklist_default_value1:I

    invoke-virtual {v2, v6}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 705
    .local v17, "playlistNameLocalized":Ljava/lang/String;
    :goto_3
    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 714
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/vlingo/midas/music/SearchMusic;->searchSongForPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v18

    .line 715
    .local v18, "songList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    if-eqz v18, :cond_2

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_2

    .line 716
    new-instance v2, Lcom/vlingo/midas/music/MusicDetails;

    const-string/jumbo v6, ""

    sget-object v7, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    move-object/from16 v0, v17

    invoke-direct {v2, v15, v0, v6, v7}, Lcom/vlingo/midas/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 717
    add-int/lit8 v8, v8, 0x1

    .line 719
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    sget v2, Lcom/vlingo/midas/music/SearchMusic;->SEARCH_MUSIC_MAX:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-lt v8, v2, :cond_1

    :cond_3
    move-object v12, v13

    .line 725
    .end local v8    # "count":I
    .end local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .end local v15    # "playlistId":Ljava/lang/Long;
    .end local v16    # "playlistName":Ljava/lang/String;
    .end local v17    # "playlistNameLocalized":Ljava/lang/String;
    .end local v18    # "songList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_4
    if-eqz v9, :cond_5

    .line 726
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 729
    :cond_5
    :goto_5
    return-object v12

    .line 668
    .end local v3    # "playlistUri":Landroid/net/Uri;
    .end local v5    # "where":Ljava/lang/String;
    .end local v11    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v14    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :cond_6
    const-string/jumbo v2, "name"

    sget-object v6, Lcom/vlingo/midas/util/PlayMusicType;->PLAYLIST:Lcom/vlingo/midas/util/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v2, v0, v1, v6}, Lcom/vlingo/midas/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 674
    .restart local v5    # "where":Ljava/lang/String;
    .restart local v11    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v14    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_0
    move-exception v10

    .line 675
    .local v10, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v10}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_1

    .line 676
    .end local v10    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v10

    .line 677
    .local v10, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v10}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_1

    .line 685
    .end local v10    # "e":Ljava/lang/IllegalAccessException;
    .restart local v3    # "playlistUri":Landroid/net/Uri;
    :cond_7
    :try_start_3
    const-string/jumbo v2, "content://com.samsung.musicplus/audio/playlists/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    goto/16 :goto_2

    .line 704
    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v8    # "count":I
    .restart local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v15    # "playlistId":Ljava/lang/Long;
    .restart local v16    # "playlistName":Ljava/lang/String;
    :cond_8
    :try_start_4
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    sget v6, Lcom/vlingo/midas/R$string;->playlists_quicklist_default_value:I

    invoke-virtual {v2, v6}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v17

    .restart local v17    # "playlistNameLocalized":Ljava/lang/String;
    goto :goto_3

    .line 707
    .end local v17    # "playlistNameLocalized":Ljava/lang/String;
    :cond_9
    move-object/from16 v17, v16

    .restart local v17    # "playlistNameLocalized":Ljava/lang/String;
    goto :goto_4

    .line 721
    .end local v8    # "count":I
    .end local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .end local v15    # "playlistId":Ljava/lang/Long;
    .end local v16    # "playlistName":Ljava/lang/String;
    .end local v17    # "playlistNameLocalized":Ljava/lang/String;
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_2
    move-exception v2

    .line 725
    :goto_6
    if-eqz v9, :cond_5

    .line 726
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 725
    :catchall_0
    move-exception v2

    :goto_7
    if-eqz v9, :cond_a

    .line 726
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v2

    .line 725
    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v8    # "count":I
    .restart local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object v12, v13

    .end local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_7

    .line 721
    .end local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_3
    move-exception v2

    move-object v12, v13

    .end local v13    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v12    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_6
.end method

.method public static getTitleList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleName"    # Ljava/lang/String;
    .param p2, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499
    const/4 v12, 0x0

    .line 500
    .local v12, "cursor":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 501
    .local v16, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v6, "album_id"

    aput-object v6, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v6, "artist"

    aput-object v6, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v6, "title"

    aput-object v6, v4, v2

    .line 503
    .local v4, "projection":[Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v6, ""

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 504
    const-string/jumbo v5, "is_music=1"

    .line 508
    .local v5, "where":Ljava/lang/String;
    :goto_0
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v14

    .line 509
    .local v14, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v18, 0x0

    .line 510
    .local v18, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v14, :cond_0

    .line 512
    :try_start_0
    invoke-virtual {v14}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v18, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 520
    :cond_0
    :goto_1
    const/4 v3, 0x0

    .line 521
    .local v3, "musicUri":Landroid/net/Uri;
    if-eqz v18, :cond_6

    .line 522
    :try_start_1
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v3

    .line 525
    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const-string/jumbo v7, "title_key"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 527
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .local v17, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    if-eqz v12, :cond_2

    :try_start_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 531
    const-string/jumbo v9, ""

    .line 532
    .local v9, "albumUri":Ljava/lang/String;
    if-eqz v18, :cond_7

    .line 533
    sget-object v2, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 536
    :goto_3
    const/4 v11, 0x0

    .line 538
    .local v11, "count":I
    :cond_1
    const-string/jumbo v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    .line 539
    .local v19, "songId":Ljava/lang/Long;
    const-string/jumbo v2, "album_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 540
    .local v8, "albumId":Ljava/lang/String;
    const-string/jumbo v2, "artist"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 541
    .local v10, "artist":Ljava/lang/String;
    const-string/jumbo v2, "title"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 542
    .local v20, "title":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    .line 549
    .local v15, "imageUri":Landroid/net/Uri;
    new-instance v2, Lcom/vlingo/midas/music/MusicDetails;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v2, v0, v1, v10, v15}, Lcom/vlingo/midas/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 550
    add-int/lit8 v11, v11, 0x1

    .line 551
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Lcom/vlingo/midas/music/SearchMusic;->SEARCH_MUSIC_MAX:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-lt v11, v2, :cond_1

    .line 557
    .end local v8    # "albumId":Ljava/lang/String;
    .end local v9    # "albumUri":Ljava/lang/String;
    .end local v10    # "artist":Ljava/lang/String;
    .end local v11    # "count":I
    .end local v15    # "imageUri":Landroid/net/Uri;
    .end local v19    # "songId":Ljava/lang/Long;
    .end local v20    # "title":Ljava/lang/String;
    :cond_2
    if-eqz v12, :cond_9

    .line 558
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    move-object/from16 v16, v17

    .line 561
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_3
    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 565
    :cond_4
    return-object v16

    .line 506
    .end local v3    # "musicUri":Landroid/net/Uri;
    .end local v5    # "where":Ljava/lang/String;
    .end local v14    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v18    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "title"

    sget-object v7, Lcom/vlingo/midas/util/PlayMusicType;->TITLE:Lcom/vlingo/midas/util/PlayMusicType;

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v6, v0, v1, v7}, Lcom/vlingo/midas/music/SearchMusic;->buildWhereExpression(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/midas/util/PlayMusicType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, " and "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "is_music"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "=1"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "where":Ljava/lang/String;
    goto/16 :goto_0

    .line 513
    .restart local v14    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v18    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_0
    move-exception v13

    .line 514
    .local v13, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v13}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_1

    .line 515
    .end local v13    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v13

    .line 516
    .local v13, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v13}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_1

    .line 524
    .end local v13    # "e":Ljava/lang/IllegalAccessException;
    .restart local v3    # "musicUri":Landroid/net/Uri;
    :cond_6
    :try_start_3
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 535
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v9    # "albumUri":Ljava/lang/String;
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_7
    :try_start_4
    const-string/jumbo v9, "content://media/external/audio/albumart/"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_3

    .line 553
    .end local v9    # "albumUri":Ljava/lang/String;
    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_2
    move-exception v2

    .line 557
    :goto_5
    if-eqz v12, :cond_3

    .line 558
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 557
    :catchall_0
    move-exception v2

    :goto_6
    if-eqz v12, :cond_8

    .line 558
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v2

    .line 557
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catchall_1
    move-exception v2

    move-object/from16 v16, v17

    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_6

    .line 553
    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_3
    move-exception v2

    move-object/from16 v16, v17

    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_5

    .end local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_9
    move-object/from16 v16, v17

    .end local v17    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v16    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_4
.end method

.method public static isAnyMusic(Landroid/content/Context;)Z
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 104
    const/4 v1, 0x5

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v3, v11

    const-string/jumbo v1, "album_id"

    aput-object v1, v3, v12

    const/4 v1, 0x2

    const-string/jumbo v5, "artist"

    aput-object v5, v3, v1

    const/4 v1, 0x3

    const-string/jumbo v5, "title"

    aput-object v5, v3, v1

    const/4 v1, 0x4

    const-string/jumbo v5, "album"

    aput-object v5, v3, v1

    .line 105
    .local v3, "projection":[Ljava/lang/String;
    const-string/jumbo v4, "is_music != 0 "

    .line 106
    .local v4, "selection":Ljava/lang/String;
    const/4 v7, 0x0

    .line 107
    .local v7, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v9

    .line 108
    .local v9, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v10, 0x0

    .line 109
    .local v10, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v9, :cond_0

    .line 111
    :try_start_0
    invoke-virtual {v9}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v10, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 119
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 120
    .local v2, "musicUri":Landroid/net/Uri;
    if-eqz v10, :cond_1

    .line 121
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v10, v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    .line 126
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v5, 0x0

    const-string/jumbo v6, "title_key"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v7

    .line 127
    if-nez v7, :cond_2

    move v1, v11

    .line 142
    :goto_2
    return v1

    .line 112
    .end local v2    # "musicUri":Landroid/net/Uri;
    :catch_0
    move-exception v8

    .line 113
    .local v8, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v8}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 114
    .end local v8    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v8

    .line 115
    .local v8, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v8}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 123
    .end local v8    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "musicUri":Landroid/net/Uri;
    :cond_1
    :try_start_2
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 129
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v1, v12, :cond_3

    .line 130
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move v1, v11

    .line 131
    goto :goto_2

    .line 133
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move v1, v12

    .line 134
    goto :goto_2

    .line 136
    :catch_2
    move-exception v8

    .line 139
    .local v8, "e":Ljava/lang/Exception;
    if-eqz v7, :cond_4

    .line 140
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    move v1, v11

    .line 142
    goto :goto_2
.end method

.method public static isPartialFavoriteMatch(Ljava/lang/String;)Z
    .locals 6
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x5

    .line 925
    const-string/jumbo v2, "CheckFavorites"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "isPartialFavoriteMatch(String "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 926
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v5, :cond_0

    .line 927
    const-string/jumbo v0, ""

    .line 928
    .local v0, "fullName":Ljava/lang/String;
    const-string/jumbo v2, "en-GB"

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 929
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->playlists_quicklist_default_value1:I

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 932
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v5, :cond_2

    .line 933
    invoke-virtual {p0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 937
    .end local v0    # "fullName":Ljava/lang/String;
    :cond_0
    :goto_1
    return v1

    .line 931
    .restart local v0    # "fullName":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->playlists_quicklist_default_value:I

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 935
    :cond_2
    invoke-virtual {p0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_1
.end method

.method public static populateMusicMappings(Landroid/content/Context;)V
    .locals 17
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 788
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSongNameList()Ljava/util/ArrayList;

    move-result-object v16

    .line 789
    .local v16, "songNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getAlbumNameList()Ljava/util/ArrayList;

    move-result-object v8

    .line 790
    .local v8, "albumNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getArtistNameList()Ljava/util/ArrayList;

    move-result-object v10

    .line 792
    .local v10, "artistNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v4, "artist"

    aput-object v4, v3, v1

    const/4 v1, 0x1

    const-string/jumbo v4, "title"

    aput-object v4, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v4, "album"

    aput-object v4, v3, v1

    .line 794
    .local v3, "projection":[Ljava/lang/String;
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v13

    .line 795
    .local v13, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v14, 0x0

    .line 796
    .local v14, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v13, :cond_0

    .line 798
    :try_start_0
    invoke-virtual {v13}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v14, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 806
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 807
    .local v2, "musicUri":Landroid/net/Uri;
    if-eqz v14, :cond_7

    .line 808
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v14, v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    .line 811
    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string/jumbo v6, "title_key"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 813
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_5

    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 818
    :cond_1
    const-string/jumbo v1, "title"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 819
    .local v15, "songName":Ljava/lang/String;
    const-string/jumbo v1, "album"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 820
    .local v7, "albumName":Ljava/lang/String;
    const-string/jumbo v1, "artist"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 822
    .local v9, "artistName":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 825
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 827
    :cond_2
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 830
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 832
    :cond_3
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 835
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 837
    :cond_4
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 843
    .end local v7    # "albumName":Ljava/lang/String;
    .end local v9    # "artistName":Ljava/lang/String;
    .end local v15    # "songName":Ljava/lang/String;
    :cond_5
    if-eqz v11, :cond_6

    .line 844
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 845
    const/4 v11, 0x0

    .line 849
    :cond_6
    invoke-static/range {p0 .. p0}, Lcom/vlingo/midas/music/SearchMusic;->populatePlaylistMappings(Landroid/content/Context;)V

    .line 850
    return-void

    .line 799
    .end local v2    # "musicUri":Landroid/net/Uri;
    .end local v11    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v12

    .line 800
    .local v12, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v12}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 801
    .end local v12    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v12

    .line 802
    .local v12, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v12}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 810
    .end local v12    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "musicUri":Landroid/net/Uri;
    :cond_7
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 843
    .restart local v11    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_8

    .line 844
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 845
    const/4 v11, 0x0

    :cond_8
    throw v1
.end method

.method private static populatePlaylistMappings(Landroid/content/Context;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 853
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getPlaylistNameList()Ljava/util/ArrayList;

    move-result-object v12

    .line 855
    .local v12, "playlistNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v5, "name"

    aput-object v5, v3, v1

    .line 856
    .local v3, "projection":[Ljava/lang/String;
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v9

    .line 857
    .local v9, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v10, 0x0

    .line 858
    .local v10, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v9, :cond_0

    .line 860
    :try_start_0
    invoke-virtual {v9}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v10, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 867
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 868
    .local v2, "playlistUri":Landroid/net/Uri;
    if-eqz v10, :cond_5

    .line 869
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v10, v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    .line 872
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v6, "name"

    move-object v5, v4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 874
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_3

    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 879
    :cond_1
    const-string/jumbo v1, "name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 881
    .local v11, "playlistName":Ljava/lang/String;
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 884
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 886
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 892
    .end local v11    # "playlistName":Ljava/lang/String;
    :cond_3
    if-eqz v7, :cond_4

    .line 893
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 894
    :cond_4
    const/4 v7, 0x0

    .line 896
    return-void

    .line 861
    .end local v2    # "playlistUri":Landroid/net/Uri;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 862
    .local v8, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v8}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 863
    .end local v8    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v8

    .line 864
    .local v8, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v8}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 871
    .end local v8    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "playlistUri":Landroid/net/Uri;
    :cond_5
    sget-object v2, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 892
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_6

    .line 893
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 894
    :cond_6
    const/4 v7, 0x0

    throw v1
.end method

.method private static searchSongForPlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 29
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "playlistName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 569
    const/16 v19, 0x0

    .line 570
    .local v19, "cursor":Landroid/database/Cursor;
    const/16 v23, 0x0

    .line 571
    .local v23, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    const-string/jumbo v4, "name"

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vlingo/core/internal/util/SQLExpressionUtil;->like(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 572
    .local v7, "where":Ljava/lang/String;
    const-string/jumbo v9, "LENGTH(name)"

    .line 573
    .local v9, "sortOrder":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v4}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v21

    .line 574
    .local v21, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/16 v25, 0x0

    .line 575
    .local v25, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v21, :cond_0

    .line 577
    :try_start_0
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object/from16 v25, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 585
    :cond_0
    :goto_0
    const/4 v5, 0x0

    .line 586
    .local v5, "playlistUri":Landroid/net/Uri;
    if-eqz v25, :cond_5

    .line 587
    :try_start_1
    sget-object v4, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->PLAYLIST:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v5

    .line 590
    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 600
    if-eqz v19, :cond_3

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 601
    const-string/jumbo v4, "_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    .line 607
    .local v26, "playlist_id":Ljava/lang/Long;
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 608
    const/4 v4, 0x5

    new-array v12, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v12, v4

    const/4 v4, 0x1

    const-string/jumbo v6, "artist"

    aput-object v6, v12, v4

    const/4 v4, 0x2

    const-string/jumbo v6, "title"

    aput-object v6, v12, v4

    const/4 v4, 0x3

    const-string/jumbo v6, "audio_id"

    aput-object v6, v12, v4

    const/4 v4, 0x4

    const-string/jumbo v6, "album_id"

    aput-object v6, v12, v4

    .line 613
    .local v12, "projection":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 614
    .local v11, "uriTest":Landroid/net/Uri;
    if-eqz v25, :cond_6

    .line 615
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    move-object/from16 v0, v25

    invoke-interface {v0, v13, v14}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicPlaylistUri(J)Landroid/net/Uri;

    move-result-object v11

    .line 620
    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v13, "is_music != 0 "

    const/4 v14, 0x0

    const-string/jumbo v15, "play_order"

    invoke-virtual/range {v10 .. v15}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 629
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 630
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .local v24, "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    if-eqz v19, :cond_2

    :try_start_2
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 631
    const-string/jumbo v17, ""

    .line 632
    .local v17, "albumUri":Ljava/lang/String;
    if-eqz v25, :cond_7

    .line 633
    sget-object v4, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v17

    .line 637
    :cond_1
    :goto_3
    const-string/jumbo v4, "audio_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    .line 638
    .local v27, "songId":Ljava/lang/Long;
    const-string/jumbo v4, "album_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 639
    .local v16, "albumId":Ljava/lang/String;
    const-string/jumbo v4, "artist"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 640
    .local v18, "artist":Ljava/lang/String;
    const-string/jumbo v4, "title"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 641
    .local v28, "title":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    .line 642
    .local v22, "imageUri":Landroid/net/Uri;
    new-instance v4, Lcom/vlingo/midas/music/MusicDetails;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v18

    move-object/from16 v3, v22

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/vlingo/midas/music/MusicDetails;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 649
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v4

    if-nez v4, :cond_1

    .end local v16    # "albumId":Ljava/lang/String;
    .end local v17    # "albumUri":Ljava/lang/String;
    .end local v18    # "artist":Ljava/lang/String;
    .end local v22    # "imageUri":Landroid/net/Uri;
    .end local v27    # "songId":Ljava/lang/Long;
    .end local v28    # "title":Ljava/lang/String;
    :cond_2
    move-object/from16 v23, v24

    .line 656
    .end local v11    # "uriTest":Landroid/net/Uri;
    .end local v12    # "projection":[Ljava/lang/String;
    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .end local v26    # "playlist_id":Ljava/lang/Long;
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_3
    if-eqz v19, :cond_4

    .line 657
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 660
    :cond_4
    :goto_4
    return-object v23

    .line 578
    .end local v5    # "playlistUri":Landroid/net/Uri;
    :catch_0
    move-exception v20

    .line 579
    .local v20, "e":Ljava/lang/InstantiationException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_0

    .line 580
    .end local v20    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v20

    .line 581
    .local v20, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_0

    .line 589
    .end local v20    # "e":Ljava/lang/IllegalAccessException;
    .restart local v5    # "playlistUri":Landroid/net/Uri;
    :cond_5
    :try_start_3
    const-string/jumbo v4, "content://com.samsung.musicplus/audio/playlists/"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_1

    .line 617
    .restart local v11    # "uriTest":Landroid/net/Uri;
    .restart local v12    # "projection":[Ljava/lang/String;
    .restart local v26    # "playlist_id":Ljava/lang/Long;
    :cond_6
    const-string/jumbo v4, "external"

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-static {v4, v13, v14}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v11

    goto/16 :goto_2

    .line 635
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v17    # "albumUri":Ljava/lang/String;
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :cond_7
    :try_start_4
    const-string/jumbo v17, "content://media/external/audio/albumart/"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_3

    .line 652
    .end local v11    # "uriTest":Landroid/net/Uri;
    .end local v12    # "projection":[Ljava/lang/String;
    .end local v17    # "albumUri":Ljava/lang/String;
    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .end local v26    # "playlist_id":Ljava/lang/Long;
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_2
    move-exception v4

    .line 656
    :goto_5
    if-eqz v19, :cond_4

    .line 657
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 656
    :catchall_0
    move-exception v4

    :goto_6
    if-eqz v19, :cond_8

    .line 657
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v4

    .line 656
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v11    # "uriTest":Landroid/net/Uri;
    .restart local v12    # "projection":[Ljava/lang/String;
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v26    # "playlist_id":Ljava/lang/Long;
    :catchall_1
    move-exception v4

    move-object/from16 v23, v24

    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_6

    .line 652
    .end local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    :catch_3
    move-exception v4

    move-object/from16 v23, v24

    .end local v24    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    .restart local v23    # "musicList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/music/MusicDetails;>;"
    goto :goto_5
.end method
