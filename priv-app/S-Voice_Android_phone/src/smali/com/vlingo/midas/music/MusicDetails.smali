.class public Lcom/vlingo/midas/music/MusicDetails;
.super Ljava/lang/Object;
.source "MusicDetails.java"


# instance fields
.field private final albumArtUri:Landroid/net/Uri;

.field private final artistName:Ljava/lang/String;

.field private final songId:Ljava/lang/Long;

.field private final songTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .param p1, "songId"    # Ljava/lang/Long;
    .param p2, "songTitle"    # Ljava/lang/String;
    .param p3, "artistName"    # Ljava/lang/String;
    .param p4, "albumArtUri"    # Landroid/net/Uri;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/vlingo/midas/music/MusicDetails;->songId:Ljava/lang/Long;

    .line 18
    iput-object p2, p0, Lcom/vlingo/midas/music/MusicDetails;->songTitle:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lcom/vlingo/midas/music/MusicDetails;->artistName:Ljava/lang/String;

    .line 20
    iput-object p4, p0, Lcom/vlingo/midas/music/MusicDetails;->albumArtUri:Landroid/net/Uri;

    .line 21
    return-void
.end method


# virtual methods
.method public getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/midas/music/MusicDetails;->artistName:Ljava/lang/String;

    return-object v0
.end method

.method public getMusicThumbnail()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vlingo/midas/music/MusicDetails;->albumArtUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getSongId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/midas/music/MusicDetails;->songId:Ljava/lang/Long;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/midas/music/MusicDetails;->songTitle:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Song ID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/music/MusicDetails;->songId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", Title : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/music/MusicDetails;->songTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", Artist : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/music/MusicDetails;->artistName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", Image Url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/music/MusicDetails;->albumArtUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
