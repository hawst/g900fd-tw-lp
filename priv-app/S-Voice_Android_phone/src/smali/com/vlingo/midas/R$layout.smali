.class public final Lcom/vlingo/midas/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final activity_main:I = 0x7f040000

.field public static final always_on_screen:I = 0x7f040001

.field public static final asr_http_timeout_layout:I = 0x7f040002

.field public static final cma_widget_weather_single_citi:I = 0x7f040003

.field public static final cma_widget_weather_todays_plus_weekly:I = 0x7f040004

.field public static final compose_wcis:I = 0x7f040005

.field public static final control_view:I = 0x7f040006

.field public static final control_view_ewys_help:I = 0x7f040007

.field public static final control_view_help:I = 0x7f040008

.field public static final custom_alert_dialog:I = 0x7f040009

.field public static final custom_command_recording:I = 0x7f04000a

.field public static final custom_setting_titlebar:I = 0x7f04000b

.field public static final custom_titlebar:I = 0x7f04000c

.field public static final dialog_bubble:I = 0x7f04000d

.field public static final dialog_bubble_user:I = 0x7f04000e

.field public static final dialog_bubble_user_s:I = 0x7f04000f

.field public static final dialog_bubble_vlingo:I = 0x7f040010

.field public static final dialog_bubble_vlingo_s:I = 0x7f040011

.field public static final dialog_bubble_wakeup:I = 0x7f040012

.field public static final dialog_bubble_wakeup_s:I = 0x7f040013

.field public static final dialog_custom_wake_up_end_body:I = 0x7f040014

.field public static final dialog_custom_wake_up_error_body:I = 0x7f040015

.field public static final dialog_custom_wake_up_record_body:I = 0x7f040016

.field public static final dialog_edittext:I = 0x7f040017

.field public static final dialog_view:I = 0x7f040018

.field public static final dialog_view_ewys_help:I = 0x7f040019

.field public static final dialog_view_help:I = 0x7f04001a

.field public static final dialog_voice_prompt_confirmation:I = 0x7f04001b

.field public static final disclaimer_fragment_layout:I = 0x7f04001c

.field public static final disclaimer_screen_k:I = 0x7f04001d

.field public static final driving_mode_custom_notification:I = 0x7f04001e

.field public static final erase_server_data_detail_layout:I = 0x7f04001f

.field public static final erase_server_data_layout:I = 0x7f040020

.field public static final fragment_container:I = 0x7f040021

.field public static final fragment_help_choice:I = 0x7f040022

.field public static final function_list_item:I = 0x7f040023

.field public static final function_list_item_help:I = 0x7f040024

.field public static final guide_ewys_step_1_land:I = 0x7f040025

.field public static final guide_ewys_step_1_port:I = 0x7f040026

.field public static final guide_ewys_step_2_land:I = 0x7f040027

.field public static final guide_ewys_step_2_port:I = 0x7f040028

.field public static final guide_ewys_step_3_land:I = 0x7f040029

.field public static final guide_ewys_step_3_port:I = 0x7f04002a

.field public static final guide_usv_step_2_land:I = 0x7f04002b

.field public static final guide_usv_step_2_port:I = 0x7f04002c

.field public static final headline_item:I = 0x7f04002d

.field public static final help_about:I = 0x7f04002e

.field public static final help_anim_switch:I = 0x7f04002f

.field public static final help_list_view:I = 0x7f040030

.field public static final httpsrollout_preference_layout:I = 0x7f040031

.field public static final introduction_screen_k:I = 0x7f040032

.field public static final item_alarm_choice:I = 0x7f040033

.field public static final item_application_choice:I = 0x7f040034

.field public static final item_contact_choice:I = 0x7f040035

.field public static final item_contact_detail:I = 0x7f040036

.field public static final item_drive_schedule_choice:I = 0x7f040037

.field public static final item_initial_introduction_k:I = 0x7f040038

.field public static final item_local_search:I = 0x7f040039

.field public static final item_local_search_detail:I = 0x7f04003a

.field public static final item_local_search_detail_review:I = 0x7f04003b

.field public static final item_memo_choice:I = 0x7f04003c

.field public static final item_message_details:I = 0x7f04003d

.field public static final item_movie_choice:I = 0x7f04003e

.field public static final item_naver_local_search:I = 0x7f04003f

.field public static final item_phone_type_choice:I = 0x7f040040

.field public static final item_schedule_choice:I = 0x7f040041

.field public static final item_smart_notification:I = 0x7f040042

.field public static final item_social_type:I = 0x7f040043

.field public static final item_you_can_say_kk_widget:I = 0x7f040044

.field public static final item_you_can_say_widget:I = 0x7f040045

.field public static final iux_btn_finish:I = 0x7f040046

.field public static final iux_btn_next:I = 0x7f040047

.field public static final iux_btn_prev:I = 0x7f040048

.field public static final iux_btn_skip:I = 0x7f040049

.field public static final land_control_view:I = 0x7f04004a

.field public static final land_control_view_ewys_help:I = 0x7f04004b

.field public static final land_control_view_help:I = 0x7f04004c

.field public static final left_container:I = 0x7f04004d

.field public static final main:I = 0x7f04004e

.field public static final main_land_control_view:I = 0x7f04004f

.field public static final main_land_control_view_ewys_help:I = 0x7f040050

.field public static final main_normal:I = 0x7f040051

.field public static final main_normal_control_view:I = 0x7f040052

.field public static final main_normal_control_view_ewys_help:I = 0x7f040053

.field public static final main_normal_ewys_help:I = 0x7f040054

.field public static final main_normal_help:I = 0x7f040055

.field public static final memo_tile_bg:I = 0x7f040056

.field public static final mic_animation_view:I = 0x7f040057

.field public static final music:I = 0x7f040058

.field public static final navigation_setting:I = 0x7f040059

.field public static final notice_popup_custom_view:I = 0x7f04005a

.field public static final preference_with_image:I = 0x7f04005b

.field public static final right_container:I = 0x7f04005c

.field public static final round_more_icon:I = 0x7f04005d

.field public static final seamless_main:I = 0x7f04005e

.field public static final server_preference_layout:I = 0x7f04005f

.field public static final settings_path_view:I = 0x7f040060

.field public static final settings_placeholder:I = 0x7f040061

.field public static final simple_list_item_2_image:I = 0x7f040062

.field public static final sview_cover_svoice_alwaysmicon:I = 0x7f040063

.field public static final svoice_title_k:I = 0x7f040064

.field public static final switch_tool_tip:I = 0x7f040065

.field public static final tos_dialog:I = 0x7f040066

.field public static final tutorial_btn_next:I = 0x7f040067

.field public static final tutorial_btn_prev:I = 0x7f040068

.field public static final tutorial_btn_start:I = 0x7f040069

.field public static final tutorial_screen_k:I = 0x7f04006a

.field public static final tutorial_screen_t:I = 0x7f04006b

.field public static final twopanes:I = 0x7f04006c

.field public static final ui_focus:I = 0x7f04006d

.field public static final voice_control:I = 0x7f04006e

.field public static final voice_control_listitems:I = 0x7f04006f

.field public static final voice_recording:I = 0x7f040070

.field public static final wakeup_secured:I = 0x7f040071

.field public static final wakeup_setting_dialog:I = 0x7f040072

.field public static final wakeup_two_list_item_help:I = 0x7f040073

.field public static final wakeup_twoline_list_item:I = 0x7f040074

.field public static final wakeup_wavefile:I = 0x7f040075

.field public static final wakeupcommand:I = 0x7f040076

.field public static final wakeupdialog:I = 0x7f040077

.field public static final wcis_info_row:I = 0x7f040078

.field public static final wcis_info_with_caption_row:I = 0x7f040079

.field public static final wcis_lyt:I = 0x7f04007a

.field public static final wcis_lyt_row:I = 0x7f04007b

.field public static final wcis_subheading_row:I = 0x7f04007c

.field public static final wcis_top:I = 0x7f04007d

.field public static final wcis_top_row:I = 0x7f04007e

.field public static final wcis_widget_row:I = 0x7f04007f

.field public static final welcome_fragment_layout:I = 0x7f040080

.field public static final welcome_page_for_svoice:I = 0x7f040081

.field public static final widget_address_book:I = 0x7f040082

.field public static final widget_alarm_choice:I = 0x7f040083

.field public static final widget_answer_question:I = 0x7f040084

.field public static final widget_application_choice:I = 0x7f040085

.field public static final widget_button:I = 0x7f040086

.field public static final widget_chatbot:I = 0x7f040087

.field public static final widget_clock:I = 0x7f040088

.field public static final widget_common_choice:I = 0x7f040089

.field public static final widget_compose_social_status:I = 0x7f04008a

.field public static final widget_contact_choice:I = 0x7f04008b

.field public static final widget_contact_detail:I = 0x7f04008c

.field public static final widget_email:I = 0x7f04008d

.field public static final widget_help:I = 0x7f04008e

.field public static final widget_help_choice:I = 0x7f04008f

.field public static final widget_language_choice:I = 0x7f040090

.field public static final widget_language_list_view:I = 0x7f040091

.field public static final widget_local_search:I = 0x7f040092

.field public static final widget_map:I = 0x7f040093

.field public static final widget_memo:I = 0x7f040094

.field public static final widget_memo_choice:I = 0x7f040095

.field public static final widget_message:I = 0x7f040096

.field public static final widget_messagereadback:I = 0x7f040097

.field public static final widget_messagereadbackbodyhidden:I = 0x7f040098

.field public static final widget_missing:I = 0x7f040099

.field public static final widget_more:I = 0x7f04009a

.field public static final widget_movie:I = 0x7f04009b

.field public static final widget_movie_choice:I = 0x7f04009c

.field public static final widget_multiple_message:I = 0x7f04009d

.field public static final widget_multiple_message_readout:I = 0x7f04009e

.field public static final widget_multiple_message_showunread:I = 0x7f04009f

.field public static final widget_multiple_sender_message_readout:I = 0x7f0400a0

.field public static final widget_music:I = 0x7f0400a1

.field public static final widget_music_bottom:I = 0x7f0400a2

.field public static final widget_music_choice:I = 0x7f0400a3

.field public static final widget_naver:I = 0x7f0400a4

.field public static final widget_naver_local:I = 0x7f0400a5

.field public static final widget_naver_local_choice:I = 0x7f0400a6

.field public static final widget_naver_region:I = 0x7f0400a7

.field public static final widget_normal_news:I = 0x7f0400a8

.field public static final widget_phone_type_select:I = 0x7f0400a9

.field public static final widget_schedule:I = 0x7f0400aa

.field public static final widget_schedule_body:I = 0x7f0400ab

.field public static final widget_schedule_choice:I = 0x7f0400ac

.field public static final widget_schedule_delete:I = 0x7f0400ad

.field public static final widget_schedule_edit:I = 0x7f0400ae

.field public static final widget_schedule_single:I = 0x7f0400af

.field public static final widget_smart:I = 0x7f0400b0

.field public static final widget_social_network_choice:I = 0x7f0400b1

.field public static final widget_task:I = 0x7f0400b2

.field public static final widget_task_choice:I = 0x7f0400b3

.field public static final widget_timer:I = 0x7f0400b4

.field public static final widget_true_knowledge:I = 0x7f0400b5

.field public static final widget_tutorial_activity_k:I = 0x7f0400b6

.field public static final widget_tutorial_activity_l:I = 0x7f0400b7

.field public static final widget_weather_single_citi:I = 0x7f0400b8

.field public static final widget_weather_todays_plus_weekly:I = 0x7f0400b9

.field public static final widget_web_browser:I = 0x7f0400ba

.field public static final widget_wolfram_alpha:I = 0x7f0400bb

.field public static final widget_you_can_say:I = 0x7f0400bc

.field public static final winset_popup:I = 0x7f0400bd


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
