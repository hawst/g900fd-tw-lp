.class Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MidasADMController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/MidasADMController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewCoverEventBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/MidasADMController;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/MidasADMController;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/MidasADMController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/MidasADMController;Lcom/vlingo/midas/MidasADMController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/MidasADMController;
    .param p2, "x1"    # Lcom/vlingo/midas/MidasADMController$1;

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;-><init>(Lcom/vlingo/midas/MidasADMController;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 185
    const-string/jumbo v1, "com.samsung.cover.OPEN"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    const-string/jumbo v1, "coverOpen"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 187
    .local v0, "coverOpened":Z
    if-eqz v0, :cond_1

    .line 188
    iget-object v1, p0, Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/MidasADMController;

    invoke-virtual {v1, v3}, Lcom/vlingo/midas/MidasADMController;->setCoverClosed(Z)V

    .line 192
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # invokes: Lcom/vlingo/midas/MidasADMController;->refreshFeatureStates()V
    invoke-static {v1}, Lcom/vlingo/midas/MidasADMController;->access$300(Lcom/vlingo/midas/MidasADMController;)V

    .line 194
    .end local v0    # "coverOpened":Z
    :cond_0
    return-void

    .line 190
    .restart local v0    # "coverOpened":Z
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/MidasADMController;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/MidasADMController;->setCoverClosed(Z)V

    goto :goto_0
.end method
