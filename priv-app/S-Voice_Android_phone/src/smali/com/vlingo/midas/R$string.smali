.class public final Lcom/vlingo/midas/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final MAPS_API_KEY:I = 0x7f0d01f6

.field public static final about_privacy_url:I = 0x7f0d01cd

.field public static final about_tos_url:I = 0x7f0d01cc

.field public static final about_voicetalk:I = 0x7f0d0211

.field public static final adapt_voice:I = 0x7f0d02e5

.field public static final advaced:I = 0x7f0d0432

.field public static final advaced_lower:I = 0x7f0d0433

.field public static final alarm:I = 0x7f0d03a4

.field public static final alarm_notification_summary:I = 0x7f0d03a5

.field public static final alert_no:I = 0x7f0d02f5

.field public static final alert_yes:I = 0x7f0d02f4

.field public static final all_data_on_the_server_erase:I = 0x7f0d049a

.field public static final all_data_on_the_server_erase_without_car_mode:I = 0x7f0d04a6

.field public static final all_day:I = 0x7f0d030c

.field public static final alternates_for_d:I = 0x7f0d0135

.field public static final alternates_for_g:I = 0x7f0d013a

.field public static final alternates_for_l:I = 0x7f0d0139

.field public static final alternates_for_r:I = 0x7f0d0136

.field public static final alternates_for_t:I = 0x7f0d0137

.field public static final alternates_for_z:I = 0x7f0d0138

.field public static final always_including_off:I = 0x7f0d04ac

.field public static final always_listening_bubble:I = 0x7f0d04d5

.field public static final always_listening_bubble_for_japanese:I = 0x7f0d04db

.field public static final always_listening_link:I = 0x7f0d04c3

.field public static final always_on_setting_desc:I = 0x7f0d04bf

.field public static final always_on_setting_title:I = 0x7f0d04be

.field public static final always_on_text:I = 0x7f0d04a8

.field public static final always_on_text_no_thanks:I = 0x7f0d04aa

.field public static final always_on_text_yes:I = 0x7f0d04a9

.field public static final am:I = 0x7f0d02e8

.field public static final am_day:I = 0x7f0d02ea

.field public static final am_morning:I = 0x7f0d02e9

.field public static final app_name:I = 0x7f0d0212

.field public static final app_name_title:I = 0x7f0d0458

.field public static final app_settings:I = 0x7f0d0213

.field public static final arrays_autodial_always:I = 0x7f0d0214

.field public static final arrays_autodial_confident:I = 0x7f0d0215

.field public static final arrays_autodial_never:I = 0x7f0d0216

.field public static final arrays_lang_de_de:I = 0x7f0d013b

.field public static final arrays_lang_en_uk:I = 0x7f0d013c

.field public static final arrays_lang_en_us:I = 0x7f0d013d

.field public static final arrays_lang_es_es:I = 0x7f0d013e

.field public static final arrays_lang_fr_fr:I = 0x7f0d0140

.field public static final arrays_lang_it_it:I = 0x7f0d0141

.field public static final arrays_lang_ja_jp:I = 0x7f0d0142

.field public static final arrays_lang_ko_kr:I = 0x7f0d0143

.field public static final arrays_lang_pr_br:I = 0x7f0d0146

.field public static final arrays_lang_ru_ru:I = 0x7f0d0145

.field public static final arrays_lang_v_es_la:I = 0x7f0d013f

.field public static final arrays_lang_zh_cn:I = 0x7f0d0144

.field public static final artist_info:I = 0x7f0d0493

.field public static final battery_level_at_tts:I = 0x7f0d045c

.field public static final bg_wakeup_notification_text:I = 0x7f0d038b

.field public static final bg_wakeup_notification_title:I = 0x7f0d038a

.field public static final birthday_greet:I = 0x7f0d03b1

.field public static final bvoice_ui_focus_text:I = 0x7f0d0412

.field public static final bvra_unsupported_toast:I = 0x7f0d03d0

.field public static final call:I = 0x7f0d0217

.field public static final callback_btn:I = 0x7f0d0218

.field public static final cancel:I = 0x7f0d0219

.field public static final cancel_btn:I = 0x7f0d021a

.field public static final car_iux_ok:I = 0x7f0d021b

.field public static final car_iux_oops:I = 0x7f0d021c

.field public static final car_spot_text:I = 0x7f0d0201

.field public static final car_spot_text_oneline:I = 0x7f0d020d

.field public static final car_tts_KEYWORD_SPOT_ON_DEMAND:I = 0x7f0d0208

.field public static final change_now:I = 0x7f0d021d

.field public static final character_max_length_reached:I = 0x7f0d041b

.field public static final chatbot_could_not_load_news:I = 0x7f0d03bc

.field public static final chatbot_first_news:I = 0x7f0d03b8

.field public static final chatbot_just_read_first_news:I = 0x7f0d03bd

.field public static final chatbot_just_read_last_news:I = 0x7f0d03be

.field public static final chatbot_next_news:I = 0x7f0d03b9

.field public static final chatbot_prev_news:I = 0x7f0d03bb

.field public static final chatbot_read_news:I = 0x7f0d03b7

.field public static final chatbot_repeat_news:I = 0x7f0d03ba

.field public static final chatbot_stop_news:I = 0x7f0d03bf

.field public static final checkEventTitleTimeDetail:I = 0x7f0d021e

.field public static final check_phone_event_main:I = 0x7f0d0353

.field public static final checked:I = 0x7f0d04d3

.field public static final checking_software_updates:I = 0x7f0d048d

.field public static final close_button:I = 0x7f0d0361

.field public static final comma:I = 0x7f0d015e

.field public static final contact_address_large_list:I = 0x7f0d04f2

.field public static final contact_address_one:I = 0x7f0d04f3

.field public static final contact_email_large_list:I = 0x7f0d04f0

.field public static final contact_email_one:I = 0x7f0d04f1

.field public static final contact_home_address_found_large_list:I = 0x7f0d04fa

.field public static final contact_home_address_found_one:I = 0x7f0d04fb

.field public static final contact_home_email_found_large_list:I = 0x7f0d04fe

.field public static final contact_home_email_found_one:I = 0x7f0d04ff

.field public static final contact_home_phone_found_large_list:I = 0x7f0d04f4

.field public static final contact_home_phone_found_one:I = 0x7f0d04f5

.field public static final contact_info:I = 0x7f0d0492

.field public static final contact_mobile_phone_found_large_list:I = 0x7f0d04f8

.field public static final contact_mobile_phone_found_one:I = 0x7f0d04f9

.field public static final contact_phone_number_large_list:I = 0x7f0d04ee

.field public static final contact_phone_number_one:I = 0x7f0d04ef

.field public static final contact_work_address_found_large_list:I = 0x7f0d04fc

.field public static final contact_work_address_found_one:I = 0x7f0d04fd

.field public static final contact_work_email_found_large_list:I = 0x7f0d0500

.field public static final contact_work_email_found_one:I = 0x7f0d0501

.field public static final contact_work_phone_found_large_list:I = 0x7f0d04f6

.field public static final contact_work_phone_found_one:I = 0x7f0d04f7

.field public static final core_address_book_is_empty:I = 0x7f0d00b3

.field public static final core_alarm_set:I = 0x7f0d0000

.field public static final core_alert_message:I = 0x7f0d0001

.field public static final core_and:I = 0x7f0d0105

.field public static final core_answer_prompt:I = 0x7f0d0002

.field public static final core_app_disambiguation_multiple_pages:I = 0x7f0d012e

.field public static final core_app_disambiguation_single_page:I = 0x7f0d012d

.field public static final core_bluetooth_setting_change_on_error:I = 0x7f0d00ca

.field public static final core_car_already_disable:I = 0x7f0d011f

.field public static final core_car_already_enable:I = 0x7f0d011d

.field public static final core_car_disable:I = 0x7f0d011e

.field public static final core_car_enable:I = 0x7f0d011c

.field public static final core_car_event_save_confirm:I = 0x7f0d0003

.field public static final core_car_event_saved:I = 0x7f0d0004

.field public static final core_car_redial_confirm_contact:I = 0x7f0d0106

.field public static final core_car_redial_confirm_number:I = 0x7f0d0107

.field public static final core_car_safereader_hidden_message_body:I = 0x7f0d0005

.field public static final core_car_sms_error_help_tts:I = 0x7f0d0006

.field public static final core_car_sms_error_sending:I = 0x7f0d0007

.field public static final core_car_sms_message_empty_tts:I = 0x7f0d0008

.field public static final core_car_sms_send_confirm:I = 0x7f0d0009

.field public static final core_car_sms_speak_msg_tts:I = 0x7f0d000a

.field public static final core_car_sms_text_who:I = 0x7f0d000b

.field public static final core_car_social_final_prompt:I = 0x7f0d000c

.field public static final core_car_social_prompt_ex1:I = 0x7f0d000d

.field public static final core_car_social_service_prompt:I = 0x7f0d000e

.field public static final core_car_social_status_prompt:I = 0x7f0d000f

.field public static final core_car_task_save_cancel_update_prompt:I = 0x7f0d0010

.field public static final core_car_tts_ALBUM_PROMPT_DEMAND:I = 0x7f0d0011

.field public static final core_car_tts_ARTIST_PROMPT_DEMAND:I = 0x7f0d0012

.field public static final core_car_tts_EVENT_SAY_TITLE:I = 0x7f0d0013

.field public static final core_car_tts_GOTO_URL:I = 0x7f0d0014

.field public static final core_car_tts_LAUNCHAPP_PROMPT_DEMAND:I = 0x7f0d0015

.field public static final core_car_tts_MAP_OF:I = 0x7f0d0016

.field public static final core_car_tts_MEMO_SAVED:I = 0x7f0d00ac

.field public static final core_car_tts_MUSIC_PROMPT_DEMAND:I = 0x7f0d0017

.field public static final core_car_tts_NAVIGATE_TO:I = 0x7f0d0018

.field public static final core_car_tts_NO_ALBUMMATCH_DEMAND:I = 0x7f0d0019

.field public static final core_car_tts_NO_ANYMATCH_DEMAND:I = 0x7f0d010b

.field public static final core_car_tts_NO_APPMATCH_DEMAND:I = 0x7f0d001a

.field public static final core_car_tts_NO_ARTISTMATCH_DEMAND:I = 0x7f0d001d

.field public static final core_car_tts_NO_MATCH_DEMAND_CALL:I = 0x7f0d001e

.field public static final core_car_tts_NO_MATCH_DEMAND_MESSAGE:I = 0x7f0d001f

.field public static final core_car_tts_NO_MUSICMATCH_DEMAND:I = 0x7f0d0020

.field public static final core_car_tts_NO_PLAYLISTMATCH_DEMAND:I = 0x7f0d0021

.field public static final core_car_tts_NO_SPOKEN_APPMATCH_DEMAND:I = 0x7f0d001c

.field public static final core_car_tts_NO_SPOKEN_APPMATCH_DEMAND_FOR_CAMERA_ONLY:I = 0x7f0d001b

.field public static final core_car_tts_NO_TITLEMATCH_DEMAND:I = 0x7f0d0022

.field public static final core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:I = 0x7f0d0023

.field public static final core_car_tts_SAFEREADER_MULTIPLE_NEW_MESSAGES:I = 0x7f0d0024

.field public static final core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO:I = 0x7f0d00e7

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN:I = 0x7f0d0027

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN_NOCALL:I = 0x7f0d00c5

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:I = 0x7f0d0028

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN_NOCALL:I = 0x7f0d00c6

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN:I = 0x7f0d0025

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SHOWN:I = 0x7f0d00c3

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SPOKEN:I = 0x7f0d00c4

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SPOKEN:I = 0x7f0d0026

.field public static final core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT:I = 0x7f0d0029

.field public static final core_car_tts_SAFEREADER_NEW_MSG_SHOWN:I = 0x7f0d002a

.field public static final core_car_tts_SAFEREADER_NEW_MSG_SPOKEN:I = 0x7f0d002b

.field public static final core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SHOWN:I = 0x7f0d00c7

.field public static final core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SPOKEN:I = 0x7f0d00c8

.field public static final core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:I = 0x7f0d002c

.field public static final core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SPOKEN:I = 0x7f0d002d

.field public static final core_car_tts_SAFEREADER_NEW_SMS_FROM:I = 0x7f0d002e

.field public static final core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:I = 0x7f0d002f

.field public static final core_car_tts_SAFEREADER_NO_REPLY:I = 0x7f0d00cb

.field public static final core_car_tts_SCHEDULE_EVENTS:I = 0x7f0d0030

.field public static final core_car_tts_SCHEDULE_NO_EVENTS:I = 0x7f0d0031

.field public static final core_car_tts_SMS_SENT_CONFIRM_DEMAND:I = 0x7f0d0032

.field public static final core_car_tts_TASK_CANCELLED:I = 0x7f0d0033

.field public static final core_car_tts_TASK_SAY_TITLE:I = 0x7f0d0034

.field public static final core_car_tts_TITLE_PROMPT_DEMAND:I = 0x7f0d0035

.field public static final core_car_tts_VD_CALLING_CONFIRM_DEMAND:I = 0x7f0d0036

.field public static final core_car_tts_VD_MULTIPLE_CONTACTS_DEMAND:I = 0x7f0d0037

.field public static final core_car_tts_VD_MULTIPLE_TYPES_DEMAND:I = 0x7f0d0038

.field public static final core_car_tts_WHICH_CONTACT_DEMAND:I = 0x7f0d0039

.field public static final core_car_util_loading:I = 0x7f0d003a

.field public static final core_checkEventJapaneseMoreTenYouHave:I = 0x7f0d003e

.field public static final core_checkEventOneYouHave:I = 0x7f0d003d

.field public static final core_checkEventRussianTwoThreeFourYouHave:I = 0x7f0d003c

.field public static final core_checkEventTitleTimeDetailBrief:I = 0x7f0d003b

.field public static final core_checkEventYouHave:I = 0x7f0d003f

.field public static final core_checkEventYouHaveOnDate:I = 0x7f0d0040

.field public static final core_checkEventYouHaveToday:I = 0x7f0d0041

.field public static final core_colon:I = 0x7f0d00b8

.field public static final core_comma:I = 0x7f0d00b6

.field public static final core_contact_address:I = 0x7f0d0111

.field public static final core_contact_address_not_found:I = 0x7f0d0044

.field public static final core_contact_birthday_not_found:I = 0x7f0d0043

.field public static final core_contact_disambiguation_multiple_pages:I = 0x7f0d0130

.field public static final core_contact_disambiguation_single_page:I = 0x7f0d012f

.field public static final core_contact_do_not_address:I = 0x7f0d004b

.field public static final core_contact_do_not_address_type:I = 0x7f0d004f

.field public static final core_contact_do_not_birthday:I = 0x7f0d004c

.field public static final core_contact_do_not_email:I = 0x7f0d004a

.field public static final core_contact_do_not_email_type:I = 0x7f0d004e

.field public static final core_contact_do_not_phone:I = 0x7f0d004d

.field public static final core_contact_do_not_phone_type:I = 0x7f0d0050

.field public static final core_contact_email:I = 0x7f0d0110

.field public static final core_contact_email_not_found:I = 0x7f0d0042

.field public static final core_contact_home_address_found:I = 0x7f0d0116

.field public static final core_contact_home_email_found:I = 0x7f0d0118

.field public static final core_contact_home_phone_found:I = 0x7f0d0113

.field public static final core_contact_mobile_phone_found:I = 0x7f0d0115

.field public static final core_contact_phone_number:I = 0x7f0d010f

.field public static final core_contact_phone_number_not_found:I = 0x7f0d0124

.field public static final core_contact_work_address_found:I = 0x7f0d0117

.field public static final core_contact_work_email_found:I = 0x7f0d0119

.field public static final core_contact_work_phone_found:I = 0x7f0d0114

.field public static final core_contacts_do_not_address:I = 0x7f0d0047

.field public static final core_contacts_do_not_birthday:I = 0x7f0d0048

.field public static final core_contacts_do_not_email:I = 0x7f0d0046

.field public static final core_contacts_do_not_phone:I = 0x7f0d0049

.field public static final core_contacts_no_match_openquote:I = 0x7f0d0045

.field public static final core_cradle_date:I = 0x7f0d0108

.field public static final core_current_location:I = 0x7f0d00c2

.field public static final core_default_alarm_title:I = 0x7f0d0051

.field public static final core_dot:I = 0x7f0d00b5

.field public static final core_ellipsis:I = 0x7f0d00ba

.field public static final core_error:I = 0x7f0d00d1

.field public static final core_format_time_AM:I = 0x7f0d00cf

.field public static final core_format_time_PM:I = 0x7f0d00d0

.field public static final core_local_search_blank_request_message_shown:I = 0x7f0d00b1

.field public static final core_local_search_blank_request_message_spoken:I = 0x7f0d00b2

.field public static final core_local_search_default_location:I = 0x7f0d0125

.field public static final core_localsearch:I = 0x7f0d0052

.field public static final core_localsearch_bad_location:I = 0x7f0d0128

.field public static final core_localsearch_bad_response:I = 0x7f0d0129

.field public static final core_localsearch_default_location:I = 0x7f0d010d

.field public static final core_localsearch_no_location:I = 0x7f0d0053

.field public static final core_localsearch_no_results:I = 0x7f0d012a

.field public static final core_localsearch_provider_dianping:I = 0x7f0d0054

.field public static final core_map_default_location:I = 0x7f0d0127

.field public static final core_memo_confirm_delete:I = 0x7f0d0055

.field public static final core_memo_disambiguation_multiple_pages:I = 0x7f0d012c

.field public static final core_memo_disambiguation_single_page:I = 0x7f0d012b

.field public static final core_memo_multiple_found:I = 0x7f0d0056

.field public static final core_memo_not_saved:I = 0x7f0d0057

.field public static final core_message_disambiguation_multiple_pages:I = 0x7f0d0132

.field public static final core_message_disambiguation_single_page:I = 0x7f0d0131

.field public static final core_message_none_from_sender:I = 0x7f0d00d4

.field public static final core_message_none_from_sender_spoken:I = 0x7f0d00d5

.field public static final core_message_reading_multi_from_sender:I = 0x7f0d00c1

.field public static final core_message_reading_none:I = 0x7f0d00bf

.field public static final core_message_reading_none_spoken:I = 0x7f0d00d3

.field public static final core_message_reading_preface:I = 0x7f0d00be

.field public static final core_message_reading_single_from_sender:I = 0x7f0d00c0

.field public static final core_message_sending:I = 0x7f0d00bc

.field public static final core_message_sending_readout:I = 0x7f0d00bd

.field public static final core_mic_in_use:I = 0x7f0d00ab

.field public static final core_multiple_applications:I = 0x7f0d0058

.field public static final core_multiple_contacts:I = 0x7f0d0059

.field public static final core_multiple_phone_numbers:I = 0x7f0d005a

.field public static final core_multiple_phone_numbers2:I = 0x7f0d005b

.field public static final core_music_play_music:I = 0x7f0d011a

.field public static final core_music_play_playlist:I = 0x7f0d011b

.field public static final core_nav_home_prompt_shown:I = 0x7f0d005c

.field public static final core_nav_home_prompt_spoken:I = 0x7f0d005d

.field public static final core_navigate_home:I = 0x7f0d005e

.field public static final core_navigation_default_location:I = 0x7f0d0126

.field public static final core_network_error:I = 0x7f0d00b4

.field public static final core_no_call_log:I = 0x7f0d005f

.field public static final core_no_memo_saved:I = 0x7f0d0060

.field public static final core_no_memo_saved_about:I = 0x7f0d0061

.field public static final core_no_more_messages_regular:I = 0x7f0d00d7

.field public static final core_no_more_messages_spoken:I = 0x7f0d00d8

.field public static final core_no_more_messages_verbose:I = 0x7f0d00d6

.field public static final core_not_detected_current_location:I = 0x7f0d00cd

.field public static final core_opening_app:I = 0x7f0d00bb

.field public static final core_permission_internet_error:I = 0x7f0d00d2

.field public static final core_phone_in_use:I = 0x7f0d00aa

.field public static final core_phone_type_home:I = 0x7f0d0120

.field public static final core_phone_type_mobile:I = 0x7f0d0121

.field public static final core_phone_type_other:I = 0x7f0d0122

.field public static final core_phone_type_work:I = 0x7f0d0123

.field public static final core_playing_music:I = 0x7f0d00cc

.field public static final core_qa_more:I = 0x7f0d0062

.field public static final core_qa_tts_NO_ANS_WEB_SEARCH:I = 0x7f0d0063

.field public static final core_qzone_dialog_title:I = 0x7f0d010a

.field public static final core_readout_multi_message_nocall_shown:I = 0x7f0d00ff

.field public static final core_readout_multi_message_nocall_spoken:I = 0x7f0d0100

.field public static final core_readout_multi_message_nocall_withoutaskingmsg_spoken:I = 0x7f0d0101

.field public static final core_readout_multi_message_overflow:I = 0x7f0d00eb

.field public static final core_readout_multi_message_shown:I = 0x7f0d00e8

.field public static final core_readout_multi_message_spoken:I = 0x7f0d00e9

.field public static final core_readout_multi_message_withoutaskingmsg_spoken:I = 0x7f0d00ea

.field public static final core_readout_multi_sender:I = 0x7f0d00ec

.field public static final core_readout_multi_sender_command_spoken:I = 0x7f0d00ef

.field public static final core_readout_multi_sender_dm_overflow:I = 0x7f0d00f0

.field public static final core_readout_multi_sender_info_verbose_overflow_spoken:I = 0x7f0d00ee

.field public static final core_readout_multi_sender_info_verbose_spoken:I = 0x7f0d00ed

.field public static final core_readout_multi_sender_overflow:I = 0x7f0d00f3

.field public static final core_readout_multi_sender_overflow_spoken:I = 0x7f0d00f1

.field public static final core_readout_multi_sender_overflow_withoutaskingmsg_spoken:I = 0x7f0d00f2

.field public static final core_readout_new_messages_for_russian_1:I = 0x7f0d0102

.field public static final core_readout_new_messages_for_russian_2:I = 0x7f0d0103

.field public static final core_readout_new_messages_for_russian_3:I = 0x7f0d0104

.field public static final core_readout_no_match_found_shown:I = 0x7f0d00d9

.field public static final core_readout_no_match_found_spoken:I = 0x7f0d00da

.field public static final core_readout_single_message:I = 0x7f0d00e4

.field public static final core_readout_single_message_initial_hidden:I = 0x7f0d00db

.field public static final core_readout_single_message_initial_hidden_nocall:I = 0x7f0d00f4

.field public static final core_readout_single_message_initial_hidden_nocall_spoken:I = 0x7f0d00f5

.field public static final core_readout_single_message_initial_hidden_nocall_withoutaskingmsg_spoken:I = 0x7f0d00f6

.field public static final core_readout_single_message_initial_hidden_spoken:I = 0x7f0d00dc

.field public static final core_readout_single_message_initial_hidden_withoutaskingmsg_spoken:I = 0x7f0d00dd

.field public static final core_readout_single_message_initial_nocall_spoken:I = 0x7f0d00f7

.field public static final core_readout_single_message_initial_nocall_withoutaskingmsg_spoken:I = 0x7f0d00f8

.field public static final core_readout_single_message_initial_shown:I = 0x7f0d00de

.field public static final core_readout_single_message_initial_spoken:I = 0x7f0d00df

.field public static final core_readout_single_message_initial_withoutaskingmsg_spoken:I = 0x7f0d00e0

.field public static final core_readout_single_message_next:I = 0x7f0d00e1

.field public static final core_readout_single_message_next_nocall:I = 0x7f0d00f9

.field public static final core_readout_single_message_next_nocall_spoken:I = 0x7f0d00fa

.field public static final core_readout_single_message_next_nocall_withoutaskingmsg_spoken:I = 0x7f0d00fb

.field public static final core_readout_single_message_next_spoken:I = 0x7f0d00e2

.field public static final core_readout_single_message_next_withoutaskingmsg_spoken:I = 0x7f0d00e3

.field public static final core_readout_single_message_nocall:I = 0x7f0d00fc

.field public static final core_readout_single_message_nocall_spoken:I = 0x7f0d00fd

.field public static final core_readout_single_message_nocall_withoutaskingmsg_spoken:I = 0x7f0d00fe

.field public static final core_readout_single_message_spoken:I = 0x7f0d00e5

.field public static final core_readout_single_message_withoutaskingmsg_spoken:I = 0x7f0d00e6

.field public static final core_redial:I = 0x7f0d0069

.field public static final core_safe_reader_default_error:I = 0x7f0d006a

.field public static final core_safereader_enabled:I = 0x7f0d006b

.field public static final core_safereader_new_sms_from:I = 0x7f0d006c

.field public static final core_safereader_notif_reading_ticker:I = 0x7f0d006d

.field public static final core_safereader_notif_reading_title:I = 0x7f0d006e

.field public static final core_safereader_notif_title:I = 0x7f0d006f

.field public static final core_safereader_subject:I = 0x7f0d0070

.field public static final core_say_command:I = 0x7f0d0071

.field public static final core_schedule_all_day:I = 0x7f0d0072

.field public static final core_schedule_to:I = 0x7f0d0073

.field public static final core_search_web_label_button:I = 0x7f0d0074

.field public static final core_semicolon:I = 0x7f0d00b9

.field public static final core_single_contact:I = 0x7f0d0075

.field public static final core_social_api_err_auth1:I = 0x7f0d0076

.field public static final core_social_api_err_auth2:I = 0x7f0d0077

.field public static final core_social_api_error:I = 0x7f0d0078

.field public static final core_social_api_qzone_err_login:I = 0x7f0d007f

.field public static final core_social_api_qzone_error:I = 0x7f0d0080

.field public static final core_social_api_qzone_update_error:I = 0x7f0d0081

.field public static final core_social_api_qzone_update_error_publish:I = 0x7f0d0082

.field public static final core_social_api_twitter_err_login:I = 0x7f0d0088

.field public static final core_social_api_twitter_error:I = 0x7f0d0089

.field public static final core_social_api_twitter_update_error:I = 0x7f0d008a

.field public static final core_social_api_weibo_err_login:I = 0x7f0d007a

.field public static final core_social_api_weibo_error:I = 0x7f0d0079

.field public static final core_social_api_weibo_update_error:I = 0x7f0d007b

.field public static final core_social_err_msg1:I = 0x7f0d008b

.field public static final core_social_err_msg2:I = 0x7f0d008c

.field public static final core_social_login_to_facebook_msg:I = 0x7f0d008d

.field public static final core_social_login_to_network_msg:I = 0x7f0d008e

.field public static final core_social_login_to_qzone_msg:I = 0x7f0d0083

.field public static final core_social_login_to_twitter_msg:I = 0x7f0d008f

.field public static final core_social_login_to_weibo_msg:I = 0x7f0d007c

.field public static final core_social_logout_facebook:I = 0x7f0d0090

.field public static final core_social_logout_facebook_msg:I = 0x7f0d0091

.field public static final core_social_logout_qzone:I = 0x7f0d0084

.field public static final core_social_logout_qzone_msg:I = 0x7f0d0087

.field public static final core_social_logout_twitter:I = 0x7f0d0092

.field public static final core_social_logout_twitter_msg:I = 0x7f0d0093

.field public static final core_social_logout_weibo:I = 0x7f0d007d

.field public static final core_social_logout_weibo_msg:I = 0x7f0d007e

.field public static final core_social_no_network:I = 0x7f0d0094

.field public static final core_social_no_status:I = 0x7f0d0095

.field public static final core_social_status_updated:I = 0x7f0d0096

.field public static final core_social_too_long:I = 0x7f0d0097

.field public static final core_social_update_failed:I = 0x7f0d0098

.field public static final core_space:I = 0x7f0d00b7

.field public static final core_time_at_present:I = 0x7f0d0112

.field public static final core_today:I = 0x7f0d00a3

.field public static final core_tomorrow:I = 0x7f0d00a4

.field public static final core_tts_NO_ANS_GOOGLE_NOW_SEARCH:I = 0x7f0d010e

.field public static final core_tts_NO_ANS_WEB_SEARCH:I = 0x7f0d0064

.field public static final core_tts_NO_ANS_WEB_SEARCH_1:I = 0x7f0d0065

.field public static final core_tts_NO_ANS_WEB_SEARCH_2:I = 0x7f0d0066

.field public static final core_tts_NO_ANS_WEB_SEARCH_3:I = 0x7f0d0067

.field public static final core_tts_NO_ANS_WEB_SEARCH_4:I = 0x7f0d0068

.field public static final core_tts_local_fallback_engine_name:I = 0x7f0d014a

.field public static final core_tts_local_required_engine_name:I = 0x7f0d0149

.field public static final core_unknown:I = 0x7f0d00ce

.field public static final core_voice_recognition_service_not_thru_iux_error:I = 0x7f0d0099

.field public static final core_voicedial_call_name:I = 0x7f0d009a

.field public static final core_voicedial_call_name_type:I = 0x7f0d009b

.field public static final core_voicevideodial_call_name:I = 0x7f0d009c

.field public static final core_voicevideodial_call_name_type:I = 0x7f0d009d

.field public static final core_wcis_social_facebook:I = 0x7f0d009e

.field public static final core_wcis_social_qzone:I = 0x7f0d0086

.field public static final core_wcis_social_twitter:I = 0x7f0d009f

.field public static final core_wcis_social_weibo:I = 0x7f0d0085

.field public static final core_weather_current:I = 0x7f0d00a0

.field public static final core_weather_date_display:I = 0x7f0d00a1

.field public static final core_weather_date_tts:I = 0x7f0d00a2

.field public static final core_weather_default_location:I = 0x7f0d010c

.field public static final core_weather_general:I = 0x7f0d00a5

.field public static final core_weather_no_results:I = 0x7f0d00a6

.field public static final core_weather_plus_seven:I = 0x7f0d00a7

.field public static final core_weather_with_locatin:I = 0x7f0d00a8

.field public static final core_weibo_dialog_title:I = 0x7f0d0109

.field public static final core_what_is_the_weather_date_var:I = 0x7f0d00b0

.field public static final core_what_is_the_weather_date_var_location_var:I = 0x7f0d00ad

.field public static final core_what_is_the_weather_today:I = 0x7f0d00ae

.field public static final core_what_is_the_weather_today_location_var:I = 0x7f0d00af

.field public static final core_who_would_you_like_to_call:I = 0x7f0d00a9

.field public static final core_wifi_setting_change_on_error:I = 0x7f0d00c9

.field public static final cradle_home_date:I = 0x7f0d02eb

.field public static final cradle_month:I = 0x7f0d0343

.field public static final cradle_year:I = 0x7f0d0342

.field public static final custom_recording_current_status_for_gsa:I = 0x7f0d04d8

.field public static final custom_recording_desc_before_tap_mic_for_gsa:I = 0x7f0d04d7

.field public static final custom_recording_success_desc_for_gsa:I = 0x7f0d04d9

.field public static final custom_wakeup_command:I = 0x7f0d04c0

.field public static final custom_wakeup_disabled:I = 0x7f0d033f

.field public static final custom_wakeup_enabled:I = 0x7f0d033e

.field public static final debug_settings_show_enabled:I = 0x7f0d0373

.field public static final delete:I = 0x7f0d021f

.field public static final destination:I = 0x7f0d038d

.field public static final destination_hint:I = 0x7f0d038f

.field public static final device_id:I = 0x7f0d03cd

.field public static final did_you_know:I = 0x7f0d0220

.field public static final did_you_know_driving_mode:I = 0x7f0d0221

.field public static final did_you_know_driving_mode_no_call:I = 0x7f0d0222

.field public static final did_you_know_local_listings:I = 0x7f0d031f

.field public static final did_you_know_message:I = 0x7f0d0223

.field public static final did_you_know_movies:I = 0x7f0d0200

.field public static final did_you_know_music:I = 0x7f0d01dc

.field public static final did_you_know_music_in_car:I = 0x7f0d01dd

.field public static final did_you_know_navigate:I = 0x7f0d0224

.field public static final did_you_know_news:I = 0x7f0d01e1

.field public static final disable_voice_prompt:I = 0x7f0d042f

.field public static final do_not_display_again:I = 0x7f0d02f0

.field public static final done:I = 0x7f0d01c6

.field public static final download_btn:I = 0x7f0d04bc

.field public static final download_language_data_desc:I = 0x7f0d04b8

.field public static final download_language_data_title:I = 0x7f0d04b7

.field public static final downloadable_dialog_popup_text:I = 0x7f0d041f

.field public static final downloadable_dialog_popup_text_for_chinese:I = 0x7f0d0420

.field public static final drive_greeting_processing:I = 0x7f0d03b5

.field public static final drive_help_greeting_call_land:I = 0x7f0d01de

.field public static final drive_help_greeting_message_land:I = 0x7f0d01df

.field public static final drive_help_greeting_navi_land:I = 0x7f0d01e0

.field public static final drivine_mode_news_updated:I = 0x7f0d03c3

.field public static final driving_call_reject_message:I = 0x7f0d03ac

.field public static final driving_help_wakeup_custom_tts:I = 0x7f0d03d3

.field public static final driving_help_wakeup_default_tts:I = 0x7f0d03d2

.field public static final driving_listening_under_text:I = 0x7f0d03de

.field public static final driving_mode_alert_notification_body:I = 0x7f0d03e0

.field public static final driving_mode_already_disabled:I = 0x7f0d0380

.field public static final driving_mode_already_disabled_tts:I = 0x7f0d0381

.field public static final driving_mode_already_enabled:I = 0x7f0d037e

.field public static final driving_mode_already_enabled_tts:I = 0x7f0d037f

.field public static final driving_mode_description:I = 0x7f0d039b

.field public static final driving_mode_message_body_hidden:I = 0x7f0d03c7

.field public static final driving_mode_news_headlines:I = 0x7f0d03c2

.field public static final driving_mode_notification_bar_summary:I = 0x7f0d03b6

.field public static final driving_mode_off_root_display:I = 0x7f0d03e4

.field public static final driving_mode_off_root_tts:I = 0x7f0d03e5

.field public static final driving_mode_off_suffix_display:I = 0x7f0d03e8

.field public static final driving_mode_off_suffix_tts:I = 0x7f0d03e9

.field public static final driving_mode_on_root_display:I = 0x7f0d03e6

.field public static final driving_mode_on_root_tts:I = 0x7f0d03e7

.field public static final driving_mode_on_suffix_display:I = 0x7f0d03ea

.field public static final driving_mode_on_suffix_tts:I = 0x7f0d03eb

.field public static final driving_mode_settings_title:I = 0x7f0d0399

.field public static final driving_mode_title:I = 0x7f0d039a

.field public static final driving_tap_the_screen:I = 0x7f0d03ce

.field public static final dueto:I = 0x7f0d0225

.field public static final email_type_home:I = 0x7f0d0202

.field public static final emergency_location_not_detected:I = 0x7f0d0438

.field public static final emergency_phone_not_found:I = 0x7f0d0437

.field public static final enable_voice_prompt:I = 0x7f0d0430

.field public static final enavi_unable_find_location:I = 0x7f0d04bd

.field public static final erase_server_data_abb:I = 0x7f0d0499

.field public static final erase_server_data_body:I = 0x7f0d0496

.field public static final erase_server_data_body_zero:I = 0x7f0d04e7

.field public static final erase_server_data_confirm:I = 0x7f0d0498

.field public static final erase_server_data_summary:I = 0x7f0d049e

.field public static final erase_server_data_summary_zero:I = 0x7f0d04e8

.field public static final erase_server_data_title:I = 0x7f0d049d

.field public static final erase_server_data_title_without_car_mode:I = 0x7f0d04a5

.field public static final erased_data:I = 0x7f0d04a4

.field public static final erasing_data:I = 0x7f0d04a3

.field public static final example_commands:I = 0x7f0d0486

.field public static final examples:I = 0x7f0d0227

.field public static final expand:I = 0x7f0d04d6

.field public static final facebooksso_google_account:I = 0x7f0d0392

.field public static final failed_to_erase_data:I = 0x7f0d04b4

.field public static final featured:I = 0x7f0d0228

.field public static final find_a_quieter_area_and_try_1_more_time:I = 0x7f0d04b6

.field public static final find_route:I = 0x7f0d034f

.field public static final finish:I = 0x7f0d0229

.field public static final force_close:I = 0x7f0d036b

.field public static final fri1:I = 0x7f0d031d

.field public static final full_fir:I = 0x7f0d042d

.field public static final full_mon:I = 0x7f0d0429

.field public static final full_sat:I = 0x7f0d042e

.field public static final full_sun:I = 0x7f0d0428

.field public static final full_thu:I = 0x7f0d042c

.field public static final full_tue:I = 0x7f0d042a

.field public static final full_wed:I = 0x7f0d042b

.field public static final function_item_check_schedule:I = 0x7f0d0315

.field public static final greeting_tts_checking_1:I = 0x7f0d03df

.field public static final greeting_tts_checking_2:I = 0x7f0d03e1

.field public static final greeting_tts_dawn_1:I = 0x7f0d03fc

.field public static final greeting_tts_daytime_1:I = 0x7f0d03ff

.field public static final greeting_tts_default:I = 0x7f0d03fb

.field public static final greeting_tts_evening_1:I = 0x7f0d0400

.field public static final greeting_tts_evening_2:I = 0x7f0d0401

.field public static final greeting_tts_help_1:I = 0x7f0d03d1

.field public static final greeting_tts_morning_1:I = 0x7f0d03fd

.field public static final greeting_tts_morning_2:I = 0x7f0d03fe

.field public static final greeting_tts_night_1:I = 0x7f0d0402

.field public static final greeting_tts_night_2:I = 0x7f0d0403

.field public static final greeting_tts_others_1:I = 0x7f0d040b

.field public static final greeting_tts_others_2:I = 0x7f0d040c

.field public static final greeting_tts_week_last_1:I = 0x7f0d0406

.field public static final greeting_tts_week_last_2:I = 0x7f0d0407

.field public static final greeting_tts_week_last_3:I = 0x7f0d0408

.field public static final greeting_tts_week_start_1:I = 0x7f0d0404

.field public static final greeting_tts_week_start_2:I = 0x7f0d0405

.field public static final greeting_tts_weekends_1:I = 0x7f0d0409

.field public static final greeting_tts_weekends_2:I = 0x7f0d040a

.field public static final guide_ewys_memo_content:I = 0x7f0d01e6

.field public static final guide_ewys_memo_prompt:I = 0x7f0d01e5

.field public static final hands_free_info_body:I = 0x7f0d041a

.field public static final hands_free_info_title:I = 0x7f0d0419

.field public static final handsfree_mode_popup_enable:I = 0x7f0d0418

.field public static final handsfree_mode_securelock_popup_message:I = 0x7f0d0417

.field public static final handsfree_mode_voicewakeup_popup_message:I = 0x7f0d0416

.field public static final handwriting:I = 0x7f0d0341

.field public static final handwriting_mode_off:I = 0x7f0d0346

.field public static final handwriting_mode_on:I = 0x7f0d0345

.field public static final help:I = 0x7f0d022a

.field public static final help_about_copyright:I = 0x7f0d022b

.field public static final help_about_privacy:I = 0x7f0d022c

.field public static final help_about_terms:I = 0x7f0d022d

.field public static final help_about_vlingo:I = 0x7f0d022e

.field public static final help_about_wakeup_powered_by:I = 0x7f0d022f

.field public static final help_call_driving:I = 0x7f0d01d0

.field public static final help_invalid_action:I = 0x7f0d03f4

.field public static final help_memo_driving:I = 0x7f0d01d4

.field public static final help_menu7:I = 0x7f0d0230

.field public static final help_menu8:I = 0x7f0d0231

.field public static final help_music_driving:I = 0x7f0d01ce

.field public static final help_navigate_driving:I = 0x7f0d01d1

.field public static final help_news_driving:I = 0x7f0d01d5

.field public static final help_schedule_driving:I = 0x7f0d01d3

.field public static final help_text_driving:I = 0x7f0d01d2

.field public static final help_tooltip_available_commands_tap:I = 0x7f0d0421

.field public static final help_tooltip_edit:I = 0x7f0d03f0

.field public static final help_tooltip_help_button:I = 0x7f0d03ed

.field public static final help_tooltip_keypad:I = 0x7f0d03f1

.field public static final help_tooltip_mic:I = 0x7f0d03ec

.field public static final help_tooltip_question_tap:I = 0x7f0d041e

.field public static final help_tooltip_voice_tap:I = 0x7f0d03ee

.field public static final help_tooltip_wakeup_unlock:I = 0x7f0d03ef

.field public static final help_version:I = 0x7f0d0232

.field public static final help_wcis_check_weather:I = 0x7f0d01c1

.field public static final help_wcis_check_weather_sub1:I = 0x7f0d01c2

.field public static final help_wcis_dial:I = 0x7f0d0233

.field public static final help_wcis_dial_sub1:I = 0x7f0d017d

.field public static final help_wcis_driving_mode:I = 0x7f0d017e

.field public static final help_wcis_driving_mode_sub1:I = 0x7f0d017f

.field public static final help_wcis_findcontact:I = 0x7f0d0191

.field public static final help_wcis_findcontact_sub1:I = 0x7f0d0192

.field public static final help_wcis_get_an_answer:I = 0x7f0d01c3

.field public static final help_wcis_get_an_answer_sub1:I = 0x7f0d01c4

.field public static final help_wcis_local_listings:I = 0x7f0d01c8

.field public static final help_wcis_local_listings_naver:I = 0x7f0d01ff

.field public static final help_wcis_local_listings_sub1:I = 0x7f0d01c9

.field public static final help_wcis_local_listings_sub1_naver:I = 0x7f0d01fb

.field public static final help_wcis_memo:I = 0x7f0d0180

.field public static final help_wcis_memo_sub1:I = 0x7f0d0181

.field public static final help_wcis_movie:I = 0x7f0d0234

.field public static final help_wcis_movie_sub1_naver:I = 0x7f0d01f8

.field public static final help_wcis_music:I = 0x7f0d0182

.field public static final help_wcis_music_sub1:I = 0x7f0d0183

.field public static final help_wcis_nav:I = 0x7f0d0184

.field public static final help_wcis_nav_sub1:I = 0x7f0d0185

.field public static final help_wcis_news:I = 0x7f0d01d8

.field public static final help_wcis_news_sub1:I = 0x7f0d01d9

.field public static final help_wcis_open_app:I = 0x7f0d0186

.field public static final help_wcis_open_app_sub1:I = 0x7f0d0187

.field public static final help_wcis_open_applications:I = 0x7f0d0235

.field public static final help_wcis_recordvoice:I = 0x7f0d0188

.field public static final help_wcis_recordvoice_sub1:I = 0x7f0d0189

.field public static final help_wcis_schedule:I = 0x7f0d0236

.field public static final help_wcis_schedule_lookup_sub1:I = 0x7f0d018b

.field public static final help_wcis_schedule_sub1:I = 0x7f0d018a

.field public static final help_wcis_search:I = 0x7f0d0237

.field public static final help_wcis_search_sub1:I = 0x7f0d018c

.field public static final help_wcis_set_an_alarm:I = 0x7f0d01bc

.field public static final help_wcis_set_an_alarm_lookup_sub1:I = 0x7f0d01be

.field public static final help_wcis_set_an_alarm_sub1:I = 0x7f0d01bd

.field public static final help_wcis_simple_setting_controls_sub1:I = 0x7f0d01c5

.field public static final help_wcis_simple_setting_controls_sub1_for_chinese:I = 0x7f0d01e4

.field public static final help_wcis_social_update:I = 0x7f0d018d

.field public static final help_wcis_social_update_sub1:I = 0x7f0d018e

.field public static final help_wcis_social_update_sub1_cn:I = 0x7f0d018f

.field public static final help_wcis_spoken_prompt:I = 0x7f0d01cf

.field public static final help_wcis_start_timer:I = 0x7f0d01bf

.field public static final help_wcis_start_timer_sub1:I = 0x7f0d01c0

.field public static final help_wcis_task:I = 0x7f0d0193

.field public static final help_wcis_task_lookup_sub1:I = 0x7f0d0195

.field public static final help_wcis_task_sub1:I = 0x7f0d0194

.field public static final help_wcis_text_message:I = 0x7f0d0238

.field public static final help_wcis_text_message_sub1:I = 0x7f0d0190

.field public static final help_wcis_world_clock:I = 0x7f0d01e7

.field public static final help_wcis_world_clock_sub1:I = 0x7f0d01e8

.field public static final help_weather_driving:I = 0x7f0d01d6

.field public static final highest:I = 0x7f0d0484

.field public static final incar_greeting_hi_galaxy:I = 0x7f0d020a

.field public static final incar_greeting_wakeup:I = 0x7f0d020b

.field public static final incoming_call:I = 0x7f0d039c

.field public static final incoming_call_notification_summary:I = 0x7f0d039d

.field public static final items_were_found_for_disambiguation:I = 0x7f0d04b1

.field public static final items_were_found_for_disambiguation_large_list:I = 0x7f0d04b2

.field public static final iux_wake_up_1_not_voicewakeup:I = 0x7f0d020c

.field public static final latest_updates_have_already_been_installed:I = 0x7f0d048b

.field public static final lcd_on:I = 0x7f0d04ad

.field public static final line1:I = 0x7f0d0239

.field public static final line2:I = 0x7f0d023a

.field public static final localsearch_details:I = 0x7f0d023b

.field public static final localsearch_powered_by:I = 0x7f0d023c

.field public static final localsearch_reviews:I = 0x7f0d023d

.field public static final location:I = 0x7f0d023e

.field public static final lowest:I = 0x7f0d0485

.field public static final ls_listing_detail_activity_reserve:I = 0x7f0d023f

.field public static final map:I = 0x7f0d0240

.field public static final memo_delete_confirmation:I = 0x7f0d0241

.field public static final memo_delete_fail:I = 0x7f0d0242

.field public static final memo_multiple_found_large_list:I = 0x7f0d04e9

.field public static final memo_multiple_found_new_loockup:I = 0x7f0d04ea

.field public static final memo_multiple_found_new_loockup_large_list:I = 0x7f0d04eb

.field public static final memo_multiple_found_new_loockup_one:I = 0x7f0d04ec

.field public static final menu_custom_wake_up:I = 0x7f0d0243

.field public static final menu_driving_mode:I = 0x7f0d0244

.field public static final menu_driving_mode_off:I = 0x7f0d0245

.field public static final menu_driving_mode_on:I = 0x7f0d0246

.field public static final menu_setting:I = 0x7f0d0247

.field public static final menu_use_external_speaker:I = 0x7f0d0248

.field public static final menu_use_internal_speaker:I = 0x7f0d0249

.field public static final merry_christmas_greet:I = 0x7f0d03b0

.field public static final merry_christmas_title:I = 0x7f0d03af

.field public static final message:I = 0x7f0d039e

.field public static final message_notification_summary:I = 0x7f0d039f

.field public static final mic_button:I = 0x7f0d035d

.field public static final mic_processing:I = 0x7f0d043c

.field public static final mic_speak_now:I = 0x7f0d043b

.field public static final mic_stanby:I = 0x7f0d043a

.field public static final midas_greeting:I = 0x7f0d024a

.field public static final mon1:I = 0x7f0d0319

.field public static final mood_music_notification:I = 0x7f0d03c4

.field public static final more_btn:I = 0x7f0d0350

.field public static final more_options:I = 0x7f0d0460

.field public static final movie_actors:I = 0x7f0d0347

.field public static final movie_booking:I = 0x7f0d0348

.field public static final movie_directors:I = 0x7f0d0349

.field public static final movie_grade:I = 0x7f0d034a

.field public static final movie_grade_12_over:I = 0x7f0d0359

.field public static final movie_grade_15_over:I = 0x7f0d035a

.field public static final movie_grade_18_over:I = 0x7f0d035b

.field public static final movie_grade_all:I = 0x7f0d035c

.field public static final movie_more_info:I = 0x7f0d034b

.field public static final movie_now_showing:I = 0x7f0d0352

.field public static final movie_same_name:I = 0x7f0d034c

.field public static final movie_soon:I = 0x7f0d0351

.field public static final msg_from:I = 0x7f0d024b

.field public static final msg_sent_fail:I = 0x7f0d0372

.field public static final msg_sent_successful:I = 0x7f0d0371

.field public static final msg_to:I = 0x7f0d024c

.field public static final multiple_contacts_large_list:I = 0x7f0d04ed

.field public static final music_album_info:I = 0x7f0d0494

.field public static final music_disambiguation_multiple_pages:I = 0x7f0d04e0

.field public static final music_disambiguation_single_page:I = 0x7f0d04df

.field public static final music_playing_calm:I = 0x7f0d03d7

.field public static final music_playing_calm_spoken:I = 0x7f0d03db

.field public static final music_playing_exciting:I = 0x7f0d03d5

.field public static final music_playing_exciting_spoken:I = 0x7f0d03d9

.field public static final music_playing_joyful:I = 0x7f0d03d6

.field public static final music_playing_joyful_spoken:I = 0x7f0d03da

.field public static final music_playing_passionate:I = 0x7f0d03d8

.field public static final music_playing_passionate_spoken:I = 0x7f0d03dc

.field public static final my_event:I = 0x7f0d0483

.field public static final my_place_for_driving_mode:I = 0x7f0d03b2

.field public static final my_place_summary_for_driving_mode:I = 0x7f0d03b4

.field public static final myplace_alert_summary:I = 0x7f0d03c1

.field public static final myplace_alert_summary_for_chinese:I = 0x7f0d03f3

.field public static final myplace_connect_to_vehicle:I = 0x7f0d03d4

.field public static final myplace_title:I = 0x7f0d03b3

.field public static final naver_connection_failure:I = 0x7f0d0344

.field public static final naver_local_near_suffix:I = 0x7f0d01f7

.field public static final navigate:I = 0x7f0d024d

.field public static final navigate_office:I = 0x7f0d0504

.field public static final navigate_office_fail:I = 0x7f0d0506

.field public static final navigate_office_notsupport:I = 0x7f0d0505

.field public static final navigate_recentdest:I = 0x7f0d0507

.field public static final navigate_recentdest_notsupport:I = 0x7f0d0508

.field public static final new_address:I = 0x7f0d034d

.field public static final new_email:I = 0x7f0d03a0

.field public static final new_email_notification_summary:I = 0x7f0d03a1

.field public static final new_voicemail:I = 0x7f0d03a2

.field public static final new_voicemail_notification_summary:I = 0x7f0d03a3

.field public static final new_year_greet:I = 0x7f0d03ae

.field public static final new_year_title:I = 0x7f0d03ad

.field public static final news_chatbot_with_total:I = 0x7f0d04e1

.field public static final news_test_volume_down:I = 0x7f0d0427

.field public static final news_test_volume_up:I = 0x7f0d0426

.field public static final newscp_flipboard:I = 0x7f0d03c8

.field public static final newscp_noflipboard:I = 0x7f0d03ca

.field public static final newscp_yonhap:I = 0x7f0d03c9

.field public static final next:I = 0x7f0d024e

.field public static final no_application:I = 0x7f0d04a2

.field public static final no_network:I = 0x7f0d024f

.field public static final no_text:I = 0x7f0d0321

.field public static final no_title:I = 0x7f0d0320

.field public static final not_have_the_song:I = 0x7f0d0329

.field public static final object_is_null:I = 0x7f0d0370

.field public static final off:I = 0x7f0d0481

.field public static final ok_uc:I = 0x7f0d0250

.field public static final on:I = 0x7f0d0480

.field public static final only_is_s_voice:I = 0x7f0d04ae

.field public static final participants:I = 0x7f0d0251

.field public static final personal_briefing:I = 0x7f0d045d

.field public static final personal_briefing_title_birthday:I = 0x7f0d04ce

.field public static final personal_briefing_title_schedule:I = 0x7f0d04cf

.field public static final phone_in_use:I = 0x7f0d0369

.field public static final phone_type_home:I = 0x7f0d0203

.field public static final phone_type_mobile:I = 0x7f0d0204

.field public static final phone_type_other:I = 0x7f0d0205

.field public static final phone_type_work:I = 0x7f0d0206

.field public static final phrasespotter_SEARCH_GRAMMAR_RES_NAME:I = 0x7f0d014b

.field public static final playlists:I = 0x7f0d049c

.field public static final playlists_quicklist_default_value:I = 0x7f0d0323

.field public static final playlists_quicklist_default_value1:I = 0x7f0d0488

.field public static final pm:I = 0x7f0d02ec

.field public static final pm_evening:I = 0x7f0d02ed

.field public static final pm_night:I = 0x7f0d02ee

.field public static final popup_connect_network_body:I = 0x7f0d04a0

.field public static final popup_connect_network_body_wifionly:I = 0x7f0d04a1

.field public static final popup_connect_network_title:I = 0x7f0d049f

.field public static final previous:I = 0x7f0d0252

.field public static final prompt_generic_hintset_eightth_1:I = 0x7f0d0176

.field public static final prompt_generic_hintset_eightth_2:I = 0x7f0d0177

.field public static final prompt_generic_hintset_eightth_3:I = 0x7f0d0178

.field public static final prompt_generic_hintset_eightth_4:I = 0x7f0d0179

.field public static final prompt_generic_hintset_fifth_1:I = 0x7f0d016c

.field public static final prompt_generic_hintset_fifth_2:I = 0x7f0d016d

.field public static final prompt_generic_hintset_fifth_3:I = 0x7f0d016e

.field public static final prompt_generic_hintset_fifth_4:I = 0x7f0d016f

.field public static final prompt_generic_hintset_first_1:I = 0x7f0d015f

.field public static final prompt_generic_hintset_first_2:I = 0x7f0d0160

.field public static final prompt_generic_hintset_first_3:I = 0x7f0d0161

.field public static final prompt_generic_hintset_first_4:I = 0x7f0d0162

.field public static final prompt_generic_hintset_fourth_1:I = 0x7f0d0168

.field public static final prompt_generic_hintset_fourth_2:I = 0x7f0d0169

.field public static final prompt_generic_hintset_fourth_3:I = 0x7f0d016a

.field public static final prompt_generic_hintset_fourth_4:I = 0x7f0d016b

.field public static final prompt_generic_hintset_ninth_1:I = 0x7f0d017a

.field public static final prompt_generic_hintset_ninth_2:I = 0x7f0d017b

.field public static final prompt_generic_hintset_ninth_3:I = 0x7f0d017c

.field public static final prompt_generic_hintset_second_1:I = 0x7f0d0163

.field public static final prompt_generic_hintset_second_2:I = 0x7f0d0164

.field public static final prompt_generic_hintset_second_3:I = 0x7f0d0165

.field public static final prompt_generic_hintset_seventh_1:I = 0x7f0d0173

.field public static final prompt_generic_hintset_seventh_2:I = 0x7f0d0174

.field public static final prompt_generic_hintset_seventh_3:I = 0x7f0d0175

.field public static final prompt_generic_hintset_sixth_1:I = 0x7f0d0170

.field public static final prompt_generic_hintset_sixth_2:I = 0x7f0d0171

.field public static final prompt_generic_hintset_sixth_3:I = 0x7f0d0172

.field public static final prompt_generic_hintset_third_1:I = 0x7f0d0166

.field public static final prompt_generic_hintset_third_2:I = 0x7f0d0167

.field public static final prompt_no_previously_read_news:I = 0x7f0d04cb

.field public static final qa_tts_NO_ANS_LOCAL_SEARCH:I = 0x7f0d032a

.field public static final qa_tts_NO_ANS_LOCAL_SEARCH_NON_US:I = 0x7f0d032b

.field public static final radio_button:I = 0x7f0d04d2

.field public static final readout_btn:I = 0x7f0d0253

.field public static final receive_schedule_when_open_svoice:I = 0x7f0d045e

.field public static final reco_error_0:I = 0x7f0d0254

.field public static final reco_error_1:I = 0x7f0d0255

.field public static final reco_error_10:I = 0x7f0d0256

.field public static final reco_error_2:I = 0x7f0d0257

.field public static final reco_error_3:I = 0x7f0d0258

.field public static final reco_error_4:I = 0x7f0d0259

.field public static final reco_error_5:I = 0x7f0d025a

.field public static final reco_error_6:I = 0x7f0d025b

.field public static final reco_error_7:I = 0x7f0d025c

.field public static final reco_error_8:I = 0x7f0d025d

.field public static final reco_warning_nothing_recognized:I = 0x7f0d03dd

.field public static final recognized_wakeup:I = 0x7f0d03f2

.field public static final reminder:I = 0x7f0d0226

.field public static final reply_btn:I = 0x7f0d025e

.field public static final reset_dialog_message:I = 0x7f0d02f3

.field public static final retry_wake_up_word:I = 0x7f0d030a

.field public static final s_voice_is_working_now:I = 0x7f0d04b3

.field public static final samsung_powered_by_vlingo:I = 0x7f0d03e3

.field public static final samsung_version:I = 0x7f0d03f6

.field public static final sat1:I = 0x7f0d031e

.field public static final save_btn:I = 0x7f0d025f

.field public static final say_save_memo:I = 0x7f0d0209

.field public static final say_what_you_want:I = 0x7f0d0260

.field public static final schedule:I = 0x7f0d03a6

.field public static final schedule_notification_summary:I = 0x7f0d03a7

.field public static final search_label_button:I = 0x7f0d0382

.field public static final secured_lock_pop_up_desc:I = 0x7f0d04d1

.field public static final select_bookmark:I = 0x7f0d038c

.field public static final selected_function_already_exist:I = 0x7f0d0309

.field public static final send_btn:I = 0x7f0d0261

.field public static final send_current_location:I = 0x7f0d0439

.field public static final send_message_to:I = 0x7f0d03ab

.field public static final sentence_separators:I = 0x7f0d0134

.field public static final set:I = 0x7f0d0262

.field public static final setting_idle_press_button:I = 0x7f0d0431

.field public static final setting_voice_wakeup_screenoff_body:I = 0x7f0d0435

.field public static final setting_voice_wakeup_screenoff_body_without_carmode:I = 0x7f0d0436

.field public static final setting_wakeup_idle_hint:I = 0x7f0d030f

.field public static final setting_wakeup_idle_hint_for_gsa:I = 0x7f0d04de

.field public static final setting_wakeup_in_secured_body:I = 0x7f0d0434

.field public static final setting_wakeup_recognize_fail_try_again:I = 0x7f0d0263

.field public static final setting_wakeup_screenoff:I = 0x7f0d03f9

.field public static final setting_wakeup_screenoff_title:I = 0x7f0d03f8

.field public static final setting_wakeup_screenoff_title_zero:I = 0x7f0d04e5

.field public static final setting_wakeup_secure_body:I = 0x7f0d0414

.field public static final setting_wakeup_secure_popupbody:I = 0x7f0d0415

.field public static final setting_wakeup_secure_title:I = 0x7f0d0413

.field public static final setting_wakeup_set_wakeup_command_dialog_title:I = 0x7f0d0265

.field public static final setting_wakeup_set_wakeup_command_reset:I = 0x7f0d0266

.field public static final setting_wakeup_speak_now:I = 0x7f0d0267

.field public static final setting_wakeup_tts_RECOGNIZE_PARTIAL_SUCCESS_LOCK_ADAPT:I = 0x7f0d0312

.field public static final setting_wakeup_tts_RECOGNIZE_PARTIAL_SUCCESS_LOCK_ADAPT_US:I = 0x7f0d0313

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS:I = 0x7f0d0268

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION:I = 0x7f0d0310

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION_US:I = 0x7f0d0311

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION_toast:I = 0x7f0d03cc

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS_LOCK_ADAPT:I = 0x7f0d0314

.field public static final setting_wakeup_tts_RECOGNIZE_SUCCESS_toast:I = 0x7f0d03cb

.field public static final setting_wakeup_tts_TAP_BUTTON_BELOW:I = 0x7f0d0269

.field public static final setting_wakeup_tts_TAP_BUTTON_BELOW_UNLOCK:I = 0x7f0d030d

.field public static final setting_wakeup_tts_TAP_BUTTON_BELOW_UNLOCK_ADAPT:I = 0x7f0d030e

.field public static final setting_wakeup_unlock_screen:I = 0x7f0d0316

.field public static final setting_wakeup_unlock_screen_non_motion:I = 0x7f0d0317

.field public static final setting_wakeup_unlock_screen_title:I = 0x7f0d0324

.field public static final settings_EnableAudioFileLog:I = 0x7f0d027b

.field public static final settings_EnableServerLog:I = 0x7f0d027c

.field public static final settings_EnableTimingLog:I = 0x7f0d027d

.field public static final settings_add_account:I = 0x7f0d026a

.field public static final settings_advanced_BTlisten_summary:I = 0x7f0d026d

.field public static final settings_advanced_BTlisten_title:I = 0x7f0d026e

.field public static final settings_advanced_autopunc_summary:I = 0x7f0d026b

.field public static final settings_advanced_autopunc_title:I = 0x7f0d026c

.field public static final settings_advanced_useloc_summary:I = 0x7f0d026f

.field public static final settings_advanced_useloc_title:I = 0x7f0d0270

.field public static final settings_autodial_dialogtitle:I = 0x7f0d0271

.field public static final settings_autodial_summary:I = 0x7f0d0272

.field public static final settings_autodial_title:I = 0x7f0d0273

.field public static final settings_call_title:I = 0x7f0d0274

.field public static final settings_click_to_login:I = 0x7f0d0275

.field public static final settings_click_to_login_facebook:I = 0x7f0d0276

.field public static final settings_click_to_login_twitter:I = 0x7f0d0277

.field public static final settings_click_to_login_weibo:I = 0x7f0d0398

.field public static final settings_configure_home_address:I = 0x7f0d027a

.field public static final settings_custom_wakeup_speaknow:I = 0x7f0d02f1

.field public static final settings_custom_wakeup_times:I = 0x7f0d02f2

.field public static final settings_detailed_tts_feedback_summary:I = 0x7f0d0394

.field public static final settings_detailed_tts_feedback_title:I = 0x7f0d0393

.field public static final settings_general:I = 0x7f0d027e

.field public static final settings_general_lower:I = 0x7f0d027f

.field public static final settings_headset_mode:I = 0x7f0d0280

.field public static final settings_headset_mode_summary:I = 0x7f0d0281

.field public static final settings_headset_mode_title:I = 0x7f0d0282

.field public static final settings_help_set_home_address:I = 0x7f0d0264

.field public static final settings_incar_BTlanuch_summary:I = 0x7f0d0285

.field public static final settings_incar_BTlaunch_title:I = 0x7f0d0286

.field public static final settings_incar_autostart_speakerphone_summary:I = 0x7f0d0283

.field public static final settings_incar_autostart_speakerphone_title:I = 0x7f0d0284

.field public static final settings_incar_homeaddress_summary:I = 0x7f0d0287

.field public static final settings_incar_homeaddress_title:I = 0x7f0d0288

.field public static final settings_incar_motion_summary:I = 0x7f0d0289

.field public static final settings_incar_motion_title:I = 0x7f0d028a

.field public static final settings_incar_navigation:I = 0x7f0d028b

.field public static final settings_incar_officeaddress_summary:I = 0x7f0d0502

.field public static final settings_incar_officeaddress_title:I = 0x7f0d0503

.field public static final settings_incar_onwhilecharging_summary:I = 0x7f0d028c

.field public static final settings_incar_onwhilecharging_title:I = 0x7f0d028d

.field public static final settings_incar_say_your_wakeup_title:I = 0x7f0d02ef

.field public static final settings_incar_set_summary:I = 0x7f0d028e

.field public static final settings_incar_set_title:I = 0x7f0d028f

.field public static final settings_incar_speakshow_msgbodies_summary:I = 0x7f0d0290

.field public static final settings_incar_speakshow_msgbodies_title:I = 0x7f0d0291

.field public static final settings_incar_texttospeech:I = 0x7f0d0292

.field public static final settings_incar_wakeup:I = 0x7f0d0293

.field public static final settings_incar_wakeup_lower:I = 0x7f0d0294

.field public static final settings_incar_wakeup_lower_zero:I = 0x7f0d04e4

.field public static final settings_incar_wakeup_summary:I = 0x7f0d0207

.field public static final settings_incar_wakeup_title:I = 0x7f0d0295

.field public static final settings_language_dialogtitle:I = 0x7f0d0296

.field public static final settings_language_summary:I = 0x7f0d0297

.field public static final settings_language_title:I = 0x7f0d0298

.field public static final settings_launch_voicetalk_summary:I = 0x7f0d0299

.field public static final settings_launch_voicetalk_title:I = 0x7f0d029a

.field public static final settings_launch_voicetalk_title_zero:I = 0x7f0d04e6

.field public static final settings_logged_in_as:I = 0x7f0d029b

.field public static final settings_login_facebook:I = 0x7f0d029c

.field public static final settings_login_twitter:I = 0x7f0d029d

.field public static final settings_login_weibo:I = 0x7f0d0397

.field public static final settings_logout:I = 0x7f0d029e

.field public static final settings_profanityfilter_summary:I = 0x7f0d029f

.field public static final settings_profanityfilter_title:I = 0x7f0d02a0

.field public static final settings_search_engine_dialogtitle:I = 0x7f0d02a1

.field public static final settings_search_engine_title:I = 0x7f0d02a2

.field public static final settings_social_facebook_summary:I = 0x7f0d02a3

.field public static final settings_social_facebook_title:I = 0x7f0d02a4

.field public static final settings_social_qzone_summary:I = 0x7f0d02a5

.field public static final settings_social_qzone_title:I = 0x7f0d02a6

.field public static final settings_social_summary:I = 0x7f0d02a7

.field public static final settings_social_title:I = 0x7f0d02a8

.field public static final settings_social_twitter_summary:I = 0x7f0d02a9

.field public static final settings_social_twitter_title:I = 0x7f0d02aa

.field public static final settings_space_accounts:I = 0x7f0d02ab

.field public static final settings_space_minute:I = 0x7f0d02ac

.field public static final settings_space_minutes:I = 0x7f0d02ad

.field public static final settings_tap_to_logout_facebook:I = 0x7f0d0278

.field public static final settings_tap_to_signout_twitter:I = 0x7f0d0279

.field public static final settings_uuid_summary:I = 0x7f0d02ae

.field public static final settings_uuid_title:I = 0x7f0d02af

.field public static final settings_voice_talk_help_title:I = 0x7f0d02b0

.field public static final shortcut_name:I = 0x7f0d038e

.field public static final shortcut_name_hint:I = 0x7f0d0390

.field public static final skip:I = 0x7f0d02b1

.field public static final social_facebook_comm_error:I = 0x7f0d02b2

.field public static final social_facebook_login_error:I = 0x7f0d02b3

.field public static final social_no:I = 0x7f0d02b6

.field public static final social_not_supported:I = 0x7f0d0325

.field public static final social_qzone_login_error:I = 0x7f0d02b4

.field public static final social_twitter_login_error:I = 0x7f0d02b7

.field public static final social_wait_logging_in:I = 0x7f0d02b8

.field public static final social_weibo_login_error:I = 0x7f0d02b5

.field public static final social_yes:I = 0x7f0d02b9

.field public static final software_update:I = 0x7f0d048a

.field public static final software_updates_are_available:I = 0x7f0d048c

.field public static final space:I = 0x7f0d015d

.field public static final start:I = 0x7f0d04a7

.field public static final start_automation_playback:I = 0x7f0d036c

.field public static final start_automation_recording:I = 0x7f0d036e

.field public static final starting_voice_recorder:I = 0x7f0d03cf

.field public static final stop_automation_playback:I = 0x7f0d036d

.field public static final stop_automation_recording:I = 0x7f0d036f

.field public static final string_a_new_version_is_available_desc:I = 0x7f0d0489

.field public static final success:I = 0x7f0d0411

.field public static final success_to_erase_data:I = 0x7f0d04b5

.field public static final sun1:I = 0x7f0d0318

.field public static final sview_cover_alwaysmicon:I = 0x7f0d040d

.field public static final svoice_is_listening_speak_now:I = 0x7f0d04cc

.field public static final svoice_unlock:I = 0x7f0d0322

.field public static final talkback_bubble_editable:I = 0x7f0d037d

.field public static final talkback_expand_collapse_lists:I = 0x7f0d04cd

.field public static final talkback_mic_button_idle:I = 0x7f0d0378

.field public static final talkback_mic_button_recognizing:I = 0x7f0d0379

.field public static final talkback_voice_prompt_off:I = 0x7f0d037b

.field public static final talkback_voice_prompt_on:I = 0x7f0d037a

.field public static final talkback_widget_detail:I = 0x7f0d037c

.field public static final talkback_you_said:I = 0x7f0d0389

.field public static final test_schedule:I = 0x7f0d036a

.field public static final text_copied_to_clipboard:I = 0x7f0d041d

.field public static final text_message:I = 0x7f0d01bb

.field public static final that_is_all_for_disambiguation:I = 0x7f0d04b0

.field public static final thu1:I = 0x7f0d031c

.field public static final time_prefix_0_1:I = 0x7f0d040f

.field public static final time_prefix_2_23:I = 0x7f0d0410

.field public static final timer_hour:I = 0x7f0d02ba

.field public static final timer_hr:I = 0x7f0d02bb

.field public static final timer_hrs:I = 0x7f0d035e

.field public static final timer_min:I = 0x7f0d02bc

.field public static final timer_mins:I = 0x7f0d035f

.field public static final timer_minute:I = 0x7f0d02bd

.field public static final timer_sec:I = 0x7f0d02be

.field public static final timer_second:I = 0x7f0d02bf

.field public static final timer_seconds:I = 0x7f0d0360

.field public static final timer_widget_time_info:I = 0x7f0d0391

.field public static final tip_bubble_on_wake_up_function_list:I = 0x7f0d03c6

.field public static final tip_bubble_on_wake_up_function_list_h_device:I = 0x7f0d03fa

.field public static final tip_bubble_on_wake_up_whole_list:I = 0x7f0d03c5

.field public static final tips_and_guidelines:I = 0x7f0d045f

.field public static final title_address:I = 0x7f0d02f8

.field public static final title_birthday:I = 0x7f0d02f9

.field public static final title_bookmark_shortcut:I = 0x7f0d0356

.field public static final title_change_voice_command:I = 0x7f0d0305

.field public static final title_change_voice_function:I = 0x7f0d0306

.field public static final title_check_missed_call:I = 0x7f0d02fa

.field public static final title_check_missed_message:I = 0x7f0d02fb

.field public static final title_direct_dial:I = 0x7f0d0354

.field public static final title_direct_message:I = 0x7f0d0355

.field public static final title_email:I = 0x7f0d0462

.field public static final title_listen_my_voice_command:I = 0x7f0d0307

.field public static final title_navigation_shortcut:I = 0x7f0d0357

.field public static final title_notification:I = 0x7f0d0308

.field public static final title_play_music:I = 0x7f0d02fd

.field public static final title_play_radio:I = 0x7f0d02fe

.field public static final title_record_voice:I = 0x7f0d02ff

.field public static final title_voice_camera:I = 0x7f0d02fc

.field public static final title_wake_up_auto_function1:I = 0x7f0d0301

.field public static final title_wake_up_auto_function2:I = 0x7f0d0302

.field public static final title_wake_up_auto_function3:I = 0x7f0d0303

.field public static final title_wake_up_auto_function4:I = 0x7f0d0304

.field public static final title_wake_up_voice_talk:I = 0x7f0d0300

.field public static final toast_while_running_sound_detector_for_gsa:I = 0x7f0d04dd

.field public static final toast_while_running_sound_detector_for_svoice:I = 0x7f0d04dc

.field public static final today:I = 0x7f0d04e2

.field public static final tomorrow:I = 0x7f0d04e3

.field public static final tos_accept:I = 0x7f0d02c0

.field public static final tos_accept_for_pt_br:I = 0x7f0d0367

.field public static final tos_accept_samsung:I = 0x7f0d020e

.field public static final tos_accept_samsung_for_pt_br:I = 0x7f0d0366

.field public static final tos_already_accepted_still_binding_base:I = 0x7f0d047e

.field public static final tos_decline:I = 0x7f0d02c1

.field public static final tos_decline_for_pt_br:I = 0x7f0d0368

.field public static final tos_decline_samsung:I = 0x7f0d02c2

.field public static final tos_dialog_view_terms_cc:I = 0x7f0d047b

.field public static final tos_dialog_view_terms_cc_for_pt_br:I = 0x7f0d047c

.field public static final tos_dialog_view_terms_old:I = 0x7f0d02c3

.field public static final tos_dialog_view_terms_old_for_pt_br:I = 0x7f0d0365

.field public static final tos_dialog_view_terms_old_without_carmode:I = 0x7f0d02c4

.field public static final tos_dialog_view_terms_samsung:I = 0x7f0d020f

.field public static final tos_dialog_view_terms_samsung_for_pt_br:I = 0x7f0d0363

.field public static final tos_dialog_view_terms_samsung_without_carmode:I = 0x7f0d0210

.field public static final tos_must_accept_to_use:I = 0x7f0d02c7

.field public static final tos_notice_regarding_service_agreement:I = 0x7f0d047d

.field public static final tos_nuance_privacy_policy_link:I = 0x7f0d02c6

.field public static final tos_terms_of_service_link:I = 0x7f0d02c5

.field public static final tos_url_href:I = 0x7f0d047f

.field public static final tos_vlingo_terms:I = 0x7f0d02c8

.field public static final tos_vlingo_terms_for_pt_br:I = 0x7f0d0364

.field public static final tos_vlingo_terms_samsung:I = 0x7f0d02c9

.field public static final tos_vlingo_terms_samsung_for_pt_br:I = 0x7f0d0362

.field public static final track_titles:I = 0x7f0d0495

.field public static final try_again:I = 0x7f0d02e6

.field public static final tts_time_format:I = 0x7f0d0358

.field public static final tue1:I = 0x7f0d031a

.field public static final turn_off_notification_alarm:I = 0x7f0d0469

.field public static final turn_off_notification_alarm_already:I = 0x7f0d046a

.field public static final turn_off_notification_all:I = 0x7f0d0465

.field public static final turn_off_notification_all_already:I = 0x7f0d0466

.field public static final turn_off_notification_call:I = 0x7f0d0471

.field public static final turn_off_notification_call_already:I = 0x7f0d0472

.field public static final turn_off_notification_email:I = 0x7f0d0479

.field public static final turn_off_notification_email_already:I = 0x7f0d047a

.field public static final turn_off_notification_msg:I = 0x7f0d0475

.field public static final turn_off_notification_msg_already:I = 0x7f0d0476

.field public static final turn_off_notification_schedule:I = 0x7f0d046d

.field public static final turn_off_notification_schedule_already:I = 0x7f0d046e

.field public static final turn_on_notification_alarm:I = 0x7f0d0467

.field public static final turn_on_notification_alarm_already:I = 0x7f0d0468

.field public static final turn_on_notification_all:I = 0x7f0d0463

.field public static final turn_on_notification_all_already:I = 0x7f0d0464

.field public static final turn_on_notification_call:I = 0x7f0d046f

.field public static final turn_on_notification_call_already:I = 0x7f0d0470

.field public static final turn_on_notification_email:I = 0x7f0d0477

.field public static final turn_on_notification_email_already:I = 0x7f0d0478

.field public static final turn_on_notification_msg:I = 0x7f0d0473

.field public static final turn_on_notification_msg_already:I = 0x7f0d0474

.field public static final turn_on_notification_schedule:I = 0x7f0d046b

.field public static final turn_on_notification_schedule_already:I = 0x7f0d046c

.field public static final tutorial_agree_check:I = 0x7f0d0461

.field public static final tutorial_check_weather:I = 0x7f0d0441

.field public static final tutorial_display_higalaxy:I = 0x7f0d044e

.field public static final tutorial_display_or:I = 0x7f0d044f

.field public static final tutorial_display_processing:I = 0x7f0d0452

.field public static final tutorial_display_speak:I = 0x7f0d0451

.field public static final tutorial_display_tap_mic:I = 0x7f0d0450

.field public static final tutorial_first_txt:I = 0x7f0d04c4

.field public static final tutorial_make_calls:I = 0x7f0d043e

.field public static final tutorial_much_more:I = 0x7f0d0443

.field public static final tutorial_play_music:I = 0x7f0d0442

.field public static final tutorial_preview:I = 0x7f0d0445

.field public static final tutorial_preview_subheader:I = 0x7f0d0456

.field public static final tutorial_say_higalaxy:I = 0x7f0d0455

.field public static final tutorial_say_higalaxy_seamless:I = 0x7f0d04ca

.field public static final tutorial_second_txt:I = 0x7f0d04c5

.field public static final tutorial_send_messages:I = 0x7f0d043f

.field public static final tutorial_set_alarms:I = 0x7f0d0440

.field public static final tutorial_start_svoice:I = 0x7f0d0444

.field public static final tutorial_start_svoice_subheader:I = 0x7f0d0457

.field public static final tutorial_subheader:I = 0x7f0d043d

.field public static final tutorial_today_weather:I = 0x7f0d0453

.field public static final tutorial_tts_active:I = 0x7f0d0446

.field public static final tutorial_tts_always_with_you:I = 0x7f0d04c7

.field public static final tutorial_tts_ask_question:I = 0x7f0d044a

.field public static final tutorial_tts_lets_see:I = 0x7f0d0449

.field public static final tutorial_tts_lets_start:I = 0x7f0d044d

.field public static final tutorial_tts_lets_start_seamless:I = 0x7f0d04c9

.field public static final tutorial_tts_or:I = 0x7f0d0447

.field public static final tutorial_tts_proccessing:I = 0x7f0d044b

.field public static final tutorial_tts_show_result:I = 0x7f0d044c

.field public static final tutorial_tts_tap_mic:I = 0x7f0d0448

.field public static final tutorial_tts_to_activate:I = 0x7f0d04c8

.field public static final tutorial_user_request_txt:I = 0x7f0d04c6

.field public static final tutorial_weather_result:I = 0x7f0d0454

.field public static final unable_to_connect_network:I = 0x7f0d0497

.field public static final unable_to_connect_to_server:I = 0x7f0d049b

.field public static final unable_to_update_software:I = 0x7f0d048e

.field public static final unalbe_to_set_wakeup_using_bt_toast:I = 0x7f0d04d0

.field public static final unchecked:I = 0x7f0d04d4

.field public static final unit_temperature_farenheit:I = 0x7f0d032c

.field public static final unknown_search_1:I = 0x7f0d0383

.field public static final unknown_search_2:I = 0x7f0d0384

.field public static final unknown_search_3:I = 0x7f0d0385

.field public static final unknown_search_4:I = 0x7f0d0386

.field public static final unknown_search_5:I = 0x7f0d0387

.field public static final unknown_search_6:I = 0x7f0d0388

.field public static final unlock_screen:I = 0x7f0d03a8

.field public static final unlock_screen_contents_summary:I = 0x7f0d03a9

.field public static final unlock_screen_contents_summary_wifi:I = 0x7f0d03aa

.field public static final update_btn:I = 0x7f0d02e7

.field public static final user_data_deletion_confirm:I = 0x7f0d048f

.field public static final user_data_deletion_confirm_server_failed:I = 0x7f0d0490

.field public static final user_data_deletion_confirm_title:I = 0x7f0d0491

.field public static final util_WEB_SEARCH_NAME_DEFAULT:I = 0x7f0d0147

.field public static final util_WEB_SEARCH_NAME_DEFAULT_CN:I = 0x7f0d0148

.field public static final util_WEB_SEARCH_URL_BAIDU_DEFAULT:I = 0x7f0d014c

.field public static final util_WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:I = 0x7f0d014d

.field public static final util_WEB_SEARCH_URL_BING_DEFAULT:I = 0x7f0d014e

.field public static final util_WEB_SEARCH_URL_BING_HOME_DEFAULT:I = 0x7f0d014f

.field public static final util_WEB_SEARCH_URL_DAUM_DEFAULT:I = 0x7f0d015b

.field public static final util_WEB_SEARCH_URL_DAUM_HOME_DEFAULT:I = 0x7f0d015c

.field public static final util_WEB_SEARCH_URL_DEFAULT:I = 0x7f0d0150

.field public static final util_WEB_SEARCH_URL_DEFAULT_CN:I = 0x7f0d0151

.field public static final util_WEB_SEARCH_URL_GOOGLE_DEFAULT:I = 0x7f0d0153

.field public static final util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT:I = 0x7f0d0154

.field public static final util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT_US:I = 0x7f0d0152

.field public static final util_WEB_SEARCH_URL_HOME_DEFAULT:I = 0x7f0d0155

.field public static final util_WEB_SEARCH_URL_HOME_DEFAULT_CN:I = 0x7f0d0156

.field public static final util_WEB_SEARCH_URL_NAVER_DEFAULT:I = 0x7f0d0159

.field public static final util_WEB_SEARCH_URL_NAVER_HOME_DEFAULT:I = 0x7f0d015a

.field public static final util_WEB_SEARCH_URL_YAHOO_DEFAULT:I = 0x7f0d0157

.field public static final util_WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:I = 0x7f0d0158

.field public static final voice_feedback:I = 0x7f0d0340

.field public static final voice_input_control_alarm:I = 0x7f0d0331

.field public static final voice_input_control_alarm_summary:I = 0x7f0d0332

.field public static final voice_input_control_camera:I = 0x7f0d0333

.field public static final voice_input_control_camera_summary:I = 0x7f0d0334

.field public static final voice_input_control_cancel:I = 0x7f0d033d

.field public static final voice_input_control_chatonv:I = 0x7f0d0395

.field public static final voice_input_control_chatonv_summary:I = 0x7f0d0396

.field public static final voice_input_control_incomming_calls:I = 0x7f0d032f

.field public static final voice_input_control_incomming_calls_summary:I = 0x7f0d0330

.field public static final voice_input_control_message:I = 0x7f0d033a

.field public static final voice_input_control_message_no_call:I = 0x7f0d033b

.field public static final voice_input_control_message_santosh:I = 0x7f0d03f5

.field public static final voice_input_control_music:I = 0x7f0d0335

.field public static final voice_input_control_music_summary:I = 0x7f0d0336

.field public static final voice_input_control_ok:I = 0x7f0d033c

.field public static final voice_input_control_radio:I = 0x7f0d0337

.field public static final voice_input_control_radio_summary:I = 0x7f0d0338

.field public static final voice_input_control_summry:I = 0x7f0d032e

.field public static final voice_input_control_title:I = 0x7f0d032d

.field public static final voice_input_control_warrning:I = 0x7f0d0339

.field public static final voice_prompt_confirmation_cancel:I = 0x7f0d02ca

.field public static final voice_prompt_confirmation_disable:I = 0x7f0d02cb

.field public static final voice_prompt_confirmation_enable:I = 0x7f0d02cc

.field public static final voice_prompt_confirmation_for_off:I = 0x7f0d02cd

.field public static final voice_prompt_confirmation_for_on:I = 0x7f0d02ce

.field public static final voice_prompt_confirmation_ok:I = 0x7f0d02cf

.field public static final voice_wake_up_in_sercured_lock_desc:I = 0x7f0d04ba

.field public static final voice_wake_up_in_sercured_lock_title:I = 0x7f0d04b9

.field public static final voice_wakeup_desc_from_car_mode:I = 0x7f0d04c2

.field public static final voice_wakeup_dialog_desc:I = 0x7f0d04bb

.field public static final volume_at_maximum:I = 0x7f0d0422

.field public static final volume_at_minimum:I = 0x7f0d0423

.field public static final volume_down:I = 0x7f0d0425

.field public static final volume_up:I = 0x7f0d0424

.field public static final wake_up_enabled:I = 0x7f0d01c7

.field public static final wake_up_phrase_hi_galaxy:I = 0x7f0d04da

.field public static final wake_up_record_already_exist:I = 0x7f0d041c

.field public static final wake_up_time:I = 0x7f0d04ab

.field public static final wake_up_word_already_assigned:I = 0x7f0d030b

.field public static final wakeup_auto_function_1:I = 0x7f0d0375

.field public static final wakeup_auto_function_2:I = 0x7f0d0376

.field public static final wakeup_auto_function_3:I = 0x7f0d0377

.field public static final wakeup_error_case_1:I = 0x7f0d0459

.field public static final wakeup_error_case_2:I = 0x7f0d045a

.field public static final wakeup_error_case_3:I = 0x7f0d045b

.field public static final wakeup_lock_screen_dialog_message:I = 0x7f0d0327

.field public static final wakeup_lock_screen_dialog_message2:I = 0x7f0d0328

.field public static final wakeup_lock_screen_dialog_title:I = 0x7f0d0326

.field public static final wakeup_screen_off_dialog_message:I = 0x7f0d03f7

.field public static final wakeup_setting_dialog_message:I = 0x7f0d02f6

.field public static final wakeup_setting_dialog_title:I = 0x7f0d02f7

.field public static final wakeup_voice_talk:I = 0x7f0d0374

.field public static final wakeup_when_locked_summry:I = 0x7f0d04c1

.field public static final wcis_alarm:I = 0x7f0d0196

.field public static final wcis_alarm_ex:I = 0x7f0d0197

.field public static final wcis_alarm_lookup_ex:I = 0x7f0d0198

.field public static final wcis_check_weather:I = 0x7f0d01b5

.field public static final wcis_check_weather_ex:I = 0x7f0d01b6

.field public static final wcis_driving_mode:I = 0x7f0d0199

.field public static final wcis_driving_mode_ex:I = 0x7f0d019a

.field public static final wcis_edit_what_you_said:I = 0x7f0d02e1

.field public static final wcis_edit_what_you_said_caption:I = 0x7f0d02e2

.field public static final wcis_find_contact:I = 0x7f0d02df

.field public static final wcis_findcontact_ex:I = 0x7f0d01b1

.field public static final wcis_general_help:I = 0x7f0d02d0

.field public static final wcis_get_an_answers:I = 0x7f0d01b7

.field public static final wcis_get_an_answers_ex:I = 0x7f0d01b8

.field public static final wcis_handwriting_caption:I = 0x7f0d02e0

.field public static final wcis_how_to_use:I = 0x7f0d02d1

.field public static final wcis_how_to_use_VC_caption:I = 0x7f0d02d3

.field public static final wcis_how_to_use_caption:I = 0x7f0d02d2

.field public static final wcis_info_row_more_info:I = 0x7f0d02d4

.field public static final wcis_local_listings:I = 0x7f0d01ca

.field public static final wcis_local_listings_ex:I = 0x7f0d01cb

.field public static final wcis_local_listings_ex_naver:I = 0x7f0d01fc

.field public static final wcis_local_listings_naver:I = 0x7f0d01fa

.field public static final wcis_memo:I = 0x7f0d02d5

.field public static final wcis_memo_ex:I = 0x7f0d019b

.field public static final wcis_memo_in_car_ex:I = 0x7f0d019c

.field public static final wcis_movie_naver:I = 0x7f0d01fd

.field public static final wcis_movies_ex_naver:I = 0x7f0d01fe

.field public static final wcis_music:I = 0x7f0d019d

.field public static final wcis_music_ex:I = 0x7f0d019e

.field public static final wcis_music_ex2:I = 0x7f0d019f

.field public static final wcis_nav:I = 0x7f0d02d6

.field public static final wcis_nav_ex:I = 0x7f0d01a0

.field public static final wcis_news:I = 0x7f0d01da

.field public static final wcis_news_ex:I = 0x7f0d01db

.field public static final wcis_open_app:I = 0x7f0d01a1

.field public static final wcis_open_app_ex:I = 0x7f0d01a2

.field public static final wcis_open_app_ex_american:I = 0x7f0d01f5

.field public static final wcis_recordvoice:I = 0x7f0d01a3

.field public static final wcis_recordvoice_ex:I = 0x7f0d01a4

.field public static final wcis_schedule:I = 0x7f0d02d7

.field public static final wcis_schedule_ex:I = 0x7f0d01a5

.field public static final wcis_schedule_in_car_ex:I = 0x7f0d01a7

.field public static final wcis_schedule_lookup_ex:I = 0x7f0d01a6

.field public static final wcis_search_ex:I = 0x7f0d01a8

.field public static final wcis_search_ex_cn:I = 0x7f0d01a9

.field public static final wcis_search_ex_naver:I = 0x7f0d01f9

.field public static final wcis_search_speak_query:I = 0x7f0d02d8

.field public static final wcis_simple_setting_controls:I = 0x7f0d01b9

.field public static final wcis_simple_setting_controls_ex:I = 0x7f0d01ba

.field public static final wcis_simple_setting_controls_ex_for_chinese:I = 0x7f0d01e3

.field public static final wcis_simple_setting_controls_for_chinese:I = 0x7f0d01e2

.field public static final wcis_sms_ex:I = 0x7f0d01aa

.field public static final wcis_sms_speak_text:I = 0x7f0d02d9

.field public static final wcis_social_update:I = 0x7f0d02da

.field public static final wcis_social_update_cn:I = 0x7f0d02db

.field public static final wcis_social_update_ex:I = 0x7f0d01ab

.field public static final wcis_social_update_ex_cn:I = 0x7f0d01ac

.field public static final wcis_task:I = 0x7f0d01b2

.field public static final wcis_task_ex:I = 0x7f0d01b3

.field public static final wcis_task_lookup_ex:I = 0x7f0d01b4

.field public static final wcis_timer:I = 0x7f0d01af

.field public static final wcis_timer_ex:I = 0x7f0d01b0

.field public static final wcis_voice_dial_call_contacts:I = 0x7f0d02dc

.field public static final wcis_voice_dial_contact_ex:I = 0x7f0d01ad

.field public static final wcis_voice_dial_contact_ex_us:I = 0x7f0d01d7

.field public static final wcis_voice_dial_tip:I = 0x7f0d01ae

.field public static final wcis_wake_up:I = 0x7f0d02dd

.field public static final wcis_wake_up_caption:I = 0x7f0d02de

.field public static final wcis_world_clock:I = 0x7f0d01e9

.field public static final wcis_world_clock_ex:I = 0x7f0d01ea

.field public static final weather_accuweathercom_accuweather:I = 0x7f0d0487

.field public static final weather_location:I = 0x7f0d03e2

.field public static final web:I = 0x7f0d02e3

.field public static final wed1:I = 0x7f0d031b

.field public static final welcome:I = 0x7f0d0482

.field public static final what_can_i_say:I = 0x7f0d02e4

.field public static final which_one_for_disambiguation:I = 0x7f0d04af

.field public static final word_separators:I = 0x7f0d0133

.field public static final world_time_with_location:I = 0x7f0d040e

.field public static final ycs_ex_first_1:I = 0x7f0d01eb

.field public static final ycs_ex_first_2:I = 0x7f0d01ec

.field public static final ycs_ex_first_2_carmode:I = 0x7f0d01ed

.field public static final ycs_ex_first_3:I = 0x7f0d01ee

.field public static final ycs_ex_second_1:I = 0x7f0d01ef

.field public static final ycs_ex_second_2:I = 0x7f0d01f0

.field public static final ycs_ex_second_3:I = 0x7f0d01f1

.field public static final ycs_ex_third_1:I = 0x7f0d01f2

.field public static final ycs_ex_third_2:I = 0x7f0d01f3

.field public static final ycs_ex_third_3:I = 0x7f0d01f4

.field public static final you_can_say_dot_dot:I = 0x7f0d03c0

.field public static final zip_code:I = 0x7f0d034e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
