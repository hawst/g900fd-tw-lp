.class Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;
.super Landroid/database/ContentObserver;
.source "MidasADMController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/MidasADMController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrivingModeContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/MidasADMController;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/MidasADMController;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    .line 82
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 83
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 11
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 88
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v5

    .line 89
    .local v5, "systemDrivingModeOn":Z
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v0

    .line 90
    .local v0, "drivingModeOn":Z
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishDialog()V

    .line 91
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stop()V

    .line 93
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 94
    .local v2, "lastSafereaderTime":Ljava/util/Date;
    const-string/jumbo v6, "car_safereader_last_saferead_time"

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-static {v6, v7, v8}, Lcom/vlingo/midas/settings/MidasSettings;->setLong(Ljava/lang/String;J)V

    .line 96
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 97
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->startSafeReading()V

    .line 100
    :cond_0
    if-eq v5, v0, :cond_3

    .line 101
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 102
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v5, :cond_5

    .line 103
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v6

    if-eqz v6, :cond_1

    const-string/jumbo v6, "voiceinputcontrol_showNeverAgain"

    invoke-static {v6, v9}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v6

    if-eq v6, v10, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 115
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 116
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "svoice_incar_mode"

    invoke-static {v6, v7, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 118
    invoke-static {v1}, Lcom/vlingo/core/internal/settings/Settings;->enableCarMode(Landroid/content/SharedPreferences$Editor;)V

    .line 130
    :cond_2
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    new-instance v7, Landroid/content/Intent;

    const-string/jumbo v8, "android.settings.DRIVING_MODE_CHANGED"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 131
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 133
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_3
    iget-object v6, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    invoke-virtual {v6}, Lcom/vlingo/midas/MidasADMController;->refreshAppModeStates()V

    .line 136
    if-nez v5, :cond_7

    .line 137
    const-string/jumbo v6, "temp_driving_mode_from_myplace"

    invoke-static {v6, v9}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 138
    iget-object v6, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # getter for: Lcom/vlingo/midas/MidasADMController;->prevSoundProfile:I
    invoke-static {v6}, Lcom/vlingo/midas/MidasADMController;->access$000(Lcom/vlingo/midas/MidasADMController;)I

    move-result v6

    if-ltz v6, :cond_4

    .line 139
    iget-object v6, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    iget-object v6, v6, Lcom/vlingo/midas/MidasADMController;->am:Landroid/media/AudioManager;

    iget-object v7, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # getter for: Lcom/vlingo/midas/MidasADMController;->prevSoundProfile:I
    invoke-static {v7}, Lcom/vlingo/midas/MidasADMController;->access$000(Lcom/vlingo/midas/MidasADMController;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 140
    iget-object v6, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    iget-object v7, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # getter for: Lcom/vlingo/midas/MidasADMController;->prevSoundProfile:I
    invoke-static {v7}, Lcom/vlingo/midas/MidasADMController;->access$000(Lcom/vlingo/midas/MidasADMController;)I

    move-result v7

    # setter for: Lcom/vlingo/midas/MidasADMController;->currentSoundProfile:I
    invoke-static {v6, v7}, Lcom/vlingo/midas/MidasADMController;->access$102(Lcom/vlingo/midas/MidasADMController;I)I

    .line 141
    iget-object v6, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    const/4 v7, -0x1

    # setter for: Lcom/vlingo/midas/MidasADMController;->prevSoundProfile:I
    invoke-static {v6, v7}, Lcom/vlingo/midas/MidasADMController;->access$002(Lcom/vlingo/midas/MidasADMController;I)I

    .line 149
    :cond_4
    :goto_1
    return-void

    .line 121
    .restart local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 122
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "svoice_incar_mode"

    invoke-static {v6, v7, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 123
    invoke-static {v1}, Lcom/vlingo/core/internal/settings/Settings;->disableCarMode(Landroid/content/SharedPreferences$Editor;)V

    .line 125
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 126
    .local v3, "mainContext":Landroid/content/Context;
    const-string/jumbo v6, "notification"

    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 127
    .local v4, "notificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v4}, Landroid/app/NotificationManager;->cancelAll()V

    goto :goto_0

    .line 144
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "mainContext":Landroid/content/Context;
    .end local v4    # "notificationManager":Landroid/app/NotificationManager;
    :cond_7
    iget-object v6, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # getter for: Lcom/vlingo/midas/MidasADMController;->prevSoundProfile:I
    invoke-static {v6}, Lcom/vlingo/midas/MidasADMController;->access$000(Lcom/vlingo/midas/MidasADMController;)I

    move-result v6

    if-gez v6, :cond_8

    .line 145
    iget-object v6, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    iget-object v7, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # getter for: Lcom/vlingo/midas/MidasADMController;->currentSoundProfile:I
    invoke-static {v7}, Lcom/vlingo/midas/MidasADMController;->access$100(Lcom/vlingo/midas/MidasADMController;)I

    move-result v7

    # setter for: Lcom/vlingo/midas/MidasADMController;->prevSoundProfile:I
    invoke-static {v6, v7}, Lcom/vlingo/midas/MidasADMController;->access$002(Lcom/vlingo/midas/MidasADMController;I)I

    .line 146
    :cond_8
    iget-object v6, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    const/4 v7, 0x2

    # setter for: Lcom/vlingo/midas/MidasADMController;->currentSoundProfile:I
    invoke-static {v6, v7}, Lcom/vlingo/midas/MidasADMController;->access$102(Lcom/vlingo/midas/MidasADMController;I)I

    .line 147
    iget-object v6, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    iget-object v6, v6, Lcom/vlingo/midas/MidasADMController;->am:Landroid/media/AudioManager;

    iget-object v7, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # getter for: Lcom/vlingo/midas/MidasADMController;->currentSoundProfile:I
    invoke-static {v7}, Lcom/vlingo/midas/MidasADMController;->access$100(Lcom/vlingo/midas/MidasADMController;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/media/AudioManager;->setRingerMode(I)V

    goto :goto_1
.end method
