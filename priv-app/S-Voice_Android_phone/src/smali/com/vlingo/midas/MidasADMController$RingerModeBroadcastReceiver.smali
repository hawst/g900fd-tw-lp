.class Lcom/vlingo/midas/MidasADMController$RingerModeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MidasADMController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/MidasADMController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RingerModeBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/MidasADMController;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/MidasADMController;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/vlingo/midas/MidasADMController$RingerModeBroadcastReceiver;->this$0:Lcom/vlingo/midas/MidasADMController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/MidasADMController;Lcom/vlingo/midas/MidasADMController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/MidasADMController;
    .param p2, "x1"    # Lcom/vlingo/midas/MidasADMController$1;

    .prologue
    .line 312
    invoke-direct {p0, p1}, Lcom/vlingo/midas/MidasADMController$RingerModeBroadcastReceiver;-><init>(Lcom/vlingo/midas/MidasADMController;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x1

    .line 315
    if-eqz p2, :cond_1

    .line 316
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 317
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 318
    iget-object v1, p0, Lcom/vlingo/midas/MidasADMController$RingerModeBroadcastReceiver;->this$0:Lcom/vlingo/midas/MidasADMController;

    const-string/jumbo v2, "android.media.EXTRA_RINGER_MODE"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/vlingo/midas/MidasADMController;->currentSoundProfile:I
    invoke-static {v1, v2}, Lcom/vlingo/midas/MidasADMController;->access$102(Lcom/vlingo/midas/MidasADMController;I)I

    .line 319
    iget-object v1, p0, Lcom/vlingo/midas/MidasADMController$RingerModeBroadcastReceiver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # getter for: Lcom/vlingo/midas/MidasADMController;->currentSoundProfile:I
    invoke-static {v1}, Lcom/vlingo/midas/MidasADMController;->access$100(Lcom/vlingo/midas/MidasADMController;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/MidasADMController$RingerModeBroadcastReceiver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # getter for: Lcom/vlingo/midas/MidasADMController;->currentSoundProfile:I
    invoke-static {v1}, Lcom/vlingo/midas/MidasADMController;->access$100(Lcom/vlingo/midas/MidasADMController;)I

    move-result v1

    if-nez v1, :cond_1

    .line 322
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/MidasADMController$RingerModeBroadcastReceiver;->this$0:Lcom/vlingo/midas/MidasADMController;

    # setter for: Lcom/vlingo/midas/MidasADMController;->prevSoundProfile:I
    invoke-static {v1, v3}, Lcom/vlingo/midas/MidasADMController;->access$002(Lcom/vlingo/midas/MidasADMController;I)I

    .line 323
    invoke-static {p1}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->disablePhoneDrivingMode(Landroid/content/Context;)V

    .line 327
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    return-void
.end method
