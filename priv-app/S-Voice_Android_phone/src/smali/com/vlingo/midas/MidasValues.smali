.class public Lcom/vlingo/midas/MidasValues;
.super Ljava/lang/Object;
.source "MidasValues.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;


# static fields
.field private static final DORMANT_ALWAYS:Ljava/lang/String; = "dormant_always"

.field private static final DORMANT_DISABLE_NOTIFICATIONS:Ljava/lang/String; = "dormant_disable_notifications"

.field private static final DORMANT_END_HOUR:Ljava/lang/String; = "dormant_end_hour"

.field private static final DORMANT_END_MIN:Ljava/lang/String; = "dormant_end_min"

.field private static final DORMANT_START_HOUR:Ljava/lang/String; = "dormant_start_hour"

.field private static final DORMANT_START_MIN:Ljava/lang/String; = "dormant_start_min"

.field private static final DORMANT_SWITCH_ONOFF:Ljava/lang/String; = "dormant_switch_onoff"

.field public static final FEATURE_CALL_VT_SUPPORT:Ljava/lang/String; = "com.sec.feature.call_vt_support"

.field public static final PACKAGE_REMOTE_SERVICE:Ljava/lang/String; = "com.nuance.sample"

.field public static final PACKAGE_SVOICE_PROVIDER:Ljava/lang/String; = "com.samsung.svoiceprovider"

.field public static final PACKAGE_SVOICE_PROVIDER_1:Ljava/lang/String; = "com.samsung.accessory.saproviders"

.field public static final PACKAGE_VACSAMPLEAPP_CLASSNAME:Ljava/lang/String; = "com.nuance.sample.SampleAppApplication"


# instance fields
.field private context:Landroid/content/Context;

.field private fmRadioAppInfo:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

.field private memoAppInfo:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/MidasValues;->memoAppInfo:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    .line 74
    return-void
.end method

.method public static hasRemoteService()Landroid/content/pm/ApplicationInfo;
    .locals 6

    .prologue
    .line 489
    const/4 v2, 0x0

    .line 490
    .local v2, "remoteService":Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 492
    .local v0, "context":Landroid/content/Context;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string/jumbo v4, "com.nuance.sample"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 493
    if-eqz v2, :cond_0

    iget-object v3, v2, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v2, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    const-string/jumbo v4, "com.nuance.sample.SampleAppApplication"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 497
    const/4 v2, 0x0

    .line 505
    .end local v2    # "remoteService":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return-object v2

    .line 499
    .restart local v2    # "remoteService":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v1

    .line 502
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isMessageNotificationSystemSetting()Z
    .locals 5

    .prologue
    .line 194
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 195
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "driving_mode_message_notification"

    const/16 v4, -0x63

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 197
    .local v1, "systemMessagesNotificationSettingValue":I
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private systemCarModeSettingExists()Z
    .locals 5

    .prologue
    const/16 v4, -0x63

    .line 278
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 279
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "driving_mode_on"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 281
    .local v1, "systemSettingValue":I
    if-eq v1, v4, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public disableAppCarMode()V
    .locals 0

    .prologue
    .line 170
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->disableAppCarMode()V

    .line 171
    return-void
.end method

.method public disablePhoneDrivingMode()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->disablePhoneDrivingMode(Landroid/content/Context;)V

    .line 181
    return-void
.end method

.method public enableAppCarMode()V
    .locals 0

    .prologue
    .line 165
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->enableAppCarMode()V

    .line 166
    return-void
.end method

.method public enablePhoneDrivingMode()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->enablePhoneDrivingMode(Landroid/content/Context;)V

    .line 186
    return-void
.end method

.method public getADMController()Lcom/vlingo/core/internal/util/ADMController;
    .locals 1

    .prologue
    .line 292
    invoke-static {}, Lcom/vlingo/midas/MidasADMController;->getInstance()Lcom/vlingo/core/internal/util/ADMController;

    move-result-object v0

    return-object v0
.end method

.method public getAssets()Landroid/content/res/AssetManager;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    return-object v0
.end method

.method public getCarModePackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 556
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->getCarModePackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getClientAudioValues()Lcom/vlingo/core/facade/audio/ClientAudioValues;
    .locals 1

    .prologue
    .line 457
    const/4 v0, 0x0

    return-object v0
.end method

.method public getConfiguration()Landroid/content/res/Configuration;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0
.end method

.method public getContactProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 529
    invoke-static {}, Lcom/vlingo/core/facade/lmtt/MdsoUtils;->getMasterContactProviderAuthority()Ljava/lang/String;

    move-result-object v0

    .line 530
    .local v0, "masterAuthority":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 531
    const-string/jumbo v0, "com.vlingo.midas.contacts.contentprovider"

    .line 533
    .end local v0    # "masterAuthority":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 463
    const-string/jumbo v0, "dm_main"

    return-object v0
.end method

.method public getDrivingModeWidgetMax()I
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x3

    return v0
.end method

.method public getFmRadioApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/midas/MidasValues;->fmRadioAppInfo:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;->FM_RADIO:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    invoke-direct {v0, v1}, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;-><init>(Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;)V

    iput-object v0, p0, Lcom/vlingo/midas/MidasValues;->fmRadioAppInfo:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/MidasValues;->fmRadioAppInfo:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    return-object v0
.end method

.method public getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 304
    invoke-static {}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->getInstance()Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 305
    return-void
.end method

.method public getHomeAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 123
    const-string/jumbo v0, "car_nav_home_address"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLaunchingClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 127
    const-class v0, Lcom/vlingo/midas/gui/ConversationActivity;

    return-object v0
.end method

.method public getMaxDisplayNumber()I
    .locals 2

    .prologue
    .line 324
    const-string/jumbo v0, "widget_display_max"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getMemoApplicationInfo()Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vlingo/midas/MidasValues;->memoAppInfo:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;->MEMO:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;

    invoke-direct {v0, v1}, Lcom/vlingo/midas/samsungutils/utils/IntegratedAppInfo;-><init>(Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface$IntegrateAppType;)V

    iput-object v0, p0, Lcom/vlingo/midas/MidasValues;->memoAppInfo:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/MidasValues;->memoAppInfo:Lcom/vlingo/core/internal/dialogmanager/util/IntegratedAppInfoInterface;

    return-object v0
.end method

.method public getMmsBodyText(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 404
    sget-object v5, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v5}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v3

    .line 405
    .local v3, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v4, 0x0

    .line 406
    .local v4, "mmsUtil":Lcom/vlingo/core/internal/util/MsgUtil;
    const/4 v1, 0x0

    .line 407
    .local v1, "body":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 409
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/vlingo/core/internal/util/MsgUtil;

    move-object v4, v0

    .line 410
    if-eqz v4, :cond_0

    .line 411
    invoke-interface {v4, p1}, Lcom/vlingo/core/internal/util/MsgUtil;->getMmsTextByCursor(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 423
    :cond_0
    :goto_0
    return-object v1

    .line 416
    :catch_0
    move-exception v2

    .line 417
    .local v2, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v2}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 418
    .end local v2    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v2

    .line 419
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMmsSubject(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 137
    const/16 v0, 0x13

    .line 139
    .local v0, "kitkat":I
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v4, v0, :cond_1

    .line 140
    const-string/jumbo v4, "sub"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 152
    :cond_0
    :goto_0
    return-object v2

    .line 144
    :cond_1
    const-string/jumbo v4, "sub_cs"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 145
    .local v1, "sub_cs":I
    const-string/jumbo v4, "sub"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 146
    .local v2, "subject":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 147
    if-eqz v2, :cond_0

    .line 148
    new-instance v3, Lcom/google/android/mms/pdu/EncodedStringValue;

    invoke-static {v2}, Lcom/google/android/mms/pdu/PduPersister;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lcom/google/android/mms/pdu/EncodedStringValue;-><init>(I[B)V

    .line 149
    .local v3, "v":Lcom/google/android/mms/pdu/EncodedStringValue;
    invoke-virtual {v3}, Lcom/google/android/mms/pdu/EncodedStringValue;->getString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getRegularWidgetMax()I
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x5

    return v0
.end method

.method public getRelativePriority()B
    .locals 1

    .prologue
    .line 543
    const/4 v0, 0x2

    return v0
.end method

.method public getSafeReaderHandler(ZLjava/util/LinkedList;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
    .locals 1
    .param p1, "isSilentHandler"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;)",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;"
        }
    .end annotation

    .prologue
    .line 297
    .local p2, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;-><init>(Z)V

    .line 298
    .local v0, "srh":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;
    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/IncomingAlertHandler;->init(Ljava/util/Queue;)V

    .line 299
    return-object v0
.end method

.method public getSettingsProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 538
    const-string/jumbo v0, "com.vlingo.client.vlingoconfigprovider"

    return-object v0
.end method

.method public getWakeLockManager()Lcom/vlingo/core/internal/display/WakeLockManager;
    .locals 1

    .prologue
    .line 109
    invoke-static {}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->getInstance()Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v0

    return-object v0
.end method

.method public isAppCarModeEnabled()Z
    .locals 1

    .prologue
    .line 160
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v0

    return v0
.end method

.method public isBlockingMode()Z
    .locals 14

    .prologue
    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 367
    const/4 v1, 0x0

    .line 368
    .local v1, "UNCHECK":I
    const/4 v0, 0x1

    .line 369
    .local v0, "CHECK":I
    const/4 v7, 0x0

    .line 370
    .local v7, "isEnabled":Z
    iget-object v10, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "dormant_switch_onoff"

    invoke-static {v10, v11, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 371
    .local v6, "isBlockingModeOn":I
    iget-object v10, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "dormant_disable_notifications"

    invoke-static {v10, v11, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    .line 372
    .local v8, "isNotificationChecked":I
    if-ne v8, v12, :cond_0

    if-ne v6, v12, :cond_0

    .line 373
    iget-object v10, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "dormant_always"

    invoke-static {v10, v11, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 374
    .local v5, "isBlockingModeAlways":I
    if-ne v5, v12, :cond_1

    .line 375
    const/4 v7, 0x1

    .line 398
    .end local v5    # "isBlockingModeAlways":I
    :cond_0
    :goto_0
    return v7

    .line 377
    .restart local v5    # "isBlockingModeAlways":I
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 378
    .local v2, "c":Ljava/util/Calendar;
    const/16 v10, 0xb

    invoke-virtual {v2, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    mul-int/lit8 v10, v10, 0x3c

    const/16 v11, 0xc

    invoke-virtual {v2, v11}, Ljava/util/Calendar;->get(I)I

    move-result v11

    add-int v3, v10, v11

    .line 379
    .local v3, "curTime":I
    iget-object v10, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "dormant_start_hour"

    invoke-static {v10, v11, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    mul-int/lit8 v10, v10, 0x3c

    iget-object v11, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "dormant_start_min"

    invoke-static {v11, v12, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v11

    add-int v9, v10, v11

    .line 382
    .local v9, "startTime":I
    iget-object v10, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "dormant_end_hour"

    invoke-static {v10, v11, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    mul-int/lit8 v10, v10, 0x3c

    iget-object v11, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "dormant_end_min"

    invoke-static {v11, v12, v13}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v11

    add-int v4, v10, v11

    .line 385
    .local v4, "endTime":I
    if-ge v9, v4, :cond_2

    .line 386
    if-lt v3, v9, :cond_0

    if-ge v3, v4, :cond_0

    .line 387
    const/4 v7, 0x1

    goto :goto_0

    .line 389
    :cond_2
    if-le v9, v4, :cond_4

    .line 390
    if-lt v3, v4, :cond_3

    if-lt v3, v9, :cond_0

    .line 391
    :cond_3
    const/4 v7, 0x1

    goto :goto_0

    .line 394
    :cond_4
    const/4 v7, 0x1

    goto :goto_0
.end method

.method public isChinesePhone()Z
    .locals 1

    .prologue
    .line 561
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->isChinesePhone()Z

    move-result v0

    return v0
.end method

.method public isCurrentModelNotExactlySynchronizedWithPlayingTts()Z
    .locals 1

    .prologue
    .line 439
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isShannonAP()Z

    move-result v0

    return v0
.end method

.method public isDSPSeamlessWakeupEnabled()Z
    .locals 1

    .prologue
    .line 549
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSeamlessWakeupStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmbeddedBrowserUsedForSearchResults()Z
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x0

    return v0
.end method

.method public isEyesFree()Z
    .locals 2

    .prologue
    .line 344
    const-string/jumbo v0, "is_eyes_free_mode"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isGearProviderExistOnPhone()Z
    .locals 6

    .prologue
    .line 510
    const/4 v2, 0x0

    .line 511
    .local v2, "svoiceProvider":Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 513
    .local v0, "context":Landroid/content/Context;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string/jumbo v4, "com.samsung.svoiceprovider"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 524
    :goto_0
    if-eqz v2, :cond_0

    const/4 v3, 0x1

    :goto_1
    return v3

    .line 514
    :catch_0
    move-exception v1

    .line 516
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string/jumbo v4, "com.samsung.accessory.saproviders"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_0

    .line 524
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 517
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method public isIUXComplete()Z
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v0

    return v0
.end method

.method public isInDmFlowChanging()Z
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x0

    return v0
.end method

.method public isLanguageChangeAllowed()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    return v0
.end method

.method public isMdsoApplication()Z
    .locals 1

    .prologue
    .line 566
    const/4 v0, 0x1

    return v0
.end method

.method public isMessageReadbackFlowEnabled()Z
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x1

    return v0
.end method

.method public isMessagingLocked()Z
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public isMiniTabletDevice()Z
    .locals 2

    .prologue
    .line 428
    iget-object v1, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 429
    .local v0, "res":Landroid/content/res/Resources;
    sget v1, Lcom/vlingo/midas/R$bool;->isMiniTablet:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    return v1
.end method

.method public isPhoneDrivingModeEnabled()Z
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v0

    return v0
.end method

.method public isReadMessageBodyEnabled()Z
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x1

    return v0
.end method

.method public isSeamless()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 349
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "seamless_wakeup"

    invoke-static {v1, v0}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isTalkbackOn()Z
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/midas/samsungutils/utils/TalkbackUtils;->isTalkbackEnabled(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public isTutorialMode()Z
    .locals 1

    .prologue
    .line 481
    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialOn()Z

    move-result v0

    return v0
.end method

.method public isVideoCallingSupported()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 244
    const-string/jumbo v5, "fake_video_calling"

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 245
    const-string/jumbo v5, "video_calling_supported_value"

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 265
    :cond_0
    :goto_0
    return v4

    .line 248
    :cond_1
    const/4 v4, 0x0

    .line 249
    .local v4, "result":Z
    const/4 v0, 0x0

    .line 253
    .local v0, "enableVoLTE":Z
    new-instance v1, Ljava/io/File;

    const-string/jumbo v5, "/system/lib/libvtmanager.so"

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 254
    .local v1, "libvtmanager":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string/jumbo v5, "/system/lib/libvtstack.so"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 257
    .local v2, "libvtstack":Ljava/io/File;
    new-instance v3, Lcom/vlingo/midas/util/CscFeature;

    invoke-direct {v3}, Lcom/vlingo/midas/util/CscFeature;-><init>()V

    .line 258
    .local v3, "mCscFeature":Lcom/vlingo/midas/util/CscFeature;
    const-string/jumbo v5, "CscFeature_IMS_EnableVoLTE"

    invoke-virtual {v3, v5}, Lcom/vlingo/midas/util/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    .line 261
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    if-eqz v0, :cond_0

    .line 262
    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public isViewCoverOpened()Z
    .locals 1

    .prologue
    .line 220
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCoverOpened()Z

    move-result v0

    return v0
.end method

.method public releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V
    .locals 1
    .param p1, "foregroundListener"    # Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;

    .prologue
    .line 309
    invoke-static {}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->getInstance()Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/safereader/JProjectBackgroundHandler;->releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 310
    return-void
.end method

.method public setInDmFlowChanging(Z)V
    .locals 0
    .param p1, "inDmFlowChanging"    # Z

    .prologue
    .line 476
    return-void
.end method

.method public shouldIncomingMessagesReadout()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 202
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isIUXComplete()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isTOSAccepted()Z

    move-result v2

    if-nez v2, :cond_1

    .line 213
    :cond_0
    :goto_0
    return v0

    .line 206
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/MidasValues;->systemCarModeSettingExists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 209
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->isInForeground()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 213
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/vlingo/midas/MidasValues;->isMessageNotificationSystemSetting()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->isInForeground()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneCarModeEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->isInForeground()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public shouldSetWideBandSpeechToUse16khzVoice()Z
    .locals 1

    .prologue
    .line 446
    const/4 v0, 0x0

    return v0
.end method

.method public showViewCoverUi()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 224
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$layout;->sview_cover_svoice_alwaysmicon:I

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 226
    .local v0, "alwaysView":Landroid/widget/RemoteViews;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.samsung.cover.REMOTEVIEWS_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 227
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "visibility"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 228
    const-string/jumbo v2, "type"

    const-string/jumbo v3, "svoice"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    const-string/jumbo v2, "remote"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 230
    iget-object v2, p0, Lcom/vlingo/midas/MidasValues;->context:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 231
    sput-boolean v4, Lcom/vlingo/midas/settings/MidasSettings;->isShowingViewCoverUi:Z

    .line 232
    return-void
.end method

.method public supportsSVoiceAssociatedServiceOnly()Z
    .locals 1

    .prologue
    .line 434
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    return v0
.end method

.method public updateCurrentLocale(Landroid/content/res/Resources;)V
    .locals 0
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 287
    invoke-static {p1}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 288
    return-void
.end method

.method public useGoogleSearch()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 314
    const-string/jumbo v2, "language"

    const-string/jumbo v3, "en-US"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 315
    .local v0, "currentLanguage":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "ko-KR"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "pt-BR"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "zh-CN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "com.google.android.googlequicksearchbox"

    invoke-static {v2, v1}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method
