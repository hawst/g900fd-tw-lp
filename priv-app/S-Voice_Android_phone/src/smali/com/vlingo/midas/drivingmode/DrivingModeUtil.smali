.class public Lcom/vlingo/midas/drivingmode/DrivingModeUtil;
.super Ljava/lang/Object;
.source "DrivingModeUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static disableAppCarMode()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 50
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->disableCarMode(Landroid/content/SharedPreferences$Editor;)V

    .line 51
    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 52
    return-void
.end method

.method public static disablePhoneDrivingMode(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    invoke-static {p0}, Lcom/vlingo/midas/samsungutils/configuration/DrivingModeUtil;->disablePhoneDrivingMode(Landroid/content/Context;)V

    .line 101
    return-void
.end method

.method public static enableAppCarMode()V
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 41
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->enableCarMode(Landroid/content/SharedPreferences$Editor;)V

    .line 42
    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 43
    return-void
.end method

.method public static enablePhoneDrivingMode(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    invoke-static {p0}, Lcom/vlingo/midas/samsungutils/configuration/DrivingModeUtil;->enablePhoneDrivingMode(Landroid/content/Context;)V

    .line 110
    return-void
.end method

.method public static getPhoneDrivingModeUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 113
    const-string/jumbo v0, "driving_mode_on"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static isAppCarModeEnabled()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public static isDrivingMode()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "driving_mode_on"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 91
    .local v0, "state":I
    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static isPhoneCarModeEnabled()Z
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCarModeSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isSystemCarModeEnabled()Z

    move-result v0

    .line 83
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPhoneDrivingModeEnabled()Z
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerPhoneCarMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isSystemDrivingModeEnabled()Z

    move-result v0

    .line 75
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSystemCarModeEnabled()Z
    .locals 2

    .prologue
    .line 129
    :try_start_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/DrivingModeUtil;->isSystemCarModeEnabled()Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 134
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    :goto_0
    return v1

    .line 130
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_0
    move-exception v0

    .line 134
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method private static isSystemDrivingModeEnabled()Z
    .locals 3

    .prologue
    .line 117
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 119
    .local v0, "context":Landroid/content/Context;
    :try_start_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/configuration/DrivingModeUtil;->isSystemDrivingModeEnabled()Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 123
    :goto_0
    return v2

    .line 120
    :catch_0
    move-exception v1

    .line 121
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 123
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v2

    goto :goto_0
.end method

.method public static togglePhoneDrivingMode(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-static {p0}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->disablePhoneDrivingMode(Landroid/content/Context;)V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-static {p0}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->enablePhoneDrivingMode(Landroid/content/Context;)V

    goto :goto_0
.end method
