.class public final enum Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;
.super Ljava/lang/Enum;
.source "NaverMovieItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/naver/NaverMovieItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GradeInt"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

.field public static final enum MOVIE_GRADE_12_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

.field public static final enum MOVIE_GRADE_15_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

.field public static final enum MOVIE_GRADE_18_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

.field public static final enum MOVIE_GRADE_ALL:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

.field public static final enum MOVIE_GRADE_UNKNOWN:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    const-string/jumbo v1, "MOVIE_GRADE_12_OVER"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_12_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    new-instance v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    const-string/jumbo v1, "MOVIE_GRADE_15_OVER"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_15_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    new-instance v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    const-string/jumbo v1, "MOVIE_GRADE_18_OVER"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_18_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    new-instance v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    const-string/jumbo v1, "MOVIE_GRADE_ALL"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_ALL:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    new-instance v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    const-string/jumbo v1, "MOVIE_GRADE_UNKNOWN"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_UNKNOWN:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    .line 92
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    sget-object v1, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_12_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_15_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_18_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_ALL:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_UNKNOWN:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->$VALUES:[Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 92
    const-class v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->$VALUES:[Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    invoke-virtual {v0}, [Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    return-object v0
.end method
