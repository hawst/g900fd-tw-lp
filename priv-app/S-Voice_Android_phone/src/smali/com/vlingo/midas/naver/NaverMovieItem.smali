.class public Lcom/vlingo/midas/naver/NaverMovieItem;
.super Lcom/vlingo/midas/naver/NaverItem;
.source "NaverMovieItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Hashtable;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p2, "input":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p2}, Lcom/vlingo/midas/naver/NaverItem;-><init>(Ljava/util/Hashtable;)V

    .line 15
    iput-object p1, p0, Lcom/vlingo/midas/naver/NaverMovieItem;->mContext:Landroid/content/Context;

    .line 16
    return-void
.end method


# virtual methods
.method public getActors()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string/jumbo v0, "actors"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBookingUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string/jumbo v0, "bookingUrl"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDirectors()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    const-string/jumbo v0, "directors"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    const-string/jumbo v0, "genre"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getFirstCSV(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGrade()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string/jumbo v0, "grade"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGradeToInt(Ljava/lang/String;)Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;
    .locals 2
    .param p1, "grade"    # Ljava/lang/String;

    .prologue
    .line 98
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 99
    :cond_0
    sget-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_UNKNOWN:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    .line 112
    :goto_0
    return-object v0

    .line 101
    :cond_1
    const-string/jumbo v0, "12"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    sget-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_12_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    goto :goto_0

    .line 103
    :cond_2
    const-string/jumbo v0, "15"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104
    sget-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_15_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    goto :goto_0

    .line 105
    :cond_3
    const-string/jumbo v0, "18"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 106
    sget-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_18_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    goto :goto_0

    .line 107
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverMovieItem;->mContext:Landroid/content/Context;

    sget v1, Lcom/vlingo/midas/R$string;->movie_grade_18_over:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 108
    sget-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_18_OVER:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    goto :goto_0

    .line 109
    :cond_5
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverMovieItem;->mContext:Landroid/content/Context;

    sget v1, Lcom/vlingo/midas/R$string;->movie_grade_all:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 110
    sget-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_ALL:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    goto :goto_0

    .line 112
    :cond_6
    sget-object v0, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->MOVIE_GRADE_UNKNOWN:Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    goto :goto_0
.end method

.method public getOpenDate()Ljava/lang/String;
    .locals 3

    .prologue
    .line 58
    const-string/jumbo v0, "opendate"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    const-string/jumbo v0, "opendate"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2d

    const/16 v2, 0x2e

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getPlayFlags()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "playflags"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPosterUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    const-string/jumbo v0, "poster"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRating()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 74
    const-string/jumbo v0, "rating"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 75
    const-string/jumbo v0, "rating"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public getRunningTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const-string/jumbo v0, "runtime"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSameNameUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const-string/jumbo v0, "samenamesUrl"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServiceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    const-string/jumbo v0, "serviceUrl"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isNowShowing()Z
    .locals 5

    .prologue
    .line 43
    const-string/jumbo v2, "playflag"

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "test":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/naver/NaverMovieItem;->mContext:Landroid/content/Context;

    sget v3, Lcom/vlingo/midas/R$string;->movie_now_showing:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, "test2":Ljava/lang/String;
    const-string/jumbo v2, "playflag"

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/naver/NaverMovieItem;->mContext:Landroid/content/Context;

    sget v4, Lcom/vlingo/midas/R$string;->movie_now_showing:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "playflag"

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 49
    const/4 v2, 0x1

    .line 50
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isOpenSoon()Z
    .locals 3

    .prologue
    .line 35
    const-string/jumbo v0, "playflag"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/naver/NaverMovieItem;->mContext:Landroid/content/Context;

    sget v2, Lcom/vlingo/midas/R$string;->movie_soon:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "playflag"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverMovieItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x1

    .line 39
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
