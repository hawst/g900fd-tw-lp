.class public Lcom/vlingo/midas/naver/NaverLocalItem;
.super Lcom/vlingo/midas/naver/NaverItem;
.source "NaverLocalItem.java"


# instance fields
.field private final MAP_URI:Ljava/lang/String;

.field private final PHONE_URI:Ljava/lang/String;

.field private final ROUTE_URI:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Hashtable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p1, "input":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lcom/vlingo/midas/naver/NaverItem;-><init>(Ljava/util/Hashtable;)V

    .line 7
    const-string/jumbo v0, "phoneNumber"

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverLocalItem;->PHONE_URI:Ljava/lang/String;

    .line 8
    const-string/jumbo v0, "route"

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverLocalItem;->ROUTE_URI:Ljava/lang/String;

    .line 9
    const-string/jumbo v0, "mapImage"

    iput-object v0, p0, Lcom/vlingo/midas/naver/NaverLocalItem;->MAP_URI:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public static getSeparatedLocation(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "orig"    # Ljava/lang/String;

    .prologue
    .line 66
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    if-nez p0, :cond_1

    .line 80
    .end local p1    # "orig":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 69
    .restart local p1    # "orig":Ljava/lang/String;
    :cond_1
    move-object v0, p0

    .line 71
    .local v0, "compareBase":Ljava/lang/String;
    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 72
    .local v1, "comparePosition":I
    :goto_1
    if-lez v1, :cond_0

    .line 73
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 74
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 75
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 77
    :cond_2
    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    goto :goto_1
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string/jumbo v0, "address"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverLocalItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    const-string/jumbo v0, "category"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverLocalItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDistance()Ljava/lang/String;
    .locals 7

    .prologue
    .line 21
    const-string/jumbo v2, "0"

    .line 22
    .local v2, "result":Ljava/lang/String;
    const/4 v3, 0x0

    .line 23
    .local v3, "unit":I
    const/4 v0, 0x0

    .line 24
    .local v0, "decimal":I
    const-string/jumbo v4, "km"

    .line 25
    .local v4, "units":Ljava/lang/String;
    const-string/jumbo v5, "distance"

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/naver/NaverLocalItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 27
    .local v1, "f":F
    const-string/jumbo v5, "distance"

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/naver/NaverLocalItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 28
    const v5, 0x461c4000    # 10000.0f

    cmpl-float v5, v1, v5

    if-lez v5, :cond_2

    float-to-int v5, v1

    div-int/lit16 v3, v5, 0x3e8

    .line 37
    :goto_0
    new-instance v2, Ljava/lang/String;

    .end local v2    # "result":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 38
    .restart local v2    # "result":Ljava/lang/String;
    if-eqz v0, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 39
    :cond_0
    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 41
    :cond_1
    return-object v2

    .line 29
    :cond_2
    const/high16 v5, 0x447a0000    # 1000.0f

    cmpl-float v5, v1, v5

    if-lez v5, :cond_3

    .line 30
    float-to-int v5, v1

    div-int/lit16 v3, v5, 0x3e8

    .line 31
    float-to-int v5, v1

    rem-int/lit16 v5, v5, 0x3e8

    div-int/lit8 v0, v5, 0x64

    goto :goto_0

    .line 33
    :cond_3
    float-to-int v3, v1

    .line 34
    const-string/jumbo v4, "m"

    goto :goto_0
.end method

.method public getMapImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const-string/jumbo v0, "mapImage"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverLocalItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const-string/jumbo v0, "phoneNumber"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverLocalItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRating()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 53
    const-string/jumbo v0, "reviewScore"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverLocalItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    const-string/jumbo v0, "reviewScore"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverLocalItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    .line 56
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public getReviewCount()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 46
    const-string/jumbo v0, "reviewCount"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverLocalItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 47
    const-string/jumbo v0, "reviewCount"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverLocalItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 48
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getRouteUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string/jumbo v0, "route"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverLocalItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
