.class public Lcom/vlingo/midas/naver/NaverItem;
.super Ljava/lang/Object;
.source "NaverItem.java"


# static fields
.field public static final DETAIL_URL:Ljava/lang/String; = "detailUrl"

.field public static final Empty:Ljava/lang/String; = "-"

.field public static final MORE_URL:Ljava/lang/String; = "moreUrl"


# instance fields
.field table:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "input":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/vlingo/midas/naver/NaverItem;->table:Ljava/util/Hashtable;

    .line 16
    return-void
.end method


# virtual methods
.method public getDetailUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string/jumbo v0, "detailUrl"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFirstCSV(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "findKey"    # Ljava/lang/String;

    .prologue
    .line 24
    iget-object v2, p0, Lcom/vlingo/midas/naver/NaverItem;->table:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 26
    iget-object v2, p0, Lcom/vlingo/midas/naver/NaverItem;->table:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 27
    .local v0, "ret":Ljava/lang/String;
    const-string/jumbo v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 28
    .local v1, "splitted":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    .line 31
    .end local v0    # "ret":Ljava/lang/String;
    .end local v1    # "splitted":[Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    const-string/jumbo v2, "-"

    goto :goto_0
.end method

.method public getMoreUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string/jumbo v0, "moreUrl"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "findKey"    # Ljava/lang/String;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverItem;->table:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/naver/NaverItem;->table:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 36
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "findkey"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/naver/NaverItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "-"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/naver/NaverItem;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    const-string/jumbo v0, "title"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/naver/NaverItem;->getStringOrEmptyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
