.class public Lcom/vlingo/midas/ServiceManager;
.super Lcom/vlingo/midas/services/ServicesUtil;
.source "ServiceManager.java"


# static fields
.field private static smInstance:Lcom/vlingo/midas/ServiceManager;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/vlingo/midas/services/ServicesUtil;-><init>()V

    .line 34
    return-void
.end method

.method public static getInstance()Lcom/vlingo/midas/ServiceManager;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/vlingo/midas/ServiceManager;->smInstance:Lcom/vlingo/midas/ServiceManager;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lcom/vlingo/midas/ServiceManager;

    invoke-direct {v0}, Lcom/vlingo/midas/ServiceManager;-><init>()V

    sput-object v0, Lcom/vlingo/midas/ServiceManager;->smInstance:Lcom/vlingo/midas/ServiceManager;

    .line 30
    :cond_0
    sget-object v0, Lcom/vlingo/midas/ServiceManager;->smInstance:Lcom/vlingo/midas/ServiceManager;

    return-object v0
.end method


# virtual methods
.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
