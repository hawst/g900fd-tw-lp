.class Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;
.super Ljava/lang/Object;
.source "MidasADMController.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/MidasADMController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrivingModeSharedPreferenceChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/MidasADMController;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/MidasADMController;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;->this$0:Lcom/vlingo/midas/MidasADMController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/MidasADMController;Lcom/vlingo/midas/MidasADMController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/MidasADMController;
    .param p2, "x1"    # Lcom/vlingo/midas/MidasADMController$1;

    .prologue
    .line 168
    invoke-direct {p0, p1}, Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;-><init>(Lcom/vlingo/midas/MidasADMController;)V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 174
    if-nez p2, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    const-string/jumbo v0, "driving_mode_on"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;->this$0:Lcom/vlingo/midas/MidasADMController;

    # invokes: Lcom/vlingo/midas/MidasADMController;->processOnChange()V
    invoke-static {v0}, Lcom/vlingo/midas/MidasADMController;->access$200(Lcom/vlingo/midas/MidasADMController;)V

    goto :goto_0
.end method
