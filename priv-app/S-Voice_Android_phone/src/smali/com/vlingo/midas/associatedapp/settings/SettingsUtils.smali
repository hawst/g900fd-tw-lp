.class public Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;
.super Ljava/lang/Object;
.source "SettingsUtils.java"


# static fields
.field private static appName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->appName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAppName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 75
    sget-object v0, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->appName:Ljava/lang/String;

    const-string/jumbo v1, " "

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isLaunchedForTosAcceptOtherApp()Z
    .locals 3

    .prologue
    .line 19
    const-string/jumbo v1, "tos_launched_for_tos_other_app"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 22
    .local v0, "toReturn":Z
    return v0
.end method

.method public static isNeedToRemindOfPriorAcceptanceMaster()Z
    .locals 4

    .prologue
    .line 33
    const-string/jumbo v2, "former_tos_acceptance_state"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "state":Ljava/lang/String;
    const/4 v1, 0x1

    .line 35
    .local v1, "toReturn":Z
    if-nez v0, :cond_1

    .line 36
    const/4 v1, 0x0

    .line 42
    :cond_0
    :goto_0
    return v1

    .line 37
    :cond_1
    const-string/jumbo v2, "reminder_done"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "reminder_not_needed"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isNeedToRemindOfPriorAcceptanceOtherApp()Z
    .locals 4

    .prologue
    .line 46
    const-string/jumbo v2, "former_tos_acceptance_state"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "state":Ljava/lang/String;
    const/4 v1, 0x1

    .line 48
    .local v1, "toReturn":Z
    if-eqz v0, :cond_0

    .line 49
    const-string/jumbo v2, "reminder_not_needed"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    const/4 v1, 0x0

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    const-string/jumbo v2, "reminder_done"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isTPhoneGUI()Z
    .locals 3

    .prologue
    .line 61
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "ro.build.product"

    invoke-virtual {v1, v2}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "sales_code":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 63
    const-string/jumbo v1, "tr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "tb"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    :cond_0
    const/4 v1, 0x1

    .line 67
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setAppName(Ljava/lang/String;)V
    .locals 0
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 71
    sput-object p0, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->appName:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public static setLaunchedForTosAcceptOtherApp(Z)V
    .locals 1
    .param p0, "b"    # Z

    .prologue
    .line 26
    const-string/jumbo v0, "tos_launched_for_tos_other_app"

    invoke-static {v0, p0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 30
    return-void
.end method
