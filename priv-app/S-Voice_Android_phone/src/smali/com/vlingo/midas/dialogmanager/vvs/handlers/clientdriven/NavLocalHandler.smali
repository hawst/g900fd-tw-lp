.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "NavLocalHandler.java"


# static fields
.field private static final PARAM_LOCALE:Ljava/lang/String; = "Locale"

.field private static final PARAM_LOCATION:Ljava/lang/String; = "Location"

.field private static final VALUE_KOREAN:Ljava/lang/String; = "Korean"

.field private static final VALUE_OFFICE:Ljava/lang/String; = "office"

.field private static final VALUE_RECENT:Ljava/lang/String; = "recentdestination"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 12
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 39
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    .line 40
    .local v1, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->doesLocationUseNetworkProvider()Z

    move-result v9

    if-nez v9, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->core_not_detected_current_location:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 42
    const/4 v9, 0x0

    .line 152
    :goto_0
    return v9

    .line 44
    :cond_0
    const-string/jumbo v9, "Location"

    const/4 v10, 0x1

    invoke-static {p1, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 45
    .local v5, "location":Ljava/lang/String;
    const-string/jumbo v9, "Locale"

    const/4 v10, 0x0

    invoke-static {p1, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 46
    .local v4, "locale":Ljava/lang/String;
    const/4 v8, 0x0

    .line 47
    .local v8, "versionName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 48
    .local v6, "parts":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 49
    .local v3, "intentToLaunch":Landroid/content/Intent;
    const/4 v7, 0x0

    .line 51
    .local v7, "version":I
    const-string/jumbo v9, "car_nav_office_address"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "addr":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string/jumbo v9, "Korean"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 57
    const-string/jumbo v9, "office"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 62
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 64
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string/jumbo v10, "com.skt.skaf.l001mtm091"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    iget-object v8, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 65
    const-string/jumbo v9, "\\."

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v6

    .line 70
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->navigate_office:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 71
    const-string/jumbo v9, "com.skt.skaf.l001mtm091"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_3

    const/4 v9, 0x0

    aget-object v9, v6, v9

    const-string/jumbo v10, "3"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 72
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 73
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.skt.skaf.l001mtm091"

    const-string/jumbo v10, "com.skt.tmap.activity.TmapIntroActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "tmap://search?name="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 133
    :cond_1
    :goto_2
    if-eqz v3, :cond_2

    .line 134
    const/high16 v9, 0x10000000

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 135
    const-string/jumbo v9, "kt.navi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 136
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 152
    :cond_2
    :goto_3
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 75
    :cond_3
    const-string/jumbo v9, "kt.navi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 76
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "kt.navi.OLLEH_NAVIGATION"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const/16 v9, 0x20

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 78
    const-string/jumbo v9, "CALLER_PACKAGE_NAME"

    const-string/jumbo v10, "com.vlingo.midas"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const-string/jumbo v9, "EXTERN_LINK_TYPE"

    const/4 v10, 0x1

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 80
    const-string/jumbo v9, "LINK_SEARCH_WORD"

    invoke-virtual {v3, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 81
    :cond_4
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 83
    :try_start_1
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string/jumbo v10, "com.mnsoft.lgunavi"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    iget v7, v9, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 87
    :goto_4
    const/4 v9, 0x1

    if-le v7, v9, :cond_5

    .line 88
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.mappy.MAPPYSMART_EXTERNAL_SERVICE"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 89
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.mappy.action.REQUEST_TO_MAPPYSMART"

    const/high16 v10, 0x10000

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 90
    const-string/jumbo v9, "com.mnsoft.mappy.extra.EXTRA_KIND"

    const-string/jumbo v10, "poi"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    const-string/jumbo v9, "com.mnsoft.mappy.extra.EXTRA_VALUE"

    invoke-virtual {v3, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 84
    :catch_0
    move-exception v2

    .line 85
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_4

    .line 93
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_5
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 94
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const-string/jumbo v10, "com.mnsoft.offboardnavi.DispatcherActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 97
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->navigate_office_notsupport:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 100
    :cond_7
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->navigate_office_fail:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 102
    :cond_8
    const-string/jumbo v9, "recentdestination"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 107
    const-string/jumbo v9, "kt.navi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 108
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->navigate_recentdest:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 109
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "kt.navi.OLLEH_NAVIGATION"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 110
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const/16 v9, 0x20

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 111
    const-string/jumbo v9, "CALLER_PACKAGE_NAME"

    const-string/jumbo v10, "com.vlingo.midas"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string/jumbo v9, "EXTERN_LINK_TYPE"

    const/4 v10, 0x3

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_2

    .line 113
    :cond_9
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 114
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->navigate_recentdest:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 116
    :try_start_2
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string/jumbo v10, "com.mnsoft.lgunavi"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    iget v7, v9, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 120
    :goto_5
    const/4 v9, 0x1

    if-le v7, v9, :cond_a

    .line 121
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.mappy.MAPPYSMART_EXTERNAL_SERVICE"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 122
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.mappy.action.REQUEST_TO_MAPPYSMART"

    const/high16 v10, 0x10000

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 123
    const-string/jumbo v9, "com.mnsoft.mappy.extra.EXTRA_KIND"

    const-string/jumbo v10, "recent"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 117
    :catch_1
    move-exception v2

    .line 118
    .restart local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_5

    .line 125
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_a
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 126
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const-string/jumbo v10, "com.mnsoft.offboardnavi.DispatcherActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 129
    :cond_b
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->navigate_recentdest_notsupport:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 140
    :cond_c
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_d

    const/4 v9, 0x1

    if-le v7, v9, :cond_d

    .line 141
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->service(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto/16 :goto_3

    .line 146
    :cond_d
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto/16 :goto_3

    .line 66
    :catch_2
    move-exception v9

    goto/16 :goto_1
.end method
