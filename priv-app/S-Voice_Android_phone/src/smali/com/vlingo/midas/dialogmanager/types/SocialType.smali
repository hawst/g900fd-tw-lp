.class public Lcom/vlingo/midas/dialogmanager/types/SocialType;
.super Ljava/lang/Object;
.source "SocialType.java"


# instance fields
.field private message:Ljava/lang/String;

.field private socialNetwork:Lcom/vlingo/midas/social/api/SocialNetworkType;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/social/api/SocialNetworkType;Ljava/lang/String;)V
    .locals 0
    .param p1, "socialNetwork"    # Lcom/vlingo/midas/social/api/SocialNetworkType;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/types/SocialType;->socialNetwork:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 16
    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/types/SocialType;->message:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/types/SocialType;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getSocialNetwork()Lcom/vlingo/midas/social/api/SocialNetworkType;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/types/SocialType;->socialNetwork:Lcom/vlingo/midas/social/api/SocialNetworkType;

    return-object v0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/types/SocialType;->message:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setSocialNetwork(Lcom/vlingo/midas/social/api/SocialNetworkType;)V
    .locals 0
    .param p1, "socialNetwork"    # Lcom/vlingo/midas/social/api/SocialNetworkType;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/types/SocialType;->socialNetwork:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 25
    return-void
.end method
