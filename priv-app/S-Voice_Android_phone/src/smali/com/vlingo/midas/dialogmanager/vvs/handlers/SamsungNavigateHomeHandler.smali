.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;
.super Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavBaseHandler;
.source "SamsungNavigateHomeHandler.java"


# static fields
.field private static final ACTION_NAVIGATE_CHINESE:Ljava/lang/String; = "com.autonavi.xmgd.action.NAVIGATE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavBaseHandler;-><init>()V

    return-void
.end method

.method private doExecuteActionOriginal(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p3, "addr"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 78
    invoke-static {p3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_navigate_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 80
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    invoke-direct {p0, p3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->executeChineseNavigation(Ljava/lang/String;)V

    .line 96
    :goto_0
    return v6

    .line 84
    :cond_0
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "google.navigation:q="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    const-string/jumbo v5, "+"

    invoke-virtual {p3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto :goto_0

    .line 91
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "shownText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 94
    .local v1, "spokenText":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doExecuteActionSamsung(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Z
    .locals 12
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p3, "addr"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    .line 101
    .local v1, "context":Landroid/content/Context;
    const/4 v8, 0x0

    .line 102
    .local v8, "versionName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 103
    .local v4, "parts":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 104
    .local v3, "intentToLaunch":Landroid/content/Intent;
    const/4 v7, 0x0

    .line 106
    .local v7, "version":I
    invoke-static {p3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 107
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_navigate_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 109
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string/jumbo v10, "com.skt.skaf.l001mtm091"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    iget-object v8, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 110
    const-string/jumbo v9, "\\."

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 115
    :goto_0
    const-string/jumbo v9, "com.skt.skaf.l001mtm091"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v9, 0x0

    aget-object v9, v4, v9

    const-string/jumbo v10, "3"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 116
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 117
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.skt.skaf.l001mtm091"

    const-string/jumbo v10, "com.skt.tmap.activity.TmapIntroActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "tmap://search?name="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 166
    :goto_1
    if-eqz v3, :cond_0

    .line 167
    const/high16 v9, 0x10000000

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 168
    const-string/jumbo v9, "kt.navi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 169
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 185
    :cond_0
    :goto_2
    const/4 v9, 0x0

    :goto_3
    return v9

    .line 119
    :cond_1
    const-string/jumbo v9, "kt.navi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 120
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "kt.navi.OLLEH_NAVIGATION"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 121
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const/16 v9, 0x20

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 122
    const-string/jumbo v9, "CALLER_PACKAGE_NAME"

    const-string/jumbo v10, "com.vlingo.midas"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    const-string/jumbo v9, "EXTERN_LINK_TYPE"

    const/4 v10, 0x1

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 124
    const-string/jumbo v9, "LINK_SEARCH_WORD"

    invoke-virtual {v3, v9, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 125
    :cond_2
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 127
    :try_start_1
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string/jumbo v10, "com.mnsoft.lgunavi"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    iget v7, v9, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 132
    :goto_4
    const/4 v9, 0x1

    if-le v7, v9, :cond_3

    .line 133
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.mappy.MAPPYSMART_EXTERNAL_SERVICE"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 134
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.mappy.action.REQUEST_TO_MAPPYSMART"

    const/high16 v10, 0x10000

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 135
    const-string/jumbo v9, "com.mnsoft.mappy.extra.EXTRA_KIND"

    const-string/jumbo v10, "poi"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    const-string/jumbo v9, "com.mnsoft.mappy.extra.EXTRA_VALUE"

    invoke-virtual {v3, v9, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 138
    :cond_3
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 139
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const-string/jumbo v10, "com.mnsoft.offboardnavi.DispatcherActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 142
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 143
    invoke-direct {p0, p3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->getChineseNavigationIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    goto/16 :goto_1

    .line 151
    :cond_5
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 152
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "android.intent.action.VIEW"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string/jumbo v9, "com.google.android.apps.maps"

    const-string/jumbo v10, "com.google.android.maps.driveabout.app.NavigationActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "google.navigation:q="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    const-string/jumbo v11, "+"

    invoke-virtual {p3, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "arg":Ljava/lang/String;
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 156
    .local v2, "data":Landroid/net/Uri;
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 160
    .end local v0    # "arg":Ljava/lang/String;
    .end local v2    # "data":Landroid/net/Uri;
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v5

    .line 161
    .local v5, "shownText":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v6

    .line 162
    .local v6, "spokenText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 173
    .end local v5    # "shownText":Ljava/lang/String;
    .end local v6    # "spokenText":Ljava/lang/String;
    :cond_7
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_8

    const/4 v9, 0x1

    if-le v7, v9, :cond_8

    .line 174
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->service(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto/16 :goto_2

    .line 179
    :cond_8
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto/16 :goto_2

    .line 128
    :catch_0
    move-exception v9

    goto/16 :goto_4

    .line 111
    :catch_1
    move-exception v9

    goto/16 :goto_0
.end method

.method private executeChineseNavigation(Ljava/lang/String;)V
    .locals 4
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    .line 200
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasAutonavi()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 201
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    const-string/jumbo v2, "com.autonavi.xmgd.action.NAVIGATE"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "target,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasBaiduMaps()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 206
    invoke-static {p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getNaviagateIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 207
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto :goto_0

    .line 210
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->isENaviEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 211
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-static {p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getNavigateIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto :goto_0
.end method

.method private getChineseNavigationIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    .line 189
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasAutonavi()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.autonavi.xmgd.action.NAVIGATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "target"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 196
    :goto_0
    return-object v0

    .line 191
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasBaiduMaps()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    invoke-static {p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getNaviagateIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 193
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->isENaviEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 194
    invoke-static {p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getNavigateIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 196
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 11
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v7, 0x0

    .line 49
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavBaseHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 50
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getHomeAddress()Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "addr":Ljava/lang/String;
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    .line 53
    .local v1, "context":Landroid/content/Context;
    const-string/jumbo v8, "location"

    invoke-virtual {v1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/LocationManager;

    .line 54
    .local v3, "lm":Landroid/location/LocationManager;
    const-string/jumbo v8, "network"

    invoke-virtual {v3, v8}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    .line 56
    .local v2, "isGpsEnable":Z
    if-nez v2, :cond_0

    .line 57
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$string;->core_navigation_default_location:I

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 58
    .local v6, "prompt":Ljava/lang/String;
    invoke-interface {p2, v6, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .end local v1    # "context":Landroid/content/Context;
    .end local v6    # "prompt":Ljava/lang/String;
    :goto_0
    return v7

    .line 62
    .restart local v1    # "context":Landroid/content/Context;
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v5

    .line 63
    .local v5, "mLocale":Ljava/util/Locale;
    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    .line 65
    .local v4, "mCurrentLanguage":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->doesLocationUseNetworkProvider()Z

    move-result v8

    if-nez v8, :cond_1

    .line 66
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v8

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$string;->core_not_detected_current_location:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_1
    instance-of v7, v1, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v7, :cond_3

    const-string/jumbo v7, "ko_KR"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    check-cast v1, Lcom/vlingo/midas/gui/ConversationActivity;

    .end local v1    # "context":Landroid/content/Context;
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->isDrivingMode()Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    move-result-object v7

    sget-object v8, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    if-ne v7, v8, :cond_3

    .line 71
    :cond_2
    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->doExecuteActionSamsung(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Z

    move-result v7

    goto :goto_0

    .line 73
    :cond_3
    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;->doExecuteActionOriginal(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Z

    move-result v7

    goto :goto_0
.end method
