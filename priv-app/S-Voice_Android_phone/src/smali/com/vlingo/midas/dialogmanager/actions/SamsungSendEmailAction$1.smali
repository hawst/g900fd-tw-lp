.class Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;
.super Ljava/lang/Object;
.source "SamsungSendEmailAction.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    invoke-static {p2}, Lcom/android/email/backgroundsender/IEmailBackgroundSender$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    move-result-object v1

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$502(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Lcom/android/email/backgroundsender/IEmailBackgroundSender;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    .line 107
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    const/4 v1, 0x1

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mSvcConnected:Z
    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$602(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Z)Z

    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$500(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$500(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/email/backgroundsender/IEmailBackgroundSender;->startListening()V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->sendMessage()V
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$700(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    return-void

    .line 115
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 127
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$500(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/email/backgroundsender/IEmailBackgroundSender;->stopListening()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$502(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Lcom/android/email/backgroundsender/IEmailBackgroundSender;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    .line 133
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mSvcConnected:Z
    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$602(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Z)Z

    .line 134
    return-void

    .line 128
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
