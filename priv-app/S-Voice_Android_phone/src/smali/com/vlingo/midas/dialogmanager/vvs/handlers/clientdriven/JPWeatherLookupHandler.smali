.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;
.source "JPWeatherLookupHandler.java"


# static fields
.field private static final weatherCodeToStringArray:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final weatherCodeTodayToStringArray:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private index:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    .line 29
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_sunny:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_partly_sunny:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mostly_cloudy:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Fog:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_foggy:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_showers:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_partly_sunny_with_showers:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_thunderstorms:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithThunderShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mostly_cloudy_with_thunder_showers:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_rainy:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_flurries:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mostly_cloudy_with_flurries:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_snow:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_ice:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mixed_rain_and_snow:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_hot:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Cold:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_cold:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Windy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_windy:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Clear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_clear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_moslty_clear:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyNight:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mostly_cloudy_night:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    .line 52
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_sunny_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunny:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_partly_sunny_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mostly_cloudy_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Fog:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_foggy_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Showers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_showers_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->PartlySunnyWithShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_partly_sunny_with_showers_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Thunderstorms:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_thunderstorms_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithThunderShowers:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mostly_cloudy_with_thunder_showers_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Rain:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_rainy_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Flurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_flurries_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyWithFlurries:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mostly_cloudy_with_flurries_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Snow:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_snow_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Ice:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_ice_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->RainAndSnowMixed:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mixed_rain_and_snow_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Hot:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_hot_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Cold:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_cold_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Windy:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_windy_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->Clear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_clear_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MosltyClear:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_moslty_clear_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;->MostlyCloudyNight:Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mostly_cloudy_night:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;-><init>()V

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->index:I

    return-void
.end method

.method private getFormattedForecast(Ljava/lang/Integer;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "stringRes"    # I
    .param p3, "code"    # I
    .param p4, "date"    # Ljava/lang/String;
    .param p5, "high_temp"    # Ljava/lang/String;
    .param p6, "cur_temp"    # Ljava/lang/String;

    .prologue
    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 100
    .local v0, "replacements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v1, "day"

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    const-string/jumbo v1, "temp"

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    const-string/jumbo v1, "cur_temp"

    invoke-interface {v0, v1, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->getWeatherStringFromArrayRes(Ljava/lang/Integer;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/util/StringUtils;->replaceTokens(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected getMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;
    .locals 10
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "date"    # Ljava/lang/String;
    .param p3, "city"    # Ljava/lang/String;
    .param p4, "state"    # Ljava/lang/String;
    .param p5, "high_temp"    # Ljava/lang/String;
    .param p6, "cur_temp"    # Ljava/lang/String;
    .param p7, "weatherCode"    # I
    .param p8, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    .prologue
    .line 121
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    move-object/from16 v0, p8

    if-ne v0, v1, :cond_0

    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->getAdaptor()Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getProvider()Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-result-object v2

    move/from16 v0, p7

    invoke-static {v0, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil;->WeatherCodeToWeatherType(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 122
    .local v8, "stringRes":Ljava/lang/Integer;
    if-nez v8, :cond_1

    .line 123
    invoke-super/range {p0 .. p8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;

    move-result-object v9

    .line 127
    .local v9, "value":Ljava/lang/String;
    :goto_1
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->capitalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 121
    .end local v8    # "stringRes":Ljava/lang/Integer;
    .end local v9    # "value":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    goto :goto_0

    .line 125
    .restart local v8    # "stringRes":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v1, p0

    move-object v2, p1

    move/from16 v4, p7

    move-object v5, p2

    move-object v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->getFormattedForecast(Ljava/lang/Integer;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "value":Ljava/lang/String;
    goto :goto_1
.end method

.method protected getOneDaySpokenMsg(Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "weatherInfoUtil"    # Lcom/vlingo/core/internal/weather/WeatherInfoUtil;
    .param p3, "day"    # Ljava/lang/String;
    .param p4, "date"    # Ljava/lang/String;
    .param p5, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;
    .param p6, "generalMsg"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    .line 132
    .local v0, "context":Landroid/content/Context;
    instance-of v3, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v3, :cond_0

    check-cast v0, Lcom/vlingo/midas/gui/ConversationActivity;

    .end local v0    # "context":Landroid/content/Context;
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->isDrivingMode()Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    move-result-object v3

    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    if-ne v3, v4, :cond_0

    const/4 v1, 0x1

    .line 134
    .local v1, "inDrivingMode":Z
    :goto_0
    invoke-super/range {p0 .. p6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getOneDaySpokenMsg(Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 135
    .local v2, "msgEndPart":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 136
    const/4 v3, 0x0

    .line 138
    :goto_1
    return-object v3

    .line 132
    .end local v1    # "inDrivingMode":Z
    .end local v2    # "msgEndPart":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 138
    .restart local v1    # "inDrivingMode":Z
    .restart local v2    # "msgEndPart":Ljava/lang/String;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v1, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$string;->space:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-super/range {p0 .. p6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getOneDaySpokenMsg(Ljava/lang/Integer;Lcom/vlingo/core/internal/weather/WeatherInfoUtil;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    const-string/jumbo v3, ""

    goto :goto_2
.end method

.method protected getSpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;
    .locals 10
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "forecast"    # Ljava/lang/String;
    .param p3, "date"    # Ljava/lang/String;
    .param p4, "city"    # Ljava/lang/String;
    .param p5, "state"    # Ljava/lang/String;
    .param p6, "high_temp"    # Ljava/lang/String;
    .param p7, "cur_temp"    # Ljava/lang/String;
    .param p8, "weatherCode"    # I
    .param p9, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    .prologue
    .line 109
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;

    move-object/from16 v0, p9

    if-ne v0, v1, :cond_0

    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->getAdaptor()Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getProvider()Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-result-object v2

    move/from16 v0, p8

    invoke-static {v0, v2}, Lcom/vlingo/midas/util/WeatherResourceUtil;->WeatherCodeToWeatherType(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)Lcom/vlingo/midas/util/WeatherResourceUtil$WeatherType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 110
    .local v8, "stringRes":Ljava/lang/Integer;
    if-nez v8, :cond_1

    .line 111
    invoke-super/range {p0 .. p9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler;->getSpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/WeatherLookupHandler$DateType;)Ljava/lang/String;

    move-result-object v9

    .line 115
    .local v9, "value":Ljava/lang/String;
    :goto_1
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->capitalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 109
    .end local v8    # "stringRes":Ljava/lang/Integer;
    .end local v9    # "value":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    goto :goto_0

    .line 113
    .restart local v8    # "stringRes":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v1, p0

    move-object v2, p1

    move/from16 v4, p8

    move-object v5, p3

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v1 .. v7}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->getFormattedForecast(Ljava/lang/Integer;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "value":Ljava/lang/String;
    goto :goto_1
.end method

.method getWeatherStringFromArrayRes(Ljava/lang/Integer;I)Ljava/lang/String;
    .locals 6
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "code"    # I

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "strList":[Ljava/lang/String;
    if-nez p1, :cond_1

    .line 81
    iget v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->index:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 82
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    array-length v4, v0

    int-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->index:I

    .line 84
    :cond_0
    iget v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->index:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 92
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v1, v0, v2

    .line 95
    .local v1, "toReturn":Ljava/lang/String;
    return-object v1

    .line 88
    .end local v1    # "toReturn":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;->index:I

    goto :goto_0
.end method
