.class Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$1;
.super Landroid/content/BroadcastReceiver;
.source "SocialUpdateController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$1;->this$0:Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 53
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.controllers.socialupdatecontroller"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 89
    :goto_0
    return-void

    .line 60
    :cond_0
    :try_start_0
    const-string/jumbo v3, "extra_controller_login_result"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 61
    # getter for: Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventReceiverRegistered:Z
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->access$000()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 62
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$1;->this$0:Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    # getter for: Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventComplete:Landroid/content/BroadcastReceiver;
    invoke-static {v4}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->access$100(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Landroid/content/BroadcastReceiver;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 63
    const/4 v3, 0x0

    # setter for: Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventReceiverRegistered:Z
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->access$002(Z)Z

    .line 65
    :cond_1
    const-string/jumbo v3, "extra_controller_login_result"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 66
    .local v1, "didLogin":Z
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$1;->this$0:Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    # invokes: Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->access$200(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    .line 68
    .local v0, "currentController":Ljava/lang/Object;
    instance-of v3, v0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    if-eqz v3, :cond_3

    .line 69
    if-eqz v1, :cond_4

    .line 70
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$1;->this$0:Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    # invokes: Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->access$300(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_STATUS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v4}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 73
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getVlingoApp()Lcom/vlingo/core/internal/util/VlingoApplicationInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/util/VlingoApplicationInterface;->isAppInForeground()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_2

    .line 74
    const-wide/16 v3, 0xc8

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :cond_2
    :goto_1
    :try_start_2
    check-cast v0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    .end local v0    # "currentController":Ljava/lang/Object;
    # invokes: Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->doSocialUpdateAction()Z
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->access$400(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 87
    .end local v1    # "didLogin":Z
    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$1;->this$0:Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    # invokes: Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->access$600(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    goto :goto_0

    .line 75
    .restart local v0    # "currentController":Ljava/lang/Object;
    .restart local v1    # "didLogin":Z
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 87
    .end local v0    # "currentController":Ljava/lang/Object;
    .end local v1    # "didLogin":Z
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$1;->this$0:Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    # invokes: Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v4}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->access$600(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v3

    .line 81
    .restart local v0    # "currentController":Ljava/lang/Object;
    .restart local v1    # "didLogin":Z
    :cond_4
    :try_start_4
    check-cast v0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    .end local v0    # "currentController":Ljava/lang/Object;
    # invokes: Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->reset()V
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->access$500(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method
