.class public Lcom/vlingo/midas/dialogmanager/actions/DeleteMemoAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "DeleteMemoAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteMemoInterface;


# instance fields
.field private memo:Lcom/vlingo/core/internal/memo/Memo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 5

    .prologue
    .line 32
    :try_start_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/MemoManager;->getMemoUtil()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v1

    .line 35
    .local v1, "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteMemoAction;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/DeleteMemoAction;->memo:Lcom/vlingo/core/internal/memo/Memo;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v3

    int-to-long v3, v3

    invoke-interface {v1, v2, v3, v4}, Lcom/vlingo/core/internal/memo/IMemoUtil;->deleteMemo(Landroid/content/Context;J)V

    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteMemoAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Lcom/vlingo/core/internal/memo/MemoUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    .end local v1    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Lcom/vlingo/core/internal/memo/MemoUtilException;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/memo/MemoUtilException;->printStackTrace()V

    .line 43
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteMemoAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/memo/MemoUtilException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public memo(Lcom/vlingo/core/internal/memo/Memo;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteMemoInterface;
    .locals 0
    .param p1, "memo"    # Lcom/vlingo/core/internal/memo/Memo;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/DeleteMemoAction;->memo:Lcom/vlingo/core/internal/memo/Memo;

    .line 26
    return-object p0
.end method
