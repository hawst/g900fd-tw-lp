.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;
.source "SamsungContactLookupHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;
    }
.end annotation


# static fields
.field private static staticContactMatchClone:Lcom/vlingo/core/internal/contacts/ContactMatch;


# instance fields
.field private birthdayType:Ljava/lang/String;

.field contactDetail:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field contactMatchDetail:Lcom/vlingo/core/internal/contacts/ContactMatch;

.field private maxWidget:I

.field private systemTurn:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->staticContactMatchClone:Lcom/vlingo/core/internal/contacts/ContactMatch;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;-><init>()V

    .line 46
    const-string/jumbo v0, "birthday"

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->birthdayType:Ljava/lang/String;

    .line 47
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    .line 48
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getRegularWidgetMax()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetail:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 121
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getSearchType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getContactNotFoundString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Ljava/util/List;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->hasContactsData(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getTurnContactsDoNotType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V

    return-void
.end method

.method private createSystemTurn(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 4
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 296
    const/4 v1, 0x0

    .line 297
    .local v1, "newDecor":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    const/4 v0, 0x0

    .line 298
    .local v0, "contactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v2, v3, :cond_2

    .line 299
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v0

    .line 300
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowPhone()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    .line 312
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 313
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getTurnDoNotInformation()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    .line 318
    :goto_1
    return-void

    .line 301
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v2, v3, :cond_3

    .line 302
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAddressData()Ljava/util/List;

    move-result-object v0

    .line 303
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowAddress()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    goto :goto_0

    .line 304
    :cond_3
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v2, v3, :cond_4

    .line 305
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getEmailData()Ljava/util/List;

    move-result-object v0

    .line 306
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowEmail()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    goto :goto_0

    .line 307
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v2, v3, :cond_0

    .line 308
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBirthdayData()Ljava/util/List;

    move-result-object v0

    .line 309
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeContactShowBirthday()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v1

    goto :goto_0

    .line 317
    :cond_5
    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->queryShowType(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)V

    goto :goto_1
.end method

.method private filterPhonesByType(ILjava/util/List;)Ljava/util/List;
    .locals 2
    .param p1, "requestedTypes"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591
    .local p2, "phonesList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 592
    .local v0, "filteredPhonesList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)V

    invoke-static {p2, v1, v0}, Lcom/vlingo/sdk/util/CollectionUtils;->select(Ljava/lang/Iterable;Lcom/vlingo/sdk/util/Predicate;Ljava/util/Collection;)V

    .line 602
    return-object v0
.end method

.method private getBirthday(Lcom/vlingo/core/internal/contacts/ContactMatch;)Ljava/lang/String;
    .locals 4
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 606
    const/4 v0, 0x0

    .line 607
    .local v0, "birthday":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBirthdayData()Ljava/util/List;

    move-result-object v1

    .line 608
    .local v1, "birthdayData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 609
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 610
    .local v2, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v0, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    .line 612
    .end local v2    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_0
    return-object v0
.end method

.method private getSearchType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)I
    .locals 5
    .param p1, "mWidgetDecor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 616
    if-nez p1, :cond_1

    .line 640
    :cond_0
    :goto_0
    return v0

    .line 618
    :cond_1
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v3, v4, :cond_4

    .line 619
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v3}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 620
    goto :goto_0

    .line 621
    :cond_2
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowMobilePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v0, v2

    .line 622
    goto :goto_0

    .line 623
    :cond_3
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 624
    const/4 v0, 0x3

    goto :goto_0

    .line 626
    :cond_4
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v3, v4, :cond_6

    .line 627
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomeAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v3}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v1

    .line 628
    goto :goto_0

    .line 629
    :cond_5
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 630
    goto :goto_0

    .line 632
    :cond_6
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v3, v4, :cond_0

    .line 633
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomeEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v3}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v3

    if-eqz v3, :cond_7

    move v0, v1

    .line 635
    goto :goto_0

    .line 636
    :cond_7
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 637
    goto :goto_0
.end method

.method private getTurnContactsDoNotType()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 534
    const/4 v0, 0x0

    .line 535
    .local v0, "turn":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v1, v2, :cond_1

    .line 536
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_do_not_phone:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 544
    :cond_0
    :goto_0
    return-object v0

    .line 537
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v1, v2, :cond_2

    .line 538
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_do_not_address:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 539
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v1, v2, :cond_3

    .line 540
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_do_not_email:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 541
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v2, Lcom/vlingo/core/internal/contacts/ContactType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v1, v2, :cond_0

    .line 542
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_do_not_birthday:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getTurnDoNoTypeInformation()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 548
    const/4 v0, 0x0

    .line 549
    .local v0, "turn":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 550
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getTypeData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 551
    .local v1, "typeData":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v2, v3, :cond_1

    .line 552
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_phone_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 559
    .end local v1    # "typeData":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 553
    .restart local v1    # "typeData":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v2, v3, :cond_2

    .line 554
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_address_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 555
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v2, v3, :cond_0

    .line 556
    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_email_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getTurnDoNotInformation()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 563
    const/4 v0, 0x0

    .line 564
    .local v0, "turn":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 565
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v2, "pn"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 566
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_phone:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 574
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 576
    :cond_1
    return-object v0

    .line 567
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v2, "email"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 568
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_email:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 569
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v2, "address"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 570
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_address:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 571
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v2, "birthday"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 572
    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_birthday:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getTurnFoundTypeData(Ljava/util/List;)Ljava/lang/String;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "contactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 389
    const/4 v2, 0x0

    .line 390
    .local v2, "prompt":Ljava/lang/String;
    const/4 v3, 0x0

    .line 391
    .local v3, "searchTypeCount":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 392
    .local v0, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    iget v4, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-direct {p0, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getSearchType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 393
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 396
    .end local v0    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-direct {p0, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getSearchType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)I

    move-result v4

    invoke-direct {p0, v4, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->filterPhonesByType(ILjava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactDetail:Ljava/util/List;

    .line 397
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "pn"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 398
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    iget v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v4, v5, :cond_5

    .line 399
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_phone_number_large_list:I

    new-array v6, v11, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 411
    :goto_1
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "hpn"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 412
    iget v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v3, v4, :cond_7

    .line 413
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_home_phone_found_large_list:I

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 425
    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "mpn"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 426
    iget v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v3, v4, :cond_9

    .line 427
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_mobile_phone_found_large_list:I

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 439
    :cond_3
    :goto_3
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "wpn"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 440
    iget v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v3, v4, :cond_b

    .line 441
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_work_phone_found_large_list:I

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 530
    :cond_4
    :goto_4
    return-object v2

    .line 403
    :cond_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v8, :cond_6

    .line 404
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_phone_number_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 407
    :cond_6
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_phone_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 417
    :cond_7
    if-ne v3, v8, :cond_8

    .line 418
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_home_phone_found_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 421
    :cond_8
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 431
    :cond_9
    if-ne v3, v8, :cond_a

    .line 432
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_mobile_phone_found_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 435
    :cond_a
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_mobile_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 445
    :cond_b
    if-ne v3, v8, :cond_c

    .line 446
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_work_phone_found_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 449
    :cond_c
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 453
    :cond_d
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "email"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 454
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    iget v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v4, v5, :cond_f

    .line 455
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_email_large_list:I

    new-array v6, v11, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 463
    :goto_5
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "homeemail"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 464
    iget v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v3, v4, :cond_11

    .line 465
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_home_email_found_large_list:I

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 477
    :cond_e
    :goto_6
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "workemail"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 478
    iget v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v3, v4, :cond_13

    .line 479
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_work_email_found_large_list:I

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 457
    :cond_f
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v8, :cond_10

    .line 458
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_email_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 460
    :cond_10
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_email:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 469
    :cond_11
    if-ne v3, v8, :cond_12

    .line 470
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_home_email_found_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 473
    :cond_12
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_email_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 483
    :cond_13
    if-ne v3, v8, :cond_14

    .line 484
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_work_email_found_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 487
    :cond_14
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_email_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 491
    :cond_15
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "address"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 492
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    iget v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v4, v5, :cond_17

    .line 493
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_address_large_list:I

    new-array v6, v11, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 501
    :goto_7
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "workaddress"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 502
    iget v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v3, v4, :cond_19

    .line 503
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_work_address_found_large_list:I

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 515
    :cond_16
    :goto_8
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "homeaddress"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 516
    iget v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v3, v4, :cond_1b

    .line 517
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_home_address_found_large_list:I

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 495
    :cond_17
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v8, :cond_18

    .line 496
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_address_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7

    .line 498
    :cond_18
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_address:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7

    .line 507
    :cond_19
    if-ne v3, v8, :cond_1a

    .line 508
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_work_address_found_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    .line 511
    :cond_1a
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_address_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    .line 521
    :cond_1b
    if-ne v3, v8, :cond_1c

    .line 522
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_home_address_found_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 525
    :cond_1c
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_address_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4
.end method

.method private getTypeData(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 580
    const-string/jumbo v0, "hpn"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "homeemail"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "homeaddress"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 581
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 587
    :goto_0
    return-object v0

    .line 582
    :cond_1
    const-string/jumbo v0, "wpn"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "workemail"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "workaddress"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 583
    :cond_2
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_work:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 584
    :cond_3
    const-string/jumbo v0, "mpn"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 585
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_mobile:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 587
    :cond_4
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_other:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private hasContactsData(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v5, 0x1

    .line 275
    const/4 v3, 0x0

    .line 276
    .local v3, "tr":Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 277
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    const/4 v1, 0x0

    .line 278
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v6, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v4, v6, :cond_0

    .line 292
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v1    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :goto_1
    return v5

    .line 280
    .restart local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .restart local v1    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_0
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v6, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v4, v6, :cond_2

    .line 281
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v1

    .line 290
    :cond_1
    :goto_2
    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    move v4, v5

    :goto_3
    or-int/2addr v3, v4

    .line 291
    goto :goto_0

    .line 282
    :cond_2
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v6, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v4, v6, :cond_3

    .line 283
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAddressData()Ljava/util/List;

    move-result-object v1

    goto :goto_2

    .line 284
    :cond_3
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v6, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v4, v6, :cond_4

    .line 285
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getEmailData()Ljava/util/List;

    move-result-object v1

    goto :goto_2

    .line 286
    :cond_4
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v6, Lcom/vlingo/core/internal/contacts/ContactType;->BIRTHDAY:Lcom/vlingo/core/internal/contacts/ContactType;

    if-ne v4, v6, :cond_1

    .line 287
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBirthdayData()Ljava/util/List;

    move-result-object v1

    goto :goto_2

    .line 290
    :cond_5
    const/4 v4, 0x0

    goto :goto_3

    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v1    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_6
    move v5, v3

    .line 292
    goto :goto_1
.end method

.method private queryShowType(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)V
    .locals 11
    .param p2, "newDecor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "contactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 321
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "pn"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "email"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    const-string/jumbo v5, "address"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 322
    const/4 v2, 0x0

    .line 323
    .local v2, "isType":Z
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-direct {p0, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getSearchType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)I

    move-result v3

    .line 324
    .local v3, "searhcType":I
    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 325
    const/4 v2, 0x1

    .line 334
    :cond_0
    if-nez v2, :cond_4

    .line 335
    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 336
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getTurnDoNoTypeInformation()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    .line 386
    .end local v2    # "isType":Z
    .end local v3    # "searhcType":I
    :cond_1
    :goto_0
    return-void

    .line 327
    .restart local v2    # "isType":Z
    .restart local v3    # "searhcType":I
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 328
    .local v0, "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    iget v4, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-ne v4, v3, :cond_3

    .line 329
    const/4 v2, 0x1

    goto :goto_1

    .line 338
    .end local v0    # "cd":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_4
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getTurnFoundTypeData(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    goto :goto_0

    .line 341
    .end local v2    # "isType":Z
    .end local v3    # "searhcType":I
    :cond_5
    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .line 342
    const-string/jumbo v4, "pn"

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 343
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    iget v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v4, v5, :cond_6

    .line 344
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_phone_number_large_list:I

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    goto :goto_0

    .line 348
    :cond_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v8, :cond_7

    .line 349
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_phone_number_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    goto/16 :goto_0

    .line 352
    :cond_7
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_phone_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    goto/16 :goto_0

    .line 356
    :cond_8
    const-string/jumbo v4, "email"

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 357
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    iget v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v4, v5, :cond_9

    .line 358
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_email_large_list:I

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    goto/16 :goto_0

    .line 362
    :cond_9
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v8, :cond_a

    .line 363
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_email_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    goto/16 :goto_0

    .line 366
    :cond_a
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_email:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    goto/16 :goto_0

    .line 370
    :cond_b
    const-string/jumbo v4, "address"

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 371
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    iget v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v4, v5, :cond_c

    .line 372
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_address_large_list:I

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    goto/16 :goto_0

    .line 376
    :cond_c
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v8, :cond_d

    .line 377
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->contact_address_one:I

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    goto/16 :goto_0

    .line 380
    :cond_d
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_address:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    goto/16 :goto_0
.end method


# virtual methods
.method protected addContactMatchDetailData(Ljava/util/List;II)V
    .locals 2
    .param p2, "size"    # I
    .param p3, "interval"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p1, "contactDetail":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :goto_0
    if-ge p2, p3, :cond_6

    .line 175
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->CALL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-eq v0, v1, :cond_0

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactData;->isPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetail:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addPhone(Lcom/vlingo/core/internal/contacts/ContactData;)V

    .line 182
    :cond_1
    :goto_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 177
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    if-eq v0, v1, :cond_3

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactData;->isEmail()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 178
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetail:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addEmail(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto :goto_1

    .line 179
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v1, Lcom/vlingo/core/internal/contacts/ContactType;->ADDRESS:Lcom/vlingo/core/internal/contacts/ContactType;

    if-eq v0, v1, :cond_5

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactData;->isAddress()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetail:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->addAddress(Lcom/vlingo/core/internal/contacts/ContactData;)V

    goto :goto_1

    .line 184
    :cond_6
    return-void
.end method

.method protected contactMatchDetailCheck(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contactDetail":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v2, 0x0

    .line 187
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetail:Lcom/vlingo/core/internal/contacts/ContactMatch;

    if-eqz v0, :cond_0

    .line 188
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v0, v1, :cond_1

    .line 189
    iget v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-virtual {p0, p1, v2, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->addContactMatchDetailData(Ljava/util/List;II)V

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-gt v0, v1, :cond_0

    .line 191
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, p1, v2, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->addContactMatchDetailData(Ljava/util/List;II)V

    goto :goto_0
.end method

.method protected createContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 7
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 164
    invoke-virtual {p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->clone()Lcom/vlingo/core/internal/contacts/ContactMatch;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->staticContactMatchClone:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 165
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->staticContactMatchClone:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v1, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    .line 166
    .local v1, "primaryDisplayName":Ljava/lang/String;
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->staticContactMatchClone:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-object v4, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->lookupKey:Ljava/lang/String;

    .line 167
    .local v4, "lookupKey":Ljava/lang/String;
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->staticContactMatchClone:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v2, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 168
    .local v6, "primaryContactID":Ljava/lang/Long;
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->staticContactMatchClone:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-boolean v5, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    .line 170
    .local v5, "starred":Z
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/core/internal/contacts/ContactMatch;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetail:Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 171
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 13
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 56
    const-string/jumbo v9, "ListPositionChange"

    const/4 v10, 0x0

    invoke-static {p1, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 57
    .local v6, "listPositionChange":Ljava/lang/String;
    invoke-static {p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v5

    .line 58
    .local v5, "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    if-eqz v6, :cond_3

    if-eqz v5, :cond_3

    .line 59
    const-string/jumbo v9, "next"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 61
    :try_start_0
    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ListControlData;->incCurrentPage()V

    .line 62
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->which_one_for_disambiguation:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :cond_0
    :goto_0
    const-string/jumbo v9, "prev"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 69
    :try_start_1
    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ListControlData;->decCurrentPage()V

    .line 70
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->which_one_for_disambiguation:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_1 .. :try_end_1} :catch_1

    .line 75
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 76
    .local v0, "actionName":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string/jumbo v9, "multipleContactDetailControl"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 77
    invoke-static {p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v3

    .line 78
    .local v3, "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-static {p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 79
    .local v1, "displayedContactDetail":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    sget-object v9, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->staticContactMatchClone:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->createContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 80
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetailCheck(Ljava/util/List;)V

    .line 81
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ContactDetail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v11, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    iget-object v12, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetail:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-interface {v9, v10, v11, v12, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 88
    .end local v1    # "displayedContactDetail":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v3    # "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :goto_2
    const/4 v9, 0x0

    .line 109
    :goto_3
    return v9

    .line 63
    .end local v0    # "actionName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 64
    .local v2, "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->that_is_all_for_disambiguation:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    .end local v2    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    :catch_1
    move-exception v2

    .line 72
    .restart local v2    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->that_is_all_for_disambiguation:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto :goto_1

    .line 83
    .end local v2    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    .restart local v0    # "actionName":Ljava/lang/String;
    :cond_2
    invoke-static {p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v4

    .line 84
    .local v4, "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-static {p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->displayedContacts:Ljava/util/List;

    .line 85
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddressBook:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeReplaceable()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v11

    iget-object v12, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->displayedContacts:Ljava/util/List;

    invoke-interface {v9, v10, v11, v12, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_2

    .line 90
    .end local v0    # "actionName":Ljava/lang/String;
    .end local v4    # "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_3
    const-string/jumbo v9, "Which"

    const/4 v10, 0x0

    invoke-static {p1, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 91
    .local v7, "ordinal":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 92
    .restart local v0    # "actionName":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    const-string/jumbo v9, "multipleContactDetailControl"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 93
    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-static {v9, v7}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElement(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 94
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v3

    .line 95
    .restart local v3    # "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-static {p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 96
    .restart local v1    # "displayedContactDetail":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-static {v1, v7}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 97
    .local v8, "whichInfo":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 98
    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v9, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->staticContactMatchClone:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->createContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 100
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetailCheck(Ljava/util/List;)V

    .line 101
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ContactDetail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v11, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    iget-object v12, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetail:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-interface {v9, v10, v11, v12, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 102
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v10}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 103
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 104
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 105
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 109
    .end local v1    # "displayedContactDetail":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v3    # "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v8    # "whichInfo":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_4
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ContactLookupHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v9

    goto/16 :goto_3
.end method

.method protected getMultipleContactsString(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 644
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    if-le v0, v1, :cond_0

    .line 645
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->multiple_contacts_large_list:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->maxWidget:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 647
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_contacts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v1, v5, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 259
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "com.vlingo.core.internal.dialogmanager.ContactChoice"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 260
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 261
    .local v1, "extras":Landroid/os/Bundle;
    const-string/jumbo v5, "choice"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 262
    .local v2, "index":I
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 263
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v3

    .line 264
    .local v3, "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 265
    .local v4, "pageContactInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 266
    .local v0, "contactSelected":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->openContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 272
    .end local v0    # "contactSelected":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v1    # "extras":Landroid/os/Bundle;
    .end local v2    # "index":I
    .end local v3    # "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local v4    # "pageContactInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .end local p2    # "object":Ljava/lang/Object;
    :goto_0
    return-void

    .line 267
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    if-eqz v5, :cond_2

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.DataTransfered"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 268
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    instance-of v5, p2, Ljava/lang/Integer;

    if-eqz v5, :cond_1

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :goto_1
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v6, v5}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;I)V

    goto :goto_0

    .restart local p2    # "object":Ljava/lang/Object;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 270
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected matchContacts(Lcom/vlingo/core/internal/contacts/ContactType;)V
    .locals 12
    .param p1, "contactType"    # Lcom/vlingo/core/internal/contacts/ContactType;

    .prologue
    const/4 v6, 0x0

    .line 114
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    .line 116
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v11, v0

    check-cast v11, Ljava/util/List;

    .line 117
    .local v11, "disambiguationList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v0, Lcom/vlingo/core/internal/contacts/ContactMatcher;

    new-instance v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;

    invoke-direct {v2, p0, v6}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$1;)V

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v4, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->ALL_PHONE_TYPES:[I

    sget-object v5, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->ALL_EMAIL_TYPES:[I

    sget-object v7, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->ALL_ADDRESS_TYPES:[I

    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;

    const/high16 v9, 0x42c80000    # 100.0f

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/vlingo/core/internal/contacts/ContactMatcher;-><init>(Landroid/content/Context;Lcom/vlingo/core/internal/contacts/ContactMatchListener;Lcom/vlingo/core/internal/contacts/ContactType;[I[I[I[ILjava/lang/String;FILjava/util/List;)V

    .line 119
    return-void
.end method

.method protected multipleContacts(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 247
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v2, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 248
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getMultipleContactsString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 249
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP_CHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 250
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->startReco()Z

    .line 251
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->displayedContacts:Ljava/util/List;

    .line 252
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 253
    .local v0, "pageContactInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddressBook:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 254
    return-void
.end method

.method protected openContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 9
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 198
    const/4 v2, 0x1

    .line 199
    .local v2, "showWidget":Z
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {p1, v3}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getData(Lcom/vlingo/core/internal/contacts/ContactType;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactDetail:Ljava/util/List;

    .line 200
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 201
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->createSystemTurn(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 202
    const-string/jumbo v1, ""

    .line 203
    .local v1, "data":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->query:Ljava/lang/String;

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->birthdayType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 204
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getBirthday(Lcom/vlingo/core/internal/contacts/ContactMatch;)Ljava/lang/String;

    move-result-object v1

    .line 205
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 206
    const/4 v2, 0x0

    .line 207
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->checkWrongDateOfBirthday(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 208
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getContactData(Lcom/vlingo/core/internal/contacts/ContactMatch;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    .line 209
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getISOLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/schedule/DateUtil;->getTTSPlayingFormat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 212
    :cond_0
    if-nez v2, :cond_1

    .line 213
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    invoke-virtual {p0, v3, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .end local v1    # "data":Ljava/lang/String;
    :goto_0
    if-eqz v2, :cond_4

    .line 225
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->type:Lcom/vlingo/core/internal/contacts/ContactType;

    sget-object v4, Lcom/vlingo/core/internal/contacts/ContactType;->UNDEFINED:Lcom/vlingo/core/internal/contacts/ContactType;

    if-eq v3, v4, :cond_3

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactDetail:Ljava/util/List;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactDetail:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v7, :cond_3

    .line 226
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->createContactMatch(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 227
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactDetail:Ljava/util/List;

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    .line 228
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactDetail:Ljava/util/List;

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetailCheck(Ljava/util/List;)V

    .line 229
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ContactDetail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->contactMatchDetail:Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-interface {v3, v4, v5, v6, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 230
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP_CHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v4}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 231
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->startReco()Z

    .line 232
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const-string/jumbo v5, "multipleContactDetailControl"

    invoke-interface {v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 240
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SELECTED_CONTACT:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 241
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_QUERY:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 242
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v3, v4, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 243
    return-void

    .line 215
    .restart local v1    # "data":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->systemTurn:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto :goto_0

    .line 218
    .end local v1    # "data":Ljava/lang/String;
    :cond_2
    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_single_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->formatKoreanNameForTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 221
    .local v0, "contactFound":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    .end local v0    # "contactFound":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ContactDetail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    invoke-interface {v3, v4, v5, p1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 235
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v4}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto :goto_1

    .line 238
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_CONTACTLOOKUP:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v4}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto :goto_1
.end method
