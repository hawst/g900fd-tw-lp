.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "SamsungNavHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

.field final synthetic val$action:Lcom/vlingo/sdk/recognition/VLAction;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field final synthetic val$promptForCover:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Landroid/content/Context;Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$promptForCover:Ljava/lang/String;

    iput-object p3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    iput-object p4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$context:Landroid/content/Context;

    iput-object p5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$action:Lcom/vlingo/sdk/recognition/VLAction;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 4
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    const/4 v3, 0x0

    .line 79
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 80
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->waitingForCoverOpened:Z
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81
    const-string/jumbo v0, "always"

    const-string/jumbo v1, "mCoverStateListener cover Opened"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 83
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    iget-object v1, v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 84
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$promptForCover:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$promptForCover:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$promptForCover:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$context:Landroid/content/Context;

    instance-of v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$action:Lcom/vlingo/sdk/recognition/VLAction;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->doExecuteActionSamsung(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 92
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->waitingForCoverOpened:Z
    invoke-static {v0, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;Z)Z

    .line 99
    :goto_1
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$action:Lcom/vlingo/sdk/recognition/VLAction;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->doExecuteActionOriginal(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    goto :goto_0

    .line 94
    :cond_2
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "Cover opened, waitingForCoverOpened is false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 97
    :cond_3
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "mCoverStateListener cover Closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
