.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$1;
.super Ljava/lang/Object;
.source "SamsungContactLookupHandler.java"

# interfaces
.implements Lcom/vlingo/sdk/util/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->filterPhonesByType(ILjava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/vlingo/sdk/util/Predicate",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)V
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/vlingo/core/internal/contacts/ContactData;)Z
    .locals 3
    .param p1, "object"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 595
    iget v0, p1, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->decor:Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$1200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v2

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getSearchType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)I
    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$1300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 596
    const/4 v0, 0x1

    .line 598
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 592
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$1;->apply(Lcom/vlingo/core/internal/contacts/ContactData;)Z

    move-result v0

    return v0
.end method
