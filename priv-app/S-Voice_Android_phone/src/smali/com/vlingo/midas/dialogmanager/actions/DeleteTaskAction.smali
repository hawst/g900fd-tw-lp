.class public Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "DeleteTaskAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteTaskInterface;


# instance fields
.field private task:Lcom/vlingo/core/internal/schedule/ScheduleTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 4

    .prologue
    .line 30
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;->task:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    if-eqz v1, :cond_0

    .line 32
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;->task:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getID()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->deleteTask(Landroid/content/Context;J)V

    .line 33
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to delete task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 41
    .end local v0    # "e":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "No task to delete"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic task(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteTaskInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;->task(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;

    move-result-object v0

    return-object v0
.end method

.method public task(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;
    .locals 0
    .param p1, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;->task:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 25
    return-object p0
.end method
