.class public Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "RecordVoiceAction.java"


# instance fields
.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 4

    .prologue
    .line 31
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;->title:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 32
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "Activity could not be found."

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 35
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 36
    .local v1, "noMatchMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 42
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 43
    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    :cond_0
    const-string/jumbo v1, "android.intent.action.RUN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 46
    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 48
    const-string/jumbo v1, "com.sec.android.app.voicerecorder"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.sec.android.app.voicerecorder"

    const-string/jumbo v3, "com.sec.android.app.voicerecorder.VoiceRecorderMainActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 58
    :goto_0
    return-object v0

    .line 53
    :cond_1
    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.sec.android.app.voicenote"

    const-string/jumbo v3, "com.sec.android.app.voicenote.main.VNMainActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public title(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;->title:Ljava/lang/String;

    .line 25
    return-object p0
.end method
