.class Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SamsungSettingChangeAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkChangeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .param p2, "x1"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;

    .prologue
    .line 457
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 462
    const-string/jumbo v0, "android.net.wifi.WIFI_DIALOG_CANCEL_ACTION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 464
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver$1;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 477
    :cond_0
    :goto_0
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 478
    # operator-- for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiReceiverCount:I
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$710()I

    .line 479
    return-void

    .line 474
    :cond_1
    const-string/jumbo v0, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn:Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$400(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOnTTS:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$500(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$300(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
