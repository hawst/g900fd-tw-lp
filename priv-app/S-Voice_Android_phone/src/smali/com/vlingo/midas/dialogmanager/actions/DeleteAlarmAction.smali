.class public Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "DeleteAlarmAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteAlarmInterface;


# static fields
.field private static final INTENT_DELETE_ALARM_ACTION:Ljava/lang/String; = "com.sec.android.clockpackage.DELETE_ALARM"

.field private static final INTENT_DELETE_ALARM_EXTRA:Ljava/lang/String; = "listitemId"


# instance fields
.field private alarm:Lcom/vlingo/core/internal/util/Alarm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteAlarmInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;->alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;

    move-result-object v0

    return-object v0
.end method

.method public alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;
    .locals 0
    .param p1, "alarm"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    .line 28
    return-object p0
.end method

.method public deleteAlarm()V
    .locals 3

    .prologue
    .line 44
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.clockpackage.DELETE_ALARM"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 45
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "listitemId"

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 46
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 47
    return-void
.end method

.method protected execute()V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;->deleteAlarm()V

    .line 35
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->enableNextAlert(Landroid/content/Context;)V

    .line 37
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.samsung.sec.android.clockpackage.alarm.NOTIFY_ALARM_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 41
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    const-string/jumbo v1, "No alarm to delete"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method
