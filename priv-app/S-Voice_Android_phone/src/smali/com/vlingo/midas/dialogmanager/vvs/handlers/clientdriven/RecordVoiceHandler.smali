.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "RecordVoiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler$RunnableRecordVoice;
    }
.end annotation


# static fields
.field private static final PARAM_TITLE:Ljava/lang/String; = "Title"


# instance fields
.field private mAction:Lcom/vlingo/sdk/recognition/VLAction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;)Lcom/vlingo/sdk/recognition/VLAction;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;->mAction:Lcom/vlingo/sdk/recognition/VLAction;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .param p2, "x2"    # Ljava/lang/Class;

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const-wide/16 v6, 0x3e8

    .line 37
    const-string/jumbo v2, "com.sec.android.app.voicerecorder"

    .line 38
    .local v2, "packageName":Ljava/lang/String;
    const-string/jumbo v3, "com.sec.android.app.voicenote"

    .line 39
    .local v3, "packageNameVoiceNote":Ljava/lang/String;
    const/4 v1, 0x0

    .line 41
    .local v1, "minVersionCode":I
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;->mAction:Lcom/vlingo/sdk/recognition/VLAction;

    .line 43
    invoke-static {v2, v1}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 47
    .local v0, "handler":Landroid/os/Handler;
    new-instance v5, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler$RunnableRecordVoice;

    invoke-direct {v5, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler$RunnableRecordVoice;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;)V

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 59
    .end local v0    # "handler":Landroid/os/Handler;
    :goto_0
    const/4 v5, 0x0

    return v5

    .line 48
    :cond_0
    invoke-static {v3, v1}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 52
    .restart local v0    # "handler":Landroid/os/Handler;
    new-instance v5, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler$RunnableRecordVoice;

    invoke-direct {v5, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler$RunnableRecordVoice;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;)V

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 56
    .end local v0    # "handler":Landroid/os/Handler;
    :cond_1
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$string;->core_car_tts_NO_APPMATCH_DEMAND:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 57
    .local v4, "tts":Ljava/lang/String;
    invoke-interface {p2, v4, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
