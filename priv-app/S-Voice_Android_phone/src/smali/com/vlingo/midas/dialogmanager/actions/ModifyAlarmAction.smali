.class public Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "ModifyAlarmAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyAlarmInterface;


# static fields
.field private static final INTENT_ALARM_ACTIVE:Ljava/lang/String; = "alarm_activate"

.field private static final INTENT_EDIT_ALARM_ACTION:Ljava/lang/String; = "com.sec.android.clockpackage.DIRECT_EDIT_ALARM"

.field private static final INTENT_EDIT_ALARM_EXTRA:Ljava/lang/String; = "listitemId"


# instance fields
.field private modifiedAlarm:Lcom/vlingo/core/internal/util/Alarm;

.field private origAlarm:Lcom/vlingo/core/internal/util/Alarm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->modifiedAlarm:Lcom/vlingo/core/internal/util/Alarm;

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->storeModifiedAlarm()V

    .line 51
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 52
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.samsung.sec.android.clockpackage.alarm.NOTIFY_ALARM_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    const-string/jumbo v1, "No alarm to update"

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic modified(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyAlarmInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->modified(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;

    move-result-object v0

    return-object v0
.end method

.method public modified(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;
    .locals 0
    .param p1, "alarm"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->modifiedAlarm:Lcom/vlingo/core/internal/util/Alarm;

    .line 44
    return-object p0
.end method

.method public bridge synthetic original(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyAlarmInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->original(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;

    move-result-object v0

    return-object v0
.end method

.method public original(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;
    .locals 0
    .param p1, "alarm"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->origAlarm:Lcom/vlingo/core/internal/util/Alarm;

    .line 38
    return-object p0
.end method

.method public storeModifiedAlarm()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 60
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.sec.android.clockpackage.DIRECT_EDIT_ALARM"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "editIntent":Landroid/content/Intent;
    const-string/jumbo v2, "listitemId"

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->origAlarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/Alarm;->getId()I

    move-result v5

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 62
    const-string/jumbo v2, "android.intent.extra.alarm.HOUR"

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->modifiedAlarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/Alarm;->getHour()I

    move-result v5

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 63
    const-string/jumbo v2, "android.intent.extra.alarm.MINUTES"

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->modifiedAlarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/Alarm;->getMinute()I

    move-result v5

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 64
    const-string/jumbo v5, "alarm_activate"

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->modifiedAlarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->getAlarmStatus()Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    move-result-object v2

    sget-object v6, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v2, v6}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v0, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 65
    const-string/jumbo v2, "alarm_repeat"

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->modifiedAlarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/Alarm;->getDayMask()I

    move-result v5

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 66
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->modifiedAlarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->isWeeklyRepeating()Z

    move-result v2

    if-eqz v2, :cond_1

    move v1, v3

    .line 67
    .local v1, "everyWeekRepeat":I
    :goto_1
    const-string/jumbo v2, "alarm_everyweekrepeat"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 68
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 69
    return-void

    .end local v1    # "everyWeekRepeat":I
    :cond_0
    move v2, v4

    .line 64
    goto :goto_0

    :cond_1
    move v1, v4

    .line 66
    goto :goto_1
.end method
