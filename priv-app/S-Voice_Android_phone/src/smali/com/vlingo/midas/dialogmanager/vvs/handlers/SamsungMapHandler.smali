.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "SamsungMapHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# static fields
.field private static final MAX_ADDRESSES:I = 0x1

.field private static final MSG_WAITING_FOR_COVER_OPENED:I

.field private static final NO_LAT:D

.field private static final NO_LONG:D

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field final mHandler:Landroid/os/Handler;

.field private waitingForCoverOpened:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->waitingForCoverOpened:Z

    .line 174
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$2;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->waitingForCoverOpened:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->waitingForCoverOpened:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "x2"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->doExecuteNavigation(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    return-void
.end method

.method private doExecuteActionOriginal(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v7, 0x0

    .line 306
    const/4 v1, 0x0

    .line 307
    .local v1, "className":Ljava/lang/String;
    const-string/jumbo v5, "IntentName"

    const/4 v6, 0x1

    invoke-static {p1, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 308
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->getArgFromAction(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/lang/String;

    move-result-object v0

    .line 309
    .local v0, "arg":Ljava/lang/String;
    const-string/jumbo v5, "Extras"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 310
    .local v2, "extras":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->isGoogleNavAvailable()Z

    move-result v5

    if-nez v5, :cond_0

    .line 311
    const-string/jumbo v5, "ClassName"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 313
    :cond_0
    const-string/jumbo v5, "broadcast"

    invoke-static {p1, v5, v7, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v3

    .line 314
    .local v3, "isBroadcast":Z
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v6, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v5, v6}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v5, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 322
    return v7
.end method

.method private doExecuteActionSamsung(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 12
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 227
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v1

    .line 228
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->getArgFromAction(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "arg":Ljava/lang/String;
    const-string/jumbo v9, "Query"

    const/4 v10, 0x0

    invoke-static {p1, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 230
    .local v6, "query":Ljava/lang/String;
    const-string/jumbo v9, "action.prompt"

    const/4 v10, 0x0

    invoke-static {p1, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 231
    .local v5, "prompt":Ljava/lang/String;
    const/4 v8, 0x0

    .line 232
    .local v8, "versionName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 234
    .local v4, "parts":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 236
    .local v7, "version":I
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 238
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string/jumbo v10, "com.skt.skaf.l001mtm091"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    iget-object v8, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 239
    const-string/jumbo v9, "\\."

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 241
    :goto_0
    const-string/jumbo v9, "com.skt.skaf.l001mtm091"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v9, 0x0

    aget-object v9, v4, v9

    const-string/jumbo v10, "3"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 242
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 243
    .local v3, "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.skt.skaf.l001mtm091"

    const-string/jumbo v10, "com.skt.tmap.activity.TmapIntroActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "tmap://search?name="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 279
    :goto_1
    const/high16 v9, 0x10000000

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 280
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 281
    const-string/jumbo v9, "kt.navi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 282
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 302
    :goto_2
    const/4 v9, 0x0

    return v9

    .line 245
    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    :cond_0
    const-string/jumbo v9, "kt.navi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 246
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v9, "kt.navi.OLLEH_NAVIGATION"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 247
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const/16 v9, 0x20

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 248
    const-string/jumbo v9, "CALLER_PACKAGE_NAME"

    const-string/jumbo v10, "com.vlingo.midas"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 249
    const-string/jumbo v9, "EXTERN_LINK_TYPE"

    const/4 v10, 0x1

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 250
    const-string/jumbo v9, "LINK_SEARCH_WORD"

    invoke-virtual {v3, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 251
    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    :cond_1
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 253
    :try_start_1
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string/jumbo v10, "com.mnsoft.lgunavi"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    iget v7, v9, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 255
    :goto_3
    const/4 v9, 0x1

    if-le v7, v9, :cond_2

    .line 256
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v9, "com.mnsoft.mappy.MAPPYSMART_EXTERNAL_SERVICE"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 257
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.mappy.action.REQUEST_TO_MAPPYSMART"

    const/high16 v10, 0x10000

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 258
    const-string/jumbo v9, "com.mnsoft.mappy.extra.EXTRA_KIND"

    const-string/jumbo v10, "poi"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    const-string/jumbo v9, "com.mnsoft.mappy.extra.EXTRA_VALUE"

    invoke-virtual {v3, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 261
    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    :cond_2
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 262
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const-string/jumbo v10, "com.mnsoft.offboardnavi.DispatcherActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 265
    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    :cond_3
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 266
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "android.intent.action.VIEW"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    const-string/jumbo v9, "com.google.android.apps.maps"

    const-string/jumbo v10, "com.google.android.maps.MapsActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 269
    .local v2, "data":Landroid/net/Uri;
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 272
    .end local v2    # "data":Landroid/net/Uri;
    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    :cond_4
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 273
    .restart local v3    # "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v9, "android.intent.action.VIEW"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 274
    const-string/jumbo v9, "com.google.android.apps.maps"

    const-string/jumbo v10, "com.google.android.maps.MapsActivity"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 276
    .restart local v2    # "data":Landroid/net/Uri;
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 286
    .end local v2    # "data":Landroid/net/Uri;
    :cond_5
    const-string/jumbo v9, "com.mnsoft.lgunavi"

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_6

    const/4 v9, 0x1

    if-le v7, v9, :cond_6

    .line 287
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->service(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto/16 :goto_2

    .line 292
    :cond_6
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto/16 :goto_2

    .line 297
    :cond_7
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v9, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto/16 :goto_2

    .line 254
    .end local v3    # "intentToLaunch":Landroid/content/Intent;
    :catch_0
    move-exception v9

    goto/16 :goto_3

    .line 240
    :catch_1
    move-exception v9

    goto/16 :goto_0
.end method

.method private doExecuteNavigation(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 162
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v2

    .line 163
    .local v2, "mLocale":Ljava/util/Locale;
    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "mCurrentLanguage":Ljava/lang/String;
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    .line 166
    .local v0, "context":Landroid/content/Context;
    instance-of v3, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v3, :cond_0

    const-string/jumbo v3, "ko_KR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 168
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->doExecuteActionSamsung(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->doExecuteActionOriginal(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    goto :goto_0
.end method

.method private static isLocationAvailableForMap()Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    const-wide/16 v10, 0x0

    const/4 v9, 0x0

    .line 328
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLat()D

    move-result-wide v1

    .line 329
    .local v1, "latitude":D
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLong()D

    move-result-wide v3

    .line 332
    .local v3, "longitude":D
    cmpl-double v5, v3, v10

    if-eqz v5, :cond_0

    cmpl-double v5, v1, v10

    if-nez v5, :cond_1

    :cond_0
    move v5, v9

    .line 346
    :goto_0
    return v5

    .line 334
    :cond_1
    new-instance v0, Landroid/location/Geocoder;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 336
    .local v0, "geocoder":Landroid/location/Geocoder;
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v6

    .line 339
    .local v6, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    if-eqz v6, :cond_2

    invoke-interface {v6}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-lez v5, :cond_2

    move v5, v8

    .line 340
    goto :goto_0

    :cond_2
    move v5, v9

    .line 342
    goto :goto_0

    .line 344
    .end local v6    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :catch_0
    move-exception v7

    .line 345
    .local v7, "e":Ljava/io/IOException;
    sget-object v5, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "isMapAvailable() catch exception: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v9

    .line 346
    goto :goto_0
.end method


# virtual methods
.method public actionSuccess()V
    .locals 0

    .prologue
    .line 214
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionSuccess()V

    .line 218
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 17
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 64
    invoke-super/range {p0 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 65
    const-string/jumbo v13, "Query"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    .line 66
    .local v11, "query":Ljava/lang/String;
    const-string/jumbo v13, "action.prompt"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    .line 68
    .local v9, "prompt":Ljava/lang/String;
    const-string/jumbo v13, "CurrentLocation"

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v12

    .line 69
    .local v12, "useCurrentLocation":Z
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->isLocationAvailableForMap()Z

    move-result v6

    .line 71
    .local v6, "isLocationAvaiable":Z
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    .line 72
    .local v4, "context":Landroid/content/Context;
    const-string/jumbo v13, "location"

    invoke-virtual {v4, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/LocationManager;

    .line 73
    .local v7, "lm":Landroid/location/LocationManager;
    const-string/jumbo v13, "network"

    invoke-virtual {v7, v13}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v5

    .line 75
    .local v5, "isGpsEnable":Z
    const-string/jumbo v13, "IntentArgument"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 76
    .local v3, "arg":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string/jumbo v13, "current location"

    invoke-virtual {v3, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 77
    if-nez v5, :cond_1

    .line 78
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    sget v14, Lcom/vlingo/midas/R$string;->core_navigation_default_location:I

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 80
    move-object/from16 v0, p2

    invoke-interface {v0, v9, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 82
    const/4 v13, 0x0

    .line 158
    :goto_0
    return v13

    .line 85
    :cond_0
    if-nez v5, :cond_1

    if-nez v11, :cond_1

    .line 86
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    sget v14, Lcom/vlingo/midas/R$string;->core_map_default_location:I

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 88
    move-object/from16 v0, p2

    invoke-interface {v0, v9, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const/4 v13, 0x0

    goto :goto_0

    .line 93
    :cond_1
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-static {v11}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_2

    .line 94
    sget-object v13, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MAP_OF:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v11, v14, v15

    invoke-static {v13, v14}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 97
    :cond_2
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    if-eqz v12, :cond_3

    .line 98
    if-eqz v6, :cond_4

    .line 99
    sget-object v13, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v11, v14, v15

    invoke-static {v13, v14}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 109
    :cond_3
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v13

    const-string/jumbo v14, "maps"

    invoke-virtual {v13, v14}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 112
    move-object v10, v9

    .line 114
    .local v10, "promptForCover":Ljava/lang/String;
    new-instance v13, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v13, v4}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 115
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v13}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v8

    .line 117
    .local v8, "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    new-instance v13, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v13, v0, v10, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/sdk/recognition/VLAction;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 138
    if-eqz v8, :cond_5

    invoke-virtual {v8}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v13

    if-nez v13, :cond_5

    .line 140
    const-string/jumbo v13, "Always"

    const-string/jumbo v14, "maphandler ExcuteAction. CoverClosed"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v13

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$string;->sview_cover_alwaysmicon:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v13, v14}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 147
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mHandler:Landroid/os/Handler;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/os/Handler;->removeMessages(I)V

    .line 148
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mHandler:Landroid/os/Handler;

    const/4 v14, 0x0

    const-wide/16 v15, 0x2710

    invoke-virtual/range {v13 .. v16}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 150
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->waitingForCoverOpened:Z

    .line 158
    :goto_2
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 101
    .end local v8    # "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    .end local v10    # "promptForCover":Ljava/lang/String;
    :cond_4
    sget-object v13, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_not_detected_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v13, v14}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_1

    .line 152
    .restart local v8    # "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    .restart local v10    # "promptForCover":Ljava/lang/String;
    :cond_5
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_6

    .line 153
    move-object/from16 v0, p2

    invoke-interface {v0, v9, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_6
    invoke-direct/range {p0 .. p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->doExecuteNavigation(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    goto :goto_2
.end method

.method protected getArgFromAction(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/lang/String;
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v7, 0x0

    .line 187
    const-string/jumbo v6, "IntentArgument"

    invoke-static {p1, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "arg":Ljava/lang/String;
    const-string/jumbo v6, "ZoomLevel"

    invoke-static {p1, v6, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 190
    .local v5, "zoom":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 191
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLat()D

    move-result-wide v1

    .line 192
    .local v1, "lat":D
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLong()D

    move-result-wide v3

    .line 193
    .local v3, "lng":D
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "geo:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1, v2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 201
    .end local v1    # "lat":D
    .end local v3    # "lng":D
    :cond_0
    :goto_0
    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 202
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "?z="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 205
    :cond_1
    return-object v0

    .line 194
    :cond_2
    const-string/jumbo v6, "current location"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 196
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLat()D

    move-result-wide v1

    .line 197
    .restart local v1    # "lat":D
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLong()D

    move-result-wide v3

    .line 198
    .restart local v3    # "lng":D
    const-string/jumbo v6, "current location"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1, v2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 210
    return-void
.end method
