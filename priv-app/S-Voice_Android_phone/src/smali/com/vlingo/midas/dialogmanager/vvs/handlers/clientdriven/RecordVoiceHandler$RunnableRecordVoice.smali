.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler$RunnableRecordVoice;
.super Ljava/lang/Object;
.source "RecordVoiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RunnableRecordVoice"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler$RunnableRecordVoice;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler$RunnableRecordVoice;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->RECORD_VOICE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v2, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;
    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;->access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler$RunnableRecordVoice;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;->mAction:Lcom/vlingo/sdk/recognition/VLAction;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;->access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;)Lcom/vlingo/sdk/recognition/VLAction;

    move-result-object v1

    const-string/jumbo v2, "Title"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;->title(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;->queue()V

    .line 69
    return-void
.end method
