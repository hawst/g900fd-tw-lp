.class public Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "PlayMusicViaIntentAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction$1;
    }
.end annotation


# static fields
.field private static final MUSIC_SQUARE_CONTENT_URI:Ljava/lang/String; = "content://com.sec.android.music/music_square/"


# instance fields
.field private moodType:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

.field private quickList:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->quickList:Z

    return-void
.end method

.method private getPlayMusicTypeStr()Ljava/lang/String;
    .locals 2

    .prologue
    .line 260
    sget-object v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction$1;->$SwitchMap$com$vlingo$midas$samsungutils$utils$music$PlayMusicType:[I

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 269
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 261
    :pswitch_0
    const-string/jumbo v0, "album"

    goto :goto_0

    .line 262
    :pswitch_1
    const-string/jumbo v0, "artist"

    goto :goto_0

    .line 263
    :pswitch_2
    const-string/jumbo v0, "playlist"

    goto :goto_0

    .line 264
    :pswitch_3
    const-string/jumbo v0, "title"

    goto :goto_0

    .line 265
    :pswitch_4
    const-string/jumbo v0, "playlist"

    goto :goto_0

    .line 260
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method protected execute()V
    .locals 22

    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getPlayMusicTypeStr()Ljava/lang/String;

    move-result-object v21

    .line 62
    .local v21, "typeStr":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    .line 63
    .local v20, "tooOld":Ljava/lang/Boolean;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->moodType:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 64
    const/16 v19, 0x0

    .line 65
    .local v19, "squareDB":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v8

    check-cast v8, Lcom/vlingo/midas/gui/ConversationActivity;

    .line 67
    .local v8, "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 68
    new-instance v12, Landroid/content/Intent;

    const-string/jumbo v2, "com.sec.android.app.music.intent.action.PLAY_BY_MOOD"

    invoke-direct {v12, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 69
    .local v12, "moodIntent":Landroid/content/Intent;
    const-string/jumbo v2, "extra_type"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->moodType:Ljava/lang/String;

    invoke-virtual {v12, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-nez v2, :cond_0

    .line 75
    const-string/jumbo v2, "launchMusicSquare"

    const/4 v4, 0x1

    invoke-virtual {v12, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 78
    :cond_0
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 79
    .local v10, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v13, 0x0

    .line 80
    .local v13, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v10, :cond_1

    .line 82
    :try_start_1
    invoke-virtual {v10}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v13, v0
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :cond_1
    :goto_0
    const/4 v3, 0x0

    .line 91
    .local v3, "musicSquareUri":Landroid/net/Uri;
    if-eqz v13, :cond_5

    .line 92
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->moodType:Ljava/lang/String;

    invoke-interface {v13, v2}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicMoodUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 95
    :goto_1
    const-string/jumbo v2, "test"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "musicSquareURL = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v19

    .line 107
    :goto_2
    :try_start_3
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v19, :cond_6

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_6

    .line 110
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 123
    :cond_3
    :goto_3
    if-eqz v19, :cond_4

    .line 125
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 126
    const/16 v19, 0x0

    .line 210
    .end local v3    # "musicSquareUri":Landroid/net/Uri;
    .end local v8    # "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    .end local v10    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v12    # "moodIntent":Landroid/content/Intent;
    .end local v13    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    .end local v19    # "squareDB":Landroid/database/Cursor;
    :cond_4
    :goto_4
    return-void

    .line 83
    .restart local v8    # "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    .restart local v10    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v12    # "moodIntent":Landroid/content/Intent;
    .restart local v13    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    .restart local v19    # "squareDB":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 84
    .local v9, "e":Ljava/lang/InstantiationException;
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/InstantiationException;->printStackTrace()V
    :try_end_4
    .catch Landroid/content/ActivityNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 118
    .end local v9    # "e":Ljava/lang/InstantiationException;
    .end local v10    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v12    # "moodIntent":Landroid/content/Intent;
    .end local v13    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_1
    move-exception v9

    .line 119
    .local v9, "e":Landroid/content/ActivityNotFoundException;
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v4, "Activity could not be found."

    invoke-interface {v2, v4}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 123
    if-eqz v19, :cond_4

    .line 125
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 126
    const/16 v19, 0x0

    goto :goto_4

    .line 85
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .restart local v10    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v12    # "moodIntent":Landroid/content/Intent;
    .restart local v13    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_2
    move-exception v9

    .line 86
    .local v9, "e":Ljava/lang/IllegalAccessException;
    :try_start_6
    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_6
    .catch Landroid/content/ActivityNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 120
    .end local v9    # "e":Ljava/lang/IllegalAccessException;
    .end local v10    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v12    # "moodIntent":Landroid/content/Intent;
    .end local v13    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_3
    move-exception v9

    .line 121
    .local v9, "e":Ljava/lang/IllegalStateException;
    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v4, "Failed to find music."

    invoke-interface {v2, v4}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 123
    if-eqz v19, :cond_4

    .line 125
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 126
    const/16 v19, 0x0

    goto :goto_4

    .line 94
    .end local v9    # "e":Ljava/lang/IllegalStateException;
    .restart local v3    # "musicSquareUri":Landroid/net/Uri;
    .restart local v10    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v12    # "moodIntent":Landroid/content/Intent;
    .restart local v13    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :cond_5
    :try_start_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "content://com.sec.android.music/music_square/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->moodType:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_8
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Landroid/content/ActivityNotFoundException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v3

    goto/16 :goto_1

    .line 98
    :catch_4
    move-exception v9

    .line 101
    .local v9, "e":Ljava/lang/SecurityException;
    const/4 v2, 0x1

    :try_start_9
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    .line 106
    goto :goto_2

    .line 102
    .end local v9    # "e":Ljava/lang/SecurityException;
    :catch_5
    move-exception v9

    .line 105
    .local v9, "e":Ljava/lang/IllegalStateException;
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    goto :goto_2

    .line 114
    .end local v9    # "e":Ljava/lang/IllegalStateException;
    :cond_6
    if-eqz v8, :cond_3

    .line 115
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/vlingo/midas/R$string;->mood_music_notification:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->promptUser(Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/content/ActivityNotFoundException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    .line 123
    .end local v3    # "musicSquareUri":Landroid/net/Uri;
    .end local v10    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v12    # "moodIntent":Landroid/content/Intent;
    .end local v13    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catchall_0
    move-exception v2

    if-eqz v19, :cond_7

    .line 125
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 126
    const/16 v19, 0x0

    :cond_7
    throw v2

    .line 130
    .end local v8    # "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    .end local v19    # "squareDB":Landroid/database/Cursor;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 134
    :try_start_a
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 135
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getPlayIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v17

    .line 136
    .local v17, "playIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v2

    if-nez v2, :cond_9

    .line 138
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-nez v2, :cond_9

    .line 141
    const-string/jumbo v2, "launchMusicPlayer"

    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 147
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_a
    .catch Landroid/content/ActivityNotFoundException; {:try_start_a .. :try_end_a} :catch_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_7

    goto/16 :goto_4

    .line 148
    .end local v17    # "playIntent":Landroid/content/Intent;
    :catch_6
    move-exception v9

    .line 149
    .local v9, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v4, "Activity could not be found."

    invoke-interface {v2, v4}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 150
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    :catch_7
    move-exception v11

    .line 151
    .local v11, "iae":Ljava/lang/IllegalArgumentException;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v4, "Illegal argument exception in target activity raised"

    invoke-interface {v2, v4}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 153
    .end local v11    # "iae":Ljava/lang/IllegalArgumentException;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAY:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    if-ne v2, v4, :cond_c

    .line 154
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 155
    new-instance v17, Landroid/content/Intent;

    const-string/jumbo v2, "com.sec.android.app.music.musicservicecommand.play"

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 156
    .restart local v17    # "playIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v2

    if-nez v2, :cond_b

    .line 158
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-nez v2, :cond_b

    .line 161
    const-string/jumbo v2, "launchMusicPlayer"

    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 167
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_4

    .line 168
    .end local v17    # "playIntent":Landroid/content/Intent;
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PAUSE:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    if-ne v2, v4, :cond_d

    .line 169
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 170
    new-instance v16, Landroid/content/Intent;

    const-string/jumbo v2, "com.sec.android.app.music.musicservicecommand.pause"

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 171
    .local v16, "pauseIntent":Landroid/content/Intent;
    const-string/jumbo v2, "isFromSVoice"

    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 175
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_4

    .line 176
    .end local v16    # "pauseIntent":Landroid/content/Intent;
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->NEXT:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    if-ne v2, v4, :cond_f

    .line 177
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 178
    new-instance v14, Landroid/content/Intent;

    const-string/jumbo v2, "com.sec.android.app.music.musicservicecommand.playnext"

    invoke-direct {v14, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 179
    .local v14, "nextIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v2

    if-nez v2, :cond_e

    .line 181
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-nez v2, :cond_e

    .line 184
    const-string/jumbo v2, "launchMusicPlayer"

    const/4 v4, 0x1

    invoke-virtual {v14, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 190
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v14}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_4

    .line 191
    .end local v14    # "nextIntent":Landroid/content/Intent;
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    sget-object v4, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PREVIOUS:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    if-ne v2, v4, :cond_11

    .line 192
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 193
    new-instance v18, Landroid/content/Intent;

    const-string/jumbo v2, "com.sec.android.app.music.musicservicecommand.playprevious"

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 194
    .local v18, "previousIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v2

    if-nez v2, :cond_10

    .line 196
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-nez v2, :cond_10

    .line 199
    const-string/jumbo v2, "launchMusicPlayer"

    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 205
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_4

    .line 207
    .end local v18    # "previousIntent":Landroid/content/Intent;
    :cond_11
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v15

    .line 208
    .local v15, "noMatchMsg":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2, v15}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_4
.end method

.method public getPlayIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 8
    .param p1, "typeStr"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0xb

    .line 213
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v6, "com.sec.android.app.music.intent.action.PLAY_VIA"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 216
    .local v3, "intent":Landroid/content/Intent;
    iget-boolean v6, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->quickList:Z

    if-eqz v6, :cond_3

    .line 217
    sget-object v6, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v6}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v2

    .line 218
    .local v2, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v4, 0x0

    .line 219
    .local v4, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v2, :cond_0

    .line 221
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 228
    :cond_0
    :goto_0
    if-eqz v4, :cond_2

    invoke-interface {v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicAppVersion()I

    move-result v6

    if-ge v6, v7, :cond_2

    .line 229
    const-string/jumbo v5, "Quick list"

    .line 236
    .end local v2    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v4    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    .local v5, "playlistNameLocalized":Ljava/lang/String;
    :goto_1
    sget-object v6, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v6}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v2

    .line 237
    .restart local v2    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v4, 0x0

    .line 238
    .restart local v4    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v2, :cond_1

    .line 240
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v4, v0
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3

    .line 247
    :cond_1
    :goto_2
    const-string/jumbo v6, "Quick list"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    if-eqz v4, :cond_4

    invoke-interface {v4}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicAppVersion()I

    move-result v6

    if-ge v6, v7, :cond_4

    const/4 v6, 0x0

    invoke-interface {v4, v6}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->usingLocalMusicDB(Z)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 249
    const-string/jumbo v6, "extra_type"

    const-string/jumbo v7, "favorite_playlist"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    const-string/jumbo v6, "launchMusicPlayer"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 256
    :goto_3
    return-object v3

    .line 222
    .end local v5    # "playlistNameLocalized":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 223
    .local v1, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 224
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v1

    .line 225
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 231
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :cond_2
    const-string/jumbo v5, "FavoriteList#328795!432@1341"

    .restart local v5    # "playlistNameLocalized":Ljava/lang/String;
    goto :goto_1

    .line 234
    .end local v2    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v4    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    .end local v5    # "playlistNameLocalized":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->name:Ljava/lang/String;

    .restart local v5    # "playlistNameLocalized":Ljava/lang/String;
    goto :goto_1

    .line 241
    .restart local v2    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v4    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_2
    move-exception v1

    .line 242
    .local v1, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_2

    .line 243
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_3
    move-exception v1

    .line 244
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 253
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :cond_4
    const-string/jumbo v6, "extra_type"

    invoke-virtual {v3, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 254
    const-string/jumbo v6, "extra_name"

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3
.end method

.method public name(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->name:Ljava/lang/String;

    .line 45
    return-object p0
.end method

.method public playMusicCharacteristic(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;
    .locals 0
    .param p1, "moodType"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->moodType:Ljava/lang/String;

    .line 55
    return-object p0
.end method

.method public playMusicType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;
    .locals 0
    .param p1, "playMusicType"    # Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    .line 50
    return-object p0
.end method

.method public quickList(Z)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;
    .locals 0
    .param p1, "quickList"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->quickList:Z

    .line 40
    return-object p0
.end method
