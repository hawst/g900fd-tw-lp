.class public final enum Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;
.super Ljava/lang/Enum;
.source "ChatbotWidgetHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NewsCommand"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Down:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Next:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum None:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Pause:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Prev:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Read:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Repeat:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Reset:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Restart:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Resume:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Stop:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field public static final enum Up:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 416
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "None"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->None:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 417
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Read"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Read:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 419
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Next"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Next:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 421
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Prev"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Prev:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 423
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Stop"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Stop:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 425
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Pause"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Pause:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 427
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Resume"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Resume:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 429
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Up"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Up:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 431
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Down"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Down:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 433
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Reset"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Reset:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 435
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Restart"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Restart:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 437
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    const-string/jumbo v1, "Repeat"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Repeat:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 415
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->None:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Read:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Next:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Prev:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Stop:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Pause:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Resume:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Up:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Down:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Reset:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Restart:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Repeat:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->$VALUES:[Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 415
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 415
    const-class v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;
    .locals 1

    .prologue
    .line 415
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->$VALUES:[Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    invoke-virtual {v0}, [Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    return-object v0
.end method
