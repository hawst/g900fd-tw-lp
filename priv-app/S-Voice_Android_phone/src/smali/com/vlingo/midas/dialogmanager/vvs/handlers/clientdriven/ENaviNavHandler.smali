.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ENaviNavHandler.java"


# static fields
.field public static final API_ACT_NAME:Ljava/lang/String; = "com.pdager.enavi.Act.APIMain"

.field public static final BROADCAST_ENAVI_ACTION_RESOLVE_LOC:Ljava/lang/String; = "com.vlingo.midas.ACTION_ENAVI_RESOLVE_LOCATION"

.field private static final CTC_SALES_CODE_VALUE:Ljava/lang/String; = "CTC"

.field public static final ENAVI_REQUEST_CODE:I = 0x76

.field public static final EXTRA_QUERY_VALUE:Ljava/lang/String; = "key_enavi_poi_name"

.field public static final KEY_BUNDLE_NAV_QUERY:Ljava/lang/String; = "key_bundle_enavi_address"

.field private static final NAVIGATE_HOME_TYPE:Ljava/lang/String; = "NAVIGATE_HOME"

.field private static final NAVIGATION_TYPE:Ljava/lang/String; = "NAVIGATE"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.pdager"

.field private static final PARAM_NAVTYPE:Ljava/lang/String; = "Navtype"

.field private static final PARAM_QUERY:Ljava/lang/String; = "Query"

.field private static final SHOW_MAP_TYPE:Ljava/lang/String; = "SHOWMAP"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method

.method public static getBaseNavIntent(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 103
    const/4 v0, 0x0

    .line 104
    .local v0, "intent":Landroid/content/Intent;
    if-nez p0, :cond_0

    .line 105
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.pdager"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 109
    .restart local v0    # "intent":Landroid/content/Intent;
    :goto_0
    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    const-string/jumbo v1, "com.pdager"

    const-string/jumbo v2, "com.pdager.enavi.Act.APIMain"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 112
    return-object v0

    .line 107
    :cond_0
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method

.method private getBroadcast(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 88
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.vlingo.midas.ACTION_ENAVI_RESOLVE_LOCATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 89
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "key_enavi_poi_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    return-object v0
.end method

.method private getIntentAction(Landroid/content/Intent;Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isBroadcast"    # Z

    .prologue
    .line 154
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v0

    return-object v0
.end method

.method public static getNaviagateIntent(DDLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "address"    # Ljava/lang/String;

    .prologue
    .line 124
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getBaseNavIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 125
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, p1, p2, p3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getNavigateIntentWithUri(DD)Landroid/content/Intent;

    move-result-object v0

    .line 128
    return-object v0
.end method

.method public static getNavigateIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 81
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getBaseNavIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 82
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "name"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    const-string/jumbo v1, "code"

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 84
    return-object v0
.end method

.method public static getNavigateIntentWithUri(DD)Landroid/content/Intent;
    .locals 2
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D

    .prologue
    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "http://maps.google.com/maps?navi=+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getBaseNavIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static getShowMapIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "lat"    # Ljava/lang/String;
    .param p1, "lon"    # Ljava/lang/String;

    .prologue
    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "http://maps.google.com/maps?q=+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getBaseNavIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static getShowMapIntentWithQuery(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "query"    # Ljava/lang/String;

    .prologue
    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "http://maps.google.com/maps?q="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getBaseNavIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static isENaviEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 94
    const-string/jumbo v1, "FAKE_CHINESE_NAVIGATION_APP"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    const-string/jumbo v1, "CHINESE_NAVIGATION_ENAVI"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 97
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string/jumbo v1, "com.pdager"

    invoke-static {v1, v0}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getPhoneSalesCode()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "CTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static processResult(Ljava/lang/String;)Landroid/content/Intent;
    .locals 9
    .param p0, "res"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    const-wide v4, 0x414b774000000000L    # 3600000.0

    .line 136
    invoke-static {p0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-object v0

    .line 142
    :cond_1
    const-string/jumbo v1, "%%"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 143
    .local v6, "results":[Ljava/lang/String;
    aget-object v1, v6, v8

    const-string/jumbo v2, "\\^\\^"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 144
    .local v7, "singePOI":[Ljava/lang/String;
    array-length v1, v7

    const/4 v2, 0x4

    if-lt v1, v2, :cond_0

    .line 145
    const/4 v0, 0x2

    aget-object v0, v7, v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    float-to-double v0, v0

    div-double/2addr v0, v4

    const/4 v2, 0x1

    aget-object v2, v7, v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    float-to-double v2, v2

    div-double/2addr v2, v4

    aget-object v4, v7, v8

    const/4 v5, 0x3

    aget-object v5, v7, v5

    const-string/jumbo v8, "address"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getNaviagateIntent(DDLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v8, 0x0

    .line 46
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 48
    const-string/jumbo v6, "Navtype"

    invoke-static {p1, v6, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "navType":Ljava/lang/String;
    const-string/jumbo v6, "Query"

    invoke-static {p1, v6, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 50
    .local v3, "query":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 51
    .local v2, "prompt":Ljava/lang/String;
    const/4 v0, 0x0

    .line 53
    .local v0, "intentAction":Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;
    const-string/jumbo v6, "NAVIGATE_HOME"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 54
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getHomeAddress()Ljava/lang/String;

    move-result-object v3

    .line 55
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 56
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 57
    .local v4, "shownText":Ljava/lang/String;
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 58
    .local v5, "spokenText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    invoke-interface {v6, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .end local v4    # "shownText":Ljava/lang/String;
    .end local v5    # "spokenText":Ljava/lang/String;
    :cond_0
    :goto_0
    return v8

    .line 61
    :cond_1
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getNavigateIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-direct {p0, v6, v8}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getIntentAction(Landroid/content/Intent;Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v0

    .line 62
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_navigate_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 71
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v6

    const-string/jumbo v7, "navigate"

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 73
    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    invoke-interface {v6, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    goto :goto_0

    .line 64
    :cond_2
    const-string/jumbo v6, "SHOWMAP"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 65
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getShowMapIntentWithQuery(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-direct {p0, v6, v8}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getIntentAction(Landroid/content/Intent;Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v0

    goto :goto_1

    .line 67
    :cond_3
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getNavigateIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-direct {p0, v6, v8}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getIntentAction(Landroid/content/Intent;Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v0

    .line 68
    sget-object v6, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NAVIGATE_TO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v3, v7, v8

    invoke-static {v6, v7}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method
