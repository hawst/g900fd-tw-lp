.class Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SamsungSettingChangeAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothChangeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .param p2, "x1"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;

    .prologue
    .line 411
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 415
    const-string/jumbo v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 418
    const-string/jumbo v1, "android.bluetooth.adapter.extra.STATE"

    const/high16 v2, -0x80000000

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 420
    .local v0, "state":I
    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 421
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver$1;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 451
    .end local v0    # "state":I
    :cond_0
    :goto_0
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 452
    # operator-- for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiBlueReceiverCount:I
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$610()I

    .line 453
    return-void

    .line 435
    .restart local v0    # "state":I
    :cond_1
    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 437
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver$2;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver$2;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
