.class public Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "PlayOutNewsMultiAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$1;,
        Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;,
        Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    }
.end annotation


# instance fields
.field private isPlayingFirst:Z

.field private playbackListener:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;

.field private prepareAllRequestsAsyncTask:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

.field private requests:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/audio/TTSRequest;",
            ">;"
        }
    .end annotation
.end field

.field private ttsStrings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    .line 31
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->requests:Ljava/util/LinkedList;

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->isPlayingFirst:Z

    .line 107
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->requests:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->isPlayingFirst:Z

    return v0
.end method

.method static synthetic access$202(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->isPlayingFirst:Z

    return p1
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    return-object v0
.end method

.method static synthetic access$302(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;)Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;
    .param p1, "x1"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    return-object p1
.end method

.method static synthetic access$400(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method private playTts()V
    .locals 6

    .prologue
    .line 49
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v4

    const-string/jumbo v5, "No news found."

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 73
    :cond_1
    return-void

    .line 56
    :cond_2
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 57
    .local v3, "tts":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/core/internal/audio/TTSRequest;->getResult(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;

    move-result-object v2

    .line 59
    .local v2, "request":Lcom/vlingo/core/internal/audio/TTSRequest;
    new-instance v4, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;-><init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$1;)V

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->playbackListener:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;

    .line 62
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->playbackListener:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->play(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 64
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->setTtsCachingOn()V

    .line 66
    const/4 v1, 0x0

    .line 67
    .local v1, "listSize":I
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    if-eqz v4, :cond_3

    .line 68
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    .line 70
    :cond_3
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 71
    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->requests:Ljava/util/LinkedList;

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/vlingo/core/internal/audio/TTSRequest;->getResult(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected execute()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->playTts()V

    .line 44
    return-void
.end method

.method public tts(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "ttses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->ttsStrings:Ljava/util/List;

    .line 37
    return-void
.end method
