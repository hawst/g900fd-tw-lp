.class public Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SamsungSendEmailAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$SamsungSendEmailActionHandler;
    }
.end annotation


# static fields
.field private static final RECEIVE_MSG:I = 0x64

.field public static final REQUEST_CODE_ATTACH_MEDIA:I = 0xa

.field private static final SENDING_FAIL:I = 0x0

.field private static final SENDING_SUCCESS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "BackgroundSender.EmailBackgroundSenderService"

.field private static jobIDByGallery:I

.field private static jobIDByQuickPannel:I

.field private static jobIDByViceQuick:I


# instance fields
.field private contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private mCallbackBinder:Lcom/android/email/backgroundsender/IEmailRemoteService;

.field private mCallbackConn:Landroid/content/ServiceConnection;

.field private mCallbackConnected:Z

.field private mEmailSendCallback:Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;

.field private mHandler:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$SamsungSendEmailActionHandler;

.field private mJobID:I

.field private mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

.field mServiceConn:Landroid/content/ServiceConnection;

.field private mSvcConnected:Z

.field private message:Ljava/lang/String;

.field private retriesLeft:I

.field private subject:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x3e8

    .line 45
    sput v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->jobIDByQuickPannel:I

    .line 46
    const/16 v0, 0x7d0

    sput v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->jobIDByGallery:I

    .line 47
    sput v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->jobIDByViceQuick:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    .line 48
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mJobID:I

    .line 50
    const/16 v0, 0x64

    iput v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->retriesLeft:I

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contacts:Ljava/util/List;

    .line 57
    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    .line 58
    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackBinder:Lcom/android/email/backgroundsender/IEmailRemoteService;

    .line 60
    iput-boolean v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mSvcConnected:Z

    .line 61
    iput-boolean v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackConnected:Z

    .line 98
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$SamsungSendEmailActionHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$SamsungSendEmailActionHandler;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mHandler:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$SamsungSendEmailActionHandler;

    .line 100
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$1;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceConn:Landroid/content/ServiceConnection;

    .line 243
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$2;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mEmailSendCallback:Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;

    .line 256
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackConn:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->jobIDByQuickPannel:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->jobIDByGallery:I

    return v0
.end method

.method static synthetic access$1002(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackConnected:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mEmailSendCallback:Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->jobIDByViceQuick:I

    return v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    return-object v0
.end method

.method static synthetic access$502(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Lcom/android/email/backgroundsender/IEmailBackgroundSender;)Lcom/android/email/backgroundsender/IEmailBackgroundSender;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
    .param p1, "x1"    # Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    return-object p1
.end method

.method static synthetic access$602(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mSvcConnected:Z

    return p1
.end method

.method static synthetic access$700(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->sendMessage()V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$SamsungSendEmailActionHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mHandler:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$SamsungSendEmailActionHandler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/android/email/backgroundsender/IEmailRemoteService;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackBinder:Lcom/android/email/backgroundsender/IEmailRemoteService;

    return-object v0
.end method

.method static synthetic access$902(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Lcom/android/email/backgroundsender/IEmailRemoteService;)Lcom/android/email/backgroundsender/IEmailRemoteService;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
    .param p1, "x1"    # Lcom/android/email/backgroundsender/IEmailRemoteService;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackBinder:Lcom/android/email/backgroundsender/IEmailRemoteService;

    return-object p1
.end method

.method private contacts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contacts:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contacts:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contacts:Ljava/util/List;

    goto :goto_0
.end method

.method private extractAddresses()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contacts()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 227
    .local v2, "listSize":I
    new-array v0, v2, [Ljava/lang/String;

    .line 228
    .local v0, "addresses":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 229
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contacts()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/contacts/ContactData;

    iget-object v3, v3, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    aput-object v3, v0, v1

    .line 228
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    :cond_0
    return-object v0
.end method

.method private sendMessage()V
    .locals 4

    .prologue
    .line 192
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    invoke-interface {v2}, Lcom/android/email/backgroundsender/IEmailBackgroundSender;->hasAccount()Z

    move-result v2

    if-nez v2, :cond_0

    .line 194
    const-string/jumbo v2, "BackgroundSender.EmailBackgroundSenderService"

    const-string/jumbo v3, "no account! can\'t send a email"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :goto_0
    return-void

    .line 198
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.email.backgroundsender.SEND_BACKGROUND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 200
    .local v1, "mIntent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->extractAddresses()[Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "addresses":[Ljava/lang/String;
    const-string/jumbo v2, "to"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->subject:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 206
    const-string/jumbo v2, "subject"

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->subject:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    :cond_1
    const-string/jumbo v2, "text"

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->message:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mServiceBinder:Lcom/android/email/backgroundsender/IEmailBackgroundSender;

    iget v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mJobID:I

    invoke-interface {v2, v3, v1}, Lcom/android/email/backgroundsender/IEmailBackgroundSender;->sendMessage(ILandroid/content/Intent;)V

    .line 214
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 215
    .end local v0    # "addresses":[Ljava/lang/String;
    .end local v1    # "mIntent":Landroid/content/Intent;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private updateMRU()V
    .locals 6

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contacts()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 236
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-static {}, Lcom/vlingo/core/internal/contacts/mru/MRU;->getMRU()Lcom/vlingo/core/internal/contacts/mru/MRU;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/contacts/ContactType;->EMAIL:Lcom/vlingo/core/internal/contacts/ContactType;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/contacts/ContactType;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/vlingo/core/internal/contacts/ContactData;->contact:Lcom/vlingo/core/internal/contacts/ContactMatch;

    iget-wide v4, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    long-to-int v4, v4

    iget-object v5, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/vlingo/core/internal/contacts/mru/MRU;->incrementCount(Ljava/lang/String;ILjava/lang/String;)F

    goto :goto_0

    .line 238
    .end local v0    # "contact":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic contact(Lcom/vlingo/core/internal/contacts/ContactData;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contact(Lcom/vlingo/core/internal/contacts/ContactData;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    move-result-object v0

    return-object v0
.end method

.method public contact(Lcom/vlingo/core/internal/contacts/ContactData;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
    .locals 1
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactData;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contacts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    return-object p0
.end method

.method public bridge synthetic contacts(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    .locals 1
    .param p1, "x0"    # Ljava/util/List;

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contacts(Ljava/util/List;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    move-result-object v0

    return-object v0
.end method

.method public contacts(Ljava/util/List;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)",
            "Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->contacts:Ljava/util/List;

    .line 139
    return-object p0
.end method

.method protected execute()V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public bridge synthetic message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->message(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    move-result-object v0

    return-object v0
.end method

.method public message(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->message:Ljava/lang/String;

    .line 154
    return-object p0
.end method

.method public bridge synthetic subject(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SendEmailInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->subject(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    move-result-object v0

    return-object v0
.end method

.method public subject(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
    .locals 0
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->subject:Ljava/lang/String;

    .line 149
    return-object p0
.end method
