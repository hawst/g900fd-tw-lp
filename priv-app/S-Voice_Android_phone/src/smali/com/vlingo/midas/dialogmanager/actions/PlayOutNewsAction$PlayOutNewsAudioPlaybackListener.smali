.class Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;
.super Ljava/lang/Object;
.source "PlayOutNewsAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayOutNewsAudioPlaybackListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;
    .param p2, "x1"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$1;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;-><init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;)V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 72
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "request ignored, reason="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "msg":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->access$300(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->access$100(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 60
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "request ignored, reason="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "msg":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->access$200(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 41
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener$1;-><init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;)V

    const-wide/16 v1, 0x5dc

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 53
    return-void
.end method
