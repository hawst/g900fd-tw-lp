.class Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction$1;
.super Ljava/lang/Object;
.source "LocateCarAction.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->execute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 33
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->locateCar()Z
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->access$000(Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;)Z

    move-result v0

    .line 34
    .local v0, "locatedCar":Z
    if-eqz v0, :cond_0

    .line 35
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->launchFindMyCar()V
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->access$100(Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;)V

    .line 36
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->access$200(Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->access$300(Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "locateCar failed"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method
