.class Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction$3;
.super Landroid/os/Handler;
.source "SamsungLaunchActivityAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 82
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 83
    const-string/jumbo v0, "always"

    const-string/jumbo v1, "mHandler is called, But Time is out"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;->waitingForCoverOpened:Z
    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;->access$002(Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;Z)Z

    .line 86
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;

    iget-object v1, v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 88
    :cond_0
    return-void
.end method
