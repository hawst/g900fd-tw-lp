.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyContactNumberUtils;
.super Ljava/lang/Object;
.source "EmergencyContactNumberUtils.java"


# static fields
.field public static final TYPE_EMERGENCY_CONTACT:I = 0x1

.field public static final TYPE_FAVORITE_CONTACT:I = 0x2

.field public static final TYPE_FREQUENT_CONTACT:I = 0x3

.field public static final TYPE_NORMAL_CONTACT:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContactNumbers(Landroid/content/Context;II)Ljava/util/List;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I
    .param p2, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    const/4 v6, 0x4

    if-ne p1, v6, :cond_1

    .line 64
    invoke-static {p0, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyContactNumberUtils;->getNormalContactIds(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v2

    .line 68
    .local v2, "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v5, "numbers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 70
    .local v0, "contactId":J
    invoke-static {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyContactNumberUtils;->getDefaultOrLastUsedNumber(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 71
    .local v4, "number":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 72
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 66
    .end local v0    # "contactId":J
    .end local v2    # "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "number":Ljava/lang/String;
    .end local v5    # "numbers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyContactNumberUtils;->getLastUsedContactIds(Landroid/content/Context;II)Ljava/util/List;

    move-result-object v2

    .restart local v2    # "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    goto :goto_0

    .line 75
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v5    # "numbers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    return-object v5
.end method

.method private static getDefaultOrLastUsedNumber(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contactId"    # J

    .prologue
    const/4 v8, 0x0

    .line 182
    const/4 v7, 0x0

    .line 184
    .local v7, "number":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "data1"

    aput-object v0, v2, v8

    const/4 v0, 0x1

    const-string/jumbo v1, "display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "last_time_used"

    aput-object v1, v2, v0

    .line 185
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "mimetype=\'vnd.android.cursor.item/phone_v2\' AND contact_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 187
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v5, "is_super_primary DESC, last_time_used DESC"

    .line 189
    .local v5, "sortOrder":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 191
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 192
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 195
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 198
    :cond_1
    return-object v7
.end method

.method public static getEmergencyContactNumbers(Landroid/content/Context;)Ljava/util/List;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 27
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 29
    .local v9, "numbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v3, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "title"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "ICE"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "contacts"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "emergency"

    const-string/jumbo v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "defaultId"

    const-string/jumbo v5, "3"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 34
    .local v1, "uri":Landroid/net/Uri;
    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/String;

    const-string/jumbo v3, "data1"

    aput-object v3, v2, v10

    const/4 v3, 0x1

    const-string/jumbo v4, "display_name"

    aput-object v4, v2, v3

    .line 36
    .local v2, "PROJECTION":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 38
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 40
    .local v6, "c":Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 41
    if-eqz v6, :cond_1

    .line 42
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 43
    const/4 v3, 0x0

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 44
    .local v8, "number":Ljava/lang/String;
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 47
    .end local v8    # "number":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 48
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 50
    if-eqz v6, :cond_0

    .line 51
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 55
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    :goto_1
    return-object v9

    .line 50
    :cond_1
    if-eqz v6, :cond_0

    .line 51
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 50
    :catchall_0
    move-exception v3

    if-eqz v6, :cond_2

    .line 51
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v3
.end method

.method private static getLastUsedContactIds(Landroid/content/Context;II)Ljava/util/List;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I
    .param p2, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 103
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "contact_id"

    aput-object v0, v2, v10

    const/4 v0, 0x1

    const-string/jumbo v1, "last_time_used"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "display_name"

    aput-object v1, v2, v0

    .line 105
    .local v2, "projection":[Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyContactNumberUtils;->getSelection(I)Ljava/lang/String;

    move-result-object v3

    .line 106
    .local v3, "selection":Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyContactNumberUtils;->getSortOrder(I)Ljava/lang/String;

    move-result-object v5

    .line 108
    .local v5, "sortOrder":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 111
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 112
    .local v8, "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz v9, :cond_3

    .line 113
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 115
    .local v6, "contactId":J
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_1
    if-lez p2, :cond_0

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, p2, :cond_0

    .line 122
    .end local v6    # "contactId":J
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 125
    :cond_3
    return-object v8
.end method

.method private static getNormalContactIds(Landroid/content/Context;I)Ljava/util/List;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 79
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 80
    .local v6, "builder":Landroid/net/Uri$Builder;
    if-lez p1, :cond_0

    .line 81
    const-string/jumbo v0, "limit"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 83
    :cond_0
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 84
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "_id"

    aput-object v0, v2, v11

    .line 85
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "has_phone_number!=0"

    .line 86
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v5, "sort_key"

    .line 88
    .local v5, "sortOrder":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 91
    .local v8, "cursor":Landroid/database/Cursor;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v7, "contactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz v8, :cond_2

    .line 93
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    invoke-interface {v8, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 99
    :cond_2
    return-object v7
.end method

.method private static getSelection(I)Ljava/lang/String;
    .locals 4
    .param p0, "type"    # I

    .prologue
    .line 131
    packed-switch p0, :pswitch_data_0

    .line 152
    const/4 v1, 0x0

    .line 156
    .local v1, "selection":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 133
    .end local v1    # "selection":Ljava/lang/String;
    :pswitch_0
    const-string/jumbo v0, "( SELECT contact_id FROM view_data_restricted WHERE mimetype=\'vnd.android.cursor.item/group_membership\' AND title=\'ICE\')"

    .line 137
    .local v0, "emergencyContactIdSelection":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "contact_id in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "mimetype"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 139
    .restart local v1    # "selection":Ljava/lang/String;
    goto :goto_0

    .line 142
    .end local v0    # "emergencyContactIdSelection":Ljava/lang/String;
    .end local v1    # "selection":Ljava/lang/String;
    :pswitch_1
    const-string/jumbo v1, "starred!=0 AND mimetype=\'vnd.android.cursor.item/phone_v2\'"

    .line 144
    .restart local v1    # "selection":Ljava/lang/String;
    goto :goto_0

    .line 147
    .end local v1    # "selection":Ljava/lang/String;
    :pswitch_2
    const-string/jumbo v1, "times_used>0 AND mimetype=\'vnd.android.cursor.item/phone_v2\'"

    .line 149
    .restart local v1    # "selection":Ljava/lang/String;
    goto :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static getSortOrder(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 162
    packed-switch p0, :pswitch_data_0

    .line 174
    const/4 v0, 0x0

    .line 178
    .local v0, "sortOrder":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 165
    .end local v0    # "sortOrder":Ljava/lang/String;
    :pswitch_0
    const-string/jumbo v0, "last_time_used DESC, sort_key"

    .line 166
    .restart local v0    # "sortOrder":Ljava/lang/String;
    goto :goto_0

    .line 169
    .end local v0    # "sortOrder":Ljava/lang/String;
    :pswitch_1
    const-string/jumbo v0, "times_used DESC, last_time_used DESC, sort_key"

    .line 171
    .restart local v0    # "sortOrder":Ljava/lang/String;
    goto :goto_0

    .line 162
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
