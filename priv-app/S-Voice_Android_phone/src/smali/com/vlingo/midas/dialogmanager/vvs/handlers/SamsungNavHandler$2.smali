.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$2;
.super Landroid/os/Handler;
.source "SamsungNavHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 132
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 133
    const-string/jumbo v0, "always"

    const-string/jumbo v1, "mHandler is called, But Time is out"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->waitingForCoverOpened:Z
    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;Z)Z

    .line 136
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    iget-object v1, v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 138
    :cond_0
    return-void
.end method
