.class public Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;
.super Lcom/vlingo/core/internal/dialogmanager/Controller;
.source "AddMemoController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private memoText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;-><init>()V

    return-void
.end method

.method private findCreatedMemoId(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "memoText"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/MemoManager;->getMemoUtil()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v1

    .line 137
    .local v1, "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    const/4 v0, 0x0

    .line 139
    .local v0, "memo":Lcom/vlingo/core/internal/memo/Memo;
    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/vlingo/core/internal/memo/IMemoUtil;->getMostRecentlyCreatedMemo(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/core/internal/memo/Memo;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {v0}, Lcom/vlingo/core/internal/memo/Memo;->getId()I
    :try_end_0
    .catch Lcom/vlingo/core/internal/memo/MemoUtilException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 149
    :goto_0
    return v2

    .line 145
    :catch_0
    move-exception v2

    .line 149
    :cond_0
    const/4 v2, -0x2

    goto :goto_0
.end method

.method private save(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)V
    .locals 3
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    .prologue
    .line 206
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SAVE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v2, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/MemoInterface;

    invoke-virtual {p0, v1, v2, p1}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/MemoInterface;

    .line 207
    .local v0, "memoInterface":Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/MemoInterface;
    if-eqz v0, :cond_0

    .line 208
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->memoText:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/MemoInterface;->memo(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/MemoInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/MemoInterface;->queue()V

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->memoText:Ljava/lang/String;

    sget-object v2, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;->MEMO:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->sendAcceptedText(Ljava/lang/String;Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$TextType;)V

    .line 213
    return-void
.end method

.method private viewMemo(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)V
    .locals 6
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    .prologue
    .line 185
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 186
    .local v0, "context":Landroid/content/Context;
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->memoText:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->findCreatedMemoId(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 187
    .local v1, "id":I
    new-instance v2, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/MemoManager;->getMemoUtil()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/memo/IMemoUtil;->getViewMemoAction()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 188
    .local v2, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->isInstalled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 189
    const-string/jumbo v3, "id"

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 190
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 203
    :goto_0
    return-void

    .line 191
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->isInstalled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 192
    const-string/jumbo v3, "content://com.samsung.android.memo/memo"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    int-to-long v4, v1

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 194
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 195
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->isInstalled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 196
    const-string/jumbo v3, "id"

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 197
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 199
    :cond_2
    const-string/jumbo v3, "memoID"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 200
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 231
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/Controller;->actionFail(Ljava/lang/String;)V

    .line 232
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_not_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    invoke-virtual {v1, v2, v3, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 235
    return-void
.end method

.method public actionSuccess()V
    .locals 4

    .prologue
    .line 221
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/Controller;->actionSuccess()V

    .line 222
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lcom/vlingo/midas/R$string;->core_car_tts_MEMO_SAVED:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    invoke-virtual {v1, v2, v3, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 225
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 12
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 89
    const/4 v5, 0x0

    .line 91
    .local v5, "needLocalSystemTurn":Z
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 93
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "PopulateTextbox"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 94
    const-string/jumbo v8, "Action"

    invoke-static {p1, v8, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "actionName":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_5

    const-string/jumbo v8, "append"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 96
    const-string/jumbo v8, "Value"

    invoke-static {p1, v8, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "addedText":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 98
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->memoText:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->memoText:Ljava/lang/String;

    .line 105
    .end local v1    # "addedText":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v5, 0x1

    .line 111
    .end local v0    # "actionName":Ljava/lang/String;
    :goto_1
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    .line 112
    .local v2, "context":Landroid/content/Context;
    instance-of v8, v2, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v8, :cond_7

    check-cast v2, Lcom/vlingo/midas/gui/ConversationActivity;

    .end local v2    # "context":Landroid/content/Context;
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ConversationActivity;->isDrivingMode()Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    move-result-object v8

    sget-object v9, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    if-ne v8, v9, :cond_7

    move v3, v6

    .line 113
    .local v3, "inDrivingMode":Z
    :goto_2
    if-nez v3, :cond_1

    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->memoText:Ljava/lang/String;

    if-eqz v8, :cond_4

    .line 114
    :cond_1
    new-instance v4, Lcom/vlingo/core/internal/memo/Memo;

    invoke-direct {v4}, Lcom/vlingo/core/internal/memo/Memo;-><init>()V

    .line 115
    .local v4, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->memoText:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lcom/vlingo/core/internal/memo/Memo;->setText(Ljava/lang/String;)V

    .line 116
    const-string/jumbo v8, "doit"

    invoke-static {p1, v8, v7, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 117
    invoke-direct {p0, p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->save(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)V

    .line 118
    invoke-virtual {v4}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v8

    const/4 v9, -0x1

    if-ne v8, v9, :cond_2

    .line 122
    const/4 v8, -0x2

    invoke-virtual {v4, v8}, Lcom/vlingo/core/internal/memo/Memo;->setId(I)V

    .line 125
    :cond_2
    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Memo:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v9, 0x0

    invoke-interface {p2, v8, v9, v4, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 126
    if-nez v5, :cond_3

    const-string/jumbo v8, "doit"

    invoke-static {p1, v8, v7, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v8

    if-nez v8, :cond_4

    .line 128
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->memoText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->say_save_memo:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v10}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v10

    invoke-virtual {v8, v9, v6, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 132
    .end local v4    # "memo":Lcom/vlingo/core/internal/memo/Memo;
    :cond_4
    return v7

    .line 101
    .end local v3    # "inDrivingMode":Z
    .restart local v0    # "actionName":Ljava/lang/String;
    :cond_5
    const-string/jumbo v8, "Value"

    invoke-static {p1, v8, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->memoText:Ljava/lang/String;

    goto/16 :goto_0

    .line 107
    .end local v0    # "actionName":Ljava/lang/String;
    :cond_6
    const-string/jumbo v8, "memo"

    invoke-static {p1, v8, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->memoText:Ljava/lang/String;

    goto/16 :goto_1

    :cond_7
    move v3, v7

    .line 112
    goto :goto_2
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 168
    const-string/jumbo v0, "memo.save"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 172
    invoke-direct {p0, p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->save(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)V

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    const-string/jumbo v0, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 175
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 176
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->reset()V

    goto :goto_0

    .line 177
    :cond_2
    const-string/jumbo v0, "memo.findmostrecent"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 179
    invoke-direct {p0, p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddMemoController;->viewMemo(Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;)V

    goto :goto_0
.end method
