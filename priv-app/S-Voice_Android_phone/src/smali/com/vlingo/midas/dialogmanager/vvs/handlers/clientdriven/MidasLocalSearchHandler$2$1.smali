.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2$1;
.super Ljava/lang/Object;
.source "MidasLocalSearchHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;

.field final synthetic val$searchQuery:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2$1;->this$1:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;

    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2$1;->val$searchQuery:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 130
    :try_start_0
    new-instance v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;

    invoke-direct {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;-><init>()V

    .line 131
    .local v1, "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2$1;->this$1:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;

    iget-object v3, v3, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->access$700(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 132
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2$1;->val$searchQuery:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->getWebSearchURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 133
    .local v2, "url":Ljava/lang/String;
    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->executeSearchIntentFromURL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .end local v1    # "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
    .end local v2    # "url":Ljava/lang/String;
    :goto_0
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
