.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;
.super Ljava/lang/Object;
.source "SamsungContactLookupHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/contacts/ContactMatchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalContactMatchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;
    .param p2, "x1"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$1;

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)V

    return-void
.end method


# virtual methods
.method public onAutoAction(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 0
    .param p1, "contact"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    .line 126
    return-void
.end method

.method public onContactMatchResultsUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    return-void
.end method

.method public onContactMatchingFailed()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$1100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 161
    return-void
.end method

.method public onContactMatchingFinished(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    const/4 v3, 0x1

    .line 136
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 137
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CONTACT_NAME:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 139
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->name:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getContactNotFoundString(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$400(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$500(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$1000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 153
    return-void

    .line 140
    :cond_2
    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_4

    .line 141
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->hasContactsData(Ljava/util/List;)Z
    invoke-static {v0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$600(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 142
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$700(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;)V

    .line 143
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->multipleContacts(Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$1000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v0

    .line 145
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->getTurnContactsDoNotType()Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$800(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->showSystemTurn(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->access$900(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 148
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler$LocalContactMatchListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;->openContact(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
