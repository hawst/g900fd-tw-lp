.class Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$3;
.super Landroid/os/Handler;
.source "SamsungOpenAppAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 75
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 76
    const-string/jumbo v0, "always"

    const-string/jumbo v1, "mHandler is called, But Time is out"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->waitingForCoverOpened:Z
    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->access$002(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;Z)Z

    .line 78
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    iget-object v1, v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 80
    :cond_0
    return-void
.end method
