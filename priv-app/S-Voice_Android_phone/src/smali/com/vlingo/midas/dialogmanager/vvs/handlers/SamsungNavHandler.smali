.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;
.super Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavBaseHandler;
.source "SamsungNavHandler.java"


# static fields
.field private static final MSG_WAITING_FOR_COVER_OPENED:I


# instance fields
.field mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field final mHandler:Landroid/os/Handler;

.field private waitingForCoverOpened:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavBaseHandler;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->waitingForCoverOpened:Z

    .line 129
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$2;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->waitingForCoverOpened:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->waitingForCoverOpened:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "x2"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->doExecuteActionSamsung(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "x2"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->doExecuteActionOriginal(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method

.method private doExecuteActionOriginal(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 8
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v7, 0x0

    .line 142
    const/4 v1, 0x0

    .line 143
    .local v1, "className":Ljava/lang/String;
    const-string/jumbo v5, "IntentName"

    const/4 v6, 0x1

    invoke-static {p1, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 144
    .local v4, "name":Ljava/lang/String;
    const-string/jumbo v5, "IntentArgument"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "arg":Ljava/lang/String;
    const-string/jumbo v5, "Extras"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 146
    .local v2, "extras":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->isGoogleNavAvailable()Z

    move-result v5

    if-nez v5, :cond_0

    .line 147
    const-string/jumbo v5, "ClassName"

    invoke-static {p1, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 149
    :cond_0
    const-string/jumbo v5, "broadcast"

    invoke-static {p1, v5, v7, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v3

    .line 150
    .local v3, "isBroadcast":Z
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v6, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v5, v6}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v5, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->className(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->broadcast(Z)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 158
    return v7
.end method

.method private doExecuteActionSamsung(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v5, 0x0

    .line 164
    const-string/jumbo v3, "IntentArgument"

    invoke-static {p1, v3, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "arg":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 166
    .local v2, "intentToLaunch":Landroid/content/Intent;
    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    const-string/jumbo v3, "com.google.android.apps.maps"

    const-string/jumbo v4, "com.google.android.maps.driveabout.app.NavigationActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 169
    .local v1, "data":Landroid/net/Uri;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 170
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 171
    const v3, 0x8000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 174
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {v3, v2}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->intent(Landroid/content/Intent;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 178
    return v5
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 13
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavBaseHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 50
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    .line 51
    .local v4, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->doesLocationUseNetworkProvider()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->core_not_detected_current_location:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 53
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 54
    const/4 v0, 0x0

    .line 126
    :goto_0
    return v0

    .line 57
    :cond_0
    const-string/jumbo v0, "location"

    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/LocationManager;

    .line 58
    .local v7, "lm":Landroid/location/LocationManager;
    const-string/jumbo v0, "network"

    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v6

    .line 60
    .local v6, "isGpsEnable":Z
    if-nez v6, :cond_1

    .line 61
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->core_navigation_default_location:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 62
    .local v9, "prompt":Ljava/lang/String;
    invoke-interface {p2, v9, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 64
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    .end local v9    # "prompt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v0, "Query"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v10

    .line 68
    .local v10, "query":Ljava/lang/String;
    const/4 v9, 0x0

    .line 69
    .restart local v9    # "prompt":Ljava/lang/String;
    invoke-static {v10}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 70
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NAVIGATE_TO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v10, v1, v3

    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 73
    :cond_2
    move-object v2, v9

    .line 74
    .local v2, "promptForCover":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v0, v4}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 75
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v8

    .line 77
    .local v8, "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;

    move-object v1, p0

    move-object v3, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Landroid/content/Context;Lcom/vlingo/sdk/recognition/VLAction;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 103
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    if-nez v0, :cond_3

    .line 105
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "maphandler ExcuteAction. CoverClosed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/vlingo/midas/R$string;->sview_cover_alwaysmicon:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 112
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 113
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v11, 0x2710

    invoke-virtual {v0, v1, v11, v12}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->waitingForCoverOpened:Z

    .line 126
    :goto_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 117
    :cond_3
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 118
    invoke-interface {p2, v9, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_4
    instance-of v0, v4, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v0, :cond_5

    .line 121
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->doExecuteActionSamsung(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    goto :goto_1

    .line 123
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;->doExecuteActionOriginal(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    goto :goto_1
.end method
