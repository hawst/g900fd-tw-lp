.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "CarModeHandler.java"


# instance fields
.field private activityName:Ljava/lang/String;

.field private enableCarModeAppAction:Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

.field private mContext:Landroid/content/Context;

.field private packageName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 18
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getCarModePackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->packageName:Ljava/lang/String;

    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".firstaccess.DisclaimerActivity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->activityName:Ljava/lang/String;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->enableCarModeAppAction:Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    return-void
.end method

.method private disableCarModeApp()V
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "car_mode_on"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 88
    return-void
.end method

.method private getEnableCarModeAction()Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->enclosingPackage(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->activityName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->activity(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v0

    return-object v0
.end method

.method private isCarModeEnabled()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "car_mode_on"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-ne v3, v1, :cond_0

    .line 100
    :goto_0
    return v1

    :cond_0
    move v1, v2

    .line 98
    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    move v1, v2

    .line 100
    goto :goto_0
.end method


# virtual methods
.method public actionSuccess()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "car_mode_on"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 93
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionSuccess()V

    .line 94
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x0

    .line 26
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasCarModeApp()Z

    move-result v4

    if-nez v4, :cond_1

    .line 27
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->core_car_tts_NO_APPMATCH_DEMAND:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 28
    .local v1, "drivelinkApplicationNotFound":Ljava/lang/String;
    invoke-interface {p2, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .end local v1    # "drivelinkApplicationNotFound":Ljava/lang/String;
    :cond_0
    :goto_0
    return v6

    .line 32
    :cond_1
    const-string/jumbo v4, "confirmOn"

    invoke-interface {p1, v4}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "confirm":Ljava/lang/String;
    const-string/jumbo v3, ""

    .line 35
    .local v3, "text":Ljava/lang/String;
    const/4 v2, 0x0

    .line 37
    .local v2, "isShowSystemTurn":Z
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->mContext:Landroid/content/Context;

    .line 39
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->enableCarModeAppAction:Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    .line 41
    const-string/jumbo v4, "on"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 43
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->isCarModeEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 44
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_already_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 70
    :goto_1
    if-eqz v2, :cond_2

    .line 71
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v3, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_2
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->enableCarModeAppAction:Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    if-eqz v4, :cond_0

    .line 75
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->enableCarModeAppAction:Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->queue()V

    goto :goto_0

    .line 46
    :cond_3
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 47
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->getEnableCarModeAction()Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->enableCarModeAppAction:Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    goto :goto_1

    .line 50
    :cond_4
    const-string/jumbo v4, "off"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 51
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->isCarModeEnabled()Z

    move-result v4

    if-nez v4, :cond_5

    .line 52
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_already_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 53
    const/4 v2, 0x1

    goto :goto_1

    .line 55
    :cond_5
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 56
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->disableCarModeApp()V

    goto :goto_1

    .line 59
    :cond_6
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->isCarModeEnabled()Z

    move-result v4

    if-nez v4, :cond_7

    .line 60
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 61
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->getEnableCarModeAction()Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->enableCarModeAppAction:Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    goto :goto_1

    .line 63
    :cond_7
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 64
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;->disableCarModeApp()V

    goto :goto_1
.end method
