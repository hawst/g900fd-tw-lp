.class public Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SocialUpdateAction.java"

# interfaces
.implements Lcom/vlingo/midas/social/api/FacebookAPI$FacebookAPICallback;
.implements Lcom/vlingo/midas/social/api/TwitterAPI$TwitterCallback;
.implements Lcom/vlingo/midas/social/api/WeiboAPI$WeiboCallBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private apis:Lcom/vlingo/core/internal/util/SparseArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vlingo/core/internal/util/SparseArrayMap",
            "<",
            "Lcom/vlingo/midas/social/api/SocialAPI;",
            ">;"
        }
    .end annotation
.end field

.field private apisUpdating:I

.field private errorMsg:Ljava/lang/String;

.field private hasFailed:Z

.field private status:Ljava/lang/String;

.field private type:Lcom/vlingo/midas/social/api/SocialNetworkType;

.field private updateQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/vlingo/midas/social/api/SocialAPI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    .line 42
    iput v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apisUpdating:I

    .line 44
    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->hasFailed:Z

    .line 47
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->updateQueue:Ljava/util/Queue;

    return-void
.end method

.method private isNetworkEnabled(Lcom/vlingo/midas/social/api/SocialAPI;)Z
    .locals 1
    .param p1, "api"    # Lcom/vlingo/midas/social/api/SocialAPI;

    .prologue
    .line 149
    invoke-virtual {p1}, Lcom/vlingo/midas/social/api/SocialAPI;->isEnabled()Z

    move-result v0

    return v0
.end method

.method private performAllUpdates()V
    .locals 2

    .prologue
    .line 141
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->updateQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->updateQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/social/api/SocialAPI;

    .line 143
    .local v0, "api":Lcom/vlingo/midas/social/api/SocialAPI;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/api/SocialAPI;->setUpdateStatus(I)V

    .line 144
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/social/api/SocialAPI;->updateStatus(Ljava/lang/String;)V

    goto :goto_0

    .line 146
    .end local v0    # "api":Lcom/vlingo/midas/social/api/SocialAPI;
    :cond_0
    return-void
.end method

.method private performUpdate()V
    .locals 3

    .prologue
    .line 131
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apis:Lcom/vlingo/core/internal/util/SparseArrayMap;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/SparseArrayMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/social/api/SocialAPI;

    .line 132
    .local v0, "api":Lcom/vlingo/midas/social/api/SocialAPI;
    invoke-direct {p0, v0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->isNetworkEnabled(Lcom/vlingo/midas/social/api/SocialAPI;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->updateQueue:Ljava/util/Queue;

    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 136
    .end local v0    # "api":Lcom/vlingo/midas/social/api/SocialAPI;
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->updateQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apisUpdating:I

    .line 137
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->performAllUpdates()V

    .line 138
    return-void
.end method

.method private setNetworkEnabled(Lcom/vlingo/midas/social/api/SocialAPI;Z)V
    .locals 1
    .param p1, "api"    # Lcom/vlingo/midas/social/api/SocialAPI;
    .param p2, "enabled"    # Z

    .prologue
    .line 153
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/midas/social/api/SocialAPI;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/social/api/SocialAPI;->setEnabled(Z)V

    .line 158
    :goto_0
    return-void

    .line 156
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/social/api/SocialAPI;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected execute()V
    .locals 5

    .prologue
    .line 62
    :try_start_0
    invoke-static {p0}, Lcom/vlingo/midas/util/SocialUtils;->getAPIs(Lcom/vlingo/midas/social/api/SocialAPI$SocialCallback;)Lcom/vlingo/core/internal/util/SparseArrayMap;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apis:Lcom/vlingo/core/internal/util/SparseArrayMap;

    .line 65
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->type:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->status:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 66
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_no_network:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, "noServiceMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 128
    .end local v2    # "noServiceMsg":Ljava/lang/String;
    :goto_0
    return-void

    .line 72
    :cond_1
    sget-object v3, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction$1;->$SwitchMap$com$vlingo$midas$social$api$SocialNetworkType:[I

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->type:Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-virtual {v4}, Lcom/vlingo/midas/social/api/SocialNetworkType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 124
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->performUpdate()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 125
    :catch_0
    move-exception v1

    .line 126
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 74
    .end local v1    # "ex":Ljava/lang/Exception;
    :pswitch_0
    :try_start_1
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apis:Lcom/vlingo/core/internal/util/SparseArrayMap;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/social/api/SocialAPI;

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->setNetworkEnabled(Lcom/vlingo/midas/social/api/SocialAPI;Z)V

    goto :goto_1

    .line 81
    :pswitch_1
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->status:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x8c

    if-gt v3, v4, :cond_3

    .line 82
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apis:Lcom/vlingo/core/internal/util/SparseArrayMap;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/social/api/SocialAPI;

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->setNetworkEnabled(Lcom/vlingo/midas/social/api/SocialAPI;Z)V

    goto :goto_1

    .line 85
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_too_long:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "charLimitMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1

    .line 92
    .end local v0    # "charLimitMsg":Ljava/lang/String;
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 93
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 94
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->status:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x8c

    if-gt v3, v4, :cond_4

    .line 95
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apis:Lcom/vlingo/core/internal/util/SparseArrayMap;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/social/api/SocialAPI;

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->setNetworkEnabled(Lcom/vlingo/midas/social/api/SocialAPI;Z)V

    goto :goto_1

    .line 97
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_too_long:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v3, v4}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 99
    .restart local v0    # "charLimitMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1

    .line 105
    .end local v0    # "charLimitMsg":Ljava/lang/String;
    :pswitch_3
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 106
    const-string/jumbo v3, "weibo_account"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 107
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apis:Lcom/vlingo/core/internal/util/SparseArrayMap;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/social/api/SocialAPI;

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->setNetworkEnabled(Lcom/vlingo/midas/social/api/SocialAPI;Z)V

    goto/16 :goto_1

    .line 112
    :cond_5
    const-string/jumbo v3, "facebook_account"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 114
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apis:Lcom/vlingo/core/internal/util/SparseArrayMap;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/social/api/SocialAPI;

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->setNetworkEnabled(Lcom/vlingo/midas/social/api/SocialAPI;Z)V

    .line 117
    :cond_6
    const-string/jumbo v3, "twitter_account"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 118
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apis:Lcom/vlingo/core/internal/util/SparseArrayMap;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/social/api/SocialAPI;

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->setNetworkEnabled(Lcom/vlingo/midas/social/api/SocialAPI;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onFacebookAPILogin(Lcom/vlingo/midas/social/api/FacebookAPI;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "api"    # Lcom/vlingo/midas/social/api/FacebookAPI;
    .param p2, "responseType"    # I
    .param p3, "params"    # Landroid/os/Bundle;

    .prologue
    .line 245
    return-void
.end method

.method public onFacebookAPIMethod(Lcom/vlingo/midas/social/api/FacebookAPI;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "api"    # Lcom/vlingo/midas/social/api/FacebookAPI;
    .param p2, "responseType"    # I
    .param p3, "method"    # Ljava/lang/String;
    .param p4, "params"    # Landroid/os/Bundle;

    .prologue
    .line 166
    iget v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apisUpdating:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apisUpdating:I

    .line 167
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v2

    .line 171
    .local v2, "fbString":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 172
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    .line 180
    :goto_0
    const/16 v4, 0x385

    if-ne p2, v4, :cond_3

    .line 181
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_status_updated:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v3

    .line 183
    .local v3, "updatedString":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    .line 184
    iget v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apisUpdating:I

    if-nez v4, :cond_0

    .line 185
    iget-boolean v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->hasFailed:Z

    if-eqz v4, :cond_2

    .line 186
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 204
    .end local v3    # "updatedString":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 174
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    goto :goto_0

    .line 188
    .restart local v3    # "updatedString":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    goto :goto_1

    .line 192
    .end local v3    # "updatedString":Ljava/lang/String;
    :cond_3
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->hasFailed:Z

    .line 193
    const-string/jumbo v4, "error_id"

    invoke-virtual {p4, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 194
    .local v1, "errMsgId":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "error"

    invoke-virtual {p4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "errMsg":Ljava/lang/String;
    const/4 v4, 0x4

    if-ne v1, v4, :cond_4

    .line 196
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_network_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 199
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    .line 200
    iget v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apisUpdating:I

    if-nez v4, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onFollowVlingoComplete(ILjava/lang/String;)V
    .locals 0
    .param p1, "result"    # I
    .param p2, "errMessage"    # Ljava/lang/String;

    .prologue
    .line 256
    return-void
.end method

.method public onLoginComplete(IZLjava/lang/String;)V
    .locals 0
    .param p1, "result"    # I
    .param p2, "success"    # Z
    .param p3, "errMessage"    # Ljava/lang/String;

    .prologue
    .line 249
    return-void
.end method

.method public onUpdateComplete(ILjava/lang/String;)V
    .locals 4
    .param p1, "result"    # I
    .param p2, "errMessage"    # Ljava/lang/String;

    .prologue
    .line 208
    iget v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apisUpdating:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apisUpdating:I

    .line 209
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "twString":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 214
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ":  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    .line 222
    :goto_0
    if-nez p1, :cond_3

    .line 223
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_status_updated:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 225
    .local v1, "updatedString":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    .line 226
    iget v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apisUpdating:I

    if-nez v2, :cond_0

    .line 227
    iget-boolean v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->hasFailed:Z

    if-eqz v2, :cond_2

    .line 228
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 240
    .end local v1    # "updatedString":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 216
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ":  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    goto :goto_0

    .line 230
    .restart local v1    # "updatedString":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    goto :goto_1

    .line 234
    .end local v1    # "updatedString":Ljava/lang/String;
    :cond_3
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->hasFailed:Z

    .line 235
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    .line 236
    iget v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->apisUpdating:I

    if-nez v2, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->errorMsg:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onVerifyComplete(ILjava/lang/String;)V
    .locals 0
    .param p1, "result"    # I
    .param p2, "errMessage"    # Ljava/lang/String;

    .prologue
    .line 253
    return-void
.end method

.method public onVlingoFriendshipExists(IZLjava/lang/String;)V
    .locals 0
    .param p1, "result"    # I
    .param p2, "exists"    # Z
    .param p3, "errMessage"    # Ljava/lang/String;

    .prologue
    .line 260
    return-void
.end method

.method public onWeiboFail(I)V
    .locals 3
    .param p1, "typeParam"    # I

    .prologue
    .line 264
    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    if-nez p1, :cond_2

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_err_login:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 271
    :cond_1
    :goto_0
    return-void

    .line 267
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 268
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_update_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onWeiboSuccess(I)V
    .locals 1
    .param p1, "typeParam"    # I

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 276
    return-void
.end method

.method public status(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;
    .locals 0
    .param p1, "statusParam"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->status:Ljava/lang/String;

    .line 56
    return-object p0
.end method

.method public type(Lcom/vlingo/midas/social/api/SocialNetworkType;)Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;
    .locals 0
    .param p1, "socialNetworkType"    # Lcom/vlingo/midas/social/api/SocialNetworkType;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->type:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 51
    return-object p0
.end method
