.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowCreateTaskHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private final TITLE:Ljava/lang/String;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field private task:Lcom/vlingo/core/internal/schedule/ScheduleTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 32
    const-string/jumbo v0, "title"

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->TITLE:Ljava/lang/String;

    return-void
.end method

.method private createTask(Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 68
    const-string/jumbo v0, "execute"

    invoke-static {p1, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->CREATE_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/AddTaskInterface;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/AddTaskInterface;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->task:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/AddTaskInterface;->task(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/AddTaskInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/AddTaskInterface;->queue()V

    .line 70
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->task:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    if-eqz v0, :cond_0

    .line 71
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->task:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->task:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBeginDate()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 74
    :cond_0
    return-void
.end method

.method private showWidget(Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v1, 0x0

    .line 60
    const-string/jumbo v0, "confirm"

    invoke-static {p1, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeConfirmButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->task:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-interface {v0, v1, v2, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 65
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->task:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-interface {v0, v1, v2, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 88
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 89
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 90
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 82
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 83
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 84
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 50
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->extractTask(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/schedule/ScheduleTask;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->task:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 51
    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 53
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->showWidget(Lcom/vlingo/sdk/recognition/VLAction;)V

    .line 54
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->createTask(Lcom/vlingo/sdk/recognition/VLAction;)V

    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "unused"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;-><init>()V

    .line 109
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 122
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 111
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;-><init>()V

    .line 112
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 113
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.BodyChange"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 114
    const-string/jumbo v2, "title"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    const-string/jumbo v2, "title"

    invoke-static {p1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamString(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 116
    .local v1, "message":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;

    const-string/jumbo v2, "title"

    invoke-direct {v0, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 120
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    .end local v1    # "message":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method
