.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;
.super Ljava/lang/Object;
.source "MidasLocalSearchHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->showFailure()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 109
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    invoke-virtual {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getKeyTerms()Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "query":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->useGoogleSearch()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/util/WebSearchUtils;->isDefaultGoogleSearch()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    invoke-virtual {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCurrentSpokenSearch()Ljava/lang/String;

    move-result-object v0

    .line 114
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v4, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;
    invoke-static {v2, v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->access$600(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    const-string/jumbo v3, "com.google.android.googlequicksearchbox"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->enclosingPackage(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v2

    const-string/jumbo v3, "com.google.android.googlequicksearchbox.SearchActivity"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->activity(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "query,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->extra(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v2

    const-string/jumbo v3, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->action(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v2

    const-string/jumbo v3, "Google Search"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->app(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/actions/LaunchActivityAction;->queue()V

    .line 142
    :goto_0
    return-void

    .line 121
    :cond_1
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 122
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    invoke-virtual {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCurrentSearchQuery()Ljava/lang/String;

    move-result-object v0

    .line 124
    :cond_2
    move-object v1, v0

    .line 126
    .local v1, "searchQuery":Ljava/lang/String;
    new-instance v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2$1;

    invoke-direct {v2, p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
