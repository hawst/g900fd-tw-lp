.class public Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "PlaySongListAction.java"


# instance fields
.field private songList:[J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 4

    .prologue
    .line 26
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->songList:[J

    if-eqz v2, :cond_0

    .line 29
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->getPlayIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 31
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->songList:[J

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->getLaunchIntent([J)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 32
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "Activity could not be found."

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 37
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "noMatchMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getLaunchIntent([J)Landroid/content/Intent;
    .locals 3
    .param p1, "songList"    # [J

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 44
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.android.music/launchplayer"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    const-string/jumbo v1, "musicplayer.action"

    const-string/jumbo v2, "launchplayer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    const-string/jumbo v1, "list"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 47
    const-string/jumbo v1, "list_position"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 48
    return-object v0
.end method

.method public getPlayIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.music.musicservicecommand.play"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 53
    .local v0, "playIntent":Landroid/content/Intent;
    return-object v0
.end method

.method public songList([J)Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;
    .locals 0
    .param p1, "songList"    # [J

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->songList:[J

    .line 21
    return-object p0
.end method
