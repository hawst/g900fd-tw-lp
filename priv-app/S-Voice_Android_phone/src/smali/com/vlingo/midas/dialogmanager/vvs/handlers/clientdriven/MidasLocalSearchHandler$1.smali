.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;
.super Ljava/lang/Object;
.source "MidasLocalSearchHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->noData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 57
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    invoke-virtual {v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getKeyTerms()Ljava/lang/String;

    move-result-object v2

    .line 58
    .local v2, "search":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 59
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    invoke-virtual {v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCurrentSearchQuery()Ljava/lang/String;

    move-result-object v2

    .line 61
    :cond_0
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 62
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v4, v5, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 65
    :cond_1
    const-string/jumbo v4, "US"

    invoke-static {}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getInstance()Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/sdk/internal/deviceinfo/PhoneInfo;->getCarrierCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 66
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->qa_tts_NO_ANS_LOCAL_SEARCH_NON_US:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "prompt":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    invoke-static {v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 69
    invoke-static {v2, v8}, Lcom/vlingo/core/internal/util/WebSearchUtils;->getDefaultSearchString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 70
    .local v3, "searchUrl":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v6, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;
    invoke-static {v4, v5, v6}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 80
    .end local v3    # "searchUrl":Ljava/lang/String;
    :goto_0
    return-void

    .line 75
    .end local v1    # "prompt":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->qa_tts_NO_ANS_LOCAL_SEARCH:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 76
    .restart local v1    # "prompt":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    invoke-static {v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->access$300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 77
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_search_web_label_button:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v7, [Ljava/lang/Object;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->access$400(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "buttonText":Ljava/lang/String;
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->access$500(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowButton:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    invoke-interface {v4, v5, v8, v0, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0
.end method
