.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNotiChangeHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "SamsungNotiChangeHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 23
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 25
    const-string/jumbo v2, "name"

    invoke-static {p1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "name":Ljava/lang/String;
    const-string/jumbo v2, "state"

    invoke-static {p1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 28
    .local v1, "state":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v2

    const-string/jumbo v3, "settings-change"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 30
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->NOTIFICATION_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNotiChangeHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->state(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v2

    invoke-interface {v2, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->confirmOn(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v2

    invoke-interface {v2, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->confirmOff(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v2

    invoke-interface {v2, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->confirmOnTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v2

    invoke-interface {v2, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->confirmOffTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v2

    invoke-interface {v2, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->alreadySet(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v2

    invoke-interface {v2, p2}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->vvsListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;->queue()V

    .line 41
    const/4 v2, 0x0

    return v2
.end method
