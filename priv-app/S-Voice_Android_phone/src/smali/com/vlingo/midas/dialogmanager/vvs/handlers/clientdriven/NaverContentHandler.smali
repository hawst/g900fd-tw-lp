.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "NaverContentHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
.implements Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# static fields
.field private static final MSG_WAITING_FOR_COVER_OPENED:I


# instance fields
.field private adaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

.field handler:Landroid/os/Handler;

.field private log:Lcom/vlingo/core/internal/logging/Logger;

.field mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field final mHandler:Landroid/os/Handler;

.field private waitingForCoverOpened:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 35
    const-class v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    const-string/jumbo v1, "VLG_NaverContentHandler"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 36
    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->adaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->waitingForCoverOpened:Z

    .line 43
    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->handler:Landroid/os/Handler;

    .line 45
    new-instance v0, Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-direct {v0}, Lcom/vlingo/midas/naver/NaverAdaptor;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->adaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    .line 46
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->adaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/naver/NaverAdaptor;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->handler:Landroid/os/Handler;

    .line 205
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$4;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$4;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->waitingForCoverOpened:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->waitingForCoverOpened:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "x2"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->doExcuteNaverContent(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Lcom/vlingo/midas/naver/NaverAdaptor;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->adaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method private doExcuteNaverContent(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 99
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v7

    const-string/jumbo v8, "search"

    invoke-virtual {v7, v8}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 101
    const-string/jumbo v7, "Request"

    invoke-static {p1, v7, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 102
    .local v2, "request":Ljava/lang/String;
    const-string/jumbo v7, "Type"

    invoke-static {p1, v7, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 104
    .local v3, "type":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PARSE_TYPE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 105
    .local v1, "parseType":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/WebSearchUtils;->getEngineFromParseType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "engine":Ljava/lang/String;
    invoke-virtual {p0, v1, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->shouldForwardToUsualSearch(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 107
    invoke-static {v2, v0}, Lcom/vlingo/core/internal/util/WebSearchUtils;->getDefaultSearchString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "url":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v7, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    invoke-virtual {p0, v5, v7}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    const-string/jumbo v7, "android.intent.action.VIEW"

    invoke-virtual {v5, v7}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->argument(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/actions/ExecuteIntentAction;->queue()V

    .line 109
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->PARSE_TYPE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v8, 0x0

    invoke-interface {v5, v7, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    move v5, v6

    .line 115
    .end local v4    # "url":Ljava/lang/String;
    :goto_0
    return v5

    .line 112
    :cond_0
    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->adaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v6, v3}, Lcom/vlingo/midas/naver/NaverAdaptor;->setType(Ljava/lang/String;)V

    .line 113
    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->adaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v6, p2}, Lcom/vlingo/midas/naver/NaverAdaptor;->setVVSActionHandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 114
    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->adaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v6, v2, v5}, Lcom/vlingo/midas/naver/NaverAdaptor;->sendNaverRequest(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private showFailure()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$3;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$3;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 190
    return-void
.end method

.method private showSuccess()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$2;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 180
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 58
    new-instance v2, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 60
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    .line 61
    .local v0, "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    new-instance v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 79
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 81
    const-string/jumbo v2, "Always"

    const-string/jumbo v3, "maphandler ExcuteAction. CoverClosed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->sview_cover_alwaysmicon:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 86
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 87
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 88
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x2710

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 90
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->waitingForCoverOpened:Z

    .line 94
    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->doExcuteNaverContent(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v1

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 197
    return-void
.end method

.method public onRequestFailed()V
    .locals 2

    .prologue
    .line 159
    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->showFailure()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 163
    return-void

    .line 161
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v0
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 0
    .param p1, "prompt"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 203
    return-void
.end method

.method public onRequestScheduled()V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method public onResponseReceived()V
    .locals 3

    .prologue
    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->adaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getContent()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "content":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->showFailure()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 145
    return-void

    .line 140
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->showSuccess()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143
    .end local v0    # "content":Ljava/lang/String;
    :catchall_0
    move-exception v1

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v1
.end method

.method protected shouldForwardToUsualSearch(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "parseType"    # Ljava/lang/String;
    .param p2, "engine"    # Ljava/lang/String;

    .prologue
    .line 120
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const-string/jumbo v0, "wsearch:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "Naver"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
