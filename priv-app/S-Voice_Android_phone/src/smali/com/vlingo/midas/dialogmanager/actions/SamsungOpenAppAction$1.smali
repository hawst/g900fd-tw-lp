.class Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$1;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "SamsungOpenAppAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->execute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 3
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 29
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->waitingForCoverOpened:Z
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->access$000(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    const-string/jumbo v0, "always"

    const-string/jumbo v1, "mCoverStateListener cover Opened"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 32
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    iget-object v1, v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->doExecuteLaunchActivity()V
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->access$100(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;)V

    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->waitingForCoverOpened:Z
    invoke-static {v0, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->access$002(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;Z)Z

    .line 41
    :goto_0
    return-void

    .line 36
    :cond_0
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "Cover opened, waitingForCoverOpened is false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 39
    :cond_1
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "mCoverStateListener cover Closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
