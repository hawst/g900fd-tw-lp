.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;
.super Ljava/lang/Object;
.source "ChatbotWidgetHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->onResponseReceived()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)V
    .locals 0

    .prologue
    .line 555
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 558
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->newsManager:Lcom/vlingo/midas/news/NewsManager;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/midas/news/NewsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/news/NewsManager;->getCurrentNews()Lcom/vlingo/midas/news/NewsItem;

    move-result-object v0

    .line 559
    .local v0, "item":Lcom/vlingo/midas/news/NewsItem;
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    iget-object v2, v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->isPrevOrNextBeforeRead:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Next:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    if-ne v2, v3, :cond_0

    .line 560
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->newsManager:Lcom/vlingo/midas/news/NewsManager;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/midas/news/NewsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/news/NewsManager;->getNextNews()Lcom/vlingo/midas/news/NewsItem;

    move-result-object v0

    .line 563
    :cond_0
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/vlingo/midas/news/NewsItem;->getText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lcom/vlingo/midas/news/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 564
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 565
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 566
    .local v1, "rsc":Landroid/content/res/Resources;
    invoke-virtual {v0}, Lcom/vlingo/midas/news/NewsItem;->getNewsCP()I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 567
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    sget v3, Lcom/vlingo/midas/R$string;->chatbot_read_news:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    sget v5, Lcom/vlingo/midas/R$string;->newscp_flipboard:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$102(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;Ljava/lang/String;)Ljava/lang/String;

    .line 571
    :goto_0
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    iget-object v2, v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->isPrevOrNextBeforeRead:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Prev:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    if-ne v2, v3, :cond_5

    .line 572
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    sget v3, Lcom/vlingo/midas/R$string;->prompt_no_previously_read_news:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$102(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;Ljava/lang/String;)Ljava/lang/String;

    .line 578
    .end local v1    # "rsc":Landroid/content/res/Resources;
    :cond_1
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 579
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 582
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$400(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    invoke-interface {v2, v3, v4, v0, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 583
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 584
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$500(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/midas/news/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;)V

    .line 587
    :cond_3
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    invoke-virtual {v0}, Lcom/vlingo/midas/news/NewsItem;->getText()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->playOutNews(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$600(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;Ljava/lang/String;)V

    .line 589
    new-instance v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;)V

    const-wide/16 v3, 0x5dc

    invoke-static {v2, v3, v4}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 607
    :goto_2
    return-void

    .line 569
    .restart local v1    # "rsc":Landroid/content/res/Resources;
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    sget v3, Lcom/vlingo/midas/R$string;->chatbot_read_news:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    sget v5, Lcom/vlingo/midas/R$string;->newscp_yonhap:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$102(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 573
    :cond_5
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    iget-object v2, v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->isPrevOrNextBeforeRead:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Next:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    if-ne v2, v3, :cond_1

    .line 574
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    sget v3, Lcom/vlingo/midas/R$string;->chatbot_next_news:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$102(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 604
    .end local v1    # "rsc":Landroid/content/res/Resources;
    :cond_6
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$800(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$700(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$700(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->access$900(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    goto :goto_2
.end method
