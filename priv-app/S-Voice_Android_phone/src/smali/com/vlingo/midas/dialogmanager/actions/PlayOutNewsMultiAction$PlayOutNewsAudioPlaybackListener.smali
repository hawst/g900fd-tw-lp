.class Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;
.super Ljava/lang/Object;
.source "PlayOutNewsMultiAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayOutNewsAudioPlaybackListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;
    .param p2, "x1"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$1;

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;-><init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 4
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 174
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "request cancelled, reason="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "msg":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$300(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 178
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$300(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->cancel(Z)Z

    .line 180
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 182
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getRecoState()Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    move-result-object v1

    .line 183
    .local v1, "recoState":Lcom/vlingo/sdk/recognition/VLRecognitionStates;
    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->CONNECTING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    if-eq v1, v2, :cond_1

    .line 184
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$900(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 185
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getContext()Landroid/content/Context;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$1000(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 187
    :cond_1
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 145
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->requests:Ljava/util/LinkedList;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$100(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/audio/TTSRequest;

    .line 146
    .local v0, "newRequest":Lcom/vlingo/core/internal/audio/TTSRequest;
    if-eqz v0, :cond_0

    .line 147
    invoke-static {v0, p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->play(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 153
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 150
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$500(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 151
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$600(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    goto :goto_0
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 4
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 157
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "request ignored, reason="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 160
    .local v0, "msg":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$300(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 161
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$300(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->cancel(Z)Z

    .line 163
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 165
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getRecoState()Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    move-result-object v1

    .line 166
    .local v1, "recoState":Lcom/vlingo/sdk/recognition/VLRecognitionStates;
    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->CONNECTING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    if-eq v1, v2, :cond_1

    .line 167
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$700(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    .line 168
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getContext()Landroid/content/Context;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$800(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 170
    :cond_1
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 5
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/4 v4, 0x0

    .line 114
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getRecoState()Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    move-result-object v0

    .line 115
    .local v0, "currentRecoState":Lcom/vlingo/sdk/recognition/VLRecognitionStates;
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->CONNECTING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    if-ne v0, v1, :cond_1

    .line 118
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;->STOPPED:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    invoke-virtual {p0, p1, v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 121
    :cond_1
    new-instance v1, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener$1;-><init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;)V

    const-wide/16 v2, 0x5dc

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 133
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->isPlayingFirst:Z
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$200(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 134
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->isPlayingFirst:Z
    invoke-static {v1, v4}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$202(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;Z)Z

    .line 135
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    new-instance v2, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    invoke-direct {v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;-><init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)V

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$302(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;)Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    .line 136
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->prepareAllRequestsAsyncTask:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$300(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;

    move-result-object v2

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/Void;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Void;

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 137
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PlayOutNewsAudioPlaybackListener;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$400(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 139
    :cond_2
    return-void
.end method
