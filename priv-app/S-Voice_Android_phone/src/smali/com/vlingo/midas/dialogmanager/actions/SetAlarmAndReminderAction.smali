.class public Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SetAlarmAndReminderAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;


# static fields
.field private static final INTENT_ADD_ALARM_ACTION:Ljava/lang/String; = "com.sec.android.clockpackage.ADD_ALARM"


# instance fields
.field private alarm:Lcom/vlingo/core/internal/util/Alarm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method

.method public static addAlarm(Landroid/content/Context;Lcom/vlingo/core/internal/util/Alarm;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "alarm"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 66
    if-eqz p1, :cond_1

    .line 67
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v10, "com.sec.android.clockpackage.ADD_ALARM"

    invoke-direct {v3, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 68
    .local v3, "i":Landroid/content/Intent;
    const-string/jumbo v10, "android.intent.extra.alarm.MESSAGE"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string/jumbo v10, "android.intent.extra.alarm.HOUR"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getHour()I

    move-result v11

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 70
    const-string/jumbo v10, "android.intent.extra.alarm.MINUTES"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getMinute()I

    move-result v11

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 71
    const-string/jumbo v10, "alarm_repeat"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getDayMask()I

    move-result v11

    invoke-virtual {v3, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 73
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isNewAlarmConcept()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 74
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 75
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v10, 0xb

    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    mul-int/lit16 v10, v10, 0xe10

    const/16 v11, 0xc

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->get(I)I

    move-result v11

    mul-int/lit8 v11, v11, 0x3c

    add-int v5, v10, v11

    .line 76
    .local v5, "nowTime":I
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getHour()I

    move-result v10

    mul-int/lit16 v10, v10, 0xe10

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getMinute()I

    move-result v11

    mul-int/lit8 v11, v11, 0x3c

    add-int v7, v10, v11

    .line 77
    .local v7, "setTime":I
    const/4 v10, 0x7

    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    add-int/lit8 v4, v10, -0x1

    .line 78
    .local v4, "nowDay":I
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getDayMask()I

    move-result v10

    invoke-static {v10}, Lcom/vlingo/core/internal/util/Alarm;->getDaysCanonical(I)Ljava/lang/String;

    move-result-object v6

    .line 80
    .local v6, "setDay":Ljava/lang/String;
    invoke-static {v6, v4}, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->searchDay(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "day":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->isWeeklyRepeating()Z

    move-result v10

    if-nez v10, :cond_0

    .line 82
    const-string/jumbo v10, "today"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    if-le v5, v7, :cond_2

    .line 83
    invoke-virtual {p1, v8}, Lcom/vlingo/core/internal/util/Alarm;->setWeeklyRepeating(Z)V

    .line 92
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v1    # "day":Ljava/lang/String;
    .end local v4    # "nowDay":I
    .end local v5    # "nowTime":I
    .end local v6    # "setDay":Ljava/lang/String;
    .end local v7    # "setTime":I
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->isWeeklyRepeating()Z

    move-result v10

    if-eqz v10, :cond_4

    move v2, v8

    .line 93
    .local v2, "everyWeekRepeat":I
    :goto_1
    const-string/jumbo v10, "alarm_everyweekrepeat"

    invoke-virtual {v3, v10, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 94
    const-string/jumbo v10, "android.intent.extra.alarm.SKIP_UI"

    invoke-virtual {v3, v10, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    const-string/jumbo v10, "alarm_activate"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getAlarmStatus()Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    move-result-object v11

    sget-object v12, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v11, v12}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_5

    :goto_2
    invoke-virtual {v3, v10, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 96
    const/16 v8, 0x20

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 97
    invoke-virtual {p0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 100
    invoke-static {p0}, Lcom/sec/android/app/clockpackage/alarm/AlarmProvider;->enableNextAlert(Landroid/content/Context;)V

    .line 102
    const-string/jumbo v8, "com.vlingo.midas"

    const-string/jumbo v9, "ALAR"

    invoke-static {p0, v8, v9}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    .end local v2    # "everyWeekRepeat":I
    .end local v3    # "i":Landroid/content/Intent;
    :cond_1
    return-void

    .line 84
    .restart local v0    # "calendar":Ljava/util/Calendar;
    .restart local v1    # "day":Ljava/lang/String;
    .restart local v3    # "i":Landroid/content/Intent;
    .restart local v4    # "nowDay":I
    .restart local v5    # "nowTime":I
    .restart local v6    # "setDay":Ljava/lang/String;
    .restart local v7    # "setTime":I
    :cond_2
    const-string/jumbo v10, "tomorrow"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    if-ge v5, v7, :cond_3

    .line 85
    invoke-virtual {p1, v8}, Lcom/vlingo/core/internal/util/Alarm;->setWeeklyRepeating(Z)V

    goto :goto_0

    .line 87
    :cond_3
    invoke-virtual {p1, v9}, Lcom/vlingo/core/internal/util/Alarm;->setWeeklyRepeating(Z)V

    goto :goto_0

    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v1    # "day":Ljava/lang/String;
    .end local v4    # "nowDay":I
    .end local v5    # "nowTime":I
    .end local v6    # "setDay":Ljava/lang/String;
    .end local v7    # "setTime":I
    :cond_4
    move v2, v9

    .line 92
    goto :goto_1

    .restart local v2    # "everyWeekRepeat":I
    :cond_5
    move v8, v9

    .line 95
    goto :goto_2
.end method

.method public static searchDay(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p0, "day"    # Ljava/lang/String;
    .param p1, "nowDay"    # I

    .prologue
    .line 108
    const/4 v2, 0x7

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "sun"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "mon"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "tue"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "wed"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "thu"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string/jumbo v3, "fri"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "sat"

    aput-object v3, v1, v2

    .line 109
    .local v1, "week":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 110
    aget-object v2, v1, v0

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    if-ne v0, p1, :cond_0

    .line 112
    const-string/jumbo v2, "today"

    .line 118
    :goto_1
    return-object v2

    .line 113
    :cond_0
    add-int/lit8 v2, p1, 0x1

    rem-int/lit8 v2, v2, 0x7

    if-ne v0, v2, :cond_1

    .line 114
    const-string/jumbo v2, "tomorrow"

    goto :goto_1

    .line 109
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_2
    const-string/jumbo v2, ""

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;

    move-result-object v0

    return-object v0
.end method

.method public alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;
    .locals 0
    .param p1, "alarm"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    .line 41
    return-object p0
.end method

.method protected execute()V
    .locals 5

    .prologue
    .line 45
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    if-eqz v2, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->core_default_alarm_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "alarmName":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-virtual {v2, v0}, Lcom/vlingo/core/internal/util/Alarm;->setName(Ljava/lang/String;)V

    .line 50
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->addAlarm(Landroid/content/Context;Lcom/vlingo/core/internal/util/Alarm;)V

    .line 51
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .end local v0    # "alarmName":Ljava/lang/String;
    :goto_0
    return-void

    .line 52
    .restart local v0    # "alarmName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 55
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unable to schedule alarm: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 59
    .end local v0    # "alarmName":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "No alarm to set"

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method
