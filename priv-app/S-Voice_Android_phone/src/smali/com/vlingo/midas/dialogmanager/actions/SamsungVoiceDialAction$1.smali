.class Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;
.super Ljava/lang/Object;
.source "SamsungVoiceDialAction.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->execute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

.field final synthetic val$bluetoothOn:Z


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;Z)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    iput-boolean p2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->val$bluetoothOn:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 61
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->address:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->access$000(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 65
    const-string/jumbo v3, "tel"

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->address:Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->access$000(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 68
    .local v0, "callUri":Landroid/net/Uri;
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.CALL_PRIVILEGED"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 69
    .local v2, "i":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 71
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-gt v3, v4, :cond_0

    .line 72
    new-instance v3, Landroid/content/ComponentName;

    const-string/jumbo v4, "com.android.phone"

    const-string/jumbo v5, "com.android.phone.PrivilegedOutgoingCallBroadcaster"

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 76
    :cond_0
    const/high16 v3, 0x14000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 77
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    .end local v0    # "callUri":Landroid/net/Uri;
    :goto_0
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->access$300(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 87
    return-void

    .line 78
    .end local v2    # "i":Landroid/content/Intent;
    .restart local v0    # "callUri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 79
    .local v1, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->address:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->access$000(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->val$bluetoothOn:Z

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/util/DialUtil;->getDialIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 80
    .restart local v2    # "i":Landroid/content/Intent;
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->getContext()Landroid/content/Context;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->access$100(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 83
    .end local v0    # "callUri":Landroid/net/Uri;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "i":Landroid/content/Intent;
    :cond_1
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->address:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->access$000(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->val$bluetoothOn:Z

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/util/DialUtil;->getDialIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 84
    .restart local v2    # "i":Landroid/content/Intent;
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->getContext()Landroid/content/Context;
    invoke-static {v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->access$200(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
