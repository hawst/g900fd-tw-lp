.class final enum Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;
.super Ljava/lang/Enum;
.source "SamsungSettingChangeAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "NetworkType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

.field public static final enum BLUETOOTH:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

.field public static final enum WIFI:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    const-string/jumbo v1, "WIFI"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;->WIFI:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    const-string/jumbo v1, "BLUETOOTH"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;->BLUETOOTH:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    sget-object v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;->WIFI:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;->BLUETOOTH:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;->$VALUES:[Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;->$VALUES:[Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    invoke-virtual {v0}, [Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    return-object v0
.end method
