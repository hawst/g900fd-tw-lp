.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;
.super Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;
.source "ShowPlayGenericWidgetHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler$1;
    }
.end annotation


# static fields
.field private static final SEARCH_MUSIC_MAX:I


# instance fields
.field private playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->getMultiWidgetItemsUltimateMax()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sput v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->SEARCH_MUSIC_MAX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;-><init>()V

    .line 80
    return-void
.end method


# virtual methods
.method protected getDisambiguationList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    sget v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->SEARCH_MUSIC_MAX:I

    sget v3, Lcom/vlingo/midas/R$string;->playlists_quicklist_default_value:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, p2, p3, v2, v3}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getGenericList(Landroid/content/Context;Ljava/lang/String;ZILjava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 50
    .local v1, "musicDetailPair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;>;"
    if-eqz v1, :cond_1

    .line 51
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 52
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    .line 54
    :cond_0
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v2, :cond_1

    .line 55
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 60
    :cond_1
    return-object v0
.end method

.method protected getList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getMusicList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler$1;->$SwitchMap$com$vlingo$midas$samsungutils$utils$music$PlayMusicType:[I

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 84
    :goto_0
    return-object v0

    .line 68
    :pswitch_0
    sget v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->SEARCH_MUSIC_MAX:I

    sget v1, Lcom/vlingo/midas/R$string;->playlists_quicklist_default_value:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->byPlaylist(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 71
    :pswitch_1
    sget v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->SEARCH_MUSIC_MAX:I

    invoke-static {p1, p2, v0}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->byTitle(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 74
    :pswitch_2
    sget v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->SEARCH_MUSIC_MAX:I

    invoke-static {p1, p2, v0}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->byAlbum(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 77
    :pswitch_3
    sget v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->SEARCH_MUSIC_MAX:I

    invoke-static {p1, p2, v0}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->byArtist(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 84
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected getNoMatchPromptRes(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 90
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    if-nez v0, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->UNDEFINED:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    .line 186
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 187
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->playMusicType:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    .line 191
    :goto_0
    return-void

    .line 189
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected playFromDetails(Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;)V
    .locals 2
    .param p1, "md"    # Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ARTIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getArtist()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p1}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getArtist()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->playByName(Ljava/lang/String;)V

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->playByName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected promptForDisambiguation(I)V
    .locals 10
    .param p1, "size"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-result-object v1

    if-nez v1, :cond_0

    .line 166
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getRegularWidgetMax()I

    move-result v0

    .line 100
    .local v0, "maxWidget":I
    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler$1;->$SwitchMap$com$vlingo$midas$samsungutils$utils$music$PlayMusicType:[I

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 102
    :pswitch_0
    if-le p1, v0, :cond_1

    .line 103
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->items_were_found_for_disambiguation_large_list:I

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYLISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto :goto_0

    .line 109
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->items_were_found_for_disambiguation:I

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYLISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto/16 :goto_0

    .line 118
    :pswitch_1
    if-le p1, v0, :cond_2

    .line 119
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->items_were_found_for_disambiguation_large_list:I

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MUSIC_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYTITLECHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto/16 :goto_0

    .line 125
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->items_were_found_for_disambiguation:I

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MUSIC_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYTITLECHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto/16 :goto_0

    .line 134
    :pswitch_2
    if-le p1, v0, :cond_3

    .line 135
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->items_were_found_for_disambiguation_large_list:I

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ALBUM_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYALBUMCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto/16 :goto_0

    .line 141
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->items_were_found_for_disambiguation:I

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ALBUM_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYALBUMCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto/16 :goto_0

    .line 150
    :pswitch_3
    if-le p1, v0, :cond_4

    .line 151
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->items_were_found_for_disambiguation_large_list:I

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYARTISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto/16 :goto_0

    .line 157
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->items_were_found_for_disambiguation:I

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYARTISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto/16 :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
