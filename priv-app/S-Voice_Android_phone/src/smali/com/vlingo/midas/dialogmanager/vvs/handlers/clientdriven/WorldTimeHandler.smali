.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "WorldTimeHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# instance fields
.field private adaptor:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method protected buildDateFromString(Ljava/lang/String;F)Ljava/util/Date;
    .locals 13
    .param p1, "time"    # Ljava/lang/String;
    .param p2, "gmtOffset"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    .line 76
    .local v6, "defaultTimezone":Ljava/util/TimeZone;
    invoke-virtual {v6}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v11

    int-to-long v1, v11

    .line 77
    .local v1, "currentGmtOffset":J
    new-instance v11, Ljava/util/Date;

    invoke-direct {v11}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6, v11}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 78
    invoke-virtual {v6}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v11

    int-to-long v11, v11

    add-long/2addr v1, v11

    .line 81
    :cond_0
    const/high16 v11, 0x42700000    # 60.0f

    mul-float/2addr v11, p2

    const/high16 v12, 0x42700000    # 60.0f

    mul-float/2addr v11, v12

    const/high16 v12, 0x447a0000    # 1000.0f

    mul-float/2addr v11, v12

    float-to-long v9, v11

    .line 83
    .local v9, "timeOffset":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 84
    .local v3, "currentTimeInMillis":J
    add-long v11, v3, v9

    sub-long v7, v11, v1

    .line 86
    .local v7, "expectedTimeInMillis":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 88
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 93
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    .line 94
    .local v5, "dateObj":Ljava/util/Date;
    return-object v5
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x0

    .line 35
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 37
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v7

    const-string/jumbo v8, "worldtime"

    invoke-virtual {v7, v8}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 39
    const-string/jumbo v7, "Location"

    invoke-static {p1, v7, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 40
    .local v3, "location":Ljava/lang/String;
    const-string/jumbo v7, "Time"

    invoke-static {p1, v7, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 41
    .local v4, "time":Ljava/lang/String;
    const-string/jumbo v7, "TimeZone"

    invoke-static {p1, v7, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 42
    .local v5, "timeZone":Ljava/lang/String;
    const-string/jumbo v7, "Date"

    invoke-static {p1, v7, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "date":Ljava/lang/String;
    const-string/jumbo v7, "GmtOffset"

    const/4 v8, 0x0

    invoke-static {p1, v7, v8, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamFloat(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;FZ)F

    move-result v2

    .line 48
    .local v2, "gmtOffset":F
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 53
    :try_start_0
    invoke-virtual {p0, v4, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->buildDateFromString(Ljava/lang/String;F)Ljava/util/Date;

    move-result-object v1

    .line 54
    .local v1, "dateObj":Ljava/util/Date;
    invoke-virtual {p0, v3, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->showWidgetAndTTS(Ljava/lang/String;Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .end local v1    # "dateObj":Ljava/util/Date;
    :cond_0
    :goto_0
    return v6

    .line 59
    :cond_1
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 63
    new-instance v6, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;

    invoke-direct {v6}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;-><init>()V

    iput-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->adaptor:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;

    .line 64
    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->adaptor:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;

    invoke-virtual {v6, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 65
    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->adaptor:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;

    invoke-virtual {v6, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->sendWorldTimeRequest(Ljava/lang/String;)V

    .line 66
    const/4 v6, 0x1

    goto :goto_0

    .line 55
    :catch_0
    move-exception v7

    goto :goto_0
.end method

.method public onRequestFailed()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 0
    .param p1, "arg0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 135
    return-void
.end method

.method public onRequestScheduled()V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public onResponseReceived()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->adaptor:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;

    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->getResult()Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->showWidgetAndTTS(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;)V

    .line 144
    return-void
.end method

.method protected showWidgetAndTTS(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;)V
    .locals 3
    .param p1, "dateObj"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    .prologue
    .line 99
    :try_start_0
    iget-object v0, p1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;->errorMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    iget-object v1, p1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;->errorMessage:Ljava/lang/String;

    iget-object v2, p1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;->errorMessage:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->adaptor:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;

    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->getLocation()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;->time:Ljava/lang/String;

    iget v2, p1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;->offset:F

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->buildDateFromString(Ljava/lang/String;F)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->showWidgetAndTTS(Ljava/lang/String;Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected showWidgetAndTTS(Ljava/lang/String;Ljava/util/Date;)V
    .locals 12
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "dateObj"    # Ljava/util/Date;

    .prologue
    .line 111
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 112
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 113
    const/16 v7, 0xb

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_0

    sget v5, Lcom/vlingo/midas/R$string;->time_prefix_0_1:I

    .line 114
    .local v5, "timePrefixId":I
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 116
    .local v4, "timePrefix":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 117
    invoke-static {p2}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "hourMin":Ljava/lang/String;
    const/4 v7, 0x0

    const-string/jumbo v8, ":"

    invoke-virtual {v2, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "hour":Ljava/lang/String;
    const-string/jumbo v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 121
    .local v3, "min":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$string;->world_time_with_location:I

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v9, v10

    const/4 v10, 0x1

    aput-object v1, v9, v10

    const/4 v10, 0x2

    aput-object v3, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 125
    .end local v1    # "hour":Ljava/lang/String;
    .end local v2    # "hourMin":Ljava/lang/String;
    .end local v3    # "min":Ljava/lang/String;
    .local v6, "ttsText":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowClock:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-interface {v7, v8, v9, p2, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 127
    return-void

    .line 113
    .end local v4    # "timePrefix":Ljava/lang/String;
    .end local v5    # "timePrefixId":I
    .end local v6    # "ttsText":Ljava/lang/String;
    :cond_0
    sget v5, Lcom/vlingo/midas/R$string;->time_prefix_2_23:I

    goto :goto_0

    .line 123
    .restart local v4    # "timePrefix":Ljava/lang/String;
    .restart local v5    # "timePrefixId":I
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v7

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$string;->world_time_with_location:I

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v9, v10

    const/4 v10, 0x1

    invoke-static {p2}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    aput-object v4, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "ttsText":Ljava/lang/String;
    goto :goto_1
.end method
