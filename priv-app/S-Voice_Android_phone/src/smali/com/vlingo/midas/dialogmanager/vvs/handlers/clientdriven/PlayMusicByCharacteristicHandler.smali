.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "PlayMusicByCharacteristicHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler$1;
    }
.end annotation


# static fields
.field private static final CHARACTERISTICS_EXTRA_MOOD_CALM:Ljava/lang/String; = "calm"

.field private static final CHARACTERISTICS_EXTRA_MOOD_EXCITING:Ljava/lang/String; = "exciting"

.field private static final CHARACTERISTICS_EXTRA_MOOD_JOYFUL:Ljava/lang/String; = "joyful"

.field private static final CHARACTERISTICS_EXTRA_MOOD_PASSIONATE:Ljava/lang/String; = "passionate"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 74
    return-void
.end method

.method private getMoodStringForIntent(Lcom/samsung/music/IntentsAndExtras$MoodType;)Ljava/lang/String;
    .locals 2
    .param p1, "moodType"    # Lcom/samsung/music/IntentsAndExtras$MoodType;

    .prologue
    .line 105
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler$1;->$SwitchMap$com$samsung$music$IntentsAndExtras$MoodType:[I

    invoke-virtual {p1}, Lcom/samsung/music/IntentsAndExtras$MoodType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 112
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    .line 106
    :pswitch_0
    const-string/jumbo v0, "joyful"

    goto :goto_0

    .line 107
    :pswitch_1
    const-string/jumbo v0, "passionate"

    goto :goto_0

    .line 108
    :pswitch_2
    const-string/jumbo v0, "calm"

    goto :goto_0

    .line 109
    :pswitch_3
    const-string/jumbo v0, "exciting"

    goto :goto_0

    .line 110
    :pswitch_4
    const-string/jumbo v0, ""

    goto :goto_0

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method private getMoodTypeFromAction(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/samsung/music/IntentsAndExtras$MoodType;
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 95
    const-string/jumbo v2, "Characteristic"

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "characteristic":Ljava/lang/String;
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->UNDEFINED:Lcom/samsung/music/IntentsAndExtras$MoodType;

    .line 97
    .local v1, "moodType":Lcom/samsung/music/IntentsAndExtras$MoodType;
    const-string/jumbo v2, "joyful"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->JOYFUL:Lcom/samsung/music/IntentsAndExtras$MoodType;

    .line 98
    :cond_0
    const-string/jumbo v2, "calm"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->CALM:Lcom/samsung/music/IntentsAndExtras$MoodType;

    .line 99
    :cond_1
    const-string/jumbo v2, "passionate"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->PASSIONATE:Lcom/samsung/music/IntentsAndExtras$MoodType;

    .line 100
    :cond_2
    const-string/jumbo v2, "exciting"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->EXCITING:Lcom/samsung/music/IntentsAndExtras$MoodType;

    .line 101
    :cond_3
    return-object v1
.end method

.method private getMusicCountForMood(Lcom/samsung/music/IntentsAndExtras$MoodType;)I
    .locals 13
    .param p1, "moodType"    # Lcom/samsung/music/IntentsAndExtras$MoodType;

    .prologue
    const/4 v3, 0x0

    .line 116
    sget-object v4, Lcom/samsung/music/IntentsAndExtras$MoodType;->UNDEFINED:Lcom/samsung/music/IntentsAndExtras$MoodType;

    if-eq p1, v4, :cond_2

    .line 117
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 118
    .local v1, "resolver":Landroid/content/ContentResolver;
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;->getMoodStringForIntent(Lcom/samsung/music/IntentsAndExtras$MoodType;)Ljava/lang/String;

    move-result-object v11

    .line 120
    .local v11, "moodStr":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v4}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v10

    .line 121
    .local v10, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v12, 0x0

    .line 122
    .local v12, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v10, :cond_0

    .line 124
    :try_start_0
    invoke-virtual {v10}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v12, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 132
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 133
    .local v2, "musicSquareURL":Landroid/net/Uri;
    if-eqz v12, :cond_1

    .line 134
    invoke-interface {v12, v11}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicMoodUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 137
    :goto_1
    const-string/jumbo v4, "test"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "musicSquareURL = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    .line 138
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 140
    .local v7, "data":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    .line 141
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 142
    .local v9, "getCound":I
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 143
    const/4 v7, 0x0

    .line 148
    .end local v1    # "resolver":Landroid/content/ContentResolver;
    .end local v2    # "musicSquareURL":Landroid/net/Uri;
    .end local v7    # "data":Landroid/database/Cursor;
    .end local v9    # "getCound":I
    .end local v10    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v11    # "moodStr":Ljava/lang/String;
    .end local v12    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :goto_2
    return v9

    .line 125
    .restart local v1    # "resolver":Landroid/content/ContentResolver;
    .restart local v10    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v11    # "moodStr":Ljava/lang/String;
    .restart local v12    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :catch_0
    move-exception v8

    .line 126
    .local v8, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v8}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 127
    .end local v8    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v8

    .line 128
    .local v8, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v8}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 136
    .end local v8    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "musicSquareURL":Landroid/net/Uri;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "content://com.sec.android.music/music_square/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    .line 148
    .end local v1    # "resolver":Landroid/content/ContentResolver;
    .end local v2    # "musicSquareURL":Landroid/net/Uri;
    .end local v10    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v11    # "moodStr":Ljava/lang/String;
    .end local v12    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :cond_2
    const/4 v9, 0x0

    goto :goto_2
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 9
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 43
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 44
    const-string/jumbo v4, "Query"

    invoke-static {p1, v4, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 45
    .local v3, "query":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;->getMoodTypeFromAction(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/samsung/music/IntentsAndExtras$MoodType;

    move-result-object v0

    .line 47
    .local v0, "moodType":Lcom/samsung/music/IntentsAndExtras$MoodType;
    sget-object v4, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "[ActionQuery] Music Action : Query = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", Type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const-string/jumbo v1, ""

    .line 49
    .local v1, "playingString":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 50
    .local v2, "playingStringSpoken":Ljava/lang/String;
    sget-object v4, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler$1;->$SwitchMap$com$samsung$music$IntentsAndExtras$MoodType:[I

    invoke-virtual {v0}, Lcom/samsung/music/IntentsAndExtras$MoodType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 77
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 82
    :cond_0
    sget-object v4, Lcom/samsung/music/IntentsAndExtras$MoodType;->UNDEFINED:Lcom/samsung/music/IntentsAndExtras$MoodType;

    if-eq v0, v4, :cond_3

    .line 85
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 86
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v4, v5, v7, v7, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 88
    :cond_2
    invoke-direct {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;->getMoodStringForIntent(Lcom/samsung/music/IntentsAndExtras$MoodType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;->launchMusicSquare(Ljava/lang/String;)V

    .line 91
    :cond_3
    return v8

    .line 52
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->music_playing_calm:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 53
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->music_playing_calm_spoken:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 54
    goto :goto_0

    .line 57
    :pswitch_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->music_playing_exciting:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->music_playing_exciting_spoken:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 59
    goto :goto_0

    .line 62
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->music_playing_joyful_spoken:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->music_playing_joyful:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 64
    goto/16 :goto_0

    .line 67
    :pswitch_3
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->music_playing_passionate:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->music_playing_passionate_spoken:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 69
    goto/16 :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method launchMusicSquare(Ljava/lang/String;)V
    .locals 2
    .param p1, "moodTypeStr"    # Ljava/lang/String;

    .prologue
    .line 152
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicCharacteristic(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->MOOD:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->queue()V

    .line 156
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "music-play-list"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 157
    return-void
.end method
