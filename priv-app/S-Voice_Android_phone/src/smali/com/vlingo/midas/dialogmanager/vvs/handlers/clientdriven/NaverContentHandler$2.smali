.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$2;
.super Ljava/lang/Object;
.source "NaverContentHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->showSuccess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 169
    const/4 v0, 0x0

    .line 170
    .local v0, "msg":Ljava/lang/String;
    const/4 v1, 0x0

    .line 172
    .local v1, "spokenMsg":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 173
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 177
    :goto_0
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->access$500(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowNaverWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->adaptor:Lcom/vlingo/midas/naver/NaverAdaptor;
    invoke-static {v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->access$400(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Lcom/vlingo/midas/naver/NaverAdaptor;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 178
    return-void

    .line 175
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$2;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->access$300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
