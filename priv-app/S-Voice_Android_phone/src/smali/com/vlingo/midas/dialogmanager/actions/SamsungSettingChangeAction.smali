.class public Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SamsungSettingChangeAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;,
        Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver;,
        Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;
    }
.end annotation


# static fields
.field private static final WIFI_CANCEL_INTENT:Ljava/lang/String; = "android.net.wifi.WIFI_DIALOG_CANCEL_ACTION"

.field private static final WIFI_CONFIGURED_NETWORKS_CHANGED:Ljava/lang/String; = "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

.field private static regiBlueReceiverCount:I

.field private static regiReceiverCount:I


# instance fields
.field SamsungSettingCahageListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field private VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field private alreadySet:Ljava/lang/String;

.field private confirmOff:Ljava/lang/String;

.field private confirmOffTTS:Ljava/lang/String;

.field private confirmOn:Ljava/lang/String;

.field private confirmOnTTS:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private state:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    sput v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiReceiverCount:I

    .line 49
    sput v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiBlueReceiverCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    .line 490
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->SamsungSettingCahageListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    return-void
.end method

.method static synthetic access$1000(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$610()I
    .locals 2

    .prologue
    .line 31
    sget v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiBlueReceiverCount:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiBlueReceiverCount:I

    return v0
.end method

.method static synthetic access$710()I
    .locals 2

    .prologue
    .line 31
    sget v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiReceiverCount:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiReceiverCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    return-object v0
.end method

.method private fakeSystemListenerPrompt(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "tts"    # Ljava/lang/String;

    .prologue
    .line 485
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object p2, p1

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoText(Ljava/lang/String;)V

    .line 487
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->SamsungSettingCahageListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v0, p2, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 488
    return-void
.end method

.method private fakeSystemPrompt(II)V
    .locals 2
    .param p1, "text"    # I
    .param p2, "tts"    # I

    .prologue
    .line 372
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    return-void
.end method

.method private fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "tts"    # Ljava/lang/String;

    .prologue
    .line 376
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object p2, p1

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    return-void
.end method

.method private registerBluetoothChangeReceiver()V
    .locals 4

    .prologue
    .line 399
    sget v3, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiBlueReceiverCount:I

    if-lez v3, :cond_0

    .line 408
    :goto_0
    return-void

    .line 402
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 403
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;)V

    .line 404
    .local v0, "bluetoothChangereceiver":Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$BluetoothChangeReceiver;
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 405
    .local v2, "f":Landroid/content/IntentFilter;
    const-string/jumbo v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 406
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 407
    sget v3, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiBlueReceiverCount:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiBlueReceiverCount:I

    goto :goto_0
.end method

.method private registerNetworkChaneReceiver(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;)V
    .locals 4
    .param p1, "type"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    .prologue
    .line 381
    sget v3, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiReceiverCount:I

    if-lez v3, :cond_0

    .line 396
    :goto_0
    return-void

    .line 385
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 386
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;)V

    .line 387
    .local v2, "networkChangereceiver":Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkChangeReceiver;
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 389
    .local v1, "f":Landroid/content/IntentFilter;
    sget-object v3, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;->WIFI:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    if-ne p1, v3, :cond_1

    .line 390
    const-string/jumbo v3, "android.net.wifi.WIFI_DIALOG_CANCEL_ACTION"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 391
    const-string/jumbo v3, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 394
    :cond_1
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 395
    sget v3, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiReceiverCount:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->regiReceiverCount:I

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic alreadySet(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public alreadySet(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .locals 0
    .param p1, "alreadySetTTS"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    .line 82
    return-object p0
.end method

.method public bridge synthetic confirmOff(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOff(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public confirmOff(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .locals 0
    .param p1, "confirmOff"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOff:Ljava/lang/String;

    .line 67
    return-object p0
.end method

.method public bridge synthetic confirmOffTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOffTTS(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public confirmOffTTS(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .locals 0
    .param p1, "confirmOffTTS"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    .line 77
    return-object p0
.end method

.method public bridge synthetic confirmOn(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public confirmOn(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .locals 0
    .param p1, "confirmOn"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn:Ljava/lang/String;

    .line 62
    return-object p0
.end method

.method public bridge synthetic confirmOnTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOnTTS(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public confirmOnTTS(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .locals 0
    .param p1, "confirmOnTTS"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method protected execute()V
    .locals 24

    .prologue
    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    if-eqz v21, :cond_34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    if-eqz v21, :cond_34

    .line 98
    new-instance v19, Lcom/vlingo/core/internal/util/SystemServicesUtil;

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;-><init>(Landroid/content/Context;)V

    .line 100
    .local v19, "ssu":Lcom/vlingo/core/internal/util/SystemServicesUtil;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "wifi"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 102
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v21

    sget-object v22, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wifi_setting_change_on_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v18

    .line 103
    .local v18, "settingChangeErrorString":Ljava/lang/String;
    const/16 v20, 0x0

    .line 104
    .local v20, "success":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "on"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 105
    invoke-virtual/range {v19 .. v19}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isWifiEnabled()Z

    move-result v21

    if-eqz v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_0

    .line 106
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    .end local v18    # "settingChangeErrorString":Ljava/lang/String;
    .end local v20    # "success":Z
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    .line 368
    .end local v19    # "ssu":Lcom/vlingo/core/internal/util/SystemServicesUtil;
    :goto_1
    return-void

    .line 108
    .restart local v18    # "settingChangeErrorString":Ljava/lang/String;
    .restart local v19    # "ssu":Lcom/vlingo/core/internal/util/SystemServicesUtil;
    .restart local v20    # "success":Z
    :cond_0
    const/16 v21, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setWifiEnabled(Z)Z

    move-result v20

    .line 113
    if-eqz v20, :cond_1

    .line 114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v21

    if-nez v21, :cond_2

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v21

    if-eqz v21, :cond_3

    .line 117
    :cond_2
    sget-object v21, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;->WIFI:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->registerNetworkChaneReceiver(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;)V

    goto :goto_0

    .line 119
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "off"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 124
    invoke-virtual/range {v19 .. v19}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isWifiEnabled()Z

    move-result v21

    if-nez v21, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_5

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :cond_5
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setWifiEnabled(Z)Z

    move-result v20

    .line 128
    if-eqz v20, :cond_6

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOff:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 131
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 134
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "toggle"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 135
    invoke-virtual/range {v19 .. v19}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isWifiEnabled()Z

    move-result v17

    .line 136
    .local v17, "en":Z
    if-nez v17, :cond_8

    const/16 v21, 0x1

    :goto_2
    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setWifiEnabled(Z)Z

    move-result v20

    .line 137
    if-eqz v20, :cond_b

    .line 138
    if-nez v17, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v22, v21

    :goto_3
    if-nez v17, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    move-object/from16 v21, v0

    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 136
    :cond_8
    const/16 v21, 0x0

    goto :goto_2

    .line 138
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOff:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v22, v21

    goto :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    move-object/from16 v21, v0

    goto :goto_4

    .line 141
    :cond_b
    if-nez v17, :cond_d

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v21

    if-nez v21, :cond_c

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v21

    if-eqz v21, :cond_d

    .line 142
    :cond_c
    sget-object v21, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;->WIFI:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->registerNetworkChaneReceiver(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$NetworkType;)V

    goto/16 :goto_0

    .line 144
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 148
    .end local v17    # "en":Z
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported state"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 166
    .end local v18    # "settingChangeErrorString":Ljava/lang/String;
    .end local v20    # "success":Z
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "bt"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1e

    .line 167
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v21

    sget v22, Lcom/vlingo/midas/R$string;->core_bluetooth_setting_change_on_error:I

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 169
    .restart local v18    # "settingChangeErrorString":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "on"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_14

    .line 170
    invoke-virtual/range {v19 .. v19}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isBluetoothEnabled()Z

    move-result v21

    if-eqz v21, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_10

    .line 172
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 220
    :catch_0
    move-exception v16

    .line 221
    .local v16, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 174
    .end local v16    # "e":Ljava/lang/Exception;
    :cond_10
    const/16 v21, 0x1

    :try_start_1
    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setBluetoothEnabled(Z)Z

    move-result v20

    .line 175
    .restart local v20    # "success":Z
    if-eqz v20, :cond_12

    .line 176
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v21

    if-eqz v21, :cond_11

    .line 177
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->registerBluetoothChangeReceiver()V

    goto/16 :goto_0

    .line 180
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 184
    :cond_12
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v21

    if-eqz v21, :cond_13

    .line 185
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->registerBluetoothChangeReceiver()V

    goto/16 :goto_0

    .line 187
    :cond_13
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 192
    .end local v20    # "success":Z
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "off"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_17

    .line 193
    invoke-virtual/range {v19 .. v19}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isBluetoothEnabled()Z

    move-result v21

    if-nez v21, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_15

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 197
    :cond_15
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setBluetoothEnabled(Z)Z

    move-result v20

    .line 198
    .restart local v20    # "success":Z
    if-eqz v20, :cond_16

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOff:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    :cond_16
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 204
    .end local v20    # "success":Z
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "toggle"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1d

    .line 205
    invoke-virtual/range {v19 .. v19}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->isBluetoothEnabled()Z

    move-result v17

    .line 206
    .restart local v17    # "en":Z
    if-nez v17, :cond_18

    const/16 v21, 0x1

    :goto_5
    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/util/SystemServicesUtil;->setBluetoothEnabled(Z)Z

    move-result v20

    .line 207
    .restart local v20    # "success":Z
    if-eqz v20, :cond_1b

    .line 208
    if-nez v17, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v22, v21

    :goto_6
    if-nez v17, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    move-object/from16 v21, v0

    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 206
    .end local v20    # "success":Z
    :cond_18
    const/16 v21, 0x0

    goto :goto_5

    .line 208
    .restart local v20    # "success":Z
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOff:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v22, v21

    goto :goto_6

    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    move-object/from16 v21, v0

    goto :goto_7

    .line 211
    :cond_1b
    if-nez v17, :cond_1c

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v21

    if-eqz v21, :cond_1c

    .line 212
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->registerBluetoothChangeReceiver()V

    goto/16 :goto_0

    .line 214
    :cond_1c
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 218
    .end local v17    # "en":Z
    .end local v20    # "success":Z
    :cond_1d
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported state"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 225
    .end local v18    # "settingChangeErrorString":Ljava/lang/String;
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "safereader"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_29

    .line 227
    sget v12, Lcom/vlingo/midas/R$string;->driving_mode_on_root_display:I

    .line 228
    .local v12, "driving_mode_on_root_display_id":I
    sget v13, Lcom/vlingo/midas/R$string;->driving_mode_on_root_tts:I

    .line 229
    .local v13, "driving_mode_on_root_tts_id":I
    sget v8, Lcom/vlingo/midas/R$string;->driving_mode_off_root_display:I

    .line 230
    .local v8, "driving_mode_off_root_display_id":I
    sget v9, Lcom/vlingo/midas/R$string;->driving_mode_off_root_tts:I

    .line 231
    .local v9, "driving_mode_off_root_tts_id":I
    sget v14, Lcom/vlingo/midas/R$string;->driving_mode_on_suffix_display:I

    .line 232
    .local v14, "driving_mode_on_suffix_display_id":I
    sget v15, Lcom/vlingo/midas/R$string;->driving_mode_on_suffix_tts:I

    .line 233
    .local v15, "driving_mode_on_suffix_tts_id":I
    sget v10, Lcom/vlingo/midas/R$string;->driving_mode_off_suffix_display:I

    .line 234
    .local v10, "driving_mode_off_suffix_display_id":I
    sget v11, Lcom/vlingo/midas/R$string;->driving_mode_off_suffix_tts:I

    .line 235
    .local v11, "driving_mode_off_suffix_tts_id":I
    sget v6, Lcom/vlingo/midas/R$string;->driving_mode_already_enabled:I

    .line 236
    .local v6, "driving_mode_already_enabled_id":I
    sget v7, Lcom/vlingo/midas/R$string;->driving_mode_already_enabled_tts:I

    .line 237
    .local v7, "driving_mode_already_enabled_tts_id":I
    sget v4, Lcom/vlingo/midas/R$string;->driving_mode_already_disabled:I

    .line 238
    .local v4, "driving_mode_already_disabled_id":I
    sget v5, Lcom/vlingo/midas/R$string;->driving_mode_already_disabled_tts:I

    .line 240
    .local v5, "driving_mode_already_disabled_tts_id":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "on"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_21

    .line 243
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v21

    if-eqz v21, :cond_1f

    .line 244
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(II)V

    goto/16 :goto_0

    .line 246
    :cond_1f
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->enablePhoneDrivingMode(Landroid/content/Context;)V

    .line 247
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v21

    if-eqz v21, :cond_20

    .line 248
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string/jumbo v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 252
    :cond_20
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(II)V

    goto/16 :goto_0

    .line 255
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "off"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_24

    .line 259
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v21

    if-nez v21, :cond_22

    .line 260
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(II)V

    goto/16 :goto_0

    .line 262
    :cond_22
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->disablePhoneDrivingMode(Landroid/content/Context;)V

    .line 263
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v21

    if-eqz v21, :cond_23

    .line 264
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string/jumbo v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 268
    :cond_23
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(II)V

    goto/16 :goto_0

    .line 272
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "toggle"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_28

    .line 276
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v3

    .line 277
    .local v3, "carModeEnabled":Z
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->togglePhoneDrivingMode(Landroid/content/Context;)V

    .line 278
    if-eqz v3, :cond_26

    .line 279
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v21

    if-eqz v21, :cond_25

    .line 280
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string/jumbo v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 284
    :cond_25
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(II)V

    goto/16 :goto_0

    .line 287
    :cond_26
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v21

    if-eqz v21, :cond_27

    .line 288
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string/jumbo v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 292
    :cond_27
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(II)V

    goto/16 :goto_0

    .line 298
    .end local v3    # "carModeEnabled":Z
    :cond_28
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported state"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 300
    .end local v4    # "driving_mode_already_disabled_id":I
    .end local v5    # "driving_mode_already_disabled_tts_id":I
    .end local v6    # "driving_mode_already_enabled_id":I
    .end local v7    # "driving_mode_already_enabled_tts_id":I
    .end local v8    # "driving_mode_off_root_display_id":I
    .end local v9    # "driving_mode_off_root_tts_id":I
    .end local v10    # "driving_mode_off_suffix_display_id":I
    .end local v11    # "driving_mode_off_suffix_tts_id":I
    .end local v12    # "driving_mode_on_root_display_id":I
    .end local v13    # "driving_mode_on_root_tts_id":I
    .end local v14    # "driving_mode_on_suffix_display_id":I
    .end local v15    # "driving_mode_on_suffix_tts_id":I
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "emergencymode"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2e

    .line 302
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "on"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2b

    .line 303
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/samsungutils/utils/EmergencyModeUtils;->isEmergencyModeOn(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_2a

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_6

    goto/16 :goto_0

    .line 320
    :catch_1
    move-exception v16

    .line 321
    .local v16, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 322
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported device"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 306
    .end local v16    # "e":Ljava/lang/ClassNotFoundException;
    :cond_2a
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemListenerPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_6

    goto/16 :goto_0

    .line 323
    :catch_2
    move-exception v16

    .line 324
    .local v16, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    .line 325
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported device"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 308
    .end local v16    # "e":Ljava/lang/NoSuchMethodException;
    :cond_2b
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "off"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2d

    .line 309
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/samsungutils/utils/EmergencyModeUtils;->isEmergencyModeOn(Landroid/content/Context;)Z

    move-result v21

    if-nez v21, :cond_2c

    .line 310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_4 .. :try_end_4} :catch_6

    goto/16 :goto_0

    .line 326
    :catch_3
    move-exception v16

    .line 327
    .local v16, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 328
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported device"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 312
    .end local v16    # "e":Ljava/lang/IllegalAccessException;
    :cond_2c
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v21

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Lcom/vlingo/midas/samsungutils/utils/EmergencyModeUtils;->toggleEmergencyMode(Landroid/content/Context;Z)V

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOff:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOff:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_5 .. :try_end_5} :catch_6

    goto/16 :goto_0

    .line 329
    :catch_4
    move-exception v16

    .line 330
    .local v16, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 331
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported device"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 318
    .end local v16    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2d
    :try_start_6
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported state"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    .line 332
    :catch_5
    move-exception v16

    .line 333
    .local v16, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    .line 334
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported device"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 335
    .end local v16    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_6
    move-exception v16

    .line 336
    .local v16, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    .line 337
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported device"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 339
    .end local v16    # "e":Ljava/lang/NoSuchFieldException;
    :cond_2e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "talkback"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_33

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "on"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_30

    .line 343
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/samsungutils/utils/TalkbackUtils;->isTalkbackEnabled(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_2f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_2f

    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 347
    :cond_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOn:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOnTTS:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 349
    :cond_30
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "off"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_32

    .line 352
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/samsungutils/utils/TalkbackUtils;->isTalkbackEnabled(Landroid/content/Context;)Z

    move-result v21

    if-nez v21, :cond_31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_31

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->alreadySet:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 356
    :cond_31
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOff:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->confirmOffTTS:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 359
    :cond_32
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported state"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 362
    :cond_33
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported device"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 366
    .end local v19    # "ssu":Lcom/vlingo/core/internal/util/SystemServicesUtil;
    :cond_34
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v21

    const-string/jumbo v22, "unsupported device"

    invoke-interface/range {v21 .. v22}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public bridge synthetic name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->name(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->name:Ljava/lang/String;

    .line 52
    return-object p0
.end method

.method public bridge synthetic state(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public state(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .locals 0
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;

    .line 57
    return-object p0
.end method

.method public bridge synthetic vvsListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->vvsListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    move-result-object v0

    return-object v0
.end method

.method public vvsListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 87
    return-object p0
.end method
