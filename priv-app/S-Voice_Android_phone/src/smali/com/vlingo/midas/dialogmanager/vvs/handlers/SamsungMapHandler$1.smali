.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "SamsungMapHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

.field final synthetic val$action:Lcom/vlingo/sdk/recognition/VLAction;

.field final synthetic val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field final synthetic val$promptForCover:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->val$promptForCover:Ljava/lang/String;

    iput-object p3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    iput-object p4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->val$action:Lcom/vlingo/sdk/recognition/VLAction;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 4
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    const/4 v3, 0x0

    .line 119
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 120
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->waitingForCoverOpened:Z
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    const-string/jumbo v0, "always"

    const-string/jumbo v1, "mCoverStateListener cover Opened"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 123
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

    iget-object v1, v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 124
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->val$promptForCover:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->val$promptForCover:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->val$promptForCover:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->val$action:Lcom/vlingo/sdk/recognition/VLAction;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->doExecuteNavigation(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 128
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->waitingForCoverOpened:Z
    invoke-static {v0, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;->access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;Z)Z

    .line 135
    :goto_0
    return-void

    .line 130
    :cond_1
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "Cover opened, waitingForCoverOpened is false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 133
    :cond_2
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "mCoverStateListener cover Closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
