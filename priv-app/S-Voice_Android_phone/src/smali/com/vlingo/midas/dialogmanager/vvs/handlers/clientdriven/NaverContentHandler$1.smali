.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "NaverContentHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

.field final synthetic val$action:Lcom/vlingo/sdk/recognition/VLAction;

.field final synthetic val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->val$action:Lcom/vlingo/sdk/recognition/VLAction;

    iput-object p3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 4
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    const/4 v3, 0x0

    .line 63
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->waitingForCoverOpened:Z
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string/jumbo v0, "always"

    const-string/jumbo v1, "mCoverStateListener cover Opened"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 67
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    iget-object v1, v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 68
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->val$action:Lcom/vlingo/sdk/recognition/VLAction;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->doExcuteNaverContent(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 69
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->waitingForCoverOpened:Z
    invoke-static {v0, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;->access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;Z)Z

    .line 76
    :goto_0
    return-void

    .line 71
    :cond_0
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "Cover opened, waitingForCoverOpened is false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 74
    :cond_1
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "mCoverStateListener cover Closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
