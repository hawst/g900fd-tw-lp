.class public abstract Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowPlayMusicWidgetHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler$2;
    }
.end annotation


# static fields
.field private static final RESPONSES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/ResourceIdProvider$string;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field private type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 68
    const-class v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->TAG:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->RESPONSES:Ljava/util/Map;

    .line 75
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->RESPONSES:Ljava/util/Map;

    const-string/jumbo v1, "Music:Play"

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_play_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 70
    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 116
    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;
    .param p1, "x1"    # Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/lang/String;)V

    return-void
.end method

.method private getNoMatchAnyMusic()Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .locals 1

    .prologue
    .line 420
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ANYMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    return-object v0
.end method

.method private isAnyMusic()Z
    .locals 1

    .prologue
    .line 382
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->isAnyMusic(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private launchMusicPlayer(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 424
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->queue()V

    .line 428
    return-void
.end method

.method private launchMusicPlayerWithType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 431
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->queue()V

    .line 435
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "music-play-list"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 436
    return-void
.end method

.method private playByMood(Lcom/samsung/music/IntentsAndExtras$MoodType;)V
    .locals 4
    .param p1, "moodType"    # Lcom/samsung/music/IntentsAndExtras$MoodType;

    .prologue
    const/4 v3, 0x0

    .line 343
    const-string/jumbo v0, ""

    .line 344
    .local v0, "playingString":Ljava/lang/String;
    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler$2;->$SwitchMap$com$samsung$music$IntentsAndExtras$MoodType:[I

    invoke-virtual {p1}, Lcom/samsung/music/IntentsAndExtras$MoodType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 367
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 370
    sget-object v1, Lcom/samsung/music/IntentsAndExtras$MoodType;->UNDEFINED:Lcom/samsung/music/IntentsAndExtras$MoodType;

    if-eq p1, v1, :cond_2

    .line 373
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 374
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v1, v2, v3, v3, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 376
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchMusicSquare(Ljava/lang/String;)V

    .line 379
    :cond_2
    return-void

    .line 346
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->music_playing_calm:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 347
    goto :goto_0

    .line 350
    :pswitch_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->music_playing_exciting:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 351
    goto :goto_0

    .line 354
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->music_playing_joyful:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 355
    goto :goto_0

    .line 358
    :pswitch_3
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->music_playing_passionate:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 359
    goto :goto_0

    .line 344
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private playMusic(Ljava/util/List;Ljava/lang/String;)V
    .locals 7
    .param p2, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 387
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const/4 v2, 0x0

    .line 388
    .local v2, "listSize":I
    if-eqz p1, :cond_0

    .line 389
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 391
    :cond_0
    new-array v3, v2, [J

    .line 392
    .local v3, "mSongList":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 393
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    invoke-virtual {v5}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getSongId()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    aput-wide v5, v3, v1

    .line 392
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 396
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 397
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-virtual {p0, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getPlayString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 400
    :cond_2
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "com.sec.android.app.music.intent.action.PLAY_VIA"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 402
    .local v4, "musicIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getIsFavorite()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 403
    const/4 v5, 0x1

    invoke-virtual {p0, v5, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchQuickList(ZLjava/lang/String;)V

    .line 410
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v5

    const-string/jumbo v6, "music-play-list"

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 412
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p2, v3}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->resolved(Ljava/lang/String;Ljava/lang/String;[J)Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;

    move-result-object v0

    .line 413
    .local v0, "acceptedText":Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;
    if-eqz v0, :cond_3

    .line 414
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 416
    :cond_3
    return-void

    .line 405
    .end local v0    # "acceptedText":Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/vlingo/sdk/internal/util/PackageUtil;->canHandleIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 406
    invoke-direct {p0, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchMusicPlayer(Ljava/lang/String;)V

    goto :goto_1

    .line 408
    :cond_5
    invoke-virtual {p0, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchList([J)V

    goto :goto_1
.end method


# virtual methods
.method public actionSuccess()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 278
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v1, "Music:Pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v1, "Music:Cancel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 281
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v0, v1, v2, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 283
    :cond_3
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionSuccess()V

    .line 284
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 17
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 124
    invoke-super/range {p0 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 126
    const-string/jumbo v14, "Type"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    .line 127
    const-string/jumbo v14, "Query"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    .line 129
    .local v9, "name":Ljava/lang/String;
    sget-object v14, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "[ActionQuery] Music Action : Query = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, ", Type = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    if-eqz v14, :cond_0

    .line 132
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    invoke-static {v14, v15, v9}, Lcom/vlingo/core/internal/recognition/acceptedtext/PlayMusicAcceptedText;->requested(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->isAnyMusic()Z

    move-result v14

    if-nez v14, :cond_1

    .line 136
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v14

    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getNoMatchAnyMusic()Lcom/vlingo/core/internal/ResourceIdProvider$string;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 273
    :goto_0
    const/4 v14, 0x0

    :goto_1
    return v14

    .line 139
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    if-eqz v14, :cond_c

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    sget-object v15, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->UNDEFINED:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-result-object v16

    invoke-interface/range {v14 .. v16}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 141
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v15, "Music:Play"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v15, "Music:Resume"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v15, "Music:Restart"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 144
    :cond_3
    sget-object v14, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->RESPONSES:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    invoke-interface {v14, v15}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_4

    .line 145
    sget-object v14, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAY:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    const-string/jumbo v15, " "

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/lang/String;)V

    .line 182
    :goto_2
    const/4 v14, 0x0

    goto :goto_1

    .line 148
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 149
    .local v2, "context":Landroid/content/Context;
    invoke-static {v2}, Lcom/vlingo/midas/samsungutils/utils/TalkbackUtils;->isTalkbackEnabled(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_5

    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v14

    if-nez v14, :cond_6

    .line 150
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v15

    sget-object v14, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->RESPONSES:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {v15, v14}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 151
    sget-object v14, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAY:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    const-string/jumbo v15, " "

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/lang/String;)V

    goto :goto_2

    .line 153
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 154
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v15

    sget-object v14, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->RESPONSES:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    new-instance v16, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler$1;

    invoke-direct/range {v16 .. v17}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;)V

    move-object/from16 v0, v16

    invoke-interface {v15, v14, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    goto :goto_2

    .line 178
    :cond_7
    sget-object v14, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PLAY:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    const-string/jumbo v15, " "

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/lang/String;)V

    goto :goto_2

    .line 183
    .end local v2    # "context":Landroid/content/Context;
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v15, "Music:Pause"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 184
    sget-object v14, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PAUSE:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    const-string/jumbo v15, " "

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/lang/String;)V

    .line 185
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 186
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v15, "Music:Prev"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 187
    sget-object v14, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PREVIOUS:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    const-string/jumbo v15, " "

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/lang/String;)V

    .line 188
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 189
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v15, "Music:Next"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 190
    sget-object v14, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->NEXT:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    const-string/jumbo v15, " "

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/lang/String;)V

    .line 191
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 192
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->type:Ljava/lang/String;

    const-string/jumbo v15, "Music:Cancel"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 193
    sget-object v14, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->PAUSE:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    const-string/jumbo v15, " "

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->launchMusicPlayerWithType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;Ljava/lang/String;)V

    .line 194
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v14

    check-cast v14, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v14}, Lcom/vlingo/midas/gui/ConversationActivity;->hideMusic()V

    .line 195
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 199
    :cond_c
    const-string/jumbo v14, "Which"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v10

    .line 200
    .local v10, "ordinal":Ljava/lang/String;
    if-eqz v9, :cond_d

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    const/4 v15, 0x1

    if-ge v14, v15, :cond_e

    .line 201
    :cond_d
    const-string/jumbo v9, " "

    .line 204
    :cond_e
    const/4 v3, 0x0

    .line 205
    .local v3, "decorator":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    const-string/jumbo v14, "ListPositionChange"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 206
    .local v7, "listPositionChange":Ljava/lang/String;
    if-eqz v7, :cond_10

    .line 207
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeReplaceable()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v3

    .line 208
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v6

    .line 209
    .local v6, "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    const-string/jumbo v14, "next"

    invoke-virtual {v14, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 211
    :try_start_0
    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ListControlData;->incCurrentPage()V

    .line 212
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->which_one_for_disambiguation:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    :cond_f
    :goto_3
    const-string/jumbo v14, "prev"

    invoke-virtual {v14, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_10

    .line 219
    :try_start_1
    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ListControlData;->decCurrentPage()V

    .line 220
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->which_one_for_disambiguation:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_1 .. :try_end_1} :catch_1

    .line 227
    .end local v6    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    :cond_10
    :goto_4
    const/4 v8, 0x0

    .line 229
    .local v8, "musicDetailsSelection":Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;
    sget-object v14, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    .line 230
    .local v11, "ordinalData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    if-eqz v11, :cond_11

    invoke-static {v10}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_11

    .line 231
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v14

    invoke-virtual {v14, v11}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    .line 232
    .local v12, "pageInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    invoke-static {v12, v10}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "musicDetailsSelection":Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;
    check-cast v8, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    .line 235
    .end local v12    # "pageInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v8    # "musicDetailsSelection":Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;
    :cond_11
    invoke-static {v10}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_12

    if-nez v8, :cond_19

    .line 239
    :cond_12
    const/4 v5, 0x0

    .line 241
    .local v5, "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    const/4 v15, 0x1

    if-lt v14, v15, :cond_16

    .line 242
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-interface {v14}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v14

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v9, v15}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getDisambiguationList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v5

    .line 243
    if-eqz v5, :cond_13

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-ge v14, v15, :cond_14

    .line 244
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-interface {v14}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v9, v15}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getDisambiguationList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v5

    .line 246
    :cond_14
    if-eqz v5, :cond_15

    .line 247
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v14

    invoke-static {v14}, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->getDisplayCount(I)I

    move-result v14

    move-object/from16 v0, p2

    invoke-static {v0, v5, v14}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;I)V

    .line 254
    :cond_15
    :goto_5
    if-eqz v5, :cond_17

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-le v14, v15, :cond_17

    .line 255
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->promptForDisambiguation(I)V

    .line 256
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v14

    invoke-virtual {v14, v5}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    .line 257
    .restart local v12    # "pageInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    sget-object v15, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->PlayMusic:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    move-object/from16 v0, p0

    invoke-interface {v14, v15, v3, v12, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 258
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v14

    const-string/jumbo v15, "music-play-disambiguate"

    invoke-virtual {v14, v15}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 213
    .end local v5    # "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .end local v8    # "musicDetailsSelection":Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;
    .end local v11    # "ordinalData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .end local v12    # "pageInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v6    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    :catch_0
    move-exception v4

    .line 214
    .local v4, "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->that_is_all_for_disambiguation:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 221
    .end local v4    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    :catch_1
    move-exception v4

    .line 222
    .restart local v4    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->that_is_all_for_disambiguation:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 251
    .end local v4    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    .end local v6    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    .restart local v5    # "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    .restart local v8    # "musicDetailsSelection":Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;
    .restart local v11    # "ordinalData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_16
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    sget-object v15, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/List;

    .line 252
    .local v13, "temp":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    move-object v5, v13

    goto :goto_5

    .line 260
    .end local v13    # "temp":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_17
    if-eqz v5, :cond_18

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_18

    .line 261
    const/4 v14, 0x0

    invoke-interface {v5, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    invoke-virtual {v14}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getTitle()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->playByName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 263
    :cond_18
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 264
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->playByName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 268
    .end local v5    # "info":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_19
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->playFromDetails(Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;)V

    .line 269
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    goto/16 :goto_0
.end method

.method protected abstract getDisambiguationList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getMusicList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getNoMatchPromptRes(Ljava/lang/String;)Ljava/lang/String;
.end method

.method protected abstract getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;
.end method

.method protected getPlayString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 112
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_playing_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    move-object v0, p2

    .line 86
    check-cast v0, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    .line 88
    .local v0, "md":Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;
    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->playFromDetails(Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;)V

    .line 94
    :cond_0
    return-void
.end method

.method protected launchList([J)V
    .locals 2
    .param p1, "songList"    # [J

    .prologue
    .line 439
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_SONGLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->songList([J)Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;->queue()V

    .line 442
    return-void
.end method

.method launchMusicSquare(Ljava/lang/String;)V
    .locals 2
    .param p1, "moodTypeStr"    # Ljava/lang/String;

    .prologue
    .line 453
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicCharacteristic(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->MOOD:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->queue()V

    .line 457
    return-void
.end method

.method protected launchQuickList(ZLjava/lang/String;)V
    .locals 2
    .param p1, "quickList"    # Z
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 445
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->playMusicType(Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->name(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->quickList(Z)Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;->queue()V

    .line 450
    return-void
.end method

.method protected playByName(Ljava/lang/String;)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 287
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0, v4, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getMusicList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 289
    .local v0, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    const/4 v1, 0x0

    .line 290
    .local v1, "infoListSize":I
    if-eqz v0, :cond_0

    .line 291
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 293
    :cond_0
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v0, :cond_4

    .line 298
    if-lez v1, :cond_2

    .line 299
    if-ne v1, v5, :cond_1

    .line 300
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    invoke-virtual {v4}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->playMusic(Ljava/util/List;Ljava/lang/String;)V

    .line 339
    :goto_0
    return-void

    .line 303
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->getDisplayCount(I)I

    move-result v5

    invoke-static {v4, v0, v5}, Lcom/vlingo/core/internal/util/OrdinalUtil;->storeOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Ljava/util/List;I)V

    .line 304
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->promptForDisambiguation(I)V

    .line 305
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 306
    .local v3, "pageList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->PlayMusic:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 307
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v4

    const-string/jumbo v5, "music-play-disambiguate"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    goto :goto_0

    .line 311
    .end local v3    # "pageList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    :cond_2
    invoke-static {p1}, Lcom/samsung/music/IntentsAndExtras$MoodType;->getMoodTypeByName(Ljava/lang/String;)Lcom/samsung/music/IntentsAndExtras$MoodType;

    move-result-object v2

    .line 312
    .local v2, "moodType":Lcom/samsung/music/IntentsAndExtras$MoodType;
    if-eqz v2, :cond_3

    sget-object v4, Lcom/samsung/music/IntentsAndExtras$MoodType;->UNDEFINED:Lcom/samsung/music/IntentsAndExtras$MoodType;

    invoke-virtual {v2, v4}, Lcom/samsung/music/IntentsAndExtras$MoodType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 313
    invoke-direct {p0, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->playByMood(Lcom/samsung/music/IntentsAndExtras$MoodType;)V

    goto :goto_0

    .line 316
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v4

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getNoMatchAnyMusic()Lcom/vlingo/core/internal/ResourceIdProvider$string;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 317
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v4

    const-string/jumbo v5, "music-play-no-match"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    goto :goto_0

    .line 322
    .end local v2    # "moodType":Lcom/samsung/music/IntentsAndExtras$MoodType;
    :cond_4
    if-eqz v0, :cond_5

    if-lez v1, :cond_5

    .line 323
    invoke-direct {p0, v0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->playMusic(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    .line 325
    :cond_5
    invoke-static {p1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->isPartialFavoriteMatch(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 326
    const/4 v0, 0x0

    .line 327
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getFavoritePlaylist(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 328
    if-eqz v0, :cond_6

    .line 329
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 330
    :cond_6
    if-eqz v0, :cond_7

    if-lt v1, v5, :cond_7

    .line 331
    invoke-direct {p0, v0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->playMusic(Ljava/util/List;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 335
    :cond_7
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v4

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getNoMatchAnyMusic()Lcom/vlingo/core/internal/ResourceIdProvider$string;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 336
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v4

    const-string/jumbo v5, "music-play-no-match"

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected abstract playFromDetails(Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;)V
.end method

.method protected abstract promptForDisambiguation(I)V
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishDialog()V

    .line 103
    return-void
.end method
