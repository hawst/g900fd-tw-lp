.class Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;
.super Ljava/lang/Object;
.source "SamsungSettingChangeAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 539
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 500
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->name:Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$800(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "emergencymode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 501
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->state:Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$900(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "on"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 503
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$1000(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/midas/samsungutils/utils/EmergencyModeUtils;->toggleEmergencyMode(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_5

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 504
    :catch_0
    move-exception v0

    .line 505
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 506
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$1100(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "unsupported device"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 507
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 508
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    .line 509
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$1200(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "unsupported device"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 510
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v0

    .line 511
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 512
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$1300(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "unsupported device"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 513
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 514
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 515
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$1400(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "unsupported device"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 516
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v0

    .line 517
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    .line 518
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$1500(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "unsupported device"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 519
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_5
    move-exception v0

    .line 520
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    .line 521
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;->access$1600(Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "unsupported device"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 532
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 496
    return-void
.end method
