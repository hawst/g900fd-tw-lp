.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction$1;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "SamsungExecuteIntentAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;->execute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 3
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 30
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;->waitingForCoverOpened:Z
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;->access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    const-string/jumbo v0, "always"

    const-string/jumbo v1, "mCoverStateListener cover Opened"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;

    iget-object v1, v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;->doExecuteLaunchActivity()V
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;->access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;)V

    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;->waitingForCoverOpened:Z
    invoke-static {v0, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;->access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;Z)Z

    .line 42
    :goto_0
    return-void

    .line 37
    :cond_0
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "Cover opened, waitingForCoverOpened is false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 40
    :cond_1
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "mCoverStateListener cover Closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
