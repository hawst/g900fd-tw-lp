.class public Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "SamsungNotiChangeAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;


# static fields
.field private static final DRIVING_MODE_ALARM_NOTIFICATION:Ljava/lang/String; = "driving_mode_alarm_notification"

.field private static final DRIVING_MODE_EMAIL_NOTIFICATION:Ljava/lang/String; = "driving_mode_email_notification"

.field private static final DRIVING_MODE_INCOMING_CALL_NOTIFICATION:Ljava/lang/String; = "driving_mode_incoming_call_notification"

.field private static final DRIVING_MODE_MESSAGE_NOTIFICATION:Ljava/lang/String; = "driving_mode_message_notification"

.field private static final DRIVING_MODE_SCHEDULE_NOTIFICATION:Ljava/lang/String; = "driving_mode_schedule_notification"


# instance fields
.field private VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field private confirmOff:Ljava/lang/String;

.field private confirmOffTTS:Ljava/lang/String;

.field private confirmOn:Ljava/lang/String;

.field private confirmOnTTS:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private state:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method

.method private alarmNotificationEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 269
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 270
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "driving_mode_alarm_notification"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 272
    .local v1, "value":I
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private allNotificationAlreadyOff()Z
    .locals 1

    .prologue
    .line 289
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->callNotificationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->msgNotificationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->scheduleNotificationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->alarmNotificationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->emailNotificationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private allNotificationAlreadyOn()Z
    .locals 1

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->callNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->msgNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->scheduleNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->alarmNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->emailNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private callNotificationEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 248
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 249
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "driving_mode_incoming_call_notification"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 251
    .local v1, "value":I
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private emailNotificationEnabled()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 276
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 277
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "driving_mode_email_notification"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 279
    .local v1, "value":I
    const-string/jumbo v3, "driving_mode_email_notification"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "emailNotificationEnabled() value = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private fakeSystemPrompt(II)V
    .locals 2
    .param p1, "text"    # I
    .param p2, "tts"    # I

    .prologue
    .line 238
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/vlingo/midas/VlingoApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    return-void
.end method

.method private fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "tts"    # Ljava/lang/String;

    .prologue
    .line 242
    invoke-static {p2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object p2, p1

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_1
    return-void
.end method

.method private fakeSystemPromptAlreadyOff()V
    .locals 4

    .prologue
    .line 351
    const-string/jumbo v0, ""

    .line 352
    .local v0, "str":Ljava/lang/String;
    const-string/jumbo v1, ""

    .line 353
    .local v1, "strTTS":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "all"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 354
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_all_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 355
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_all_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 373
    :cond_0
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    return-void

    .line 356
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "call"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 357
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_call_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 358
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_call_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 359
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "sms"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 360
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_msg_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 361
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_msg_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 362
    :cond_3
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "calendar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 363
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_schedule_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 364
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_schedule_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 365
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "alarm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 366
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_alarm_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 367
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_alarm_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 368
    :cond_5
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "email"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 369
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_email_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 370
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_off_notification_email_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method private fakeSystemPromptAlreadyOn()V
    .locals 4

    .prologue
    .line 325
    const-string/jumbo v0, ""

    .line 326
    .local v0, "str":Ljava/lang/String;
    const-string/jumbo v1, ""

    .line 327
    .local v1, "strTTS":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "all"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 328
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_all_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 329
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_all_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 347
    :cond_0
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    return-void

    .line 330
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "call"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 331
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_call_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 332
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_call_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 333
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "sms"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 334
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_msg_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 335
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_msg_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 336
    :cond_3
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "calendar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 337
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_schedule_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 338
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_schedule_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 339
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "alarm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 340
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_alarm_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 341
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_alarm_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 342
    :cond_5
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v3, "email"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 343
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_email_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->turn_on_notification_email_already:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method private getConfirmOffDefaultString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 413
    const-string/jumbo v0, ""

    .line 414
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "all"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 415
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_all:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 427
    :cond_0
    :goto_0
    return-object v0

    .line 416
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "call"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 417
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_call:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 418
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "sms"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 419
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_msg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 420
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "calendar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 421
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_schedule:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 422
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "alarm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 423
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_alarm:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 424
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "email"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 425
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_email:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private getConfirmOffDefaultTTSString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 431
    const-string/jumbo v0, ""

    .line 432
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "all"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 433
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_all:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 445
    :cond_0
    :goto_0
    return-object v0

    .line 434
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "call"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 435
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_call:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 436
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "sms"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 437
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_msg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 438
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "calendar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 439
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_schedule:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 440
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "alarm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 441
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_alarm:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 442
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "email"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 443
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_off_notification_email:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private getConfirmOnDefaultString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 377
    const-string/jumbo v0, ""

    .line 378
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "all"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 379
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_all:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 391
    :cond_0
    :goto_0
    return-object v0

    .line 380
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "call"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 381
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_call:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 382
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "sms"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 383
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_msg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 384
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "calendar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 385
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_schedule:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 386
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "alarm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 387
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_alarm:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 388
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "email"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 389
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_email:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private getConfirmOnDefaultTTSString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 395
    const-string/jumbo v0, ""

    .line 396
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "all"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 397
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_all:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 409
    :cond_0
    :goto_0
    return-object v0

    .line 398
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "call"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 399
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_call:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 400
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "sms"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 401
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_msg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 402
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "calendar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 403
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_schedule:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 404
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "alarm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 405
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_alarm:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 406
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v2, "email"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 407
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->turn_on_notification_email:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private msgNotificationEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 255
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 256
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "driving_mode_message_notification"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 258
    .local v1, "value":I
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private onAlarmNotificationChange(I)V
    .locals 3
    .param p1, "noti_enable"    # I

    .prologue
    .line 312
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 313
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "driving_mode_alarm_notification"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 315
    return-void
.end method

.method private onCallNotificationChange(I)V
    .locals 3
    .param p1, "noti_enable"    # I

    .prologue
    .line 294
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 295
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "driving_mode_incoming_call_notification"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 297
    return-void
.end method

.method private onEmailNotificationChange(I)V
    .locals 4
    .param p1, "noti_enable"    # I

    .prologue
    .line 318
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 319
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v1, "driving_mode_email_notification"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "emailNotificationEnabled() noti_enable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "driving_mode_email_notification"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 322
    return-void
.end method

.method private onMsgNotificationChange(I)V
    .locals 3
    .param p1, "noti_enable"    # I

    .prologue
    .line 300
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 301
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "driving_mode_message_notification"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 303
    return-void
.end method

.method private onScheduleNotificationChange(I)V
    .locals 3
    .param p1, "noti_enable"    # I

    .prologue
    .line 306
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 307
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "driving_mode_schedule_notification"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 309
    return-void
.end method

.method private scheduleNotificationEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 262
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 263
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "driving_mode_schedule_notification"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 265
    .local v1, "value":I
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method


# virtual methods
.method public alreadySet(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 0
    .param p1, "confirmOffTTS"    # Ljava/lang/String;

    .prologue
    .line 91
    return-object p0
.end method

.method public confirmOff(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOff:Ljava/lang/String;

    .line 61
    if-eqz p1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->getConfirmOffDefaultString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOff:Ljava/lang/String;

    .line 64
    :cond_1
    return-object p0
.end method

.method public confirmOffTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOffTTS:Ljava/lang/String;

    .line 82
    if-eqz p1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 83
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->getConfirmOffDefaultTTSString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOffTTS:Ljava/lang/String;

    .line 85
    :cond_1
    return-object p0
.end method

.method public confirmOn(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOn:Ljava/lang/String;

    .line 51
    if-eqz p1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 52
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->getConfirmOnDefaultString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOn:Ljava/lang/String;

    .line 54
    :cond_1
    return-object p0
.end method

.method public confirmOnTTS(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOnTTS:Ljava/lang/String;

    .line 71
    if-eqz p1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 72
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->getConfirmOnDefaultTTSString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOnTTS:Ljava/lang/String;

    .line 75
    :cond_1
    return-object p0
.end method

.method protected execute()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 102
    const/4 v1, 0x0

    .line 103
    .local v1, "noti_enable":I
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    if-eqz v4, :cond_1e

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->state:Ljava/lang/String;

    if-eqz v4, :cond_1e

    .line 104
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v5, "all"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 106
    :try_start_0
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v5, "enable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    .line 107
    :goto_0
    if-ne v1, v2, :cond_1

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->allNotificationAlreadyOn()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOn()V

    .line 235
    :goto_1
    return-void

    :cond_0
    move v1, v3

    .line 106
    goto :goto_0

    .line 110
    :cond_1
    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->allNotificationAlreadyOff()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 111
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOff()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 114
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->onCallNotificationChange(I)V

    .line 115
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->onMsgNotificationChange(I)V

    .line 116
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->onScheduleNotificationChange(I)V

    .line 117
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->onAlarmNotificationChange(I)V

    .line 118
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->onEmailNotificationChange(I)V

    .line 121
    if-eqz v1, :cond_3

    .line 122
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOn:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOnTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 124
    :cond_3
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOff:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOffTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 129
    :cond_4
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v5, "call"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 131
    :try_start_2
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v5, "enable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    move v1, v2

    .line 132
    :goto_2
    if-ne v1, v2, :cond_6

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->callNotificationEnabled()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 133
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOn()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 146
    :catch_1
    move-exception v0

    .line 147
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    move v1, v3

    .line 131
    goto :goto_2

    .line 135
    :cond_6
    if-nez v1, :cond_7

    :try_start_3
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->callNotificationEnabled()Z

    move-result v2

    if-nez v2, :cond_7

    .line 136
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOff()V

    goto :goto_1

    .line 139
    :cond_7
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->onCallNotificationChange(I)V

    .line 140
    if-eqz v1, :cond_8

    .line 141
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOn:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOnTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 143
    :cond_8
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOff:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOffTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 149
    :cond_9
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v5, "sms"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 151
    :try_start_4
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v5, "enable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    move v1, v2

    .line 152
    :goto_3
    if-ne v1, v2, :cond_b

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->msgNotificationEnabled()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 153
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOn()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    .line 166
    :catch_2
    move-exception v0

    .line 167
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_a
    move v1, v3

    .line 151
    goto :goto_3

    .line 155
    :cond_b
    if-nez v1, :cond_c

    :try_start_5
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->msgNotificationEnabled()Z

    move-result v2

    if-nez v2, :cond_c

    .line 156
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOff()V

    goto/16 :goto_1

    .line 159
    :cond_c
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->onMsgNotificationChange(I)V

    .line 160
    if-eqz v1, :cond_d

    .line 161
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOn:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOnTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 163
    :cond_d
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOff:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOffTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_1

    .line 169
    :cond_e
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v5, "calendar"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 171
    :try_start_6
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v5, "enable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    move v1, v2

    .line 172
    :goto_4
    if-ne v1, v2, :cond_10

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->scheduleNotificationEnabled()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 173
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOn()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_1

    .line 186
    :catch_3
    move-exception v0

    .line 187
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_f
    move v1, v3

    .line 171
    goto :goto_4

    .line 175
    :cond_10
    if-nez v1, :cond_11

    :try_start_7
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->scheduleNotificationEnabled()Z

    move-result v2

    if-nez v2, :cond_11

    .line 176
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOff()V

    goto/16 :goto_1

    .line 179
    :cond_11
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->onScheduleNotificationChange(I)V

    .line 180
    if-eqz v1, :cond_12

    .line 181
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOn:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOnTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 183
    :cond_12
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOff:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOffTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_1

    .line 189
    :cond_13
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v5, "alarm"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 191
    :try_start_8
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v5, "enable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    move v1, v2

    .line 192
    :goto_5
    if-ne v1, v2, :cond_15

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->alarmNotificationEnabled()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 193
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOn()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_1

    .line 206
    :catch_4
    move-exception v0

    .line 207
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_14
    move v1, v3

    .line 191
    goto :goto_5

    .line 195
    :cond_15
    if-nez v1, :cond_16

    :try_start_9
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->alarmNotificationEnabled()Z

    move-result v2

    if-nez v2, :cond_16

    .line 196
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOff()V

    goto/16 :goto_1

    .line 199
    :cond_16
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->onAlarmNotificationChange(I)V

    .line 200
    if-eqz v1, :cond_17

    .line 201
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOn:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOnTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 203
    :cond_17
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOff:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOffTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_1

    .line 209
    :cond_18
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    const-string/jumbo v5, "email"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 211
    :try_start_a
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->state:Ljava/lang/String;

    const-string/jumbo v5, "enable"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_19

    move v1, v2

    .line 212
    :goto_6
    if-ne v1, v2, :cond_1a

    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->emailNotificationEnabled()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 213
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOn()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_1

    .line 226
    :catch_5
    move-exception v0

    .line 227
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_19
    move v1, v3

    .line 211
    goto :goto_6

    .line 215
    :cond_1a
    if-nez v1, :cond_1b

    :try_start_b
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->emailNotificationEnabled()Z

    move-result v2

    if-nez v2, :cond_1b

    .line 216
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPromptAlreadyOff()V

    goto/16 :goto_1

    .line 219
    :cond_1b
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->onEmailNotificationChange(I)V

    .line 220
    if-eqz v1, :cond_1c

    .line 221
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOn:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOnTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 223
    :cond_1c
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOff:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->confirmOffTTS:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->fakeSystemPrompt(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5

    goto/16 :goto_1

    .line 230
    :cond_1d
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "unsupported device"

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 233
    :cond_1e
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "unsupported device"

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public name(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->name:Ljava/lang/String;

    .line 38
    return-object p0
.end method

.method public state(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 0
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->state:Ljava/lang/String;

    .line 44
    return-object p0
.end method

.method public vvsListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SettingChangeInterface;
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;->VVSlistener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 97
    return-object p0
.end method
