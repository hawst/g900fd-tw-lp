.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;
.super Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
.source "SamsungLocalSearchAdaptor.java"

# interfaces
.implements Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$1;,
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;
    }
.end annotation


# instance fields
.field private currentSearchQuery:Ljava/lang/String;

.field private currentSpokenLocation:Ljava/lang/String;

.field private currentSpokenSearch:Ljava/lang/String;

.field private failReason:Ljava/lang/String;

.field private isRequestComplete:Z

.field private isRequestFailed:Z

.field private localSearchDetailsListing:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;"
        }
    .end annotation
.end field

.field private localSearchListings:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;"
        }
    .end annotation
.end field

.field private lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

.field private numberOfRetries:I

.field private providers:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private selectedLocationType:Ljava/lang/String;

.field private widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;-><init>()V

    .line 31
    iput-boolean v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestComplete:Z

    .line 32
    iput-boolean v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestFailed:Z

    .line 33
    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->failReason:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->selectedLocationType:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 41
    iput v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->numberOfRetries:I

    .line 45
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    .line 46
    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    .line 47
    iput-boolean v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestComplete:Z

    .line 48
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    .line 49
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSpokenLocation:Ljava/lang/String;

    .line 50
    return-void
.end method

.method private executeSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "search"    # Ljava/lang/String;
    .param p2, "spokenLocation"    # Ljava/lang/String;
    .param p3, "region"    # Ljava/lang/String;
    .param p4, "force"    # Z

    .prologue
    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestComplete:Z

    .line 63
    if-nez p1, :cond_0

    .line 64
    const-string/jumbo p1, ""

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 66
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 69
    iget v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->numberOfRetries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->numberOfRetries:I

    .line 72
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p4, :cond_4

    .line 73
    :cond_3
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    .line 74
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSpokenSearch:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSpokenLocation:Ljava/lang/String;

    .line 76
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 77
    const-string/jumbo v0, "language"

    const-string/jumbo v1, "zh-CN"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "zh-CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 78
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->sendChineseSearchRequet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_4
    :goto_0
    return-void

    .line 80
    :cond_5
    const-string/jumbo v0, "Not match conditon"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_6
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-virtual {v0, p1, p2, p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendSearchRequest(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    goto :goto_0
.end method

.method private sendChineseSearchRequet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "spokenLocation"    # Ljava/lang/String;
    .param p3, "region"    # Ljava/lang/String;

    .prologue
    .line 302
    :try_start_0
    invoke-static {p2, p3, p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchChineseURLMaker;->getBusinessURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 303
    .local v1, "requetURL":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 304
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 312
    .end local v1    # "requetURL":Ljava/lang/String;
    :goto_0
    return-void

    .line 307
    .restart local v1    # "requetURL":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-virtual {v2, v1, p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendChineseSearchRequest(Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 308
    .end local v1    # "requetURL":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 309
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setLocalSearchListings(Ljava/util/Vector;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "items":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;"
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    monitor-enter v1

    .line 99
    :try_start_0
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    .line 100
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->setProviders(Ljava/util/Vector;)V

    .line 102
    return-void

    .line 100
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private setProviders(Ljava/util/Vector;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "items":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->providers:Ljava/util/HashSet;

    .line 106
    invoke-virtual {p1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .line 107
    .local v1, "item":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getProvider()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 108
    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getProvider()Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, "provider":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->providers:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 112
    .end local v1    # "item":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .end local v2    # "provider":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public executeDetailRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 2
    .param p1, "currentBusinessItem"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .param p2, "listenerFromWidget"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 89
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;

    invoke-direct {v1, p0, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    invoke-virtual {v0, p1, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendChineseMoreDetailsRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    .line 95
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;

    invoke-direct {v1, p0, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    invoke-virtual {v0, p1, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendMoreDetailsRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    goto :goto_0
.end method

.method public executeForceSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "search"    # Ljava/lang/String;
    .param p2, "spokenLocation"    # Ljava/lang/String;
    .param p3, "region"    # Ljava/lang/String;
    .param p4, "isForced"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->executeSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 54
    return-void
.end method

.method public executeSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "search"    # Ljava/lang/String;
    .param p2, "spokenLocation"    # Ljava/lang/String;
    .param p3, "region"    # Ljava/lang/String;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->executeSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 59
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 131
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    monitor-enter v1

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCurrentSearchQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentSpokenLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSpokenLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentSpokenSearch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSpokenSearch:Ljava/lang/String;

    return-object v0
.end method

.method public getFailReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->failReason:Ljava/lang/String;

    return-object v0
.end method

.method public getItem(I)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 121
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 123
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    monitor-exit v1

    .line 125
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getLocalSearchDetailsListing()Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchDetailsListing:Ljava/util/Vector;

    monitor-enter v1

    .line 285
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchDetailsListing:Ljava/util/Vector;

    monitor-exit v1

    return-object v0

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getNumberOfRetries()I
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->numberOfRetries:I

    return v0
.end method

.method public getWidgetListener()Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    return-object v0
.end method

.method public isDefaultLocation()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 319
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->selectedLocationType:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 320
    const-string/jumbo v1, "DEFAULT"

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->selectedLocationType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "USERCACHEDLOCATION"

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->selectedLocationType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "USERPHONENUMBER"

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->selectedLocationType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 324
    :cond_0
    :goto_0
    return v0

    .line 320
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLocationTypeEmpty()Z
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->selectedLocationType:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isRequestComplete()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestComplete:Z

    return v0
.end method

.method public isRequestFailed()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestFailed:Z

    return v0
.end method

.method public onRequestComplete(ZLjava/lang/Object;)V
    .locals 2
    .param p1, "success"    # Z
    .param p2, "items"    # Ljava/lang/Object;

    .prologue
    .line 201
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestComplete:Z

    .line 202
    if-eqz p1, :cond_2

    .line 205
    instance-of v1, p2, Landroid/util/Pair;

    if-eqz v1, :cond_2

    move-object v1, p2

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    instance-of v1, v1, Ljava/util/Vector;

    if-eqz v1, :cond_2

    move-object v0, p2

    .line 206
    check-cast v0, Landroid/util/Pair;

    .line 207
    .local v0, "items2":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;>;"
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/Vector;

    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->setLocalSearchListings(Ljava/util/Vector;)V

    .line 208
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getQuery()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSearchQuery:Ljava/lang/String;

    .line 211
    :cond_0
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getLocalizedCityState()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 212
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getLocalizedCityState()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->currentSpokenLocation:Ljava/lang/String;

    .line 214
    :cond_1
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getSelectedLocationType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 215
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;->getSelectedLocationType()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->selectedLocationType:Ljava/lang/String;

    .line 224
    .end local v0    # "items2":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/vlingo/core/internal/localsearch/LocalSearchRequestInfo;Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;>;"
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v1, :cond_3

    .line 225
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onResponseReceived()V

    .line 227
    :cond_3
    return-void
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;)V
    .locals 2
    .param p1, "failureType"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    .prologue
    .line 242
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$1;->$SwitchMap$com$vlingo$core$internal$localsearch$LocalSearchRequestListener$LocalSearchFailureType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 253
    :goto_0
    return-void

    .line 244
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    goto :goto_0

    .line 247
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    goto :goto_0

    .line 250
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 242
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onRequestFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "failReasonParam"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 231
    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestComplete:Z

    .line 232
    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestFailed:Z

    .line 233
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->failReason:Ljava/lang/String;

    .line 235
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    .line 238
    :cond_0
    return-void
.end method

.method public onRequestScheduled()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 258
    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestComplete:Z

    .line 259
    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->isRequestFailed:Z

    .line 261
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestScheduled()V

    .line 264
    :cond_0
    return-void
.end method

.method public resetNumberOfRetries()V
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->numberOfRetries:I

    .line 278
    return-void
.end method

.method public resetSearch()V
    .locals 2

    .prologue
    .line 115
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    monitor-enter v1

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchListings:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 117
    monitor-exit v1

    .line 118
    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setLocalSearchDetailsListing(Ljava/util/Vector;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 294
    .local p1, "localSearchDetailsListing":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;"
    monitor-enter p1

    .line 295
    :try_start_0
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->localSearchDetailsListing:Ljava/util/Vector;

    .line 296
    monitor-exit p1

    .line 297
    return-void

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 0
    .param p1, "widgetListener"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 195
    return-void
.end method
