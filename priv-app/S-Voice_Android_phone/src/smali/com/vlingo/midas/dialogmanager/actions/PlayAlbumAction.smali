.class public Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "PlayAlbumAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/PlayMusicInterface;


# instance fields
.field private musicInfo:Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 4

    .prologue
    .line 29
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;->musicInfo:Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;

    if-eqz v2, :cond_0

    .line 31
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;->musicInfo:Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;->getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 32
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    const-string/jumbo v3, "Activity could not be found."

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 37
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "noMatchMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 44
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    const-string/jumbo v1, "android.intent.extra.focus"

    const-string/jumbo v2, "vnd.android.cursor.item/album"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    const-string/jumbo v1, "android.intent.extra.album"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    return-object v0
.end method

.method public bridge synthetic playlist(Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/PlayMusicInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;->playlist(Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;)Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;

    move-result-object v0

    return-object v0
.end method

.method public playlist(Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;)Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;
    .locals 0
    .param p1, "playlist"    # Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;->musicInfo:Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;

    .line 24
    return-object p0
.end method
