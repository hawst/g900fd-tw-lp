.class public Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;
.super Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;
.source "SVoiceMemoController.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;-><init>()V

    return-void
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->memo_delete_fail:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 124
    invoke-super {p0, p1}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->actionFail(Ljava/lang/String;)V

    .line 125
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->memo_delete_confirmation:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 118
    invoke-super {p0}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->actionSuccess()V

    .line 119
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 21
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 39
    const-string/jumbo v14, "ListPositionChange"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 40
    .local v6, "listPositionChange":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 41
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v5

    .line 42
    .local v5, "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    const-string/jumbo v14, "next"

    invoke-virtual {v14, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 44
    :try_start_0
    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ListControlData;->incCurrentPage()V

    .line 45
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->which_one_for_disambiguation:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :cond_0
    :goto_0
    const-string/jumbo v14, "prev"

    invoke-virtual {v14, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 52
    :try_start_1
    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ListControlData;->decCurrentPage()V

    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->which_one_for_disambiguation:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_1 .. :try_end_1} :catch_1

    .line 59
    :cond_1
    :goto_1
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v10

    .line 60
    .local v10, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v14

    invoke-virtual {v14, v10}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v13

    .line 61
    .local v13, "pageMemos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    sget-object v14, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MemoList:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeReplaceable()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v15

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-interface {v0, v14, v15, v13, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 62
    const/4 v14, 0x0

    .line 112
    .end local v5    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    .end local v10    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .end local v13    # "pageMemos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :goto_2
    return v14

    .line 46
    .restart local v5    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    :catch_0
    move-exception v3

    .line 47
    .local v3, "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->that_is_all_for_disambiguation:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto :goto_0

    .line 54
    .end local v3    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    :catch_1
    move-exception v3

    .line 55
    .restart local v3    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->that_is_all_for_disambiguation:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto :goto_1

    .line 64
    .end local v3    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    .end local v5    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    :cond_2
    invoke-interface/range {p1 .. p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "LPAction"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 67
    invoke-super/range {p0 .. p2}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v14

    goto :goto_2

    .line 71
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getRegularWidgetMax()I

    move-result v7

    .line 73
    .local v7, "maxWidget":I
    const/4 v8, 0x0

    .line 74
    .local v8, "memoSelection":Lcom/vlingo/core/internal/memo/Memo;
    const-string/jumbo v14, "Name"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    .line 75
    .local v11, "name":Ljava/lang/String;
    sget-object v14, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->CURRENT_ACTION:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 77
    .local v2, "actionName":Ljava/lang/String;
    const-string/jumbo v14, "Which"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    .line 78
    .local v12, "ordinal":Ljava/lang/String;
    invoke-static {v12}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 79
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->clearOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 86
    :goto_3
    invoke-static {v12}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_7

    if-nez v8, :cond_7

    .line 87
    sget-object v14, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ORDINAL_DATA:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    .line 93
    .restart local v10    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :goto_4
    if-eqz v10, :cond_8

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-eq v14, v15, :cond_4

    if-eqz v8, :cond_8

    .line 94
    :cond_4
    const-string/jumbo v14, "MemoLookup"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 95
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->memo_multiple_found_new_loockup_one:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 112
    :cond_5
    :goto_5
    invoke-super/range {p0 .. p2}, Lcom/vlingo/midas/samsungutils/vvs/controllers/MemoController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v14

    goto/16 :goto_2

    .line 81
    .end local v10    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :cond_6
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getOrdinalData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Ljava/util/List;

    move-result-object v10

    .line 82
    .restart local v10    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    invoke-static/range {p2 .. p2}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v14

    invoke-virtual {v14, v10}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v13

    .line 83
    .restart local v13    # "pageMemos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    invoke-static {v13, v12}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "memoSelection":Lcom/vlingo/core/internal/memo/Memo;
    check-cast v8, Lcom/vlingo/core/internal/memo/Memo;

    .restart local v8    # "memoSelection":Lcom/vlingo/core/internal/memo/Memo;
    goto :goto_3

    .line 89
    .end local v10    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    .end local v13    # "pageMemos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/MemoManager;->getMemoUtil()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v9

    .line 90
    .local v9, "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    invoke-interface/range {p2 .. p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v14

    invoke-interface {v9, v14, v11}, Lcom/vlingo/core/internal/memo/IMemoUtil;->searchMemos(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .restart local v10    # "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    goto :goto_4

    .line 97
    .end local v9    # "memoUtil":Lcom/vlingo/core/internal/memo/IMemoUtil;
    :cond_8
    if-eqz v10, :cond_5

    .line 98
    sget-object v14, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_DELETECHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v14}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    .line 99
    .local v4, "fieldId":Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;
    const-string/jumbo v14, "MemoLookup"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 100
    sget-object v14, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MEMO_LOOKUPCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v14}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    .line 102
    :cond_9
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v14

    if-le v14, v7, :cond_a

    .line 103
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    sget v17, Lcom/vlingo/midas/R$string;->memo_multiple_found_new_loockup_large_list:I

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-virtual/range {v16 .. v18}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v15, v0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto :goto_5

    .line 107
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/dialogmanager/controllers/SVoiceMemoController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    sget v17, Lcom/vlingo/midas/R$string;->memo_multiple_found_new_loockup:I

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-virtual/range {v16 .. v18}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v15, v0, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto/16 :goto_5
.end method
