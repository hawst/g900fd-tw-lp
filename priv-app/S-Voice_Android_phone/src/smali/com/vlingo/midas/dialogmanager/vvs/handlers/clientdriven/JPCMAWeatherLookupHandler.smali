.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;
.source "JPCMAWeatherLookupHandler.java"


# static fields
.field private static final weatherCodeToStringArray:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final weatherCodeTodayToStringArray:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private index:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    .line 27
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_sunny:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Cloudy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mostly_cloudy:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Overcast:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_partly_sunny:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Shower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_partly_sunny_with_showers:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_thunderstorms:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShowerwithHail:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_thunderstorms:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sleet:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mixed_rain_and_snow:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_rainy:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavyRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_rainy:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_showers:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowFlurry:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_partly_sunny_with_showers:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightSnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_flurries:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavySnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_snow:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_ice:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Foggy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_foggy:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_duststorm:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Haze:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_haze:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    .line 46
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sunny:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_sunny_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Cloudy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mostly_cloudy_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Overcast:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_partly_sunny_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Shower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_partly_sunny_with_showers_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShower:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_thunderstorms_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->ThunderShowerwithHail:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_thunderstorms_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Sleet:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_mixed_rain_and_snow_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_rainy_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavyRain:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_rainy_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Storm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_showers_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowFlurry:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_partly_sunny_with_showers_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->LightSnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_flurries_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->HeavySnow:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_snow_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->SnowStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_ice_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Foggy:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_foggy_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->DustStorm:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_duststorm_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    sget-object v1, Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;->Haze:Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    sget v2, Lcom/vlingo/midas/R$array;->weather_haze_today:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;-><init>()V

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->index:I

    return-void
.end method

.method private getFormattedForecast(Ljava/lang/Integer;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "stringRes"    # I
    .param p3, "weatherCode"    # I
    .param p4, "day"    # Ljava/lang/String;
    .param p5, "maxTemp"    # Ljava/lang/String;
    .param p6, "currentTemp"    # Ljava/lang/String;

    .prologue
    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 93
    .local v0, "replacements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v1, "day"

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    const-string/jumbo v1, "temp"

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    const-string/jumbo v1, "cur_temp"

    invoke-interface {v0, v1, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->getWeatherStringFromArrayRes(Ljava/lang/Integer;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/util/StringUtils;->replaceTokens(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getWeatherStringFromArrayRes(Ljava/lang/Integer;I)Ljava/lang/String;
    .locals 6
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "code"    # I

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "strList":[Ljava/lang/String;
    if-nez p1, :cond_1

    .line 74
    iget v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->index:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 75
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    array-length v4, v0

    int-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->index:I

    .line 77
    :cond_0
    iget v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->index:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 85
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v1, v0, v2

    .line 88
    .local v1, "toReturn":Ljava/lang/String;
    return-object v1

    .line 81
    .end local v1    # "toReturn":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->index:I

    goto :goto_0
.end method


# virtual methods
.method protected getMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;
    .locals 10
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "day"    # Ljava/lang/String;
    .param p3, "city"    # Ljava/lang/String;
    .param p4, "state"    # Ljava/lang/String;
    .param p5, "maxTemp"    # Ljava/lang/String;
    .param p6, "currentTemp"    # Ljava/lang/String;
    .param p7, "weatherCode"    # I
    .param p8, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    .prologue
    .line 114
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    move-object/from16 v0, p8

    if-ne v0, v1, :cond_0

    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    :goto_0
    invoke-static/range {p7 .. p7}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->WeatherCodeToWeatherType(I)Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 115
    .local v8, "stringRes":Ljava/lang/Integer;
    if-nez v8, :cond_1

    .line 116
    invoke-super/range {p0 .. p8}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;

    move-result-object v9

    .line 120
    .local v9, "value":Ljava/lang/String;
    :goto_1
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->capitalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 114
    .end local v8    # "stringRes":Ljava/lang/Integer;
    .end local v9    # "value":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    goto :goto_0

    .line 118
    .restart local v8    # "stringRes":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v1, p0

    move-object v2, p1

    move/from16 v4, p7

    move-object v5, p2

    move-object v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->getFormattedForecast(Ljava/lang/Integer;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "value":Ljava/lang/String;
    goto :goto_1
.end method

.method protected getOneDaySpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "day"    # Ljava/lang/String;
    .param p3, "indexDay"    # Ljava/lang/Integer;
    .param p4, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;
    .param p5, "generalMsg"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    .line 126
    .local v0, "context":Landroid/content/Context;
    instance-of v2, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/vlingo/midas/gui/ConversationActivity;

    .end local v0    # "context":Landroid/content/Context;
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->isDrivingMode()Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    move-result-object v2

    sget-object v3, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 127
    .local v1, "inDrivingMode":Z
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v1, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->space:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-super/range {p0 .. p5}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getOneDaySpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 126
    .end local v1    # "inDrivingMode":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 127
    .restart local v1    # "inDrivingMode":Z
    :cond_1
    const-string/jumbo v2, ""

    goto :goto_1
.end method

.method protected getSpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;
    .locals 10
    .param p1, "strContext"    # Ljava/lang/Integer;
    .param p2, "weatherPhenomenon"    # Ljava/lang/String;
    .param p3, "day"    # Ljava/lang/String;
    .param p4, "city"    # Ljava/lang/String;
    .param p5, "state"    # Ljava/lang/String;
    .param p6, "maxTemp"    # Ljava/lang/String;
    .param p7, "currentTemp"    # Ljava/lang/String;
    .param p8, "weatherCode"    # I
    .param p9, "dateType"    # Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    .prologue
    .line 102
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;->TODAY:Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;

    move-object/from16 v0, p9

    if-ne v0, v1, :cond_0

    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeTodayToStringArray:Ljava/util/HashMap;

    :goto_0
    invoke-static/range {p8 .. p8}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->WeatherCodeToWeatherType(I)Lcom/vlingo/midas/util/CMAWeatherResourceUtil$WeatherType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 103
    .local v8, "stringRes":Ljava/lang/Integer;
    if-nez v8, :cond_1

    .line 104
    invoke-super/range {p0 .. p9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler;->getSpokenMsg(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/vlingo/core/internal/dialogmanager/vvs/handlers/CMAWeatherLookupHandler$CMADateType;)Ljava/lang/String;

    move-result-object v9

    .line 108
    .local v9, "value":Ljava/lang/String;
    :goto_1
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->capitalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 102
    .end local v8    # "stringRes":Ljava/lang/Integer;
    .end local v9    # "value":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->weatherCodeToStringArray:Ljava/util/HashMap;

    goto :goto_0

    .line 106
    .restart local v8    # "stringRes":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v1, p0

    move-object v2, p1

    move/from16 v4, p8

    move-object v5, p3

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v1 .. v7}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;->getFormattedForecast(Ljava/lang/Integer;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "value":Ljava/lang/String;
    goto :goto_1
.end method
