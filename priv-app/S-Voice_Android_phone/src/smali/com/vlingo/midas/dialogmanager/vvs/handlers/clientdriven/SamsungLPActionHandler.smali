.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/LPActionHandler;
.source "SamsungLPActionHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/LPActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 12
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v8, 0x0

    .line 31
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/dialogmanager/Controller;

    .line 32
    .local v2, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    instance-of v9, v2, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    if-eqz v9, :cond_3

    move-object v1, v2

    .line 33
    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    .line 34
    .local v1, "alertController":Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;
    const-string/jumbo v9, "Action"

    invoke-static {p1, v9, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "actionValue":Ljava/lang/String;
    const-string/jumbo v9, "nextPage"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string/jumbo v9, "prevPage"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 36
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v5

    .line 37
    .local v5, "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    const-string/jumbo v9, "nextPage"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 39
    :try_start_0
    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ListControlData;->incCurrentPage()V

    .line 40
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->which_one_for_disambiguation:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v3

    .line 53
    .local v3, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getAlertQueue()Ljava/util/LinkedList;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getAlertQueue()Ljava/util/LinkedList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 54
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v9

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getAlertQueue()Ljava/util/LinkedList;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 55
    .local v7, "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1, v7}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getMessagesForWidget(Ljava/util/List;)Ljava/util/List;

    move-result-object v10

    invoke-interface {p2, v9, v3, v10, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 63
    .end local v0    # "actionValue":Ljava/lang/String;
    .end local v1    # "alertController":Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;
    .end local v3    # "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .end local v5    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    .end local v7    # "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :goto_1
    return v8

    .line 41
    .restart local v0    # "actionValue":Ljava/lang/String;
    .restart local v1    # "alertController":Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;
    .restart local v5    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    :catch_0
    move-exception v4

    .line 42
    .local v4, "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->that_is_all_for_disambiguation:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto :goto_0

    .line 46
    .end local v4    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    :cond_1
    :try_start_1
    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ListControlData;->decCurrentPage()V

    .line 47
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->which_one_for_disambiguation:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 48
    :catch_1
    move-exception v4

    .line 49
    .restart local v4    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v10

    invoke-interface {v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->that_is_all_for_disambiguation:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto :goto_0

    .line 57
    .end local v4    # "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    .restart local v3    # "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v9

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getSenderList()Ljava/util/LinkedList;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 58
    .local v6, "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {p2, v9, v3, v6, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_1

    .line 63
    .end local v0    # "actionValue":Ljava/lang/String;
    .end local v1    # "alertController":Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;
    .end local v3    # "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .end local v5    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    .end local v6    # "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/LPActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v8

    goto :goto_1
.end method
