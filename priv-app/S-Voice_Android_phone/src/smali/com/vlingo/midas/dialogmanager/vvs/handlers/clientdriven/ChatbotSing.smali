.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChatbotSing;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ChatbotSing.java"


# instance fields
.field private mFileToPlayResid:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v3, 0x0

    .line 24
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 26
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    const-string/jumbo v2, "chatbot"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 28
    iput v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChatbotSing;->mFileToPlayResid:I

    .line 29
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 37
    sget v1, Lcom/vlingo/midas/R$raw;->chatbot_sunshine:I

    iput v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChatbotSing;->mFileToPlayResid:I

    .line 40
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 41
    .local v0, "flow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    iget v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChatbotSing;->mFileToPlayResid:I

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->playMedia(I)V

    .line 45
    return v3

    .line 31
    .end local v0    # "flow":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    :pswitch_0
    sget v1, Lcom/vlingo/midas/R$raw;->chatbot_big_band:I

    iput v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChatbotSing;->mFileToPlayResid:I

    goto :goto_0

    .line 34
    :pswitch_1
    sget v1, Lcom/vlingo/midas/R$raw;->chatbot_overdrive:I

    iput v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChatbotSing;->mFileToPlayResid:I

    goto :goto_0

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
