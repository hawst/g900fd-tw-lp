.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "EmergencyLocationSharingHandler.java"


# instance fields
.field private prompt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->prompt:Ljava/lang/String;

    return-void
.end method

.method private doSendEmergencyLocation()Z
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v11, 0x0

    .line 44
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyContactNumberUtils;->getEmergencyContactNumbers(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 46
    .local v1, "emergencyPhoneNumberList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$string;->emergency_phone_not_found:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "errMessage":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    .end local v2    # "errMessage":Ljava/lang/String;
    :goto_0
    return v11

    .line 53
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLat()D

    move-result-wide v3

    .line 54
    .local v3, "lat":D
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLong()D

    move-result-wide v5

    .line 55
    .local v5, "lng":D
    cmpl-double v9, v12, v3

    if-nez v9, :cond_2

    cmpl-double v9, v12, v5

    if-nez v9, :cond_2

    .line 56
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$string;->emergency_location_not_detected:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "answer":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9, v0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    .end local v0    # "answer":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$string;->send_current_location:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 64
    .restart local v0    # "answer":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 65
    .local v8, "msgBuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "http://maps.google.com/maps?f=q&q=("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v5, v6}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 74
    .local v7, "messageToSend":Ljava/lang/String;
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    invoke-virtual {v9, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->addresses(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->queue()V

    goto/16 :goto_0
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionFail(Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->msg_sent_fail:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->prompt:Ljava/lang/String;

    .line 31
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->prompt:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->prompt:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 36
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionSuccess()V

    .line 37
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->msg_sent_successful:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->prompt:Ljava/lang/String;

    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->prompt:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->prompt:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;->doSendEmergencyLocation()Z

    move-result v0

    return v0
.end method
