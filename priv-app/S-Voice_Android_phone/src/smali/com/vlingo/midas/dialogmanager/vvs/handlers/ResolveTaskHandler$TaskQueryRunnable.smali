.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;
.super Ljava/lang/Object;
.source "ResolveTaskHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TaskQueryRunnable"
.end annotation


# instance fields
.field action:Lcom/vlingo/sdk/recognition/VLAction;

.field resolveTaskHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 0
    .param p1, "resolveAppointmentHandler"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;
    .param p2, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;->resolveTaskHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;

    .line 80
    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;->action:Lcom/vlingo/sdk/recognition/VLAction;

    .line 81
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 85
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;->resolveTaskHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;->action:Lcom/vlingo/sdk/recognition/VLAction;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getTaskByQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v0

    .line 87
    .local v0, "queriedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    if-eqz v0, :cond_0

    .line 88
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;->resolveTaskHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getStoredTasks()Ljava/util/List;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;)Ljava/util/List;

    move-result-object v1

    .line 90
    .local v1, "storedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;->resolveTaskHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->sendTaskResolvedEvent(Ljava/util/List;I)V
    invoke-static {v2, v0, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;Ljava/util/List;I)V

    .line 91
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;->resolveTaskHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->addQueriedTasksToStore(Ljava/util/List;Ljava/util/List;)V
    invoke-static {v2, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->access$300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;Ljava/util/List;Ljava/util/List;)V

    .line 94
    .end local v1    # "storedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;->resolveTaskHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->access$400(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 96
    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;->resolveTaskHandler:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;

    .line 97
    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;->action:Lcom/vlingo/sdk/recognition/VLAction;

    .line 98
    return-void
.end method
