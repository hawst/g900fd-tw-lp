.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAlertReadbackHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/AlertReadbackHandler;
.source "SamsungAlertReadbackHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/AlertReadbackHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x0

    .line 34
    const-class v5, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAlertReadbackHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v2

    .line 35
    .local v2, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAllNewAlerts(I)Ljava/util/LinkedList;

    move-result-object v0

    .line 41
    .local v0, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-static {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->createSMSMMSSenderQueueMap(Ljava/util/LinkedList;)Ljava/util/HashMap;

    move-result-object v4

    .line 42
    .local v4, "senderQueue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    const-string/jumbo v5, "Contact"

    invoke-static {p1, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "contactName":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 44
    invoke-static {v1}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getMessageQueueByContactName(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v4

    .line 45
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 47
    :cond_0
    invoke-static {v0}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->createSMSMMSSenderQueueMap(Ljava/util/LinkedList;)Ljava/util/HashMap;

    move-result-object v4

    .line 48
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getMessageQueueByContactName(Ljava/util/HashMap;Ljava/lang/String;Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v4

    :cond_1
    move-object v5, v2

    .line 51
    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    invoke-virtual {v5, v1}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->setCurrentContactSearch(Ljava/lang/String;)V

    move-object v5, v2

    .line 52
    check-cast v5, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    invoke-virtual {v5, v4}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->setSenderQueue(Ljava/util/HashMap;)V

    .line 54
    invoke-static {}, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;->getInstance()Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;

    move-result-object v3

    .line 55
    .local v3, "multipleMessageReadoutUtil":Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;
    invoke-virtual {v3, p2}, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;->setMessagehandlerListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 56
    invoke-virtual {v2, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v5

    return v5
.end method
