.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;
.source "SamsungAnswerQuestionHandler.java"


# static fields
.field private static final MSG_WAITING_FOR_COVER_OPENED:I


# instance fields
.field mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field final mHandler:Landroid/os/Handler;

.field msg:Ljava/lang/String;

.field private waitingForCoverOpened:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->waitingForCoverOpened:Z

    .line 72
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler$2;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->waitingForCoverOpened:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 16
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->waitingForCoverOpened:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "x2"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->doExecuteAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method

.method private doExecuteAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/AnswerQuestionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v1, 0x0

    .line 29
    new-instance v2, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 31
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    .line 32
    .local v0, "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    new-instance v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 49
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 51
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->sview_cover_alwaysmicon:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 55
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 56
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 57
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x2710

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 59
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->waitingForCoverOpened:Z

    .line 63
    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;->doExecuteAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v1

    goto :goto_0
.end method
