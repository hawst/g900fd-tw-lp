.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;
.super Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;
.source "ShowPlayArtistWidgetHandler.java"


# static fields
.field private static final SEARCH_MUSIC_MAX:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->getMultiWidgetItemsUltimateMax()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sput v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->SEARCH_MUSIC_MAX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;-><init>()V

    return-void
.end method


# virtual methods
.method protected getDisambiguationList(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "exactSearch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    sget v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->SEARCH_MUSIC_MAX:I

    invoke-static {p1, p2, p3, v0}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->getArtistList(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/MusicUtil$MusicInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    invoke-static {p1, p2}, Lcom/vlingo/core/internal/util/MusicUtil;->findMatchingArtistList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getMusicList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    sget v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->SEARCH_MUSIC_MAX:I

    invoke-static {p1, p2, v0}, Lcom/vlingo/midas/samsungutils/utils/music/SearchMusic;->byArtist(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getNoMatchPromptRes(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ARTISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getPlayMusicType()Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;->ARTIST:Lcom/vlingo/midas/samsungutils/utils/music/PlayMusicType;

    return-object v0
.end method

.method protected playFromDetails(Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;)V
    .locals 2
    .param p1, "md"    # Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getArtist()Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "s":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 82
    invoke-virtual {p1}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getArtist()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->playByName(Ljava/lang/String;)V

    .line 89
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->playByName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected promptForDisambiguation(I)V
    .locals 10
    .param p1, "size"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 58
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getRegularWidgetMax()I

    move-result v0

    .line 60
    .local v0, "maxWidget":I
    if-le p1, v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->items_were_found_for_disambiguation_large_list:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYARTISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 74
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->items_were_found_for_disambiguation:I

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_MUSIC_PLAYARTISTCHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    goto :goto_0
.end method
