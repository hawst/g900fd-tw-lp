.class public Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;
.super Lcom/vlingo/core/internal/dialogmanager/StateController;
.source "SocialUpdateController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$2;
    }
.end annotation


# static fields
.field private static loginEventReceiverRegistered:Z


# instance fields
.field private loginEventComplete:Landroid/content/BroadcastReceiver;

.field private socialNetworks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private status:Ljava/lang/String;

.field thread:Ljava/lang/Thread;

.field private updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventReceiverRegistered:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->socialNetworks:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$1;-><init>(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventComplete:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 39
    sget-boolean v0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventReceiverRegistered:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 39
    sput-boolean p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventReceiverRegistered:Z

    return p0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventComplete:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->doSocialUpdateAction()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->reset()V

    return-void
.end method

.method static synthetic access$600(Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method private doSocialUpdateAction()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 243
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-eqz v1, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-virtual {v3}, Lcom/vlingo/midas/social/api/SocialNetworkType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 246
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->isLoggedIn(Lcom/vlingo/midas/social/api/SocialNetworkType;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 247
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 248
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->promptForStatus()V

    .line 256
    :goto_0
    return v0

    .line 251
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->promptForConfirm()V

    goto :goto_0

    .line 253
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->notLoggedIn()V

    .line 254
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isLoggedIn(Lcom/vlingo/midas/social/api/SocialNetworkType;)Z
    .locals 3
    .param p1, "socialNetwork"    # Lcom/vlingo/midas/social/api/SocialNetworkType;

    .prologue
    const/4 v0, 0x0

    .line 260
    if-eqz p1, :cond_0

    .line 261
    sget-object v1, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController$2;->$SwitchMap$com$vlingo$midas$social$api$SocialNetworkType:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/social/api/SocialNetworkType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 281
    :cond_0
    :goto_0
    return v0

    .line 263
    :pswitch_0
    const-string/jumbo v1, "facebook_account"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 266
    :pswitch_1
    const-string/jumbo v1, "twitter_account"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 269
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 270
    const-string/jumbo v1, "weibo_account"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 274
    :pswitch_3
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 275
    const-string/jumbo v1, "weibo_account"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 277
    :cond_1
    const-string/jumbo v1, "facebook_account"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "twitter_account"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 261
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private notLoggedIn()V
    .locals 5

    .prologue
    .line 305
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-nez v1, :cond_0

    .line 321
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-static {v1}, Lcom/vlingo/midas/util/SocialUtils;->loginToNetwork(Lcom/vlingo/midas/social/api/SocialNetworkType;)Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "loginMessage":Ljava/lang/String;
    sget-boolean v1, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventReceiverRegistered:Z

    if-nez v1, :cond_1

    .line 314
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventComplete:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.controllers.socialupdatecontroller"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 316
    const/4 v1, 0x1

    sput-boolean v1, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->loginEventReceiverRegistered:Z

    .line 319
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    const-string/jumbo v2, "car-social-home"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 320
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private promptForConfirm()V
    .locals 5

    .prologue
    .line 329
    new-instance v0, Lcom/vlingo/midas/dialogmanager/types/SocialType;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/dialogmanager/types/SocialType;-><init>(Lcom/vlingo/midas/social/api/SocialNetworkType;Ljava/lang/String;)V

    .line 330
    .local v0, "st":Lcom/vlingo/midas/dialogmanager/types/SocialType;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v1

    const-string/jumbo v2, ""

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_final_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_STATUS:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v4}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 331
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeSocialStatus:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 332
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v1

    const-string/jumbo v2, "car-social-home"

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 333
    return-void
.end method

.method private promptForSocialType()V
    .locals 4

    .prologue
    .line 295
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getSocialNetworkNameList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->socialNetworks:Ljava/util/ArrayList;

    .line 296
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    const-string/jumbo v1, ""

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_service_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_CHOICE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 297
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SocialNetworkChoice:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->socialNetworks:Ljava/util/ArrayList;

    invoke-interface {v0, v1, v2, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 298
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "car-social-select"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 299
    return-void
.end method

.method private promptForStatus()V
    .locals 3

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_status_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_SOCIAL_STATUS_MSG:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v2}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 325
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v0

    const-string/jumbo v1, "car-social-home"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 326
    return-void
.end method

.method private update(Z)V
    .locals 4
    .param p1, "isLoggedIn"    # Z

    .prologue
    .line 285
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->type(Lcom/vlingo/midas/social/api/SocialNetworkType;)Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->status(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;->queue()V

    .line 289
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-eqz v0, :cond_0

    .line 290
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-virtual {v1}, Lcom/vlingo/midas/social/api/SocialNetworkType;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/core/internal/recognition/acceptedtext/SocialAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 292
    :cond_0
    return-void
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 349
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_network_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 351
    .local v0, "errorMsg":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 352
    move-object p1, v0

    .line 354
    :cond_0
    move-object v1, p1

    .line 355
    .local v1, "spokenText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2, p1, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/StateController;->actionFail(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 360
    return-void

    .line 358
    .end local v0    # "errorMsg":Ljava/lang/String;
    .end local v1    # "spokenText":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v2
.end method

.method public actionSuccess()V
    .locals 2

    .prologue
    .line 338
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_status_updated:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    .line 339
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;->actionSuccess()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 344
    return-void

    .line 341
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    throw v0
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 12
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v11, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 148
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/StateController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 149
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->action:Lcom/vlingo/sdk/recognition/VLAction;

    .line 150
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "LPAction"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 151
    const-string/jumbo v9, "Action"

    invoke-static {p1, v9, v8}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "lpAction":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v9, "save"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 155
    iget-object v9, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 156
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->promptForStatus()V

    .line 238
    .end local v0    # "lpAction":Ljava/lang/String;
    :cond_0
    :goto_0
    return v7

    .line 159
    .restart local v0    # "lpAction":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, v8}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->update(Z)V

    move v7, v8

    .line 160
    goto :goto_0

    .line 165
    .end local v0    # "lpAction":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v8

    const-string/jumbo v9, "social"

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 168
    const-string/jumbo v8, "Status"

    invoke-static {p1, v8, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 169
    .local v4, "statusParam":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 170
    iput-object v4, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    .line 175
    :cond_3
    const-string/jumbo v8, "Value"

    invoke-static {p1, v8, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 176
    .local v3, "plus":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 177
    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 178
    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    .line 185
    :cond_4
    :goto_1
    const-string/jumbo v8, "Type"

    invoke-static {p1, v8, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 186
    .local v6, "type":Ljava/lang/String;
    const-string/jumbo v8, "Which"

    invoke-static {p1, v8, v7}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, "ordinal":Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 189
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v8

    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 190
    .local v5, "t":Ljava/lang/String;
    if-eqz v5, :cond_5

    .line 191
    move-object v6, v5

    .line 192
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v8

    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v8, v9, v11}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 195
    .end local v5    # "t":Ljava/lang/String;
    :cond_5
    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 196
    invoke-static {v6}, Lcom/vlingo/midas/social/api/SocialNetworkType;->getSocialNetworkType(Ljava/lang/String;)Lcom/vlingo/midas/social/api/SocialNetworkType;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 198
    :cond_6
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 199
    const-string/jumbo v8, "all"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 200
    invoke-static {v1}, Lcom/vlingo/midas/social/api/SocialNetworkType;->getSocialNetworkType(Ljava/lang/String;)Lcom/vlingo/midas/social/api/SocialNetworkType;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 208
    :cond_7
    :goto_2
    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    sget-object v9, Lcom/vlingo/midas/social/api/SocialNetworkType;->QZONE:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne v8, v9, :cond_a

    .line 209
    iput-object v11, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    goto/16 :goto_0

    .line 180
    .end local v1    # "ordinal":Ljava/lang/String;
    .end local v6    # "type":Ljava/lang/String;
    :cond_8
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    goto :goto_1

    .line 202
    .restart local v1    # "ordinal":Ljava/lang/String;
    .restart local v6    # "type":Ljava/lang/String;
    :cond_9
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getSocialNetworkNameList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-static {v8, v1}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getElementFromList(Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 203
    .local v2, "ordinalType":Ljava/lang/String;
    if-eqz v2, :cond_7

    .line 204
    invoke-static {v2}, Lcom/vlingo/midas/social/api/SocialNetworkType;->getSocialNetworkType(Ljava/lang/String;)Lcom/vlingo/midas/social/api/SocialNetworkType;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    goto :goto_2

    .line 213
    .end local v2    # "ordinalType":Ljava/lang/String;
    :cond_a
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v8

    if-eqz v8, :cond_c

    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    sget-object v9, Lcom/vlingo/midas/social/api/SocialNetworkType;->FACEBOOK:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-eq v8, v9, :cond_b

    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    sget-object v9, Lcom/vlingo/midas/social/api/SocialNetworkType;->TWITTER:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne v8, v9, :cond_c

    .line 215
    :cond_b
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v8

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_no_network:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 218
    :cond_c
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v8

    if-nez v8, :cond_d

    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    sget-object v9, Lcom/vlingo/midas/social/api/SocialNetworkType;->WEIBO:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne v8, v9, :cond_d

    .line 219
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v8

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_no_network:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 225
    :cond_d
    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-nez v8, :cond_e

    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "UpdatePage"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 226
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v8

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_no_network:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 228
    :cond_e
    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-nez v8, :cond_f

    .line 230
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isChinesePhone()Z

    move-result v8

    if-eqz v8, :cond_10

    .line 231
    sget-object v7, Lcom/vlingo/midas/social/api/SocialNetworkType;->WEIBO:Lcom/vlingo/midas/social/api/SocialNetworkType;

    iput-object v7, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 238
    :cond_f
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->doSocialUpdateAction()Z

    move-result v7

    goto/16 :goto_0

    .line 234
    :cond_10
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->promptForSocialType()V

    goto/16 :goto_0
.end method

.method public getRuleMappings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRules()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 365
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 95
    const-string/jumbo v3, "choice"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 96
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 97
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 98
    .local v0, "extras":Landroid/os/Bundle;
    const-string/jumbo v3, "choice"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 99
    .local v1, "index":I
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->socialNetworks:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 100
    .local v2, "socialType":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/midas/social/api/SocialNetworkType;->getSocialNetworkType(Ljava/lang/String;)Lcom/vlingo/midas/social/api/SocialNetworkType;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 101
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-eqz v3, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-virtual {v5}, Lcom/vlingo/midas/social/api/SocialNetworkType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 104
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-direct {p0, v3}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->isLoggedIn(Lcom/vlingo/midas/social/api/SocialNetworkType;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 105
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->promptForStatus()V

    .line 145
    .end local v0    # "extras":Landroid/os/Bundle;
    .end local v1    # "index":I
    .end local v2    # "socialType":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 106
    .restart local v0    # "extras":Landroid/os/Bundle;
    .restart local v1    # "index":I
    .restart local v2    # "socialType":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-direct {p0, v3}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->isLoggedIn(Lcom/vlingo/midas/social/api/SocialNetworkType;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 107
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->promptForConfirm()V

    goto :goto_0

    .line 109
    :cond_3
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->notLoggedIn()V

    goto :goto_0

    .line 112
    .end local v0    # "extras":Landroid/os/Bundle;
    .end local v1    # "index":I
    .end local v2    # "socialType":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 113
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 114
    const-string/jumbo v3, "message"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 115
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 116
    .restart local v0    # "extras":Landroid/os/Bundle;
    const-string/jumbo v3, "message"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    .line 118
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_5
    const-string/jumbo v3, "social choice"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 119
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 120
    .restart local v0    # "extras":Landroid/os/Bundle;
    const-string/jumbo v3, "social choice"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/midas/social/api/SocialNetworkType;->getSocialNetworkType(Ljava/lang/String;)Lcom/vlingo/midas/social/api/SocialNetworkType;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 122
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_6
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->update(Z)V

    goto :goto_0

    .line 123
    :cond_7
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 124
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->interruptTurn()V

    .line 125
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;->showSystemTurn(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->reset()V

    goto/16 :goto_0

    .line 127
    :cond_8
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.BodyChange"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 128
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishTurn()V

    .line 129
    const-string/jumbo v3, "message"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 130
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 131
    .restart local v0    # "extras":Landroid/os/Bundle;
    const-string/jumbo v3, "message"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    goto/16 :goto_0

    .line 133
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_9
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->status:Ljava/lang/String;

    goto/16 :goto_0

    .line 135
    :cond_a
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.SocialChange"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 136
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->finishTurn()V

    .line 137
    const-string/jumbo v3, "social choice"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 138
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 139
    .restart local v0    # "extras":Landroid/os/Bundle;
    const-string/jumbo v3, "social choice"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/midas/social/api/SocialNetworkType;->getSocialNetworkType(Ljava/lang/String;)Lcom/vlingo/midas/social/api/SocialNetworkType;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    .line 140
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-eqz v3, :cond_1

    .line 141
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/controllers/SocialUpdateController;->updateType:Lcom/vlingo/midas/social/api/SocialNetworkType;

    invoke-virtual {v5}, Lcom/vlingo/midas/social/api/SocialNetworkType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 1
    .param p1, "actionParam"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 375
    const/4 v0, 0x0

    return v0
.end method
