.class final Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;
.super Ljava/lang/Object;
.source "WorldTimeResponseParser.java"

# interfaces
.implements Lorg/xml/sax/ContentHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser;->parse(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private currentName:Ljava/lang/String;

.field private level:I

.field final synthetic val$response:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;)V
    .locals 1

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->val$response:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->level:I

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->currentName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 2
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 85
    const-string/jumbo v0, "Message"

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->currentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->level:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->val$response:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    iput-object v1, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;->errorMessage:Ljava/lang/String;

    .line 88
    :cond_0
    return-void
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 80
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 75
    iget v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->level:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->level:I

    .line 76
    return-void
.end method

.method public endPrefixMapping(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 70
    return-void
.end method

.method public ignorableWhitespace([CII)V
    .locals 0
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 66
    return-void
.end method

.method public processingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 61
    return-void
.end method

.method public setDocumentLocator(Lorg/xml/sax/Locator;)V
    .locals 0
    .param p1, "locator"    # Lorg/xml/sax/Locator;

    .prologue
    .line 56
    return-void
.end method

.method public skippedEntity(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 52
    return-void
.end method

.method public startDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 48
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "atts"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 31
    iget v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->level:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->level:I

    .line 32
    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->currentName:Ljava/lang/String;

    .line 33
    const-string/jumbo v0, "WorldTimeResponse"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->val$response:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    const-string/jumbo v1, "Date"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;->date:Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->val$response:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    const-string/jumbo v1, "Time"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;->time:Ljava/lang/String;

    .line 36
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->val$response:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    const-string/jumbo v1, "TimeZone"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;->timezone:Ljava/lang/String;

    .line 38
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;->val$response:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    const-string/jumbo v1, "GmtOffset"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;->offset:F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 26
    return-void
.end method
