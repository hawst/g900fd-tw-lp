.class public Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;
.super Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;
.source "SamsungOpenAppAction.java"


# static fields
.field private static final MSG_WAITING_FOR_COVER_OPENED:I


# instance fields
.field mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field final mHandler:Landroid/os/Handler;

.field private waitingForCoverOpened:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->waitingForCoverOpened:Z

    .line 72
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$3;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$3;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->waitingForCoverOpened:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;
    .param p1, "x1"    # Z

    .prologue
    .line 14
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->waitingForCoverOpened:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->doExecuteLaunchActivity()V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->executeDelay()V

    return-void
.end method

.method private doExecuteLaunchActivity()V
    .locals 3

    .prologue
    .line 59
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$2;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;)V

    const-wide/16 v1, 0x3e8

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 65
    return-void
.end method

.method private executeDelay()V
    .locals 3

    .prologue
    .line 68
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/OpenAppAction;->execute()V

    .line 69
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "com.vlingo.midas"

    const-string/jumbo v2, "OPEN"

    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 23
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 24
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    .line 26
    .local v0, "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    new-instance v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction$1;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;)V

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 44
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    const-string/jumbo v1, "Always"

    const-string/jumbo v2, "maphandler ExcuteAction. CoverClosed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->playCoverUnsupportedTTS()V

    .line 48
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 49
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 50
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 52
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->waitingForCoverOpened:Z

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;->doExecuteLaunchActivity()V

    goto :goto_0
.end method
