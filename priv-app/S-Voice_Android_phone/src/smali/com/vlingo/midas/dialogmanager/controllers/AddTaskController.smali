.class public Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;
.super Lcom/vlingo/core/internal/dialogmanager/StateController;
.source "AddTaskController.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private maps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private rules:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation
.end field

.field private st:Lcom/vlingo/core/internal/schedule/ScheduleTask;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 42
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/StateController;-><init>()V

    .line 43
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->rules:Ljava/util/Map;

    .line 44
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_TASK_TITLE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v1}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->getFieldId()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "taskTitleFieldIdString":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->rules:Ljava/util/Map;

    const-string/jumbo v2, "title"

    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_SAY_TITLE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v3, v0, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->rules:Ljava/util/Map;

    const-string/jumbo v2, "date"

    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    const-string/jumbo v4, ""

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->rules:Ljava/util/Map;

    const-string/jumbo v2, "Confirm"

    new-instance v3, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_task_save_cancel_update_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4, v6}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->maps:Ljava/util/Map;

    .line 50
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->maps:Ljava/util/Map;

    const-string/jumbo v2, "Value"

    const-string/jumbo v3, "title"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    return-void
.end method

.method private save()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 115
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->CREATE_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/AddTaskInterface;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/AddTaskInterface;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->st:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/AddTaskInterface;->task(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/AddTaskInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/AddTaskInterface;->queue()V

    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->st:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    if-eqz v0, :cond_0

    .line 117
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->st:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->st:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBeginDate()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/vlingo/core/internal/recognition/acceptedtext/AddTaskAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 119
    :cond_0
    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 7
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v6, 0x0

    .line 89
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/StateController;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 91
    invoke-interface {p1}, Lcom/vlingo/sdk/recognition/VLAction;->getName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "LPAction"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 111
    :cond_0
    :goto_0
    return v6

    .line 98
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->isAllSlotsFilled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    new-instance v3, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-direct {v3}, Lcom/vlingo/core/internal/schedule/ScheduleTask;-><init>()V

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->st:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 100
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->st:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->rules:Ljava/util/Map;

    const-string/jumbo v5, "title"

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setTitle(Ljava/lang/String;)V

    .line 102
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->rules:Ljava/util/Map;

    const-string/jumbo v4, "date"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 103
    .local v2, "tt":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 104
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/schedule/DateUtil;->getMillisFromString(Ljava/lang/String;Z)J

    move-result-wide v0

    .line 105
    .local v0, "dueDate":J
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->st:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v3, v0, v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->setBegin(J)V

    .line 107
    .end local v0    # "dueDate":J
    :cond_2
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeConfirmButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->st:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-interface {p2, v3, v4, v5, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 108
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->hasConfirm()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->executeConfirm()V

    goto :goto_0
.end method

.method public getRuleMappings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->maps:Ljava/util/Map;

    return-object v0
.end method

.method public getRules()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/dialogmanager/StateController$Rule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->rules:Ljava/util/Map;

    return-object v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 58
    return-void
.end method

.method public handleLPAction(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v1, 0x0

    .line 73
    const-string/jumbo v2, "Action"

    invoke-static {p1, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "actionValue":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 75
    const-string/jumbo v2, "send"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "save"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 76
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/controllers/AddTaskController;->save()V

    .line 77
    const/4 v1, 0x1

    .line 80
    :cond_1
    return v1
.end method
