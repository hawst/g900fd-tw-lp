.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowCreateAlarmHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private alarm:Lcom/vlingo/core/internal/util/Alarm;

.field private listAlarms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    return-void
.end method

.method private setAlarm(Lcom/vlingo/core/internal/util/Alarm;)V
    .locals 2
    .param p1, "alarm"    # Lcom/vlingo/core/internal/util/Alarm;

    .prologue
    .line 73
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;

    invoke-interface {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;->alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/SetAlarmInterface;->queue()V

    .line 76
    if-eqz p1, :cond_0

    .line 77
    new-instance v0, Lcom/vlingo/core/internal/recognition/acceptedtext/AddAlarmAcceptedText;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/util/Alarm;->getTimeCanonical()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/recognition/acceptedtext/AddAlarmAcceptedText;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->sendAcceptedText(Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText;)V

    .line 79
    :cond_0
    return-void
.end method

.method private updateTime(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 127
    const-string/jumbo v2, "time"

    invoke-static {p1, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->extractParamString(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "time":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;

    const-string/jumbo v2, "time"

    invoke-direct {v0, v2, v1}, Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ContentChangedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->queueEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 130
    return-void
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 93
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 94
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 95
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 87
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 88
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 89
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 50
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 52
    const-string/jumbo v3, "confirm"

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v1

    .line 53
    .local v1, "isConfirm":Z
    const-string/jumbo v3, "execute"

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v2

    .line 55
    .local v2, "isExecute":Z
    const/4 v0, 0x0

    .line 56
    .local v0, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    if-nez v1, :cond_0

    if-nez v2, :cond_1

    .line 57
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeConfirmButton()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 60
    :cond_1
    invoke-static {p1}, Lcom/vlingo/midas/samsungutils/utils/SamsungAlarmUtil;->extractAlarm(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/util/Alarm;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    .line 61
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->listAlarms:Ljava/util/List;

    .line 62
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->listAlarms:Ljava/util/List;

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->listAlarms:Ljava/util/List;

    invoke-interface {p2, v3, v0, v4, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 66
    const-string/jumbo v3, "execute"

    invoke-static {p1, v3, v5, v5}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 67
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-direct {p0, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->setAlarm(Lcom/vlingo/core/internal/util/Alarm;)V

    .line 69
    :cond_2
    return v5
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "unused"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->updateTime(Landroid/content/Intent;)V

    .line 112
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;-><init>()V

    .line 113
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 124
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;-><init>()V

    .line 118
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    goto :goto_0

    .line 119
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCancelledEvent;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.UpdateTime"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 120
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->updateTime(Landroid/content/Intent;)V

    goto :goto_0

    .line 122
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method
