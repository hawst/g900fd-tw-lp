.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;
.source "ResolveTaskHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/QueryHandler;-><init>()V

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getTaskByQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getStoredTasks()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;Ljava/util/List;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->sendTaskResolvedEvent(Ljava/util/List;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->addQueriedTasksToStore(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method private addQueriedTasksToStore(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "queriedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    .local p2, "storedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    invoke-interface {p2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TASK_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v0, v1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 65
    return-void
.end method

.method private clearCacheOnRequest(Lcom/vlingo/sdk/recognition/VLAction;)V
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    const/4 v1, 0x0

    .line 35
    const-string/jumbo v0, "clear.cache"

    invoke-static {p1, v0, v1, v1}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TASK_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->storeState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;Ljava/lang/Object;)V

    .line 38
    :cond_0
    return-void
.end method

.method private getStoredTasks()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->TASK_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    move-object v0, v1

    check-cast v0, Ljava/util/List;

    .line 50
    .local v0, "storedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/util/LinkedList;

    .end local v0    # "storedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 54
    .restart local v0    # "storedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    :cond_0
    return-object v0
.end method

.method private getTaskByQuery(Lcom/vlingo/sdk/recognition/VLAction;)Ljava/util/List;
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/sdk/recognition/VLAction;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {p1}, Lcom/vlingo/core/internal/dialogmanager/util/EventUtil;->extractTaskQuery(Lcom/vlingo/sdk/recognition/VLAction;)Lcom/vlingo/core/internal/schedule/TaskQueryObject;

    move-result-object v0

    .line 42
    .local v0, "criteria":Lcom/vlingo/core/internal/schedule/TaskQueryObject;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->getTasks(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/TaskQueryObject;)Ljava/util/List;

    move-result-object v1

    .line 43
    .local v1, "newTasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    return-object v1
.end method

.method private sendTaskResolvedEvent(Ljava/util/List;I)V
    .locals 3
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "queriedTasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, p1, p2, v1}, Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;-><init>(Ljava/util/List;II)V

    .line 59
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 60
    return-void
.end method


# virtual methods
.method public executeQuery(Lcom/vlingo/sdk/recognition/VLAction;)Z
    .locals 3
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->clearCacheOnRequest(Lcom/vlingo/sdk/recognition/VLAction;)V

    .line 29
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler$TaskQueryRunnable;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;Lcom/vlingo/sdk/recognition/VLAction;)V

    const-string/jumbo v2, "TaskQueryRunnable"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 31
    const/4 v0, 0x1

    return v0
.end method

.method protected sendEmptyEvent()V
    .locals 3

    .prologue
    .line 69
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;-><init>()V

    .line 70
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/TaskResolvedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 71
    return-void
.end method
