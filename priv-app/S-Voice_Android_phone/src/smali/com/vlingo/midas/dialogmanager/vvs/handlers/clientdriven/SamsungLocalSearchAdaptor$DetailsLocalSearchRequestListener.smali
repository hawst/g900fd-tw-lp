.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;
.super Ljava/lang/Object;
.source "SamsungLocalSearchAdaptor.java"

# interfaces
.implements Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetailsLocalSearchRequestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;

.field private widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 0
    .param p2, "widgetDetailResponseListener"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 332
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;

    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334
    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 335
    return-void
.end method


# virtual methods
.method public onRequestComplete(ZLjava/lang/Object;)V
    .locals 2
    .param p1, "success"    # Z
    .param p2, "items"    # Ljava/lang/Object;

    .prologue
    .line 342
    instance-of v0, p2, Landroid/util/Pair;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 343
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;

    check-cast p2, Landroid/util/Pair;

    .end local p2    # "items":Ljava/lang/Object;
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->setLocalSearchDetailsListing(Ljava/util/Vector;)V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onResponseReceived()V

    .line 346
    return-void
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;)V
    .locals 2
    .param p1, "failureType"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    .prologue
    .line 355
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$1;->$SwitchMap$com$vlingo$core$internal$localsearch$LocalSearchRequestListener$LocalSearchFailureType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 367
    :goto_0
    return-void

    .line 357
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 360
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 363
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 355
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onRequestFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "failReasonParam"    # Ljava/lang/String;

    .prologue
    .line 350
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    .line 351
    return-void
.end method

.method public onRequestScheduled()V
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor$DetailsLocalSearchRequestListener;->widgetDetailResponseListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestScheduled()V

    .line 372
    return-void
.end method
