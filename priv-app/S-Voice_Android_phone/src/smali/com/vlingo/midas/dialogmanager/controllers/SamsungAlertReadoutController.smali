.class public Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;
.super Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;
.source "SamsungAlertReadoutController.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;-><init>()V

    return-void
.end method


# virtual methods
.method protected displayMultipleSenders()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 21
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->senderList:Ljava/util/LinkedList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 43
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v0

    .line 28
    .local v0, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->senderQueue:Ljava/util/HashMap;

    invoke-static {v4}, Lcom/vlingo/core/internal/util/AlertReadoutUtil;->getTotalMessagesFromSenderQueue(Ljava/util/HashMap;)I

    move-result v3

    .line 30
    .local v3, "totalMessages":I
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "ru-RU"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 31
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->getPhraseNewMessagesRussian(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 36
    .local v2, "spokenMultiSenderText":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_READBACK_SENDERCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v5}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->VP_CAR_READBACK_SENDERCHOOSE:Lcom/vlingo/core/internal/dialogmanager/FieldIds;

    invoke-static {v5}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getFieldId(Lcom/vlingo/core/internal/dialogmanager/FieldIds;)Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->setFieldId(Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 39
    invoke-virtual {p0, v10, v2, v7, v10}, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->showSystemTurn(Ljava/lang/String;Ljava/lang/String;ZLcom/vlingo/core/internal/dialogmanager/DialogFieldID;)V

    .line 41
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v4, v5}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 42
    .local v1, "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v4, v5, v0, v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    goto :goto_0

    .line 33
    .end local v1    # "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    .end local v2    # "spokenMultiSenderText":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->senderList:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/vlingo/midas/dialogmanager/controllers/SamsungAlertReadoutController;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "spokenMultiSenderText":Ljava/lang/String;
    goto :goto_1
.end method
