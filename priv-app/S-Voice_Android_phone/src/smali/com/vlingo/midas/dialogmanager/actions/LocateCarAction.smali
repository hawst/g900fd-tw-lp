.class public Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "LocateCarAction.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->locateCar()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->launchFindMyCar()V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method private launchFindMyCar()V
    .locals 6

    .prologue
    .line 52
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 54
    .local v2, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getCarModePackageName()Ljava/lang/String;

    move-result-object v3

    .line 55
    .local v3, "packageName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".action.VIEW_FIND_MY_CAR"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    const/high16 v4, 0x10210000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 59
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "packageName":Ljava/lang/String;
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v1

    .line 61
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 62
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v4

    const-string/jumbo v5, "locateCar failed"

    invoke-interface {v4, v5}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private locateCar()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method protected execute()V
    .locals 3

    .prologue
    .line 29
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction$1;-><init>(Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;)V

    const-wide/16 v1, 0x3e8

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 43
    return-void
.end method
