.class Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;
.super Ljava/lang/Object;
.source "SamsungSendEmailAction.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 261
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    invoke-static {p2}, Lcom/android/email/backgroundsender/IEmailRemoteService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/email/backgroundsender/IEmailRemoteService;

    move-result-object v2

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackBinder:Lcom/android/email/backgroundsender/IEmailRemoteService;
    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$902(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Lcom/android/email/backgroundsender/IEmailRemoteService;)Lcom/android/email/backgroundsender/IEmailRemoteService;

    .line 262
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    const/4 v2, 0x1

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackConnected:Z
    invoke-static {v1, v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$1002(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Z)Z

    .line 265
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackBinder:Lcom/android/email/backgroundsender/IEmailRemoteService;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$900(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/android/email/backgroundsender/IEmailRemoteService;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mEmailSendCallback:Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;
    invoke-static {v2}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$1100(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/email/backgroundsender/IEmailRemoteService;->registerCallback(Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_0
    return-void

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 277
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackBinder:Lcom/android/email/backgroundsender/IEmailRemoteService;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$900(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/android/email/backgroundsender/IEmailRemoteService;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mEmailSendCallback:Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$1100(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/email/backgroundsender/IEmailRemoteService;->unregisterCallback(Lcom/android/email/backgroundsender/IEmailRemoteServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackBinder:Lcom/android/email/backgroundsender/IEmailRemoteService;
    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$902(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Lcom/android/email/backgroundsender/IEmailRemoteService;)Lcom/android/email/backgroundsender/IEmailRemoteService;

    .line 283
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$3;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->mCallbackConnected:Z
    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$1002(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;Z)Z

    .line 284
    return-void

    .line 278
    :catch_0
    move-exception v0

    goto :goto_0
.end method
