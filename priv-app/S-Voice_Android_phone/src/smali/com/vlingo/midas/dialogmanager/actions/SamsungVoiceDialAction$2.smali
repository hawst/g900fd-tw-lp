.class Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$2;
.super Landroid/os/Handler;
.source "SamsungVoiceDialAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$2;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 103
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$2;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->telephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->access$400(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-nez v0, :cond_0

    .line 105
    const-string/jumbo v0, "always"

    const-string/jumbo v1, "Call failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->callFromSVoice:Z

    .line 108
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->isAppInForeground()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$2;->this$0:Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->context:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.vlingo.midas.ACTION_VOICEWAKEUP_START"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 113
    :cond_0
    return-void
.end method
