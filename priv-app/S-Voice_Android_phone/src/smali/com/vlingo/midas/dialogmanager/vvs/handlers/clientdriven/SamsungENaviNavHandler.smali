.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungENaviNavHandler;
.super Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;
.source "SamsungENaviNavHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 20
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    .line 21
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v4, "location"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    .line 22
    .local v2, "lm":Landroid/location/LocationManager;
    const-string/jumbo v4, "network"

    invoke-virtual {v2, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    .line 24
    .local v1, "isGpsEnable":Z
    if-nez v1, :cond_0

    .line 25
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->core_navigation_default_location:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 26
    .local v3, "prompt":Ljava/lang/String;
    invoke-interface {p2, v3, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const/4 v4, 0x0

    .line 29
    .end local v3    # "prompt":Ljava/lang/String;
    :goto_0
    return v4

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v4

    goto :goto_0
.end method
