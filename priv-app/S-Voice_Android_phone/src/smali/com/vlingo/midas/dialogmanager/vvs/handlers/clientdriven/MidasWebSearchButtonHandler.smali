.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;
.super Lcom/vlingo/core/internal/dialogmanager/WebSearchButtonHandler;
.source "MidasWebSearchButtonHandler.java"


# static fields
.field private static final MSG_WAITING_FOR_COVER_OPENED:I

.field private static final POSSIBLE_RANDOM_TEXTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isExecuted:Z

.field mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field final mHandler:Landroid/os/Handler;

.field msg:Ljava/lang/String;

.field private waitingForCoverOpened:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 90
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    sget v2, Lcom/vlingo/midas/R$string;->unknown_search_1:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/vlingo/midas/R$string;->unknown_search_2:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/vlingo/midas/R$string;->unknown_search_3:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/vlingo/midas/R$string;->unknown_search_4:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/vlingo/midas/R$string;->unknown_search_5:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/vlingo/midas/R$string;->unknown_search_6:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->POSSIBLE_RANDOM_TEXTS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/WebSearchButtonHandler;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->waitingForCoverOpened:Z

    .line 26
    iput-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->isExecuted:Z

    .line 104
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$2;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->waitingForCoverOpened:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->waitingForCoverOpened:Z

    return p1
.end method

.method static synthetic access$102(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->isExecuted:Z

    return p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;
    .param p1, "x1"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "x2"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->doExecuteAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method

.method private doExecuteAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 4
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 118
    const-string/jumbo v2, "true"

    const-string/jumbo v3, "RandomText"

    invoke-interface {p1, v3}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    const-string/jumbo v2, "Query"

    invoke-interface {p1, v2}, Lcom/vlingo/sdk/recognition/VLAction;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "query":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->getRandomAnswer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2, v0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    .end local v0    # "msg":Ljava/lang/String;
    .end local v1    # "query":Ljava/lang/String;
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/WebSearchButtonHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v2

    return v2
.end method

.method private getRandomAnswer(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->POSSIBLE_RANDOM_TEXTS:Ljava/util/List;

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    sget-object v3, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->POSSIBLE_RANDOM_TEXTS:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v1, 0x0

    .line 35
    new-instance v2, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 37
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    .line 38
    .local v0, "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    new-instance v2, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    iput-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 55
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 57
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->sview_cover_alwaysmicon:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 61
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 62
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 63
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x2710

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 65
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->waitingForCoverOpened:Z

    .line 69
    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->doExecuteAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v1

    goto :goto_0
.end method

.method protected getLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->useGoogleSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/WebSearchUtils;->isDefaultGoogleSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->search_label_button:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/WebSearchButtonHandler;->getLabel()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected handleSearch(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->useGoogleSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/WebSearchUtils;->isDefaultGoogleSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    invoke-static {p1}, Lcom/vlingo/core/internal/util/WebSearchUtils;->googleNowSearch(Ljava/lang/String;)V

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/WebSearchButtonHandler;->handleSearch(Ljava/lang/String;)V

    goto :goto_0
.end method
