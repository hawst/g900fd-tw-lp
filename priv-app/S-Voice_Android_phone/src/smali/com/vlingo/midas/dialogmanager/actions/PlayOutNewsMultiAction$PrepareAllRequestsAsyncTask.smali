.class Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;
.super Landroid/os/AsyncTask;
.source "PlayOutNewsMultiAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PrepareAllRequestsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 75
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v5, 0x0

    .line 81
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 82
    .local v0, "appContext":Landroid/content/Context;
    new-instance v1, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->this$0:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->requests:Ljava/util/LinkedList;
    invoke-static {v4}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->access$100(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 83
    .local v1, "clonedRequestsList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/audio/TTSRequest;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/audio/TTSRequest;

    .line 84
    .local v3, "ttsRequest":Lcom/vlingo/core/internal/audio/TTSRequest;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction$PrepareAllRequestsAsyncTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 93
    .end local v3    # "ttsRequest":Lcom/vlingo/core/internal/audio/TTSRequest;
    :goto_1
    return-object v5

    .line 88
    .restart local v3    # "ttsRequest":Lcom/vlingo/core/internal/audio/TTSRequest;
    :cond_0
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/TTSEngine;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/TTSEngine;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/vlingo/core/internal/audio/TTSRequest;->prepareForPlayback(Landroid/content/Context;Lcom/vlingo/core/internal/audio/TTSEngine;)Z

    goto :goto_0

    .line 91
    .end local v3    # "ttsRequest":Lcom/vlingo/core/internal/audio/TTSRequest;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->setTtsCachingOff()V

    goto :goto_1
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 99
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 102
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->setTtsCachingOff()V

    .line 103
    return-void
.end method
