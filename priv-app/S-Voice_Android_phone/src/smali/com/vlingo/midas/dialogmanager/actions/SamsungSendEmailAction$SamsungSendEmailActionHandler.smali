.class Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$SamsungSendEmailActionHandler;
.super Landroid/os/Handler;
.source "SamsungSendEmailAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SamsungSendEmailActionHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 69
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$SamsungSendEmailActionHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 70
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 73
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction$SamsungSendEmailActionHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    .line 74
    .local v0, "o":Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;
    if-eqz v0, :cond_0

    .line 75
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 92
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 79
    :pswitch_0
    iget v1, p1, Landroid/os/Message;->arg1:I

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->jobIDByQuickPannel:I
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$000()I

    move-result v2

    if-eq v1, v2, :cond_1

    iget v1, p1, Landroid/os/Message;->arg1:I

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->jobIDByGallery:I
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$100()I

    move-result v2

    if-eq v1, v2, :cond_1

    iget v1, p1, Landroid/os/Message;->arg1:I

    # getter for: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->jobIDByViceQuick:I
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$200()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 83
    :cond_1
    iget v1, p1, Landroid/os/Message;->arg2:I

    if-ne v1, v3, :cond_2

    .line 84
    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->getContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$300(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->msg_sent_successful:I

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 86
    :cond_2
    iget v1, p1, Landroid/os/Message;->arg2:I

    if-nez v1, :cond_0

    .line 87
    # invokes: Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->getContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;->access$400(Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;)Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->msg_sent_fail:I

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
