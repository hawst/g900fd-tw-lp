.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowMessagesWidgetHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowMessagesWidgetHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 32
    sget-object v4, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {p2, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/Controller;

    .line 33
    .local v1, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    const-class v4, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowMessagesWidgetHandler;->getController(Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/Controller;

    move-result-object v1

    .line 34
    const/16 v2, 0x3c

    .line 35
    .local v2, "maxToReturn":I
    invoke-static {}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getInstance()Lcom/vlingo/core/internal/messages/SMSMMSProvider;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->getAllNewAlerts(I)Ljava/util/LinkedList;

    move-result-object v0

    .local v0, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    move-object v3, v1

    .line 41
    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;

    .line 42
    .local v3, "messagesController":Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;
    invoke-virtual {v3, v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/MessagesController;->setMessageAlertQueue(Ljava/util/LinkedList;)V

    .line 43
    invoke-virtual {v1, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/Controller;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v4

    return v4
.end method
