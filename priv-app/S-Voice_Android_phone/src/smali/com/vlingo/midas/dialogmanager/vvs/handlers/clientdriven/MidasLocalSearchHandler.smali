.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;
.source "MidasLocalSearchHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field public context:Landroid/content/Context;

.field public samsungAdaptor:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;-><init>()V

    .line 40
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;

    invoke-direct {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->samsungAdaptor:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;

    .line 44
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->samsungAdaptor:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 45
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->samsungAdaptor:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLocalSearchAdaptor;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->setAdaptor(Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;)V

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .param p2, "x2"    # Ljava/lang/Class;

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->unified()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionBase$UnifiedPrompter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/DMActionType;
    .param p2, "x2"    # Ljava/lang/Class;

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 1
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "actionHandlerListener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    .line 49
    invoke-interface {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->context:Landroid/content/Context;

    .line 50
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v0

    return v0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 87
    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 89
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;-><init>()V

    .line 90
    .local v0, "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 91
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getKeyTerms()Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "search":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 93
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getAdaptor()Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCurrentSearchQuery()Ljava/lang/String;

    move-result-object v1

    .line 95
    :cond_0
    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->getWebSearchURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "url":Ljava/lang/String;
    invoke-virtual {v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->executeSearchIntentFromURL(Ljava/lang/String;)V

    .line 101
    .end local v0    # "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
    .end local v1    # "search":Ljava/lang/String;
    .end local v2    # "url":Ljava/lang/String;
    :goto_0
    return-void

    .line 99
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/LocalSearchHandler;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected noData()V
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 82
    return-void
.end method

.method protected showFailure()V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler$2;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 145
    return-void
.end method
