.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser;
.super Ljava/lang/Object;
.source "WorldTimeResponseParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;
    .locals 2
    .param p0, "is"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 19
    new-instance v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    invoke-direct {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;-><init>()V

    .line 20
    .local v0, "response":Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;
    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;

    invoke-direct {v1, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;)V

    invoke-static {p0, v1}, Landroid/util/Xml;->parse(Ljava/lang/String;Lorg/xml/sax/ContentHandler;)V

    .line 90
    return-object v0
.end method
