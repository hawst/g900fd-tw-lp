.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;
.super Ljava/lang/Object;
.source "WorldTimeAdaptor.java"

# interfaces
.implements Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor$1;
    }
.end annotation


# instance fields
.field private location:Ljava/lang/String;

.field private lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

.field private response:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

.field private widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-direct {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    return-void
.end method


# virtual methods
.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getResult()Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->response:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    return-object v0
.end method

.method public onRequestComplete(ZLjava/lang/Object;)V
    .locals 2
    .param p1, "success"    # Z
    .param p2, "items"    # Ljava/lang/Object;

    .prologue
    .line 22
    :try_start_0
    check-cast p2, Ljava/lang/String;

    .end local p2    # "items":Ljava/lang/Object;
    invoke-static {p2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponseParser;->parse(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->response:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeResponse;

    .line 23
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onResponseReceived()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1

    .line 29
    :goto_0
    return-void

    .line 24
    :catch_0
    move-exception v0

    .line 25
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 26
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 27
    .local v0, "e":Lorg/xml/sax/SAXException;
    invoke-virtual {v0}, Lorg/xml/sax/SAXException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->onRequestFailed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;)V
    .locals 2
    .param p1, "failureType"    # Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;

    .prologue
    .line 38
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor$1;->$SwitchMap$com$vlingo$core$internal$localsearch$LocalSearchRequestListener$LocalSearchFailureType:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener$LocalSearchFailureType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 50
    :goto_0
    return-void

    .line 40
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 43
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 46
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V

    goto :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onRequestFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "failReason"    # Ljava/lang/String;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestFailed()V

    .line 34
    return-void
.end method

.method public onRequestScheduled()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    invoke-interface {v0}, Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;->onRequestScheduled()V

    .line 55
    return-void
.end method

.method public sendWorldTimeRequest(Ljava/lang/String;)V
    .locals 1
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->location:Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->lsManager:Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;

    invoke-virtual {v0, p1, p0}, Lcom/vlingo/core/internal/localsearch/LocalSearchServiceManager;->sendWorldTimeRequest(Ljava/lang/String;Lcom/vlingo/core/internal/localsearch/LocalSearchRequestListener;)V

    .line 60
    return-void
.end method

.method public setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V
    .locals 0
    .param p1, "widgetListener"    # Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/localsearch/WorldTimeAdaptor;->widgetListener:Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;

    .line 68
    return-void
.end method
