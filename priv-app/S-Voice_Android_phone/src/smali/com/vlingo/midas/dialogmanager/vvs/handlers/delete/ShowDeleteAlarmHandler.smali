.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ShowDeleteAlarmHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# instance fields
.field private alarm:Lcom/vlingo/core/internal/util/Alarm;

.field private listAlarms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    return-void
.end method

.method private deleteAlarm()V
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteAlarmInterface;

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteAlarmInterface;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteAlarmInterface;->alarm(Lcom/vlingo/core/internal/util/Alarm;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteAlarmInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/DeleteAlarmInterface;->queue()V

    .line 75
    return-void
.end method

.method private getAlarm(I)Lcom/vlingo/core/internal/util/Alarm;
    .locals 4
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ALARM_MATCHES:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 59
    .local v0, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    if-nez v0, :cond_0

    const/4 v2, 0x0

    .line 61
    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/util/Alarm;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v1

    .line 67
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    new-instance v2, Ljava/security/InvalidParameterException;

    const-string/jumbo v3, "Alarm index passed in is not in the cache"

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 89
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;

    invoke-direct {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;-><init>(Ljava/lang/String;)V

    .line 90
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionFailedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 91
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 83
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;-><init>()V

    .line 84
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionCompletedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 85
    return-void
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 5
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 41
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 42
    const-string/jumbo v1, "id"

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-static {p1, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamInt(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;IZ)I

    move-result v0

    .line 43
    .local v0, "index":I
    invoke-direct {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->getAlarm(I)Lcom/vlingo/core/internal/util/Alarm;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    .line 44
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    if-nez v1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v4

    .line 45
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->listAlarms:Ljava/util/List;

    .line 46
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->listAlarms:Ljava/util/List;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->alarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->listAlarms:Ljava/util/List;

    invoke-interface {p2, v1, v2, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 50
    const-string/jumbo v1, "execute"

    invoke-static {p1, v1, v4, v4}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamBool(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->deleteAlarm()V

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 101
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;-><init>()V

    .line 102
    .local v0, "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->turn:Lcom/vlingo/core/internal/dialogmanager/DialogTurn;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->sendEvent(Lcom/vlingo/core/internal/dialogmanager/DialogEvent;Lcom/vlingo/core/internal/dialogmanager/DialogTurn;)V

    .line 110
    .end local v0    # "event":Lcom/vlingo/core/internal/dialogmanager/events/ActionConfirmedEvent;
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 108
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;->throwUnknownActionException(Ljava/lang/String;)V

    goto :goto_0
.end method
