.class public Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;
.super Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;
.source "SamsungVoiceDialAction.java"


# static fields
.field private static final LOG:Lcom/vlingo/core/internal/logging/Logger;

.field private static final MSG_CHECK_CALL_SUCCESS:I


# instance fields
.field private address:Ljava/lang/String;

.field context:Landroid/content/Context;

.field final mHandler:Landroid/os/Handler;

.field private telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->LOG:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;-><init>()V

    .line 34
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->context:Landroid/content/Context;

    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->context:Landroid/content/Context;

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 100
    new-instance v0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$2;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)V

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->address:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->telephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method


# virtual methods
.method public address(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/VoiceDialAction;
    .locals 0
    .param p1, "newAddress"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->address:Ljava/lang/String;

    .line 41
    return-object p0
.end method

.method protected execute()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 46
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->address:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 49
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->clearPendingSafeReaderTurn()V

    .line 50
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    .line 51
    .local v0, "bluetoothOn":Z
    if-eqz v0, :cond_0

    .line 52
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->setAbandonInSync(Z)V

    .line 57
    new-instance v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;

    invoke-direct {v1, p0, v0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction$1;-><init>(Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;Z)V

    const-wide/16 v2, 0x258

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 93
    .end local v0    # "bluetoothOn":Z
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 94
    sput-boolean v4, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->callFromSVoice:Z

    .line 95
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    const-wide/16 v3, 0xfa0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 97
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->context:Landroid/content/Context;

    const-string/jumbo v2, "com.vlingo.midas"

    const-string/jumbo v3, "CALL"

    invoke-static {v1, v2, v3}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    return-void

    .line 90
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V

    goto :goto_0
.end method
