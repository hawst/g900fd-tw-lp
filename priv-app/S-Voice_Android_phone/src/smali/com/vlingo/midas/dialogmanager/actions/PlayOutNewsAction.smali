.class public Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "PlayOutNewsAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$1;,
        Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;
    }
.end annotation


# instance fields
.field private playbackListener:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;

.field private tts:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    .line 34
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;)Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected execute()V
    .locals 3

    .prologue
    .line 26
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->tts:Ljava/lang/String;

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/TTSRequest;->getResult(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/TTSRequest;

    move-result-object v0

    .line 27
    .local v0, "request":Lcom/vlingo/core/internal/audio/TTSRequest;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/TTSRequest;->setFlag(I)V

    .line 28
    new-instance v1, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;-><init>(Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$1;)V

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->playbackListener:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;

    .line 31
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->playbackListener:Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction$PlayOutNewsAudioPlaybackListener;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->play(Lcom/vlingo/core/internal/audio/TTSRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 32
    return-void
.end method

.method public tts(Ljava/lang/String;)V
    .locals 0
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;->tts:Ljava/lang/String;

    .line 20
    return-void
.end method
