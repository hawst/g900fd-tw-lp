.class public Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;
.super Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;
.source "ChatbotWidgetHandler.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
.implements Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$3;,
        Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;
    }
.end annotation


# instance fields
.field private final ACTION_TYPE:Ljava/lang/String;

.field private final XML_PREFIX:Ljava/lang/String;

.field private errorPrompt:Ljava/lang/String;

.field private final handler:Landroid/os/Handler;

.field public isPrevOrNextBeforeRead:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

.field mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

.field private final newsManager:Lcom/vlingo/midas/news/NewsManager;

.field private prompt:Ljava/lang/String;

.field private xmlResponse:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 75
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;-><init>()V

    .line 72
    invoke-static {}, Lcom/vlingo/midas/news/NewsManager;->getInstance()Lcom/vlingo/midas/news/NewsManager;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->newsManager:Lcom/vlingo/midas/news/NewsManager;

    .line 73
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->handler:Landroid/os/Handler;

    .line 87
    const-string/jumbo v1, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->XML_PREFIX:Ljava/lang/String;

    .line 88
    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->xmlResponse:Ljava/lang/String;

    .line 89
    const-string/jumbo v1, "action_type"

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->ACTION_TYPE:Ljava/lang/String;

    .line 90
    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    .line 91
    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    .line 324
    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$1;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)V

    iput-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    .line 76
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->newsManager:Lcom/vlingo/midas/news/NewsManager;

    invoke-virtual {v1, p0}, Lcom/vlingo/midas/news/NewsManager;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 77
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 79
    .local v0, "df":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getWidgetSpecificProperties(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_0

    .line 83
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v2, "embeddedNews"

    const-string/jumbo v3, "true"

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->addWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/midas/news/NewsManager;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->newsManager:Lcom/vlingo/midas/news/NewsManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->playOutNews(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    return-object v0
.end method

.method private doSendEmergencyLocation()Z
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v11, 0x0

    .line 122
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getEmergencyContactList()Ljava/util/List;

    move-result-object v1

    .line 123
    .local v1, "emergencyPhoneNumberList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 124
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$string;->emergency_phone_not_found:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "errMessage":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    .end local v2    # "errMessage":Ljava/lang/String;
    :goto_0
    return v11

    .line 129
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLat()D

    move-result-wide v3

    .line 130
    .local v3, "lat":D
    invoke-static {}, Lcom/vlingo/core/internal/location/LocationUtils;->getLastLong()D

    move-result-wide v5

    .line 131
    .local v5, "lng":D
    cmpl-double v9, v12, v3

    if-nez v9, :cond_2

    cmpl-double v9, v12, v5

    if-nez v9, :cond_2

    .line 132
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_not_detected_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v9, v10}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "answer":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9, v0, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 136
    .end local v0    # "answer":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$string;->send_current_location:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 137
    .restart local v0    # "answer":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 138
    .local v8, "msgBuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "http://maps.google.com/maps?f=q&q=("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v5, v6}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 143
    .local v7, "messageToSend":Ljava/lang/String;
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v10, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    invoke-virtual {v9, v1}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->addresses(Ljava/util/List;)Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->message(Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/actions/SendMessageAction;->queue()V

    goto :goto_0
.end method

.method private getEmergencyContactList()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 175
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 176
    .local v8, "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string/jumbo v0, "content://com.android.contacts/emergency"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 177
    .local v1, "uri":Landroid/net/Uri;
    const-string/jumbo v3, "default_emergency = 2 OR default_emergency = 3"

    .line 178
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "number"

    aput-object v0, v2, v4

    .line 179
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 181
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 183
    if-eqz v6, :cond_1

    .line 184
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 188
    :catch_0
    move-exception v7

    .line 189
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    if-eqz v6, :cond_0

    .line 192
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 196
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    return-object v8

    .line 191
    :cond_1
    if-eqz v6, :cond_0

    .line 192
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 191
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 192
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getEmergencyPhoneNumber()Ljava/lang/String;
    .locals 10

    .prologue
    .line 152
    :try_start_0
    const-string/jumbo v6, "com.sec.android.emergencymode.EmergencyConstants"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 153
    .local v2, "emergencyConstantsClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v6, "EMCALL"

    invoke-virtual {v2, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 154
    .local v1, "eMCALL":Ljava/lang/reflect/Field;
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 155
    .local v5, "type":Ljava/lang/String;
    const-string/jumbo v6, "com.sec.android.emergencymode.EmergencySettings"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 156
    .local v3, "emergencySettings":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v6, "getEmergencyNumber"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/ContentResolver;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v3, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 157
    .local v4, "getEmergencyNumber":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v5, v7, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    .line 171
    .end local v1    # "eMCALL":Ljava/lang/reflect/Field;
    .end local v2    # "emergencyConstantsClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "emergencySettings":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "getEmergencyNumber":Ljava/lang/reflect/Method;
    .end local v5    # "type":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 171
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :goto_1
    const-string/jumbo v6, ""

    goto :goto_0

    .line 160
    :catch_1
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_1

    .line 162
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_2
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 164
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 166
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v0

    .line 167
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 168
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_5
    move-exception v0

    .line 169
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method private getNewsCommand(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;
    .locals 1
    .param p1, "xmlFragment"    # Ljava/lang/String;

    .prologue
    .line 441
    if-eqz p1, :cond_b

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 442
    const-string/jumbo v0, "news:read"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Read:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 467
    :goto_0
    return-object v0

    .line 444
    :cond_0
    const-string/jumbo v0, "news:previous"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 445
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Prev:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto :goto_0

    .line 446
    :cond_1
    const-string/jumbo v0, "news:next"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 447
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Next:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto :goto_0

    .line 448
    :cond_2
    const-string/jumbo v0, "news:stop"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "news:pause"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 449
    :cond_3
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Stop:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto :goto_0

    .line 450
    :cond_4
    const-string/jumbo v0, "news:up"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 451
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Up:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto :goto_0

    .line 452
    :cond_5
    const-string/jumbo v0, "news:down"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 453
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Down:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto :goto_0

    .line 454
    :cond_6
    const-string/jumbo v0, "news:repeat"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 455
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Repeat:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto :goto_0

    .line 456
    :cond_7
    const-string/jumbo v0, "news:reset"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 457
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Reset:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto :goto_0

    .line 458
    :cond_8
    const-string/jumbo v0, "news:restart"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 459
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Restart:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto :goto_0

    .line 460
    :cond_9
    const-string/jumbo v0, "news:resume"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 461
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Resume:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto :goto_0

    .line 462
    :cond_a
    const-string/jumbo v0, "news:pause"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 463
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Pause:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto/16 :goto_0

    .line 467
    :cond_b
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->None:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    goto/16 :goto_0
.end method

.method private isNewsActionType(Ljava/lang/String;)Z
    .locals 1
    .param p1, "xmlFragment"    # Ljava/lang/String;

    .prologue
    .line 406
    invoke-direct {p0, p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->isNewsActionTypeFaked(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isNewsActionTypeFaked(Ljava/lang/String;)Z
    .locals 1
    .param p1, "xmlFragment"    # Ljava/lang/String;

    .prologue
    .line 410
    if-eqz p1, :cond_0

    const-string/jumbo v0, "<custom_action><action_type>news:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    const/4 v0, 0x1

    .line 412
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNewsActionTypeFull(Ljava/lang/String;)Z
    .locals 10
    .param p1, "xmlFragment"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 471
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 472
    .local v4, "fullXml":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v3

    .line 473
    .local v3, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    const/4 v0, 0x0

    .line 474
    .local v0, "builder":Ljavax/xml/parsers/DocumentBuilder;
    const/4 v1, 0x0

    .line 476
    .local v1, "doc":Lorg/w3c/dom/Document;
    :try_start_0
    invoke-virtual {v3}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 477
    invoke-virtual {v0, v4}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/lang/String;)Lorg/w3c/dom/Document;
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    .line 494
    invoke-interface {v1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v6

    .line 495
    .local v6, "root":Lorg/w3c/dom/Element;
    const-string/jumbo v7, "action_type"

    invoke-interface {v6, v7}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 498
    .local v5, "items":Lorg/w3c/dom/NodeList;
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    if-lez v7, :cond_0

    .line 499
    invoke-interface {v5, v9}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    .line 506
    .end local v5    # "items":Lorg/w3c/dom/NodeList;
    .end local v6    # "root":Lorg/w3c/dom/Element;
    :cond_0
    :goto_0
    return v9

    .line 479
    :catch_0
    move-exception v2

    .line 480
    .local v2, "e":Ljavax/xml/parsers/ParserConfigurationException;
    invoke-virtual {v2}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    goto :goto_0

    .line 482
    .end local v2    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_1
    move-exception v2

    .line 483
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 485
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v2

    .line 486
    .local v2, "e":Lorg/xml/sax/SAXException;
    invoke-virtual {v2}, Lorg/xml/sax/SAXException;->printStackTrace()V

    goto :goto_0

    .line 488
    .end local v2    # "e":Lorg/xml/sax/SAXException;
    :catch_3
    move-exception v2

    .line 489
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private isSendEmergencyLocationActionType(Ljava/lang/String;)Z
    .locals 1
    .param p1, "xmlResponse"    # Ljava/lang/String;

    .prologue
    .line 390
    const-string/jumbo v0, "<custom_action><action_type></action_type><system_response></system_response></custom_action>"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private playOutNews(Ljava/lang/String;)V
    .locals 4
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 369
    const/4 v1, 0x0

    .line 370
    .local v1, "ttsStrings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 371
    const-string/jumbo v2, "\u00ad"

    const-string/jumbo v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 372
    invoke-static {p1}, Lcom/vlingo/midas/tss/TtsUtil;->getTtsArray(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 374
    :cond_0
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_OUT_NEWS_MULTI:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getAction(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ActionInterface;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    .line 375
    .local v0, "action":Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 376
    invoke-virtual {v0, v1}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->tts(Ljava/util/List;)V

    .line 377
    invoke-virtual {v0}, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;->queue()V

    .line 387
    :goto_0
    return-void

    .line 385
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    goto :goto_0
.end method


# virtual methods
.method public actionFail(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 526
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 527
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionFail(Ljava/lang/String;)V

    .line 528
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->xmlResponse:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->isSendEmergencyLocationActionType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->msg_sent_fail:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    .line 530
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    :cond_0
    return-void
.end method

.method public actionSuccess()V
    .locals 3

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 512
    invoke-super {p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->actionSuccess()V

    .line 513
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->xmlResponse:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->isSendEmergencyLocationActionType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 514
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->msg_sent_successful:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    .line 515
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    :goto_0
    return-void

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->newsManager:Lcom/vlingo/midas/news/NewsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/news/NewsManager;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    goto :goto_0
.end method

.method public doNewsCommand(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;)Z
    .locals 14
    .param p1, "command"    # Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .prologue
    const/4 v13, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 201
    invoke-static {}, Lcom/vlingo/midas/news/NewsManager;->getInstance()Lcom/vlingo/midas/news/NewsManager;

    move-result-object v5

    .line 202
    .local v5, "nm":Lcom/vlingo/midas/news/NewsManager;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v4

    .line 203
    .local v4, "listener":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    invoke-interface {v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v0

    .line 204
    .local v0, "context":Landroid/content/Context;
    const/4 v3, 0x0

    .line 206
    .local v3, "item":Lcom/vlingo/midas/news/NewsItem;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 207
    .local v6, "resources":Landroid/content/res/Resources;
    sget v10, Lcom/vlingo/midas/R$string;->chatbot_could_not_load_news:I

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    .line 209
    const-string/jumbo v10, "com.vlingo.midas"

    const-string/jumbo v11, "NEWS"

    invoke-static {v0, v10, v11}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->isAvailable()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 212
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->hasCachedNews()Z

    move-result v10

    if-nez v10, :cond_1

    .line 213
    sget-object v10, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Next:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    if-eq p1, v10, :cond_0

    sget-object v10, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Prev:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    if-ne p1, v10, :cond_1

    .line 214
    :cond_0
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->isPrevOrNextBeforeRead:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 215
    sget-object p1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Read:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    .line 218
    :cond_1
    sget-object v10, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$3;->$SwitchMap$com$vlingo$midas$dialogmanager$vvs$handlers$ChatbotWidgetHandler$NewsCommand:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 294
    :cond_2
    :goto_0
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/vlingo/midas/news/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_8

    invoke-virtual {v3}, Lcom/vlingo/midas/news/NewsItem;->getText()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_8

    .line 295
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 296
    iget-object v8, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    invoke-interface {v4, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 299
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v8

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v8, v10, v13, v3, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 301
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 302
    invoke-virtual {v3}, Lcom/vlingo/midas/news/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->mAudioPlaybackListener:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;

    invoke-interface {v4, v8, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->ttsAnyway(Ljava/lang/String;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 305
    :cond_4
    invoke-virtual {v3}, Lcom/vlingo/midas/news/NewsItem;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->playOutNews(Ljava/lang/String;)V

    move v8, v9

    .line 321
    :goto_1
    return v8

    .line 220
    :pswitch_0
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->getCurrentNews()Lcom/vlingo/midas/news/NewsItem;

    move-result-object v3

    .line 222
    iput-object v13, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    .line 223
    if-eqz v3, :cond_2

    .line 224
    move-object v7, v6

    .line 225
    .local v7, "rsc":Landroid/content/res/Resources;
    invoke-virtual {v3}, Lcom/vlingo/midas/news/NewsItem;->getNewsCP()I

    move-result v10

    if-ne v10, v9, :cond_5

    .line 226
    sget v10, Lcom/vlingo/midas/R$string;->chatbot_read_news:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    new-array v11, v9, [Ljava/lang/Object;

    sget v12, Lcom/vlingo/midas/R$string;->newscp_flipboard:I

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    goto :goto_0

    .line 228
    :cond_5
    sget v10, Lcom/vlingo/midas/R$string;->chatbot_read_news:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    new-array v11, v9, [Ljava/lang/Object;

    sget v12, Lcom/vlingo/midas/R$string;->newscp_yonhap:I

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    goto :goto_0

    .line 233
    .end local v7    # "rsc":Landroid/content/res/Resources;
    :pswitch_1
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->getNextNews()Lcom/vlingo/midas/news/NewsItem;

    move-result-object v3

    .line 234
    sget v10, Lcom/vlingo/midas/R$string;->chatbot_next_news:I

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    .line 235
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->hasCachedNews()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 236
    sget v10, Lcom/vlingo/midas/R$string;->chatbot_just_read_last_news:I

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    goto/16 :goto_0

    .line 241
    :pswitch_2
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->getPrevNews()Lcom/vlingo/midas/news/NewsItem;

    move-result-object v3

    .line 242
    sget v10, Lcom/vlingo/midas/R$string;->chatbot_prev_news:I

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    .line 243
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->hasCachedNews()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 244
    sget v10, Lcom/vlingo/midas/R$string;->chatbot_just_read_first_news:I

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    goto/16 :goto_0

    .line 250
    :pswitch_3
    sget v9, Lcom/vlingo/midas/R$string;->chatbot_stop_news:I

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    .line 251
    iget-object v9, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    iget-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    invoke-interface {v4, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    .line 256
    .local v1, "df":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    sget-object v9, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1, v9}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V

    goto/16 :goto_1

    .line 260
    .end local v1    # "df":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    :pswitch_4
    const/4 v3, 0x0

    .line 261
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->incrementCurrentStreamVolume()Z

    move-result v10

    if-nez v10, :cond_6

    .line 262
    sget v10, Lcom/vlingo/midas/R$string;->volume_at_maximum:I

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    goto/16 :goto_0

    .line 266
    :cond_6
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->getCurrentNews()Lcom/vlingo/midas/news/NewsItem;

    move-result-object v3

    .line 267
    goto/16 :goto_0

    .line 270
    :pswitch_5
    const/4 v3, 0x0

    .line 271
    invoke-static {}, Lcom/vlingo/core/internal/util/PhoneUtil;->decrementCurrentStreamVolume()Z

    move-result v10

    if-nez v10, :cond_7

    .line 272
    sget v10, Lcom/vlingo/midas/R$string;->volume_at_minimum:I

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    goto/16 :goto_0

    .line 276
    :cond_7
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->getCurrentNews()Lcom/vlingo/midas/news/NewsItem;

    move-result-object v3

    .line 277
    goto/16 :goto_0

    .line 280
    :pswitch_6
    sget v10, Lcom/vlingo/midas/R$string;->chatbot_first_news:I

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    .line 281
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->getFirstNews()Lcom/vlingo/midas/news/NewsItem;

    move-result-object v3

    .line 282
    goto/16 :goto_0

    .line 288
    :pswitch_7
    sget v10, Lcom/vlingo/midas/R$string;->chatbot_repeat_news:I

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->prompt:Ljava/lang/String;

    .line 289
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->getCurrentNews()Lcom/vlingo/midas/news/NewsItem;

    move-result-object v3

    goto/16 :goto_0

    .line 308
    :cond_8
    if-nez v3, :cond_9

    iget-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    if-eqz v10, :cond_9

    .line 309
    iget-object v9, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    iget-object v10, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    invoke-interface {v4, v9, v10}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 312
    :cond_9
    invoke-virtual {v5}, Lcom/vlingo/midas/news/NewsManager;->fetchNews()Z

    move v8, v9

    .line 313
    goto/16 :goto_1

    .line 317
    :cond_a
    sget v9, Lcom/vlingo/midas/R$string;->newscp_noflipboard:I

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 318
    .local v2, "errorStr":Ljava/lang/String;
    invoke-interface {v4, v2, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    .locals 6
    .param p1, "action"    # Lcom/vlingo/sdk/recognition/VLAction;
    .param p2, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-super {p0, p1, p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    .line 98
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v3

    const-string/jumbo v4, "chatbot"

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 100
    const-string/jumbo v3, "xmlresponse"

    invoke-static {p1, v3, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->xmlResponse:Ljava/lang/String;

    .line 104
    const-string/jumbo v3, "VoiceNews"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "xmlresponse=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->xmlResponse:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const-string/jumbo v3, "Type"

    invoke-static {p1, v3, v2}, Lcom/vlingo/core/internal/dialogmanager/util/VLActionUtil;->getParamString(Lcom/vlingo/sdk/recognition/VLAction;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 106
    .local v1, "type":Ljava/lang/String;
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->xmlResponse:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->isSendEmergencyLocationActionType(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 107
    invoke-direct {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->doSendEmergencyLocation()Z

    move-result v2

    .line 118
    :cond_0
    :goto_0
    return v2

    .line 109
    :cond_1
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isBlank(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 110
    invoke-direct {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getNewsCommand(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    move-result-object v0

    .line 111
    .local v0, "command":Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->doNewsCommand(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;)Z

    move-result v2

    goto :goto_0

    .line 113
    .end local v0    # "command":Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;
    :cond_2
    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->xmlResponse:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->isNewsActionType(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->xmlResponse:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getNewsCommand(Ljava/lang/String;)Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    move-result-object v0

    .line 115
    .restart local v0    # "command":Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->doNewsCommand(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;)Z

    move-result v2

    goto :goto_0
.end method

.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "unused"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 615
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 616
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.VolumeUp"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 617
    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Up:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->doNewsCommand(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;)Z

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 619
    :cond_1
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.VolumeDown"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 620
    sget-object v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;->Down:Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->doNewsCommand(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$NewsCommand;)Z

    goto :goto_0
.end method

.method public onRequestFailed()V
    .locals 3

    .prologue
    .line 542
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->newsManager:Lcom/vlingo/midas/news/NewsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/news/NewsManager;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 543
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->errorPrompt:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 545
    return-void
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 0
    .param p1, "prompt"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 629
    return-void
.end method

.method public onRequestScheduled()V
    .locals 1

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->getListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->asyncHandlerDone()V

    .line 550
    return-void
.end method

.method public onResponseReceived()V
    .locals 2

    .prologue
    .line 554
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->newsManager:Lcom/vlingo/midas/news/NewsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/news/NewsManager;->setWidgetListener(Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 555
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler$2;-><init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 609
    return-void
.end method

.method protected resetEmbeddedNews()V
    .locals 0

    .prologue
    .line 634
    return-void
.end method
