.class public Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;
.super Lcom/vlingo/core/internal/dialogmanager/DMAction;
.source "ModifyTaskAction.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyTaskInterface;


# instance fields
.field private modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

.field private origTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/DMAction;-><init>()V

    return-void
.end method


# virtual methods
.method protected execute()V
    .locals 4

    .prologue
    .line 37
    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    if-eqz v1, :cond_0

    .line 39
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->origTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->updateTask(Landroid/content/Context;Lcom/vlingo/core/internal/schedule/ScheduleTask;Lcom/vlingo/core/internal/schedule/ScheduleTask;)V

    .line 40
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionSuccess()V
    :try_end_0
    .catch Lcom/vlingo/core/internal/schedule/ScheduleUtilException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to modify task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/core/internal/schedule/ScheduleUtilException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    .end local v0    # "e":Lcom/vlingo/core/internal/schedule/ScheduleUtilException;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->getListener()Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;

    move-result-object v1

    const-string/jumbo v2, "No task to update"

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DMAction$Listener;->actionFail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic modified(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyTaskInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->modified(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;

    move-result-object v0

    return-object v0
.end method

.method public modified(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;
    .locals 0
    .param p1, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->modifiedTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 32
    return-object p0
.end method

.method public bridge synthetic original(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/core/internal/dialogmanager/actions/interfaces/ModifyTaskInterface;
    .locals 1
    .param p1, "x0"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->original(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;

    move-result-object v0

    return-object v0
.end method

.method public original(Lcom/vlingo/core/internal/schedule/ScheduleTask;)Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;
    .locals 0
    .param p1, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;->origTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 26
    return-object p0
.end method
