.class Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "MidasWebSearchButtonHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->executeAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

.field final synthetic val$action:Lcom/vlingo/sdk/recognition/VLAction;

.field final synthetic val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

    iput-object p2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->val$action:Lcom/vlingo/sdk/recognition/VLAction;

    iput-object p3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 5
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    const/4 v4, 0x0

    .line 40
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 41
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

    # getter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->waitingForCoverOpened:Z
    invoke-static {v0}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->access$000(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    const-string/jumbo v0, "always"

    const-string/jumbo v1, "mCoverStateListener cover Opened"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 44
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

    iget-object v0, v0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

    iget-object v1, v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 45
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

    iget-object v1, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

    iget-object v2, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->val$action:Lcom/vlingo/sdk/recognition/VLAction;

    iget-object v3, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->val$listener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    # invokes: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->doExecuteAction(Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z
    invoke-static {v1, v2, v3}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->access$200(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;Lcom/vlingo/sdk/recognition/VLAction;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Z

    move-result v1

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->isExecuted:Z
    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->access$102(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;Z)Z

    .line 46
    iget-object v0, p0, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler$1;->this$0:Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

    # setter for: Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->waitingForCoverOpened:Z
    invoke-static {v0, v4}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;->access$002(Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;Z)Z

    .line 53
    :goto_0
    return-void

    .line 48
    :cond_0
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "Cover opened, waitingForCoverOpened is false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 51
    :cond_1
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "mCoverStateListener cover Closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
