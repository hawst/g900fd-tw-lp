.class public abstract Lcom/vlingo/midas/ui/SimpleListScreen;
.super Landroid/app/ListActivity;
.source "SimpleListScreen.java"


# static fields
.field public static final EXTRA_LIST_PARAM:Ljava/lang/String; = "listitemparam"


# instance fields
.field protected m_adapter:Landroid/widget/SimpleAdapter;

.field private m_forwardToNextActivity:Z

.field protected m_list:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field protected m_targets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/ui/SimpleListScreen;->m_list:Ljava/util/ArrayList;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/ui/SimpleListScreen;->m_targets:Ljava/util/ArrayList;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/ui/SimpleListScreen;->m_forwardToNextActivity:Z

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/ui/SimpleListScreen;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/ui/SimpleListScreen;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/vlingo/midas/ui/SimpleListScreen;->m_forwardToNextActivity:Z

    return v0
.end method

.method private addListItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p4, "listItemParam"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    .local p3, "activityToStart":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 89
    .local v0, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v2, "line1"

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    const-string/jumbo v2, "line2"

    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v2, p0, Lcom/vlingo/midas/ui/SimpleListScreen;->m_list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 96
    .local v1, "target":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v2, "activity"

    invoke-virtual {v1, v2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    const-string/jumbo v2, "url"

    invoke-virtual {v1, v2, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v2, p0, Lcom/vlingo/midas/ui/SimpleListScreen;->m_targets:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method


# virtual methods
.method protected addListItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p3, "activityToStart":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/vlingo/midas/ui/SimpleListScreen;->addListItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method protected addListItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "urlToOpen"    # Ljava/lang/String;

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/vlingo/midas/ui/SimpleListScreen;->addListItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method protected initAdapter()Landroid/widget/SimpleAdapter;
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 72
    new-instance v0, Landroid/widget/SimpleAdapter;

    iget-object v2, p0, Lcom/vlingo/midas/ui/SimpleListScreen;->m_list:Ljava/util/ArrayList;

    const v3, 0x1090004

    new-array v4, v6, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v5, "line1"

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const-string/jumbo v5, "line2"

    aput-object v5, v4, v1

    new-array v5, v6, [I

    fill-array-data v5, :array_0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-object v0

    :array_0
    .array-data 4
        0x1020014
        0x1020015
    .end array-data
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lcom/vlingo/midas/ui/SimpleListScreen;->initAdapter()Landroid/widget/SimpleAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/ui/SimpleListScreen;->m_adapter:Landroid/widget/SimpleAdapter;

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/midas/ui/SimpleListScreen;->onInitListItems()V

    .line 38
    iget-object v0, p0, Lcom/vlingo/midas/ui/SimpleListScreen;->m_adapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/ui/SimpleListScreen;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 40
    invoke-virtual {p0}, Lcom/vlingo/midas/ui/SimpleListScreen;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/vlingo/midas/ui/SimpleListScreen$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/ui/SimpleListScreen$1;-><init>(Lcom/vlingo/midas/ui/SimpleListScreen;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 69
    return-void
.end method

.method protected abstract onInitListItems()V
.end method

.method protected setForwardToNextActivity(Z)V
    .locals 0
    .param p1, "forwardToNextActivity"    # Z

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/vlingo/midas/ui/SimpleListScreen;->m_forwardToNextActivity:Z

    .line 104
    return-void
.end method
