.class public Lcom/vlingo/midas/ui/HomeKeyListener;
.super Ljava/lang/Object;
.source "HomeKeyListener.java"


# static fields
.field private static CALLED_CREATE_DESCRIPTION:I

.field private static CALLED_PAUSE:I

.field private static CALLED_SAVE_INSTANCE_STATE:I

.field private static CALLED_STOP:I

.field private static CALLED_TRIM_MEMORY:I

.field private static CALLED_USER_INTERACTION:I

.field private static CALLED_USER_LEAVE_HINT:I

.field private static CALLED_WINDOW_FOCUS_CHANGED:I

.field private static SIGNATURE:I


# instance fields
.field private action:Ljava/lang/Runnable;

.field private explicitLaunch:Z

.field private presentState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 201
    const v0, -0x5432108f

    sput v0, Lcom/vlingo/midas/ui/HomeKeyListener;->SIGNATURE:I

    .line 206
    const/16 v0, 0xa

    sput v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_USER_INTERACTION:I

    .line 207
    const/16 v0, 0xb

    sput v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_USER_LEAVE_HINT:I

    .line 208
    const/16 v0, 0xc

    sput v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_PAUSE:I

    .line 209
    const/16 v0, 0xd

    sput v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_WINDOW_FOCUS_CHANGED:I

    .line 210
    const/16 v0, 0xe

    sput v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_TRIM_MEMORY:I

    .line 211
    const/16 v0, 0xf

    sput v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_CREATE_DESCRIPTION:I

    .line 212
    const/4 v0, 0x7

    sput v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_SAVE_INSTANCE_STATE:I

    .line 213
    const/4 v0, 0x1

    sput v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_STOP:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    if-nez p1, :cond_0

    .line 60
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": \'action\' must be non-null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    iput-object p1, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->action:Ljava/lang/Runnable;

    .line 62
    return-void
.end method

.method public static onHomePressed(Ljava/lang/Runnable;)Lcom/vlingo/midas/ui/HomeKeyListener;
    .locals 1
    .param p0, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 55
    new-instance v0, Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/ui/HomeKeyListener;-><init>(Ljava/lang/Runnable;)V

    return-object v0
.end method

.method private reset()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->presentState:I

    .line 166
    return-void
.end method

.method private setState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 161
    iget v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->presentState:I

    shl-int/lit8 v0, v0, 0x4

    or-int/2addr v0, p1

    iput v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->presentState:I

    .line 162
    return-void
.end method


# virtual methods
.method public onCreateDescription()V
    .locals 1

    .prologue
    .line 136
    sget v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_CREATE_DESCRIPTION:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->setState(I)V

    .line 137
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 119
    sget v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_PAUSE:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->setState(I)V

    .line 120
    return-void
.end method

.method public onSaveInstanceState()V
    .locals 1

    .prologue
    .line 140
    sget v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_SAVE_INSTANCE_STATE:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->setState(I)V

    .line 141
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 144
    sget v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_STOP:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->setState(I)V

    .line 145
    iget v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->presentState:I

    sget v1, Lcom/vlingo/midas/ui/HomeKeyListener;->SIGNATURE:I

    if-ne v0, v1, :cond_0

    .line 146
    iget-object v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->action:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 147
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/ui/HomeKeyListener;->reset()V

    .line 148
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 131
    const/16 v0, 0x14

    if-ne p1, v0, :cond_0

    .line 132
    sget v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_TRIM_MEMORY:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->setState(I)V

    .line 133
    :cond_0
    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/vlingo/midas/ui/HomeKeyListener;->reset()V

    .line 107
    iget-boolean v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->explicitLaunch:Z

    if-nez v0, :cond_0

    .line 108
    sget v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_USER_INTERACTION:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->setState(I)V

    .line 111
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->explicitLaunch:Z

    .line 112
    return-void
.end method

.method public onUserLeaveHint()V
    .locals 1

    .prologue
    .line 115
    sget v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_USER_LEAVE_HINT:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->setState(I)V

    .line 116
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 123
    if-eqz p1, :cond_0

    .line 124
    invoke-direct {p0}, Lcom/vlingo/midas/ui/HomeKeyListener;->reset()V

    .line 127
    :goto_0
    return-void

    .line 126
    :cond_0
    sget v0, Lcom/vlingo/midas/ui/HomeKeyListener;->CALLED_WINDOW_FOCUS_CHANGED:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->setState(I)V

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->explicitLaunch:Z

    .line 73
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->explicitLaunch:Z

    .line 77
    return-void
.end method

.method public startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;I)V
    .locals 1
    .param p1, "child"    # Landroid/app/Activity;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "requestCode"    # I

    .prologue
    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->explicitLaunch:Z

    .line 81
    return-void
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V
    .locals 1
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "requestCode"    # I

    .prologue
    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->explicitLaunch:Z

    .line 89
    return-void
.end method

.method public startActivityIfNeeded(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/ui/HomeKeyListener;->explicitLaunch:Z

    .line 85
    return-void
.end method
