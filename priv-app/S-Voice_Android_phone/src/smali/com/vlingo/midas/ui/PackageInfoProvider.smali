.class public Lcom/vlingo/midas/ui/PackageInfoProvider;
.super Lcom/vlingo/core/internal/util/CorePackageInfoProvider;
.source "PackageInfoProvider.java"


# static fields
.field public static final ALARM:[Ljava/lang/String;

.field public static final CALL:[Ljava/lang/String;

.field public static final CN_BAIDU_MAPS:[Ljava/lang/String;

.field public static final EMAIL:[Ljava/lang/String;

.field public static final EVENT:[Ljava/lang/String;

.field public static final MAP:[Ljava/lang/String;

.field public static final MEMO:[Ljava/lang/String;

.field public static final MESSAGING:[Ljava/lang/String;

.field public static final MUSIC:[Ljava/lang/String;

.field public static final NAV:[Ljava/lang/String;

.field public static final RADIO:[Ljava/lang/String;

.field public static final SAFETY:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field public static final TASK:[Ljava/lang/String;

.field public static final TIMER:[Ljava/lang/String;

.field public static final VOICERECORDER:[Ljava/lang/String;

.field public static final WORLDCLOCK_TAB:[Ljava/lang/String;


# instance fields
.field private callAppDrawable:Landroid/graphics/drawable/Drawable;

.field private mContext:Landroid/content/Context;

.field mInstalledPkgList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRadioText:I

.field private mapsAppDrawable:Landroid/graphics/drawable/Drawable;

.field private memoDrawable:Landroid/graphics/drawable/Drawable;

.field private messageDrawable:Landroid/graphics/drawable/Drawable;

.field private naviDrawable:Landroid/graphics/drawable/Drawable;

.field private navigationAppDrawable:Landroid/graphics/drawable/Drawable;

.field private plannerDrawable:Landroid/graphics/drawable/Drawable;

.field private voicerecorderDrawable:Landroid/graphics/drawable/Drawable;

.field private weatherDrawable:Landroid/graphics/drawable/Drawable;

.field private webAppDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    const-class v0, Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->TAG:Ljava/lang/String;

    .line 37
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.android.phone"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->CALL:[Ljava/lang/String;

    .line 38
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.memo"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.sec.android.widgetapp.diotek.smemo"

    aput-object v1, v0, v4

    const-string/jumbo v1, "com.sec.android.app.snotebook"

    aput-object v1, v0, v5

    const-string/jumbo v1, "com.samsung.android.snote"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string/jumbo v2, "com.samsung.android.app.memo"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->MEMO:[Ljava/lang/String;

    .line 39
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.google.android.apps.maps"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->NAV:[Ljava/lang/String;

    .line 40
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.google.android.apps.maps"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->MAP:[Ljava/lang/String;

    .line 41
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.clockpackage"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->ALARM:[Ljava/lang/String;

    .line 42
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.android.email"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->EMAIL:[Ljava/lang/String;

    .line 43
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "com.android.calendar"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.nttdocomo.android.schedulememo"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->EVENT:[Ljava/lang/String;

    .line 44
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.android.calendar"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->TASK:[Ljava/lang/String;

    .line 45
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.music"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.samsung.music"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->MUSIC:[Ljava/lang/String;

    .line 46
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.fm"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->RADIO:[Ljava/lang/String;

    .line 47
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.voicerecorder"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.sec.android.app.voicenote"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->VOICERECORDER:[Ljava/lang/String;

    .line 48
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.clockpackage.timer"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.sec.android.app.clockpackage"

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->TIMER:[Ljava/lang/String;

    .line 49
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.android.mms"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->MESSAGING:[Ljava/lang/String;

    .line 50
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.worldclock"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->WORLDCLOCK_TAB:[Ljava/lang/String;

    .line 51
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "com.baidu.BaiduMap"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.baidu.BaiduMap.pad"

    aput-object v1, v0, v4

    const-string/jumbo v1, "com.baidu.BaiduMap.samsung"

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->CN_BAIDU_MAPS:[Ljava/lang/String;

    .line 52
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "com.sec.android.app.safetyassurance"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/ui/PackageInfoProvider;->SAFETY:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CorePackageInfoProvider;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->memoDrawable:Landroid/graphics/drawable/Drawable;

    .line 30
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->messageDrawable:Landroid/graphics/drawable/Drawable;

    .line 31
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->naviDrawable:Landroid/graphics/drawable/Drawable;

    .line 32
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->weatherDrawable:Landroid/graphics/drawable/Drawable;

    .line 33
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->plannerDrawable:Landroid/graphics/drawable/Drawable;

    .line 34
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->voicerecorderDrawable:Landroid/graphics/drawable/Drawable;

    .line 56
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->callAppDrawable:Landroid/graphics/drawable/Drawable;

    .line 57
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mapsAppDrawable:Landroid/graphics/drawable/Drawable;

    .line 58
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->navigationAppDrawable:Landroid/graphics/drawable/Drawable;

    .line 59
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->webAppDrawable:Landroid/graphics/drawable/Drawable;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/CorePackageInfoProvider;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->memoDrawable:Landroid/graphics/drawable/Drawable;

    .line 30
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->messageDrawable:Landroid/graphics/drawable/Drawable;

    .line 31
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->naviDrawable:Landroid/graphics/drawable/Drawable;

    .line 32
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->weatherDrawable:Landroid/graphics/drawable/Drawable;

    .line 33
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->plannerDrawable:Landroid/graphics/drawable/Drawable;

    .line 34
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->voicerecorderDrawable:Landroid/graphics/drawable/Drawable;

    .line 56
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->callAppDrawable:Landroid/graphics/drawable/Drawable;

    .line 57
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mapsAppDrawable:Landroid/graphics/drawable/Drawable;

    .line 58
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->navigationAppDrawable:Landroid/graphics/drawable/Drawable;

    .line 59
    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->webAppDrawable:Landroid/graphics/drawable/Drawable;

    .line 65
    iput-object p1, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    .line 68
    return-void
.end method

.method public static hasAlarm()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 533
    const/4 v6, 0x0

    .line 534
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->ALARM:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 536
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 537
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 541
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 545
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 538
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 539
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 534
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 545
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasAutonavi()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 489
    const-string/jumbo v0, "FAKE_CHINESE_NAVIGATION_APP"

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    const-string/jumbo v0, "CHINESE_NAVIGATION_AUTONAVI"

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 492
    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "com.autonavi.xmgd.navigator.keyboard"

    invoke-static {v0, v1}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public static hasBaiduMaps()Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 497
    const-string/jumbo v9, "FAKE_CHINESE_NAVIGATION_APP"

    invoke-static {v9, v8}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 498
    const-string/jumbo v7, "CHINESE_NAVIGATION_BAIDU"

    invoke-static {v7, v8}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 512
    .local v1, "arr$":[Ljava/lang/String;
    .local v4, "i$":I
    .local v5, "len$":I
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return v7

    .line 500
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "mAppInfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    const/4 v6, 0x0

    .line 501
    .restart local v6    # "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->CN_BAIDU_MAPS:[Ljava/lang/String;

    .restart local v1    # "arr$":[Ljava/lang/String;
    array-length v5, v1

    .restart local v5    # "len$":I
    const/4 v4, 0x0

    .restart local v4    # "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 503
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 504
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const/16 v10, 0x80

    invoke-virtual {v9, v0, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 508
    .end local v2    # "context":Landroid/content/Context;
    :goto_2
    if-nez v6, :cond_0

    .line 501
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 505
    :catch_0
    move-exception v3

    .line 506
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_2

    .line 512
    .end local v0    # "app":Ljava/lang/String;
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    if-nez v6, :cond_0

    move v7, v8

    goto :goto_0
.end method

.method public static hasCallEnabled()Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    .line 597
    const/4 v6, 0x0

    .line 598
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->CALL:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 600
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 601
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 606
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 607
    sget-object v8, Lcom/vlingo/midas/ui/PackageInfoProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "hasCallEnabled() - true"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 602
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 603
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v8, Lcom/vlingo/midas/ui/PackageInfoProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "hasCallEnabled() - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    const/4 v6, 0x0

    goto :goto_1

    .line 598
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 611
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasDialing()Z
    .locals 4

    .prologue
    .line 297
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 298
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 299
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasCallEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 302
    :cond_0
    sget-object v2, Lcom/vlingo/midas/ui/PackageInfoProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "hasDialing() - false"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    const/4 v2, 0x0

    .line 308
    :goto_0
    return v2

    .line 307
    :cond_1
    sget-object v2, Lcom/vlingo/midas/ui/PackageInfoProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "hasDialing() - true"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static hasEvent()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 549
    const/4 v6, 0x0

    .line 550
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->EVENT:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 552
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 553
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 557
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 561
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 554
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 555
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 550
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 561
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasMaps()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 517
    const/4 v6, 0x0

    .line 518
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->MAP:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 520
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 521
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 525
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 529
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 522
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 523
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 518
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 529
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasMemo()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 450
    const/4 v6, 0x0

    .line 451
    .local v6, "mMemo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->MEMO:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 453
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 454
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 458
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 462
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 455
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 456
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 451
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 462
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasMessaging()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 343
    const/4 v6, 0x0

    .line 345
    .local v6, "messaging":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->MESSAGING:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 347
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 348
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 352
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 356
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 349
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 350
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 345
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 356
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasMusic()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 581
    const/4 v6, 0x0

    .line 582
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->MUSIC:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 584
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 585
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 589
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 593
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 586
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 587
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 582
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 593
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasNav()Z
    .locals 11

    .prologue
    const/4 v8, 0x1

    .line 469
    const/4 v7, 0x0

    .line 470
    .local v7, "mActInfo":Landroid/content/pm/ActivityInfo;
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 471
    .local v3, "context":Landroid/content/Context;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->NAV:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_3

    aget-object v0, v1, v5

    .line 474
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    new-instance v2, Landroid/content/ComponentName;

    const-string/jumbo v9, "com.google.android.maps.driveabout.app.DestinationActivity"

    invoke-direct {v2, v0, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    .local v2, "component":Landroid/content/ComponentName;
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const/16 v10, 0x80

    invoke-virtual {v9, v2, v10}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 481
    .end local v2    # "component":Landroid/content/ComponentName;
    :cond_0
    if-eqz v7, :cond_2

    .line 485
    .end local v0    # "app":Ljava/lang/String;
    :cond_1
    :goto_1
    return v8

    .line 476
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 477
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasAutonavi()Z

    move-result v9

    if-nez v9, :cond_1

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasBaiduMaps()Z

    move-result v9

    if-eqz v9, :cond_0

    goto :goto_1

    .line 471
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 485
    .end local v0    # "app":Ljava/lang/String;
    :cond_3
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public static hasRadio()Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 360
    const/4 v7, 0x0

    .line 361
    .local v7, "radio":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->RADIO:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_3

    aget-object v0, v1, v5

    .line 363
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 364
    .local v3, "context":Landroid/content/Context;
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/16 v11, 0x80

    invoke-virtual {v10, v0, v11}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 369
    .end local v3    # "context":Landroid/content/Context;
    :goto_1
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 370
    .local v2, "build_model":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 381
    .end local v0    # "app":Ljava/lang/String;
    .end local v2    # "build_model":Ljava/lang/String;
    :cond_0
    :goto_2
    return v9

    .line 365
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 366
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v7, 0x0

    goto :goto_1

    .line 372
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "build_model":Ljava/lang/String;
    :cond_1
    const-string/jumbo v10, "GT-I9195"

    invoke-virtual {v2, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string/jumbo v10, "DGT-I9195"

    invoke-virtual {v2, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string/jumbo v10, "GT-I9192"

    invoke-virtual {v2, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 377
    if-eqz v7, :cond_2

    move v9, v8

    .line 378
    goto :goto_2

    .line 361
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 381
    .end local v0    # "app":Ljava/lang/String;
    .end local v2    # "build_model":Ljava/lang/String;
    :cond_3
    if-eqz v7, :cond_4

    :goto_3
    move v9, v8

    goto :goto_2

    :cond_4
    move v8, v9

    goto :goto_3
.end method

.method public static hasTask()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 565
    const/4 v6, 0x0

    .line 566
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->TASK:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 568
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 569
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 573
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 577
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 570
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 571
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 566
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 577
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasTimer()Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 401
    const/4 v6, 0x0

    .line 402
    .local v6, "mTimer":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->TIMER:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_4

    aget-object v0, v1, v4

    .line 404
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 405
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const/16 v10, 0x80

    invoke-virtual {v9, v0, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    .line 409
    if-eqz v6, :cond_0

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasWorldClock()Z

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    if-eqz v6, :cond_3

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->isTabletClockPackageAvailable()Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    if-eqz v9, :cond_3

    :cond_1
    move v7, v8

    .line 419
    .end local v0    # "app":Ljava/lang/String;
    .end local v2    # "context":Landroid/content/Context;
    :cond_2
    :goto_1
    return v7

    .line 412
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 413
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    .line 415
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    if-nez v6, :cond_2

    .line 402
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 419
    .end local v0    # "app":Ljava/lang/String;
    :cond_4
    if-nez v6, :cond_2

    move v7, v8

    goto :goto_1
.end method

.method public static hasVoiceRecorder()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 385
    const/4 v6, 0x0

    .line 386
    .local v6, "voiceRecorder":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->VOICERECORDER:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 388
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 389
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 393
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 397
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 390
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 391
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 386
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 397
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static hasWorldClock()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 423
    const/4 v6, 0x0

    .line 424
    .local v6, "mWorldClock":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->WORLDCLOCK_TAB:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 426
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 427
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 431
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 435
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 428
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 429
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 424
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 435
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static isEmergencyModeSupported()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 615
    const/4 v6, 0x0

    .line 616
    .local v6, "mAppInfo":Landroid/content/pm/ApplicationInfo;
    sget-object v1, Lcom/vlingo/midas/ui/PackageInfoProvider;->SAFETY:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 618
    .local v0, "app":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 619
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 623
    .end local v2    # "context":Landroid/content/Context;
    :goto_1
    if-eqz v6, :cond_1

    .line 627
    .end local v0    # "app":Ljava/lang/String;
    :cond_0
    :goto_2
    return v7

    .line 620
    .restart local v0    # "app":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 621
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 616
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 627
    .end local v0    # "app":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_0

    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static isTabletClockPackageAvailable()Z
    .locals 7

    .prologue
    .line 439
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 440
    .local v1, "intent":Landroid/content/Intent;
    new-instance v4, Landroid/content/ComponentName;

    const-string/jumbo v5, "com.sec.android.app.clockpackage"

    const-string/jumbo v6, "com.sec.android.app.clockpackage.TabletClockPackage"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 442
    invoke-static {}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getInstance()Lcom/vlingo/sdk/internal/core/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/sdk/internal/core/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 443
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 444
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const/high16 v4, 0x10000

    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 446
    .local v2, "list":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAppCallIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->callAppDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getAppMapIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mapsAppDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getAppNavigationIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->navigationAppDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getAppWebIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->webAppDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getMemoIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->memoDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getMessageIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->messageDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getNavigationIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->naviDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getPlannerIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->plannerDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getRadioText()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mRadioText:I

    return v0
.end method

.method public getVoiceRecorderIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->voicerecorderDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getWeatherIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->weatherDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public hasPenFeature()Z
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x0

    return v0
.end method

.method public setAppCallIcon()V
    .locals 4

    .prologue
    .line 226
    :try_start_0
    new-instance v0, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.android.contacts"

    const-string/jumbo v3, "com.android.contacts.activities.DialtactsActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    .local v0, "cp":Landroid/content/ComponentName;
    iget-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->callAppDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    .end local v0    # "cp":Landroid/content/ComponentName;
    :goto_0
    return-void

    .line 228
    :catch_0
    move-exception v1

    .line 229
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->callAppDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setAppMapIcon()V
    .locals 4

    .prologue
    .line 239
    :try_start_0
    new-instance v0, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.google.android.apps.maps"

    const-string/jumbo v3, "com.google.android.maps.MapsActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    .local v0, "cp":Landroid/content/ComponentName;
    iget-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mapsAppDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    .end local v0    # "cp":Landroid/content/ComponentName;
    :goto_0
    return-void

    .line 241
    :catch_0
    move-exception v1

    .line 243
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mapsAppDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setAppNavigationIcon()V
    .locals 8

    .prologue
    .line 253
    :try_start_0
    new-instance v0, Landroid/content/ComponentName;

    const-string/jumbo v6, "com.google.android.apps.maps"

    const-string/jumbo v7, "com.google.android.maps.driveabout.app.DestinationActivity"

    invoke-direct {v0, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    .local v0, "cp":Landroid/content/ComponentName;
    iget-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->navigationAppDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    .end local v0    # "cp":Landroid/content/ComponentName;
    :goto_0
    return-void

    .line 255
    :catch_0
    move-exception v1

    .line 258
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    iget-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string/jumbo v7, "com.baidu.BaiduMap.samsung"

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->navigationAppDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 259
    :catch_1
    move-exception v2

    .line 261
    .local v2, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_2
    iget-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string/jumbo v7, "com.baidu.BaiduMap"

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->navigationAppDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 262
    :catch_2
    move-exception v3

    .line 264
    .local v3, "e2":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    iget-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string/jumbo v7, "com.baidu.BaiduMap.pad"

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->navigationAppDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 265
    :catch_3
    move-exception v4

    .line 267
    .local v4, "e3":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_4
    iget-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string/jumbo v7, "com.autonavi.xmgd.navigator.keyboard"

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->navigationAppDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    .line 268
    :catch_4
    move-exception v5

    .line 269
    .local v5, "e4":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->navigationAppDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setAppWebIcon()V
    .locals 5

    .prologue
    .line 283
    :try_start_0
    new-instance v0, Landroid/content/ComponentName;

    const-string/jumbo v3, "com.android.browser"

    const-string/jumbo v4, "com.android.browser.BrowserActivity"

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    .local v0, "cp":Landroid/content/ComponentName;
    iget-object v3, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->webAppDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    .end local v0    # "cp":Landroid/content/ComponentName;
    :goto_0
    return-void

    .line 285
    :catch_0
    move-exception v1

    .line 288
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    iget-object v3, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string/jumbo v4, "com.sec.android.app.sbrowser"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->webAppDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 289
    :catch_1
    move-exception v2

    .line 290
    .local v2, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->webAppDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setMemoIcon()V
    .locals 7

    .prologue
    .line 88
    :try_start_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 89
    iget-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->memoDrawable:Landroid/graphics/drawable/Drawable;

    .line 126
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string/jumbo v6, "com.samsung.android.app.memo"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->memoDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    iget-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string/jumbo v6, "com.sec.android.app.snotebook"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->memoDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 101
    :catch_1
    move-exception v1

    .line 103
    .local v1, "e2":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_2
    iget-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string/jumbo v6, "com.samsung.android.snote"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->memoDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 104
    :catch_2
    move-exception v2

    .line 106
    .local v2, "e3":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    iget-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string/jumbo v6, "com.sec.android.widgetapp.diotek.smemo"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->memoDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 107
    :catch_3
    move-exception v3

    .line 109
    .local v3, "e4":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_4
    iget-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$drawable;->tmemo:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->memoDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    .line 110
    :catch_4
    move-exception v4

    .line 111
    .local v4, "e5":Ljava/lang/Exception;
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->memoDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setMessageIcon()V
    .locals 3

    .prologue
    .line 176
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.android.mms"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->messageDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->messageDrawable:Landroid/graphics/drawable/Drawable;

    .line 181
    return-void

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->messageDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setNavigationIcon()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$drawable;->help_navi:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->naviDrawable:Landroid/graphics/drawable/Drawable;

    .line 218
    return-void
.end method

.method public setPlannerIcon()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_planner:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->plannerDrawable:Landroid/graphics/drawable/Drawable;

    .line 167
    return-void
.end method

.method public setRadioText()V
    .locals 1

    .prologue
    .line 188
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasRadio()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    sget v0, Lcom/vlingo/midas/R$array;->wcis_music_examples:I

    iput v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mRadioText:I

    .line 193
    :goto_0
    return-void

    .line 191
    :cond_0
    sget v0, Lcom/vlingo/midas/R$array;->wcis_music_examples_no_radio:I

    iput v0, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mRadioText:I

    goto :goto_0
.end method

.method public setVoiceRecorderIcon()V
    .locals 4

    .prologue
    .line 324
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.sec.android.app.voicerecorder"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->voicerecorderDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    :goto_0
    iget-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->voicerecorderDrawable:Landroid/graphics/drawable/Drawable;

    .line 333
    return-void

    .line 325
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    iget-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.sec.android.app.voicenote"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->voicerecorderDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 328
    :catch_1
    move-exception v1

    .line 329
    .local v1, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->voicerecorderDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setWeatherIcon()V
    .locals 6

    .prologue
    .line 135
    :try_start_0
    iget-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string/jumbo v5, "com.sec.android.widgetapp.ap.hero.accuweather.widget"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->weatherDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$drawable;->help_weather:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->weatherDrawable:Landroid/graphics/drawable/Drawable;

    .line 152
    return-void

    .line 136
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    iget-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string/jumbo v5, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->weatherDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 139
    :catch_1
    move-exception v1

    .line 141
    .local v1, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_2
    iget-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string/jumbo v5, "com.sec.android.widgetapp.at.hero.accuweather"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->weatherDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 142
    :catch_2
    move-exception v2

    .line 144
    .local v2, "e2":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    iget-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string/jumbo v5, "com.sec.android.widgetapp.at.hero.accuweather"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->weatherDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 145
    :catch_3
    move-exception v3

    .line 146
    .local v3, "e3":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/midas/ui/PackageInfoProvider;->weatherDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
