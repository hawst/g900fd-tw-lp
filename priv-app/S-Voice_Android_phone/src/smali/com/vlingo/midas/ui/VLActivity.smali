.class public Lcom/vlingo/midas/ui/VLActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "VLActivity.java"


# static fields
.field public static final MENU_HELP:I = 0x0

.field public static final MENU_LAST:I = 0x64

.field public static final MENU_SETTINGS:I = 0x2

.field public static final MENU_VOICE_TALK:I = 0x3

.field public static final MENU_WCIS:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public getSharedPrefsFile(Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 130
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method protected makeDefaultMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 102
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 70
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 72
    invoke-virtual {p0}, Lcom/vlingo/midas/ui/VLActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 73
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/midas/ui/VLActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 37
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/ui/VLActivity;->setVolumeControlStream(I)V

    .line 38
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->considerRightBeforeForeground(Z)V

    .line 39
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 63
    const-string/jumbo v1, "com.vlingo.core.internal.vlservice.FINISH_ACTIVITY"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 64
    .local v0, "finishActivity":Z
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/vlingo/midas/ui/VLActivity;->finish()V

    .line 67
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 106
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 120
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 108
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/ui/VLActivity;->showAbout()V

    goto :goto_0

    .line 111
    :pswitch_1
    invoke-virtual {p0}, Lcom/vlingo/midas/ui/VLActivity;->showWCIS()V

    goto :goto_0

    .line 114
    :pswitch_2
    invoke-virtual {p0}, Lcom/vlingo/midas/ui/VLActivity;->showSettings()V

    goto :goto_0

    .line 117
    :pswitch_3
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getMainActivityClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/ui/VLActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 43
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    const-string/jumbo v1, "com.vlingo.client.app.extra.STATE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 45
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/ui/VLActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 47
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 48
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 51
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 53
    invoke-virtual {p0}, Lcom/vlingo/midas/ui/VLActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 55
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 56
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    const-string/jumbo v1, "com.vlingo.client.app.extra.STATE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 58
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/ui/VLActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 59
    return-void
.end method

.method protected showAbout()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method protected showSettings()V
    .locals 0

    .prologue
    .line 96
    return-void
.end method

.method protected showWCIS()V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/ui/VLActivity;->startActivity(Landroid/content/Intent;)V

    .line 85
    return-void
.end method
