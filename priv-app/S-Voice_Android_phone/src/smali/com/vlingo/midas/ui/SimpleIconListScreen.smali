.class public abstract Lcom/vlingo/midas/ui/SimpleIconListScreen;
.super Lcom/vlingo/midas/ui/SimpleListScreen;
.source "SimpleIconListScreen.java"


# instance fields
.field protected m_listItemResource:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/vlingo/midas/ui/SimpleListScreen;-><init>()V

    .line 15
    sget v0, Lcom/vlingo/midas/R$layout;->simple_list_item_2_image:I

    iput v0, p0, Lcom/vlingo/midas/ui/SimpleIconListScreen;->m_listItemResource:I

    return-void
.end method


# virtual methods
.method protected addListItem(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Class;)V
    .locals 6
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "iconResource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p4, "activityToStart":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/midas/ui/SimpleIconListScreen;->addListItem(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Class;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method protected addListItem(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Class;Ljava/lang/String;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "iconResource"    # I
    .param p5, "listItemParam"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    .local p4, "activityToStart":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 32
    .local v0, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v2, "line1"

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const-string/jumbo v2, "line2"

    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const-string/jumbo v2, "icon1"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object v2, p0, Lcom/vlingo/midas/ui/SimpleIconListScreen;->m_list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 40
    .local v1, "target":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v2, "activity"

    invoke-virtual {v1, v2, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const-string/jumbo v2, "url"

    invoke-virtual {v1, v2, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iget-object v2, p0, Lcom/vlingo/midas/ui/SimpleIconListScreen;->m_targets:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method

.method protected initAdapter()Landroid/widget/SimpleAdapter;
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 22
    new-instance v0, Landroid/widget/SimpleAdapter;

    iget-object v2, p0, Lcom/vlingo/midas/ui/SimpleIconListScreen;->m_list:Ljava/util/ArrayList;

    iget v3, p0, Lcom/vlingo/midas/ui/SimpleIconListScreen;->m_listItemResource:I

    new-array v4, v6, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v5, "icon1"

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const-string/jumbo v5, "line1"

    aput-object v5, v4, v1

    const/4 v1, 0x2

    const-string/jumbo v5, "line2"

    aput-object v5, v4, v1

    new-array v5, v6, [I

    fill-array-data v5, :array_0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-object v0

    nop

    :array_0
    .array-data 4
        0x1020007
        0x1020014
        0x1020015
    .end array-data
.end method
