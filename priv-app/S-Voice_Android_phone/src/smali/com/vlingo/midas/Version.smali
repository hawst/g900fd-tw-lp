.class public Lcom/vlingo/midas/Version;
.super Ljava/lang/Object;
.source "Version.java"


# static fields
.field private static final c_Major:Ljava/lang/String; = "11"

.field private static final c_Minor:Ljava/lang/String; = "7"

.field private static final c_Patch:Ljava/lang/String; = "0"

.field private static final c_Release:Ljava/lang/String; = "0"

.field private static final c_SVN:Ljava/lang/String; = "37633"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method getMajor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string/jumbo v0, "11"

    return-object v0
.end method

.method getMinor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string/jumbo v0, "7"

    return-object v0
.end method

.method getPatch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string/jumbo v0, "0"

    return-object v0
.end method

.method getRelease()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const-string/jumbo v0, "0"

    return-object v0
.end method

.method getSVN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    const-string/jumbo v0, "37633"

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/Version;->getMajor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/Version;->getMinor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/Version;->getRelease()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/Version;->getPatch()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/Version;->getSVN()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
