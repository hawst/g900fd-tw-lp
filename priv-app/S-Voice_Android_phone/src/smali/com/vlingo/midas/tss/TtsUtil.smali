.class public Lcom/vlingo/midas/tss/TtsUtil;
.super Ljava/lang/Object;
.source "TtsUtil.java"


# static fields
.field private static final TTS_CHUNK_SIZE_MAX:I = 0xfa

.field private static final TTS_CHUNK_SIZE_TARGET:I = 0x96

.field private static final comma:Ljava/lang/String;

.field private static final commaSpace:Ljava/lang/String;

.field private static final period:Ljava/lang/String;

.field private static final periodSpace:Ljava/lang/String;

.field private static final questionMark:Ljava/lang/String; = "?"

.field private static final questionMarkSpace:Ljava/lang/String;

.field private static final space:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/tss/TtsUtil;->space:Ljava/lang/String;

    .line 17
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/tss/TtsUtil;->period:Ljava/lang/String;

    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/vlingo/midas/tss/TtsUtil;->period:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/tss/TtsUtil;->space:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/tss/TtsUtil;->periodSpace:Ljava/lang/String;

    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/tss/TtsUtil;->space:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/tss/TtsUtil;->questionMarkSpace:Ljava/lang/String;

    .line 25
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/tss/TtsUtil;->comma:Ljava/lang/String;

    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/tss/TtsUtil;->space:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/tss/TtsUtil;->commaSpace:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getBreakPoint(Ljava/lang/String;)I
    .locals 3
    .param p0, "tts"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xfa

    if-gt v1, v2, :cond_1

    .line 69
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 111
    :cond_0
    :goto_0
    return v0

    .line 72
    :cond_1
    sget-object v1, Lcom/vlingo/midas/tss/TtsUtil;->periodSpace:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/vlingo/midas/tss/TtsUtil;->searchFor(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 73
    .local v0, "offset":I
    if-gez v0, :cond_0

    .line 76
    sget-object v1, Lcom/vlingo/midas/tss/TtsUtil;->questionMarkSpace:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/vlingo/midas/tss/TtsUtil;->searchFor(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 77
    if-gez v0, :cond_0

    .line 80
    sget-object v1, Lcom/vlingo/midas/tss/TtsUtil;->commaSpace:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/vlingo/midas/tss/TtsUtil;->searchFor(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 81
    if-gez v0, :cond_0

    .line 90
    sget-object v1, Lcom/vlingo/midas/tss/TtsUtil;->period:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/vlingo/midas/tss/TtsUtil;->searchFor(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 91
    if-gez v0, :cond_0

    .line 94
    const-string/jumbo v1, "?"

    invoke-static {p0, v1}, Lcom/vlingo/midas/tss/TtsUtil;->searchFor(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 95
    if-gez v0, :cond_0

    .line 98
    sget-object v1, Lcom/vlingo/midas/tss/TtsUtil;->comma:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/vlingo/midas/tss/TtsUtil;->searchFor(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 99
    if-gez v0, :cond_0

    .line 105
    sget-object v1, Lcom/vlingo/midas/tss/TtsUtil;->space:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/vlingo/midas/tss/TtsUtil;->searchFor(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 106
    if-gez v0, :cond_0

    .line 111
    const/16 v0, 0x95

    goto :goto_0
.end method

.method public static getTtsArray(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6
    .param p0, "originalTts"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v2, "toReturn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v3, p0

    .line 36
    .local v3, "workingTts":Ljava/lang/String;
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 37
    invoke-static {v3}, Lcom/vlingo/midas/tss/TtsUtil;->getBreakPoint(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v0, v4, 0x1

    .line 38
    .local v0, "breakPoint":I
    const/4 v4, 0x0

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "fragmentTTS":Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0xfa

    if-le v4, v5, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 43
    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 48
    goto :goto_0

    .line 59
    .end local v0    # "breakPoint":I
    .end local v1    # "fragmentTTS":Ljava/lang/String;
    :cond_0
    return-object v2
.end method

.method private static searchFor(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0, "tts"    # Ljava/lang/String;
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 115
    const/16 v0, 0x96

    .line 116
    .local v0, "breakPoint":I
    const/16 v2, 0x96

    invoke-virtual {p0, p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 117
    .local v1, "offset":I
    if-ltz v1, :cond_0

    const/16 v2, 0xfa

    if-gt v1, v2, :cond_0

    .line 120
    .end local v1    # "offset":I
    :goto_0
    return v1

    .restart local v1    # "offset":I
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method
