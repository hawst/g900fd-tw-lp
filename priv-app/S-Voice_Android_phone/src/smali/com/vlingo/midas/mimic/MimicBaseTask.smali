.class public abstract Lcom/vlingo/midas/mimic/MimicBaseTask;
.super Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;
.source "MimicBaseTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;
    }
.end annotation


# static fields
.field public static final BUFFER_SIZE:I = 0x140


# instance fields
.field protected context:Landroid/content/Context;

.field protected filePlay:Ljava/io/File;

.field protected notificationHandler:Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/core/internal/dialogmanager/tasks/PausableTask;-><init>()V

    .line 81
    new-instance v1, Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;-><init>(Lcom/vlingo/midas/mimic/MimicBaseTask;)V

    iput-object v1, p0, Lcom/vlingo/midas/mimic/MimicBaseTask;->notificationHandler:Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;

    .line 34
    iput-object p1, p0, Lcom/vlingo/midas/mimic/MimicBaseTask;->context:Landroid/content/Context;

    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "sdPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "mimic.raw"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/vlingo/midas/mimic/MimicBaseTask;->filePlay:Ljava/io/File;

    .line 38
    return-void
.end method
