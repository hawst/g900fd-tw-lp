.class Lcom/vlingo/midas/mimic/MimicRecordTask$1;
.super Ljava/lang/Object;
.source "MimicRecordTask.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/TonePlayer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/mimic/MimicRecordTask;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/mimic/MimicRecordTask;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/mimic/MimicRecordTask;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$1;->this$0:Lcom/vlingo/midas/mimic/MimicRecordTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStarted(I)V
    .locals 0
    .param p1, "audioSessionId"    # I

    .prologue
    .line 65
    return-void
.end method

.method public onStopped()V
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$1;->this$0:Lcom/vlingo/midas/mimic/MimicRecordTask;

    const/4 v1, 0x0

    sget-object v2, Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;->LOCALRECORD:Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->request(Lcom/vlingo/sdk/recognition/VLRecognitionContext;Lcom/vlingo/core/internal/audio/MicrophoneStream$TaskType;)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-result-object v1

    # setter for: Lcom/vlingo/midas/mimic/MimicRecordTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;
    invoke-static {v0, v1}, Lcom/vlingo/midas/mimic/MimicRecordTask;->access$002(Lcom/vlingo/midas/mimic/MimicRecordTask;Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 61
    iget-object v0, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$1;->this$0:Lcom/vlingo/midas/mimic/MimicRecordTask;

    # invokes: Lcom/vlingo/midas/mimic/MimicRecordTask;->startRecording()V
    invoke-static {v0}, Lcom/vlingo/midas/mimic/MimicRecordTask;->access$100(Lcom/vlingo/midas/mimic/MimicRecordTask;)V

    .line 62
    return-void
.end method
