.class Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;
.super Ljava/lang/Thread;
.source "MimicRecordTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/mimic/MimicRecordTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReaderThread"
.end annotation


# instance fields
.field buffer:[B

.field final bufferSize:I

.field fos:Ljava/io/FileOutputStream;

.field private micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

.field private mimicTask:Lcom/vlingo/midas/mimic/MimicRecordTask;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/mimic/MimicRecordTask;Landroid/content/Context;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 2
    .param p1, "mimicTask"    # Lcom/vlingo/midas/mimic/MimicRecordTask;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/16 v0, 0x140

    .line 115
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 111
    iput v0, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->bufferSize:I

    .line 112
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->buffer:[B

    .line 116
    iput-object p1, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicRecordTask;

    .line 117
    iput-object p3, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 119
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p1, Lcom/vlingo/midas/mimic/MimicRecordTask;->filePlay:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->fos:Ljava/io/FileOutputStream;

    .line 120
    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;)Lcom/vlingo/midas/mimic/MimicRecordTask;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicRecordTask;

    return-object v0
.end method

.method static synthetic access$302(Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;Lcom/vlingo/midas/mimic/MimicRecordTask;)Lcom/vlingo/midas/mimic/MimicRecordTask;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;
    .param p1, "x1"    # Lcom/vlingo/midas/mimic/MimicRecordTask;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicRecordTask;

    return-object p1
.end method

.method static synthetic access$502(Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;
    .param p1, "x1"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    return-object p1
.end method


# virtual methods
.method public run()V
    .locals 33

    .prologue
    .line 126
    const/4 v4, 0x1

    .line 128
    .local v4, "bContinue":Z
    new-instance v5, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;

    invoke-direct {v5}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;-><init>()V

    .line 129
    .local v5, "builder":Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;
    const-string/jumbo v28, "endpoint.speechdetect_threshold"

    const/high16 v29, 0x41300000    # 11.0f

    invoke-static/range {v28 .. v29}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v28

    const-string/jumbo v29, "endpoint.speechdetect_voice_duration"

    const v30, 0x3da3d70a    # 0.08f

    invoke-static/range {v29 .. v30}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v29

    const-string/jumbo v30, "endpoint.speechdetect_voice_portion"

    const v31, 0x3ca3d70a    # 0.02f

    invoke-static/range {v30 .. v31}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v30

    const-string/jumbo v31, "endpoint.speechdetect_min_voice_level"

    const/high16 v32, 0x42640000    # 57.0f

    invoke-static/range {v31 .. v32}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v31

    move/from16 v0, v28

    move/from16 v1, v29

    move/from16 v2, v30

    move/from16 v3, v31

    invoke-virtual {v5, v0, v1, v2, v3}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->speechDetectorParams(FFFF)Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;

    move-result-object v5

    .line 134
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/vlingo/sdk/VLSdk;->getSpeechDetector()Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;

    move-result-object v20

    .line 135
    .local v20, "speechDetector":Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;
    invoke-virtual {v5}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext$Builder;->build()Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;

    move-result-object v28

    move-object/from16 v0, v20

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;->startSpeechDetector(Lcom/vlingo/sdk/recognition/speech/VLSpeechDetectorContext;)Z

    .line 138
    const-string/jumbo v28, "endpoint.time_withoutspeech"

    const/16 v29, 0x1388

    invoke-static/range {v28 .. v29}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 139
    .local v9, "endpoint_time_without_speech":I
    const-string/jumbo v28, "endpoint.time_withspeech"

    const/16 v29, 0x6d6

    invoke-static/range {v28 .. v29}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 140
    .local v8, "endpoint_time_with_speech":I
    const-string/jumbo v28, "max_audio_time"

    const v29, 0x9c40

    invoke-static/range {v28 .. v29}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v14

    .line 141
    .local v14, "max_audio_time":I
    const/16 v28, 0x7530

    move/from16 v0, v28

    if-le v14, v0, :cond_0

    .line 142
    const/16 v14, 0x7530

    .line 145
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    .line 146
    .local v22, "startTime":J
    const-wide/16 v26, 0x0

    .line 148
    .local v26, "timeLastSpeech":J
    const/16 v28, 0xa0

    move/from16 v0, v28

    new-array v0, v0, [S

    move-object/from16 v18, v0

    .line 149
    .local v18, "sBuffer":[S
    const/16 v19, 0x0

    .line 150
    .local v19, "silenceBufferCount":I
    const/4 v11, 0x0

    .line 151
    .local v11, "foundInitialSpeech":Z
    const/4 v10, 0x1

    .line 152
    .local v10, "firstBuffer":Z
    :cond_1
    :goto_0
    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicRecordTask;

    move-object/from16 v28, v0

    # invokes: Lcom/vlingo/midas/mimic/MimicRecordTask;->isCancelled()Z
    invoke-static/range {v28 .. v28}, Lcom/vlingo/midas/mimic/MimicRecordTask;->access$200(Lcom/vlingo/midas/mimic/MimicRecordTask;)Z

    move-result v28

    if-nez v28, :cond_a

    .line 154
    const-wide/16 v24, 0x0

    .line 155
    .local v24, "sum":D
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->buffer:[B

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->read([B)I

    move-result v6

    .line 156
    .local v6, "bytesRead":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    div-int/lit8 v28, v6, 0x2

    move/from16 v0, v28

    if-ge v13, v0, :cond_2

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->buffer:[B

    move-object/from16 v28, v0

    mul-int/lit8 v29, v13, 0x2

    add-int/lit8 v29, v29, 0x1

    aget-byte v28, v28, v29

    shl-int/lit8 v28, v28, 0x8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->buffer:[B

    move-object/from16 v29, v0

    mul-int/lit8 v30, v13, 0x2

    aget-byte v29, v29, v30

    add-int v28, v28, v29

    move/from16 v0, v28

    int-to-short v0, v0

    move/from16 v28, v0

    aput-short v28, v18, v13

    .line 158
    aget-short v28, v18, v13

    aget-short v29, v18, v13

    mul-int v28, v28, v29

    move/from16 v0, v28

    int-to-double v0, v0

    move-wide/from16 v28, v0

    add-double v24, v24, v28

    .line 156
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 162
    :cond_2
    div-int/lit8 v28, v6, 0x2

    move/from16 v0, v28

    int-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v28, v24, v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v28, v0

    shr-int/lit8 v17, v28, 0x4

    .line 163
    .local v17, "rms":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicRecordTask;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask;->notificationHandler:Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;

    move-object/from16 v28, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;->notifyRmsChange(Ljava/lang/Object;)V

    .line 165
    const/16 v28, 0x0

    div-int/lit8 v29, v6, 0x2

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-interface {v0, v1, v2, v3}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;->processShortArray([SII)Z

    move-result v12

    .line 166
    .local v12, "foundSpeech":Z
    if-eqz v10, :cond_7

    .line 167
    const/4 v10, 0x0

    .line 183
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->fos:Ljava/io/FileOutputStream;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->buffer:[B

    move-object/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2, v6}, Ljava/io/FileOutputStream;->write([BII)V

    .line 184
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    .line 185
    .local v15, "now":J
    sub-long v28, v15, v22

    int-to-long v0, v9

    move-wide/from16 v30, v0

    cmp-long v28, v28, v30

    if-lez v28, :cond_4

    if-eqz v11, :cond_6

    :cond_4
    sub-long v28, v15, v26

    int-to-long v0, v8

    move-wide/from16 v30, v0

    cmp-long v28, v28, v30

    if-lez v28, :cond_5

    if-nez v11, :cond_6

    :cond_5
    sub-long v28, v15, v22

    int-to-long v0, v14

    move-wide/from16 v30, v0

    cmp-long v28, v28, v30

    if-lez v28, :cond_1

    .line 188
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 170
    .end local v15    # "now":J
    :cond_7
    if-nez v12, :cond_9

    .line 171
    if-nez v19, :cond_8

    .line 172
    add-int/lit8 v19, v19, 0x1

    .line 173
    :cond_8
    const/16 v28, 0x5

    move/from16 v0, v19

    move/from16 v1, v28

    if-ne v0, v1, :cond_3

    if-eqz v11, :cond_3

    .line 174
    const/4 v4, 0x0

    goto :goto_2

    .line 177
    :cond_9
    const/16 v19, 0x0

    .line 178
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v26

    .line 179
    const/4 v11, 0x1

    goto :goto_2

    .line 190
    .end local v6    # "bytesRead":I
    .end local v12    # "foundSpeech":Z
    .end local v13    # "i":I
    .end local v17    # "rms":I
    :catch_0
    move-exception v7

    .line 191
    .local v7, "e":Ljava/io/IOException;
    const/4 v4, 0x0

    .line 192
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 196
    .end local v7    # "e":Ljava/io/IOException;
    .end local v24    # "sum":D
    :cond_a
    invoke-interface/range {v20 .. v20}, Lcom/vlingo/sdk/recognition/speech/VLSpeechDetector;->stopSpeechDetector()V

    .line 198
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->fos:Ljava/io/FileOutputStream;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/io/FileOutputStream;->close()V

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 207
    :goto_3
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v28

    if-eqz v28, :cond_b

    .line 208
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v28

    sget-object v29, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface/range {v28 .. v29}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v21

    .line 212
    .local v21, "startRecoTone":I
    :goto_4
    new-instance v28, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread$1;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread$1;-><init>(Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;)V

    move/from16 v0, v21

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playTone(ILcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 225
    return-void

    .line 200
    .end local v21    # "startRecoTone":I
    :catch_1
    move-exception v7

    .line 202
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 210
    .end local v7    # "e":Ljava/io/IOException;
    :cond_b
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v28

    sget-object v29, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface/range {v28 .. v29}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v21

    .restart local v21    # "startRecoTone":I
    goto :goto_4
.end method
