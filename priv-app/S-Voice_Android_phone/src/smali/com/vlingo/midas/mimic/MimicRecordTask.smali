.class public Lcom/vlingo/midas/mimic/MimicRecordTask;
.super Lcom/vlingo/midas/mimic/MimicBaseTask;
.source "MimicRecordTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;


# instance fields
.field private micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/vlingo/midas/mimic/MimicBaseTask;-><init>(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method static synthetic access$002(Lcom/vlingo/midas/mimic/MimicRecordTask;Lcom/vlingo/core/internal/audio/MicrophoneStream;)Lcom/vlingo/core/internal/audio/MicrophoneStream;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/mimic/MimicRecordTask;
    .param p1, "x1"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vlingo/midas/mimic/MimicRecordTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/mimic/MimicRecordTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/mimic/MimicRecordTask;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vlingo/midas/mimic/MimicRecordTask;->startRecording()V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/midas/mimic/MimicRecordTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/mimic/MimicRecordTask;

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/vlingo/midas/mimic/MimicRecordTask;->isCancelled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/mimic/MimicRecordTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/mimic/MimicRecordTask;

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/vlingo/midas/mimic/MimicRecordTask;->notifyFinished()V

    return-void
.end method

.method private getAudioType()Lcom/vlingo/core/internal/audio/AudioType;
    .locals 4

    .prologue
    .line 81
    sget-object v2, Lcom/vlingo/midas/mimic/MimicRecordTask;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;

    if-nez v2, :cond_0

    .line 82
    const-string/jumbo v2, "custom_tone_encoding"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "encoding":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioType;->valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/AudioType;

    move-result-object v2

    sput-object v2, Lcom/vlingo/midas/mimic/MimicRecordTask;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .end local v1    # "encoding":Ljava/lang/String;
    :cond_0
    :goto_0
    sget-object v2, Lcom/vlingo/midas/mimic/MimicRecordTask;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;

    return-object v2

    .line 85
    .restart local v1    # "encoding":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vlingo/core/internal/audio/AudioType;->PCM_22k:Lcom/vlingo/core/internal/audio/AudioType;

    sput-object v2, Lcom/vlingo/midas/mimic/MimicRecordTask;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;

    goto :goto_0
.end method

.method private startRecording()V
    .locals 4

    .prologue
    .line 95
    iget-object v2, p0, Lcom/vlingo/midas/mimic/MimicRecordTask;->notificationHandler:Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;

    sget-object v3, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;->notifyEvent(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    .line 97
    :try_start_0
    new-instance v1, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;

    iget-object v2, p0, Lcom/vlingo/midas/mimic/MimicRecordTask;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/vlingo/midas/mimic/MimicRecordTask;->micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-direct {v1, p0, v2, v3}, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;-><init>(Lcom/vlingo/midas/mimic/MimicRecordTask;Landroid/content/Context;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    .line 99
    .local v1, "rt":Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;
    invoke-virtual {v1}, Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;->start()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    .end local v1    # "rt":Lcom/vlingo/midas/mimic/MimicRecordTask$ReaderThread;
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 103
    invoke-virtual {p0}, Lcom/vlingo/midas/mimic/MimicRecordTask;->notifyFinished()V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, "startRecoTone":I
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v0

    .line 54
    :goto_0
    const-string/jumbo v1, "use_mediasync_tone_approach"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    new-instance v1, Lcom/vlingo/core/internal/audio/TonePlayer;

    invoke-direct {p0}, Lcom/vlingo/midas/mimic/MimicRecordTask;->getAudioType()Lcom/vlingo/core/internal/audio/AudioType;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/vlingo/core/internal/audio/TonePlayer;-><init>(ILcom/vlingo/core/internal/audio/AudioType;)V

    new-instance v2, Lcom/vlingo/midas/mimic/MimicRecordTask$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/mimic/MimicRecordTask$1;-><init>(Lcom/vlingo/midas/mimic/MimicRecordTask;)V

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/audio/TonePlayer;->play(Lcom/vlingo/core/internal/audio/TonePlayer$Listener;)V

    .line 78
    :goto_1
    return-void

    .line 50
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v0

    goto :goto_0

    .line 68
    :cond_1
    new-instance v1, Lcom/vlingo/midas/mimic/MimicRecordTask$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/mimic/MimicRecordTask$2;-><init>(Lcom/vlingo/midas/mimic/MimicRecordTask;)V

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->playTone(ILcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    goto :goto_1
.end method
