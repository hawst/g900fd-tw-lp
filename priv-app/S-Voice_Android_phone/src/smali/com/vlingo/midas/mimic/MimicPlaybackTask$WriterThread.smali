.class Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;
.super Ljava/lang/Thread;
.source "MimicPlaybackTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/mimic/MimicPlaybackTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WriterThread"
.end annotation


# instance fields
.field final bufferSize:I

.field private mimicTask:Lcom/vlingo/midas/mimic/MimicPlaybackTask;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/mimic/MimicPlaybackTask;)V
    .locals 1
    .param p1, "mimicTask"    # Lcom/vlingo/midas/mimic/MimicPlaybackTask;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 48
    const/16 v0, 0x140

    iput v0, p0, Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;->bufferSize:I

    .line 55
    iput-object p1, p0, Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicPlaybackTask;

    .line 56
    return-void
.end method


# virtual methods
.method public run()V
    .locals 23

    .prologue
    .line 59
    const/4 v14, 0x0

    .line 60
    .local v14, "fis":Ljava/io/FileInputStream;
    const/16 v20, 0x0

    .line 62
    .local v20, "m_oPlayer":Landroid/media/AudioTrack;
    const/16 v18, 0x140

    .line 63
    .local v18, "m_iPlayBufferSize":I
    :try_start_0
    new-instance v2, Landroid/media/AudioTrack;

    const/4 v3, 0x3

    const/16 v4, 0x3e80

    const/4 v5, 0x4

    const/4 v6, 0x2

    const/16 v7, 0x140

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Landroid/media/AudioTrack;-><init>(IIIIII)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    .end local v20    # "m_oPlayer":Landroid/media/AudioTrack;
    .local v2, "m_oPlayer":Landroid/media/AudioTrack;
    :try_start_1
    new-instance v15, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicPlaybackTask;

    iget-object v3, v3, Lcom/vlingo/midas/mimic/MimicPlaybackTask;->filePlay:Ljava/io/File;

    invoke-direct {v15, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 67
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .local v15, "fis":Ljava/io/FileInputStream;
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicPlaybackTask;

    iget-object v4, v4, Lcom/vlingo/midas/mimic/MimicPlaybackTask;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicPlaybackTask;

    iget-object v5, v5, Lcom/vlingo/midas/mimic/MimicPlaybackTask;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/lib"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 69
    .local v12, "dataDir":Ljava/lang/String;
    const-string/jumbo v3, "mimic_mode"

    const-string/jumbo v4, "-1"

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 70
    .local v21, "mimic_mode_str":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 71
    .local v17, "m_iModeIndex":I
    const/4 v3, -0x1

    move/from16 v0, v17

    if-ne v0, v3, :cond_0

    .line 72
    new-instance v22, Ljava/util/Random;

    invoke-direct/range {v22 .. v22}, Ljava/util/Random;-><init>()V

    .line 73
    .local v22, "rand":Ljava/util/Random;
    const/4 v3, 0x4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v17, v3, 0x1

    .line 78
    .end local v22    # "rand":Ljava/util/Random;
    :cond_0
    new-instance v19, Lcom/samsung/chatbot/ChatbotFilter;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/chatbot/ChatbotFilter;-><init>()V

    .line 79
    .local v19, "m_oChatbotFilter":Lcom/samsung/chatbot/ChatbotFilter;
    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lcom/samsung/chatbot/ChatbotFilter;->initialize(Ljava/lang/String;)I

    .line 80
    const/16 v3, 0xa0

    new-array v9, v3, [S

    .line 82
    .local v9, "asInOutBuffer":[S
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    const/16 v3, 0xa0

    move/from16 v0, v16

    if-ge v0, v3, :cond_1

    .line 83
    const/4 v3, 0x0

    aput-short v3, v9, v16

    .line 82
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 85
    :cond_1
    const/16 v3, 0x140

    new-array v10, v3, [B

    .line 87
    .local v10, "buffer":[B
    invoke-virtual {v2}, Landroid/media/AudioTrack;->play()V

    .line 88
    :goto_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v15}, Ljava/io/FileInputStream;->available()I

    move-result v3

    if-lez v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicPlaybackTask;

    # invokes: Lcom/vlingo/midas/mimic/MimicPlaybackTask;->isCancelled()Z
    invoke-static {v3}, Lcom/vlingo/midas/mimic/MimicPlaybackTask;->access$000(Lcom/vlingo/midas/mimic/MimicPlaybackTask;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 90
    const/4 v3, 0x0

    const/16 v4, 0x140

    invoke-virtual {v15, v10, v3, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v11

    .line 92
    .local v11, "bytesRead":I
    const/16 v16, 0x0

    :goto_2
    div-int/lit8 v3, v11, 0x2

    move/from16 v0, v16

    if-ge v0, v3, :cond_2

    .line 93
    mul-int/lit8 v3, v16, 0x2

    add-int/lit8 v3, v3, 0x1

    aget-byte v3, v10, v3

    shl-int/lit8 v3, v3, 0x8

    mul-int/lit8 v4, v16, 0x2

    aget-byte v4, v10, v4

    add-int/2addr v3, v4

    int-to-short v3, v3

    aput-short v3, v9, v16

    .line 92
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 97
    :cond_2
    div-int/lit8 v3, v11, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v9, v3, v1}, Lcom/samsung/chatbot/ChatbotFilter;->run([SII)I

    .line 99
    const/4 v3, 0x0

    div-int/lit8 v4, v11, 0x2

    invoke-virtual {v2, v9, v3, v4}, Landroid/media/AudioTrack;->write([SII)I
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    .line 103
    .end local v9    # "asInOutBuffer":[S
    .end local v10    # "buffer":[B
    .end local v11    # "bytesRead":I
    .end local v12    # "dataDir":Ljava/lang/String;
    .end local v16    # "i":I
    .end local v17    # "m_iModeIndex":I
    .end local v19    # "m_oChatbotFilter":Lcom/samsung/chatbot/ChatbotFilter;
    .end local v21    # "mimic_mode_str":Ljava/lang/String;
    :catch_0
    move-exception v13

    move-object v14, v15

    .line 104
    .end local v15    # "fis":Ljava/io/FileInputStream;
    .local v13, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    :goto_3
    :try_start_3
    invoke-virtual {v13}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 109
    if-eqz v14, :cond_3

    .line 110
    :try_start_4
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 115
    .end local v13    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    :goto_4
    if-eqz v2, :cond_4

    .line 116
    :try_start_5
    invoke-virtual {v2}, Landroid/media/AudioTrack;->stop()V

    .line 117
    invoke-virtual {v2}, Landroid/media/AudioTrack;->release()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 118
    const/4 v2, 0x0

    .line 124
    :cond_4
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;->mimicTask:Lcom/vlingo/midas/mimic/MimicPlaybackTask;

    # invokes: Lcom/vlingo/midas/mimic/MimicPlaybackTask;->notifyFinished()V
    invoke-static {v3}, Lcom/vlingo/midas/mimic/MimicPlaybackTask;->access$100(Lcom/vlingo/midas/mimic/MimicPlaybackTask;)V

    .line 125
    return-void

    .line 101
    .end local v14    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "asInOutBuffer":[S
    .restart local v10    # "buffer":[B
    .restart local v12    # "dataDir":Ljava/lang/String;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v16    # "i":I
    .restart local v17    # "m_iModeIndex":I
    .restart local v19    # "m_oChatbotFilter":Lcom/samsung/chatbot/ChatbotFilter;
    .restart local v21    # "mimic_mode_str":Ljava/lang/String;
    :cond_5
    :try_start_6
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 102
    const/4 v14, 0x0

    .line 109
    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    if-eqz v14, :cond_6

    .line 110
    :try_start_7
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 115
    :cond_6
    :goto_6
    if-eqz v2, :cond_4

    .line 116
    :try_start_8
    invoke-virtual {v2}, Landroid/media/AudioTrack;->stop()V

    .line 117
    invoke-virtual {v2}, Landroid/media/AudioTrack;->release()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 118
    const/4 v2, 0x0

    goto :goto_5

    .line 111
    :catch_1
    move-exception v13

    .line 112
    .local v13, "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 120
    .end local v13    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v13

    .line 121
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 111
    .end local v9    # "asInOutBuffer":[S
    .end local v10    # "buffer":[B
    .end local v12    # "dataDir":Ljava/lang/String;
    .end local v16    # "i":I
    .end local v17    # "m_iModeIndex":I
    .end local v19    # "m_oChatbotFilter":Lcom/samsung/chatbot/ChatbotFilter;
    .end local v21    # "mimic_mode_str":Ljava/lang/String;
    .local v13, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_3
    move-exception v13

    .line 112
    .local v13, "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 120
    .end local v13    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v13

    .line 121
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 105
    .end local v2    # "m_oPlayer":Landroid/media/AudioTrack;
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v20    # "m_oPlayer":Landroid/media/AudioTrack;
    :catch_5
    move-exception v13

    move-object/from16 v2, v20

    .line 106
    .end local v20    # "m_oPlayer":Landroid/media/AudioTrack;
    .restart local v2    # "m_oPlayer":Landroid/media/AudioTrack;
    .local v13, "e":Ljava/io/IOException;
    :goto_7
    :try_start_9
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 109
    if-eqz v14, :cond_7

    .line 110
    :try_start_a
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 115
    :cond_7
    :goto_8
    if-eqz v2, :cond_4

    .line 116
    :try_start_b
    invoke-virtual {v2}, Landroid/media/AudioTrack;->stop()V

    .line 117
    invoke-virtual {v2}, Landroid/media/AudioTrack;->release()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7

    .line 118
    const/4 v2, 0x0

    goto :goto_5

    .line 111
    :catch_6
    move-exception v13

    .line 112
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 120
    :catch_7
    move-exception v13

    .line 121
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 108
    .end local v2    # "m_oPlayer":Landroid/media/AudioTrack;
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v20    # "m_oPlayer":Landroid/media/AudioTrack;
    :catchall_0
    move-exception v3

    move-object/from16 v2, v20

    .line 109
    .end local v20    # "m_oPlayer":Landroid/media/AudioTrack;
    .restart local v2    # "m_oPlayer":Landroid/media/AudioTrack;
    :goto_9
    if-eqz v14, :cond_8

    .line 110
    :try_start_c
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 115
    :cond_8
    :goto_a
    if-eqz v2, :cond_9

    .line 116
    :try_start_d
    invoke-virtual {v2}, Landroid/media/AudioTrack;->stop()V

    .line 117
    invoke-virtual {v2}, Landroid/media/AudioTrack;->release()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_9

    .line 118
    const/4 v2, 0x0

    .line 122
    :cond_9
    :goto_b
    throw v3

    .line 111
    :catch_8
    move-exception v13

    .line 112
    .local v13, "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 120
    .end local v13    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v13

    .line 121
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_b

    .line 108
    .end local v13    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v3

    goto :goto_9

    .end local v14    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v3

    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_9

    .line 105
    :catch_a
    move-exception v13

    goto :goto_7

    .end local v14    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fis":Ljava/io/FileInputStream;
    :catch_b
    move-exception v13

    move-object v14, v15

    .end local v15    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fis":Ljava/io/FileInputStream;
    goto :goto_7

    .line 103
    .end local v2    # "m_oPlayer":Landroid/media/AudioTrack;
    .restart local v20    # "m_oPlayer":Landroid/media/AudioTrack;
    :catch_c
    move-exception v13

    move-object/from16 v2, v20

    .end local v20    # "m_oPlayer":Landroid/media/AudioTrack;
    .restart local v2    # "m_oPlayer":Landroid/media/AudioTrack;
    goto/16 :goto_3

    :catch_d
    move-exception v13

    goto/16 :goto_3
.end method
