.class Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;
.super Landroid/os/Handler;
.source "MimicBaseTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/mimic/MimicBaseTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MimicBaseTaskHandler"
.end annotation


# static fields
.field static final RMS:I = 0x2

.field static final STATE:I = 0x1


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/mimic/MimicBaseTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/mimic/MimicBaseTask;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/mimic/MimicBaseTask;

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 47
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 48
    return-void
.end method


# virtual methods
.method declared-synchronized clear()V
    .locals 1

    .prologue
    .line 76
    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;->removeMessages(I)V

    .line 77
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 51
    iget-object v1, p0, Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/mimic/MimicBaseTask;

    .line 52
    .local v0, "o":Lcom/vlingo/midas/mimic/MimicBaseTask;
    if-eqz v0, :cond_0

    .line 53
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 55
    :pswitch_0
    invoke-virtual {v0}, Lcom/vlingo/midas/mimic/MimicBaseTask;->getVlRecognitionListener()Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v0}, Lcom/vlingo/midas/mimic/MimicBaseTask;->getVlRecognitionListener()Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v2

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v2, v1}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onRmsChanged(I)V

    goto :goto_0

    .line 60
    :pswitch_1
    invoke-virtual {v0}, Lcom/vlingo/midas/mimic/MimicBaseTask;->getVlRecognitionListener()Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 61
    invoke-virtual {v0}, Lcom/vlingo/midas/mimic/MimicBaseTask;->getVlRecognitionListener()Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v2

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-interface {v2, v1}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onStateChanged(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    goto :goto_0

    .line 53
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method declared-synchronized notifyEvent(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 1
    .param p1, "newState"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    .line 72
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    monitor-exit p0

    return-void

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized notifyRmsChange(Ljava/lang/Object;)V
    .locals 1
    .param p1, "rmsValue"    # Ljava/lang/Object;

    .prologue
    .line 68
    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/vlingo/midas/mimic/MimicBaseTask$MimicBaseTaskHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    monitor-exit p0

    return-void

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
