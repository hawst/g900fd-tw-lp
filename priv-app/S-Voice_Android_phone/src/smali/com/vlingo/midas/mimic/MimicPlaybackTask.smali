.class public Lcom/vlingo/midas/mimic/MimicPlaybackTask;
.super Lcom/vlingo/midas/mimic/MimicBaseTask;
.source "MimicPlaybackTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/vlingo/midas/mimic/MimicBaseTask;-><init>(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/mimic/MimicPlaybackTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/mimic/MimicPlaybackTask;

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/vlingo/midas/mimic/MimicPlaybackTask;->isCancelled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/mimic/MimicPlaybackTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/mimic/MimicPlaybackTask;

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/vlingo/midas/mimic/MimicPlaybackTask;->notifyFinished()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/vlingo/midas/mimic/MimicPlaybackTask;->getVlRecognitionListener()Lcom/vlingo/sdk/recognition/VLRecognitionListener;

    move-result-object v1

    sget-object v2, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->THINKING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-interface {v1, v2}, Lcom/vlingo/sdk/recognition/VLRecognitionListener;->onStateChanged(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    .line 42
    new-instance v0, Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;-><init>(Lcom/vlingo/midas/mimic/MimicPlaybackTask;)V

    .line 43
    .local v0, "wt":Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;
    invoke-virtual {v0}, Lcom/vlingo/midas/mimic/MimicPlaybackTask$WriterThread;->start()V

    .line 44
    return-void
.end method
