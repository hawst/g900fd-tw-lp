.class public Lcom/vlingo/midas/MidasADMController;
.super Lcom/vlingo/core/internal/util/ADMController;
.source "MidasADMController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/MidasADMController$1;,
        Lcom/vlingo/midas/MidasADMController$RingerModeBroadcastReceiver;,
        Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;,
        Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;,
        Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;,
        Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;
    }
.end annotation


# static fields
.field public static AGGRESSIVE_NOISE_CANCELLATION:Ljava/lang/String; = null

.field public static DRIVING_MODE_GUI:Ljava/lang/String; = null

.field public static ENDPOINT_DETECTION:Ljava/lang/String; = null

.field public static EYES_FREE:Ljava/lang/String; = null

.field public static TALKBACK:Ljava/lang/String; = null

.field public static final X_VLCONF_APP_DRIVING_MODE:Ljava/lang/String; = "isInDrivingMode"

.field public static final X_VLCONF_PHONE_DRIVING_MODE:Ljava/lang/String; = "isPhoneInDrivingMode"


# instance fields
.field am:Landroid/media/AudioManager;

.field private bargeInSoFilePath:Ljava/lang/String;

.field private coverClosed:Z

.field private currentSoundProfile:I

.field drivingModeContentObserver:Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;

.field drivingModeSharedPreferenceChangeListener:Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;

.field private prevSoundProfile:I

.field private ringerModeBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private talkbackContentObserver:Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;

.field wasAppDrivingModeEnabled:Z

.field private wasPhoneDrivingModeEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-string/jumbo v0, "MidasFeature.AGGRESSIVE_NOISE_CANCELLATION"

    sput-object v0, Lcom/vlingo/midas/MidasADMController;->AGGRESSIVE_NOISE_CANCELLATION:Ljava/lang/String;

    .line 41
    const-string/jumbo v0, "MidasFeature.ENDPOINT_DETECTION"

    sput-object v0, Lcom/vlingo/midas/MidasADMController;->ENDPOINT_DETECTION:Ljava/lang/String;

    .line 42
    const-string/jumbo v0, "MidasFeature.EYES_FREE"

    sput-object v0, Lcom/vlingo/midas/MidasADMController;->EYES_FREE:Ljava/lang/String;

    .line 43
    const-string/jumbo v0, "MidasFeature.DRIVING_MODE_GUI"

    sput-object v0, Lcom/vlingo/midas/MidasADMController;->DRIVING_MODE_GUI:Ljava/lang/String;

    .line 44
    const-string/jumbo v0, "MidasFeature.TALKBACK"

    sput-object v0, Lcom/vlingo/midas/MidasADMController;->TALKBACK:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 198
    invoke-direct {p0}, Lcom/vlingo/core/internal/util/ADMController;-><init>()V

    .line 49
    iput-object v6, p0, Lcom/vlingo/midas/MidasADMController;->drivingModeContentObserver:Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;

    .line 50
    iput-object v6, p0, Lcom/vlingo/midas/MidasADMController;->drivingModeSharedPreferenceChangeListener:Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;

    .line 51
    iput-object v6, p0, Lcom/vlingo/midas/MidasADMController;->talkbackContentObserver:Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;

    .line 54
    iput-boolean v3, p0, Lcom/vlingo/midas/MidasADMController;->coverClosed:Z

    .line 58
    iput v1, p0, Lcom/vlingo/midas/MidasADMController;->currentSoundProfile:I

    .line 59
    iput v1, p0, Lcom/vlingo/midas/MidasADMController;->prevSoundProfile:I

    .line 61
    const-string/jumbo v1, "/system/lib/libsasr-jni.so"

    iput-object v1, p0, Lcom/vlingo/midas/MidasADMController;->bargeInSoFilePath:Ljava/lang/String;

    .line 199
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v4, "audio"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/vlingo/midas/MidasADMController;->am:Landroid/media/AudioManager;

    .line 201
    invoke-direct {p0}, Lcom/vlingo/midas/MidasADMController;->isAppCarModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/vlingo/midas/MidasADMController;->wasAppDrivingModeEnabled:Z

    .line 202
    invoke-direct {p0}, Lcom/vlingo/midas/MidasADMController;->isPhoneCarModeEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/vlingo/midas/MidasADMController;->wasPhoneDrivingModeEnabled:Z

    .line 204
    new-instance v1, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v4}, Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;-><init>(Lcom/vlingo/midas/MidasADMController;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/vlingo/midas/MidasADMController;->drivingModeContentObserver:Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;

    .line 205
    new-instance v1, Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;

    invoke-direct {v1, p0, v6}, Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;-><init>(Lcom/vlingo/midas/MidasADMController;Lcom/vlingo/midas/MidasADMController$1;)V

    iput-object v1, p0, Lcom/vlingo/midas/MidasADMController;->drivingModeSharedPreferenceChangeListener:Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;

    .line 206
    new-instance v1, Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v4}, Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;-><init>(Lcom/vlingo/midas/MidasADMController;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/vlingo/midas/MidasADMController;->talkbackContentObserver:Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;

    .line 208
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->getPhoneDrivingModeUri()Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/MidasADMController;->drivingModeContentObserver:Lcom/vlingo/midas/MidasADMController$DrivingModeContentObserver;

    invoke-virtual {v1, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 209
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v3, "accessibility_enabled"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/MidasADMController;->talkbackContentObserver:Lcom/vlingo/midas/MidasADMController$TalkbackContentObserver;

    invoke-virtual {v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 211
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/MidasADMController;->drivingModeSharedPreferenceChangeListener:Lcom/vlingo/midas/MidasADMController$DrivingModeSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 213
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    new-instance v1, Lcom/vlingo/midas/MidasADMController$RingerModeBroadcastReceiver;

    invoke-direct {v1, p0, v6}, Lcom/vlingo/midas/MidasADMController$RingerModeBroadcastReceiver;-><init>(Lcom/vlingo/midas/MidasADMController;Lcom/vlingo/midas/MidasADMController$1;)V

    iput-object v1, p0, Lcom/vlingo/midas/MidasADMController;->ringerModeBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 215
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/MidasADMController;->ringerModeBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string/jumbo v4, "android.media.RINGER_MODE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 220
    :cond_0
    new-instance v0, Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;

    invoke-direct {v0, p0, v6}, Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;-><init>(Lcom/vlingo/midas/MidasADMController;Lcom/vlingo/midas/MidasADMController$1;)V

    .line 221
    .local v0, "viewCoverEventBroadcastReceiver":Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "com.samsung.cover.OPEN"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 224
    return-void

    .end local v0    # "viewCoverEventBroadcastReceiver":Lcom/vlingo/midas/MidasADMController$ViewCoverEventBroadcastReceiver;
    :cond_1
    move v1, v3

    .line 201
    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/vlingo/midas/MidasADMController;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/MidasADMController;

    .prologue
    .line 36
    iget v0, p0, Lcom/vlingo/midas/MidasADMController;->prevSoundProfile:I

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/MidasADMController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/MidasADMController;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/vlingo/midas/MidasADMController;->prevSoundProfile:I

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/MidasADMController;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/MidasADMController;

    .prologue
    .line 36
    iget v0, p0, Lcom/vlingo/midas/MidasADMController;->currentSoundProfile:I

    return v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/MidasADMController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/MidasADMController;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/vlingo/midas/MidasADMController;->currentSoundProfile:I

    return p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/MidasADMController;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/MidasADMController;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vlingo/midas/MidasADMController;->processOnChange()V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/MidasADMController;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/MidasADMController;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/vlingo/midas/MidasADMController;->refreshFeatureStates()V

    return-void
.end method

.method private getCurrentSystemLanguage()Ljava/util/Locale;
    .locals 7

    .prologue
    .line 289
    :try_start_0
    const-string/jumbo v4, "android.app.ActivityManagerNative"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 290
    .local v0, "activityManagerNative":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v4, "getDefault"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 291
    .local v1, "am":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string/jumbo v5, "getConfiguration"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 292
    .local v2, "config":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string/jumbo v5, "locale"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    .end local v0    # "activityManagerNative":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "am":Ljava/lang/Object;
    .end local v2    # "config":Ljava/lang/Object;
    :goto_0
    return-object v4

    .line 294
    :catch_0
    move-exception v3

    .line 298
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    goto :goto_0
.end method

.method protected static getInstance()Lcom/vlingo/core/internal/util/ADMController;
    .locals 1

    .prologue
    .line 227
    sget-object v0, Lcom/vlingo/midas/MidasADMController;->instance:Lcom/vlingo/core/internal/util/ADMController;

    if-nez v0, :cond_0

    .line 228
    new-instance v0, Lcom/vlingo/midas/MidasADMController;

    invoke-direct {v0}, Lcom/vlingo/midas/MidasADMController;-><init>()V

    sput-object v0, Lcom/vlingo/midas/MidasADMController;->instance:Lcom/vlingo/core/internal/util/ADMController;

    .line 230
    :cond_0
    sget-object v0, Lcom/vlingo/midas/MidasADMController;->instance:Lcom/vlingo/core/internal/util/ADMController;

    return-object v0
.end method

.method private handleDrivingModeSettingChange(ZZ)V
    .locals 4
    .param p1, "enabled"    # Z
    .param p2, "isAppDrivingMode"    # Z

    .prologue
    .line 303
    if-eqz p1, :cond_2

    .line 304
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    if-eqz p2, :cond_0

    const-string/jumbo v0, "isInDrivingMode"

    :goto_0
    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeUserProperties(Ljava/lang/String;)V

    .line 305
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    if-eqz p2, :cond_1

    const-string/jumbo v0, "isInDrivingMode"

    :goto_1
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->addUserProperties(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :goto_2
    return-void

    .line 304
    :cond_0
    const-string/jumbo v0, "isPhoneInDrivingMode"

    goto :goto_0

    .line 305
    :cond_1
    const-string/jumbo v0, "isPhoneInDrivingMode"

    goto :goto_1

    .line 307
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    if-eqz p2, :cond_3

    const-string/jumbo v0, "isInDrivingMode"

    :goto_3
    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeUserProperties(Ljava/lang/String;)V

    .line 308
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    if-eqz p2, :cond_4

    const-string/jumbo v0, "isInDrivingMode"

    :goto_4
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->addUserProperties(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 307
    :cond_3
    const-string/jumbo v0, "isPhoneInDrivingMode"

    goto :goto_3

    .line 308
    :cond_4
    const-string/jumbo v0, "isPhoneInDrivingMode"

    goto :goto_4
.end method

.method private isAppCarModeEnabled()Z
    .locals 1

    .prologue
    .line 275
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v0

    return v0
.end method

.method private isBargeInAvailable()Z
    .locals 2

    .prologue
    .line 332
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/vlingo/midas/MidasADMController;->bargeInSoFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 333
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 334
    const/4 v1, 0x0

    .line 336
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static isDetailedTtsFeedback()Z
    .locals 2

    .prologue
    .line 284
    const-string/jumbo v0, "detailed_tts_feedback"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private isHeadsetModeEnabled()Z
    .locals 3

    .prologue
    .line 279
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 280
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isPhoneCarModeEnabled()Z
    .locals 1

    .prologue
    .line 271
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v0

    return v0
.end method

.method private processOnChange()V
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/vlingo/midas/MidasADMController;->refreshFeatureStates()V

    .line 235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/MidasADMController;->onBoot:Z

    .line 236
    invoke-virtual {p0}, Lcom/vlingo/midas/MidasADMController;->refreshAppModeStates()V

    .line 237
    return-void
.end method


# virtual methods
.method protected getFeatureStatus(Ljava/lang/String;)Z
    .locals 3
    .param p1, "feature"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 241
    sget-object v2, Lcom/vlingo/midas/MidasADMController;->AGGRESSIVE_NOISE_CANCELLATION:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 242
    invoke-direct {p0}, Lcom/vlingo/midas/MidasADMController;->isAppCarModeEnabled()Z

    move-result v0

    .line 252
    :cond_0
    :goto_0
    return v0

    .line 243
    :cond_1
    sget-object v2, Lcom/vlingo/midas/MidasADMController;->DRIVING_MODE_GUI:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 244
    invoke-direct {p0}, Lcom/vlingo/midas/MidasADMController;->isAppCarModeEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 245
    :cond_3
    sget-object v2, Lcom/vlingo/midas/MidasADMController;->ENDPOINT_DETECTION:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 246
    invoke-direct {p0}, Lcom/vlingo/midas/MidasADMController;->isAppCarModeEnabled()Z

    move-result v0

    goto :goto_0

    .line 247
    :cond_4
    sget-object v2, Lcom/vlingo/midas/MidasADMController;->EYES_FREE:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 248
    invoke-direct {p0}, Lcom/vlingo/midas/MidasADMController;->isAppCarModeEnabled()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-direct {p0}, Lcom/vlingo/midas/MidasADMController;->isHeadsetModeEnabled()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {}, Lcom/vlingo/midas/MidasADMController;->isDetailedTtsFeedback()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/vlingo/midas/MidasADMController;->isCoverClosed()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    move v1, v0

    :cond_6
    move v0, v1

    goto :goto_0

    .line 249
    :cond_7
    sget-object v0, Lcom/vlingo/midas/MidasADMController;->TALKBACK:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 250
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v0

    goto :goto_0

    .line 252
    :cond_8
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/util/ADMController;->getFeatureStatus(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isCoverClosed()Z
    .locals 1

    .prologue
    .line 340
    iget-boolean v0, p0, Lcom/vlingo/midas/MidasADMController;->coverClosed:Z

    return v0
.end method

.method protected refreshAppModeStates()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 257
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v2

    .line 258
    .local v2, "isTalkbackOn":Z
    invoke-direct {p0}, Lcom/vlingo/midas/MidasADMController;->isAppCarModeEnabled()Z

    move-result v5

    if-eqz v5, :cond_2

    if-nez v2, :cond_2

    move v0, v3

    .line 259
    .local v0, "isAppDrivingModeEnabled":Z
    :goto_0
    invoke-direct {p0}, Lcom/vlingo/midas/MidasADMController;->isPhoneCarModeEnabled()Z

    move-result v1

    .line 260
    .local v1, "isPhoneDrivingModeEnabled":Z
    iget-boolean v5, p0, Lcom/vlingo/midas/MidasADMController;->wasAppDrivingModeEnabled:Z

    if-eq v5, v0, :cond_0

    .line 261
    invoke-direct {p0, v0, v3}, Lcom/vlingo/midas/MidasADMController;->handleDrivingModeSettingChange(ZZ)V

    .line 262
    iput-boolean v0, p0, Lcom/vlingo/midas/MidasADMController;->wasAppDrivingModeEnabled:Z

    .line 264
    :cond_0
    iget-boolean v3, p0, Lcom/vlingo/midas/MidasADMController;->wasPhoneDrivingModeEnabled:Z

    if-eq v3, v1, :cond_1

    .line 265
    invoke-direct {p0, v1, v4}, Lcom/vlingo/midas/MidasADMController;->handleDrivingModeSettingChange(ZZ)V

    .line 266
    iput-boolean v1, p0, Lcom/vlingo/midas/MidasADMController;->wasPhoneDrivingModeEnabled:Z

    .line 268
    :cond_1
    return-void

    .end local v0    # "isAppDrivingModeEnabled":Z
    .end local v1    # "isPhoneDrivingModeEnabled":Z
    :cond_2
    move v0, v4

    .line 258
    goto :goto_0
.end method

.method public setCoverClosed(Z)V
    .locals 0
    .param p1, "coverClosed"    # Z

    .prologue
    .line 344
    iput-boolean p1, p0, Lcom/vlingo/midas/MidasADMController;->coverClosed:Z

    .line 345
    return-void
.end method
