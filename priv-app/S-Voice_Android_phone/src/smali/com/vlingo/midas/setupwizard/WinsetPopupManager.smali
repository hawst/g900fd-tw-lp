.class public Lcom/vlingo/midas/setupwizard/WinsetPopupManager;
.super Ljava/lang/Object;
.source "WinsetPopupManager.java"


# static fields
.field private static button:Landroid/widget/Button;

.field private static log:Lcom/vlingo/core/internal/logging/Logger;

.field private static winsetDialog:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13
    const-class v0, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 15
    sput-object v1, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->winsetDialog:Landroid/app/AlertDialog;

    .line 16
    sput-object v1, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->button:Landroid/widget/Button;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->winsetDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public static getWinsetPopupDailog(Landroid/content/Context;)Landroid/app/AlertDialog;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    sget-object v2, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->log:Lcom/vlingo/core/internal/logging/Logger;

    const-string/jumbo v3, "getWinsetPopupDailog()"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/logging/Logger;->debug(Ljava/lang/String;)V

    .line 20
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 21
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/vlingo/midas/R$layout;->winset_popup:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 22
    .local v1, "layout":Landroid/view/View;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    sput-object v2, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->winsetDialog:Landroid/app/AlertDialog;

    .line 24
    sget-object v2, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->winsetDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 25
    sget-object v2, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->winsetDialog:Landroid/app/AlertDialog;

    sget v3, Lcom/vlingo/midas/R$id;->ok_button:I

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    sput-object v2, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->button:Landroid/widget/Button;

    .line 26
    sget-object v2, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->button:Landroid/widget/Button;

    new-instance v3, Lcom/vlingo/midas/setupwizard/WinsetPopupManager$1;

    invoke-direct {v3}, Lcom/vlingo/midas/setupwizard/WinsetPopupManager$1;-><init>()V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    sget-object v2, Lcom/vlingo/midas/setupwizard/WinsetPopupManager;->winsetDialog:Landroid/app/AlertDialog;

    return-object v2
.end method
