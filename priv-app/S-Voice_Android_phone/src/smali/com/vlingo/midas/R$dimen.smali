.class public final Lcom/vlingo/midas/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_switch_padding:I = 0x7f0f000a

.field public static final contact_border_width:I = 0x7f0f0031

.field public static final dialogview_width:I = 0x7f0f0034

.field public static final drive_schedule_date_size:I = 0x7f0f000b

.field public static final drive_schedule_land_size:I = 0x7f0f000d

.field public static final drive_schedule_list_height:I = 0x7f0f000f

.field public static final drive_schedule_list_land_height:I = 0x7f0f0011

.field public static final drive_schedule_location_height:I = 0x7f0f0010

.field public static final drive_schedule_location_size:I = 0x7f0f000c

.field public static final drive_schedule_title_height:I = 0x7f0f0012

.field public static final drive_schedule_title_size:I = 0x7f0f000e

.field public static final guideline_height:I = 0x7f0f002d

.field public static final help_unlockbubble_land:I = 0x7f0f0025

.field public static final help_unlockbubble_port:I = 0x7f0f0024

.field public static final help_wakeupbubble_land:I = 0x7f0f0023

.field public static final help_wakeupbubble_port:I = 0x7f0f0022

.field public static final help_widget_image_size:I = 0x7f0f0008

.field public static final help_widget_title_size:I = 0x7f0f0009

.field public static final helpfragment_width:I = 0x7f0f003d

.field public static final item_application_choice_height:I = 0x7f0f003b

.field public static final item_contact_choice_height:I = 0x7f0f0038

.field public static final item_contact_list_height:I = 0x7f0f0032

.field public static final item_memo_choice_height:I = 0x7f0f0033

.field public static final item_naver_local_search_height:I = 0x7f0f0041

.field public static final item_naver_movie_choice_height:I = 0x7f0f0042

.field public static final item_schedule_choice_height:I = 0x7f0f0039

.field public static final item_social_type_choice_height:I = 0x7f0f003a

.field public static final item_task_choice_height:I = 0x7f0f003c

.field public static final keypad_detect_value_land:I = 0x7f0f001f

.field public static final keypad_detect_value_port:I = 0x7f0f001e

.field public static final land_scrolldialog_marginleft:I = 0x7f0f0013

.field public static final line_gap:I = 0x7f0f002c

.field public static final line_thickness:I = 0x7f0f002b

.field public static final padding_border:I = 0x7f0f0030

.field public static final padding_left:I = 0x7f0f002e

.field public static final padding_right:I = 0x7f0f002f

.field public static final relative_layout_margin_right:I = 0x7f0f0040

.field public static final scroll_view_padding:I = 0x7f0f003f

.field public static final uifocus_height:I = 0x7f0f0021

.field public static final uifocus_width:I = 0x7f0f0020

.field public static final user_bubble_height:I = 0x7f0f0036

.field public static final user_bubble_height_2line:I = 0x7f0f0037

.field public static final vlingo_bubble_height:I = 0x7f0f0035

.field public static final wakeup_dialog_padding_lr:I = 0x7f0f0027

.field public static final wakeup_dialog_padding_tb:I = 0x7f0f0028

.field public static final wakeup_dialog_text_size:I = 0x7f0f0026

.field public static final wcis_heading_size:I = 0x7f0f0006

.field public static final wcis_para_marginBottom:I = 0x7f0f0005

.field public static final wcis_para_marginTop:I = 0x7f0f0004

.field public static final wcis_subheading_size:I = 0x7f0f0007

.field public static final weather_weekday_date:I = 0x7f0f0044

.field public static final weather_weekday_text_size:I = 0x7f0f0043

.field public static final widget_layout_width:I = 0x7f0f003e

.field public static final widget_margin_left:I = 0x7f0f0000

.field public static final widget_margin_right:I = 0x7f0f0001

.field public static final widget_naver_map_height:I = 0x7f0f0015

.field public static final widget_naver_map_width:I = 0x7f0f0014

.field public static final widget_padding_bottom:I = 0x7f0f0003

.field public static final widget_padding_top:I = 0x7f0f0002

.field public static final widget_weather_accu_bottom_margin:I = 0x7f0f0018

.field public static final widget_weather_citi_max_width:I = 0x7f0f001b

.field public static final widget_weather_citi_top_margin:I = 0x7f0f0016

.field public static final widget_weather_climate_bottom_margin:I = 0x7f0f0019

.field public static final widget_weather_icon_top_margin:I = 0x7f0f001c

.field public static final widget_weather_left_right_margin:I = 0x7f0f0017

.field public static final widget_weather_temprature_bottom_margin:I = 0x7f0f001a

.field public static final widget_weather_weekly_icon_top_margin:I = 0x7f0f001d

.field public static final you_can_say_padding_top_land:I = 0x7f0f0029

.field public static final you_can_say_padding_top_portrait:I = 0x7f0f002a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
