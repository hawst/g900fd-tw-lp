.class public final Lcom/vlingo/midas/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final CustomCommandHint:I = 0x7f120073

.field public static final ImageView01:I = 0x7f1200f2

.field public static final ItemLocalChoiceContainer:I = 0x7f120178

.field public static final ItemMovieChoiceContainer:I = 0x7f12016c

.field public static final LinearLayout01:I = 0x7f1201fa

.field public static final LinearLayout03:I = 0x7f1201c6

.field public static final ListView:I = 0x7f1201d9

.field public static final Local_MainTitlee_LinearLayout:I = 0x7f120283

.field public static final MessageLayout:I = 0x7f120231

.field public static final RelativeLayout01:I = 0x7f12004a

.field public static final ScrollView01:I = 0x7f1201c5

.field public static final Svoice:I = 0x7f1200f6

.field public static final TextView02:I = 0x7f1200ee

.field public static final View01:I = 0x7f120150

.field public static final View02:I = 0x7f12014e

.field public static final View03:I = 0x7f12014f

.field public static final about_layout:I = 0x7f1200ea

.field public static final about_layout_1:I = 0x7f1200f7

.field public static final about_layout_2:I = 0x7f1200f9

.field public static final accu_weather:I = 0x7f1202f4

.field public static final action_info:I = 0x7f120258

.field public static final adapt_button:I = 0x7f120090

.field public static final alarm_day_container:I = 0x7f12011b

.field public static final alarm_divider:I = 0x7f120112

.field public static final alarm_enable:I = 0x7f120111

.field public static final alarm_ic_l_in:I = 0x7f12011c

.field public static final alarm_item_ampm:I = 0x7f120108

.field public static final alarm_item_ampmnext:I = 0x7f120115

.field public static final alarm_item_ampmprevious:I = 0x7f120114

.field public static final alarm_item_center:I = 0x7f120116

.field public static final alarm_item_colon:I = 0x7f120106

.field public static final alarm_item_friday:I = 0x7f12010d

.field public static final alarm_item_hour:I = 0x7f120105

.field public static final alarm_item_leftside:I = 0x7f120103

.field public static final alarm_item_min:I = 0x7f120107

.field public static final alarm_item_monday:I = 0x7f120109

.field public static final alarm_item_saturday:I = 0x7f12010e

.field public static final alarm_item_subject:I = 0x7f120119

.field public static final alarm_item_sunday:I = 0x7f12010f

.field public static final alarm_item_thursday:I = 0x7f12010c

.field public static final alarm_item_time_layout:I = 0x7f120104

.field public static final alarm_item_time_layout1:I = 0x7f12011d

.field public static final alarm_item_tuesday:I = 0x7f12010a

.field public static final alarm_item_wednesday:I = 0x7f12010b

.field public static final alarm_repeat:I = 0x7f12011a

.field public static final alarm_text_container:I = 0x7f120118

.field public static final alarm_vertical_divider:I = 0x7f120110

.field public static final always_accept_radio_btn:I = 0x7f120004

.field public static final always_content_url:I = 0x7f120008

.field public static final always_decline_radio_btn:I = 0x7f120009

.field public static final always_on_content:I = 0x7f120005

.field public static final always_on_scrollview:I = 0x7f120003

.field public static final answer_scrollview:I = 0x7f12020f

.field public static final answer_text:I = 0x7f120210

.field public static final arrow:I = 0x7f1201ef

.field public static final asr_http_timeout_layout:I = 0x7f12000d

.field public static final asr_http_timeout_preference_text:I = 0x7f12000e

.field public static final audioArtist:I = 0x7f1201b4

.field public static final audioClose:I = 0x7f120275

.field public static final audioDivider:I = 0x7f1201b5

.field public static final audioImage:I = 0x7f1201b2

.field public static final audioNext:I = 0x7f120274

.field public static final audioPlay:I = 0x7f120273

.field public static final audioPrevious:I = 0x7f120272

.field public static final audioTemplateR1Layout:I = 0x7f12026f

.field public static final audioTemplateRLayout:I = 0x7f1201b1

.field public static final audioTitle:I = 0x7f1201b3

.field public static final audioVolumnImage:I = 0x7f120270

.field public static final body:I = 0x7f120168

.field public static final bottom_layout:I = 0x7f1200b7

.field public static final btn_Local_Call:I = 0x7f12027d

.field public static final btn_Local_More:I = 0x7f12027f

.field public static final btn_Local_Route:I = 0x7f12027e

.field public static final btn_Movie_Bookinglink:I = 0x7f120267

.field public static final btn_Movie_Detaillink:I = 0x7f120266

.field public static final btn_Movie_Samenameslink:I = 0x7f120268

.field public static final btn_Region_More:I = 0x7f12028c

.field public static final btn_Region_Route:I = 0x7f12028b

.field public static final btn_callback:I = 0x7f120254

.field public static final btn_cancel:I = 0x7f12024f

.field public static final btn_cancel_schedule:I = 0x7f1202a9

.field public static final btn_cancel_social:I = 0x7f12023a

.field public static final btn_cancel_task:I = 0x7f1202c3

.field public static final btn_dim_kitkat:I = 0x7f120066

.field public static final btn_divider:I = 0x7f120091

.field public static final btn_helpanim:I = 0x7f120069

.field public static final btn_helpanim_ll:I = 0x7f120199

.field public static final btn_helpbubble:I = 0x7f120067

.field public static final btn_helpbubble_ll:I = 0x7f12019a

.field public static final btn_idle:I = 0x7f120058

.field public static final btn_idle_kitkat:I = 0x7f120055

.field public static final btn_idle_kitkat_land:I = 0x7f120196

.field public static final btn_idle_land:I = 0x7f120193

.field public static final btn_listening_backlayer:I = 0x7f12005b

.field public static final btn_listening_kitkat:I = 0x7f12005c

.field public static final btn_listening_kitkat_land:I = 0x7f120195

.field public static final btn_listening_land:I = 0x7f12019b

.field public static final btn_listening_mic_kitkat:I = 0x7f12005d

.field public static final btn_menu_kitkat:I = 0x7f120063

.field public static final btn_micanim_land:I = 0x7f12019c

.field public static final btn_more:I = 0x7f12025b

.field public static final btn_more_news:I = 0x7f120297

.field public static final btn_next:I = 0x7f12022e

.field public static final btn_prev:I = 0x7f12022d

.field public static final btn_privacy:I = 0x7f1200f4

.field public static final btn_readout:I = 0x7f120256

.field public static final btn_reply:I = 0x7f120253

.field public static final btn_save:I = 0x7f1201ba

.field public static final btn_send:I = 0x7f120250

.field public static final btn_textview:I = 0x7f12018e

.field public static final btn_thinking:I = 0x7f120057

.field public static final btn_thinking_kitkat:I = 0x7f120064

.field public static final btn_thinking_kitkat_bg:I = 0x7f12005f

.field public static final btn_thinking_kitkat_bg1:I = 0x7f12005e

.field public static final btn_thinking_land:I = 0x7f120192

.field public static final btn_toggle:I = 0x7f12005a

.field public static final btn_tos:I = 0x7f1200f3

.field public static final buffer:I = 0x7f1200a3

.field public static final button:I = 0x7f120213

.field public static final button_call:I = 0x7f120143

.field public static final button_divider1:I = 0x7f120280

.field public static final button_divider2:I = 0x7f120281

.field public static final button_done:I = 0x7f1201e8

.field public static final button_layout:I = 0x7f12000b

.field public static final button_map:I = 0x7f120144

.field public static final button_navigate:I = 0x7f120145

.field public static final button_reserve:I = 0x7f12014d

.field public static final button_set_home_address:I = 0x7f120051

.field public static final button_web:I = 0x7f120146

.field public static final chatbot_xml:I = 0x7f120218

.field public static final checkBox:I = 0x7f12006c

.field public static final child_accuweather:I = 0x7f120016

.field public static final child_date1:I = 0x7f120026

.field public static final child_date2:I = 0x7f12002c

.field public static final child_date3:I = 0x7f120032

.field public static final child_date4:I = 0x7f120038

.field public static final child_date5:I = 0x7f12003e

.field public static final child_date6:I = 0x7f120044

.field public static final child_image1:I = 0x7f120027

.field public static final child_image2:I = 0x7f12002d

.field public static final child_image3:I = 0x7f120033

.field public static final child_image4:I = 0x7f120039

.field public static final child_image5:I = 0x7f12003f

.field public static final child_image6:I = 0x7f120045

.field public static final child_layout1:I = 0x7f120024

.field public static final child_layout2:I = 0x7f12002a

.field public static final child_layout3:I = 0x7f120030

.field public static final child_layout4:I = 0x7f120036

.field public static final child_layout5:I = 0x7f12003c

.field public static final child_layout6:I = 0x7f120042

.field public static final child_max_temp1:I = 0x7f120028

.field public static final child_max_temp2:I = 0x7f12002e

.field public static final child_max_temp3:I = 0x7f120034

.field public static final child_max_temp4:I = 0x7f12003a

.field public static final child_max_temp5:I = 0x7f120040

.field public static final child_max_temp6:I = 0x7f120046

.field public static final child_min_temp1:I = 0x7f120029

.field public static final child_min_temp2:I = 0x7f12002f

.field public static final child_min_temp3:I = 0x7f120035

.field public static final child_min_temp4:I = 0x7f12003b

.field public static final child_min_temp5:I = 0x7f120041

.field public static final child_min_temp6:I = 0x7f120047

.field public static final child_weekday1:I = 0x7f120025

.field public static final child_weekday2:I = 0x7f12002b

.field public static final child_weekday3:I = 0x7f120031

.field public static final child_weekday4:I = 0x7f120037

.field public static final child_weekday5:I = 0x7f12003d

.field public static final child_weekday6:I = 0x7f120043

.field public static final chineselogo_localsearch:I = 0x7f120159

.field public static final citi_detail:I = 0x7f12001f

.field public static final citi_details:I = 0x7f120013

.field public static final citi_name:I = 0x7f120014

.field public static final climate_name:I = 0x7f12001e

.field public static final clockContainer:I = 0x7f12021a

.field public static final clock_colon:I = 0x7f12021f

.field public static final clock_country_text:I = 0x7f12022a

.field public static final clock_horizontal_divider:I = 0x7f120222

.field public static final clock_hour_postfix:I = 0x7f12021e

.field public static final clock_hour_prefix:I = 0x7f12021d

.field public static final clock_min_postfix:I = 0x7f120221

.field public static final clock_min_prefix:I = 0x7f120220

.field public static final clock_month_years_text:I = 0x7f120226

.field public static final clock_time_ampm_text:I = 0x7f12021b

.field public static final clock_time_ampm_text_korean:I = 0x7f120228

.field public static final clock_time_day_text:I = 0x7f120224

.field public static final clock_time_text:I = 0x7f120229

.field public static final clock_vertical_divider:I = 0x7f120225

.field public static final closeButton:I = 0x7f120080

.field public static final compose_social_button_container:I = 0x7f120239

.field public static final contact_detail_list:I = 0x7f120242

.field public static final contact_detail_list_container:I = 0x7f120241

.field public static final contact_name:I = 0x7f120233

.field public static final container_Actors:I = 0x7f120263

.field public static final container_Director:I = 0x7f120262

.field public static final container_Grade:I = 0x7f120264

.field public static final content:I = 0x7f120001

.field public static final control_full_container:I = 0x7f120052

.field public static final control_full_container_land:I = 0x7f12018f

.field public static final count:I = 0x7f120167

.field public static final counts:I = 0x7f12025a

.field public static final customCommandHintLayout:I = 0x7f120071

.field public static final custom_button_container:I = 0x7f120214

.field public static final custom_command_rl_ll:I = 0x7f12006e

.field public static final custom_dialog_warning_textView:I = 0x7f12006b

.field public static final custom_wake_up_end_body:I = 0x7f12008d

.field public static final custom_wake_up_error_body:I = 0x7f120094

.field public static final custom_wake_up_layout:I = 0x7f1201e3

.field public static final custom_wake_up_record:I = 0x7f120096

.field public static final custom_wake_up_record_end:I = 0x7f12008c

.field public static final custom_wake_up_record_error:I = 0x7f120093

.field public static final custom_wakeup_command:I = 0x7f12006d

.field public static final custom_wakeup_setting_bubble_tv_1:I = 0x7f1201ec

.field public static final custom_wakeup_setting_bubble_tv_2:I = 0x7f1201ed

.field public static final date_msg:I = 0x7f120169

.field public static final date_msg_detail:I = 0x7f12024d

.field public static final date_msg_image:I = 0x7f12024c

.field public static final date_msg_src:I = 0x7f120166

.field public static final date_view:I = 0x7f120015

.field public static final dayContainer:I = 0x7f120223

.field public static final detail_top:I = 0x7f12023e

.field public static final devider_small:I = 0x7f1202f7

.field public static final dialog_bubble_user_s:I = 0x7f120087

.field public static final dialog_bubble_vlingo_s:I = 0x7f120089

.field public static final dialog_bubble_wakeup_s:I = 0x7f12008b

.field public static final dialog_full_container:I = 0x7f1200a0

.field public static final dialog_full_shadowbelow:I = 0x7f1200a4

.field public static final dialog_scroll_content:I = 0x7f1200a2

.field public static final dialog_scrollview:I = 0x7f1200a1

.field public static final dialog_view:I = 0x7f1201a0

.field public static final disclaimer_checkbox:I = 0x7f1200ac

.field public static final disclaimer_content:I = 0x7f1200af

.field public static final disclaimer_title:I = 0x7f1200ae

.field public static final disclaimer_tos_scrollview:I = 0x7f1200ad

.field public static final divider:I = 0x7f120126

.field public static final divider_small:I = 0x7f12001c

.field public static final done_btn_container:I = 0x7f1201f4

.field public static final done_button:I = 0x7f120092

.field public static final dont_ask:I = 0x7f1200aa

.field public static final drive_schedule_choice_full_container:I = 0x7f12012a

.field public static final driving_mode_custom_notification_close_btn:I = 0x7f1200bf

.field public static final driving_mode_custom_notification_close_btn_container:I = 0x7f1200be

.field public static final driving_mode_custom_notification_left_container:I = 0x7f1200ba

.field public static final driving_mode_custom_notification_summary:I = 0x7f1200bd

.field public static final driving_mode_custom_notification_thumnail:I = 0x7f1200bb

.field public static final driving_mode_custom_notification_title:I = 0x7f1200bc

.field public static final dummy_widget:I = 0x7f1202e9

.field public static final dummy_widget_ll:I = 0x7f1202f3

.field public static final edit_address:I = 0x7f1201b7

.field public static final edit_shortcutname:I = 0x7f1201b9

.field public static final edittext:I = 0x7f12009f

.field public static final end_body:I = 0x7f120077

.field public static final erase_server_data_body:I = 0x7f1200c2

.field public static final erase_server_data_btn:I = 0x7f1200c8

.field public static final erase_server_data_detail_body:I = 0x7f1200c0

.field public static final erase_server_data_detail_btn:I = 0x7f1200c1

.field public static final erase_server_data_sub1:I = 0x7f1200c3

.field public static final erase_server_data_sub2:I = 0x7f1200c4

.field public static final erase_server_data_sub3:I = 0x7f1200c5

.field public static final erase_server_data_sub4:I = 0x7f1200c6

.field public static final erase_server_data_sub5:I = 0x7f1200c7

.field public static final error_body:I = 0x7f120075

.field public static final event_divider:I = 0x7f120180

.field public static final event_image:I = 0x7f120181

.field public static final event_list_check:I = 0x7f12017e

.field public static final event_time:I = 0x7f120183

.field public static final event_title:I = 0x7f120182

.field public static final facebookIcon:I = 0x7f120237

.field public static final finish:I = 0x7f1201f7

.field public static final first_colon:I = 0x7f1202dc

.field public static final first_colon_bg:I = 0x7f1202d0

.field public static final fragment_description:I = 0x7f1201c3

.field public static final fragment_placeholder:I = 0x7f1200c9

.field public static final from:I = 0x7f120165

.field public static final full_container:I = 0x7f12019e

.field public static final full_container_dialogbubble:I = 0x7f120082

.field public static final functionList:I = 0x7f1201ea

.field public static final head_text_hr:I = 0x7f1202c9

.field public static final head_text_min:I = 0x7f1202ca

.field public static final head_text_sec:I = 0x7f1202cb

.field public static final header1_examples:I = 0x7f120200

.field public static final header_did_you_know:I = 0x7f12004f

.field public static final header_examples:I = 0x7f12004d

.field public static final helpButtonPotraitLand:I = 0x7f1201aa

.field public static final helpButtonPotraitLayout:I = 0x7f1201a6

.field public static final help_about_bottom_container:I = 0x7f1200f5

.field public static final help_about_text_powered_by:I = 0x7f1200f1

.field public static final help_about_title:I = 0x7f1200eb

.field public static final help_anim_layoutland:I = 0x7f1201a9

.field public static final help_anim_layoutpotrt:I = 0x7f1201a2

.field public static final help_bubble_helpbutton_land:I = 0x7f1201ab

.field public static final help_bubble_helpbutton_ptrt:I = 0x7f1201a7

.field public static final help_bubble_micbutton_land:I = 0x7f1201ae

.field public static final help_bubble_micbutton_ptrt:I = 0x7f1201a4

.field public static final help_choice_LinearLayout:I = 0x7f1200cb

.field public static final help_choice_ScrollView:I = 0x7f1200cd

.field public static final help_choice_image:I = 0x7f1201fc

.field public static final help_choice_text1:I = 0x7f1201fd

.field public static final help_choice_text2:I = 0x7f1201fe

.field public static final help_choice_textview:I = 0x7f120202

.field public static final help_done_bubble_tv_land:I = 0x7f1200e1

.field public static final help_done_bubble_tv_port:I = 0x7f1200e3

.field public static final help_done_layout_land:I = 0x7f1200e0

.field public static final help_done_layout_port:I = 0x7f1200e2

.field public static final help_edit_bubble_layout_land:I = 0x7f1200d2

.field public static final help_edit_bubble_layout_port:I = 0x7f1200d7

.field public static final help_edit_bubble_tv_land:I = 0x7f1200d3

.field public static final help_edit_bubble_tv_port:I = 0x7f1200d8

.field public static final help_edit_layout_land:I = 0x7f1200d0

.field public static final help_edit_layout_port:I = 0x7f1200d5

.field public static final help_edit_tap_iv_land:I = 0x7f1200d1

.field public static final help_edit_tap_iv_port:I = 0x7f1200d6

.field public static final help_edit_tip_iv_land:I = 0x7f1200d4

.field public static final help_edit_tip_iv_port:I = 0x7f1200d9

.field public static final help_expander_icon:I = 0x7f120203

.field public static final help_keypad_bubble_tv_land:I = 0x7f1200dc

.field public static final help_keypad_bubble_tv_port:I = 0x7f1200df

.field public static final help_keypad_layout_land:I = 0x7f1200da

.field public static final help_keypad_layout_port:I = 0x7f1200dd

.field public static final help_keypad_tip_iv_land:I = 0x7f1200db

.field public static final help_keypad_tip_iv_port:I = 0x7f1200de

.field public static final help_layout:I = 0x7f1200fc

.field public static final help_layout_parent:I = 0x7f1201a1

.field public static final help_popup_bubble_voice_control:I = 0x7f1201db

.field public static final help_popup_bubble_voice_control_tip:I = 0x7f1201dc

.field public static final help_popup_picker_voice_control:I = 0x7f1201c4

.field public static final help_popup_tap1:I = 0x7f1200cf

.field public static final help_usv_list_btn_land:I = 0x7f1200e6

.field public static final help_usv_list_btn_port:I = 0x7f1200e9

.field public static final help_usv_list_bubble_tv_land:I = 0x7f1200e5

.field public static final help_usv_list_bubble_tv_port:I = 0x7f1200e8

.field public static final help_usv_list_layout_land:I = 0x7f1200e4

.field public static final help_usv_list_layout_port:I = 0x7f1200e7

.field public static final hi_galaxy_send_a_message:I = 0x7f120006

.field public static final hi_galaxy_text:I = 0x7f1202ed

.field public static final hour_glass_view:I = 0x7f120060

.field public static final httpsrollout_preference_seekbar:I = 0x7f1200fd

.field public static final httpsrollout_preference_text:I = 0x7f1200fe

.field public static final icon:I = 0x7f1201c2

.field public static final iconLayout:I = 0x7f120236

.field public static final image:I = 0x7f120139

.field public static final imageViewAnim:I = 0x7f1200fb

.field public static final imageView_singledayweather:I = 0x7f120012

.field public static final image_Moive_Rank:I = 0x7f12016f

.field public static final image_Movie_Grade:I = 0x7f120171

.field public static final image_Movie_Playing:I = 0x7f12025d

.field public static final image_Movie_Poster:I = 0x7f12016d

.field public static final image_Region_Map:I = 0x7f12027b

.field public static final image_Region_Map_Cover:I = 0x7f12027c

.field public static final image_contact:I = 0x7f120122

.field public static final image_contact_detail:I = 0x7f12023f

.field public static final image_logo:I = 0x7f1200ed

.field public static final image_logo_samsung:I = 0x7f1200ec

.field public static final img_thinking_tile_kitkat:I = 0x7f120065

.field public static final initbody:I = 0x7f120072

.field public static final introduction_content:I = 0x7f120101

.field public static final introduction_main:I = 0x7f120100

.field public static final item_application_choice_divider:I = 0x7f120120

.field public static final item_application_choice_thumnail:I = 0x7f12011e

.field public static final item_choice_container:I = 0x7f120121

.field public static final item_you_can_say_widget_command:I = 0x7f120185

.field public static final item_you_can_say_widget_divider1:I = 0x7f12018b

.field public static final item_you_can_say_widget_examples:I = 0x7f12018d

.field public static final item_you_can_say_widget_examples_container:I = 0x7f12018c

.field public static final item_you_can_say_widget_thumnail:I = 0x7f120187

.field public static final item_you_can_say_widget_thumnail1:I = 0x7f120189

.field public static final item_you_can_say_widget_thumnail2:I = 0x7f12018a

.field public static final item_you_can_say_widget_title:I = 0x7f120188

.field public static final label:I = 0x7f12011f

.field public static final land_control_view:I = 0x7f120079

.field public static final layout:I = 0x7f1200a8

.field public static final layout_action_button_labels:I = 0x7f120147

.field public static final layout_action_button_labels_2:I = 0x7f120151

.field public static final layout_action_buttons:I = 0x7f120142

.field public static final layout_action_buttons_2:I = 0x7f12014c

.field public static final layout_details:I = 0x7f120153

.field public static final layout_overview:I = 0x7f12013a

.field public static final layout_reviews:I = 0x7f120158

.field public static final layout_title:I = 0x7f12015b

.field public static final left_container:I = 0x7f120048

.field public static final linearLayout1:I = 0x7f12007b

.field public static final linearLayout2:I = 0x7f12007d

.field public static final linear_schedule:I = 0x7f1202b5

.field public static final link1:I = 0x7f1200b4

.field public static final link2:I = 0x7f1200b5

.field public static final listMainLayout:I = 0x7f1201d8

.field public static final list_divider:I = 0x7f120129

.field public static final listview:I = 0x7f12022b

.field public static final local_list_divider:I = 0x7f120137

.field public static final local_list_textContainer:I = 0x7f120130

.field public static final local_search_info:I = 0x7f12012f

.field public static final logo:I = 0x7f12026e

.field public static final logoImage:I = 0x7f1202e4

.field public static final logo_k:I = 0x7f12026d

.field public static final logo_layout:I = 0x7f120205

.field public static final logo_nhn:I = 0x7f12017b

.field public static final ls_item_address:I = 0x7f120135

.field public static final ls_item_call:I = 0x7f120136

.field public static final ls_item_category:I = 0x7f12017a

.field public static final ls_item_distance:I = 0x7f120179

.field public static final ls_item_name:I = 0x7f120131

.field public static final ls_item_rate_img:I = 0x7f120133

.field public static final ls_item_ratingbar:I = 0x7f120132

.field public static final ls_item_review_count:I = 0x7f120134

.field public static final mImageViewId:I = 0x7f12012d

.field public static final mTextViewId:I = 0x7f12012e

.field public static final mainControlLL:I = 0x7f120054

.field public static final mainControlLL_land:I = 0x7f120190

.field public static final mainListView:I = 0x7f1201f1

.field public static final mainTextLL:I = 0x7f1202eb

.field public static final main_container:I = 0x7f120113

.field public static final main_layout:I = 0x7f120023

.field public static final main_layout_k:I = 0x7f1200ff

.field public static final main_layout_tutorial:I = 0x7f1201c9

.field public static final max_temp:I = 0x7f12001b

.field public static final memo_container:I = 0x7f120246

.field public static final memo_container1:I = 0x7f120247

.field public static final memo_content:I = 0x7f120160

.field public static final memo_date:I = 0x7f120161

.field public static final memo_divider:I = 0x7f120163

.field public static final memo_listview:I = 0x7f120248

.field public static final memo_time:I = 0x7f120162

.field public static final menu_layout_kitkat:I = 0x7f120062

.field public static final menu_layout_kitkat_land:I = 0x7f120197

.field public static final message_button_container:I = 0x7f12022c

.field public static final message_container:I = 0x7f120249

.field public static final message_divider:I = 0x7f12024e

.field public static final message_image_contact:I = 0x7f12016b

.field public static final message_readback_button_container:I = 0x7f120252

.field public static final message_row_title_line:I = 0x7f12016a

.field public static final message_title_from_to:I = 0x7f12024b

.field public static final micButtonLandLayout:I = 0x7f1201ad

.field public static final micButtonPotraitLayout:I = 0x7f1201a3

.field public static final micSvoice:I = 0x7f1201d7

.field public static final mic_animation_container:I = 0x7f1201b0

.field public static final mic_background_mini_mode:I = 0x7f12019d

.field public static final mic_btn_layout:I = 0x7f120056

.field public static final mic_btn_layout_kitkat:I = 0x7f120053

.field public static final mic_btn_layout_kitkat_land:I = 0x7f120194

.field public static final mic_btn_layout_land:I = 0x7f120191

.field public static final mic_idle_kitkat:I = 0x7f120061

.field public static final mic_status_layout:I = 0x7f1200a6

.field public static final mic_status_txt:I = 0x7f120059

.field public static final min_temp:I = 0x7f12001d

.field public static final more_button:I = 0x7f120296

.field public static final move_to_svoice:I = 0x7f1201cd

.field public static final move_to_tutorial:I = 0x7f1201d0

.field public static final movie_button_divider1:I = 0x7f120269

.field public static final movie_button_divider2:I = 0x7f12026a

.field public static final movie_divider:I = 0x7f120177

.field public static final msg_body:I = 0x7f120235

.field public static final msg_title:I = 0x7f12024a

.field public static final musicShadow:I = 0x7f120271

.field public static final myPlacecheckBox:I = 0x7f1201df

.field public static final myTitle:I = 0x7f12007c

.field public static final naver_container:I = 0x7f120276

.field public static final news_cp_who:I = 0x7f120295

.field public static final next_btn:I = 0x7f12000a

.field public static final next_btn_img:I = 0x7f1200b9

.field public static final next_btn_txt:I = 0x7f1200b8

.field public static final next_button_layout:I = 0x7f120208

.field public static final normal_layout:I = 0x7f12019f

.field public static final normal_layout_custom:I = 0x7f12007a

.field public static final normal_news_below_container:I = 0x7f120292

.field public static final normal_news_cp_logo:I = 0x7f120294

.field public static final normal_news_detail:I = 0x7f120293

.field public static final normal_news_divider:I = 0x7f120291

.field public static final normal_news_full_container:I = 0x7f12028d

.field public static final normal_news_headline:I = 0x7f12028f

.field public static final normal_news_top_container:I = 0x7f12028e

.field public static final normal_news_updateDate:I = 0x7f120290

.field public static final notice_popup_text:I = 0x7f1201bc

.field public static final noticepopupScrollView:I = 0x7f1201bb

.field public static final noticepopup_checkbox:I = 0x7f1201bd

.field public static final object_info:I = 0x7f120259

.field public static final ok_button:I = 0x7f120305

.field public static final or:I = 0x7f1202f1

.field public static final parentLayout:I = 0x7f1201be

.field public static final parent_container:I = 0x7f1201e9

.field public static final pathDescriptor:I = 0x7f120000

.field public static final perfect_first:I = 0x7f1201e5

.field public static final perfect_second:I = 0x7f1201e6

.field public static final phone_number:I = 0x7f12017d

.field public static final phone_type:I = 0x7f12017c

.field public static final placeHolder:I = 0x7f12020a

.field public static final preferenceImage:I = 0x7f1201bf

.field public static final present_temp:I = 0x7f120018

.field public static final present_temp_degree:I = 0x7f1202f6

.field public static final prev_btn:I = 0x7f12000c

.field public static final preview_layout:I = 0x7f1201ce

.field public static final previous:I = 0x7f1201f5

.field public static final processing_text:I = 0x7f1202ef

.field public static final progress_listing:I = 0x7f120138

.field public static final prompt_btn_layout:I = 0x7f1201e0

.field public static final prompt_user_textview:I = 0x7f120086

.field public static final prompt_vlingo_textview:I = 0x7f120088

.field public static final prompt_wakeup_textview:I = 0x7f12008a

.field public static final pull_tab:I = 0x7f1200a5

.field public static final radio_group:I = 0x7f120007

.field public static final rating:I = 0x7f12015c

.field public static final ratings:I = 0x7f12013b

.field public static final recordScrollView:I = 0x7f12006f

.field public static final record_body:I = 0x7f120074

.field public static final region_container_newaddress:I = 0x7f120287

.field public static final region_container_zipcode:I = 0x7f120289

.field public static final regular_control_view:I = 0x7f120078

.field public static final relativelayout_1:I = 0x7f120164

.field public static final reminder_bottom:I = 0x7f1202bf

.field public static final result_content_layout:I = 0x7f1202e6

.field public static final retry_button:I = 0x7f120095

.field public static final right_container:I = 0x7f120049

.field public static final samsung_always_on_layout:I = 0x7f120002

.field public static final samsung_disclaimer_tos_layout:I = 0x7f1200ab

.field public static final schedule_attendee:I = 0x7f1202a6

.field public static final schedule_button:I = 0x7f1202a8

.field public static final schedule_button_divider:I = 0x7f1202aa

.field public static final schedule_confirm:I = 0x7f1202ab

.field public static final schedule_container:I = 0x7f120298

.field public static final schedule_date:I = 0x7f12017f

.field public static final schedule_day:I = 0x7f12029a

.field public static final schedule_location:I = 0x7f1202a2

.field public static final schedule_location_title:I = 0x7f1202a1

.field public static final schedule_month:I = 0x7f12029b

.field public static final schedule_participants_title:I = 0x7f1202a5

.field public static final schedule_row_button_line:I = 0x7f1202a7

.field public static final schedule_row_date:I = 0x7f120299

.field public static final schedule_row_location:I = 0x7f1202a0

.field public static final schedule_row_location_line:I = 0x7f12029f

.field public static final schedule_row_participants:I = 0x7f1202a4

.field public static final schedule_row_participants_line:I = 0x7f1202a3

.field public static final schedule_row_title:I = 0x7f12029e

.field public static final schedule_row_title_line:I = 0x7f12029d

.field public static final schedule_single_container:I = 0x7f1202ac

.field public static final schedule_time:I = 0x7f12012c

.field public static final schedule_title:I = 0x7f12012b

.field public static final schedule_week:I = 0x7f12029c

.field public static final scrollView2:I = 0x7f12006a

.field public static final scrollView3:I = 0x7f1200a7

.field public static final second_colon:I = 0x7f1202df

.field public static final second_colon_bg:I = 0x7f1202d3

.field public static final senderImage:I = 0x7f120232

.field public static final server_preference_spinner:I = 0x7f1201c1

.field public static final server_preference_text:I = 0x7f1201c0

.field public static final service_list:I = 0x7f1202b8

.field public static final settingsButton:I = 0x7f120081

.field public static final settings_contents:I = 0x7f1201d4

.field public static final settings_contents_ll:I = 0x7f1201d3

.field public static final settings_header:I = 0x7f1201d2

.field public static final settings_header_ll:I = 0x7f1201d1

.field public static final shadow:I = 0x7f1202ea

.field public static final simultaneously_txt:I = 0x7f1202ee

.field public static final skip:I = 0x7f1201f6

.field public static final social_type:I = 0x7f120184

.field public static final spacer:I = 0x7f12013d

.field public static final speak:I = 0x7f1202ec

.field public static final star_img:I = 0x7f120123

.field public static final start_svoice_layout:I = 0x7f1201cb

.field public static final start_svoice_tutorial_text1:I = 0x7f1201cc

.field public static final start_svoice_tutorial_text2:I = 0x7f1201cf

.field public static final stretch_layout:I = 0x7f120206

.field public static final stretch_layout_1:I = 0x7f12020c

.field public static final styleWidgetNaverLocalChoice_LinearLayout_1:I = 0x7f120282

.field public static final styleWigetMovieChoice_LinearLayout_1:I = 0x7f12026b

.field public static final styleWigetMovie_LinearLayout_1:I = 0x7f12025c

.field public static final subHeader:I = 0x7f1201f3

.field public static final success_image:I = 0x7f1201e4

.field public static final svoice_title_welcomepage:I = 0x7f1200b6

.field public static final svoice_tutorial:I = 0x7f1201ca

.field public static final switch1:I = 0x7f1200fa

.field public static final switchButton:I = 0x7f12007f

.field public static final switchTiltleDiv:I = 0x7f12007e

.field public static final system_bubble:I = 0x7f1202e8

.field public static final tap_mic:I = 0x7f1202f2

.field public static final task_button:I = 0x7f1202c2

.field public static final task_button_divider:I = 0x7f1202c4

.field public static final task_confirm:I = 0x7f1202c5

.field public static final task_container:I = 0x7f1202b9

.field public static final task_date:I = 0x7f1202be

.field public static final task_divider:I = 0x7f1202bc

.field public static final task_reminder_date:I = 0x7f1202c1

.field public static final task_row_dueto:I = 0x7f1202bd

.field public static final task_row_reminder:I = 0x7f1202c0

.field public static final task_row_title:I = 0x7f1202ba

.field public static final task_title:I = 0x7f1202bb

.field public static final temp_detail:I = 0x7f120021

.field public static final temprature:I = 0x7f120017

.field public static final text:I = 0x7f1200a9

.field public static final text1:I = 0x7f1200ce

.field public static final text1_examples:I = 0x7f120201

.field public static final text2:I = 0x7f1201e2

.field public static final text_Local_Address:I = 0x7f120279

.field public static final text_Local_CallNumber:I = 0x7f12027a

.field public static final text_Local_Distance:I = 0x7f120278

.field public static final text_Local_MainTitle:I = 0x7f120284

.field public static final text_Local_Main_Title:I = 0x7f120277

.field public static final text_Local_address:I = 0x7f120285

.field public static final text_Movie_Actors:I = 0x7f120174

.field public static final text_Movie_Director:I = 0x7f120173

.field public static final text_Movie_Genre:I = 0x7f12025e

.field public static final text_Movie_Grade:I = 0x7f120265

.field public static final text_Movie_MainTitle:I = 0x7f12026c

.field public static final text_Movie_OpenDate:I = 0x7f120176

.field public static final text_Movie_Poster_Cover:I = 0x7f12016e

.field public static final text_Movie_Rank:I = 0x7f120170

.field public static final text_Movie_Rating:I = 0x7f120175

.field public static final text_Movie_RunningTime:I = 0x7f120260

.field public static final text_Movie_Title:I = 0x7f120172

.field public static final text_Region_Main_Address:I = 0x7f120286

.field public static final text_Region_New_Address:I = 0x7f120288

.field public static final text_Region_Zipcode:I = 0x7f12028a

.field public static final text_address_line1:I = 0x7f12013f

.field public static final text_address_line2:I = 0x7f120140

.field public static final text_author:I = 0x7f12015d

.field public static final text_date:I = 0x7f12015f

.field public static final text_destination:I = 0x7f1201b6

.field public static final text_detail_first:I = 0x7f120127

.field public static final text_detail_second:I = 0x7f120128

.field public static final text_details:I = 0x7f120154

.field public static final text_did_you_know:I = 0x7f120050

.field public static final text_distance:I = 0x7f120141

.field public static final text_examples:I = 0x7f12004e

.field public static final text_label:I = 0x7f120152

.field public static final text_label1:I = 0x7f120148

.field public static final text_label2:I = 0x7f120149

.field public static final text_label3:I = 0x7f12014a

.field public static final text_label4:I = 0x7f12014b

.field public static final text_number:I = 0x7f120125

.field public static final text_phone_number:I = 0x7f12013e

.field public static final text_provider:I = 0x7f12015a

.field public static final text_review_body:I = 0x7f12015e

.field public static final text_reviews:I = 0x7f120157

.field public static final text_shortcutname:I = 0x7f1201b8

.field public static final text_sponsored:I = 0x7f12013c

.field public static final text_synopsis:I = 0x7f120156

.field public static final text_tagline:I = 0x7f120155

.field public static final text_title:I = 0x7f120124

.field public static final text_title_name:I = 0x7f120240

.field public static final text_tos_link:I = 0x7f1201c8

.field public static final text_tos_top:I = 0x7f1201c7

.field public static final text_version:I = 0x7f1200ef

.field public static final text_version_2:I = 0x7f1200f8

.field public static final textfocus:I = 0x7f1201d6

.field public static final timeContainer:I = 0x7f12021c

.field public static final time_layout:I = 0x7f1202cc

.field public static final timer_body:I = 0x7f1202c7

.field public static final timer_body_container:I = 0x7f1202e2

.field public static final timer_head_text:I = 0x7f1202c8

.field public static final timer_hour_bg:I = 0x7f1202d7

.field public static final timer_hour_postfix:I = 0x7f1202db

.field public static final timer_hour_postfix_bg:I = 0x7f1202cf

.field public static final timer_hour_prefix:I = 0x7f1202da

.field public static final timer_hour_prefix_bg:I = 0x7f1202ce

.field public static final timer_minute_bg:I = 0x7f1202d8

.field public static final timer_minute_postfix:I = 0x7f1202de

.field public static final timer_minute_postfix_bg:I = 0x7f1202d2

.field public static final timer_minute_prefix:I = 0x7f1202dd

.field public static final timer_minute_prefix_bg:I = 0x7f1202d1

.field public static final timer_number_background:I = 0x7f1202cd

.field public static final timer_number_bg:I = 0x7f1202d6

.field public static final timer_second_bg:I = 0x7f1202d9

.field public static final timer_second_postfix:I = 0x7f1202e1

.field public static final timer_second_postfix_bg:I = 0x7f1202d5

.field public static final timer_second_prefix:I = 0x7f1202e0

.field public static final timer_second_prefix_bg:I = 0x7f1202d4

.field public static final tip__help_ptrt:I = 0x7f1201a8

.field public static final tip_help_land:I = 0x7f1201ac

.field public static final tip_mic_land:I = 0x7f1201af

.field public static final tip_mic_ptrt:I = 0x7f1201a5

.field public static final titleCustomCommand:I = 0x7f120070

.field public static final titleDesc:I = 0x7f1201de

.field public static final titleText:I = 0x7f1201dd

.field public static final title_container:I = 0x7f120102

.field public static final today_text:I = 0x7f120117

.field public static final toolTipHelpLayout:I = 0x7f1201da

.field public static final topRelative:I = 0x7f120186

.field public static final tos_accept_radio_btn:I = 0x7f1200b2

.field public static final tos_content:I = 0x7f1200b1

.field public static final tos_decline_radio_btn:I = 0x7f1200b3

.field public static final tos_title:I = 0x7f1200b0

.field public static final true_knowledge_container:I = 0x7f1202e3

.field public static final trueknowledge_logo:I = 0x7f120211

.field public static final tutorial_end_text:I = 0x7f1202e5

.field public static final twitterIcon:I = 0x7f120238

.field public static final unit_meter_C:I = 0x7f12001a

.field public static final unit_meter_F:I = 0x7f120019

.field public static final updateButton:I = 0x7f12023b

.field public static final user_body:I = 0x7f120084

.field public static final user_bubble:I = 0x7f1202e7

.field public static final uuid_text:I = 0x7f1200f0

.field public static final view_Movie_div1:I = 0x7f12025f

.field public static final view_Movie_div2:I = 0x7f120261

.field public static final vlingo_body:I = 0x7f120083

.field public static final wake_up_count:I = 0x7f120097

.field public static final wake_up_setting:I = 0x7f12008e

.field public static final wake_up_setting_1:I = 0x7f120099

.field public static final wake_up_setting_2:I = 0x7f12009a

.field public static final wake_up_setting_3:I = 0x7f12009b

.field public static final wake_up_setting_4:I = 0x7f12009c

.field public static final wake_up_setting_5:I = 0x7f12009d

.field public static final wake_up_setting_6:I = 0x7f12009e

.field public static final wake_up_setting_tv:I = 0x7f120098

.field public static final wake_up_success:I = 0x7f12008f

.field public static final wakeupDialogOptions:I = 0x7f1201ee

.field public static final wakeupList:I = 0x7f1201eb

.field public static final wakeup_body:I = 0x7f120085

.field public static final wakeup_text:I = 0x7f1201e1

.field public static final wakeup_wave:I = 0x7f120076

.field public static final wave:I = 0x7f1201e7

.field public static final wcis_divider:I = 0x7f120244

.field public static final wcis_info_with_caption_row_divider:I = 0x7f1201f0

.field public static final wcis_lyt_row_divider:I = 0x7f1201f2

.field public static final wcis_widget_row_container:I = 0x7f1201fb

.field public static final wcis_widget_row_divider1:I = 0x7f1201ff

.field public static final wcis_widget_row_divider2:I = 0x7f120204

.field public static final weather_detail:I = 0x7f120020

.field public static final weathernews:I = 0x7f1202f5

.field public static final webview:I = 0x7f1202f9

.field public static final welcome_page_container:I = 0x7f12020b

.field public static final welcome_page_main:I = 0x7f12020d

.field public static final welcome_text_layout:I = 0x7f120207

.field public static final welcome_video:I = 0x7f120209

.field public static final widget_answer_question:I = 0x7f12020e

.field public static final widget_button:I = 0x7f120212

.field public static final widget_button_image:I = 0x7f120215

.field public static final widget_button_text:I = 0x7f120216

.field public static final widget_chatbot:I = 0x7f120217

.field public static final widget_citi:I = 0x7f120011

.field public static final widget_citi_LinearLayout:I = 0x7f120010

.field public static final widget_citi_today:I = 0x7f120022

.field public static final widget_clock:I = 0x7f120219

.field public static final widget_clock_shadow:I = 0x7f120227

.field public static final widget_compose_social_status_body_container:I = 0x7f120234

.field public static final widget_contact_detail:I = 0x7f12023c

.field public static final widget_contact_detail_relativelayout_1:I = 0x7f12023d

.field public static final widget_help_choice:I = 0x7f1200ca

.field public static final widget_local_search_LinearLayout:I = 0x7f120245

.field public static final widget_messagereadback_relative:I = 0x7f120251

.field public static final widget_messagereadbackbodyhidden_relative:I = 0x7f120255

.field public static final widget_n_bubble_layout:I = 0x7f1202f0

.field public static final widget_name:I = 0x7f120257

.field public static final widget_smart_date:I = 0x7f1202af

.field public static final widget_smart_list:I = 0x7f1202b7

.field public static final widget_smart_list_bday:I = 0x7f1202b4

.field public static final widget_smart_list_container:I = 0x7f1202ae

.field public static final widget_smart_title:I = 0x7f1202b3

.field public static final widget_smart_title1:I = 0x7f1202b6

.field public static final widget_social:I = 0x7f120230

.field public static final widget_social_update:I = 0x7f12022f

.field public static final widget_timer:I = 0x7f1202c6

.field public static final widget_weather_citi:I = 0x7f12000f

.field public static final widget_web_browser:I = 0x7f1202f8

.field public static final widget_widget_you_can_say_title:I = 0x7f1202ff

.field public static final widget_you_can_say_container:I = 0x7f1202fd

.field public static final widget_you_can_say_list:I = 0x7f120300

.field public static final widget_you_can_say_list_container:I = 0x7f1202fe

.field public static final widget_you_can_smart_say_container:I = 0x7f1202ad

.field public static final widget_you_can_smart_say_day:I = 0x7f1202b2

.field public static final widget_you_can_smart_say_month:I = 0x7f1202b1

.field public static final widget_you_can_smart_say_month_day:I = 0x7f1202b0

.field public static final winset_popup:I = 0x7f120301

.field public static final winset_popup_image:I = 0x7f120304

.field public static final winset_popup_info:I = 0x7f120303

.field public static final winset_popup_linear:I = 0x7f120302

.field public static final wmanagerImage:I = 0x7f1201d5

.field public static final wolfram_alpha_linearlayout:I = 0x7f1202fa

.field public static final wolfram_content_container:I = 0x7f1202fb

.field public static final wolframeShadow:I = 0x7f1202fc

.field public static final write_btn_layout:I = 0x7f120068

.field public static final write_btn_layout_land:I = 0x7f120198

.field public static final wycs_detail_image:I = 0x7f12004b

.field public static final wycs_detail_title_text:I = 0x7f12004c

.field public static final wycs_title:I = 0x7f120243

.field public static final wycs_top_btn_divider:I = 0x7f1201f8

.field public static final wycs_top_divider:I = 0x7f1201f9

.field public static final wycs_top_list:I = 0x7f1200cc


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
