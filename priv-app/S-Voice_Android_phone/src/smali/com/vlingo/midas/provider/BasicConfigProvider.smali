.class public abstract Lcom/vlingo/midas/provider/BasicConfigProvider;
.super Landroid/content/ContentProvider;
.source "BasicConfigProvider.java"


# static fields
.field protected static final AUTHORITY:Ljava/lang/String;

.field public static final CONTENT_URI:Ljava/lang/String;

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final VALUE:Ljava/lang/String; = "value"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_AUTHORITY:Ljava/lang/String;

    sput-object v0, Lcom/vlingo/midas/provider/BasicConfigProvider;->AUTHORITY:Ljava/lang/String;

    .line 18
    sget-object v0, Lcom/vlingo/midas/samsungutils/VlingoConfigProviderConstants;->SVOICE_CONTENT_PROVIDER_URI:Ljava/lang/String;

    sput-object v0, Lcom/vlingo/midas/provider/BasicConfigProvider;->CONTENT_URI:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 27
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "/SETTINGS/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 28
    invoke-virtual {p2}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 29
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 30
    .local v2, "value":Ljava/lang/Object;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    if-nez v3, :cond_1

    .line 31
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/midas/provider/BasicConfigProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->init(Landroid/content/Context;)V

    .line 34
    :cond_1
    instance-of v3, v2, Ljava/lang/Boolean;

    if-eqz v3, :cond_2

    .line 35
    check-cast v2, Ljava/lang/Boolean;

    .end local v2    # "value":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 36
    .restart local v2    # "value":Ljava/lang/Object;
    :cond_2
    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 37
    check-cast v2, Ljava/lang/String;

    .end local v2    # "value":Ljava/lang/Object;
    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 38
    .restart local v2    # "value":Ljava/lang/Object;
    :cond_3
    instance-of v3, v2, Ljava/lang/Integer;

    if-eqz v3, :cond_0

    .line 39
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "value":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 43
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x0

    return v3
.end method
