.class public Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;
.super Ljava/lang/Object;
.source "VlingoConfigProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/provider/VlingoConfigProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VlingoConfigProviderQueryProxy"
.end annotation


# instance fields
.field protected successor:Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;

.field protected version:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "version"    # I

    .prologue
    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291
    iput p1, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;->version:I

    .line 292
    return-void
.end method

.method private getVersion(Landroid/net/Uri;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 300
    const-string/jumbo v1, "version"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 301
    .local v0, "sVersion":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 303
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 309
    :goto_0
    return v1

    .line 304
    :catch_0
    move-exception v1

    .line 309
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public query(Lcom/vlingo/midas/provider/VlingoConfigProvider;Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 2
    .param p1, "provider"    # Lcom/vlingo/midas/provider/VlingoConfigProvider;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 318
    invoke-direct {p0, p2}, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;->getVersion(Landroid/net/Uri;)I

    move-result v0

    iget v1, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;->version:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;->successor:Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;->successor:Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;->query(Lcom/vlingo/midas/provider/VlingoConfigProvider;Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    .line 322
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, p2}, Lcom/vlingo/midas/provider/VlingoConfigProvider;->queryInternal(Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public setSuccessor(Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;)V
    .locals 0
    .param p1, "successor"    # Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;

    .prologue
    .line 295
    iput-object p1, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;->successor:Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;

    .line 296
    return-void
.end method
