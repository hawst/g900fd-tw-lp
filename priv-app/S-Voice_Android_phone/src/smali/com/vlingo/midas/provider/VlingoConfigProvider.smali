.class public Lcom/vlingo/midas/provider/VlingoConfigProvider;
.super Lcom/vlingo/midas/provider/BasicConfigProvider;
.source "VlingoConfigProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;
    }
.end annotation


# static fields
.field private static final APP_CHANNEL:I = 0xc

.field private static final APP_ID:I = 0x8

.field private static final APP_NAME:I = 0x9

.field private static final APP_VERSION:I = 0xa

.field private static final LMTT_SERVICE_STARTED:I = 0x11

.field private static PATHS:Ljava/util/Map; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PATH_APP_CHANNEL:Ljava/lang/String; = "app_channel"

.field public static final PATH_APP_ID:Ljava/lang/String; = "app_id"

.field public static final PATH_APP_NAME:Ljava/lang/String; = "app_name"

.field public static final PATH_APP_VERSION:Ljava/lang/String; = "app_version"

.field public static final PATH_LMTT_SERVICE_STARTED:Ljava/lang/String; = "lmtt_started"

.field public static final PATH_READBACK_ENABLED:Ljava/lang/String; = "readback_enabled"

.field public static final PATH_SALES_CODE:Ljava/lang/String; = "sales_code"

.field public static final PATH_SERVER_HOST_ASR:Ljava/lang/String; = "server_host_asr"

.field public static final PATH_SERVER_HOST_HELLO:Ljava/lang/String; = "server_host_hello"

.field public static final PATH_SERVER_HOST_LOG:Ljava/lang/String; = "server_host_log"

.field public static final PATH_SERVER_HOST_VCS:Ljava/lang/String; = "server_host_vcs"

.field public static final PATH_SVOICE_CONTENT_PROVIDER_VERSION:Ljava/lang/String; = "content_provider_version"

.field public static final PATH_TOS_ACCEPTED:Ljava/lang/String; = "tos_accepted"

.field public static final PATH_UIFOCUS_MANAGER_VERSION:Ljava/lang/String; = "content_uifocus_manager_version"

.field private static final READBACK_ENABLED:I = 0x2

.field private static final SALES_CODE:I = 0xb

.field private static final SERVER_HOST_ASR:I = 0x4

.field private static final SERVER_HOST_HELLO:I = 0x7

.field private static final SERVER_HOST_LOG:I = 0x6

.field private static final SERVER_HOST_VCS:I = 0x5

.field private static final SVOICE_CONTENT_PROVIDER_VERSION:I = 0x13

.field private static final TOS_ACCEPTED:I = 0x10

.field private static final UIFOCUS_MANAGER_VERSION:I = 0x14

.field private static final UIX_COMPLETE:I = 0x12


# instance fields
.field private mUriMatcher:Landroid/content/UriMatcher;

.field proxy:Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/vlingo/midas/provider/VlingoConfigProvider$1;

    invoke-direct {v0}, Lcom/vlingo/midas/provider/VlingoConfigProvider$1;-><init>()V

    sput-object v0, Lcom/vlingo/midas/provider/VlingoConfigProvider;->PATHS:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/vlingo/midas/provider/BasicConfigProvider;-><init>()V

    .line 109
    new-instance v0, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider;->proxy:Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;

    .line 286
    return-void
.end method

.method private addRowToCursor(Ljava/lang/String;Landroid/database/MatrixCursor;)V
    .locals 3
    .param p1, "configName"    # Ljava/lang/String;
    .param p2, "mc"    # Landroid/database/MatrixCursor;

    .prologue
    .line 189
    invoke-direct {p0, p1}, Lcom/vlingo/midas/provider/VlingoConfigProvider;->getConfigValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "configValue":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 193
    .local v1, "row":[Ljava/lang/String;
    invoke-virtual {p2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 194
    return-void
.end method

.method private getConfigValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "configItem"    # Ljava/lang/String;

    .prologue
    .line 197
    const-string/jumbo v3, ""

    .line 199
    .local v3, "value":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v4, "server_host_asr"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 200
    invoke-static {}, Lcom/vlingo/midas/util/ServerDetails;->getInstance()Lcom/vlingo/midas/util/ServerDetails;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/util/ServerDetails;->getASRHost()Ljava/lang/String;

    move-result-object v3

    .line 264
    :cond_0
    :goto_0
    return-object v3

    .line 201
    :cond_1
    const-string/jumbo v4, "server_host_vcs"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 202
    invoke-static {}, Lcom/vlingo/midas/util/ServerDetails;->getInstance()Lcom/vlingo/midas/util/ServerDetails;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/util/ServerDetails;->getVCSHost()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 203
    :cond_2
    const-string/jumbo v4, "server_host_log"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 204
    invoke-static {}, Lcom/vlingo/midas/util/ServerDetails;->getInstance()Lcom/vlingo/midas/util/ServerDetails;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/util/ServerDetails;->getLogHost()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 205
    :cond_3
    const-string/jumbo v4, "server_host_hello"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 206
    invoke-static {}, Lcom/vlingo/midas/util/ServerDetails;->getInstance()Lcom/vlingo/midas/util/ServerDetails;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/util/ServerDetails;->getHelloHost()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 207
    :cond_4
    const-string/jumbo v4, "app_id"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 209
    const-string/jumbo v3, "com.samsung.android.jproject"

    goto :goto_0

    .line 211
    :cond_5
    const-string/jumbo v4, "app_name"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 213
    const-string/jumbo v3, "SamsungJProject"

    goto :goto_0

    .line 215
    :cond_6
    const-string/jumbo v4, "app_version"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 217
    const-string/jumbo v3, "11.0"

    goto :goto_0

    .line 219
    :cond_7
    const-string/jumbo v4, "app_channel"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 221
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/VlingoApplication;->getAppChannel()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 222
    :cond_8
    const-string/jumbo v4, "sales_code"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 223
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 224
    :cond_9
    const-string/jumbo v4, "tos_accepted"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 225
    invoke-direct {p0}, Lcom/vlingo/midas/provider/VlingoConfigProvider;->getTosAcceptedValue()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 226
    :cond_a
    const-string/jumbo v4, "lmtt_started"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 232
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 233
    :cond_b
    const-string/jumbo v4, "readback_enabled"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 235
    const-string/jumbo v4, "content_provider_version"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 236
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 237
    :cond_c
    const-string/jumbo v4, "content_uifocus_manager_version"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 238
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    goto/16 :goto_0

    .line 241
    :catch_0
    move-exception v0

    .line 243
    .local v0, "eiie":Ljava/lang/ExceptionInInitializerError;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "caught ExceptionInInitializerError in getConfigValue() for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto/16 :goto_0

    .line 250
    .end local v0    # "eiie":Ljava/lang/ExceptionInInitializerError;
    :catch_1
    move-exception v2

    .line 254
    .local v2, "rte":Ljava/lang/RuntimeException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "caught RuntimeException in getConfigValue() for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 260
    .local v1, "msg":Ljava/lang/String;
    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private getTosAcceptedValue()Ljava/lang/String;
    .locals 4

    .prologue
    .line 277
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v0

    .line 278
    .local v0, "settingTosIsAccepted":Z
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v1

    .line 279
    .local v1, "supportsSVoiceAssociatedServiceOnly":Z
    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    .line 282
    .local v2, "toReturn":Ljava/lang/String;
    return-object v2

    .line 279
    .end local v2    # "toReturn":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 116
    iget-object v1, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 117
    .local v0, "match":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 118
    const-string/jumbo v1, "vnd.android.cursor.item/vnd.vlingo.config"

    .line 124
    :goto_0
    return-object v1

    .line 120
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 121
    const-string/jumbo v1, "vnd.android.cursor.dir/vnd.vlingo.config"

    goto :goto_0

    .line 124
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 129
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 6

    .prologue
    .line 136
    new-instance v2, Landroid/content/UriMatcher;

    const/4 v3, -0x1

    invoke-direct {v2, v3}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v2, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 137
    sget-object v2, Lcom/vlingo/midas/provider/VlingoConfigProvider;->PATHS:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 138
    .local v1, "path":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider;->mUriMatcher:Landroid/content/UriMatcher;

    sget-object v5, Lcom/vlingo/midas/provider/VlingoConfigProvider;->AUTHORITY:Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v5, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 140
    .end local v1    # "path":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider;->proxy:Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;

    invoke-virtual {v0, p0, p1}, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;->query(Lcom/vlingo/midas/provider/VlingoConfigProvider;Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected queryInternal(Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 158
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v8, v12, [Ljava/lang/String;

    const-string/jumbo v9, "name"

    aput-object v9, v8, v10

    const-string/jumbo v9, "value"

    aput-object v9, v8, v11

    invoke-direct {v0, v8}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 160
    .local v0, "c":Landroid/database/MatrixCursor;
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_0

    .line 161
    sget-object v8, Lcom/vlingo/midas/provider/VlingoConfigProvider;->PATHS:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 162
    .local v5, "path":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {p0, v8, v0}, Lcom/vlingo/midas/provider/VlingoConfigProvider;->addRowToCursor(Ljava/lang/String;Landroid/database/MatrixCursor;)V

    goto :goto_0

    .line 164
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "path":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "/SETTINGS/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 165
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    .line 166
    .local v3, "name":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 167
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 168
    .local v7, "value":Ljava/lang/Object;
    if-eqz v7, :cond_1

    .line 171
    new-array v6, v12, [Ljava/lang/String;

    aput-object v3, v6, v10

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v11

    .line 172
    .local v6, "row":[Ljava/lang/String;
    invoke-virtual {v0, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 185
    .end local v3    # "name":Ljava/lang/String;
    .end local v6    # "row":[Ljava/lang/String;
    .end local v7    # "value":Ljava/lang/Object;
    :cond_1
    :goto_1
    return-object v0

    .line 176
    :cond_2
    iget-object v8, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 179
    .local v2, "match":I
    sget-object v8, Lcom/vlingo/midas/provider/VlingoConfigProvider;->PATHS:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 180
    .local v4, "path":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 181
    invoke-direct {p0, v4, v0}, Lcom/vlingo/midas/provider/VlingoConfigProvider;->addRowToCursor(Ljava/lang/String;Landroid/database/MatrixCursor;)V

    goto :goto_1
.end method

.method public setVlingoConfigProviderQueryProxySuccessor(Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;)V
    .locals 1
    .param p1, "successor"    # Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/vlingo/midas/provider/VlingoConfigProvider;->proxy:Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;->setSuccessor(Lcom/vlingo/midas/provider/VlingoConfigProvider$VlingoConfigProviderQueryProxy;)V

    .line 154
    return-void
.end method
