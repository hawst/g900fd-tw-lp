.class public Lcom/vlingo/midas/VlingoApplication;
.super Landroid/app/Application;
.source "VlingoApplication.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/VlingoApplicationInterface;


# static fields
.field public static ACTION_VLINGO_APP_START:Ljava/lang/String; = null

.field public static final APP_DISTRIBUTION_CHANNEL:Ljava/lang/String; = "Preinstall Free"

.field private static final APP_ID:Ljava/lang/String; = "com.samsung.android.tproject"

.field private static final APP_NAME:Ljava/lang/String; = "SamsungTproject"

.field private static APP_VERSION:Ljava/lang/String; = null

.field private static CORE_VERSION:Ljava/lang/String; = null

.field public static final DEFAULT_FIELD_ID:Ljava/lang/String; = "dm_main"

.field private static LOG_TAG:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static appVersion:Ljava/lang/String;

.field private static coreVersion:Ljava/lang/String;

.field private static instance:Lcom/vlingo/midas/VlingoApplication;


# instance fields
.field private isInForeground:Z

.field private preferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private sVoiceForGear:Z

.field private socialLogin:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 166
    const-class v0, Lcom/vlingo/midas/VlingoApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/VlingoApplication;->TAG:Ljava/lang/String;

    .line 173
    const-string/jumbo v0, "VlingoApplication"

    sput-object v0, Lcom/vlingo/midas/VlingoApplication;->LOG_TAG:Ljava/lang/String;

    .line 183
    const-string/jumbo v0, "11.0"

    sput-object v0, Lcom/vlingo/midas/VlingoApplication;->APP_VERSION:Ljava/lang/String;

    .line 184
    sget-object v0, Lcom/vlingo/midas/VlingoApplication;->APP_VERSION:Ljava/lang/String;

    sput-object v0, Lcom/vlingo/midas/VlingoApplication;->appVersion:Ljava/lang/String;

    .line 185
    const-string/jumbo v0, "TBD"

    sput-object v0, Lcom/vlingo/midas/VlingoApplication;->CORE_VERSION:Ljava/lang/String;

    .line 186
    sget-object v0, Lcom/vlingo/midas/VlingoApplication;->CORE_VERSION:Ljava/lang/String;

    sput-object v0, Lcom/vlingo/midas/VlingoApplication;->coreVersion:Ljava/lang/String;

    .line 193
    const-string/jumbo v0, "com.vlingo.client.app.action.VLINGO_APP_START"

    sput-object v0, Lcom/vlingo/midas/VlingoApplication;->ACTION_VLINGO_APP_START:Ljava/lang/String;

    .line 195
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/VlingoApplication;->instance:Lcom/vlingo/midas/VlingoApplication;

    .line 204
    new-instance v0, Lcom/vlingo/midas/MidasValues;

    new-instance v1, Lcom/vlingo/midas/VlingoApplication;

    invoke-direct {v1}, Lcom/vlingo/midas/VlingoApplication;-><init>()V

    invoke-direct {v0, v1}, Lcom/vlingo/midas/MidasValues;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->setInterface(Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;)V

    .line 206
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->setupStandardMappings()V

    .line 208
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->ADDRESS_BOOK:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 209
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHINA_NAV:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungChinaNavHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 210
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHINA_BAIDU_MAP:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungChinaBaiduMapHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 211
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHINA_NAV_ENAVI:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungENaviNavHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 213
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHATBOT_SING:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChatbotSing;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 215
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CHATBOT:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ChatbotWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 216
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CONTACT_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungContactLookupHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 217
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->MAP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungMapHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 218
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->NAVER_CONTENT:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NaverContentHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 219
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->NAVIGATE_HOME:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 220
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->NAVIGATE_HOME_KOREAN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavigateHomeKoreanHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 221
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->NAVIGATE_LOCAL:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/NavLocalHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 222
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SAMSUNG_NAVIGATE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNavHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 223
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RECORD_VOICE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/RecordVoiceHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 224
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RESOLVE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveAlarmHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 225
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RESOLVE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ResolveTaskHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 226
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_ALARMS:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowAlarmsHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 227
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_ALARM_CHOICES:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/resolve/ShowAlarmChoicesHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 228
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_COMPOSE_MEMO:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowComposeMemoHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 229
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_CREATE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateAlarmHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 230
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_CREATE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/create/ShowCreateTaskHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 231
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_DELETE_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/delete/ShowDeleteAlarmHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 232
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_DELETE_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowDeleteTaskHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 233
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_EDIT_ALARM:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/edit/ShowModifyAlarmHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 234
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_EDIT_TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowModifyTaskHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 235
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_LOCAL_SEARCH:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasLocalSearchHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 236
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_MEMO_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowMemoLookupWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 237
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_NAVIGATION:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowNavWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 238
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_TITLE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayTitleWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 239
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_ARTIST:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayArtistWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 240
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_ALBUM:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayAlbumWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 241
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_PLAYLIST:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayPlaylistWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 242
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_MUSIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayMusicWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 243
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_PLAY_GENERIC_MUSIC:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowPlayGenericWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 244
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_TASKS:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowTasksHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 245
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_TASK_CHOICES:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/ShowTaskChoicesHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 246
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_UNREAD_MESSAGES:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowMessagesWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 247
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_WCIS:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungShowWCISWidget;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 248
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->TASK:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/AddTaskPageHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 249
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_0PEN_APP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungShowOpenAppWidgetHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 250
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->PLAY_MUSIC_BY_CHARACTERISTIC:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/PlayMusicByCharacteristicHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 251
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CONFIRM_HANDLER:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ConfirmHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 252
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_WEB_SEARCH_BUTTON:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/MidasWebSearchButtonHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 253
    const-string/jumbo v0, "LPAction"

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungLPActionHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Ljava/lang/String;Ljava/lang/Class;)V

    .line 254
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->ANSWER_QUESTION:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAnswerQuestionHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 255
    const-string/jumbo v0, "Preinstall Free"

    invoke-static {v0}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPCMAWeatherLookupHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 260
    :goto_0
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->RESOLVE_MESSAGE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/resolve/ResolveMessageHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 261
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->WORLD_TIME_RESPONSE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/WorldTimeHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 262
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->EMERGENCY_CALL:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyCallHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 263
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->NOTIFICATION_CHANGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungNotiChangeHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 264
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SHOW_BATTERY:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ShowBatteryLevelHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 265
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->LOCATION_SHARE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/EmergencyLocationSharingHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 266
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->SOCIAL_PAGE:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SocialUpdateHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 267
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CAR_MODE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/CarModeHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 268
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->CAR_LOCATE:Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/LocateCarHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 269
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->ONE_ROUND_WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/SamsungOneRoundWeatherHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 271
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->setupStandardMappings()V

    .line 273
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_EMAIL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendEmailAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 274
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->CREATE_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/AddTaskAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 275
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/DeleteTaskAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 276
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_TASK:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/ModifyTaskAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 277
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SAVE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 278
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SET_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/SetAlarmAndReminderAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 279
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/DeleteAlarmAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 280
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->DELETE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/DeleteMemoAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 281
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->MODIFY_ALARM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/ModifyAlarmAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 282
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SAVE_MEMO:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/samsungutils/vvs/actions/SaveMemoAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 283
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SETTING_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungSettingChangeAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 284
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_ALBUM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayAlbumAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 285
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_ARTIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayArtistAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 286
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_MUSIC:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 287
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_PLAYLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayPlaylistAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 288
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_TITLE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayTitleAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 289
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_SONGLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 290
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->RECORD_VOICE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/RecordVoiceAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 291
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->NOTIFICATION_CHANGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungNotiChangeAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 292
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->VOICE_DIAL:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungVoiceDialAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 293
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->OPEN_APP:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungOpenAppAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 294
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LAUNCH_ACTIVITY:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungLaunchActivityAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 295
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->EXECUTE_INTENT:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungExecuteIntentAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 296
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SEND_MESSAGE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/SamsungSendMessageAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 297
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->SOCIAL_UPDATE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/SocialUpdateAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 299
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_OUT_NEWS:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 300
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_OUT_NEWS_MULTI:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/PlayOutNewsMultiAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 302
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->LOCATE_CAR:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v1, Lcom/vlingo/midas/dialogmanager/actions/LocateCarAction;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 304
    sget-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->NoiseCancel:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 305
    sget-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->AudioSourceSelector:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v1, Lcom/vlingo/midas/util/SamsungAudioSourceUtil;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 306
    sget-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MsgUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v1, Lcom/samsung/msg/SamsungMSGUtil;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 307
    sget-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->BDeviceUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v1, Lcom/samsung/bdevice/SamsungBDeviceUtil;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 308
    sget-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v1, Lcom/vlingo/midas/samsungutils/utils/music/SamsungMusicPlusUtil;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 309
    return-void

    .line 258
    :cond_0
    sget-object v0, Lcom/vlingo/midas/dialogmanager/vvs/SamsungVVSActionKey;->WEATHER_LOOKUP:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/JPWeatherLookupHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    goto/16 :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 165
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 201
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/VlingoApplication;->sVoiceForGear:Z

    return-void
.end method

.method private demonstrateEndpointSetup()V
    .locals 3

    .prologue
    .line 720
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v2, 0x4e2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    .line 721
    return-void
.end method

.method private ensureCacheEmpty(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 840
    sget-object v1, Lcom/vlingo/midas/VlingoApplication;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sac sec ensureCacheEmpty() car_iux_tts_cacheing_required set to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "car_iux_tts_cacheing_required"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Landroid/content/Context;Z)I

    move-result v0

    .line 842
    .local v0, "numDeleted":I
    sget-object v1, Lcom/vlingo/midas/VlingoApplication;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sac sec ensureCacheEmpty() purged "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " historically cached TTS files"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 843
    return-void
.end method

.method private ensureCacheEmptyAndOff()V
    .locals 2

    .prologue
    .line 856
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->applicationAdapter()Lcom/vlingo/core/facade/IApplicationAdapter;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/facade/IApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 857
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->setTtsCachingOff()V

    .line 858
    invoke-direct {p0, v0}, Lcom/vlingo/midas/VlingoApplication;->ensureCacheEmpty(Landroid/content/Context;)V

    .line 859
    return-void
.end method

.method public static getInstance()Lcom/vlingo/midas/VlingoApplication;
    .locals 1

    .prologue
    .line 731
    sget-object v0, Lcom/vlingo/midas/VlingoApplication;->instance:Lcom/vlingo/midas/VlingoApplication;

    return-object v0
.end method

.method public static getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    sget-object v0, Lcom/vlingo/midas/VlingoApplication;->appVersion:Ljava/lang/String;

    return-object v0
.end method

.method private initClientSpecificSettings(Landroid/content/SharedPreferences$Editor;)V
    .locals 6
    .param p1, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 628
    const-string/jumbo v2, "key_midas_first_run"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 629
    .local v0, "firstRun":Z
    if-eqz v0, :cond_0

    .line 630
    const-string/jumbo v2, "is_svoice_for_gear"

    iget-boolean v3, p0, Lcom/vlingo/midas/VlingoApplication;->sVoiceForGear:Z

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 633
    :cond_0
    if-eqz v0, :cond_1

    .line 634
    const-string/jumbo v2, "seamless_wakeup"

    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 635
    const-string/jumbo v2, "SEND_WAKEUP_WORD_HEADERS"

    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 641
    :cond_1
    invoke-static {p1}, Lcom/vlingo/midas/settings/MidasSettings;->init(Landroid/content/SharedPreferences$Editor;)V

    .line 644
    const-string/jumbo v2, "max_audio_time"

    const v3, 0x9c40

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 647
    const-string/jumbo v2, "endpoint.speechdetect_min_voice_level"

    const/high16 v3, 0x42340000    # 45.0f

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 650
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v1

    .line 651
    .local v1, "phoneDrivingModeEnabled":Z
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 652
    if-eqz v1, :cond_3

    .line 653
    invoke-static {p1}, Lcom/vlingo/core/internal/settings/Settings;->enableCarMode(Landroid/content/SharedPreferences$Editor;)V

    .line 666
    :goto_0
    const-string/jumbo v2, "USE_EDM"

    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 667
    const-string/jumbo v2, "USE_PHONE_DISAMBIG"

    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 668
    const-string/jumbo v2, "calendar.app_package"

    const-string/jumbo v3, "com.android.calendar"

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 669
    const-string/jumbo v2, "calendar.preference_filename"

    const-string/jumbo v3, "com.android.calendar_preferences"

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 670
    const-string/jumbo v2, "calendar.default_calendar_key"

    const-string/jumbo v3, "preference_defaultCalendar"

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 673
    const-string/jumbo v2, "use_mediasync_tone_approach"

    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 674
    const-string/jumbo v2, "use_audiotrack_tone_player"

    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 675
    const-string/jumbo v2, "processing_tone_fadeout_period"

    const/16 v3, 0x1f4

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 676
    const-string/jumbo v2, "custom_tone_encoding"

    const-string/jumbo v3, "PCM_22k"

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 678
    const-string/jumbo v2, "screen.mag"

    invoke-virtual {p0}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 679
    const-string/jumbo v2, "screen.width"

    invoke-virtual {p0}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 681
    const-string/jumbo v2, "validate_launch_intent_version"

    invoke-interface {p1, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 683
    const-string/jumbo v2, "vcs.timeout.ms"

    const/16 v3, 0x2710

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 685
    new-instance v2, Lcom/vlingo/midas/VlingoApplication$2;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/VlingoApplication$2;-><init>(Lcom/vlingo/midas/VlingoApplication;)V

    iput-object v2, p0, Lcom/vlingo/midas/VlingoApplication;->preferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 696
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/VlingoApplication;->preferenceListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 698
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->lockFileDeleteIfExists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 699
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/VlingoApplication;->startMainService(Z)V

    .line 702
    :cond_2
    return-void

    .line 655
    :cond_3
    invoke-static {p1}, Lcom/vlingo/core/internal/settings/Settings;->disableCarMode(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_0

    .line 661
    :cond_4
    const-string/jumbo v2, "driving_mode_on"

    invoke-interface {p1, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_0
.end method

.method private setSpeakerPhoneOnByDefault(Landroid/content/SharedPreferences$Editor;)V
    .locals 3
    .param p1, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 725
    const-string/jumbo v1, "car_auto_start_speakerphone"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 726
    .local v0, "value":Z
    const-string/jumbo v1, "car_auto_start_speakerphone"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 727
    return-void
.end method


# virtual methods
.method public getAppChannel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 324
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    const-string/jumbo v0, "Preinstall Free China"

    .line 328
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "Preinstall Free"

    goto :goto_0
.end method

.method public getAppID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    const-string/jumbo v0, "com.samsung.android.tproject"

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    const-string/jumbo v0, "SamsungTproject"

    return-object v0
.end method

.method public getMainActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 735
    const-class v0, Lcom/vlingo/midas/gui/ConversationActivity;

    return-object v0
.end method

.method public isAppInForeground()Z
    .locals 1

    .prologue
    .line 749
    invoke-static {}, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;->isAppInForeground()Z

    move-result v0

    return v0
.end method

.method public isInForeground()Z
    .locals 1

    .prologue
    .line 828
    iget-boolean v0, p0, Lcom/vlingo/midas/VlingoApplication;->isInForeground:Z

    return v0
.end method

.method public isSocialLogin()Z
    .locals 1

    .prologue
    .line 832
    iget-boolean v0, p0, Lcom/vlingo/midas/VlingoApplication;->socialLogin:Z

    return v0
.end method

.method public onCreate()V
    .locals 27

    .prologue
    .line 458
    new-instance v2, Lcom/vlingo/midas/VlingoApplication$1;

    const-string/jumbo v3, "Preload SharedPreferences"

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/vlingo/midas/VlingoApplication$1;-><init>(Lcom/vlingo/midas/VlingoApplication;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication$1;->start()V

    .line 465
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$integer;->device_type:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->setDeviceType(I)V

    .line 466
    invoke-super/range {p0 .. p0}, Landroid/app/Application;->onCreate()V

    .line 467
    sput-object p0, Lcom/vlingo/midas/VlingoApplication;->instance:Lcom/vlingo/midas/VlingoApplication;

    .line 469
    new-instance v26, Lcom/vlingo/midas/Version;

    invoke-direct/range {v26 .. v26}, Lcom/vlingo/midas/Version;-><init>()V

    .line 470
    .local v26, "version":Lcom/vlingo/midas/Version;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v24

    .line 472
    .local v24, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v23

    .line 473
    .local v23, "packageInfo":Landroid/content/pm/PackageInfo;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/midas/Version;->getSVN()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/vlingo/midas/VlingoApplication;->appVersion:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 478
    .end local v23    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v14

    .line 479
    .local v14, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v0, v14, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v16, v0

    .line 480
    .local v16, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/vlingo/midas/VlingoApplication;->sVoiceForGear:Z

    .line 481
    if-eqz v16, :cond_0

    .line 482
    const-string/jumbo v2, "com.vlingo.midas.application_type"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 483
    .local v15, "applicationType":Ljava/lang/String;
    if-eqz v15, :cond_0

    .line 484
    const-string/jumbo v2, "svoiceforgear"

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/vlingo/midas/VlingoApplication;->sVoiceForGear:Z
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    .line 493
    .end local v14    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v15    # "applicationType":Ljava/lang/String;
    .end local v16    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_1
    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/midas/Version;->getVersion()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/vlingo/midas/VlingoApplication;->appVersion:Ljava/lang/String;

    .line 494
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->initContext(Landroid/content/Context;)V

    .line 495
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getCoreVersion()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/vlingo/midas/VlingoApplication;->coreVersion:Ljava/lang/String;

    .line 496
    sget-object v2, Lcom/vlingo/midas/VlingoApplication;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/vlingo/midas/VlingoApplication;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " appVersion =\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/vlingo/midas/VlingoApplication;->appVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\', coreVersion = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/vlingo/midas/VlingoApplication;->coreVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\', ro.csc.sales_code="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    new-instance v2, Lcom/vlingo/midas/MidasValues;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/vlingo/midas/MidasValues;-><init>(Landroid/content/Context;)V

    invoke-static {v2}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->setInterface(Lcom/vlingo/core/internal/util/ClientSuppliedValuesInterface;)V

    .line 500
    invoke-static {}, Lcom/vlingo/midas/util/ServerDetails;->getInstance()Lcom/vlingo/midas/util/ServerDetails;

    move-result-object v9

    .line 502
    .local v9, "serverDetails":Lcom/vlingo/midas/util/ServerDetails;
    :try_start_2
    new-instance v3, Lcom/vlingo/midas/CoreResourceProviderImpl;

    invoke-direct {v3}, Lcom/vlingo/midas/CoreResourceProviderImpl;-><init>()V

    const-string/jumbo v5, "com.samsung.android.tproject"

    const-string/jumbo v6, "SamsungTproject"

    sget-object v7, Lcom/vlingo/midas/VlingoApplication;->appVersion:Ljava/lang/String;

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v8

    sget-object v10, Lcom/vlingo/core/facade/RecognitionMode;->CLOUD:Lcom/vlingo/core/facade/RecognitionMode;

    new-instance v11, Lcom/vlingo/midas/notification/MidasNotificationPopUpFactory;

    invoke-direct {v11}, Lcom/vlingo/midas/notification/MidasNotificationPopUpFactory;-><init>()V

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/FieldIds;->generateFieldIdsMap()Ljava/util/Map;

    move-result-object v12

    const/4 v13, 0x1

    move-object/from16 v2, p0

    move-object/from16 v4, p0

    invoke-static/range {v2 .. v13}, Lcom/vlingo/core/internal/VlingoAndroidCore;->init(Landroid/content/Context;Lcom/vlingo/core/internal/ResourceIdProvider;Lcom/vlingo/core/internal/util/VlingoApplicationInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/core/internal/util/CoreServerInfo;Lcom/vlingo/core/facade/RecognitionMode;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;Ljava/util/Map;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 508
    :goto_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v18

    .line 509
    .local v18, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->demonstrateEndpointSetup()V

    .line 510
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/vlingo/midas/VlingoApplication;->initClientSpecificSettings(Landroid/content/SharedPreferences$Editor;)V

    .line 512
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 514
    const-string/jumbo v2, "is_from_vlingoapplication"

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 515
    new-instance v25, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    move-object/from16 v0, v25

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 517
    .local v25, "startIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 519
    const-string/jumbo v2, "Always"

    const-string/jumbo v3, "Always service started in VlingoApplication"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    .end local v25    # "startIntent":Landroid/content/Intent;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->setAppName(Ljava/lang/String;)V

    .line 528
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vlingo/midas/VlingoApplication;->sVoiceForGear:Z

    if-nez v2, :cond_4

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v2

    if-nez v2, :cond_4

    .line 529
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v2

    if-nez v2, :cond_3

    .line 530
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "com.vlingo.midas.settings.DrivingModeSettings"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v3, 0x2

    const/4 v4, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 536
    :cond_3
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.settings.VOICE_INPUT_CONTROL_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_4

    .line 537
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "com.samsung.bargeinsetting.VoiceInputControlSettings"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v3, 0x2

    const/4 v4, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 554
    :cond_4
    new-instance v2, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v2

    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 563
    const-string/jumbo v2, "facebook_app_id"

    sget-object v3, Lcom/vlingo/midas/settings/MidasSettings;->FACEBOOK_APP_ID_DEFAULT:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 566
    const-string/jumbo v2, "twitter_consumer_key"

    const-string/jumbo v3, "AGv8Ps3AlFKrf2C1YoFkQ"

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 567
    const-string/jumbo v2, "twitter_consumer_secret"

    const-string/jumbo v3, "qeX5TCXa9HPDlpNmhPACOT7sUerHPmD91Oq9nYuw6Q"

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 570
    const-string/jumbo v2, "weibo_app_id"

    const-string/jumbo v3, "2291996457"

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 571
    const-string/jumbo v2, "weibo_redirect_url"

    const-string/jumbo v3, "http://www.samsung.com/sec"

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 573
    const-string/jumbo v2, "use_network_tts"

    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 575
    const-string/jumbo v2, "stream_volume"

    const/4 v3, -0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 576
    const-string/jumbo v2, "system_volume"

    const/4 v3, -0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 580
    invoke-static {}, Lcom/vlingo/core/internal/util/DeviceWorkarounds;->shouldIgnoreRequiredSamsungTTSEngine()Z

    move-result v2

    if-nez v2, :cond_5

    .line 583
    const-string/jumbo v2, "tts_local_required_engine"

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 585
    :cond_5
    const-string/jumbo v2, "tts_local_ignore_use_speech_rate"

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 587
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/vlingo/midas/VlingoApplication;->setSpeakerPhoneOnByDefault(Landroid/content/SharedPreferences$Editor;)V

    .line 588
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 590
    const-string/jumbo v2, "location_enabled"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v21

    .line 591
    .local v21, "loc_enable":Z
    if-nez v21, :cond_6

    .line 592
    const-string/jumbo v2, "location_enabled"

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 596
    .end local v21    # "loc_enable":Z
    :cond_6
    const-string/jumbo v2, "obey_device_location_settings"

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 599
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getADMController()Lcom/vlingo/core/internal/util/ADMController;

    .line 601
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/midas/samsungutils/utils/SVoxPronunciationManager;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/TTSEngine;->setPronunciationManager(Lcom/vlingo/core/internal/audio/TTSEngine$PronunciationManager;)V

    .line 602
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/OpenAppUtil;->setOpenAppNameManager(Lcom/vlingo/core/internal/util/OpenAppUtil$IOpenAppNameManager;)V

    .line 604
    new-instance v20, Landroid/content/Intent;

    invoke-direct/range {v20 .. v20}, Landroid/content/Intent;-><init>()V

    .line 605
    .local v20, "intent":Landroid/content/Intent;
    sget-object v2, Lcom/vlingo/midas/VlingoApplication;->ACTION_VLINGO_APP_START:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 606
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/VlingoApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 609
    const-string/jumbo v2, "ShowUnreadMessagesWidget"

    const-class v3, Lcom/vlingo/midas/dialogmanager/vvs/handlers/SamsungAlertReadbackHandler;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Ljava/lang/String;Ljava/lang/Class;)V

    .line 610
    new-instance v22, Landroid/content/Intent;

    const-string/jumbo v2, "com.sec.android.app.music.intent.action.PLAY_VIA"

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 611
    .local v22, "musicIntent":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/vlingo/sdk/internal/util/PackageUtil;->canHandleIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 612
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_ALBUM:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 613
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_ARTIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 614
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_PLAYLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 615
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_TITLE:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/midas/dialogmanager/actions/PlayMusicViaIntentAction;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 616
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DMActionType;->PLAY_SONGLIST:Lcom/vlingo/core/internal/dialogmanager/DMActionType;

    const-class v3, Lcom/vlingo/midas/dialogmanager/actions/PlaySongListAction;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DMActionFactory;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/DMActionType;Ljava/lang/Class;)V

    .line 619
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/VlingoApplication;->ensureCacheEmptyAndOff()V

    .line 621
    invoke-static/range {v18 .. v18}, Lcom/vlingo/midas/settings/MidasSettings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 624
    const-string/jumbo v2, "korean_name_similarity_value_min"

    const v3, 0x3f2b851f    # 0.67f

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setFloat(Ljava/lang/String;F)V

    .line 625
    return-void

    .line 474
    .end local v9    # "serverDetails":Lcom/vlingo/midas/util/ServerDetails;
    .end local v18    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v20    # "intent":Landroid/content/Intent;
    .end local v22    # "musicIntent":Landroid/content/Intent;
    :catch_0
    move-exception v19

    .line 475
    .local v19, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/midas/Version;->getVersion()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/vlingo/midas/VlingoApplication;->appVersion:Ljava/lang/String;

    goto/16 :goto_0

    .line 505
    .end local v19    # "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v9    # "serverDetails":Lcom/vlingo/midas/util/ServerDetails;
    :catch_1
    move-exception v17

    .line 506
    .local v17, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "Vlingoapplication"

    const-string/jumbo v3, "exception caught"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 489
    .end local v9    # "serverDetails":Lcom/vlingo/midas/util/ServerDetails;
    .end local v17    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    goto/16 :goto_1
.end method

.method public onTerminate()V
    .locals 0

    .prologue
    .line 743
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->destroy()V

    .line 744
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 745
    return-void
.end method

.method public setIsInForeground(Z)V
    .locals 3
    .param p1, "isInForeground"    # Z

    .prologue
    .line 822
    iput-boolean p1, p0, Lcom/vlingo/midas/VlingoApplication;->isInForeground:Z

    .line 823
    const-string/jumbo v0, "VlingoApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Focus setInForeground = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 824
    return-void
.end method

.method public setSocialLogin(Z)V
    .locals 0
    .param p1, "socialLogin"    # Z

    .prologue
    .line 836
    iput-boolean p1, p0, Lcom/vlingo/midas/VlingoApplication;->socialLogin:Z

    .line 837
    return-void
.end method

.method public startMainActivity()V
    .locals 4

    .prologue
    .line 757
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 758
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/midas/VlingoApplication;->getMainActivityClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 759
    .local v1, "i":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 760
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 761
    return-void
.end method

.method public startMainService(Z)V
    .locals 3
    .param p1, "closeApplication"    # Z

    .prologue
    .line 768
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 769
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 770
    const-string/jumbo v1, "com.vlingo.client.app.action.CLOSE_APPLICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 772
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 773
    return-void
.end method

.method public startSocialLogin(Landroid/content/Context;Lcom/vlingo/midas/social/api/SocialNetworkType;Lcom/vlingo/midas/social/api/SocialAPI;)V
    .locals 5
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "socialNetworkType"    # Lcom/vlingo/midas/social/api/SocialNetworkType;
    .param p3, "api"    # Lcom/vlingo/midas/social/api/SocialAPI;

    .prologue
    const/16 v4, 0x20

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 776
    iput-boolean v3, p0, Lcom/vlingo/midas/VlingoApplication;->socialLogin:Z

    .line 777
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 778
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 779
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 780
    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->WEIBO:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne p2, v1, :cond_1

    const-string/jumbo v1, "weibo_account"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 781
    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 794
    :goto_0
    const-string/jumbo v1, "choice"

    invoke-virtual {p2}, Lcom/vlingo/midas/social/api/SocialNetworkType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 795
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 797
    invoke-static {p2}, Lcom/vlingo/midas/util/SocialUtils;->setUpdateType(Lcom/vlingo/midas/social/api/SocialNetworkType;)V

    .line 799
    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->FACEBOOK:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne p2, v1, :cond_6

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 800
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 819
    :cond_0
    :goto_1
    return-void

    .line 782
    :cond_1
    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->ALL:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne p2, v1, :cond_0

    const-string/jumbo v1, "weibo_account"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 783
    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 788
    :cond_2
    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->TWITTER:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-eq p2, v1, :cond_3

    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->ALL:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne p2, v1, :cond_4

    .line 789
    :cond_3
    const-string/jumbo v1, "android.intent.extra.INTENT"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 791
    :cond_4
    const-string/jumbo v1, "android.intent.extra.INTENT"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 803
    :cond_5
    invoke-static {p0, v3, v3}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->startFacebookSSO(Landroid/content/Context;ZZ)V

    goto :goto_1

    .line 805
    :cond_6
    sget-object v1, Lcom/vlingo/midas/social/api/SocialNetworkType;->TWITTER:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne p2, v1, :cond_7

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 806
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 809
    invoke-static {p0, v3, v3}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->startTwitterSSO(Landroid/content/Context;ZZ)V

    goto :goto_1

    .line 812
    :cond_7
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-nez v1, :cond_8

    .line 813
    invoke-static {v0}, Lcom/vlingo/midas/util/SocialUtils;->setIntentAfterTTS(Landroid/content/Intent;)V

    goto :goto_1

    .line 815
    :cond_8
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
