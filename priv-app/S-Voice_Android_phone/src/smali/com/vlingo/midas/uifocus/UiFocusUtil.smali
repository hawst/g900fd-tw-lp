.class public Lcom/vlingo/midas/uifocus/UiFocusUtil;
.super Ljava/lang/Object;
.source "UiFocusUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/uifocus/UiFocusUtil$OnUiFocusChangeListenerImpl;
    }
.end annotation


# static fields
.field public static associatedService:Z

.field public static smListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    return-void
.end method

.method public static onBackgrounded(Z)V
    .locals 1
    .param p0, "associatedServiceValue"    # Z

    .prologue
    .line 57
    if-eqz p0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    sget-object v0, Lcom/vlingo/midas/uifocus/UiFocusUtil;->smListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/uifocus/UiFocusUtil;->smListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 63
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->abandonUiFocus()V

    goto :goto_0
.end method

.method public static onForegrounded(Landroid/content/Context;ZLandroid/content/Intent;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "associatedServiceValue"    # Z
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    if-eqz p1, :cond_0

    .line 24
    sput-boolean p1, Lcom/vlingo/midas/uifocus/UiFocusUtil;->associatedService:Z

    move v1, v2

    .line 51
    :goto_0
    return v1

    .line 28
    :cond_0
    instance-of v1, p0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    move-object v1, p0

    .line 29
    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 30
    .local v0, "current":Landroid/content/Intent;
    if-eqz v0, :cond_1

    const-string/jumbo v1, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 33
    new-instance v1, Lcom/vlingo/midas/uifocus/UiFocusUtil$OnUiFocusChangeListenerImpl;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/uifocus/UiFocusUtil$OnUiFocusChangeListenerImpl;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/vlingo/midas/uifocus/UiFocusUtil;->smListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 34
    sput-boolean p1, Lcom/vlingo/midas/uifocus/UiFocusUtil;->associatedService:Z

    .line 35
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/uifocus/UiFocusUtil;->smListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->requestUiFocus(Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V

    move v1, v3

    .line 36
    goto :goto_0

    .line 40
    .end local v0    # "current":Landroid/content/Intent;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->remoteHasUiFocus()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 42
    invoke-static {p0, p2}, Lcom/vlingo/midas/uifocus/UiFocusUtil;->showUiFocusScreen(Landroid/content/Context;Landroid/content/Intent;)V

    move v1, v2

    .line 43
    goto :goto_0

    .line 47
    :cond_2
    new-instance v1, Lcom/vlingo/midas/uifocus/UiFocusUtil$OnUiFocusChangeListenerImpl;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/uifocus/UiFocusUtil$OnUiFocusChangeListenerImpl;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/vlingo/midas/uifocus/UiFocusUtil;->smListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 48
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/uifocus/UiFocusUtil;->smListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->requestUiFocus(Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V

    move v1, v3

    .line 51
    goto :goto_0
.end method

.method private static showUiFocusScreen(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 71
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/uifocus/UiFocusUtil;->smListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 73
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 76
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->abandonUiFocus()V

    .line 78
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 79
    return-void
.end method
