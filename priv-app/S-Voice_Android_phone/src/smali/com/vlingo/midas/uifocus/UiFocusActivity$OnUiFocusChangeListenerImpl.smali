.class Lcom/vlingo/midas/uifocus/UiFocusActivity$OnUiFocusChangeListenerImpl;
.super Ljava/lang/Object;
.source "UiFocusActivity.java"

# interfaces
.implements Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/uifocus/UiFocusActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnUiFocusChangeListenerImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/uifocus/UiFocusActivity;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/uifocus/UiFocusActivity;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity$OnUiFocusChangeListenerImpl;->this$0:Lcom/vlingo/midas/uifocus/UiFocusActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/uifocus/UiFocusActivity;Lcom/vlingo/midas/uifocus/UiFocusActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/uifocus/UiFocusActivity;
    .param p2, "x1"    # Lcom/vlingo/midas/uifocus/UiFocusActivity$1;

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/vlingo/midas/uifocus/UiFocusActivity$OnUiFocusChangeListenerImpl;-><init>(Lcom/vlingo/midas/uifocus/UiFocusActivity;)V

    return-void
.end method


# virtual methods
.method public onUiFocusChange(I)V
    .locals 2
    .param p1, "focusChange"    # I

    .prologue
    .line 138
    iget-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity$OnUiFocusChangeListenerImpl;->this$0:Lcom/vlingo/midas/uifocus/UiFocusActivity;

    # getter for: Lcom/vlingo/midas/uifocus/UiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    invoke-static {v0}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->access$100(Lcom/vlingo/midas/uifocus/UiFocusActivity;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 153
    :goto_0
    return-void

    .line 144
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 147
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity$OnUiFocusChangeListenerImpl;->this$0:Lcom/vlingo/midas/uifocus/UiFocusActivity;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/uifocus/UiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    invoke-static {v0, v1}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->access$102(Lcom/vlingo/midas/uifocus/UiFocusActivity;Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 148
    iget-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity$OnUiFocusChangeListenerImpl;->this$0:Lcom/vlingo/midas/uifocus/UiFocusActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->finish()V

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
