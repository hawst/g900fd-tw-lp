.class public Lcom/vlingo/midas/uifocus/UiFocusActivity;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "UiFocusActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/uifocus/UiFocusActivity$1;,
        Lcom/vlingo/midas/uifocus/UiFocusActivity$OnUiFocusChangeListenerImpl;
    }
.end annotation


# instance fields
.field private mContinueBtn:Landroid/widget/ImageView;

.field private mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 135
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/uifocus/UiFocusActivity;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/uifocus/UiFocusActivity;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/uifocus/UiFocusActivity;Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/uifocus/UiFocusActivity;
    .param p1, "x1"    # Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    return-object p1
.end method

.method private exitApp()V
    .locals 3

    .prologue
    .line 129
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 131
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.app.action.CLOSE_APPLICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 133
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->exitApp()V

    .line 123
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mContinueBtn:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mContinueBtn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 103
    iget-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    if-eqz v0, :cond_0

    .line 104
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->requestUiFocus(Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V

    .line 107
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.vlingo.midas.ACTION_UIFOCUS_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 115
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->setRequestedOrientation(I)V

    .line 43
    :cond_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    sget v0, Lcom/vlingo/midas/R$layout;->ui_focus:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->setContentView(I)V

    .line 47
    sget v0, Lcom/vlingo/midas/R$id;->micSvoice:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mContinueBtn:Landroid/widget/ImageView;

    .line 51
    iget-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mContinueBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 82
    invoke-virtual {p0}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 83
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onStart()V

    .line 68
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->remoteHasUiFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Lcom/vlingo/midas/uifocus/UiFocusActivity$OnUiFocusChangeListenerImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/uifocus/UiFocusActivity$OnUiFocusChangeListenerImpl;-><init>(Lcom/vlingo/midas/uifocus/UiFocusActivity;Lcom/vlingo/midas/uifocus/UiFocusActivity$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 70
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->waitForUiFocus(Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;)V

    .line 76
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/uifocus/UiFocusActivity;->finish()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onStop()V

    .line 90
    iget-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/uifocus/UiFocusActivity;->mFocusListener:Lcom/vlingo/core/internal/associatedservice/UiFocusManager$OnUiFocusChangeListener;

    .line 92
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->abandonUiFocus()V

    .line 94
    :cond_0
    return-void
.end method
