.class public Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;
.super Landroid/app/Activity;
.source "SeamlessRecoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/vlingo/core/internal/audio/MicAnimationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$1;,
        Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;
    }
.end annotation


# static fields
.field static layout:Landroid/view/View;


# instance fields
.field private abc:[[I

.field private index:I

.field private keepRecoAlive:Z

.field private stateChangeReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;

.field private thinkingView:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->layout:Landroid/view/View;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 46
    iput-boolean v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->keepRecoAlive:Z

    .line 49
    iput v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->index:I

    .line 157
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->thinkingView:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$202(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->keepRecoAlive:Z

    return p1
.end method

.method static synthetic access$300(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->switchToConversationActivity(Z)V

    return-void
.end method

.method private dismissKeyguard()V
    .locals 10

    .prologue
    .line 206
    const/4 v4, 0x1

    .line 208
    .local v4, "secure":Z
    :try_start_0
    const-string/jumbo v7, "com.android.internal.widget.LockPatternUtils"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 209
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    invoke-virtual {v0, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 210
    .local v1, "con":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v1, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 211
    .local v3, "instance":Ljava/lang/Object;
    const-string/jumbo v8, "isSecure"

    const/4 v7, 0x0

    check-cast v7, [Ljava/lang/Class;

    invoke-virtual {v0, v8, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 212
    .local v2, "get":Ljava/lang/reflect/Method;
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 222
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "con":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    .end local v2    # "get":Ljava/lang/reflect/Method;
    .end local v3    # "instance":Ljava/lang/Object;
    :goto_0
    if-nez v4, :cond_0

    .line 223
    invoke-virtual {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 224
    .local v5, "win":Landroid/view/Window;
    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    .line 225
    .local v6, "winParams":Landroid/view/WindowManager$LayoutParams;
    iget v7, v6, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v8, 0x680000

    or-int/2addr v7, v8

    iput v7, v6, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 226
    invoke-virtual {v5, v6}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 228
    .end local v5    # "win":Landroid/view/Window;
    .end local v6    # "winParams":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    return-void

    .line 218
    :catch_0
    move-exception v7

    goto :goto_0
.end method

.method public static getRandom(II)I
    .locals 6
    .param p0, "minValue"    # I
    .param p1, "maxValue"    # I

    .prologue
    .line 346
    int-to-double v0, p0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    add-int/lit8 v4, p1, 0x1

    sub-int/2addr v4, p0

    int-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method static init(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$layout;->seamless_main:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->layout:Landroid/view/View;

    .line 54
    return-void
.end method

.method private switchToConversationActivity(Z)V
    .locals 3
    .param p1, "resumeReco"    # Z

    .prologue
    .line 190
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 191
    .local v0, "conversationActivity":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 192
    const-string/jumbo v1, "resumeFromBackground"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    :goto_0
    const-string/jumbo v1, "AUTO_LISTEN"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 197
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 198
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->startActivity(Landroid/content/Intent;)V

    .line 199
    invoke-virtual {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->finish()V

    .line 200
    return-void

    .line 194
    :cond_0
    const-string/jumbo v1, "displayFromBackground"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 143
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 151
    .local v0, "id":I
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->setVolumeControlStream(I)V

    .line 65
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->considerRightBeforeForeground(Z)V

    .line 67
    sget-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->layout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->setContentView(Landroid/view/View;)V

    .line 70
    const/4 v0, 0x5

    const/16 v1, 0x80

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->abc:[[I

    .line 72
    new-instance v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->stateChangeReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;

    .line 73
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->stateChangeReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "reco_status_change"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 74
    invoke-direct {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->dismissKeyguard()V

    .line 75
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->stateChangeReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->stateChangeReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;

    .line 134
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 135
    return-void
.end method

.method public onMicAnimationData([I)V
    .locals 0
    .param p1, "v"    # [I

    .prologue
    .line 232
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->showSpectrum([I)V

    .line 233
    return-void
.end method

.method public onPause()V
    .locals 6

    .prologue
    .line 103
    const-string/jumbo v4, "keyguard"

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    .line 104
    .local v2, "kgMgr":Landroid/app/KeyguardManager;
    invoke-virtual {v2}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v3

    .line 105
    .local v3, "locked":Z
    if-nez v3, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->finish()V

    .line 111
    :cond_0
    iget-boolean v4, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->keepRecoAlive:Z

    if-nez v4, :cond_1

    .line 112
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v4, "abort_reco"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    .local v0, "abort":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 116
    .end local v0    # "abort":Landroid/content/Intent;
    :cond_1
    sget-object v4, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->layout:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    sget-object v5, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->layout:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 118
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->removeListener(Lcom/vlingo/core/internal/audio/MicAnimationListener;)V

    .line 120
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 121
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v4, "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const-string/jumbo v4, "com.vlingo.client.app.extra.STATE"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 123
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 125
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 126
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 79
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 83
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    const-string/jumbo v1, "com.vlingo.client.app.extra.STATE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 86
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 88
    sget v1, Lcom/vlingo/midas/R$id;->control_full_container:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 95
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->addListener(Lcom/vlingo/core/internal/audio/MicAnimationListener;)V

    .line 97
    return-void
.end method

.method public showSpectrum([I)V
    .locals 0
    .param p1, "spectrum"    # [I

    .prologue
    .line 254
    return-void
.end method
