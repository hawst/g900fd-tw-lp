.class Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SeamlessRecoService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivityStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V
    .locals 0

    .prologue
    .line 561
    iput-object p1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;
    .param p2, "x1"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;

    .prologue
    .line 561
    invoke-direct {p0, p1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 564
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "abort_reco"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 565
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    # invokes: Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->abortReco()V
    invoke-static {v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->access$700(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 566
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "listen_clicked"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 567
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->endpointReco()V

    goto :goto_0
.end method
