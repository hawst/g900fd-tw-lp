.class public Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;
.super Ljava/lang/Object;
.source "WidgetObject.java"

# interfaces
.implements Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;"
    }
.end annotation


# instance fields
.field private widget:Lcom/vlingo/midas/gui/Widget;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vlingo/midas/gui/Widget",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/Widget;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/midas/gui/Widget",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 10
    .local p0, "this":Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;, "Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject<TT;>;"
    .local p1, "widget":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;->widget:Lcom/vlingo/midas/gui/Widget;

    .line 12
    return-void
.end method


# virtual methods
.method public execute(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 1
    .param p1, "ca"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 17
    .local p0, "this":Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;, "Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject<TT;>;"
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;->widget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->addWidget(Lcom/vlingo/midas/gui/Widget;)V

    .line 18
    return-void
.end method

.method public mustBeShown()Z
    .locals 1

    .prologue
    .line 21
    .local p0, "this":Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;, "Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject<TT;>;"
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;->widget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/Widget;->mustBeShown()Z

    move-result v0

    return v0
.end method
