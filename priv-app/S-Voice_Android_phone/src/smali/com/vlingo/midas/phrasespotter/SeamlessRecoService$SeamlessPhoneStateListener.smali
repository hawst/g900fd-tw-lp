.class Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "SeamlessRecoService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SeamlessPhoneStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;
    .param p2, "x1"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;

    .prologue
    .line 572
    invoke-direct {p0, p1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 575
    packed-switch p1, :pswitch_data_0

    .line 586
    :goto_0
    return-void

    .line 577
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    # invokes: Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->resume()V
    invoke-static {v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->access$600(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    goto :goto_0

    .line 580
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    # invokes: Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->pause()V
    invoke-static {v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->access$500(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    goto :goto_0

    .line 583
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    # invokes: Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->pause()V
    invoke-static {v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->access$500(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    goto :goto_0

    .line 575
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
