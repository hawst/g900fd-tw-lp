.class public Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;
.super Landroid/app/Service;
.source "SeamlessRecoService.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;
.implements Lcom/vlingo/core/internal/util/ADMFeatureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$5;,
        Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;,
        Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;,
        Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;
    }
.end annotation


# static fields
.field public static final ABORT_RECO:Ljava/lang/String; = "abort_reco"

.field public static final AUDIO_SOURCE:Ljava/lang/String; = "bg_audio_source"

.field public static final DISPLAYING_GUI:Ljava/lang/String; = "speech_display"

.field public static final IDLE:Ljava/lang/String; = "speech_idle"

.field public static final LISTENING:Ljava/lang/String; = "speech_listening"

.field public static final LISTEN_CLICKED:Ljava/lang/String; = "listen_clicked"

.field public static final PASSING_RECO:Ljava/lang/String; = "speech_passing"

.field public static final PAUSE:Ljava/lang/String; = "seamless_pause"

.field public static final RESUME:Ljava/lang/String; = "seamless_resume"

.field public static final RMS_CHANGE:Ljava/lang/String; = "speech_rms_change"

.field public static final RMS_VALUE:Ljava/lang/String; = "speech_rms_value"

.field public static final SERVICE_ID:I = 0x1221d

.field public static final STATUS:Ljava/lang/String; = "reco_status"

.field public static final STATUS_CHANGE:Ljava/lang/String; = "reco_status_change"

.field public static final STOP_SERVICE:Ljava/lang/String; = "stop_seamlessreco_service"

.field public static final THINKING:Ljava/lang/String; = "speech_thinking"

.field private static dialogObjects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private audioSource:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

.field private hasFlowControl:Z

.field private isFirstUtt:Z

.field private pauseReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;

.field private phoneStateListener:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;

.field private startRecoOnSpotterStop:Z

.field private stateReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;

.field private telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 93
    iput-boolean v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startRecoOnSpotterStop:Z

    .line 94
    iput-boolean v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->hasFlowControl:Z

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->isFirstUtt:Z

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->audioSource:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .line 572
    return-void
.end method

.method private abortReco()V
    .locals 1

    .prologue
    .line 591
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 592
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 593
    return-void
.end method

.method static synthetic access$302(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startRecoOnSpotterStop:Z

    return p1
.end method

.method static synthetic access$400(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;
    .param p1, "x1"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    return-void
.end method

.method static synthetic access$500(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->pause()V

    return-void
.end method

.method static synthetic access$600(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->resume()V

    return-void
.end method

.method static synthetic access$700(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->abortReco()V

    return-void
.end method

.method public static getDialogObjects()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 600
    sget-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->dialogObjects:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getNotification()Landroid/app/Notification;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 533
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "stop_seamlessreco_service"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 534
    .local v1, "notificationIntent":Landroid/content/Intent;
    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 535
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v3, Landroid/app/Notification$Builder;

    invoke-direct {v3, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/vlingo/midas/R$string;->bg_wakeup_notification_title:I

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->bg_wakeup_notification_text:I

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->seamless_reco_notification:I

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 541
    .local v0, "nb":Landroid/app/Notification$Builder;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_0

    .line 542
    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 544
    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v3

    goto :goto_0
.end method

.method private initPhraseSpotter()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 462
    const-string/jumbo v1, "samsung_wakeup_engine_enable"

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 465
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v2, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 466
    iget-object v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->audioSource:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->audioSource:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    invoke-static {v1}, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->getPhraseSpotterParameters(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 480
    .local v0, "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1, v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 481
    return-void

    .line 466
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->getPhraseSpotterParameters()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    goto :goto_0

    .line 468
    :cond_1
    const-string/jumbo v1, "samsung_multi_engine_enable"

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 471
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v2, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 472
    iget-object v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->audioSource:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->audioSource:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    invoke-static {v1, v2}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :goto_1
    goto :goto_0

    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    goto :goto_1

    .line 477
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->audioSource:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->audioSource:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    invoke-static {v1, v2}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :goto_2
    goto :goto_0

    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    goto :goto_2
.end method

.method private pause()V
    .locals 1

    .prologue
    .line 189
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 190
    return-void
.end method

.method private resume()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->initPhraseSpotter()V

    .line 183
    invoke-direct {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startSpotting()V

    .line 184
    return-void
.end method

.method private startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 5
    .param p1, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 501
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 504
    iput-boolean v3, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startRecoOnSpotterStop:Z

    .line 505
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 528
    :goto_0
    return-void

    .line 512
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 513
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    .line 514
    .local v1, "isBtHeadsetConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v0

    .line 515
    .local v0, "isBluetoothAudioSupported":Z
    if-ne v1, v3, :cond_1

    if-nez v0, :cond_2

    .line 516
    :cond_1
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 521
    :goto_1
    new-instance v2, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$4;

    invoke-direct {v2, p0, p1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$4;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    invoke-static {v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 518
    :cond_2
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    goto :goto_1
.end method

.method private startSpotting()V
    .locals 1

    .prologue
    .line 487
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->isFirstUtt:Z

    .line 488
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->dialogObjects:Ljava/util/ArrayList;

    .line 490
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->startPhraseSpotting()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 495
    :goto_0
    return-void

    .line 491
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public addWidget(Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 604
    .local p1, "wo":Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;, "Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject<TT;>;"
    sget-object v1, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->dialogObjects:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 605
    invoke-virtual {p1}, Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;->mustBeShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 606
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "reco_status_change"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 607
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "reco_status"

    const-string/jumbo v2, "speech_display"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 608
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->sendBroadcast(Landroid/content/Intent;)V

    .line 610
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public enteredDomain(Lcom/vlingo/core/internal/domain/DomainName;)V
    .locals 0
    .param p1, "domain"    # Lcom/vlingo/core/internal/domain/DomainName;

    .prologue
    .line 643
    return-void
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x0

    return-object v0
.end method

.method public onASRRecorderClosed()V
    .locals 0

    .prologue
    .line 631
    return-void
.end method

.method public onASRRecorderOpened()V
    .locals 0

    .prologue
    .line 625
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 111
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 112
    new-instance v2, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;

    invoke-direct {v2, p0, v3}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;)V

    iput-object v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->pauseReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;

    .line 113
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v2, "seamless_pause"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 114
    .local v0, "pauseIntentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "seamless_resume"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 115
    const-string/jumbo v2, "stop_seamlessreco_service"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 116
    iget-object v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->pauseReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 118
    new-instance v2, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;

    invoke-direct {v2, p0, v3}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;)V

    iput-object v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->stateReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;

    .line 119
    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "abort_reco"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 120
    .local v1, "stateIntentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "listen_clicked"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 121
    iget-object v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->stateReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 122
    new-instance v2, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;

    invoke-direct {v2, p0, v3}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;)V

    iput-object v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->phoneStateListener:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;

    .line 123
    const-string/jumbo v2, "phone"

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    iput-object v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 124
    iget-object v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->telephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v3, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->phoneStateListener:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;

    const/16 v4, 0x20

    invoke-virtual {v2, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 126
    invoke-virtual {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->init(Landroid/content/Context;)V

    .line 128
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    const-string/jumbo v2, "widget_display_max"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 133
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 134
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->unregisterHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;)V

    .line 138
    :goto_1
    return-void

    .line 131
    :cond_0
    const-string/jumbo v2, "widget_display_max"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 136
    :cond_1
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v3, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ListenHandler;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->telephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->phoneStateListener:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->telephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->phoneStateListener:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$SeamlessPhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->pauseReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 173
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->stateReceiver:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$ActivityStateReceiver;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 174
    invoke-direct {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->pause()V

    .line 175
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->destroy()V

    .line 176
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 177
    return-void
.end method

.method public onInterceptStartReco()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 278
    iget-boolean v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->isFirstUtt:Z

    if-nez v2, :cond_0

    .line 279
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "reco_status_change"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 280
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v2, "reco_status"

    const-string/jumbo v3, "speech_passing"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->sendBroadcast(Landroid/content/Intent;)V

    .line 283
    iput-boolean v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->hasFlowControl:Z

    .line 284
    const/4 v1, 0x1

    .line 287
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return v1

    .line 286
    :cond_0
    iput-boolean v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->isFirstUtt:Z

    goto :goto_0
.end method

.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 4
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    .line 302
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 303
    .local v0, "recoActivity":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 304
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 305
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startActivity(Landroid/content/Intent;)V

    .line 306
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/vlingo/midas/phrasespotter/dialog/BackgroundWidgetBuilder;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/phrasespotter/dialog/BackgroundWidgetBuilder;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    invoke-virtual {v1, p0, p0, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->initFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 307
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->hasFlowControl:Z

    .line 308
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 5
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 314
    const/4 v0, -0x1

    .line 315
    .local v0, "iResult":I
    const/4 v1, -0x1

    .line 317
    .local v1, "intType":I
    invoke-static {p1}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->isCustomWakeupWordPhrase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 318
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 323
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 353
    :goto_1
    if-eq v1, v4, :cond_0

    .line 355
    invoke-static {v1, p0}, Lcom/samsung/voiceshell/MultipleWakeUp;->getMultipleWakeUpIntent(ILandroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 356
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 357
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startActivity(Landroid/content/Intent;)V

    .line 358
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 361
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    return-void

    .line 320
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 326
    :pswitch_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startRecoOnSpotterStop:Z

    .line 327
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    goto :goto_1

    .line 332
    :pswitch_1
    const-string/jumbo v3, "kew_wake_up_and_auto_function1"

    invoke-static {v3, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 333
    goto :goto_1

    .line 337
    :pswitch_2
    const-string/jumbo v3, "kew_wake_up_and_auto_function2"

    invoke-static {v3, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 338
    goto :goto_1

    .line 342
    :pswitch_3
    const-string/jumbo v3, "kew_wake_up_and_auto_function3"

    invoke-static {v3, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 343
    goto :goto_1

    .line 347
    :pswitch_4
    const-string/jumbo v3, "kew_wake_up_and_auto_function4"

    invoke-static {v3, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 348
    goto :goto_1

    .line 323
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onPhraseSpotterStarted()V
    .locals 2

    .prologue
    .line 389
    const v0, 0x1221d

    invoke-direct {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->getNotification()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startForeground(ILandroid/app/Notification;)V

    .line 390
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->stopForeground(Z)V

    .line 376
    iget-boolean v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startRecoOnSpotterStop:Z

    if-eqz v0, :cond_0

    .line 377
    new-instance v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$3;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$3;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 385
    :cond_0
    return-void
.end method

.method public onRecoCancelled()V
    .locals 0

    .prologue
    .line 272
    return-void
.end method

.method public onRecoToneStarting(Z)J
    .locals 2
    .param p1, "startTone"    # Z

    .prologue
    .line 618
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onRecoToneStopped(Z)V
    .locals 0
    .param p1, "startTone"    # Z

    .prologue
    .line 637
    return-void
.end method

.method public onResultsNoAction()V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 1
    .param p1, "spottedPhrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 365
    new-instance v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$2;

    invoke-direct {v0, p0, p2}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$2;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 371
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startid"    # I

    .prologue
    const/4 v6, 0x1

    .line 142
    if-eqz p1, :cond_0

    const-string/jumbo v4, "bg_audio_source"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "bg_audio_source"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 144
    :try_start_0
    const-string/jumbo v4, "bg_audio_source"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->audioSource:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :cond_0
    :goto_0
    :try_start_1
    new-instance v4, Lcom/vlingo/midas/notification/MidasNotificationPopUpFactory;

    invoke-direct {v4}, Lcom/vlingo/midas/notification/MidasNotificationPopUpFactory;-><init>()V

    invoke-static {v4}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->hasNotifications(Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 156
    .local v1, "hasNotifications":Z
    if-eqz v1, :cond_1

    .line 157
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    .local v2, "i":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 159
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startActivity(Landroid/content/Intent;)V

    .line 163
    .end local v1    # "hasNotifications":Z
    .end local v2    # "i":Landroid/content/Intent;
    :goto_1
    return v6

    .line 145
    :catch_0
    move-exception v3

    .line 146
    .local v3, "iae":Ljava/lang/IllegalArgumentException;
    const-string/jumbo v4, "SeamlessRecoService"

    const-string/jumbo v5, "Invalid AudioSourceType provided. Using default value."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 153
    .end local v3    # "iae":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 161
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "hasNotifications":Z
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->resume()V

    goto :goto_1
.end method

.method public onStatusChanged(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    .line 398
    sget-object v0, Lcom/vlingo/midas/MidasADMController;->AGGRESSIVE_NOISE_CANCELLATION:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 399
    if-eqz p2, :cond_0

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    sget-object v0, Lcom/vlingo/midas/MidasADMController;->ENDPOINT_DETECTION:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 409
    if-eqz p2, :cond_0

    goto :goto_0

    .line 418
    :cond_2
    sget-object v0, Lcom/vlingo/midas/MidasADMController;->EYES_FREE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 419
    if-eqz p2, :cond_3

    .line 422
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    const-string/jumbo v1, "isEyesFree"

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->addUserProperties(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 426
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    const-string/jumbo v1, "isEyesFree"

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeUserProperties(Ljava/lang/String;)V

    goto :goto_0

    .line 428
    :cond_4
    sget-object v0, Lcom/vlingo/midas/MidasADMController;->DRIVING_MODE_GUI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 429
    if-eqz p2, :cond_5

    .line 433
    const-string/jumbo v0, "widget_display_max"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 439
    :cond_5
    const-string/jumbo v0, "widget_display_max"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 442
    :cond_6
    sget-object v0, Lcom/vlingo/midas/MidasADMController;->TALKBACK:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    if-eqz p2, :cond_7

    .line 446
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    invoke-static {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->unregisterHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;)V

    goto :goto_0

    .line 450
    :cond_7
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ListenHandler;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V
    .locals 3
    .param p1, "newState"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    .prologue
    .line 247
    sget-object v1, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$5;->$SwitchMap$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 249
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "reco_status_change"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 250
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "reco_status"

    const-string/jumbo v2, "speech_idle"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->sendBroadcast(Landroid/content/Intent;)V

    .line 252
    iget-boolean v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->hasFlowControl:Z

    if-eqz v1, :cond_0

    .line 253
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->hasFlowControl:Z

    .line 254
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->releaseFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;)V

    .line 255
    invoke-direct {p0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->startSpotting()V

    goto :goto_0

    .line 247
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public showError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 198
    invoke-static {p1}, Lcom/vlingo/midas/gui/ConversationActivity;->shouldReportError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-static {p1, p0}, Lcom/vlingo/midas/util/ErrorCodeUtils;->getLocalizedMessageForErrorCode(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 201
    :cond_0
    return-void
.end method

.method public showRMSChange(I)V
    .locals 2
    .param p1, "rmsValue"    # I

    .prologue
    .line 214
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "speech_rms_change"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 215
    .local v0, "rmsChange":Landroid/content/Intent;
    const-string/jumbo v1, "speech_rms_value"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 216
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->sendBroadcast(Landroid/content/Intent;)V

    .line 217
    return-void
.end method

.method public showReceivedResults(Lcom/vlingo/core/internal/logging/EventLog;)V
    .locals 0
    .param p1, "results"    # Lcom/vlingo/core/internal/logging/EventLog;

    .prologue
    .line 220
    return-void
.end method

.method public showRecoStateChange(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 1
    .param p1, "newState"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    .line 227
    new-instance v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->runOnMainThread(Ljava/lang/Runnable;)V

    .line 243
    return-void
.end method

.method public showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "nbest"    # Lcom/vlingo/sdk/recognition/NBestData;

    .prologue
    .line 268
    sget-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->dialogObjects:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/midas/phrasespotter/dialog/UserTurnObject;

    invoke-direct {v1, p1, p2}, Lcom/vlingo/midas/phrasespotter/dialog/UserTurnObject;-><init>(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    return-void
.end method

.method public showVlingoText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 263
    sget-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->dialogObjects:Ljava/util/ArrayList;

    new-instance v1, Lcom/vlingo/midas/phrasespotter/dialog/SystemTurnObject;

    invoke-direct {v1, p1}, Lcom/vlingo/midas/phrasespotter/dialog/SystemTurnObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    return-void
.end method

.method public showWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;
    .locals 1
    .param p1, "warning"    # Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 205
    sget-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;->WARNING_NOTHING_RECOGNIZED:Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;

    if-eq p1, v0, :cond_0

    .line 206
    const/4 v0, 0x1

    invoke-static {p0, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 207
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;->ShowedWarning:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;

    .line 209
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;->Noop:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;

    goto :goto_0
.end method

.method public userCancel()V
    .locals 0

    .prologue
    .line 613
    return-void
.end method
