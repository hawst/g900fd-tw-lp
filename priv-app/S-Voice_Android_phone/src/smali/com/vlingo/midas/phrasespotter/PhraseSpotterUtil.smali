.class public Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;
.super Ljava/lang/Object;
.source "PhraseSpotterUtil.java"


# static fields
.field private static final CG_DIR:Ljava/io/File;

.field private static final DEFAULT_DELTA_D:I = -0x12c

.field public static final SEAMLESS_TIMEOUT_MS:I = 0x190

.field private static final WAKE_UP_FILES_EXTERNAL_STORAGE:Ljava/lang/String; = "/system/wakeupdata/sensory"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    const-string/jumbo v1, "phrasespotter"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/VlingoApplication;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->CG_DIR:Ljava/io/File;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static chooseAudioSourceType()Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->SPOTTER_DRIVING_MODE:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .line 64
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;->SPOTTER_REGULAR:Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    goto :goto_0
.end method

.method private static createFileIfNecessary(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "cgResName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 94
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 96
    .local v2, "context":Landroid/content/Context;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ".raw"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "cgFileName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    sget-object v10, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->CG_DIR:Ljava/io/File;

    invoke-direct {v3, v10, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 98
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 99
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_1

    .line 101
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    .line 138
    :cond_0
    :goto_0
    return-object v9

    .line 106
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 111
    :cond_2
    const-string/jumbo v10, "raw"

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, p1, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 112
    .local v8, "resID":I
    if-eqz v8, :cond_0

    .line 118
    invoke-virtual {p0, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v6

    .line 119
    .local v6, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 121
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v5, "fos":Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    :try_start_1
    new-array v0, v10, [B

    .line 124
    .local v0, "buffer":[B
    :goto_1
    invoke-virtual {v6, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    .local v7, "length":I
    if-lez v7, :cond_3

    .line 125
    const/4 v10, 0x0

    invoke-virtual {v5, v0, v10, v7}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 130
    .end local v0    # "buffer":[B
    .end local v7    # "length":I
    :catch_0
    move-exception v10

    move-object v4, v5

    .line 135
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :goto_2
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_6

    .line 136
    :goto_3
    :try_start_3
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v10

    goto :goto_0

    .line 127
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "length":I
    :cond_3
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V

    .line 128
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v9

    .line 135
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_4

    .line 136
    :goto_4
    :try_start_6
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    :catch_2
    move-exception v10

    goto :goto_0

    .line 135
    .end local v0    # "buffer":[B
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .end local v7    # "length":I
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v9

    :goto_5
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_8

    .line 136
    :goto_6
    :try_start_8
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_9

    :goto_7
    throw v9

    .line 135
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "length":I
    :catch_3
    move-exception v10

    goto :goto_4

    :catch_4
    move-exception v10

    goto :goto_4

    .end local v0    # "buffer":[B
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .end local v7    # "length":I
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v10

    goto :goto_3

    :catch_6
    move-exception v10

    goto :goto_3

    :catch_7
    move-exception v10

    goto :goto_6

    :catch_8
    move-exception v10

    goto :goto_6

    .line 136
    :catch_9
    move-exception v10

    goto :goto_7

    .line 135
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 130
    :catch_a
    move-exception v10

    goto :goto_2
.end method

.method public static getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 39
    invoke-static {}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->chooseAudioSourceType()Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    return-object v0
.end method

.method public static getPhraseSpotterParameters(Landroid/content/res/Resources;Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 3
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "audioSourceType"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .prologue
    .line 43
    invoke-static {p0}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getWakeupCoreParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v0

    .line 45
    .local v0, "csParams":Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    new-instance v1, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;-><init>(Ljava/lang/String;Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)V

    .line 46
    .local v1, "pspBuilder":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    const/16 v2, 0x190

    invoke-virtual {v1, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->setSeamlessTimeout(I)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 47
    invoke-virtual {v1, p1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->setAudioSourceType(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 48
    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->build()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v2

    return-object v2
.end method

.method public static getWakeupCoreParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    .locals 11
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 68
    sget v5, Lcom/vlingo/midas/R$string;->phrasespotter_SEARCH_GRAMMAR_RES_NAME:I

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 69
    .local v9, "settingValue":Ljava/lang/String;
    invoke-static {p0, v9}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->createFileIfNecessary(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 74
    .local v6, "absoluteFilePath":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v8

    .line 75
    .local v8, "language":Ljava/lang/String;
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    invoke-direct {v0, v8, v6}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .local v0, "cspBuilder":Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;
    const-string/jumbo v5, "phrasespot_beam"

    const/high16 v10, 0x41a00000    # 20.0f

    invoke-static {v5, v10}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v1

    .line 78
    .local v1, "beam":F
    const-string/jumbo v5, "phrasespot_absbeam"

    const/high16 v10, 0x42200000    # 40.0f

    invoke-static {v5, v10}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v2

    .line 79
    .local v2, "absbeam":F
    const-string/jumbo v5, "phrasespot_aoffset"

    const/4 v10, 0x0

    invoke-static {v5, v10}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v3

    .line 80
    .local v3, "aoffset":F
    const-string/jumbo v5, "phrasespot_delay"

    const/high16 v10, 0x42c80000    # 100.0f

    invoke-static {v5, v10}, Lcom/vlingo/core/internal/settings/Settings;->getFloat(Ljava/lang/String;F)F

    move-result v4

    .line 82
    .local v4, "delay":F
    const-string/jumbo v5, "/system/wakeupdata/sensory"

    invoke-virtual/range {v0 .. v5}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->setSensoryParams(FFFFLjava/lang/String;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .line 83
    const/16 v5, -0x12c

    invoke-virtual {v0, v5}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->setDeltaD(I)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;

    .line 85
    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters$Builder;->build()Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v7

    .line 87
    .local v7, "csParams":Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    return-object v7
.end method

.method public static isCustomWakeupWordPhrase(Ljava/lang/String;)Z
    .locals 2
    .param p0, "phrase"    # Ljava/lang/String;

    .prologue
    .line 53
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "ex":Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    goto :goto_0
.end method
