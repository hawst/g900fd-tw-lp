.class public Lcom/vlingo/midas/phrasespotter/dialog/UserTurnObject;
.super Ljava/lang/Object;
.source "UserTurnObject.java"

# interfaces
.implements Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;


# instance fields
.field private nbest:Lcom/vlingo/sdk/recognition/NBestData;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "nbest"    # Lcom/vlingo/sdk/recognition/NBestData;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/vlingo/midas/phrasespotter/dialog/UserTurnObject;->text:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/vlingo/midas/phrasespotter/dialog/UserTurnObject;->nbest:Lcom/vlingo/sdk/recognition/NBestData;

    .line 14
    return-void
.end method


# virtual methods
.method public execute(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 2
    .param p1, "ca"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/dialog/UserTurnObject;->text:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/midas/phrasespotter/dialog/UserTurnObject;->nbest:Lcom/vlingo/sdk/recognition/NBestData;

    invoke-virtual {p1, v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V

    .line 19
    return-void
.end method
