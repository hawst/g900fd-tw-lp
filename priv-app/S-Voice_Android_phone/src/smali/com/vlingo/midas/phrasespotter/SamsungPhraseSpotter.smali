.class public Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;
.super Ljava/lang/Object;
.source "SamsungPhraseSpotter.java"

# interfaces
.implements Lcom/vlingo/core/internal/phrasespotter/VLPhraseSpotter;


# static fields
.field private static final SAMSUNG_CHUNK_LENGTH_MS:I = 0xa

.field private static final SAMSUNG_SEAMLESS_TIMEOUT_MS:I = 0x190

.field public static UDTAlwaysAPrecog:Ljava/lang/String;

.field public static UDTAlwaysAPsearch:Ljava/lang/String;


# instance fields
.field private final NUM_MODELS:I

.field private VElib:Lcom/samsung/voiceshell/VoiceEngine;

.field public consoleInitReturn:J

.field public consoleResult:Ljava/lang/String;

.field public isSensoryUDTSIDamFileExist:Z

.field public isSensoryUDTSIDsoFileExist:Z

.field public mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

.field private final sensoryUDTSIDSoFilePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string/jumbo v0, "/data/data/com.vlingo.midas/UDT_Always_AP_recog.raw"

    sput-object v0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->UDTAlwaysAPrecog:Ljava/lang/String;

    .line 31
    const-string/jumbo v0, "/data/data/com.vlingo.midas/UDT_Always_AP_search.raw"

    sput-object v0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->UDTAlwaysAPsearch:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string/jumbo v0, "/system/lib/libSensoryUDTSIDEngine.so"

    iput-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->sensoryUDTSIDSoFilePath:Ljava/lang/String;

    .line 35
    iput-boolean v1, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isSensoryUDTSIDsoFileExist:Z

    .line 36
    iput-boolean v1, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isSensoryUDTSIDamFileExist:Z

    .line 38
    iput-object v2, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->consoleInitReturn:J

    .line 40
    iput-object v2, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->consoleResult:Ljava/lang/String;

    .line 45
    iput-object v2, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    .line 46
    const/4 v0, 0x5

    iput v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->NUM_MODELS:I

    .line 49
    return-void
.end method

.method public static getPhraseSpotterParameters()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 1

    .prologue
    .line 206
    invoke-static {}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->chooseAudioSourceType()Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->getPhraseSpotterParameters(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    return-object v0
.end method

.method public static getPhraseSpotterParameters(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 5
    .param p0, "audioSourceType"    # Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;

    .prologue
    .line 212
    const-string/jumbo v3, "samsung_wakeup_engine_enable"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 213
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getWakeupCoreParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;

    move-result-object v1

    .line 214
    .local v1, "coreParams":Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;-><init>(Ljava/lang/String;Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;)V

    .line 218
    .end local v1    # "coreParams":Lcom/vlingo/core/internal/phrasespotter/CoreSpotterParameters;
    .local v0, "builder":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    :goto_0
    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->setChunkLength(I)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 219
    const/16 v3, 0x190

    invoke-virtual {v0, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->setSeamlessTimeout(I)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 220
    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->setAudioSourceType(Lcom/vlingo/core/internal/audio/MicrophoneStream$AudioSourceType;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    .line 221
    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;->build()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v2

    .line 222
    .local v2, "samsungSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    return-object v2

    .line 216
    .end local v0    # "builder":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    .end local v2    # "samsungSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_0
    new-instance v0, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;-><init>(Ljava/lang/String;)V

    .restart local v0    # "builder":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters$Builder;
    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 106
    iget-boolean v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v0, :cond_2

    .line 108
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    if-eqz v0, :cond_1

    .line 110
    iget-wide v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->consoleInitReturn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    iget-wide v1, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->consoleInitReturn:J

    invoke-virtual {v0, v1, v2}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;->phrasespotClose(J)V

    .line 114
    :cond_0
    iput-object v4, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    .line 124
    :cond_1
    :goto_0
    return-void

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    if-eqz v0, :cond_3

    .line 120
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v0}, Lcom/samsung/voiceshell/VoiceEngine;->terminateVerify()I

    .line 122
    :cond_3
    iput-object v4, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    goto :goto_0
.end method

.method public getDeltaD()I
    .locals 1

    .prologue
    .line 202
    const/16 v0, -0xc8

    return v0
.end method

.method public getSpottedPhraseScore()F
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public init(I)V
    .locals 7
    .param p1, "sampleRate"    # I

    .prologue
    const/4 v4, 0x1

    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "NumSets":I
    const/4 v3, 0x5

    new-array v2, v3, [I

    .line 55
    .local v2, "mExist":[I
    const/4 v1, 0x2

    .line 57
    .local v1, "WType":I
    const-string/jumbo v3, "/system/lib/libSensoryUDTSIDEngine.so"

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isFileExist(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isSensoryUDTSIDsoFileExist:Z

    .line 59
    sget-object v3, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->UDTAlwaysAPrecog:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isFileExist(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->UDTAlwaysAPsearch:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isFileExist(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isSensoryUDTSIDamFileExist:Z

    .line 61
    iget-boolean v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v3, :cond_2

    .line 63
    invoke-static {}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngineWrapper;->getInstance()Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    .line 68
    iget-boolean v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isSensoryUDTSIDamFileExist:Z

    if-eqz v3, :cond_0

    .line 70
    iget-object v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    sget-object v4, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->UDTAlwaysAPrecog:Ljava/lang/String;

    sget-object v5, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->UDTAlwaysAPsearch:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;->phrasespotInit(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->consoleInitReturn:J

    .line 101
    :cond_0
    :goto_1
    return-void

    .line 59
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 76
    :cond_2
    invoke-static {}, Lcom/samsung/voiceshell/VoiceEngineWrapper;->getInstance()Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    .line 81
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 82
    const/4 v1, 0x3

    .line 85
    :cond_3
    iget-object v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v5, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    sget-object v5, Lcom/samsung/voiceshell/VoiceEngine;->ROOT:Ljava/lang/String;

    invoke-virtual {v3, v5, v1, v2}, Lcom/samsung/voiceshell/VoiceEngine;->checkFileExistence(Ljava/lang/String;I[I)I

    move-result v0

    .line 87
    if-eqz v0, :cond_0

    .line 91
    iget-object v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v3}, Lcom/samsung/voiceshell/VoiceEngine;->terminateVerify()I

    .line 92
    iget-object v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v5, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v5, v5, Lcom/samsung/voiceshell/VoiceEngine;->m_UBMpath_default:Ljava/lang/String;

    iget-object v6, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    sget-object v6, Lcom/samsung/voiceshell/VoiceEngine;->ROOT:Ljava/lang/String;

    invoke-virtual {v3, v5, v6, v1}, Lcom/samsung/voiceshell/VoiceEngine;->initializeVerify(Ljava/lang/String;Ljava/lang/String;I)I

    .line 94
    iget-object v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v3, v4}, Lcom/samsung/voiceshell/VoiceEngine;->setMode(I)V

    .line 95
    iget-object v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v4, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v4, v4, Lcom/samsung/voiceshell/VoiceEngine;->m_UBMpath_default:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/voiceshell/VoiceEngine;->setAdaptationModelPath(Ljava/lang/String;)V

    .line 97
    iget-object v3, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v3}, Lcom/samsung/voiceshell/VoiceEngine;->startVerify()I

    goto :goto_1
.end method

.method public isFileExist(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mFilePath"    # Ljava/lang/String;

    .prologue
    .line 226
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 227
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 228
    const/4 v1, 0x1

    .line 230
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public processShortArray([SII)Ljava/lang/String;
    .locals 12
    .param p1, "audioData"    # [S
    .param p2, "offset"    # I
    .param p3, "size"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    .line 128
    new-array v5, v11, [S

    .line 129
    .local v5, "CommandType":[S
    new-array v4, v11, [S

    .line 130
    .local v4, "frameLeftNumber":[S
    int-to-short v0, p2

    aput-short v0, v4, v6

    .line 132
    iget-boolean v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    if-eqz v0, :cond_0

    .line 136
    iget-wide v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->consoleInitReturn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->mSensoryWakeUpEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    iget-wide v1, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->consoleInitReturn:J

    int-to-long v4, p3

    const-wide/16 v6, 0x3e80

    move-object v3, p1

    invoke-virtual/range {v0 .. v7}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;->phrasespotPipe(J[SJJ)Ljava/lang/String;

    .end local v4    # "frameLeftNumber":[S
    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->consoleResult:Ljava/lang/String;

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->consoleResult:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 144
    iput-object v10, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->consoleResult:Ljava/lang/String;

    .line 145
    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v10

    .line 149
    goto :goto_0

    .line 155
    .restart local v4    # "frameLeftNumber":[S
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v1, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    sget-object v3, Lcom/samsung/voiceshell/VoiceEngine;->ROOT:Ljava/lang/String;

    move-object v1, p1

    move v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/voiceshell/VoiceEngine;->processBuffer([SILjava/lang/String;[S[S)I

    move-result v9

    .line 158
    .local v9, "iResult":I
    if-ne v9, v11, :cond_3

    .line 160
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/voiceshell/VoiceEngine;->setMode(I)V

    .line 164
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v1, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v1, v1, Lcom/samsung/voiceshell/VoiceEngine;->m_UBMpath_default:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    sget-object v2, Lcom/samsung/voiceshell/VoiceEngine;->ROOT:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/voiceshell/VoiceEngine;->performContinuousAdaptation(Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 167
    .local v8, "contADAPTreturn":I
    aget-short v0, v5, v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .end local v8    # "contADAPTreturn":I
    :cond_3
    move-object v0, v10

    .line 171
    goto :goto_0
.end method

.method public useSeamlessFeature(Ljava/lang/String;)Z
    .locals 4
    .param p1, "spottedPhrase"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 183
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string/jumbo v3, "seamless_wakeup"

    invoke-static {v3, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 185
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 186
    .local v0, "iResult":I
    if-ne v0, v1, :cond_1

    .line 190
    iget-object v2, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    if-eqz v2, :cond_0

    .line 191
    iget-object v2, p0, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->VElib:Lcom/samsung/voiceshell/VoiceEngine;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/voiceshell/VoiceEngine;->setMode(I)V

    .line 197
    .end local v0    # "iResult":I
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0
.end method
