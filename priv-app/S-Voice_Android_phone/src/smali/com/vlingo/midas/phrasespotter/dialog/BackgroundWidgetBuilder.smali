.class public Lcom/vlingo/midas/phrasespotter/dialog/BackgroundWidgetBuilder;
.super Lcom/vlingo/midas/gui/WidgetBuilder;
.source "BackgroundWidgetBuilder.java"


# instance fields
.field private srs:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V
    .locals 1
    .param p1, "srs"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/vlingo/midas/gui/WidgetBuilder;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Landroid/content/Context;)V

    .line 17
    iput-object p1, p0, Lcom/vlingo/midas/phrasespotter/dialog/BackgroundWidgetBuilder;->srs:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    .line 18
    return-void
.end method


# virtual methods
.method public setEavesdropper(Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionEavesdropper;)V
    .locals 0
    .param p1, "ed"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionEavesdropper;

    .prologue
    .line 26
    return-void
.end method

.method public showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 3
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "TT;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22
    .local p3, "object":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/dialog/BackgroundWidgetBuilder;->srs:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    new-instance v1, Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/phrasespotter/dialog/BackgroundWidgetBuilder;->buildWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)Lcom/vlingo/midas/gui/Widget;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;-><init>(Lcom/vlingo/midas/gui/Widget;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->addWidget(Lcom/vlingo/midas/phrasespotter/dialog/WidgetObject;)V

    .line 23
    return-void
.end method
