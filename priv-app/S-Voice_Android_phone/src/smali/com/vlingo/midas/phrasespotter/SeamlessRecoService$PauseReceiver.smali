.class Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SeamlessRecoService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PauseReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;
    .param p2, "x1"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;

    .prologue
    .line 548
    invoke-direct {p0, p1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 551
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "seamless_pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 552
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    # invokes: Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->pause()V
    invoke-static {v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->access$500(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    .line 558
    :cond_0
    :goto_0
    return-void

    .line 553
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "seamless_resume"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 554
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    # invokes: Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->resume()V
    invoke-static {v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->access$600(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;)V

    goto :goto_0

    .line 555
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "stop_seamlessreco_service"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$PauseReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    invoke-virtual {v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->stopSelf()V

    goto :goto_0
.end method
