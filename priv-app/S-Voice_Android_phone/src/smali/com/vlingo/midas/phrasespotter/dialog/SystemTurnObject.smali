.class public Lcom/vlingo/midas/phrasespotter/dialog/SystemTurnObject;
.super Ljava/lang/Object;
.source "SystemTurnObject.java"

# interfaces
.implements Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;


# instance fields
.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/vlingo/midas/phrasespotter/dialog/SystemTurnObject;->text:Ljava/lang/String;

    .line 11
    return-void
.end method


# virtual methods
.method public execute(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 1
    .param p1, "ca"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/dialog/SystemTurnObject;->text:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->showVlingoText(Ljava/lang/String;)V

    .line 16
    return-void
.end method
