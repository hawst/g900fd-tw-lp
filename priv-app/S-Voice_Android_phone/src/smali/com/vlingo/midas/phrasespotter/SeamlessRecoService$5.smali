.class synthetic Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$5;
.super Ljava/lang/Object;
.source "SeamlessRecoService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

.field static final synthetic $SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 247
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->values()[Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$5;->$SwitchMap$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

    :try_start_0
    sget-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$5;->$SwitchMap$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    .line 231
    :goto_0
    invoke-static {}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->values()[Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$5;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    :try_start_1
    sget-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$5;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    sget-object v0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$5;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->THINKING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1

    .line 247
    :catch_2
    move-exception v0

    goto :goto_0
.end method
