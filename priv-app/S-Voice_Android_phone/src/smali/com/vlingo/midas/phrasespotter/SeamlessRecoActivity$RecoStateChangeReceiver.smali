.class Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SeamlessRecoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecoStateChangeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;
    .param p2, "x1"    # Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$1;

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;-><init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 161
    const-string/jumbo v0, "speech_thinking"

    const-string/jumbo v1, "reco_status"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;

    # getter for: Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->thinkingView:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->access$100(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_0

    .line 175
    :cond_0
    const-string/jumbo v0, "speech_display"

    const-string/jumbo v1, "reco_status"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;

    # setter for: Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->keepRecoAlive:Z
    invoke-static {v0, v2}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->access$202(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;Z)Z

    .line 177
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;

    # invokes: Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->switchToConversationActivity(Z)V
    invoke-static {v0, v3}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->access$300(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;Z)V

    .line 179
    :cond_1
    const-string/jumbo v0, "speech_passing"

    const-string/jumbo v1, "reco_status"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;

    # setter for: Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->keepRecoAlive:Z
    invoke-static {v0, v2}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->access$202(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;Z)Z

    .line 181
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;

    # invokes: Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->switchToConversationActivity(Z)V
    invoke-static {v0, v2}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->access$300(Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;Z)V

    .line 183
    :cond_2
    const-string/jumbo v0, "speech_idle"

    const-string/jumbo v1, "reco_status"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 184
    iget-object v0, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity$RecoStateChangeReceiver;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoActivity;->finish()V

    .line 186
    :cond_3
    return-void
.end method
