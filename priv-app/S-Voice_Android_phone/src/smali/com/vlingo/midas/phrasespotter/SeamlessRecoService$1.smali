.class Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;
.super Ljava/lang/Object;
.source "SeamlessRecoService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->showRecoStateChange(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

.field final synthetic val$newState:Lcom/vlingo/sdk/recognition/VLRecognitionStates;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    iput-object p2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;->val$newState:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 230
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "reco_status_change"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 231
    .local v0, "i":Landroid/content/Intent;
    sget-object v1, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$5;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    iget-object v2, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;->val$newState:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v2}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 241
    :goto_0
    return-void

    .line 233
    :pswitch_0
    const-string/jumbo v1, "reco_status"

    const-string/jumbo v2, "speech_listening"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    iget-object v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 237
    :pswitch_1
    const-string/jumbo v1, "reco_status"

    const-string/jumbo v2, "speech_thinking"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    iget-object v1, p0, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService$1;->this$0:Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
