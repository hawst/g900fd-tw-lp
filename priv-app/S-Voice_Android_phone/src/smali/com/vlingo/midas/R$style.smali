.class public final Lcom/vlingo/midas/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AlarmListText:I = 0x7f0c018e

.field public static final AlarmListText_Days:I = 0x7f0c009a

.field public static final Alarm_Lin_Lyt:I = 0x7f0c0099

.field public static final ContactCountStyle:I = 0x7f0c0184

.field public static final ContactListStyle1:I = 0x7f0c0185

.field public static final ContactListStyle2:I = 0x7f0c0186

.field public static final ContactListStyle3:I = 0x7f0c0187

.field public static final ContactListStyle4:I = 0x7f0c0188

.field public static final CustomBlackNoActionBar:I = 0x7f0c0032

.field public static final CustomButtonStyle:I = 0x7f0c018d

.field public static final CustomDialogStyle:I = 0x7f0c018b

.field public static final CustomLightTheme:I = 0x7f0c017e

.field public static final CustomNoActionBar:I = 0x7f0c017a

.field public static final CustomTheme:I = 0x7f0c017d

.field public static final CustomWidgetStyle:I = 0x7f0c018c

.field public static final DialogAnimation:I = 0x7f0c017b

.field public static final DialogSlideAnim:I = 0x7f0c017c

.field public static final IUXBTNDivider:I = 0x7f0c0183

.field public static final IUXBottomBTN:I = 0x7f0c0179

.field public static final IUXDefaultBTN:I = 0x7f0c0180

.field public static final IUXDivider:I = 0x7f0c0182

.field public static final IUXNextBTN:I = 0x7f0c0181

.field public static final IUXPrevBTN:I = 0x7f0c017f

.field public static final ImageView01:I = 0x7f0c0069

.field public static final MidasBlackMainTheme:I = 0x7f0c002f

.field public static final MidasBlackMainThemeMain:I = 0x7f0c0394

.field public static final MidasBlackTheme:I = 0x7f0c002e

.field public static final MidasBlackThemeSettings:I = 0x7f0c0030

.field public static final MidasMainTheme:I = 0x7f0c002c

.field public static final MidasTheme:I = 0x7f0c002d

.field public static final MyActionBar:I = 0x7f0c0031

.field public static final Notice_Popup:I = 0x7f0c0190

.field public static final Notice_Popup_white:I = 0x7f0c0191

.field public static final RatingBarMoreSmall:I = 0x7f0c018a

.field public static final RatingBarSmall:I = 0x7f0c0189

.field public static final Svoice_style:I = 0x7f0c006e

.field public static final Svoice_style_1:I = 0x7f0c006f

.field public static final Svoice_style_2:I = 0x7f0c0074

.field public static final TextView02:I = 0x7f0c0065

.field public static final TextView02_style:I = 0x7f0c0070

.field public static final Theme_Transparent:I = 0x7f0c0192

.field public static final actionBarStyle:I = 0x7f0c016f

.field public static final actionBarStyleWakeup:I = 0x7f0c0170

.field public static final alarm_image:I = 0x7f0c018f

.field public static final alarm_item_center_style:I = 0x7f0c00a1

.field public static final alarm_vertical_divider:I = 0x7f0c009b

.field public static final audioTemplateRLayout_style:I = 0x7f0c0166

.field public static final audioTemplateRLayout_style_preload:I = 0x7f0c014a

.field public static final audioTemplateRLayout_style_preload_bottom:I = 0x7f0c0156

.field public static final audioTemplateRLayout_style_preload_bottom1:I = 0x7f0c0163

.field public static final bottom_linear_style:I = 0x7f0c02b0

.field public static final btn2_width:I = 0x7f0c0173

.field public static final btn_listening:I = 0x7f0c0016

.field public static final btn_width:I = 0x7f0c0172

.field public static final button_text_style:I = 0x7f0c0089

.field public static final checkbox_Style:I = 0x7f0c01a0

.field public static final clockwidget_ampm_text:I = 0x7f0c01ee

.field public static final clockwidget_ampm_text_korean:I = 0x7f0c01f9

.field public static final clockwidget_clockContainer:I = 0x7f0c01ed

.field public static final clockwidget_colon_style:I = 0x7f0c01f0

.field public static final clockwidget_country_text:I = 0x7f0c01fa

.field public static final clockwidget_dayContainer:I = 0x7f0c01f2

.field public static final clockwidget_day_text:I = 0x7f0c01f3

.field public static final clockwidget_horizontal_divider_style:I = 0x7f0c01f1

.field public static final clockwidget_location_text:I = 0x7f0c01f7

.field public static final clockwidget_month_years_text:I = 0x7f0c01f5

.field public static final clockwidget_time_style:I = 0x7f0c01ef

.field public static final clockwidget_time_text_style:I = 0x7f0c01f8

.field public static final clockwidget_vertical_divider:I = 0x7f0c01f4

.field public static final cma_widget_weather_todays_plus_weekly_child_layout6_k:I = 0x7f0c0378

.field public static final compose_wcis_RelativeLayout01:I = 0x7f0c0000

.field public static final compose_wcis_button:I = 0x7f0c0008

.field public static final compose_wcis_detail_image:I = 0x7f0c0001

.field public static final compose_wcis_header_did_you_know:I = 0x7f0c0007

.field public static final compose_wcis_textView1:I = 0x7f0c0003

.field public static final compose_wcis_textView2:I = 0x7f0c0004

.field public static final compose_wcis_textView3:I = 0x7f0c0005

.field public static final compose_wcis_text_container:I = 0x7f0c0006

.field public static final compose_wcis_title_text:I = 0x7f0c0002

.field public static final control_view_full_container_port:I = 0x7f0c000b

.field public static final control_view_image_button:I = 0x7f0c000c

.field public static final control_view_image_button_cover:I = 0x7f0c000d

.field public static final custom_alert_dialog_checkbox:I = 0x7f0c001a

.field public static final custom_alert_dialog_text_view:I = 0x7f0c0019

.field public static final custom_command_parent_body:I = 0x7f0c0024

.field public static final custom_command_recording_custom_wakeup_command:I = 0x7f0c001b

.field public static final custom_command_recording_rl_controlbody:I = 0x7f0c0023

.field public static final custom_command_recording_rl_ll:I = 0x7f0c001c

.field public static final custom_command_recording_rl_ll_customCommandHint:I = 0x7f0c0022

.field public static final custom_command_recording_rl_ll_customCommandHintLayout:I = 0x7f0c0020

.field public static final custom_command_recording_rl_ll_initBody:I = 0x7f0c0021

.field public static final custom_command_recording_rl_ll_ll:I = 0x7f0c001e

.field public static final custom_command_recording_rl_ll_titleCustomCommand:I = 0x7f0c001f

.field public static final custom_command_recording_rl_recordScrollView:I = 0x7f0c001d

.field public static final custom_help_parent_body:I = 0x7f0c0025

.field public static final dialog_bubble_user_style:I = 0x7f0c0034

.field public static final dialog_bubble_vlingo_prompt_vlingo_textview:I = 0x7f0c0036

.field public static final dialog_bubble_wakeup_style:I = 0x7f0c0037

.field public static final dialog_checkbox_padding:I = 0x7f0c019e

.field public static final dialog_custom_wake_up_count_text:I = 0x7f0c0045

.field public static final dialog_custom_wake_up_error_body_linear:I = 0x7f0c0040

.field public static final dialog_custom_wake_up_error_body_record_error:I = 0x7f0c003e

.field public static final dialog_custom_wake_up_error_body_retry_button:I = 0x7f0c0041

.field public static final dialog_custom_wake_up_error_body_setting:I = 0x7f0c003f

.field public static final dialog_custom_wake_up_initial_body_rl:I = 0x7f0c0042

.field public static final dialog_custom_wake_up_initial_body_rl_custom_wake_up_info_body:I = 0x7f0c0043

.field public static final dialog_custom_wake_up_record_body_linear:I = 0x7f0c0047

.field public static final dialog_custom_wake_up_record_body_relative:I = 0x7f0c0044

.field public static final dialog_custom_wake_up_record_body_setting:I = 0x7f0c0046

.field public static final dialog_custom_wake_up_record_body_setting_1:I = 0x7f0c0048

.field public static final dialog_custom_wake_up_record_body_setting_2:I = 0x7f0c004a

.field public static final dialog_custom_wake_up_record_body_setting_tv:I = 0x7f0c0049

.field public static final dialog_edittxt_margin:I = 0x7f0c019f

.field public static final dialog_help_screen_parent_container:I = 0x7f0c004b

.field public static final dialog_view_help_anim_layoutland_style:I = 0x7f0c0050

.field public static final dialog_view_help_anim_layoutlayoutland_style:I = 0x7f0c0055

.field public static final dialog_view_help_anim_layoutpotrt_style:I = 0x7f0c004f

.field public static final dialog_view_help_bubble_helpbutton_land_style:I = 0x7f0c0054

.field public static final dialog_view_help_bubble_helpbutton_ptrt_style:I = 0x7f0c0052

.field public static final dialog_view_help_bubble_micbutton_land_style:I = 0x7f0c0053

.field public static final dialog_view_help_bubble_micbutton_ptrt_style:I = 0x7f0c0051

.field public static final dialog_view_style:I = 0x7f0c004c

.field public static final dialog_view_style_scrollview:I = 0x7f0c004d

.field public static final dialog_view_style_shadow:I = 0x7f0c004e

.field public static final dialog_vlingo:I = 0x7f0c0035

.field public static final dialog_voice_prompt_confirmation_layout:I = 0x7f0c0056

.field public static final disclaimer_link_style:I = 0x7f0c0080

.field public static final driving_mode_custom_notification_close_btn:I = 0x7f0c005d

.field public static final driving_mode_custom_notification_close_btn_container:I = 0x7f0c005c

.field public static final driving_mode_custom_notification_left_container:I = 0x7f0c0057

.field public static final driving_mode_custom_notification_summary:I = 0x7f0c005b

.field public static final driving_mode_custom_notification_text_container:I = 0x7f0c0059

.field public static final driving_mode_custom_notification_thumnail:I = 0x7f0c0058

.field public static final driving_mode_custom_notification_title:I = 0x7f0c005a

.field public static final erase_server_data_body:I = 0x7f0c005f

.field public static final erase_server_data_btn:I = 0x7f0c005e

.field public static final erase_server_data_layout:I = 0x7f0c0060

.field public static final frame_layout_vert:I = 0x7f0c0010

.field public static final functionList:I = 0x7f0c01ad

.field public static final header_style:I = 0x7f0c0178

.field public static final helpEditWhatYouSaidLand:I = 0x7f0c007e

.field public static final helpEditWhatYouSaidPort:I = 0x7f0c007f

.field public static final helpHandwrittingDescription1:I = 0x7f0c0077

.field public static final helpHandwrittingDescription2:I = 0x7f0c007b

.field public static final helpHandwrittingImg:I = 0x7f0c0079

.field public static final helpHandwrittingLinear:I = 0x7f0c0078

.field public static final helpHandwrittingText:I = 0x7f0c007a

.field public static final help_about_bottom_container:I = 0x7f0c006c

.field public static final help_about_btn_privacy:I = 0x7f0c006b

.field public static final help_about_btn_tos:I = 0x7f0c006a

.field public static final help_about_container:I = 0x7f0c0061

.field public static final help_about_main_layout:I = 0x7f0c0076

.field public static final help_about_text_powered_by:I = 0x7f0c0068

.field public static final help_about_title:I = 0x7f0c0062

.field public static final help_anim_switch_padding:I = 0x7f0c019b

.field public static final help_list_style:I = 0x7f0c01c3

.field public static final help_popup_bubble_picker_style1:I = 0x7f0c01aa

.field public static final help_popup_bubble_picker_style2:I = 0x7f0c01ab

.field public static final help_popup_bubble_style1:I = 0x7f0c01a8

.field public static final help_popup_bubble_style2:I = 0x7f0c01a9

.field public static final help_popup_bubble_style_voicecontrol:I = 0x7f0c01ac

.field public static final help_popup_bubble_style_voicecontrol_relativelayout:I = 0x7f0c01af

.field public static final help_popup_bubble_style_voicecontrol_tooltip:I = 0x7f0c01b0

.field public static final helpbubble_imageview:I = 0x7f0c0014

.field public static final helptextonlist_style:I = 0x7f0c0142

.field public static final helptextonlist_style_2:I = 0x7f0c0143

.field public static final image:I = 0x7f0c01c2

.field public static final image_logo:I = 0x7f0c0064

.field public static final image_logo_samsung:I = 0x7f0c0063

.field public static final image_logo_samsung_style:I = 0x7f0c007c

.field public static final item_alarm_choice_error_in_out_image:I = 0x7f0c00a0

.field public static final item_alarm_choice_style_alarm_item_ampm:I = 0x7f0c0098

.field public static final item_alarm_choice_style_alarm_item_ampmnext:I = 0x7f0c009d

.field public static final item_alarm_choice_style_alarm_item_ampmprevious:I = 0x7f0c009e

.field public static final item_alarm_choice_style_alarm_item_leftside:I = 0x7f0c0094

.field public static final item_alarm_choice_style_alarm_item_leftside_up:I = 0x7f0c0095

.field public static final item_alarm_choice_style_alarm_item_leftside_up_time:I = 0x7f0c0096

.field public static final item_alarm_choice_style_alarm_items:I = 0x7f0c0097

.field public static final item_alarm_choice_style_alarm_subject:I = 0x7f0c009f

.field public static final item_alarm_choice_style_linearlayout:I = 0x7f0c0093

.field public static final item_alarm_enable:I = 0x7f0c009c

.field public static final item_application_chioce_container:I = 0x7f0c00a3

.field public static final item_application_choice_divider:I = 0x7f0c00a6

.field public static final item_application_choice_label:I = 0x7f0c00a5

.field public static final item_application_choice_relativeLayout:I = 0x7f0c00a4

.field public static final item_contact_choice_framelayout_1:I = 0x7f0c00a9

.field public static final item_contact_choice_imageview_1:I = 0x7f0c00aa

.field public static final item_contact_choice_imageview_2:I = 0x7f0c00ab

.field public static final item_contact_choice_linearlayout_1:I = 0x7f0c00a8

.field public static final item_contact_choice_parent:I = 0x7f0c00a7

.field public static final item_contact_choice_relativelayout_1:I = 0x7f0c00ac

.field public static final item_contact_choice_textview_1:I = 0x7f0c00ad

.field public static final item_contact_choice_textview_2:I = 0x7f0c00ae

.field public static final item_contact_choice_view_1:I = 0x7f0c00af

.field public static final item_contact_detail_linearlayout_1:I = 0x7f0c00b2

.field public static final item_contact_detail_parent:I = 0x7f0c00b1

.field public static final item_contact_detail_textview_1:I = 0x7f0c00b3

.field public static final item_contact_detail_textview_2:I = 0x7f0c00b4

.field public static final item_contact_detail_view_1:I = 0x7f0c00b6

.field public static final item_contact_detail_view_1_top:I = 0x7f0c00b5

.field public static final item_local_search_detail_review_irdv:I = 0x7f0c00b7

.field public static final item_local_search_imagebutton_1:I = 0x7f0c00bf

.field public static final item_local_search_linearlayout_1:I = 0x7f0c00b9

.field public static final item_local_search_linearlayout_2:I = 0x7f0c00bb

.field public static final item_local_search_parent:I = 0x7f0c00b8

.field public static final item_local_search_ratingbar_1:I = 0x7f0c00bc

.field public static final item_local_search_textview_1:I = 0x7f0c00ba

.field public static final item_local_search_textview_2:I = 0x7f0c00bd

.field public static final item_local_search_textview_3:I = 0x7f0c00be

.field public static final item_local_search_view_1:I = 0x7f0c00c0

.field public static final item_memo_choice_style_divider_1:I = 0x7f0c00c5

.field public static final item_memo_choice_style_relativelayout:I = 0x7f0c00c1

.field public static final item_memo_choice_style_textview_1:I = 0x7f0c00c2

.field public static final item_memo_choice_style_textview_2:I = 0x7f0c00c3

.field public static final item_memo_choice_style_textview_3:I = 0x7f0c00c4

.field public static final item_message_details_contact_framelayout_1:I = 0x7f0c00cc

.field public static final item_message_details_contact_imageview_1:I = 0x7f0c00cd

.field public static final item_message_details_contact_linearlayout_1:I = 0x7f0c00cb

.field public static final item_message_details_style_linearlayout_1:I = 0x7f0c00c6

.field public static final item_message_details_style_linearlayout_2:I = 0x7f0c00cf

.field public static final item_message_details_style_relativelayout:I = 0x7f0c00c7

.field public static final item_message_details_style_textview_1:I = 0x7f0c00c8

.field public static final item_message_details_style_textview_2:I = 0x7f0c00c9

.field public static final item_message_details_style_textview_3:I = 0x7f0c00ca

.field public static final item_message_details_style_textview_4:I = 0x7f0c00ce

.field public static final item_music_choice_linearlayout_1:I = 0x7f0c016d

.field public static final item_music_choice_linearlayout_1_preload:I = 0x7f0c0153

.field public static final item_music_choice_linearlayout_1_preload_bottom:I = 0x7f0c015d

.field public static final item_music_choice_view_1:I = 0x7f0c016c

.field public static final item_music_choice_view_1_preload:I = 0x7f0c0152

.field public static final item_music_choice_view_1_preload_bottom:I = 0x7f0c015c

.field public static final item_naver_local_search_dummyView:I = 0x7f0c00ef

.field public static final item_phone_type_choice_style:I = 0x7f0c00f9

.field public static final item_schedule_choice_date:I = 0x7f0c00ff

.field public static final item_schedule_choice_divider:I = 0x7f0c0101

.field public static final item_schedule_choice_event_list_check:I = 0x7f0c00fd

.field public static final item_schedule_choice_imageview_1:I = 0x7f0c0103

.field public static final item_schedule_choice_linear:I = 0x7f0c00fc

.field public static final item_schedule_choice_parent:I = 0x7f0c0102

.field public static final item_schedule_choice_relative:I = 0x7f0c00fb

.field public static final item_schedule_choice_textview_1:I = 0x7f0c0104

.field public static final item_schedule_choice_textview_2:I = 0x7f0c0105

.field public static final item_schedule_choice_textview_3:I = 0x7f0c0106

.field public static final item_schedule_choice_time:I = 0x7f0c0100

.field public static final item_schedule_choice_title:I = 0x7f0c00fe

.field public static final item_schedule_choice_view_1:I = 0x7f0c0107

.field public static final item_social_type_divider:I = 0x7f0c0117

.field public static final item_social_type_linear_1:I = 0x7f0c0114

.field public static final item_social_type_linear_2:I = 0x7f0c0115

.field public static final item_social_type_social_type:I = 0x7f0c0116

.field public static final item_you_can_say_smart_widget_command:I = 0x7f0c010f

.field public static final item_you_can_say_smart_widget_divider1:I = 0x7f0c0113

.field public static final item_you_can_say_smart_widget_examples:I = 0x7f0c0112

.field public static final item_you_can_say_smart_widget_examples_container:I = 0x7f0c0110

.field public static final item_you_can_say_smart_widget_examples_title:I = 0x7f0c0111

.field public static final item_you_can_say_smart_widget_normal_text_container:I = 0x7f0c010d

.field public static final item_you_can_say_smart_widget_normal_view_container:I = 0x7f0c0108

.field public static final item_you_can_say_smart_widget_normal_view_item:I = 0x7f0c0109

.field public static final item_you_can_say_smart_widget_thumnail:I = 0x7f0c010a

.field public static final item_you_can_say_smart_widget_thumnail1:I = 0x7f0c010b

.field public static final item_you_can_say_smart_widget_thumnail2:I = 0x7f0c010c

.field public static final item_you_can_say_smart_widget_title:I = 0x7f0c010e

.field public static final item_you_can_say_widget_command:I = 0x7f0c011f

.field public static final item_you_can_say_widget_divider1:I = 0x7f0c0123

.field public static final item_you_can_say_widget_examples:I = 0x7f0c0122

.field public static final item_you_can_say_widget_examples_container:I = 0x7f0c0120

.field public static final item_you_can_say_widget_examples_title:I = 0x7f0c0121

.field public static final item_you_can_say_widget_normal_text_container:I = 0x7f0c011d

.field public static final item_you_can_say_widget_normal_view_container:I = 0x7f0c0118

.field public static final item_you_can_say_widget_normal_view_item:I = 0x7f0c0119

.field public static final item_you_can_say_widget_thumnail:I = 0x7f0c011a

.field public static final item_you_can_say_widget_thumnail1:I = 0x7f0c011b

.field public static final item_you_can_say_widget_thumnail2:I = 0x7f0c011c

.field public static final item_you_can_say_widget_title:I = 0x7f0c011e

.field public static final iux_btn_finish_linearlayout_style:I = 0x7f0c0127

.field public static final iux_btn_next_imageview_style:I = 0x7f0c012a

.field public static final iux_btn_next_linearlayout_style:I = 0x7f0c0126

.field public static final iux_btn_prev_imageview_style:I = 0x7f0c0129

.field public static final iux_btn_prev_linearlayout_style:I = 0x7f0c0125

.field public static final iux_btn_skip_linearlayout_style:I = 0x7f0c0124

.field public static final iux_btn_textview_style:I = 0x7f0c0128

.field public static final iux_button_image:I = 0x7f0c0175

.field public static final iux_button_image_help:I = 0x7f0c0174

.field public static final layout1:I = 0x7f0c0073

.field public static final layout2:I = 0x7f0c0075

.field public static final left_container_style:I = 0x7f0c012c

.field public static final left_container_style_main:I = 0x7f0c012b

.field public static final lineardialog:I = 0x7f0c0033

.field public static final list_padding:I = 0x7f0c01a7

.field public static final localserach_text_info_style:I = 0x7f0c023b

.field public static final localserach_text_title_style:I = 0x7f0c023a

.field public static final mainControlLL:I = 0x7f0c0009

.field public static final mainControlLL_horizontal:I = 0x7f0c0013

.field public static final mainControlLL_horz:I = 0x7f0c000a

.field public static final mainclockwidget:I = 0x7f0c01ec

.field public static final mic_btn_layout_horz:I = 0x7f0c000e

.field public static final mic_btn_layout_vert:I = 0x7f0c000f

.field public static final mic_status_textview_style:I = 0x7f0c0018

.field public static final music_audioArtist:I = 0x7f0c016a

.field public static final music_audioArtist_preload:I = 0x7f0c014d

.field public static final music_audioArtist_preload_bottom:I = 0x7f0c015a

.field public static final music_audioDivider:I = 0x7f0c016b

.field public static final music_audioDivider_preload:I = 0x7f0c014f

.field public static final music_audioDivider_preload_bottom:I = 0x7f0c015b

.field public static final music_audioImage:I = 0x7f0c0167

.field public static final music_audioImage_preload:I = 0x7f0c0151

.field public static final music_audioImage_preload_bottom:I = 0x7f0c015e

.field public static final music_audioTemplateRLayout:I = 0x7f0c0168

.field public static final music_audioTemplateRLayout_preload:I = 0x7f0c014b

.field public static final music_audioTemplateRLayout_preload_bottom:I = 0x7f0c0157

.field public static final music_audioTitle:I = 0x7f0c0169

.field public static final music_audioTitle_preload:I = 0x7f0c014c

.field public static final music_audioTitle_preload_bottom:I = 0x7f0c0158

.field public static final music_audioVolumnImage_preload:I = 0x7f0c014e

.field public static final music_audioVolumnImage_preload_bottom:I = 0x7f0c0159

.field public static final music_choice_view_1:I = 0x7f0c016e

.field public static final music_choice_view_1_preload:I = 0x7f0c0154

.field public static final music_choice_view_1_preload_bottom:I = 0x7f0c0164

.field public static final music_close_preload_bottom:I = 0x7f0c0162

.field public static final music_ll:I = 0x7f0c0165

.field public static final music_ll_preload:I = 0x7f0c0150

.field public static final music_ll_preload_bottom:I = 0x7f0c0155

.field public static final music_next_preload_bottom:I = 0x7f0c0160

.field public static final music_play_preload_bottom:I = 0x7f0c0161

.field public static final music_previous_preload_bottom:I = 0x7f0c015f

.field public static final ok_button:I = 0x7f0c0393

.field public static final path_view_layout:I = 0x7f0c0176

.field public static final path_view_textView:I = 0x7f0c0177

.field public static final prompt_btn_layout:I = 0x7f0c0017

.field public static final right_container_style:I = 0x7f0c012d

.field public static final seamless_mic_btn_layout_horz:I = 0x7f0c0011

.field public static final seamless_mic_btn_layout_vert:I = 0x7f0c0012

.field public static final settings_style:I = 0x7f0c012e

.field public static final socialnetworkchoicewidget_linearlayout:I = 0x7f0c030b

.field public static final socialnetworkchoicewidget_main:I = 0x7f0c030a

.field public static final socialnetworkchoicewidget_service_list:I = 0x7f0c030c

.field public static final styleAccuWeather:I = 0x7f0c0354

.field public static final styleCitiDetail:I = 0x7f0c0351

.field public static final styleCmaWeather:I = 0x7f0c0355

.field public static final styleCustomSettingS_voice_mini_window_title:I = 0x7f0c0028

.field public static final styleCustomSettingTileBarLinearLayout1:I = 0x7f0c0026

.field public static final styleCustomSettingTileBarMyTitle:I = 0x7f0c0027

.field public static final styleCustomWakeUpEndBodyAdapt_button:I = 0x7f0c003d

.field public static final styleCustomWakeUpEndBodyDone_button:I = 0x7f0c003b

.field public static final styleCustomWakeUpEndBodyLinearAfterWakeUp:I = 0x7f0c003a

.field public static final styleCustomWakeUpEndBodyMainControl:I = 0x7f0c0038

.field public static final styleCustomWakeUpEndBodyWake_up_setting:I = 0x7f0c0039

.field public static final styleCustomWakeUpEndBodyline2:I = 0x7f0c003c

.field public static final styleDateView:I = 0x7f0c0353

.field public static final styleImageSingleDayWeather:I = 0x7f0c0356

.field public static final styleItemMovieChoice_FrameLayout_1:I = 0x7f0c00d1

.field public static final styleItemMovieChoice_ImageView_1:I = 0x7f0c00d2

.field public static final styleItemMovieChoice_ImageView_2:I = 0x7f0c00d3

.field public static final styleItemMovieChoice_ImageView_3:I = 0x7f0c00d7

.field public static final styleItemMovieChoice_LinearLayout_1:I = 0x7f0c00d0

.field public static final styleItemMovieChoice_LinearLayout_2:I = 0x7f0c00d5

.field public static final styleItemMovieChoice_LinearLayout_3:I = 0x7f0c00d6

.field public static final styleItemMovieChoice_LinearLayout_4:I = 0x7f0c00d9

.field public static final styleItemMovieChoice_LinearLayout_4_1:I = 0x7f0c00e3

.field public static final styleItemMovieChoice_LinearLayout_5:I = 0x7f0c00dc

.field public static final styleItemMovieChoice_LinearLayout_6:I = 0x7f0c00dd

.field public static final styleItemMovieChoice_RatingBar_1:I = 0x7f0c00de

.field public static final styleItemMovieChoice_TextView_1:I = 0x7f0c00d4

.field public static final styleItemMovieChoice_TextView_2:I = 0x7f0c00d8

.field public static final styleItemMovieChoice_TextView_3:I = 0x7f0c00da

.field public static final styleItemMovieChoice_TextView_4:I = 0x7f0c00db

.field public static final styleItemMovieChoice_TextView_5:I = 0x7f0c00df

.field public static final styleItemMovieChoice_TextView_6:I = 0x7f0c00e1

.field public static final styleItemMovieChoice_View_1:I = 0x7f0c00e0

.field public static final styleItemMovieChoice_View_2:I = 0x7f0c00e2

.field public static final styleItemPhoneChoiceDivider:I = 0x7f0c00f8

.field public static final styleItemPhoneChoiceMainControl:I = 0x7f0c00f5

.field public static final styleItemPhoneChoicePhone_number:I = 0x7f0c00f7

.field public static final styleItemPhoneChoicePhone_type:I = 0x7f0c00f6

.field public static final styleLinearLayout1:I = 0x7f0c0029

.field public static final styleMyTitle:I = 0x7f0c002a

.field public static final styleS_voice_mini_window_title:I = 0x7f0c002b

.field public static final styleSingleDayTemp:I = 0x7f0c0357

.field public static final styleWeatherIconLocation:I = 0x7f0c0352

.field public static final styleWidgetNaverLocalChoice_Button_1:I = 0x7f0c029a

.field public static final styleWidgetNaverLocalChoice_ImageView_1:I = 0x7f0c029b

.field public static final styleWidgetNaverLocalChoice_LinearLayout_1:I = 0x7f0c0292

.field public static final styleWidgetNaverLocalChoice_LinearLayout_2:I = 0x7f0c0293

.field public static final styleWidgetNaverLocalChoice_LinearLayout_3:I = 0x7f0c0294

.field public static final styleWidgetNaverLocalChoice_LinearLayout_4:I = 0x7f0c0299

.field public static final styleWidgetNaverLocalChoice_ListView_1:I = 0x7f0c0298

.field public static final styleWidgetNaverLocalChoice_TextView_1:I = 0x7f0c0295

.field public static final styleWidgetNaverLocalChoice_TextView_2:I = 0x7f0c0296

.field public static final styleWidgetNaverLocalChoice_View_1:I = 0x7f0c0297

.field public static final styleWidgetNaverLocalSearch_ImageButton_1:I = 0x7f0c00f2

.field public static final styleWidgetNaverLocalSearch_LinearLayout:I = 0x7f0c00e4

.field public static final styleWidgetNaverLocalSearch_LinearLayout_1:I = 0x7f0c00f4

.field public static final styleWidgetNaverLocalSearch_LinearLayout_2:I = 0x7f0c00e6

.field public static final styleWidgetNaverLocalSearch_LinearLayout_3:I = 0x7f0c00e7

.field public static final styleWidgetNaverLocalSearch_LinearLayout_4:I = 0x7f0c00ea

.field public static final styleWidgetNaverLocalSearch_LinearLayout_5:I = 0x7f0c00f1

.field public static final styleWidgetNaverLocalSearch_RatingBar_1:I = 0x7f0c00eb

.field public static final styleWidgetNaverLocalSearch_RelativeLayout:I = 0x7f0c00e5

.field public static final styleWidgetNaverLocalSearch_TextView_1:I = 0x7f0c00e8

.field public static final styleWidgetNaverLocalSearch_TextView_2:I = 0x7f0c00e9

.field public static final styleWidgetNaverLocalSearch_TextView_3:I = 0x7f0c00ec

.field public static final styleWidgetNaverLocalSearch_TextView_4:I = 0x7f0c00ee

.field public static final styleWidgetNaverLocalSearch_TextView_5:I = 0x7f0c00f0

.field public static final styleWidgetNaverLocalSearch_View_1:I = 0x7f0c00ed

.field public static final styleWidgetNaverLocalSearch_View_2:I = 0x7f0c00f3

.field public static final styleWidgetNaverLocal_Button_1:I = 0x7f0c02ae

.field public static final styleWidgetNaverLocal_FrameLayout_1:I = 0x7f0c02ac

.field public static final styleWidgetNaverLocal_ImageView_1:I = 0x7f0c02af

.field public static final styleWidgetNaverLocal_LinearLayout_1:I = 0x7f0c029c

.field public static final styleWidgetNaverLocal_LinearLayout_2:I = 0x7f0c029d

.field public static final styleWidgetNaverLocal_LinearLayout_3:I = 0x7f0c02a1

.field public static final styleWidgetNaverLocal_LinearLayout_4:I = 0x7f0c02a2

.field public static final styleWidgetNaverLocal_LinearLayout_5:I = 0x7f0c02a7

.field public static final styleWidgetNaverLocal_LinearLayout_6:I = 0x7f0c02ad

.field public static final styleWidgetNaverLocal_RatingBar_1:I = 0x7f0c02a3

.field public static final styleWidgetNaverLocal_TextView_1:I = 0x7f0c029e

.field public static final styleWidgetNaverLocal_TextView_2:I = 0x7f0c029f

.field public static final styleWidgetNaverLocal_TextView_3:I = 0x7f0c02a4

.field public static final styleWidgetNaverLocal_TextView_4:I = 0x7f0c02a6

.field public static final styleWidgetNaverLocal_TextView_5:I = 0x7f0c02a8

.field public static final styleWidgetNaverLocal_TextView_6:I = 0x7f0c02a9

.field public static final styleWidgetNaverLocal_TextView_7:I = 0x7f0c02aa

.field public static final styleWidgetNaverLocal_TextView_8:I = 0x7f0c02ab

.field public static final styleWidgetNaverLocal_View_1:I = 0x7f0c02a0

.field public static final styleWidgetNaverLocal_View_2:I = 0x7f0c02a5

.field public static final styleWidgetNaverRegion_Button_1:I = 0x7f0c02bf

.field public static final styleWidgetNaverRegion_FrameLayout_1:I = 0x7f0c02bb

.field public static final styleWidgetNaverRegion_ImageView_1:I = 0x7f0c02bc

.field public static final styleWidgetNaverRegion_ImageView_2:I = 0x7f0c02c0

.field public static final styleWidgetNaverRegion_LinearLayout_1:I = 0x7f0c02b1

.field public static final styleWidgetNaverRegion_LinearLayout_2:I = 0x7f0c02b2

.field public static final styleWidgetNaverRegion_LinearLayout_3:I = 0x7f0c02b5

.field public static final styleWidgetNaverRegion_LinearLayout_4:I = 0x7f0c02b6

.field public static final styleWidgetNaverRegion_LinearLayout_5:I = 0x7f0c02b9

.field public static final styleWidgetNaverRegion_LinearLayout_6:I = 0x7f0c02bd

.field public static final styleWidgetNaverRegion_LinearLayout_7:I = 0x7f0c02be

.field public static final styleWidgetNaverRegion_TextView_1:I = 0x7f0c02b3

.field public static final styleWidgetNaverRegion_TextView_2:I = 0x7f0c02b7

.field public static final styleWidgetNaverRegion_TextView_3:I = 0x7f0c02b8

.field public static final styleWidgetNaverRegion_TextView_4:I = 0x7f0c02ba

.field public static final styleWidgetNaverRegion_View_1:I = 0x7f0c02b4

.field public static final styleWigetComposeSocialBtn_cancel_social:I = 0x7f0c020e

.field public static final styleWigetComposeSocialBtn_container:I = 0x7f0c0213

.field public static final styleWigetComposeSocialButtonContainer:I = 0x7f0c020d

.field public static final styleWigetComposeSocialButtonLL:I = 0x7f0c020c

.field public static final styleWigetComposeSocialContact_name:I = 0x7f0c0206

.field public static final styleWigetComposeSocialFacebookIcon:I = 0x7f0c020a

.field public static final styleWigetComposeSocialIconLayout:I = 0x7f0c0209

.field public static final styleWigetComposeSocialLinear1:I = 0x7f0c0203

.field public static final styleWigetComposeSocialLinear2:I = 0x7f0c0210

.field public static final styleWigetComposeSocialMainLinear:I = 0x7f0c0202

.field public static final styleWigetComposeSocialMsg_body:I = 0x7f0c0208

.field public static final styleWigetComposeSocialPopup_divide_line:I = 0x7f0c0207

.field public static final styleWigetComposeSocialSenderImage:I = 0x7f0c0205

.field public static final styleWigetComposeSocialTwitterIcon:I = 0x7f0c020b

.field public static final styleWigetComposeSocialUpdateButton:I = 0x7f0c020f

.field public static final styleWigetComposeSocialVoice_talk_thumbnail_line:I = 0x7f0c0211

.field public static final styleWigetComposeSocialVoice_talk_thumbnail_line_01:I = 0x7f0c0204

.field public static final styleWigetEmail_Button_1:I = 0x7f0c0226

.field public static final styleWigetEmail_LinearLayout_1:I = 0x7f0c021c

.field public static final styleWigetEmail_LinearLayout_2:I = 0x7f0c021d

.field public static final styleWigetEmail_LinearLayout_3:I = 0x7f0c021e

.field public static final styleWigetEmail_LinearLayout_4:I = 0x7f0c021f

.field public static final styleWigetEmail_LinearLayout_5:I = 0x7f0c0225

.field public static final styleWigetEmail_TextView_1:I = 0x7f0c0220

.field public static final styleWigetEmail_TextView_2:I = 0x7f0c0221

.field public static final styleWigetEmail_TextView_3:I = 0x7f0c0222

.field public static final styleWigetEmail_TextView_4:I = 0x7f0c0223

.field public static final styleWigetEmail_View_1:I = 0x7f0c0224

.field public static final styleWigetEmail_parent:I = 0x7f0c021b

.field public static final styleWigetMovieChoice_Button_1:I = 0x7f0c026f

.field public static final styleWigetMovieChoice_ImageView_1:I = 0x7f0c0270

.field public static final styleWigetMovieChoice_LinearLayout_1:I = 0x7f0c0269

.field public static final styleWigetMovieChoice_LinearLayout_2:I = 0x7f0c026a

.field public static final styleWigetMovieChoice_LinearLayout_3:I = 0x7f0c026e

.field public static final styleWigetMovieChoice_ListView_1:I = 0x7f0c026d

.field public static final styleWigetMovieChoice_TextView_1:I = 0x7f0c026b

.field public static final styleWigetMovieChoice_View_1:I = 0x7f0c026c

.field public static final styleWigetMovie_Button_1:I = 0x7f0c028f

.field public static final styleWigetMovie_FrameLayout_1:I = 0x7f0c027b

.field public static final styleWigetMovie_ImageView_1:I = 0x7f0c027c

.field public static final styleWigetMovie_ImageView_2:I = 0x7f0c0290

.field public static final styleWigetMovie_LinearLayout_1:I = 0x7f0c0274

.field public static final styleWigetMovie_LinearLayout_10:I = 0x7f0c028e

.field public static final styleWigetMovie_LinearLayout_2:I = 0x7f0c0275

.field public static final styleWigetMovie_LinearLayout_3:I = 0x7f0c0278

.field public static final styleWigetMovie_LinearLayout_4:I = 0x7f0c027a

.field public static final styleWigetMovie_LinearLayout_5:I = 0x7f0c027d

.field public static final styleWigetMovie_LinearLayout_6:I = 0x7f0c0280

.field public static final styleWigetMovie_LinearLayout_7:I = 0x7f0c0285

.field public static final styleWigetMovie_LinearLayout_8:I = 0x7f0c028a

.field public static final styleWigetMovie_LinearLayout_9:I = 0x7f0c028d

.field public static final styleWigetMovie_RatingBar_1:I = 0x7f0c027e

.field public static final styleWigetMovie_TextView_1:I = 0x7f0c0276

.field public static final styleWigetMovie_TextView_10:I = 0x7f0c0289

.field public static final styleWigetMovie_TextView_11:I = 0x7f0c028b

.field public static final styleWigetMovie_TextView_12:I = 0x7f0c028c

.field public static final styleWigetMovie_TextView_2:I = 0x7f0c0277

.field public static final styleWigetMovie_TextView_3:I = 0x7f0c027f

.field public static final styleWigetMovie_TextView_4:I = 0x7f0c0281

.field public static final styleWigetMovie_TextView_5:I = 0x7f0c0283

.field public static final styleWigetMovie_TextView_6:I = 0x7f0c0284

.field public static final styleWigetMovie_TextView_7:I = 0x7f0c0286

.field public static final styleWigetMovie_TextView_8:I = 0x7f0c0287

.field public static final styleWigetMovie_TextView_9:I = 0x7f0c0288

.field public static final styleWigetMovie_View_1:I = 0x7f0c0279

.field public static final styleWigetMovie_View_2:I = 0x7f0c0282

.field public static final styleWigetScheduleEditMainLayout:I = 0x7f0c0305

.field public static final styleWigetScheduleEditSchedule_container:I = 0x7f0c0304

.field public static final styleWigetWeatherSingleCitiAccu_weather:I = 0x7f0c033d

.field public static final styleWigetWeatherSingleCitiAccu_weather_drive:I = 0x7f0c033e

.field public static final styleWigetWeatherSingleCitiCiti_details:I = 0x7f0c0336

.field public static final styleWigetWeatherSingleCitiCiti_details_drive:I = 0x7f0c0337

.field public static final styleWigetWeatherSingleCitiCiti_name:I = 0x7f0c0338

.field public static final styleWigetWeatherSingleCitiCiti_name_drive:I = 0x7f0c0339

.field public static final styleWigetWeatherSingleCitiClimate_name:I = 0x7f0c034c

.field public static final styleWigetWeatherSingleCitiClimate_name_drive:I = 0x7f0c034d

.field public static final styleWigetWeatherSingleCitiImageView_singledayweather:I = 0x7f0c033c

.field public static final styleWigetWeatherSingleCitiImageView_singledayweather_weekly:I = 0x7f0c0358

.field public static final styleWigetWeatherSingleCitiMin_temp:I = 0x7f0c034f

.field public static final styleWigetWeatherSingleCitiPresent_temp:I = 0x7f0c0341

.field public static final styleWigetWeatherSingleCitiPresent_temp_drive:I = 0x7f0c0342

.field public static final styleWigetWeatherSingleCitiShadow_style:I = 0x7f0c034e

.field public static final styleWigetWeatherSingleCitiTemprature:I = 0x7f0c033f

.field public static final styleWigetWeatherSingleCitiTemprature_drive:I = 0x7f0c0340

.field public static final styleWigetWeatherSingleCitiUnit_TempLayout:I = 0x7f0c0347

.field public static final styleWigetWeatherSingleCitiUnit_maxTemp:I = 0x7f0c0348

.field public static final styleWigetWeatherSingleCitiUnit_meter_C:I = 0x7f0c0345

.field public static final styleWigetWeatherSingleCitiUnit_meter_C_drive:I = 0x7f0c034b

.field public static final styleWigetWeatherSingleCitiUnit_meter_F:I = 0x7f0c0343

.field public static final styleWigetWeatherSingleCitiUnit_meter_F_drive:I = 0x7f0c0344

.field public static final styleWigetWeatherSingleCitiUnit_meter_round:I = 0x7f0c0346

.field public static final styleWigetWeatherSingleCitiUnit_minTemp:I = 0x7f0c034a

.field public static final styleWigetWeatherSingleCitiUnit_tempDevider:I = 0x7f0c0349

.field public static final styleWigetWeatherSingleCitiWidget_citi:I = 0x7f0c0334

.field public static final styleWigetWeatherSingleCitiWidget_citi_drive:I = 0x7f0c0335

.field public static final styleWigetWeatherSingleCitiWidgetwidget_citi_LinearLayout:I = 0x7f0c0333

.field public static final styleWigetWeatherSingleCitidate_view:I = 0x7f0c033a

.field public static final styleWigetWeatherSingleCitidate_view_drive:I = 0x7f0c033b

.field public static final style_custom_wakeup_setting_bubble_tv_1:I = 0x7f0c01a5

.field public static final style_custom_wakeup_setting_bubble_tv_2:I = 0x7f0c01a6

.field public static final style_help_anim_layoutland:I = 0x7f0c0147

.field public static final style_help_anim_layoutpotrt:I = 0x7f0c013f

.field public static final style_help_bubble_helpbutton_land:I = 0x7f0c0148

.field public static final style_help_bubble_textview_land:I = 0x7f0c0146

.field public static final style_help_done_bubble_layout_land:I = 0x7f0c013d

.field public static final style_help_done_tip_iv_land:I = 0x7f0c013e

.field public static final style_help_done_tip_iv_port:I = 0x7f0c0134

.field public static final style_help_edit_bubble_layout_land:I = 0x7f0c0137

.field public static final style_help_edit_bubble_tv_land:I = 0x7f0c0136

.field public static final style_help_edit_bubble_tv_port:I = 0x7f0c0131

.field public static final style_help_edit_tap_iv_land:I = 0x7f0c0135

.field public static final style_help_edit_tap_iv_port:I = 0x7f0c012f

.field public static final style_help_edit_tip_iv_land:I = 0x7f0c0138

.field public static final style_help_edit_tip_iv_port:I = 0x7f0c0130

.field public static final style_help_keypad_bubble_layout_land:I = 0x7f0c013a

.field public static final style_help_keypad_bubble_tv_land:I = 0x7f0c013c

.field public static final style_help_keypad_bubble_tv_port:I = 0x7f0c0133

.field public static final style_help_keypad_tap_iv_land:I = 0x7f0c0139

.field public static final style_help_keypad_tip_iv_land:I = 0x7f0c013b

.field public static final style_help_keypad_tip_iv_port:I = 0x7f0c0132

.field public static final style_main_layout:I = 0x7f0c0379

.field public static final style_micButtonPotraitLand:I = 0x7f0c0144

.field public static final style_textview_subheader:I = 0x7f0c01d6

.field public static final style_textview_welcome:I = 0x7f0c01d5

.field public static final style_tip_help_land:I = 0x7f0c0149

.field public static final style_tip_help_land_image:I = 0x7f0c0145

.field public static final style_tip_help_ptrt:I = 0x7f0c0141

.field public static final style_tip_mic_ptrt:I = 0x7f0c0140

.field public static final style_voice_setup_icon:I = 0x7f0c01d7

.field public static final style_widget_schedule:I = 0x7f0c0309

.field public static final stylesWidgetAlarmAlaramButtons:I = 0x7f0c01e3

.field public static final stylesWidgetAlarmAlarmHourPostfix:I = 0x7f0c01df

.field public static final stylesWidgetAlarmAlarmHourPrefix:I = 0x7f0c01de

.field public static final stylesWidgetAlarmAlarmMinutePostfix:I = 0x7f0c01e2

.field public static final stylesWidgetAlarmAlarmMinutePrefix:I = 0x7f0c01e1

.field public static final stylesWidgetAlarmAlarm_container:I = 0x7f0c01db

.field public static final stylesWidgetAlarmAlarmampm:I = 0x7f0c01dc

.field public static final stylesWidgetAlarmBtnCancelAlarm:I = 0x7f0c01e4

.field public static final stylesWidgetAlarmBtnSaveAlarm:I = 0x7f0c01e5

.field public static final stylesWidgetAlarmHourMinuteLL:I = 0x7f0c01dd

.field public static final stylesWidgetAlarmMainControlLL:I = 0x7f0c01da

.field public static final stylesWidgetAlarmView:I = 0x7f0c01e0

.field public static final stylesWidgetHelpHeader_examples:I = 0x7f0c0238

.field public static final stylesWidgetHelpLinear:I = 0x7f0c0237

.field public static final stylesWidgetHelpMainControlLL:I = 0x7f0c0232

.field public static final stylesWidgetHelpRelativeLayout01:I = 0x7f0c0233

.field public static final stylesWidgetHelpStartControlLL:I = 0x7f0c0231

.field public static final stylesWidgetHelpText_examples:I = 0x7f0c0239

.field public static final stylesWidgetHelpWcis_divider:I = 0x7f0c0236

.field public static final stylesWidgetHelpWycs_detail_image:I = 0x7f0c0234

.field public static final stylesWidgetHelpWycs_title:I = 0x7f0c0235

.field public static final stylesWidgetTaskBtn_cancel_task:I = 0x7f0c0318

.field public static final stylesWidgetTaskLinear:I = 0x7f0c031b

.field public static final stylesWidgetTaskMainControl:I = 0x7f0c030e

.field public static final stylesWidgetTaskReminder_bottom:I = 0x7f0c0316

.field public static final stylesWidgetTaskSomeTask:I = 0x7f0c0314

.field public static final stylesWidgetTaskTask_button:I = 0x7f0c0317

.field public static final stylesWidgetTaskTask_container:I = 0x7f0c0311

.field public static final stylesWidgetTaskTask_date:I = 0x7f0c0315

.field public static final stylesWidgetTaskTask_parent:I = 0x7f0c0310

.field public static final stylesWidgetTaskTask_reminder_date:I = 0x7f0c031d

.field public static final stylesWidgetTaskTask_reminder_date1:I = 0x7f0c031e

.field public static final stylesWidgetTaskTask_row_dueto:I = 0x7f0c0313

.field public static final stylesWidgetTaskTask_row_reminder:I = 0x7f0c031c

.field public static final stylesWidgetTaskTask_title:I = 0x7f0c0312

.field public static final stylesWidgetTask_confirm:I = 0x7f0c0319

.field public static final stylesWidgetTasklinearControl:I = 0x7f0c030f

.field public static final stylewidget_social_update:I = 0x7f0c0201

.field public static final svoice_title_welcomepage:I = 0x7f0c01d8

.field public static final text_version:I = 0x7f0c0066

.field public static final text_version_style:I = 0x7f0c0071

.field public static final text_version_style1:I = 0x7f0c007d

.field public static final timecontainer_style:I = 0x7f0c01f6

.field public static final timerwidget_colon:I = 0x7f0c0330

.field public static final timerwidget_head_text_hr:I = 0x7f0c0323

.field public static final timerwidget_head_text_min:I = 0x7f0c0324

.field public static final timerwidget_head_text_sec:I = 0x7f0c0325

.field public static final timerwidget_main:I = 0x7f0c031f

.field public static final timerwidget_time_frame_layout:I = 0x7f0c0322

.field public static final timerwidget_timer_body:I = 0x7f0c0320

.field public static final timerwidget_timer_head_text:I = 0x7f0c0321

.field public static final timerwidget_timer_hour_bg:I = 0x7f0c0327

.field public static final timerwidget_timer_hour_postfix:I = 0x7f0c032b

.field public static final timerwidget_timer_hour_prefix:I = 0x7f0c032a

.field public static final timerwidget_timer_minute_bg:I = 0x7f0c0328

.field public static final timerwidget_timer_minute_postfix:I = 0x7f0c032d

.field public static final timerwidget_timer_minute_prefix:I = 0x7f0c032c

.field public static final timerwidget_timer_number_bg:I = 0x7f0c0326

.field public static final timerwidget_timer_second_bg:I = 0x7f0c0329

.field public static final timerwidget_timer_second_postfix:I = 0x7f0c032f

.field public static final timerwidget_timer_second_prefix:I = 0x7f0c032e

.field public static final tos_dialog_fullContainer:I = 0x7f0c0193

.field public static final tos_dialog_linearlayout_1:I = 0x7f0c0194

.field public static final tos_dialog_textview_1:I = 0x7f0c0195

.field public static final tos_dialog_textview_2:I = 0x7f0c0196

.field public static final tutorHeader:I = 0x7f0c0081

.field public static final tutorSubHeader:I = 0x7f0c0082

.field public static final tutor_button_style:I = 0x7f0c008a

.field public static final tutor_confirm_button_style:I = 0x7f0c0092

.field public static final tutor_footor_style:I = 0x7f0c0087

.field public static final tutor_linear1:I = 0x7f0c0085

.field public static final tutor_linear3:I = 0x7f0c0086

.field public static final tutor_linear4:I = 0x7f0c0088

.field public static final tutor_list_style:I = 0x7f0c0083

.field public static final tutor_relative1_style:I = 0x7f0c0084

.field public static final tutor_second_content1_style:I = 0x7f0c008f

.field public static final tutor_second_content2_style:I = 0x7f0c0090

.field public static final tutor_second_content3_style:I = 0x7f0c0091

.field public static final tutor_second_header_style:I = 0x7f0c008c

.field public static final tutor_second_linear1_style:I = 0x7f0c008b

.field public static final tutor_second_linear2_style:I = 0x7f0c008e

.field public static final tutor_second_linear_style:I = 0x7f0c008d

.field public static final ui_focus_imageView:I = 0x7f0c019a

.field public static final ui_focus_mainlayout:I = 0x7f0c0199

.field public static final ui_focus_mic_image:I = 0x7f0c0197

.field public static final ui_focus_style:I = 0x7f0c0198

.field public static final uuid_text:I = 0x7f0c0067

.field public static final uuid_text_style:I = 0x7f0c0072

.field public static final wakeupList:I = 0x7f0c01ae

.field public static final wakeup_alertMsg_padng:I = 0x7f0c019d

.field public static final wakeup_setting_dialog_ll:I = 0x7f0c019c

.field public static final wakeup_twoline_list_item_help_text1:I = 0x7f0c01a2

.field public static final wakeup_twoline_list_item_image:I = 0x7f0c01a4

.field public static final wakeup_twoline_list_item_layout:I = 0x7f0c01a1

.field public static final wakeup_twoline_list_item_text1:I = 0x7f0c0171

.field public static final wakeup_twoline_list_item_text2:I = 0x7f0c01a3

.field public static final wakeupcommand_style:I = 0x7f0c01b1

.field public static final wcis_info_with_caption_row_container:I = 0x7f0c01b2

.field public static final wcis_info_with_caption_row_divider:I = 0x7f0c01b5

.field public static final wcis_info_with_caption_row_text1:I = 0x7f0c01b3

.field public static final wcis_info_with_caption_row_text2:I = 0x7f0c01b4

.field public static final wcis_lyt_row_container:I = 0x7f0c01b6

.field public static final wcis_lyt_row_divider:I = 0x7f0c01ba

.field public static final wcis_lyt_row_text1:I = 0x7f0c01b8

.field public static final wcis_lyt_row_text2:I = 0x7f0c01b9

.field public static final wcis_lyt_row_thumnail:I = 0x7f0c01b7

.field public static final wcis_subheading_maintext:I = 0x7f0c01bd

.field public static final wcis_subheading_maintext1:I = 0x7f0c01be

.field public static final wcis_subheading_row_text1:I = 0x7f0c01bb

.field public static final wcis_top_finish:I = 0x7f0c01c6

.field public static final wcis_top_previous:I = 0x7f0c01c4

.field public static final wcis_top_row_text1_style:I = 0x7f0c01c0

.field public static final wcis_top_row_text2_style:I = 0x7f0c01c1

.field public static final wcis_top_skip:I = 0x7f0c01c5

.field public static final wcis_wcis_subheading_row_rl:I = 0x7f0c01bc

.field public static final wcis_wcis_subheading_row_rl1:I = 0x7f0c01bf

.field public static final wcis_widget_expander_icon:I = 0x7f0c01d4

.field public static final wcis_widget_row_container:I = 0x7f0c01c8

.field public static final wcis_widget_row_divider:I = 0x7f0c01cd

.field public static final wcis_widget_row_divider2:I = 0x7f0c01ce

.field public static final wcis_widget_row_divider_help:I = 0x7f0c01cf

.field public static final wcis_widget_row_expand_view_container:I = 0x7f0c01d0

.field public static final wcis_widget_row_expand_view_text1:I = 0x7f0c01d1

.field public static final wcis_widget_row_expand_view_text2:I = 0x7f0c01d2

.field public static final wcis_widget_row_icon:I = 0x7f0c01c9

.field public static final wcis_widget_row_mainLL:I = 0x7f0c01c7

.field public static final wcis_widget_row_parent:I = 0x7f0c01d3

.field public static final wcis_widget_row_text1:I = 0x7f0c01cb

.field public static final wcis_widget_row_text2:I = 0x7f0c01cc

.field public static final wcis_widget_row_text_container:I = 0x7f0c01ca

.field public static final welcome_page_style:I = 0x7f0c006d

.field public static final widget_add_you_can_say_style:I = 0x7f0c022d

.field public static final widget_address_book_style:I = 0x7f0c01d9

.field public static final widget_alarm_choice_style:I = 0x7f0c00a2

.field public static final widget_answer_question_linear:I = 0x7f0c01e6

.field public static final widget_application_choice_style:I = 0x7f0c01e7

.field public static final widget_button_button:I = 0x7f0c01e8

.field public static final widget_button_button_container:I = 0x7f0c01e9

.field public static final widget_button_button_shadow:I = 0x7f0c01ea

.field public static final widget_button_text:I = 0x7f0c01eb

.field public static final widget_common_choice_shadow_styles:I = 0x7f0c01ff

.field public static final widget_common_choice_style:I = 0x7f0c01fb

.field public static final widget_common_gap:I = 0x7f0c02d0

.field public static final widget_common_kitkat_style:I = 0x7f0c01fc

.field public static final widget_common_shadow_styles:I = 0x7f0c01fd

.field public static final widget_common_shadow_styles_02:I = 0x7f0c01fe

.field public static final widget_common_shadow_styles_new:I = 0x7f0c0200

.field public static final widget_compose_social_status_body_container:I = 0x7f0c0212

.field public static final widget_contact_choice_style:I = 0x7f0c00b0

.field public static final widget_contact_detail_framelayout_1:I = 0x7f0c0217

.field public static final widget_contact_detail_imageview_1:I = 0x7f0c0218

.field public static final widget_contact_detail_linearlayout_1:I = 0x7f0c0216

.field public static final widget_contact_detail_listview_1:I = 0x7f0c021a

.field public static final widget_contact_detail_parent:I = 0x7f0c0214

.field public static final widget_contact_detail_relativelayout_1:I = 0x7f0c0215

.field public static final widget_contact_detail_textview_1:I = 0x7f0c0219

.field public static final widget_help_choice_fragment_list_style:I = 0x7f0c022f

.field public static final widget_help_choice_parent_style:I = 0x7f0c022e

.field public static final widget_help_choice_style:I = 0x7f0c0227

.field public static final widget_help_choice_style_border:I = 0x7f0c0229

.field public static final widget_help_choice_style_bottom:I = 0x7f0c0228

.field public static final widget_help_choice_wycs_top_list_style:I = 0x7f0c022c

.field public static final widget_help_row_style:I = 0x7f0c0230

.field public static final widget_map_style:I = 0x7f0c023c

.field public static final widget_memo_choice_linearlayout:I = 0x7f0c023e

.field public static final widget_memo_choice_listparent_style:I = 0x7f0c023f

.field public static final widget_memo_choice_parent:I = 0x7f0c023d

.field public static final widget_memo_style_linearlayout_1:I = 0x7f0c0241

.field public static final widget_memo_style_linearlayout_2:I = 0x7f0c0242

.field public static final widget_memo_style_textview_1:I = 0x7f0c0243

.field public static final widget_memo_tile_style:I = 0x7f0c0244

.field public static final widget_memo_widget_answer_question:I = 0x7f0c0240

.field public static final widget_message_readout:I = 0x7f0c0245

.field public static final widget_message_style_button_1:I = 0x7f0c024d

.field public static final widget_message_style_button_2:I = 0x7f0c024e

.field public static final widget_message_style_linearlayout_1:I = 0x7f0c0247

.field public static final widget_message_style_linearlayout_2:I = 0x7f0c0248

.field public static final widget_message_style_linearlayout_3:I = 0x7f0c024c

.field public static final widget_message_style_textview_1:I = 0x7f0c0249

.field public static final widget_message_style_textview_2:I = 0x7f0c024a

.field public static final widget_message_style_textview_3:I = 0x7f0c024b

.field public static final widget_message_style_textview_4:I = 0x7f0c024f

.field public static final widget_message_widget_answer_question:I = 0x7f0c0246

.field public static final widget_messagereadback_attach_icon:I = 0x7f0c025a

.field public static final widget_messagereadback_attach_icon_readback:I = 0x7f0c0267

.field public static final widget_messagereadback_style_button_1:I = 0x7f0c0258

.field public static final widget_messagereadback_style_button_2:I = 0x7f0c0259

.field public static final widget_messagereadback_style_linearlayout_1:I = 0x7f0c0252

.field public static final widget_messagereadback_style_linearlayout_2:I = 0x7f0c0253

.field public static final widget_messagereadback_style_linearlayout_3:I = 0x7f0c0257

.field public static final widget_messagereadback_style_relativelayout:I = 0x7f0c0251

.field public static final widget_messagereadback_style_textview_1:I = 0x7f0c0254

.field public static final widget_messagereadback_style_textview_2:I = 0x7f0c0255

.field public static final widget_messagereadback_style_textview_3:I = 0x7f0c0256

.field public static final widget_messagereadback_style_textview_4:I = 0x7f0c025b

.field public static final widget_messagereadback_widget_answer_question:I = 0x7f0c0250

.field public static final widget_messagereadbackbodyhidden_style_button_1:I = 0x7f0c0265

.field public static final widget_messagereadbackbodyhidden_style_button_2:I = 0x7f0c0266

.field public static final widget_messagereadbackbodyhidden_style_linearlayout_1:I = 0x7f0c025e

.field public static final widget_messagereadbackbodyhidden_style_linearlayout_2:I = 0x7f0c025f

.field public static final widget_messagereadbackbodyhidden_style_linearlayout_3:I = 0x7f0c0264

.field public static final widget_messagereadbackbodyhidden_style_message_divider:I = 0x7f0c0263

.field public static final widget_messagereadbackbodyhidden_style_relativelayout:I = 0x7f0c025d

.field public static final widget_messagereadbackbodyhidden_style_textview_1:I = 0x7f0c0260

.field public static final widget_messagereadbackbodyhidden_style_textview_2:I = 0x7f0c0261

.field public static final widget_messagereadbackbodyhidden_style_textview_3:I = 0x7f0c0262

.field public static final widget_messagereadbackbodyhidden_style_textview_4:I = 0x7f0c0268

.field public static final widget_messagereadbackbodyhidden_widget_answer_question:I = 0x7f0c025c

.field public static final widget_movie_styles_View:I = 0x7f0c0271

.field public static final widget_movie_styles_btn_more:I = 0x7f0c0272

.field public static final widget_movie_styles_image_view:I = 0x7f0c0273

.field public static final widget_music_choice_style:I = 0x7f0c0291

.field public static final widget_noraml_news_full_container:I = 0x7f0c02c1

.field public static final widget_normal_news_below_container:I = 0x7f0c02c5

.field public static final widget_normal_news_cp_logo:I = 0x7f0c02c7

.field public static final widget_normal_news_cp_who:I = 0x7f0c02c8

.field public static final widget_normal_news_details:I = 0x7f0c02c6

.field public static final widget_normal_news_divider:I = 0x7f0c02c4

.field public static final widget_normal_news_headline:I = 0x7f0c02c2

.field public static final widget_normal_news_updateDate:I = 0x7f0c02c3

.field public static final widget_phonetype_style:I = 0x7f0c00fa

.field public static final widget_schedule_body_attendee:I = 0x7f0c02fe

.field public static final widget_schedule_body_btn_cancel_schedule:I = 0x7f0c0300

.field public static final widget_schedule_body_button:I = 0x7f0c02ff

.field public static final widget_schedule_body_button2:I = 0x7f0c02ec

.field public static final widget_schedule_body_button_1:I = 0x7f0c02eb

.field public static final widget_schedule_body_confirm:I = 0x7f0c0301

.field public static final widget_schedule_body_linear:I = 0x7f0c02ef

.field public static final widget_schedule_body_linearlayout_1:I = 0x7f0c02d8

.field public static final widget_schedule_body_linearlayout_2:I = 0x7f0c02da

.field public static final widget_schedule_body_linearlayout_3:I = 0x7f0c02de

.field public static final widget_schedule_body_linearlayout_4:I = 0x7f0c02e2

.field public static final widget_schedule_body_linearlayout_5:I = 0x7f0c02e6

.field public static final widget_schedule_body_linearlayout_6:I = 0x7f0c02ea

.field public static final widget_schedule_body_location:I = 0x7f0c02fa

.field public static final widget_schedule_body_location_title:I = 0x7f0c02f9

.field public static final widget_schedule_body_parent:I = 0x7f0c02d7

.field public static final widget_schedule_body_participants_title:I = 0x7f0c02fd

.field public static final widget_schedule_body_row_location:I = 0x7f0c02f8

.field public static final widget_schedule_body_row_location_line:I = 0x7f0c02f7

.field public static final widget_schedule_body_row_participants:I = 0x7f0c02fc

.field public static final widget_schedule_body_row_participants_line:I = 0x7f0c02fb

.field public static final widget_schedule_body_row_title:I = 0x7f0c02f4

.field public static final widget_schedule_body_schedule_day:I = 0x7f0c02f1

.field public static final widget_schedule_body_schedule_month:I = 0x7f0c02f2

.field public static final widget_schedule_body_schedule_row_date:I = 0x7f0c02f0

.field public static final widget_schedule_body_schedule_week:I = 0x7f0c02f3

.field public static final widget_schedule_body_textview_1:I = 0x7f0c02d9

.field public static final widget_schedule_body_textview_2:I = 0x7f0c02db

.field public static final widget_schedule_body_textview_3:I = 0x7f0c02dc

.field public static final widget_schedule_body_textview_4:I = 0x7f0c02df

.field public static final widget_schedule_body_textview_5:I = 0x7f0c02e0

.field public static final widget_schedule_body_textview_6:I = 0x7f0c02e3

.field public static final widget_schedule_body_textview_7:I = 0x7f0c02e4

.field public static final widget_schedule_body_textview_8:I = 0x7f0c02e7

.field public static final widget_schedule_body_textview_9:I = 0x7f0c02e8

.field public static final widget_schedule_body_time:I = 0x7f0c02f6

.field public static final widget_schedule_body_title:I = 0x7f0c02f5

.field public static final widget_schedule_body_view_1:I = 0x7f0c02dd

.field public static final widget_schedule_body_view_2:I = 0x7f0c02e1

.field public static final widget_schedule_body_view_3:I = 0x7f0c02e5

.field public static final widget_schedule_body_view_4:I = 0x7f0c02e9

.field public static final widget_schedule_button_divider:I = 0x7f0c02ed

.field public static final widget_schedule_choice_style:I = 0x7f0c02ee

.field public static final widget_schedule_delete_ScheduleDeleteWidget:I = 0x7f0c0302

.field public static final widget_schedule_delete_schedule_container:I = 0x7f0c0303

.field public static final widget_schedule_parent:I = 0x7f0c0308

.field public static final widget_schedule_schedule_container:I = 0x7f0c0307

.field public static final widget_schedule_single:I = 0x7f0c0306

.field public static final widget_settings_language_relative_layout:I = 0x7f0c022a

.field public static final widget_settings_language_text_color:I = 0x7f0c022b

.field public static final widget_smart_date_style:I = 0x7f0c02d2

.field public static final widget_task_button_divider:I = 0x7f0c031a

.field public static final widget_task_parent:I = 0x7f0c030d

.field public static final widget_true_knowledge_logoImage:I = 0x7f0c0332

.field public static final widget_true_knowledge_true_knowledge_container:I = 0x7f0c0331

.field public static final widget_weather_box:I = 0x7f0c036c

.field public static final widget_weather_box_drive:I = 0x7f0c036d

.field public static final widget_weather_todays_plus_weekly_TempLayout:I = 0x7f0c0370

.field public static final widget_weather_todays_plus_weekly_accuweather_web:I = 0x7f0c036e

.field public static final widget_weather_todays_plus_weekly_child_accuweather:I = 0x7f0c0382

.field public static final widget_weather_todays_plus_weekly_child_date:I = 0x7f0c0366

.field public static final widget_weather_todays_plus_weekly_child_date_k:I = 0x7f0c0373

.field public static final widget_weather_todays_plus_weekly_child_image:I = 0x7f0c0367

.field public static final widget_weather_todays_plus_weekly_child_image_drive:I = 0x7f0c0368

.field public static final widget_weather_todays_plus_weekly_child_image_k:I = 0x7f0c0374

.field public static final widget_weather_todays_plus_weekly_child_layout1:I = 0x7f0c035b

.field public static final widget_weather_todays_plus_weekly_child_layout1_style:I = 0x7f0c036f

.field public static final widget_weather_todays_plus_weekly_child_layout6:I = 0x7f0c035c

.field public static final widget_weather_todays_plus_weekly_child_layout6_k:I = 0x7f0c0371

.field public static final widget_weather_todays_plus_weekly_child_weekday:I = 0x7f0c0364

.field public static final widget_weather_todays_plus_weekly_child_weekday_drive:I = 0x7f0c0365

.field public static final widget_weather_todays_plus_weekly_child_weekday_k:I = 0x7f0c0372

.field public static final widget_weather_todays_plus_weekly_citi_details:I = 0x7f0c035e

.field public static final widget_weather_todays_plus_weekly_citi_details_drive:I = 0x7f0c035f

.field public static final widget_weather_todays_plus_weekly_citi_details_linearLayout:I = 0x7f0c037d

.field public static final widget_weather_todays_plus_weekly_citi_name:I = 0x7f0c037e

.field public static final widget_weather_todays_plus_weekly_citi_name2:I = 0x7f0c037f

.field public static final widget_weather_todays_plus_weekly_climate_name:I = 0x7f0c037c

.field public static final widget_weather_todays_plus_weekly_date_view:I = 0x7f0c0380

.field public static final widget_weather_todays_plus_weekly_divider:I = 0x7f0c0381

.field public static final widget_weather_todays_plus_weekly_framelayout:I = 0x7f0c0363

.field public static final widget_weather_todays_plus_weekly_imageView_singledayweather:I = 0x7f0c0360

.field public static final widget_weather_todays_plus_weekly_line:I = 0x7f0c036b

.field public static final widget_weather_todays_plus_weekly_linearLayout:I = 0x7f0c035d

.field public static final widget_weather_todays_plus_weekly_main_layout:I = 0x7f0c0361

.field public static final widget_weather_todays_plus_weekly_main_layout_drive:I = 0x7f0c0362

.field public static final widget_weather_todays_plus_weekly_max_temp:I = 0x7f0c0369

.field public static final widget_weather_todays_plus_weekly_max_temp_k:I = 0x7f0c0375

.field public static final widget_weather_todays_plus_weekly_min_temp:I = 0x7f0c036a

.field public static final widget_weather_todays_plus_weekly_min_temp_k:I = 0x7f0c0377

.field public static final widget_weather_todays_plus_weekly_present_temp:I = 0x7f0c037a

.field public static final widget_weather_todays_plus_weekly_relativelayout:I = 0x7f0c035a

.field public static final widget_weather_todays_plus_weekly_relativelayout_drive:I = 0x7f0c0350

.field public static final widget_weather_todays_plus_weekly_temp_layout_k:I = 0x7f0c0376

.field public static final widget_weather_todays_plus_weekly_unit_meter_F_C:I = 0x7f0c037b

.field public static final widget_weather_todays_plus_weekly_widget_citi_LinearLayout:I = 0x7f0c0359

.field public static final widget_what_can_i_say_widget_parent:I = 0x7f0c0386

.field public static final widget_widget_you_can_say_line2:I = 0x7f0c038d

.field public static final widget_widget_you_can_say_title:I = 0x7f0c038c

.field public static final widget_widget_you_can_smart_say_line2:I = 0x7f0c02cf

.field public static final widget_widget_you_can_smart_say_title:I = 0x7f0c02ce

.field public static final widget_wolfram_alpha_logoImage:I = 0x7f0c0385

.field public static final widget_wolfram_alpha_wolfram_content_container:I = 0x7f0c0384

.field public static final widget_wolfram_alpha_wolfram_linearlayout_style:I = 0x7f0c0383

.field public static final widget_you_can_say_list_container:I = 0x7f0c0389

.field public static final widget_you_can_say_list_layout:I = 0x7f0c0387

.field public static final widget_you_can_say_list_line:I = 0x7f0c038b

.field public static final widget_you_can_say_list_main:I = 0x7f0c038a

.field public static final widget_you_can_say_list_main_container:I = 0x7f0c0388

.field public static final widget_you_can_smart_i_say_widget_parent:I = 0x7f0c02c9

.field public static final widget_you_can_smart_say_day_style:I = 0x7f0c02d5

.field public static final widget_you_can_smart_say_list_container:I = 0x7f0c02cb

.field public static final widget_you_can_smart_say_list_layout:I = 0x7f0c02ca

.field public static final widget_you_can_smart_say_list_main:I = 0x7f0c02cc

.field public static final widget_you_can_smart_say_list_main1:I = 0x7f0c02cd

.field public static final widget_you_can_smart_say_month_day:I = 0x7f0c02d3

.field public static final widget_you_can_smart_say_month_day_style:I = 0x7f0c02d6

.field public static final widget_you_can_smart_say_month_style:I = 0x7f0c02d4

.field public static final widget_you_can_smart_say_today:I = 0x7f0c02d1

.field public static final winset_popup:I = 0x7f0c038e

.field public static final winset_popup_image:I = 0x7f0c0392

.field public static final winset_popup_info:I = 0x7f0c0390

.field public static final winset_popup_linear1:I = 0x7f0c038f

.field public static final winset_popup_title:I = 0x7f0c0391

.field public static final write_btn_layout:I = 0x7f0c0015


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
