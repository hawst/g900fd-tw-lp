.class public final Lcom/vlingo/midas/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final about_bg:I = 0x7f020000

.field public static final address_text_color_press:I = 0x7f020001

.field public static final arrow_overlay_selector:I = 0x7f020002

.field public static final arrow_overlay_selector_disclaimer:I = 0x7f020003

.field public static final b_svoice_phone_alarm:I = 0x7f020004

.field public static final b_svoice_phone_bg:I = 0x7f020005

.field public static final b_svoice_phone_mic:I = 0x7f020006

.field public static final backrepeat:I = 0x7f020007

.field public static final bg_miniapp:I = 0x7f020008

.field public static final bg_tutorial:I = 0x7f020009

.field public static final bg_widget:I = 0x7f02000a

.field public static final bg_widget_wolfram:I = 0x7f02000b

.field public static final bluelight:I = 0x7f02000c

.field public static final btn_about_default_enabled:I = 0x7f02000d

.field public static final btn_browser:I = 0x7f02000e

.field public static final btn_browser_disabled:I = 0x7f02000f

.field public static final btn_call:I = 0x7f020010

.field public static final btn_call_disabled:I = 0x7f020011

.field public static final btn_help_close:I = 0x7f020012

.field public static final btn_iux_default_enabled:I = 0x7f020013

.field public static final btn_iux_default_round_next:I = 0x7f020014

.field public static final btn_iux_default_round_pre:I = 0x7f020015

.field public static final btn_iux_next_enabled:I = 0x7f020016

.field public static final btn_iux_prev_enabled:I = 0x7f020017

.field public static final btn_map:I = 0x7f020018

.field public static final btn_mini_expand:I = 0x7f020019

.field public static final btn_movie_status_bg:I = 0x7f02001a

.field public static final btn_navigate:I = 0x7f02001b

.field public static final btn_overlay_black_button:I = 0x7f02001c

.field public static final btn_overlay_button:I = 0x7f02001d

.field public static final btn_overlay_button_light:I = 0x7f02001e

.field public static final btn_overlay_buttonwidget:I = 0x7f02001f

.field public static final btn_overlay_listening:I = 0x7f020020

.field public static final btn_overlay_ready:I = 0x7f020021

.field public static final btn_overlay_thinking:I = 0x7f020022

.field public static final btn_reserve:I = 0x7f020023

.field public static final btn_voicetalk_popup:I = 0x7f020024

.field public static final btn_weather_button:I = 0x7f020025

.field public static final bubble_typing:I = 0x7f020026

.field public static final by_naver:I = 0x7f020027

.field public static final checkbox_bg:I = 0x7f020028

.field public static final circle:I = 0x7f020029

.field public static final contacts_default_caller_id:I = 0x7f02002a

.field public static final contacts_default_image_01:I = 0x7f02002b

.field public static final contacts_default_image_02:I = 0x7f02002c

.field public static final contacts_default_image_03:I = 0x7f02002d

.field public static final contacts_default_image_04:I = 0x7f02002e

.field public static final contacts_default_image_05:I = 0x7f02002f

.field public static final cursor:I = 0x7f020030

.field public static final cursor_blue:I = 0x7f020031

.field public static final disclaimer_overlay_button:I = 0x7f020032

.field public static final facebook_icon:I = 0x7f020033

.field public static final focused_bg2:I = 0x7f020034

.field public static final gallery_face_tagging_01:I = 0x7f020035

.field public static final gallery_face_tagging_02:I = 0x7f020036

.field public static final gallery_face_tagging_05:I = 0x7f020037

.field public static final gallery_face_tagging_06:I = 0x7f020038

.field public static final h_dim:I = 0x7f020039

.field public static final help_icon_drivingmode:I = 0x7f02003a

.field public static final help_icon_local_search:I = 0x7f02003b

.field public static final help_icon_local_search_cn:I = 0x7f02003c

.field public static final help_icon_local_search_cn_1:I = 0x7f02003d

.field public static final help_icon_movie:I = 0x7f02003e

.field public static final help_icon_weather_cn:I = 0x7f02003f

.field public static final help_list_color:I = 0x7f020040

.field public static final help_list_pressed_holo_light:I = 0x7f020041

.field public static final help_list_selector_middle:I = 0x7f020042

.field public static final help_navi:I = 0x7f020043

.field public static final help_news:I = 0x7f020044

.field public static final help_no_tail_btn_icon_focus:I = 0x7f020045

.field public static final help_no_tail_btn_icon_normal:I = 0x7f020046

.field public static final help_no_tail_btn_icon_press:I = 0x7f020047

.field public static final help_popup_picker_b_c:I = 0x7f020048

.field public static final help_popup_picker_bg_w_01:I = 0x7f020049

.field public static final help_popup_picker_bg_w_01_m:I = 0x7f02004a

.field public static final help_popup_picker_bg_w_02:I = 0x7f02004b

.field public static final help_popup_picker_l_c:I = 0x7f02004c

.field public static final help_popup_picker_r_c:I = 0x7f02004d

.field public static final help_popup_picker_t_c:I = 0x7f02004e

.field public static final help_qa:I = 0x7f02004f

.field public static final help_search:I = 0x7f020050

.field public static final help_social_hub:I = 0x7f020051

.field public static final help_tap:I = 0x7f020052

.field public static final help_weather:I = 0x7f020053

.field public static final ic_action_bar_menu:I = 0x7f020054

.field public static final ic_btn_round_more:I = 0x7f020055

.field public static final ic_btn_round_more_disabled:I = 0x7f020056

.field public static final ic_btn_round_more_normal:I = 0x7f020057

.field public static final ic_btn_round_more_normal_pressed:I = 0x7f020058

.field public static final ic_btn_round_src_more:I = 0x7f020059

.field public static final ic_menu_moreoverflow_normal_holo_dark:I = 0x7f02005a

.field public static final ic_menu_moreoverflow_pressed_holo_dark:I = 0x7f02005b

.field public static final ic_menu_selector:I = 0x7f02005c

.field public static final icon_memo:I = 0x7f02005d

.field public static final img_illust_always:I = 0x7f02005e

.field public static final intro_font_mark_left:I = 0x7f02005f

.field public static final item_background:I = 0x7f020060

.field public static final iux_btn_color:I = 0x7f020061

.field public static final iux_overlay_button:I = 0x7f020062

.field public static final language_drawable_image:I = 0x7f020063

.field public static final language_off_drawable_image:I = 0x7f020064

.field public static final language_on_drawable_image:I = 0x7f020065

.field public static final list_background:I = 0x7f020066

.field public static final list_btn:I = 0x7f020067

.field public static final list_selector_background_transition:I = 0x7f020068

.field public static final list_selector_bottom:I = 0x7f020069

.field public static final list_selector_middle:I = 0x7f02006a

.field public static final list_selector_top:I = 0x7f02006b

.field public static final logo_nhn:I = 0x7f02006c

.field public static final logo_true_knowledge:I = 0x7f02006d

.field public static final logo_wolfram_alpha:I = 0x7f02006e

.field public static final ls_button_bg:I = 0x7f02006f

.field public static final ls_button_bg_default:I = 0x7f020070

.field public static final ls_button_bg_pressed:I = 0x7f020071

.field public static final ls_button_bg_selected:I = 0x7f020072

.field public static final mainmenu_icon_applications:I = 0x7f020073

.field public static final mainmenu_icon_clock:I = 0x7f020074

.field public static final mainmenu_icon_contacts:I = 0x7f020075

.field public static final mainmenu_icon_message:I = 0x7f020076

.field public static final mainmenu_icon_music:I = 0x7f020077

.field public static final mainmenu_icon_phone:I = 0x7f020078

.field public static final mainmenu_icon_s_memo_pen:I = 0x7f020079

.field public static final mainmenu_icon_s_planner:I = 0x7f02007a

.field public static final mainmenu_icon_settings:I = 0x7f02007b

.field public static final mainmenu_icon_voice_recorder:I = 0x7f02007c

.field public static final memo_custom_bg:I = 0x7f02007d

.field public static final memo_list_selector_bottom:I = 0x7f02007e

.field public static final memo_list_selector_middle:I = 0x7f02007f

.field public static final memo_list_selector_top:I = 0x7f020080

.field public static final memo_tile_bg:I = 0x7f020081

.field public static final menu_setting_icon:I = 0x7f020082

.field public static final mic_icon:I = 0x7f020083

.field public static final mic_idle_anim:I = 0x7f020084

.field public static final movie_age_12_bg:I = 0x7f020085

.field public static final movie_age_15_bg:I = 0x7f020086

.field public static final movie_age_18_bg:I = 0x7f020087

.field public static final movie_age_all_bg:I = 0x7f020088

.field public static final movie_choice_text_color_press:I = 0x7f020089

.field public static final movie_num_bg:I = 0x7f02008a

.field public static final movie_rating_text_color_press:I = 0x7f02008b

.field public static final music_player_thumb_01:I = 0x7f02008c

.field public static final music_widget_selector_next_bottom:I = 0x7f02008d

.field public static final music_widget_selector_pause_bottom:I = 0x7f02008e

.field public static final music_widget_selector_play_bottom:I = 0x7f02008f

.field public static final music_widget_selector_previous_bottom:I = 0x7f020090

.field public static final n_memo_custom_bg:I = 0x7f020091

.field public static final n_weather_box_bg:I = 0x7f020092

.field public static final news_flipboard_logo:I = 0x7f020093

.field public static final news_united_logo:I = 0x7f020094

.field public static final news_united_logo_night:I = 0x7f020095

.field public static final next_text_selector:I = 0x7f020096

.field public static final noimage_alone:I = 0x7f020097

.field public static final non_touch_task_icon_call:I = 0x7f020098

.field public static final non_touch_task_icon_drivingmode:I = 0x7f020099

.field public static final non_touch_task_icon_goto:I = 0x7f02009a

.field public static final non_touch_task_icon_map:I = 0x7f02009b

.field public static final non_touch_task_icon_memo:I = 0x7f02009c

.field public static final non_touch_task_icon_message:I = 0x7f02009d

.field public static final non_touch_task_icon_music:I = 0x7f02009e

.field public static final non_touch_task_icon_navigate:I = 0x7f02009f

.field public static final non_touch_task_icon_open_app:I = 0x7f0200a0

.field public static final non_touch_task_icon_playradio:I = 0x7f0200a1

.field public static final non_touch_task_icon_schedule:I = 0x7f0200a2

.field public static final non_touch_task_icon_search:I = 0x7f0200a3

.field public static final non_touch_task_icon_social_update:I = 0x7f0200a4

.field public static final non_touch_task_icon_voicerecorder:I = 0x7f0200a5

.field public static final popup_divide_line:I = 0x7f0200a6

.field public static final popup_list_line:I = 0x7f0200a7

.field public static final pull_tab:I = 0x7f0200a8

.field public static final qzone_icon:I = 0x7f0200a9

.field public static final radio_button__bg:I = 0x7f0200aa

.field public static final ratingbar:I = 0x7f0200ab

.field public static final ready_animation0:I = 0x7f0200ac

.field public static final ready_animation0_h:I = 0x7f0200ad

.field public static final review_count_text_color_press:I = 0x7f0200ae

.field public static final right_arrow:I = 0x7f0200af

.field public static final ripple:I = 0x7f0200b0

.field public static final ripple1:I = 0x7f0200b1

.field public static final ripple_drawable:I = 0x7f0200b2

.field public static final s_voice_eq_bg:I = 0x7f0200b3

.field public static final s_voice_eq_bg_01:I = 0x7f0200b4

.field public static final s_voice_eq_bg_02:I = 0x7f0200b5

.field public static final s_voice_eq_bg_03:I = 0x7f0200b6

.field public static final s_voice_eq_bg_04:I = 0x7f0200b7

.field public static final s_voice_eq_mic:I = 0x7f0200b8

.field public static final s_voice_list_line_01:I = 0x7f0200b9

.field public static final s_voice_mic:I = 0x7f0200ba

.field public static final s_voice_mini_window_bg:I = 0x7f0200bb

.field public static final s_voice_mini_window_title_bg:I = 0x7f0200bc

.field public static final s_voice_mini_window_title_div:I = 0x7f0200bd

.field public static final s_voice_process_01:I = 0x7f0200be

.field public static final s_voice_process_02:I = 0x7f0200bf

.field public static final s_voice_process_bar:I = 0x7f0200c0

.field public static final s_voice_process_bg:I = 0x7f0200c1

.field public static final s_voice_process_bg_01:I = 0x7f0200c2

.field public static final s_voice_process_bg_02:I = 0x7f0200c3

.field public static final s_voice_process_talkback_bg:I = 0x7f0200c4

.field public static final s_voice_process_tlakback_bg_01:I = 0x7f0200c5

.field public static final s_voice_que_bg:I = 0x7f0200c6

.field public static final s_voice_que_bg01:I = 0x7f0200c7

.field public static final s_voice_que_bg02:I = 0x7f0200c8

.field public static final s_voice_que_bg03:I = 0x7f0200c9

.field public static final s_voice_que_bg04:I = 0x7f0200ca

.field public static final s_voice_que_talkback_bg:I = 0x7f0200cb

.field public static final s_voice_setting_bottom_bg:I = 0x7f0200cc

.field public static final s_voice_standby_bg:I = 0x7f0200cd

.field public static final s_voice_standby_talkback_bg:I = 0x7f0200ce

.field public static final seamless_reco_notification:I = 0x7f0200cf

.field public static final sep_background_gradient:I = 0x7f0200d0

.field public static final setting_panel_divider:I = 0x7f0200d1

.field public static final settingheader_background:I = 0x7f0200d2

.field public static final shape:I = 0x7f0200d3

.field public static final smallratingbar:I = 0x7f0200d4

.field public static final splitter_ondown:I = 0x7f0200d5

.field public static final star_4_34x32:I = 0x7f0200d6

.field public static final star_4_34x32_gray:I = 0x7f0200d7

.field public static final stat_notify_car_mode:I = 0x7f0200d8

.field public static final sub_text_press_color:I = 0x7f0200d9

.field public static final sview_wallpaper_002:I = 0x7f0200da

.field public static final svoice_contacts_default_img_focused:I = 0x7f0200db

.field public static final svoice_contacts_default_img_selected:I = 0x7f0200dc

.field public static final svoice_ic_search:I = 0x7f0200dd

.field public static final svoice_setup_btn_arrow_disabled:I = 0x7f0200de

.field public static final svoice_setup_btn_arrow_focused:I = 0x7f0200df

.field public static final svoice_setup_btn_arrow_focused_pressed:I = 0x7f0200e0

.field public static final svoice_setup_btn_arrow_normal:I = 0x7f0200e1

.field public static final svoice_setup_btn_arrow_pressed:I = 0x7f0200e2

.field public static final svoice_weather_ic_temp:I = 0x7f0200e3

.field public static final svoice_weather_ic_temp_c:I = 0x7f0200e4

.field public static final svoice_weather_ic_temp_f:I = 0x7f0200e5

.field public static final text_color_press:I = 0x7f0200e6

.field public static final timer_number_00:I = 0x7f0200e7

.field public static final timer_number_01:I = 0x7f0200e8

.field public static final timer_number_02:I = 0x7f0200e9

.field public static final timer_number_03:I = 0x7f0200ea

.field public static final timer_number_04:I = 0x7f0200eb

.field public static final timer_number_05:I = 0x7f0200ec

.field public static final timer_number_06:I = 0x7f0200ed

.field public static final timer_number_07:I = 0x7f0200ee

.field public static final timer_number_08:I = 0x7f0200ef

.field public static final timer_number_08_bg:I = 0x7f0200f0

.field public static final timer_number_09:I = 0x7f0200f1

.field public static final timer_number_bg:I = 0x7f0200f2

.field public static final timer_number_dot:I = 0x7f0200f3

.field public static final timer_number_dot_press:I = 0x7f0200f4

.field public static final timer_number_press_00:I = 0x7f0200f5

.field public static final timer_number_press_01:I = 0x7f0200f6

.field public static final timer_number_press_02:I = 0x7f0200f7

.field public static final timer_number_press_03:I = 0x7f0200f8

.field public static final timer_number_press_04:I = 0x7f0200f9

.field public static final timer_number_press_05:I = 0x7f0200fa

.field public static final timer_number_press_06:I = 0x7f0200fb

.field public static final timer_number_press_07:I = 0x7f0200fc

.field public static final timer_number_press_08:I = 0x7f0200fd

.field public static final timer_number_press_09:I = 0x7f0200fe

.field public static final timer_number_press_dot:I = 0x7f0200ff

.field public static final title_btn_close:I = 0x7f020100

.field public static final title_btn_close_press:I = 0x7f020101

.field public static final title_btn_close_selected:I = 0x7f020102

.field public static final title_btn_setting:I = 0x7f020103

.field public static final title_btn_setting_press:I = 0x7f020104

.field public static final title_btn_setting_selected:I = 0x7f020105

.field public static final tmemo:I = 0x7f020106

.field public static final transparent:I = 0x7f020107

.field public static final transparent_light:I = 0x7f020108

.field public static final true_knowledge_logo:I = 0x7f020109

.field public static final tts:I = 0x7f02010a

.field public static final tutorial_btn_next_drawables:I = 0x7f02010b

.field public static final tutorial_list_selector:I = 0x7f02010c

.field public static final tutorial_voice_talk_screen_custom_popup_shadow:I = 0x7f02010d

.field public static final tw_ab_transparent_dark_holo:I = 0x7f02010e

.field public static final tw_ab_transparent_light:I = 0x7f02010f

.field public static final tw_action_bar_icon_help_disabled_holo_light:I = 0x7f020110

.field public static final tw_action_bar_icon_help_holo_light:I = 0x7f020111

.field public static final tw_action_bar_icon_setting_disabled_holo_dark:I = 0x7f020112

.field public static final tw_action_bar_icon_setting_disabled_holo_light:I = 0x7f020113

.field public static final tw_action_bar_icon_setting_holo_dark:I = 0x7f020114

.field public static final tw_action_bar_icon_setting_holo_light:I = 0x7f020115

.field public static final tw_action_item_background_focused_holo_dark:I = 0x7f020116

.field public static final tw_action_item_background_pressed_holo_dark:I = 0x7f020117

.field public static final tw_action_item_background_selected_holo_dark:I = 0x7f020118

.field public static final tw_background_dark:I = 0x7f020119

.field public static final tw_background_holo_light:I = 0x7f02011a

.field public static final tw_btn_check_off_focused_holo_dark:I = 0x7f02011b

.field public static final tw_btn_check_off_focused_holo_light:I = 0x7f02011c

.field public static final tw_btn_check_off_holo_dark:I = 0x7f02011d

.field public static final tw_btn_check_off_holo_light:I = 0x7f02011e

.field public static final tw_btn_check_off_pressed_holo_dark:I = 0x7f02011f

.field public static final tw_btn_check_off_pressed_holo_light:I = 0x7f020120

.field public static final tw_btn_check_on_focused_holo_dark:I = 0x7f020121

.field public static final tw_btn_check_on_focused_holo_light:I = 0x7f020122

.field public static final tw_btn_check_on_holo_dark:I = 0x7f020123

.field public static final tw_btn_check_on_holo_light:I = 0x7f020124

.field public static final tw_btn_check_on_pressed_holo_dark:I = 0x7f020125

.field public static final tw_btn_check_on_pressed_holo_light:I = 0x7f020126

.field public static final tw_btn_circle_more_disabled_focused_holo_light:I = 0x7f020127

.field public static final tw_btn_circle_more_disabled_holo_light:I = 0x7f020128

.field public static final tw_btn_circle_more_focused_holo_light:I = 0x7f020129

.field public static final tw_btn_circle_more_holo_light:I = 0x7f02012a

.field public static final tw_btn_circle_more_pressed_holo_light:I = 0x7f02012b

.field public static final tw_btn_default_default_holo_dark:I = 0x7f02012c

.field public static final tw_btn_default_default_holo_light:I = 0x7f02012d

.field public static final tw_btn_default_disabled_focused_holo_dark:I = 0x7f02012e

.field public static final tw_btn_default_disabled_focused_holo_light:I = 0x7f02012f

.field public static final tw_btn_default_disabled_holo_dark:I = 0x7f020130

.field public static final tw_btn_default_disabled_holo_light:I = 0x7f020131

.field public static final tw_btn_default_focused_holo_dark:I = 0x7f020132

.field public static final tw_btn_default_focused_holo_light:I = 0x7f020133

.field public static final tw_btn_default_normal_holo_dark:I = 0x7f020134

.field public static final tw_btn_default_normal_holo_light:I = 0x7f020135

.field public static final tw_btn_default_pressed_holo_dark:I = 0x7f020136

.field public static final tw_btn_default_pressed_holo_light:I = 0x7f020137

.field public static final tw_btn_default_selected_holo_dark:I = 0x7f020138

.field public static final tw_btn_default_selected_holo_light:I = 0x7f020139

.field public static final tw_btn_icon_next_holo_dark:I = 0x7f02013a

.field public static final tw_btn_icon_next_holo_light:I = 0x7f02013b

.field public static final tw_btn_icon_previous_holo_dark:I = 0x7f02013c

.field public static final tw_btn_icon_previous_holo_light:I = 0x7f02013d

.field public static final tw_btn_next_1btn_default_holo_dark:I = 0x7f02013e

.field public static final tw_btn_next_default_holo_dark:I = 0x7f02013f

.field public static final tw_btn_next_depth_holo_dark:I = 0x7f020140

.field public static final tw_btn_next_depth_pressed_holo_dark:I = 0x7f020141

.field public static final tw_btn_next_disabled_focused_holo_dark:I = 0x7f020142

.field public static final tw_btn_next_disabled_focused_holo_light:I = 0x7f020143

.field public static final tw_btn_next_disabled_holo_dark:I = 0x7f020144

.field public static final tw_btn_next_disabled_holo_light:I = 0x7f020145

.field public static final tw_btn_next_focused_holo_dark:I = 0x7f020146

.field public static final tw_btn_next_focused_holo_light:I = 0x7f020147

.field public static final tw_btn_next_normal_holo_dark:I = 0x7f020148

.field public static final tw_btn_next_normal_holo_light:I = 0x7f020149

.field public static final tw_btn_next_pressed_holo_dark:I = 0x7f02014a

.field public static final tw_btn_next_pressed_holo_light:I = 0x7f02014b

.field public static final tw_btn_next_selected_holo_dark:I = 0x7f02014c

.field public static final tw_btn_next_selected_holo_light:I = 0x7f02014d

.field public static final tw_btn_previous_default_holo_dark:I = 0x7f02014e

.field public static final tw_btn_previous_disabled_focused_holo_dark:I = 0x7f02014f

.field public static final tw_btn_previous_disabled_focused_holo_light:I = 0x7f020150

.field public static final tw_btn_previous_disabled_holo_dark:I = 0x7f020151

.field public static final tw_btn_previous_disabled_holo_light:I = 0x7f020152

.field public static final tw_btn_previous_focused_holo_dark:I = 0x7f020153

.field public static final tw_btn_previous_focused_holo_light:I = 0x7f020154

.field public static final tw_btn_previous_normal_holo_dark:I = 0x7f020155

.field public static final tw_btn_previous_normal_holo_light:I = 0x7f020156

.field public static final tw_btn_previous_pressed_holo_dark:I = 0x7f020157

.field public static final tw_btn_previous_pressed_holo_light:I = 0x7f020158

.field public static final tw_btn_previous_selected_holo_dark:I = 0x7f020159

.field public static final tw_btn_previous_selected_holo_light:I = 0x7f02015a

.field public static final tw_btn_radio_off_disabled_focused_holo_dark:I = 0x7f02015b

.field public static final tw_btn_radio_off_disabled_holo_dark:I = 0x7f02015c

.field public static final tw_btn_radio_off_focused_holo_dark:I = 0x7f02015d

.field public static final tw_btn_radio_off_focused_holo_light:I = 0x7f02015e

.field public static final tw_btn_radio_off_holo_dark:I = 0x7f02015f

.field public static final tw_btn_radio_off_holo_light:I = 0x7f020160

.field public static final tw_btn_radio_off_pressed_holo_dark:I = 0x7f020161

.field public static final tw_btn_radio_off_pressed_holo_light:I = 0x7f020162

.field public static final tw_btn_radio_on_disabled_focused_holo_dark:I = 0x7f020163

.field public static final tw_btn_radio_on_disabled_holo_dark:I = 0x7f020164

.field public static final tw_btn_radio_on_focused_holo_dark:I = 0x7f020165

.field public static final tw_btn_radio_on_focused_holo_light:I = 0x7f020166

.field public static final tw_btn_radio_on_holo_dark:I = 0x7f020167

.field public static final tw_btn_radio_on_holo_light:I = 0x7f020168

.field public static final tw_btn_radio_on_pressed_holo_dark:I = 0x7f020169

.field public static final tw_btn_radio_on_pressed_holo_light:I = 0x7f02016a

.field public static final tw_btn_round_more_disabled_dark:I = 0x7f02016b

.field public static final tw_btn_round_more_disabled_focused_holo_light:I = 0x7f02016c

.field public static final tw_btn_round_more_disabled_holo_light:I = 0x7f02016d

.field public static final tw_btn_round_more_disabled_light:I = 0x7f02016e

.field public static final tw_btn_round_more_focused_holo_light:I = 0x7f02016f

.field public static final tw_btn_round_more_holo_light:I = 0x7f020170

.field public static final tw_btn_round_more_normal_dark:I = 0x7f020171

.field public static final tw_btn_round_more_normal_light:I = 0x7f020172

.field public static final tw_btn_round_more_pressed_holo_light:I = 0x7f020173

.field public static final tw_buttonbarbutton_selector_default_holo_dark:I = 0x7f020174

.field public static final tw_buttonbarbutton_selector_disabled_holo_light:I = 0x7f020175

.field public static final tw_category_list_arrow_holo_dark:I = 0x7f020176

.field public static final tw_caterory_list_pressed_holo_dark:I = 0x7f020177

.field public static final tw_checkbox_off_enabled_default_holo_light:I = 0x7f020178

.field public static final tw_checkbox_off_enabled_disabled_focused_holo_light:I = 0x7f020179

.field public static final tw_checkbox_off_enabled_disabled_holo_light:I = 0x7f02017a

.field public static final tw_checkbox_off_enabled_focused_holo_light:I = 0x7f02017b

.field public static final tw_checkbox_off_enabled_pressed_holo_light:I = 0x7f02017c

.field public static final tw_dialog_full_holo_dark:I = 0x7f02017d

.field public static final tw_dialog_full_holo_light:I = 0x7f02017e

.field public static final tw_dialog_middle_holo_dark:I = 0x7f02017f

.field public static final tw_divider_popup_horizontal_holo_light:I = 0x7f020180

.field public static final tw_expandable_list_bg_holo_light:I = 0x7f020181

.field public static final tw_expandable_list_left_focused_holo_light:I = 0x7f020182

.field public static final tw_expandable_list_left_holo_light:I = 0x7f020183

.field public static final tw_expandable_list_left_pressed_holo_light:I = 0x7f020184

.field public static final tw_expander_close_default_holo_light:I = 0x7f020185

.field public static final tw_expander_close_holo_light:I = 0x7f020186

.field public static final tw_expander_open_default_holo_light:I = 0x7f020187

.field public static final tw_expander_open_holo_light:I = 0x7f020188

.field public static final tw_fullscreen_background_left_light:I = 0x7f020189

.field public static final tw_fullscreen_background_right_light:I = 0x7f02018a

.field public static final tw_header_button_icon_cancel_holo_dark:I = 0x7f02018b

.field public static final tw_ic_ab_back_holo_light:I = 0x7f02018c

.field public static final tw_list_disabled_focused_holo_dark:I = 0x7f02018d

.field public static final tw_list_disabled_holo_dark:I = 0x7f02018e

.field public static final tw_list_divider_holo_dark:I = 0x7f02018f

.field public static final tw_list_divider_holo_light:I = 0x7f020190

.field public static final tw_list_focused_holo:I = 0x7f020191

.field public static final tw_list_focused_holo_dark:I = 0x7f020192

.field public static final tw_list_longpressed_holo:I = 0x7f020193

.field public static final tw_list_pressed_holo:I = 0x7f020194

.field public static final tw_list_pressed_holo_dark:I = 0x7f020195

.field public static final tw_list_pressed_holo_light:I = 0x7f020196

.field public static final tw_list_section_divider:I = 0x7f020197

.field public static final tw_list_section_divider_focused_holo_dark:I = 0x7f020198

.field public static final tw_list_section_divider_focused_holo_light:I = 0x7f020199

.field public static final tw_list_section_divider_holo_dark:I = 0x7f02019a

.field public static final tw_list_section_divider_holo_light:I = 0x7f02019b

.field public static final tw_list_section_divider_pressed_holo_dark:I = 0x7f02019c

.field public static final tw_list_section_divider_pressed_holo_light:I = 0x7f02019d

.field public static final tw_list_section_divider_selected_holo_dark:I = 0x7f02019e

.field public static final tw_list_section_divider_selected_holo_light:I = 0x7f02019f

.field public static final tw_list_selected_holo:I = 0x7f0201a0

.field public static final tw_list_selected_holo_dark:I = 0x7f0201a1

.field public static final tw_list_selector_disabled_holo:I = 0x7f0201a2

.field public static final tw_list_selector_disabled_holo_dark:I = 0x7f0201a3

.field public static final tw_preference_contents_bg_holo_light:I = 0x7f0201a4

.field public static final tw_preference_contents_list_divider_holo_dark:I = 0x7f0201a5

.field public static final tw_preference_list_divider_holo_light:I = 0x7f0201a6

.field public static final tw_scrollbar_holo_dark:I = 0x7f0201a7

.field public static final tw_scrollbar_holo_light:I = 0x7f0201a8

.field public static final tw_searchfiled_background_holo_dark:I = 0x7f0201a9

.field public static final tw_textfield_multiline_default_holo_light:I = 0x7f0201aa

.field public static final tw_textfield_multiline_default_holo_light_bg:I = 0x7f0201ab

.field public static final tw_toast_frame_holo_dark:I = 0x7f0201ac

.field public static final tw_widget_progressbar_holo_light:I = 0x7f0201ad

.field public static final twitter_icon:I = 0x7f0201ae

.field public static final vlingo_logo_color:I = 0x7f0201af

.field public static final voice_help_font_qmarksleft:I = 0x7f0201b0

.field public static final voice_help_font_qmarksleft_jp:I = 0x7f0201b1

.field public static final voice_help_font_qmarksright:I = 0x7f0201b2

.field public static final voice_help_font_qmarksright_jp:I = 0x7f0201b3

.field public static final voice_music_controller_ff:I = 0x7f0201b4

.field public static final voice_music_controller_ff_press:I = 0x7f0201b5

.field public static final voice_music_controller_play:I = 0x7f0201b6

.field public static final voice_music_controller_play_press:I = 0x7f0201b7

.field public static final voice_music_controller_rew:I = 0x7f0201b8

.field public static final voice_music_controller_rew_press:I = 0x7f0201b9

.field public static final voice_music_controller_stop:I = 0x7f0201ba

.field public static final voice_music_controller_stop_press:I = 0x7f0201bb

.field public static final voice_music_stop:I = 0x7f0201bc

.field public static final voice_music_thumbnail_01:I = 0x7f0201bd

.field public static final voice_music_thumbnail_02:I = 0x7f0201be

.field public static final voice_music_thumbnail_03:I = 0x7f0201bf

.field public static final voice_music_thumbnail_04:I = 0x7f0201c0

.field public static final voice_music_thumbnail_05:I = 0x7f0201c1

.field public static final voice_music_thumbnail_06:I = 0x7f0201c2

.field public static final voice_music_thumbnail_07:I = 0x7f0201c3

.field public static final voice_music_thumbnail_08:I = 0x7f0201c4

.field public static final voice_music_volume_01:I = 0x7f0201c5

.field public static final voice_music_volume_02:I = 0x7f0201c6

.field public static final voice_music_volume_03:I = 0x7f0201c7

.field public static final voice_music_widget_bg:I = 0x7f0201c8

.field public static final voice_setup_btn_arrow_disabled:I = 0x7f0201c9

.field public static final voice_setup_btn_arrow_focused:I = 0x7f0201ca

.field public static final voice_setup_btn_arrow_focused_pressed:I = 0x7f0201cb

.field public static final voice_setup_btn_arrow_normal:I = 0x7f0201cc

.field public static final voice_setup_btn_arrow_pressed:I = 0x7f0201cd

.field public static final voice_setup_icon:I = 0x7f0201ce

.field public static final voice_setup_samsung_logo:I = 0x7f0201cf

.field public static final voice_talk_accuweather_01:I = 0x7f0201d0

.field public static final voice_talk_action_bar_bg:I = 0x7f0201d1

.field public static final voice_talk_alarm_ic_l_in:I = 0x7f0201d2

.field public static final voice_talk_alarm_ic_off:I = 0x7f0201d3

.field public static final voice_talk_alarm_ic_on:I = 0x7f0201d4

.field public static final voice_talk_alarm_icon_repeat:I = 0x7f0201d5

.field public static final voice_talk_alarm_icon_snooze_light:I = 0x7f0201d6

.field public static final voice_talk_alarm_icon_snooze_off:I = 0x7f0201d7

.field public static final voice_talk_alarm_icon_snooze_on:I = 0x7f0201d8

.field public static final voice_talk_alarm_line:I = 0x7f0201d9

.field public static final voice_talk_bg_tile:I = 0x7f0201da

.field public static final voice_talk_btn:I = 0x7f0201db

.field public static final voice_talk_btn_call:I = 0x7f0201dc

.field public static final voice_talk_btn_next:I = 0x7f0201dd

.field public static final voice_talk_btn_next_1btn:I = 0x7f0201de

.field public static final voice_talk_btn_next_dim:I = 0x7f0201df

.field public static final voice_talk_btn_next_focused:I = 0x7f0201e0

.field public static final voice_talk_btn_next_pressed:I = 0x7f0201e1

.field public static final voice_talk_btn_next_selected:I = 0x7f0201e2

.field public static final voice_talk_btn_previous:I = 0x7f0201e3

.field public static final voice_talk_btn_previous_dim:I = 0x7f0201e4

.field public static final voice_talk_btn_previous_focused:I = 0x7f0201e5

.field public static final voice_talk_btn_previous_pressed:I = 0x7f0201e6

.field public static final voice_talk_btn_previous_selected:I = 0x7f0201e7

.field public static final voice_talk_button_disabled_focused:I = 0x7f0201e8

.field public static final voice_talk_button_disabled_focused_light:I = 0x7f0201e9

.field public static final voice_talk_button_disabled_light:I = 0x7f0201ea

.field public static final voice_talk_button_disible:I = 0x7f0201eb

.field public static final voice_talk_button_divider:I = 0x7f0201ec

.field public static final voice_talk_button_focus:I = 0x7f0201ed

.field public static final voice_talk_button_focused:I = 0x7f0201ee

.field public static final voice_talk_button_focused_light:I = 0x7f0201ef

.field public static final voice_talk_button_menu:I = 0x7f0201f0

.field public static final voice_talk_button_nolmal:I = 0x7f0201f1

.field public static final voice_talk_button_normal:I = 0x7f0201f2

.field public static final voice_talk_button_press:I = 0x7f0201f3

.field public static final voice_talk_button_press_light:I = 0x7f0201f4

.field public static final voice_talk_button_search:I = 0x7f0201f5

.field public static final voice_talk_button_search_press:I = 0x7f0201f6

.field public static final voice_talk_button_select:I = 0x7f0201f7

.field public static final voice_talk_button_selected:I = 0x7f0201f8

.field public static final voice_talk_button_selected_light:I = 0x7f0201f9

.field public static final voice_talk_clock_divide_line:I = 0x7f0201fa

.field public static final voice_talk_clock_dot:I = 0x7f0201fb

.field public static final voice_talk_clock_line:I = 0x7f0201fc

.field public static final voice_talk_clock_num_0:I = 0x7f0201fd

.field public static final voice_talk_clock_num_1:I = 0x7f0201fe

.field public static final voice_talk_clock_num_2:I = 0x7f0201ff

.field public static final voice_talk_clock_num_3:I = 0x7f020200

.field public static final voice_talk_clock_num_4:I = 0x7f020201

.field public static final voice_talk_clock_num_5:I = 0x7f020202

.field public static final voice_talk_clock_num_6:I = 0x7f020203

.field public static final voice_talk_clock_num_7:I = 0x7f020204

.field public static final voice_talk_clock_num_8:I = 0x7f020205

.field public static final voice_talk_clock_num_9:I = 0x7f020206

.field public static final voice_talk_contact_line:I = 0x7f020207

.field public static final voice_talk_detail_expander_bg:I = 0x7f020208

.field public static final voice_talk_detail_expander_bg_01:I = 0x7f020209

.field public static final voice_talk_detail_expander_bg_02:I = 0x7f02020a

.field public static final voice_talk_detail_expander_list_bg:I = 0x7f02020b

.field public static final voice_talk_detail_focused:I = 0x7f02020c

.field public static final voice_talk_detail_press:I = 0x7f02020d

.field public static final voice_talk_detail_select:I = 0x7f02020e

.field public static final voice_talk_event_list_check:I = 0x7f02020f

.field public static final voice_talk_help_bg:I = 0x7f020210

.field public static final voice_talk_help_line:I = 0x7f020211

.field public static final voice_talk_icon_search:I = 0x7f020212

.field public static final voice_talk_inner_scroll_shadow:I = 0x7f020213

.field public static final voice_talk_list_focused:I = 0x7f020214

.field public static final voice_talk_list_focused_01:I = 0x7f020215

.field public static final voice_talk_list_focused_02:I = 0x7f020216

.field public static final voice_talk_list_line:I = 0x7f020217

.field public static final voice_talk_list_press:I = 0x7f020218

.field public static final voice_talk_list_press_01:I = 0x7f020219

.field public static final voice_talk_list_press_02:I = 0x7f02021a

.field public static final voice_talk_list_select:I = 0x7f02021b

.field public static final voice_talk_list_select_01:I = 0x7f02021c

.field public static final voice_talk_list_select_02:I = 0x7f02021d

.field public static final voice_talk_map_bg:I = 0x7f02021e

.field public static final voice_talk_memo_custom_bg:I = 0x7f02021f

.field public static final voice_talk_message_01:I = 0x7f020220

.field public static final voice_talk_message_02:I = 0x7f020221

.field public static final voice_talk_message_counter:I = 0x7f020222

.field public static final voice_talk_message_icon_attach:I = 0x7f020223

.field public static final voice_talk_message_typed:I = 0x7f020224

.field public static final voice_talk_message_typing:I = 0x7f020225

.field public static final voice_talk_more_bg:I = 0x7f020226

.field public static final voice_talk_nuance:I = 0x7f020227

.field public static final voice_talk_popup_bg:I = 0x7f020228

.field public static final voice_talk_popup_button:I = 0x7f020229

.field public static final voice_talk_popup_button_press:I = 0x7f02022a

.field public static final voice_talk_screen_custom_popup:I = 0x7f02022b

.field public static final voice_talk_screen_custom_popup_01:I = 0x7f02022c

.field public static final voice_talk_screen_custom_popup_02:I = 0x7f02022d

.field public static final voice_talk_screen_custom_popup_03:I = 0x7f02022e

.field public static final voice_talk_screen_custom_popup_press_01:I = 0x7f02022f

.field public static final voice_talk_screen_custom_popup_press_02:I = 0x7f020230

.field public static final voice_talk_screen_custom_popup_press_03:I = 0x7f020231

.field public static final voice_talk_screen_custom_popup_shadow:I = 0x7f020232

.field public static final voice_talk_screen_custom_popup_weather:I = 0x7f020233

.field public static final voice_talk_screen_popup:I = 0x7f020234

.field public static final voice_talk_search_popup_01:I = 0x7f020235

.field public static final voice_talk_search_popup_02:I = 0x7f020236

.field public static final voice_talk_search_popup_03:I = 0x7f020237

.field public static final voice_talk_sensory:I = 0x7f020238

.field public static final voice_talk_setting:I = 0x7f020239

.field public static final voice_talk_setting_inches:I = 0x7f02023a

.field public static final voice_talk_setting_inches_02:I = 0x7f02023b

.field public static final voice_talk_setting_off:I = 0x7f02023c

.field public static final voice_talk_setting_on:I = 0x7f02023d

.field public static final voice_talk_social_update_facebook:I = 0x7f02023e

.field public static final voice_talk_social_update_qzone:I = 0x7f02023f

.field public static final voice_talk_social_update_twitter:I = 0x7f020240

.field public static final voice_talk_social_update_weibo:I = 0x7f020241

.field public static final voice_talk_star_off:I = 0x7f020242

.field public static final voice_talk_star_on:I = 0x7f020243

.field public static final voice_talk_subtitle_bg:I = 0x7f020244

.field public static final voice_talk_thumbnail_bg:I = 0x7f020245

.field public static final voice_talk_thumbnail_line:I = 0x7f020246

.field public static final voice_talk_thumbnail_line_01:I = 0x7f020247

.field public static final voice_talk_tutorial_btn_next:I = 0x7f020248

.field public static final voice_talk_tutorial_btn_next_press:I = 0x7f020249

.field public static final voice_talk_tutorial_list_focus:I = 0x7f02024a

.field public static final voice_talk_tutorial_list_press:I = 0x7f02024b

.field public static final voice_talk_tutorial_mic_01:I = 0x7f02024c

.field public static final voice_talk_tutorial_mic_02:I = 0x7f02024d

.field public static final voice_talk_tutorial_tutorial_box:I = 0x7f02024e

.field public static final voice_talk_wake_up_command_1progress_0:I = 0x7f02024f

.field public static final voice_talk_wake_up_command_1progress_1:I = 0x7f020250

.field public static final voice_talk_wake_up_command_2progress_0:I = 0x7f020251

.field public static final voice_talk_wake_up_command_2progress_1:I = 0x7f020252

.field public static final voice_talk_wake_up_command_2progress_2:I = 0x7f020253

.field public static final voice_talk_wake_up_command_3progress_0:I = 0x7f020254

.field public static final voice_talk_wake_up_command_3progress_1:I = 0x7f020255

.field public static final voice_talk_wake_up_command_3progress_2:I = 0x7f020256

.field public static final voice_talk_wake_up_command_3progress_3:I = 0x7f020257

.field public static final voice_talk_wake_up_command_4progress_0:I = 0x7f020258

.field public static final voice_talk_wake_up_command_4progress_1:I = 0x7f020259

.field public static final voice_talk_wake_up_command_4progress_2:I = 0x7f02025a

.field public static final voice_talk_wake_up_command_4progress_3:I = 0x7f02025b

.field public static final voice_talk_wake_up_command_4progress_4:I = 0x7f02025c

.field public static final voice_talk_wake_up_command_bar:I = 0x7f02025d

.field public static final voice_talk_wake_up_command_fail:I = 0x7f02025e

.field public static final voice_talk_wake_up_command_fair:I = 0x7f02025f

.field public static final voice_talk_wake_up_command_good:I = 0x7f020260

.field public static final voice_talk_wake_up_command_poor:I = 0x7f020261

.field public static final voice_talk_wake_up_command_success:I = 0x7f020262

.field public static final voice_talk_wake_up_command_verygood:I = 0x7f020263

.field public static final voice_talk_weather_bg:I = 0x7f020264

.field public static final voice_talk_weathernews:I = 0x7f020265

.field public static final voice_talk_wolfram_alpha:I = 0x7f020266

.field public static final voice_tutorial_quot_01:I = 0x7f020267

.field public static final voice_tutorial_quot_02:I = 0x7f020268

.field public static final voice_welcome_bg_bottom:I = 0x7f020269

.field public static final voicetalk_icon_star_on:I = 0x7f02026a

.field public static final voicetalk_list_icon_star_off:I = 0x7f02026b

.field public static final voicetalk_list_icon_star_on:I = 0x7f02026c

.field public static final voicetalk_message_counter:I = 0x7f02026d

.field public static final wake_up_command:I = 0x7f02026e

.field public static final wake_up_command_list_selector:I = 0x7f02026f

.field public static final wcis_row_bottom_background:I = 0x7f020270

.field public static final wcis_row_middle_background:I = 0x7f020271

.field public static final wcis_row_top_background:I = 0x7f020272

.field public static final wcis_text_color:I = 0x7f020273

.field public static final weather_01:I = 0x7f020274

.field public static final weather_01_n:I = 0x7f020275

.field public static final weather_01_v:I = 0x7f020276

.field public static final weather_01_v_n:I = 0x7f020277

.field public static final weather_02:I = 0x7f020278

.field public static final weather_02_n:I = 0x7f020279

.field public static final weather_02_v:I = 0x7f02027a

.field public static final weather_02_v_n:I = 0x7f02027b

.field public static final weather_03:I = 0x7f02027c

.field public static final weather_03_n:I = 0x7f02027d

.field public static final weather_03_v:I = 0x7f02027e

.field public static final weather_03_v_n:I = 0x7f02027f

.field public static final weather_04:I = 0x7f020280

.field public static final weather_04_n:I = 0x7f020281

.field public static final weather_04_v:I = 0x7f020282

.field public static final weather_04_v_n:I = 0x7f020283

.field public static final weather_05:I = 0x7f020284

.field public static final weather_05_n:I = 0x7f020285

.field public static final weather_05_v:I = 0x7f020286

.field public static final weather_05_v_n:I = 0x7f020287

.field public static final weather_06:I = 0x7f020288

.field public static final weather_06_n:I = 0x7f020289

.field public static final weather_06_v:I = 0x7f02028a

.field public static final weather_06_v_n:I = 0x7f02028b

.field public static final weather_07:I = 0x7f02028c

.field public static final weather_07_n:I = 0x7f02028d

.field public static final weather_07_v:I = 0x7f02028e

.field public static final weather_07_v_n:I = 0x7f02028f

.field public static final weather_08:I = 0x7f020290

.field public static final weather_08_v:I = 0x7f020291

.field public static final weather_09:I = 0x7f020292

.field public static final weather_09_v:I = 0x7f020293

.field public static final weather_bg:I = 0x7f020294

.field public static final weather_bg_1:I = 0x7f020295

.field public static final weather_box_bg:I = 0x7f020296

.field public static final weather_cma_icon:I = 0x7f020297

.field public static final weather_divide_line:I = 0x7f020298

.field public static final weather_icon_b_01:I = 0x7f020299

.field public static final weather_icon_b_02:I = 0x7f02029a

.field public static final weather_icon_b_03:I = 0x7f02029b

.field public static final weather_icon_b_04:I = 0x7f02029c

.field public static final weather_icon_b_05:I = 0x7f02029d

.field public static final weather_icon_b_06:I = 0x7f02029e

.field public static final weather_icon_b_07:I = 0x7f02029f

.field public static final weather_icon_b_08:I = 0x7f0202a0

.field public static final weather_icon_b_09:I = 0x7f0202a1

.field public static final weather_icon_b_10:I = 0x7f0202a2

.field public static final weather_icon_b_11:I = 0x7f0202a3

.field public static final weather_icon_b_12:I = 0x7f0202a4

.field public static final weather_icon_b_13:I = 0x7f0202a5

.field public static final weather_icon_b_14:I = 0x7f0202a6

.field public static final weather_icon_b_15:I = 0x7f0202a7

.field public static final weather_icon_b_16:I = 0x7f0202a8

.field public static final weather_icon_b_17:I = 0x7f0202a9

.field public static final weather_icon_b_18:I = 0x7f0202aa

.field public static final weather_icon_b_19:I = 0x7f0202ab

.field public static final weather_icon_b_20:I = 0x7f0202ac

.field public static final weather_icon_b_21:I = 0x7f0202ad

.field public static final weather_icon_b_22:I = 0x7f0202ae

.field public static final weather_icon_location:I = 0x7f0202af

.field public static final weather_icon_s_01:I = 0x7f0202b0

.field public static final weather_icon_s_02:I = 0x7f0202b1

.field public static final weather_icon_s_03:I = 0x7f0202b2

.field public static final weather_icon_s_04:I = 0x7f0202b3

.field public static final weather_icon_s_05:I = 0x7f0202b4

.field public static final weather_icon_s_06:I = 0x7f0202b5

.field public static final weather_icon_s_07:I = 0x7f0202b6

.field public static final weather_icon_s_08:I = 0x7f0202b7

.field public static final weather_icon_s_09:I = 0x7f0202b8

.field public static final weather_icon_s_10:I = 0x7f0202b9

.field public static final weather_icon_s_11:I = 0x7f0202ba

.field public static final weather_icon_s_12:I = 0x7f0202bb

.field public static final weather_icon_s_13:I = 0x7f0202bc

.field public static final weather_icon_s_14:I = 0x7f0202bd

.field public static final weather_icon_s_15:I = 0x7f0202be

.field public static final weather_icon_s_16:I = 0x7f0202bf

.field public static final weather_icon_s_17:I = 0x7f0202c0

.field public static final weather_icon_s_18:I = 0x7f0202c1

.field public static final weather_icon_s_19:I = 0x7f0202c2

.field public static final weather_icon_s_20:I = 0x7f0202c3

.field public static final weather_icon_s_21:I = 0x7f0202c4

.field public static final weather_icon_s_22:I = 0x7f0202c5

.field public static final weather_temper_line:I = 0x7f0202c6

.field public static final weather_widget_sample:I = 0x7f0202c7

.field public static final weather_widget_sample_edge:I = 0x7f0202c8

.field public static final weather_widget_sample_fahrenheit:I = 0x7f0202c9

.field public static final weather_widget_sample_fahrenheit_edge:I = 0x7f0202ca

.field public static final weather_widget_sample_fahrenheit_port:I = 0x7f0202cb

.field public static final weather_widget_sample_port:I = 0x7f0202cc

.field public static final websearch_ripple:I = 0x7f0202cd

.field public static final weibo_icon:I = 0x7f0202ce

.field public static final welcome_page_img:I = 0x7f0202cf

.field public static final welcome_page_img_v:I = 0x7f0202d0

.field public static final widget_button_ripple:I = 0x7f0202d1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
