.class public Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;
.super Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;
.source "MicListeningAnimationViewTablet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet$MyTimerTask;
    }
.end annotation


# instance fields
.field private mCount:I

.field private mRmsHandler:Landroid/os/Handler;

.field private myTimerTask:Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet$MyTimerTask;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->mCount:I

    .line 222
    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet$1;-><init>(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->mRmsHandler:Landroid/os/Handler;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;

    .prologue
    .line 20
    iget v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->mCount:I

    return v0
.end method

.method static synthetic access$008(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;

    .prologue
    .line 20
    iget v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->mCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->mCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->mRmsHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public cancelTimer()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 248
    :cond_0
    return-void
.end method

.method public micListeningAnimation(I)V
    .locals 13
    .param p1, "rmsNext"    # I

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const v3, 0x3ccccccd    # 0.025f

    const/4 v5, 0x1

    const v1, 0x3f4ccccd    # 0.8f

    .line 57
    iget v2, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->mRmsBefore:I

    int-to-float v2, v2

    mul-float v11, v2, v3

    .line 58
    .local v11, "scaleBefore":F
    int-to-float v2, p1

    mul-float v12, v2, v3

    .line 59
    .local v12, "scaleNext":F
    iput p1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->mRmsBefore:I

    .line 60
    mul-float/2addr v11, v11

    .line 61
    mul-float/2addr v12, v12

    .line 66
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v2, v1

    move v3, v1

    move v4, v1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 68
    .local v0, "ani":Landroid/view/animation/ScaleAnimation;
    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 69
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setRepeatCount(I)V

    .line 70
    invoke-virtual {v0, v5}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 71
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 72
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->micImages:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/midas/gui/customviews/MicListeningView;

    .line 73
    .local v10, "mImages":Lcom/vlingo/midas/gui/customviews/MicListeningView;
    invoke-virtual {v10, v0}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 75
    .end local v10    # "mImages":Lcom/vlingo/midas/gui/customviews/MicListeningView;
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->onFinishInflate()V

    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->micImages:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->micImages:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->micImages:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->micImages:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer4:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->micImages:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer5:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method

.method public scaleAnimation(F)V
    .locals 6
    .param p1, "ratio"    # F

    .prologue
    .line 215
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->mCount:I

    .line 216
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->timer:Ljava/util/Timer;

    .line 217
    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet$MyTimerTask;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet$MyTimerTask;-><init>(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->myTimerTask:Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet$MyTimerTask;

    .line 219
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->myTimerTask:Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet$MyTimerTask;

    const-wide/16 v2, 0x190

    const-wide/16 v4, 0x6e

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 220
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 130
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 131
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer4:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 133
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer5:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 134
    return-void
.end method

.method public setMicAnimation()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 47
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 50
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 51
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 52
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 54
    return-void
.end method

.method public showMicListeningAnimation(I)V
    .locals 8
    .param p1, "rmsValue"    # I

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->cancelAllAnimations()V

    .line 79
    const/16 v4, 0x32

    if-ge p1, v4, :cond_1

    .line 80
    move v3, p1

    .line 81
    .local v3, "temp":I
    int-to-float v4, v3

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float/2addr v4, v5

    float-to-int v3, v4

    .line 82
    int-to-double v4, v3

    const-wide v6, 0x3f947ae147ae147bL    # 0.02

    mul-double/2addr v4, v6

    const-wide v6, 0x3fe7ae147ae147aeL    # 0.74

    add-double/2addr v4, v6

    double-to-float v2, v4

    .line 83
    .local v2, "scaleRatio":F
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 84
    .local v0, "deger":D
    float-to-double v4, v2

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    div-double/2addr v4, v0

    double-to-float v2, v4

    .line 85
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fa00000    # 1.25f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 125
    .end local v0    # "deger":D
    .end local v2    # "scaleRatio":F
    .end local v3    # "temp":I
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    const/16 v4, 0x32

    if-lt p1, v4, :cond_2

    const/16 v4, 0x3a

    if-ge p1, v4, :cond_2

    .line 87
    move v3, p1

    .line 88
    .restart local v3    # "temp":I
    int-to-float v4, v3

    const/high16 v5, 0x3fe00000    # 1.75f

    mul-float/2addr v4, v5

    float-to-int v3, v4

    .line 89
    int-to-double v4, v3

    const-wide v6, 0x3f947ae147ae147bL    # 0.02

    mul-double/2addr v4, v6

    const-wide v6, 0x3fe7ae147ae147aeL    # 0.74

    add-double/2addr v4, v6

    double-to-float v2, v4

    .line 90
    .restart local v2    # "scaleRatio":F
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 91
    .restart local v0    # "deger":D
    float-to-double v4, v2

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    div-double/2addr v4, v0

    double-to-float v2, v4

    .line 92
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fa00000    # 1.25f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 93
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fc00000    # 1.5f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    goto :goto_0

    .line 94
    .end local v0    # "deger":D
    .end local v2    # "scaleRatio":F
    .end local v3    # "temp":I
    :cond_2
    const/16 v4, 0x3a

    if-lt p1, v4, :cond_3

    const/16 v4, 0x41

    if-ge p1, v4, :cond_3

    .line 95
    move v3, p1

    .line 96
    .restart local v3    # "temp":I
    int-to-float v4, v3

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    float-to-int v3, v4

    .line 97
    int-to-double v4, v3

    const-wide v6, 0x3f947ae147ae147bL    # 0.02

    mul-double/2addr v4, v6

    const-wide v6, 0x3fe7ae147ae147aeL    # 0.74

    add-double/2addr v4, v6

    double-to-float v2, v4

    .line 98
    .restart local v2    # "scaleRatio":F
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 99
    .restart local v0    # "deger":D
    float-to-double v4, v2

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    div-double/2addr v4, v0

    double-to-float v2, v4

    .line 100
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fa00000    # 1.25f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 101
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fc00000    # 1.5f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 102
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fe00000    # 1.75f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    goto :goto_0

    .line 103
    .end local v0    # "deger":D
    .end local v2    # "scaleRatio":F
    .end local v3    # "temp":I
    :cond_3
    const/16 v4, 0x41

    if-lt p1, v4, :cond_4

    const/16 v4, 0x4a

    if-ge p1, v4, :cond_4

    .line 104
    move v3, p1

    .line 105
    .restart local v3    # "temp":I
    int-to-float v4, v3

    const/high16 v5, 0x40100000    # 2.25f

    mul-float/2addr v4, v5

    float-to-int v3, v4

    .line 106
    int-to-double v4, v3

    const-wide v6, 0x3f947ae147ae147bL    # 0.02

    mul-double/2addr v4, v6

    const-wide v6, 0x3fe7ae147ae147aeL    # 0.74

    add-double/2addr v4, v6

    double-to-float v2, v4

    .line 107
    .restart local v2    # "scaleRatio":F
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 108
    .restart local v0    # "deger":D
    float-to-double v4, v2

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    div-double/2addr v4, v0

    double-to-float v2, v4

    .line 109
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fa00000    # 1.25f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 110
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fc00000    # 1.5f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 111
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fe00000    # 1.75f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 112
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer4:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    goto/16 :goto_0

    .line 113
    .end local v0    # "deger":D
    .end local v2    # "scaleRatio":F
    .end local v3    # "temp":I
    :cond_4
    const/16 v4, 0x4a

    if-lt p1, v4, :cond_0

    .line 114
    move v3, p1

    .line 115
    .restart local v3    # "temp":I
    int-to-float v4, v3

    const/high16 v5, 0x40200000    # 2.5f

    mul-float/2addr v4, v5

    float-to-int v3, v4

    .line 116
    int-to-double v4, v3

    const-wide v6, 0x3f947ae147ae147bL    # 0.02

    mul-double/2addr v4, v6

    const-wide v6, 0x3fe7ae147ae147aeL    # 0.74

    add-double/2addr v4, v6

    double-to-float v2, v4

    .line 117
    .restart local v2    # "scaleRatio":F
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 118
    .restart local v0    # "deger":D
    float-to-double v4, v2

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    div-double/2addr v4, v0

    double-to-float v2, v4

    .line 119
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fa00000    # 1.25f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 120
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fc00000    # 1.5f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 121
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x3fe00000    # 1.75f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 122
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer4:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 123
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewTablet;->layer5:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v5, 0x40200000    # 2.5f

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    goto/16 :goto_0
.end method
