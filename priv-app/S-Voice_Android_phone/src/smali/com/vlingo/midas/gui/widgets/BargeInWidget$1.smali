.class Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;
.super Ljava/lang/Object;
.source "BargeInWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/BargeInWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/BargeInWidget;)V
    .locals 0

    .prologue
    .line 237
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget.1;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget.1;"
    const/16 v2, 0x8

    .line 242
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/BargeInWidget;->capSize:I
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->access$000(Lcom/vlingo/midas/gui/widgets/BargeInWidget;)I

    move-result v0

    invoke-static {}, Lcom/vlingo/midas/gui/Widget;->getMultiWidgetItemsUltimateMax()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 245
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 246
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 254
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->requestLayout()V

    .line 255
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    invoke-static {}, Lcom/vlingo/midas/gui/Widget;->getMultiWidgetItemsUltimateMax()I

    move-result v1

    # setter for: Lcom/vlingo/midas/gui/widgets/BargeInWidget;->capSize:I
    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->access$002(Lcom/vlingo/midas/gui/widgets/BargeInWidget;I)I

    goto :goto_0
.end method
