.class Lcom/vlingo/midas/gui/ControlFragment$2;
.super Ljava/lang/Object;
.source "ControlFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/ControlFragment;->checkMissedEvents()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ControlFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ControlFragment;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragment$2;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 420
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment$2;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment;->isActivityCreated()Z

    move-result v1

    if-nez v1, :cond_0

    .line 432
    :goto_0
    return-void

    .line 425
    :cond_0
    const-string/jumbo v1, "headset_mode"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 426
    .local v0, "checkScheduleEnabled":Z
    if-eqz v0, :cond_1

    .line 427
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment$2;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-static {v1}, Lcom/vlingo/midas/util/CheckPhoneEvents;->getInstance(Lcom/vlingo/midas/gui/ControlFragment;)Lcom/vlingo/midas/util/CheckPhoneEvents;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/util/CheckPhoneEvents;->check()Z

    .line 428
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment$2;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    const/4 v2, 0x0

    # setter for: Lcom/vlingo/midas/gui/ControlFragment;->m_isRecognitionPostBTConnect:Z
    invoke-static {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->access$1002(Lcom/vlingo/midas/gui/ControlFragment;Z)Z

    .line 431
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment$2;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    goto :goto_0
.end method
