.class Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$1;
.super Ljava/lang/Object;
.source "AnswerQuestionWidget.java"

# interfaces
.implements Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->createImageContentWithoutOverlays(Lcom/vlingo/core/internal/questions/Answer$Subsection;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;

.field final synthetic val$indicator:Landroid/widget/ProgressBar;

.field final synthetic val$ri:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;Landroid/widget/ProgressBar;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$1;->val$ri:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;

    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$1;->val$indicator:Landroid/widget/ProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloaded(Lcom/vlingo/core/internal/questions/DownloadableImage;)V
    .locals 3
    .param p1, "image"    # Lcom/vlingo/core/internal/questions/DownloadableImage;

    .prologue
    .line 179
    invoke-virtual {p1, p0}, Lcom/vlingo/core/internal/questions/DownloadableImage;->removeListener(Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;)Lcom/vlingo/core/internal/questions/DownloadableImage;

    .line 180
    invoke-virtual {p1}, Lcom/vlingo/core/internal/questions/DownloadableImage;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 181
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$1$1;

    invoke-direct {v2, p0, v0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$1$1;-><init>(Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$1;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 187
    return-void
.end method

.method public onTimeout(Lcom/vlingo/core/internal/questions/DownloadableImage;)V
    .locals 0
    .param p1, "image"    # Lcom/vlingo/core/internal/questions/DownloadableImage;

    .prologue
    .line 191
    return-void
.end method
