.class public Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;
.super Ljava/lang/Object;
.source "ControlFragmentDataEWYSHelp.java"


# static fields
.field private static singletonObject:Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;


# instance fields
.field private currentGuideState:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

.field private currentState:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

.field private isPaused:Z

.field private mTTSOnIdleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private startRecoOnSpotterStop:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->startRecoOnSpotterStop:Z

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->isPaused:Z

    .line 22
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->currentState:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    .line 23
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->NONE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->currentGuideState:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->mTTSOnIdleList:Ljava/util/ArrayList;

    .line 12
    return-void
.end method

.method public static getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->singletonObject:Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    invoke-direct {v0}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;-><init>()V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->singletonObject:Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    .line 17
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->singletonObject:Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    return-object v0
.end method


# virtual methods
.method public getCurrentGuideState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->currentGuideState:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    return-object v0
.end method

.method public getCurrentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->currentState:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    return-object v0
.end method

.method public getTTSOnIdleList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->mTTSOnIdleList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->isPaused:Z

    return v0
.end method

.method public isStartRecoOnSpotterStop()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->startRecoOnSpotterStop:Z

    return v0
.end method

.method public setCurrentState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V
    .locals 0
    .param p1, "currentState"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->currentState:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    .line 49
    return-void
.end method

.method public setCurrentState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;)V
    .locals 0
    .param p1, "currentGuideState"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->currentGuideState:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    .line 56
    return-void
.end method

.method public setPaused(Z)V
    .locals 0
    .param p1, "isPaused"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->isPaused:Z

    .line 42
    return-void
.end method

.method public setStartRecoOnSpotterStop(Z)V
    .locals 0
    .param p1, "startRecoOnSpotterStop"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->startRecoOnSpotterStop:Z

    .line 33
    return-void
.end method

.method public setTTSOnIdleList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "mTTSOnIdleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->mTTSOnIdleList:Ljava/util/ArrayList;

    .line 64
    return-void
.end method
