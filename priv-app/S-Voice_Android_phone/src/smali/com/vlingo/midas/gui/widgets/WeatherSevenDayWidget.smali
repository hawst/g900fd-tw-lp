.class public Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "WeatherSevenDayWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget$WeatherSevenDayWidgetHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/weather/WeatherAdaptor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final CITY_ID_LENGHT:I = 0x7

.field private static final FIFTH_FORECAST_DAY:I = 0x5

.field private static final FIRST_FORECAST_DAY:I = 0x1

.field private static final FOURTH_FORECAST_DAY:I = 0x4

.field private static final SECOND_FORECAST_DAY:I = 0x2

.field private static final SIXTH_FORECAST_DAY:I = 0x6

.field private static final THIRD_FORECAST_DAY:I = 0x3

.field private static final ZEROTH_FORECAST_DAY:I

.field private static forecastArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected accuWeather:Landroid/widget/ImageView;

.field protected citiDetail:Landroid/widget/LinearLayout;

.field protected citi_name:Landroid/widget/TextView;

.field protected citi_name2:Landroid/widget/TextView;

.field private cityId:Ljava/lang/String;

.field protected climate_name:Landroid/widget/TextView;

.field private contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

.field private currentClimateName:Ljava/lang/String;

.field private currentCondition:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private currentTemp:Ljava/lang/String;

.field private currentWeatherCode:I

.field protected date1:Landroid/widget/TextView;

.field protected date2:Landroid/widget/TextView;

.field protected date3:Landroid/widget/TextView;

.field protected date4:Landroid/widget/TextView;

.field protected date5:Landroid/widget/TextView;

.field protected date6:Landroid/widget/TextView;

.field protected date_val:Ljava/lang/String;

.field protected date_value:Landroid/widget/TextView;

.field protected divider:Landroid/widget/TextView;

.field private forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

.field private forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

.field protected isNightmode:Z

.field protected layout1:Landroid/widget/LinearLayout;

.field protected layout2:Landroid/widget/LinearLayout;

.field protected layout3:Landroid/widget/LinearLayout;

.field protected layout4:Landroid/widget/LinearLayout;

.field protected layout5:Landroid/widget/LinearLayout;

.field protected layout6:Landroid/widget/LinearLayout;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field protected location:Ljava/lang/String;

.field protected location2:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field protected mCurrentTemp:Landroid/widget/TextView;

.field private mHandler:Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget$WeatherSevenDayWidgetHandler;

.field protected mLangApplication:Ljava/lang/String;

.field private mLastClickTime:J

.field protected mTemparature:Landroid/widget/LinearLayout;

.field protected mUnit_meter_C:Landroid/view/View;

.field protected mUnit_meter_F:Landroid/view/View;

.field private mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

.field private mWeatherInfoUtilOneDay:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

.field protected max:Ljava/lang/String;

.field protected maxTemperature1:Landroid/widget/TextView;

.field protected maxTemperature2:Landroid/widget/TextView;

.field protected maxTemperature3:Landroid/widget/TextView;

.field protected maxTemperature4:Landroid/widget/TextView;

.field protected maxTemperature5:Landroid/widget/TextView;

.field protected maxTemperature6:Landroid/widget/TextView;

.field protected maxTemperatureOneDay1:Landroid/widget/TextView;

.field protected min:Ljava/lang/String;

.field protected minTemperature1:Landroid/widget/TextView;

.field protected minTemperature2:Landroid/widget/TextView;

.field protected minTemperature3:Landroid/widget/TextView;

.field protected minTemperature4:Landroid/widget/TextView;

.field protected minTemperature5:Landroid/widget/TextView;

.field protected minTemperature6:Landroid/widget/TextView;

.field protected minTemperatureOneDay1:Landroid/widget/TextView;

.field private normalBackground:I

.field private sunRise:Ljava/lang/String;

.field private sunSet:Ljava/lang/String;

.field private tabletPortBackground:I

.field protected temp_unit:Ljava/lang/String;

.field private wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

.field protected weatherBackgroundImage:Landroid/widget/RelativeLayout;

.field protected weatherBackgroundImageNew:Landroid/widget/FrameLayout;

.field private weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

.field protected weatherImage1:Landroid/widget/ImageView;

.field protected weatherImage2:Landroid/widget/ImageView;

.field protected weatherImage3:Landroid/widget/ImageView;

.field protected weatherImage4:Landroid/widget/ImageView;

.field protected weatherImage5:Landroid/widget/ImageView;

.field protected weatherImage6:Landroid/widget/ImageView;

.field protected weatherImageOneDay1:Landroid/widget/ImageView;

.field private weathernews:Landroid/widget/ImageView;

.field protected weekday1:Landroid/widget/TextView;

.field protected weekday2:Landroid/widget/TextView;

.field protected weekday3:Landroid/widget/TextView;

.field protected weekday4:Landroid/widget/TextView;

.field protected weekday5:Landroid/widget/TextView;

.field protected weekday6:Landroid/widget/TextView;

.field protected widgetCitiLinear:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 170
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 132
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mLastClickTime:J

    .line 148
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentWeatherCode:I

    .line 152
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->isNightmode:Z

    .line 163
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->cityId:Ljava/lang/String;

    .line 166
    iput v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->normalBackground:I

    .line 167
    iput v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->tabletPortBackground:I

    .line 620
    new-instance v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget$WeatherSevenDayWidgetHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget$WeatherSevenDayWidgetHandler;-><init>(Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget$WeatherSevenDayWidgetHandler;

    .line 171
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mContext:Landroid/content/Context;

    .line 172
    new-instance v0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    .line 173
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->setValuesForcastWeatherOneDay()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->setValuesForcastWeather()V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->setTalkbackString()V

    return-void
.end method

.method private buildAccuWeatherUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "day"    # Ljava/lang/String;
    .param p2, "lang"    # Ljava/lang/String;
    .param p3, "cityId"    # Ljava/lang/String;

    .prologue
    .line 901
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 902
    .local v0, "sb":Ljava/lang/StringBuffer;
    const-string/jumbo v1, "http://www.accuweather.com/m/details"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 903
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 904
    const-string/jumbo v1, ".aspx?p=samand&lang="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 905
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 906
    const-string/jumbo v1, "&loc=cityId%3A"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 907
    invoke-virtual {v0, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 910
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getTempUnit()Z
    .locals 2

    .prologue
    .line 872
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->temp_unit:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->unit_temperature_farenheit:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->temp_unit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getWeatherDate()Ljava/lang/String;
    .locals 8

    .prologue
    .line 570
    const/4 v0, 0x0

    .line 571
    .local v0, "dateFormat":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    .line 573
    .local v1, "language":Ljava/lang/String;
    const-string/jumbo v5, "ko-KR"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "ja-JP"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 574
    :cond_0
    const-string/jumbo v5, "ko-KR"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 575
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "M"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->mon1:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " d"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " E"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->cradle_home_date:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 587
    :goto_0
    if-eqz v0, :cond_3

    .line 588
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v2, v0, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 589
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "yyyy-MM-dd"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 592
    .local v3, "sdf2":Ljava/text/SimpleDateFormat;
    :try_start_0
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtilOneDay:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getWeatherDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 593
    .local v4, "wDate":Ljava/util/Date;
    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 598
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v3    # "sdf2":Ljava/text/SimpleDateFormat;
    .end local v4    # "wDate":Ljava/util/Date;
    :goto_1
    return-object v5

    .line 579
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "M"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->mon1:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " d"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " E"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->cradle_home_date:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 584
    :cond_2
    const-string/jumbo v0, "EE, d MMMM"

    goto :goto_0

    .line 594
    .restart local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v3    # "sdf2":Ljava/text/SimpleDateFormat;
    :catch_0
    move-exception v5

    .line 598
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v3    # "sdf2":Ljava/text/SimpleDateFormat;
    :cond_3
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtilOneDay:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getDateForOneDayWidget()Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method private isLT03()Z
    .locals 3

    .prologue
    const/16 v2, 0xa00

    const/16 v1, 0x640

    .line 1003
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v0, v2, :cond_2

    .line 1007
    :cond_1
    const/4 v0, 0x1

    .line 1009
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private openAccuWeather(I)V
    .locals 4
    .param p1, "day"    # I

    .prologue
    .line 946
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 947
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "language"

    const-string/jumbo v3, "en-US"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->cityId:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->buildAccuWeatherUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 949
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 950
    return-void
.end method

.method private openAccuWeatherWeb()V
    .locals 2

    .prologue
    .line 940
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 941
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "http://www.accuweather.com/m"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 942
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 943
    return-void
.end method

.method private setTalkbackString()V
    .locals 5

    .prologue
    .line 831
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getTempUnit()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 832
    const-string/jumbo v0, "F"

    .line 837
    .local v0, "tempUnit":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 838
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xb0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 841
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperatureOneDay1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 843
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 845
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 847
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 849
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature2:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature2:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 851
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature2:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature2:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 853
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature3:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature3:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 855
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature3:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature3:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 857
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature4:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature4:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 859
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature4:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature4:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 861
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature5:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature5:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 863
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature5:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature5:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 865
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature6:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature6:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 867
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature6:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature6:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 869
    return-void

    .line 834
    .end local v0    # "tempUnit":Ljava/lang/String;
    :cond_1
    const-string/jumbo v0, "C"

    .restart local v0    # "tempUnit":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method private setValuesForcastWeather()V
    .locals 11

    .prologue
    .line 731
    const-string/jumbo v9, "#585f64"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    .line 732
    .local v7, "textColor":I
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v6

    .line 734
    .local v6, "locale":Ljava/util/Locale;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v9, "EEE"

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 735
    .local v3, "format":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v9, "EEE"

    invoke-direct {v4, v9, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 737
    .local v4, "format2":Ljava/text/SimpleDateFormat;
    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    if-eqz v9, :cond_3

    .line 738
    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 739
    .local v2, "forecastArrayListSize":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v2, :cond_3

    .line 740
    const-string/jumbo v9, "#585f64"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    .line 742
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 743
    const-string/jumbo v9, "#343434"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    .line 746
    :cond_0
    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v0

    .line 748
    .local v0, "day":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 754
    :goto_1
    const-string/jumbo v9, "SUN"

    invoke-virtual {v0, v9}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_2

    .line 755
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 756
    :cond_1
    const-string/jumbo v9, "#ea7445"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    .line 763
    :cond_2
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getWeatherCode()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->setWeatherCode(Ljava/lang/String;)V

    .line 765
    new-instance v8, Lcom/vlingo/midas/util/WeatherResourceUtil;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getWeatherCode()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v10}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getProvider()Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/vlingo/midas/util/WeatherResourceUtil;-><init>(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)V

    .line 769
    .local v8, "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    packed-switch v5, :pswitch_data_0

    .line 739
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 749
    .end local v8    # "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    :catch_0
    move-exception v1

    .line 751
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_1

    .line 771
    .end local v1    # "e":Ljava/text/ParseException;
    .restart local v8    # "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    :pswitch_0
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday1:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 772
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday1:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 773
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date1:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 775
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage1:Landroid/widget/ImageView;

    invoke-virtual {v8}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 776
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature1:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 777
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature1:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 780
    :pswitch_1
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday2:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 781
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday2:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 782
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date2:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 783
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage2:Landroid/widget/ImageView;

    invoke-virtual {v8}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 784
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature2:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 785
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature2:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 789
    :pswitch_2
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday3:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 790
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday3:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 791
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date3:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 792
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage3:Landroid/widget/ImageView;

    invoke-virtual {v8}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 793
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature3:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 794
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature3:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 798
    :pswitch_3
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday4:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 799
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday4:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 800
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date4:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 801
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage4:Landroid/widget/ImageView;

    invoke-virtual {v8}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 802
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature4:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 803
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature4:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 807
    :pswitch_4
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday5:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 808
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday5:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 809
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date5:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 810
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage5:Landroid/widget/ImageView;

    invoke-virtual {v8}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 811
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature5:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 812
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature5:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 816
    :pswitch_5
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday6:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 817
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday6:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 818
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date6:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 819
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage6:Landroid/widget/ImageView;

    invoke-virtual {v8}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 820
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature6:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 821
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature6:Landroid/widget/TextView;

    sget-object v9, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 826
    .end local v0    # "day":Ljava/lang/String;
    .end local v2    # "forecastArrayListSize":I
    .end local v5    # "i":I
    .end local v8    # "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    :cond_3
    return-void

    .line 769
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setValuesForcastWeatherOneDay()V
    .locals 14

    .prologue
    const/16 v13, 0xb0

    const/16 v12, 0x65

    const/4 v10, 0x0

    const/16 v11, 0x8

    .line 623
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weathernews:Landroid/widget/ImageView;

    if-eqz v8, :cond_0

    .line 624
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    sget-object v9, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    if-ne v8, v9, :cond_9

    .line 626
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 627
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weathernews:Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 634
    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->min:Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 635
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->min:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    double-to-int v1, v8

    .line 636
    .local v1, "minTemp":I
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 637
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 643
    .end local v1    # "minTemp":I
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->max:Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 644
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->max:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    double-to-int v0, v8

    .line 645
    .local v0, "maxTemp":I
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 646
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 653
    .end local v0    # "maxTemp":I
    :cond_2
    :goto_2
    new-instance v7, Lcom/vlingo/midas/util/WeatherResourceUtil;

    iget v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentWeatherCode:I

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getProvider()Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/vlingo/midas/util/WeatherResourceUtil;-><init>(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)V

    .line 655
    .local v7, "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->citi_name:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->location:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 656
    invoke-virtual {v7}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawable()I

    move-result v5

    .line 657
    .local v5, "weatherDrawableCode":I
    if-eq v5, v12, :cond_3

    .line 658
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImageOneDay1:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 661
    :cond_3
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v8, "yyyyMMdd h:mm a"

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 663
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    const/4 v3, 0x0

    .line 664
    .local v3, "sunRiseTime":Ljava/util/Date;
    const/4 v4, 0x0

    .line 667
    .local v4, "sunSetTime":Ljava/util/Date;
    :try_start_0
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->sunRise:Ljava/lang/String;

    if-eqz v8, :cond_4

    .line 668
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->sunRise:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 670
    :cond_4
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->sunSet:Ljava/lang/String;

    if-eqz v8, :cond_5

    .line 671
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->sunSet:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 676
    :cond_5
    :goto_3
    invoke-virtual {v7}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherSevenBackgroundDrawable()I

    move-result v6

    .line 677
    .local v6, "weatherSevenBackgroundDrawableCode":I
    if-eq v6, v12, :cond_6

    .line 688
    iget-boolean v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->isNightmode:Z

    if-eqz v8, :cond_c

    .line 689
    invoke-virtual {v7, v6}, Lcom/vlingo/midas/util/WeatherResourceUtil;->changeToNightBackground(I)I

    move-result v8

    iput v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->normalBackground:I

    .line 692
    :goto_4
    iget v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->normalBackground:I

    invoke-virtual {v7, v8}, Lcom/vlingo/midas/util/WeatherResourceUtil;->changeToVerticalBackground(I)I

    move-result v8

    iput v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->tabletPortBackground:I

    .line 693
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_d

    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->isLT03()Z

    move-result v8

    if-eqz v8, :cond_d

    .line 694
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherBackgroundImageNew:Landroid/widget/FrameLayout;

    iget v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->tabletPortBackground:I

    invoke-virtual {v8, v9}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 701
    :cond_6
    :goto_5
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->climate_name:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentClimateName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 702
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date_value:Landroid/widget/TextView;

    if-eqz v8, :cond_7

    .line 703
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date_value:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date_val:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 706
    :cond_7
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentTemp:Ljava/lang/String;

    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_f

    .line 707
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentTemp:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 713
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getTempUnit()Z

    move-result v8

    if-eqz v8, :cond_e

    .line 714
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_C:Landroid/view/View;

    if-eqz v8, :cond_8

    .line 715
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_C:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    .line 727
    :cond_8
    :goto_6
    return-void

    .line 629
    .end local v2    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    .end local v3    # "sunRiseTime":Ljava/util/Date;
    .end local v4    # "sunSetTime":Ljava/util/Date;
    .end local v5    # "weatherDrawableCode":I
    .end local v6    # "weatherSevenBackgroundDrawableCode":I
    .end local v7    # "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    :cond_9
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 630
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weathernews:Landroid/widget/ImageView;

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 639
    .restart local v1    # "minTemp":I
    :cond_a
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 648
    .end local v1    # "minTemp":I
    .restart local v0    # "maxTemp":I
    :cond_b
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 691
    .end local v0    # "maxTemp":I
    .restart local v2    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    .restart local v3    # "sunRiseTime":Ljava/util/Date;
    .restart local v4    # "sunSetTime":Ljava/util/Date;
    .restart local v5    # "weatherDrawableCode":I
    .restart local v6    # "weatherSevenBackgroundDrawableCode":I
    .restart local v7    # "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    :cond_c
    iput v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->normalBackground:I

    goto/16 :goto_4

    .line 696
    :cond_d
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v8

    if-nez v8, :cond_6

    .line 697
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherBackgroundImageNew:Landroid/widget/FrameLayout;

    iget v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->normalBackground:I

    invoke-virtual {v8, v9}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    goto/16 :goto_5

    .line 717
    :cond_e
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_F:Landroid/view/View;

    if-eqz v8, :cond_8

    .line 718
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_F:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    .line 721
    :cond_f
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 722
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_C:Landroid/view/View;

    if-eqz v8, :cond_10

    .line 723
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_C:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    .line 724
    :cond_10
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_F:Landroid/view/View;

    if-eqz v8, :cond_8

    .line 725
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_F:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    .line 673
    .end local v6    # "weatherSevenBackgroundDrawableCode":I
    :catch_0
    move-exception v8

    goto/16 :goto_3
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/weather/WeatherAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 1
    .param p1, "adaptor"    # Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 274
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 275
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    .line 276
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->onResponseReceived()V

    .line 278
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isNightMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->isNightmode:Z

    .line 279
    return-void
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 49
    check-cast p1, Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->initialize(Lcom/vlingo/core/internal/weather/WeatherAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 884
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 878
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 916
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mLastClickTime:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x3e8

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    .line 937
    :cond_0
    :goto_0
    return-void

    .line 919
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mLastClickTime:J

    .line 920
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 921
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->child_layout1:I

    if-ne v0, v1, :cond_2

    .line 922
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->openAccuWeather(I)V

    goto :goto_0

    .line 923
    :cond_2
    sget v1, Lcom/vlingo/midas/R$id;->child_layout2:I

    if-ne v0, v1, :cond_3

    .line 924
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->openAccuWeather(I)V

    goto :goto_0

    .line 925
    :cond_3
    sget v1, Lcom/vlingo/midas/R$id;->child_layout3:I

    if-ne v0, v1, :cond_4

    .line 926
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->openAccuWeather(I)V

    goto :goto_0

    .line 927
    :cond_4
    sget v1, Lcom/vlingo/midas/R$id;->child_layout4:I

    if-ne v0, v1, :cond_5

    .line 928
    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->openAccuWeather(I)V

    goto :goto_0

    .line 929
    :cond_5
    sget v1, Lcom/vlingo/midas/R$id;->child_layout5:I

    if-ne v0, v1, :cond_6

    .line 930
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->openAccuWeather(I)V

    goto :goto_0

    .line 931
    :cond_6
    sget v1, Lcom/vlingo/midas/R$id;->child_layout6:I

    if-ne v0, v1, :cond_7

    .line 932
    const/4 v1, 0x7

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->openAccuWeather(I)V

    goto :goto_0

    .line 933
    :cond_7
    sget v1, Lcom/vlingo/midas/R$id;->accu_weather:I

    if-ne v0, v1, :cond_0

    .line 934
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->openAccuWeatherWeb()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 10
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v9, 0x0

    .line 954
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 955
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->isLT03()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 957
    iget v7, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_4

    .line 958
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherBackgroundImageNew:Landroid/widget/FrameLayout;

    iget v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->tabletPortBackground:I

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 963
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_left_right_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 965
    .local v2, "citiRightMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->citiDetail:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_0

    .line 966
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_citi_top_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 968
    .local v3, "citiTopMargin":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_citi_max_width:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 970
    .local v1, "citiMaxWidth":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->citiDetail:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9, v3, v2, v9}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 971
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->citiDetail:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v1, v7, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 974
    .end local v1    # "citiMaxWidth":I
    .end local v3    # "citiTopMargin":I
    :cond_0
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    if-eqz v7, :cond_1

    .line 975
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_accu_bottom_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 977
    .local v0, "accuBottomMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v0, v7, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 978
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 981
    .end local v0    # "accuBottomMargin":I
    :cond_1
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_2

    .line 982
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_temprature_bottom_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 984
    .local v6, "tempBottomMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v6, v7, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 985
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 988
    .end local v6    # "tempBottomMargin":I
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_climate_bottom_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 990
    .local v4, "climateBottomMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->climate_name:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v4, v7, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 991
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->climate_name:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 994
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_weekly_icon_top_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 996
    .local v5, "iconTopMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImageOneDay1:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v5, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 997
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImageOneDay1:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 999
    .end local v2    # "citiRightMargin":I
    .end local v4    # "climateBottomMargin":I
    .end local v5    # "iconTopMargin":I
    :cond_3
    return-void

    .line 960
    :cond_4
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherBackgroundImageNew:Landroid/widget/FrameLayout;

    iget v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->normalBackground:I

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 182
    sget v0, Lcom/vlingo/midas/R$id;->child_layout1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout1:Landroid/widget/LinearLayout;

    .line 183
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    sget v0, Lcom/vlingo/midas/R$id;->child_layout2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout2:Landroid/widget/LinearLayout;

    .line 185
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    sget v0, Lcom/vlingo/midas/R$id;->child_layout3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout3:Landroid/widget/LinearLayout;

    .line 187
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    sget v0, Lcom/vlingo/midas/R$id;->child_layout4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout4:Landroid/widget/LinearLayout;

    .line 189
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    sget v0, Lcom/vlingo/midas/R$id;->child_layout5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout5:Landroid/widget/LinearLayout;

    .line 191
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout5:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    sget v0, Lcom/vlingo/midas/R$id;->child_layout6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout6:Landroid/widget/LinearLayout;

    .line 193
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->layout6:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday1:Landroid/widget/TextView;

    .line 197
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday2:Landroid/widget/TextView;

    .line 198
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday3:Landroid/widget/TextView;

    .line 199
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday4:Landroid/widget/TextView;

    .line 200
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday5:Landroid/widget/TextView;

    .line 201
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weekday6:Landroid/widget/TextView;

    .line 203
    sget v0, Lcom/vlingo/midas/R$id;->child_date1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date1:Landroid/widget/TextView;

    .line 204
    sget v0, Lcom/vlingo/midas/R$id;->child_date2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date2:Landroid/widget/TextView;

    .line 205
    sget v0, Lcom/vlingo/midas/R$id;->child_date3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date3:Landroid/widget/TextView;

    .line 206
    sget v0, Lcom/vlingo/midas/R$id;->child_date4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date4:Landroid/widget/TextView;

    .line 207
    sget v0, Lcom/vlingo/midas/R$id;->child_date5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date5:Landroid/widget/TextView;

    .line 208
    sget v0, Lcom/vlingo/midas/R$id;->child_date6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date6:Landroid/widget/TextView;

    .line 210
    sget v0, Lcom/vlingo/midas/R$id;->child_image1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage1:Landroid/widget/ImageView;

    .line 211
    sget v0, Lcom/vlingo/midas/R$id;->child_image2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage2:Landroid/widget/ImageView;

    .line 212
    sget v0, Lcom/vlingo/midas/R$id;->child_image3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage3:Landroid/widget/ImageView;

    .line 213
    sget v0, Lcom/vlingo/midas/R$id;->child_image4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage4:Landroid/widget/ImageView;

    .line 214
    sget v0, Lcom/vlingo/midas/R$id;->child_image5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage5:Landroid/widget/ImageView;

    .line 215
    sget v0, Lcom/vlingo/midas/R$id;->child_image6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImage6:Landroid/widget/ImageView;

    .line 217
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature1:Landroid/widget/TextView;

    .line 218
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature2:Landroid/widget/TextView;

    .line 219
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature3:Landroid/widget/TextView;

    .line 220
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature4:Landroid/widget/TextView;

    .line 221
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature5:Landroid/widget/TextView;

    .line 222
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperature6:Landroid/widget/TextView;

    .line 224
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature1:Landroid/widget/TextView;

    .line 225
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature2:Landroid/widget/TextView;

    .line 226
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature3:Landroid/widget/TextView;

    .line 227
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature4:Landroid/widget/TextView;

    .line 228
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature5:Landroid/widget/TextView;

    .line 229
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperature6:Landroid/widget/TextView;

    .line 231
    sget v0, Lcom/vlingo/midas/R$id;->citi_name:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->citi_name:Landroid/widget/TextView;

    .line 232
    sget v0, Lcom/vlingo/midas/R$id;->imageView_singledayweather:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherImageOneDay1:Landroid/widget/ImageView;

    .line 233
    sget v0, Lcom/vlingo/midas/R$id;->climate_name:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->climate_name:Landroid/widget/TextView;

    .line 234
    sget v0, Lcom/vlingo/midas/R$id;->present_temp:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    .line 235
    sget v0, Lcom/vlingo/midas/R$id;->widget_citi_today:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherBackgroundImageNew:Landroid/widget/FrameLayout;

    .line 236
    sget v0, Lcom/vlingo/midas/R$id;->widget_citi_LinearLayout:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->widgetCitiLinear:Landroid/widget/LinearLayout;

    .line 237
    sget v0, Lcom/vlingo/midas/R$id;->widget_citi:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherBackgroundImage:Landroid/widget/RelativeLayout;

    .line 238
    sget v0, Lcom/vlingo/midas/R$id;->date_view:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date_value:Landroid/widget/TextView;

    .line 239
    sget v0, Lcom/vlingo/midas/R$id;->unit_meter_F:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_F:Landroid/view/View;

    .line 240
    sget v0, Lcom/vlingo/midas/R$id;->unit_meter_C:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_C:Landroid/view/View;

    .line 241
    sget v0, Lcom/vlingo/midas/R$id;->accu_weather:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    .line 242
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    sget v0, Lcom/vlingo/midas/R$id;->min_temp:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->minTemperatureOneDay1:Landroid/widget/TextView;

    .line 244
    sget v0, Lcom/vlingo/midas/R$id;->max_temp:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    .line 245
    sget v0, Lcom/vlingo/midas/R$id;->devider_small:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->divider:Landroid/widget/TextView;

    .line 246
    sget v0, Lcom/vlingo/midas/R$id;->citi_details:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->citiDetail:Landroid/widget/LinearLayout;

    .line 248
    sget v0, Lcom/vlingo/midas/R$id;->weathernews:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weathernews:Landroid/widget/ImageView;

    .line 249
    sget v0, Lcom/vlingo/midas/R$id;->temprature:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    .line 251
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_F:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 256
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mUnit_meter_C:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 263
    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 268
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 269
    return-void
.end method

.method public declared-synchronized onResponseReceived()V
    .locals 46

    .prologue
    .line 282
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getProvider()Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    .line 284
    new-instance v32, Ljava/text/SimpleDateFormat;

    const-string/jumbo v42, "EEE"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v43

    move-object/from16 v0, v32

    move-object/from16 v1, v42

    move-object/from16 v2, v43

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 285
    .local v32, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v33, Ljava/text/SimpleDateFormat;

    const-string/jumbo v42, "yyyy-MM-dd"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v43

    move-object/from16 v0, v33

    move-object/from16 v1, v42

    move-object/from16 v2, v43

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 287
    .local v33, "sdf2":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    if-eqz v42, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v42

    if-nez v42, :cond_1

    .line 289
    :cond_0
    new-instance v25, Landroid/content/Intent;

    invoke-direct/range {v25 .. v25}, Landroid/content/Intent;-><init>()V

    .line 290
    .local v25, "intent":Landroid/content/Intent;
    const-string/jumbo v42, "com.vlingo.core.internal.dialogmanager.NoData"

    move-object/from16 v0, v25

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-object/from16 v42, v0

    const/16 v43, 0x0

    move-object/from16 v0, v42

    move-object/from16 v1, v25

    move-object/from16 v2, v43

    invoke-interface {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 567
    .end local v25    # "intent":Landroid/content/Intent;
    :goto_0
    monitor-exit p0

    return-void

    .line 296
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationState()Ljava/lang/String;

    move-result-object v36

    .line 297
    .local v36, "state":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v8

    .line 298
    .local v8, "city":Ljava/lang/String;
    if-nez v36, :cond_2

    const-string/jumbo v36, ""

    .line 299
    :cond_2
    if-nez v8, :cond_3

    const-string/jumbo v8, ""

    .line 300
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v42

    sget v43, Lcom/vlingo/midas/R$string;->weather_location:I

    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v44, v0

    const/16 v45, 0x0

    aput-object v8, v44, v45

    const/16 v45, 0x1

    aput-object v36, v44, v45

    invoke-virtual/range {v42 .. v44}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->location:Ljava/lang/String;
    :try_end_1
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 307
    .end local v8    # "city":Ljava/lang/String;
    .end local v36    # "state":Ljava/lang/String;
    :goto_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    const-string/jumbo v43, "Units"

    invoke-virtual/range {v42 .. v43}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v42

    const-string/jumbo v43, "Units"

    invoke-virtual/range {v42 .. v43}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    .line 309
    .local v3, "Unit":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v43, "Temperature"

    invoke-interface/range {v42 .. v43}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/lang/String;

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->temp_unit:Ljava/lang/String;
    :try_end_2
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 316
    .end local v3    # "Unit":Lcom/vlingo/core/internal/weather/WeatherElement;
    :goto_2
    const/16 v41, 0x0

    .line 317
    .local v41, "weatherDetailsCountChild":I
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    if-eqz v42, :cond_4

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v41

    .line 320
    :cond_4
    const/16 v22, 0x0

    .local v22, "index":I
    :goto_3
    move/from16 v0, v22

    move/from16 v1, v41

    if-ge v0, v1, :cond_1d

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v6

    .line 322
    .local v6, "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v42, "CurrentCondition"

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_5

    .line 325
    const-string/jumbo v42, "CurrentCondition"

    const/16 v43, 0x0

    move/from16 v0, v43

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1c

    .line 326
    const/16 v42, 0x0

    move/from16 v0, v42

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentCondition:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 327
    new-instance v42, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentCondition:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v43, v0

    invoke-direct/range {v42 .. v43}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;-><init>(Lcom/vlingo/core/internal/weather/WeatherElement;)V

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtilOneDay:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    .line 328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtilOneDay:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getCurrentWeatherCode()I

    move-result v42

    move/from16 v0, v42

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentWeatherCode:I

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtilOneDay:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getWeatherString()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentClimateName:Ljava/lang/String;

    .line 331
    new-instance v34, Ljava/text/SimpleDateFormat;

    const-string/jumbo v42, "yyyyMMdd "

    sget-object v43, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v34

    move-object/from16 v1, v42

    move-object/from16 v2, v43

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 333
    .local v34, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v43

    invoke-static/range {v43 .. v44}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v43

    move-object/from16 v0, v34

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentCondition:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v44, "Sunrise"

    move-object/from16 v0, v42

    move-object/from16 v1, v44

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/lang/String;

    move-object/from16 v0, v43

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->sunRise:Ljava/lang/String;

    .line 335
    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v43

    invoke-static/range {v43 .. v44}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v43

    move-object/from16 v0, v34

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentCondition:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v44, "Sunset"

    move-object/from16 v0, v42

    move-object/from16 v1, v44

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/lang/String;

    move-object/from16 v0, v43

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->sunSet:Ljava/lang/String;

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentCondition:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    invoke-interface/range {v42 .. v42}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v42

    invoke-interface/range {v42 .. v42}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v42

    if-eqz v42, :cond_1c

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/util/Map$Entry;

    goto :goto_4

    .line 301
    .end local v6    # "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v19    # "i$":Ljava/util/Iterator;
    .end local v22    # "index":I
    .end local v34    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    .end local v41    # "weatherDetailsCountChild":I
    :catch_0
    move-exception v14

    .line 303
    .local v14, "e2":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    invoke-virtual {v14}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 282
    .end local v14    # "e2":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    .end local v32    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v33    # "sdf2":Ljava/text/SimpleDateFormat;
    :catchall_0
    move-exception v42

    monitor-exit p0

    throw v42

    .line 310
    .restart local v32    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v33    # "sdf2":Ljava/text/SimpleDateFormat;
    :catch_1
    move-exception v13

    .line 312
    .local v13, "e1":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    :try_start_4
    invoke-virtual {v13}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->printStackTrace()V

    goto/16 :goto_2

    .line 346
    .end local v13    # "e1":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    .restart local v6    # "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v22    # "index":I
    .restart local v41    # "weatherDetailsCountChild":I
    :cond_5
    const-string/jumbo v42, "Forecasts"

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_17

    .line 350
    sget-object v42, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    if-eqz v42, :cond_6

    .line 351
    const/16 v42, 0x0

    sput-object v42, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    .line 354
    :cond_6
    new-instance v42, Ljava/util/ArrayList;

    invoke-direct/range {v42 .. v42}, Ljava/util/ArrayList;-><init>()V

    sput-object v42, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    .line 355
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 360
    new-instance v4, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v42

    move/from16 v0, v42

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 361
    .local v4, "arrayDate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v17, 0x0

    .line 362
    .local v17, "forecastsCountChild":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    if-eqz v42, :cond_7

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v17

    .line 365
    :cond_7
    const/16 v23, 0x0

    .local v23, "index1":I
    :goto_5
    move/from16 v0, v23

    move/from16 v1, v17

    if-ge v0, v1, :cond_8

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v43, "ForecastDate"

    invoke-interface/range {v42 .. v43}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v42

    move-object/from16 v0, v42

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 365
    add-int/lit8 v23, v23, 0x1

    goto :goto_5

    .line 369
    :cond_8
    const/16 v27, 0x0

    .line 371
    .local v27, "minDate":Ljava/util/Date;
    const/16 v42, 0x0

    :try_start_5
    move/from16 v0, v42

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/lang/String;

    move-object/from16 v0, v33

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_5
    .catch Ljava/text/ParseException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v27

    .line 374
    :goto_6
    const/4 v5, 0x0

    .line 375
    .local v5, "arrayDateSize":I
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    if-eqz v42, :cond_9

    .line 376
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v5

    .line 378
    :cond_9
    const/16 v23, 0x1

    :goto_7
    move/from16 v0, v23

    if-ge v0, v5, :cond_b

    .line 380
    if-eqz v27, :cond_a

    :try_start_7
    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/lang/String;

    move-object/from16 v0, v33

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v42

    move-object/from16 v0, v27

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v42

    if-eqz v42, :cond_a

    .line 381
    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/lang/String;

    move-object/from16 v0, v33

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_7
    .catch Ljava/text/ParseException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v27

    .line 378
    :cond_a
    :goto_8
    add-int/lit8 v23, v23, 0x1

    goto :goto_7

    .line 388
    :cond_b
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mContext:Landroid/content/Context;

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getDate()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v42 .. v44}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getWeatherInfoUtil(Landroid/content/Context;Lcom/vlingo/core/internal/weather/WeatherElement;Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtilOneDay:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    .line 390
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->getWeatherDate()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date_val:Ljava/lang/String;

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->date_val:Ljava/lang/String;

    move-object/from16 v42, v0

    if-nez v42, :cond_c

    .line 396
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtilOneDay:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getMaximumTempOneDayWeather()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->max:Ljava/lang/String;

    .line 397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->max:Ljava/lang/String;

    move-object/from16 v42, v0

    if-nez v42, :cond_d

    .line 401
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtilOneDay:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getMinimumTempOneDayWeather()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->min:Ljava/lang/String;

    .line 402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->min:Ljava/lang/String;

    move-object/from16 v42, v0

    if-nez v42, :cond_e

    .line 407
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtilOneDay:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getCurrentTempForTTS()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentTemp:Ljava/lang/String;

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->currentTemp:Ljava/lang/String;

    move-object/from16 v42, v0

    if-nez v42, :cond_f

    .line 414
    :cond_f
    const/16 v18, 0x0

    .line 415
    .local v18, "forecastsGetCountChild":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    if-eqz v42, :cond_10

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v18

    .line 418
    :cond_10
    const/16 v23, 0x0

    :goto_9
    move/from16 v0, v23

    move/from16 v1, v18

    if-ge v0, v1, :cond_1c

    .line 419
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v16

    .line 421
    .local v16, "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    if-eqz v16, :cond_13

    .line 422
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v7

    .line 424
    .local v7, "childCount":I
    const/4 v12, 0x0

    .line 425
    .local v12, "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    const/16 v31, 0x0

    .line 426
    .local v31, "nite":Lcom/vlingo/core/internal/weather/WeatherElement;
    if-lez v7, :cond_11

    .line 427
    const/16 v42, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v42

    const/16 v43, 0x0

    invoke-virtual/range {v42 .. v43}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v12

    .line 430
    const/16 v42, 0x1

    move/from16 v0, v42

    if-le v7, v0, :cond_11

    .line 431
    const/16 v42, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v42

    const/16 v43, 0x0

    invoke-virtual/range {v42 .. v43}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v31

    .line 441
    :cond_11
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v43, "ForecastDate"

    invoke-interface/range {v42 .. v43}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 442
    .local v9, "dateStr":Ljava/lang/String;
    const-string/jumbo v40, ""

    .line 443
    .local v40, "weatherCode":Ljava/lang/String;
    if-eqz v12, :cond_14

    .line 444
    invoke-virtual {v12}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v43, "WeatherCode"

    invoke-interface/range {v42 .. v43}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    .end local v40    # "weatherCode":Ljava/lang/String;
    check-cast v40, Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 457
    .restart local v40    # "weatherCode":Ljava/lang/String;
    :cond_12
    :goto_a
    move-object v11, v9

    .line 461
    .local v11, "dateValue":Ljava/lang/String;
    :try_start_9
    move-object/from16 v0, v33

    invoke-virtual {v0, v9}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v39

    .line 462
    .local v39, "wDate":Ljava/util/Date;
    move-object/from16 v0, v32

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v9

    .line 467
    .end local v39    # "wDate":Ljava/util/Date;
    :goto_b
    :try_start_a
    const-string/jumbo v37, ""

    .line 468
    .local v37, "tempMax":Ljava/lang/String;
    const-string/jumbo v38, ""

    .line 469
    .local v38, "tempMin":Ljava/lang/String;
    sget-object v42, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-object/from16 v43, v0

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    if-eq v0, v1, :cond_16

    if-eqz v12, :cond_16

    if-eqz v31, :cond_16

    .line 471
    invoke-virtual {v12}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v43, "TempMax"

    invoke-interface/range {v42 .. v43}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v37

    .end local v37    # "tempMax":Ljava/lang/String;
    check-cast v37, Ljava/lang/String;

    .line 472
    .restart local v37    # "tempMax":Ljava/lang/String;
    invoke-virtual/range {v31 .. v31}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v43, "TempMin"

    invoke-interface/range {v42 .. v43}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v38

    .end local v38    # "tempMin":Ljava/lang/String;
    check-cast v38, Ljava/lang/String;

    .line 480
    .restart local v38    # "tempMin":Ljava/lang/String;
    :goto_c
    packed-switch v23, :pswitch_data_0

    .line 418
    .end local v7    # "childCount":I
    .end local v9    # "dateStr":Ljava/lang/String;
    .end local v11    # "dateValue":Ljava/lang/String;
    .end local v12    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v31    # "nite":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v37    # "tempMax":Ljava/lang/String;
    .end local v38    # "tempMin":Ljava/lang/String;
    .end local v40    # "weatherCode":Ljava/lang/String;
    :cond_13
    :goto_d
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_9

    .line 445
    .restart local v7    # "childCount":I
    .restart local v9    # "dateStr":Ljava/lang/String;
    .restart local v12    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v31    # "nite":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v40    # "weatherCode":Ljava/lang/String;
    :cond_14
    if-eqz v31, :cond_15

    .line 446
    invoke-virtual/range {v31 .. v31}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v43, "WeatherCode"

    invoke-interface/range {v42 .. v43}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    .end local v40    # "weatherCode":Ljava/lang/String;
    check-cast v40, Ljava/lang/String;

    .restart local v40    # "weatherCode":Ljava/lang/String;
    goto :goto_a

    .line 447
    :cond_15
    sget-object v42, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-object/from16 v43, v0

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    if-ne v0, v1, :cond_12

    .line 448
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v43, "WeatherCode"

    invoke-interface/range {v42 .. v43}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    .end local v40    # "weatherCode":Ljava/lang/String;
    check-cast v40, Ljava/lang/String;

    .restart local v40    # "weatherCode":Ljava/lang/String;
    goto :goto_a

    .line 474
    .restart local v11    # "dateValue":Ljava/lang/String;
    .restart local v37    # "tempMax":Ljava/lang/String;
    .restart local v38    # "tempMin":Ljava/lang/String;
    :cond_16
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v43, "TempMaxC"

    invoke-interface/range {v42 .. v43}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v37

    .end local v37    # "tempMax":Ljava/lang/String;
    check-cast v37, Ljava/lang/String;

    .line 475
    .restart local v37    # "tempMax":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    const-string/jumbo v43, "TempMinC"

    invoke-interface/range {v42 .. v43}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v38

    .end local v38    # "tempMin":Ljava/lang/String;
    check-cast v38, Ljava/lang/String;

    .restart local v38    # "tempMin":Ljava/lang/String;
    goto :goto_c

    .line 489
    :pswitch_0
    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v43, 0x5

    const/16 v44, 0x7

    move/from16 v0, v43

    move/from16 v1, v44

    invoke-virtual {v11, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string/jumbo v43, "/"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const/16 v43, 0x8

    const/16 v44, 0xa

    move/from16 v0, v43

    move/from16 v1, v44

    invoke-virtual {v11, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 490
    .local v10, "dateVal":Ljava/lang/String;
    new-instance v42, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct/range {v42 .. v43}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;-><init>(Lcom/vlingo/core/internal/weather/WeatherInfoUtil;)V

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    invoke-virtual {v0, v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->setDays(Ljava/lang/String;)V

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    invoke-virtual {v0, v10}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->setDate(Ljava/lang/String;)V

    .line 493
    invoke-static/range {v38 .. v38}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v42

    move/from16 v0, v42

    float-to-int v0, v0

    move/from16 v28, v0

    .line 494
    .local v28, "minTemp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v42, v0

    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v43

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v43

    const-string/jumbo v44, ""

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->setMinTemp(Ljava/lang/String;)V

    .line 495
    invoke-static/range {v37 .. v37}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v42

    move/from16 v0, v42

    float-to-int v0, v0

    move/from16 v26, v0

    .line 496
    .local v26, "maxTemp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v42, v0

    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v43

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v43

    const-string/jumbo v44, ""

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->setMaxTemp(Ljava/lang/String;)V

    .line 497
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->setWeatherCode(Ljava/lang/String;)V

    .line 498
    sget-object v42, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v43, v0

    invoke-virtual/range {v42 .. v43}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501
    const/16 v37, 0x0

    .line 502
    const/16 v38, 0x0

    .line 503
    const/4 v10, 0x0

    .line 504
    goto/16 :goto_d

    .line 512
    .end local v4    # "arrayDate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "arrayDateSize":I
    .end local v7    # "childCount":I
    .end local v9    # "dateStr":Ljava/lang/String;
    .end local v10    # "dateVal":Ljava/lang/String;
    .end local v11    # "dateValue":Ljava/lang/String;
    .end local v12    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v16    # "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v17    # "forecastsCountChild":I
    .end local v18    # "forecastsGetCountChild":I
    .end local v23    # "index1":I
    .end local v26    # "maxTemp":I
    .end local v27    # "minDate":Ljava/util/Date;
    .end local v28    # "minTemp":I
    .end local v31    # "nite":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v37    # "tempMax":Ljava/lang/String;
    .end local v38    # "tempMin":Ljava/lang/String;
    .end local v40    # "weatherCode":Ljava/lang/String;
    :cond_17
    const-string/jumbo v42, "Images"

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1a

    .line 516
    const/16 v21, 0x0

    .line 520
    .local v21, "imagesCountChild":I
    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v21

    .line 523
    const/16 v23, 0x0

    .restart local v23    # "index1":I
    :goto_e
    move/from16 v0, v23

    move/from16 v1, v21

    if-ge v0, v1, :cond_1c

    .line 524
    move/from16 v0, v23

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v20

    .line 525
    .local v20, "image":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    invoke-interface/range {v42 .. v42}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v42

    invoke-interface/range {v42 .. v42}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .restart local v19    # "i$":Ljava/util/Iterator;
    :cond_18
    :goto_f
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v42

    if-eqz v42, :cond_19

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Map$Entry;

    .line 528
    .local v15, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v15}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/lang/String;

    const-string/jumbo v43, "url"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v42

    if-eqz v42, :cond_18

    .line 530
    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/lang/String;

    const-string/jumbo v43, "cityId="

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result v35

    .line 531
    .local v35, "startIndexOfCityId":I
    add-int/lit8 v24, v35, 0x7

    .line 533
    .local v24, "indexOfCityId":I
    :try_start_b
    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/lang/String;

    move-object/from16 v0, v42

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->cityId:Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_f

    .line 534
    :catch_2
    move-exception v42

    goto :goto_f

    .line 523
    .end local v15    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v24    # "indexOfCityId":I
    .end local v35    # "startIndexOfCityId":I
    :cond_19
    add-int/lit8 v23, v23, 0x1

    goto :goto_e

    .line 543
    .end local v19    # "i$":Ljava/util/Iterator;
    .end local v20    # "image":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v21    # "imagesCountChild":I
    .end local v23    # "index1":I
    :cond_1a
    :try_start_c
    const-string/jumbo v42, "MoonPhases"

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1c

    .line 548
    const/16 v30, 0x0

    .line 551
    .local v30, "moonPhasesCountChild":I
    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v30

    .line 552
    const/16 v23, 0x0

    .restart local v23    # "index1":I
    :goto_10
    move/from16 v0, v23

    move/from16 v1, v30

    if-ge v0, v1, :cond_1c

    .line 553
    move/from16 v0, v23

    invoke-virtual {v6, v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v29

    .line 555
    .local v29, "moonPhase":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-virtual/range {v29 .. v29}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v42

    invoke-interface/range {v42 .. v42}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v42

    invoke-interface/range {v42 .. v42}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .restart local v19    # "i$":Ljava/util/Iterator;
    :goto_11
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v42

    if-eqz v42, :cond_1b

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/util/Map$Entry;

    goto :goto_11

    .line 552
    :cond_1b
    add-int/lit8 v23, v23, 0x1

    goto :goto_10

    .line 320
    .end local v19    # "i$":Ljava/util/Iterator;
    .end local v23    # "index1":I
    .end local v29    # "moonPhase":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v30    # "moonPhasesCountChild":I
    :cond_1c
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_3

    .line 565
    .end local v6    # "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mContext:Landroid/content/Context;

    move-object/from16 v42, v0

    const-string/jumbo v43, "com.vlingo.midas"

    const-string/jumbo v44, "WEAT"

    invoke-static/range {v42 .. v44}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget$WeatherSevenDayWidgetHandler;

    move-object/from16 v42, v0

    const/16 v43, 0x0

    invoke-virtual/range {v42 .. v43}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget$WeatherSevenDayWidgetHandler;->sendEmptyMessage(I)Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 463
    .restart local v4    # "arrayDate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "arrayDateSize":I
    .restart local v6    # "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v7    # "childCount":I
    .restart local v9    # "dateStr":Ljava/lang/String;
    .restart local v11    # "dateValue":Ljava/lang/String;
    .restart local v12    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v16    # "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v17    # "forecastsCountChild":I
    .restart local v18    # "forecastsGetCountChild":I
    .restart local v23    # "index1":I
    .restart local v27    # "minDate":Ljava/util/Date;
    .restart local v31    # "nite":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v40    # "weatherCode":Ljava/lang/String;
    :catch_3
    move-exception v42

    goto/16 :goto_b

    .line 383
    .end local v7    # "childCount":I
    .end local v9    # "dateStr":Ljava/lang/String;
    .end local v11    # "dateValue":Ljava/lang/String;
    .end local v12    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v16    # "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v18    # "forecastsGetCountChild":I
    .end local v31    # "nite":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v40    # "weatherCode":Ljava/lang/String;
    :catch_4
    move-exception v42

    goto/16 :goto_8

    .line 372
    .end local v5    # "arrayDateSize":I
    :catch_5
    move-exception v42

    goto/16 :goto_6

    .line 480
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
