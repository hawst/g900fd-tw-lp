.class Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget$1;
.super Ljava/lang/Object;
.source "ScheduleSingleWidget.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 90
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 92
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.BodyChange"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTitle:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->access$000(Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 96
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
