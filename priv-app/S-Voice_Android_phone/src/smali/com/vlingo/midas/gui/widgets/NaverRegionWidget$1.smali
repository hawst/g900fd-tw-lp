.class Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1;
.super Ljava/lang/Object;
.source "NaverRegionWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 125
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->access$000(Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;)Lcom/vlingo/midas/naver/NaverRegionItem;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/midas/naver/NaverRegionItem;->getMapImageUrl()Ljava/lang/String;

    move-result-object v9

    .line 126
    .local v9, "url":Ljava/lang/String;
    if-nez v9, :cond_0

    .line 179
    :goto_0
    return-void

    .line 128
    :cond_0
    const-string/jumbo v10, "defaul&"

    const-string/jumbo v11, "default&"

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 129
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$dimen;->widget_naver_map_width:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v6, v10

    .line 130
    .local v6, "mapWidth":I
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$dimen;->widget_naver_map_height:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v5, v10

    .line 131
    .local v5, "mapHeight":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "&w="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "&h="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 132
    move-object v2, v9

    .line 135
    .local v2, "encryptedUrl":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/naver/api/security/client/MACManager;->initialize()V

    .line 136
    invoke-static {v9}, Lcom/naver/api/security/client/MACManager;->getEncryptUrl(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 144
    :goto_1
    :try_start_1
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 145
    .local v7, "picURL":Ljava/net/URL;
    invoke-virtual {v7}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-static {v10}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 149
    .local v3, "image":Landroid/graphics/Bitmap;
    if-nez v3, :cond_2

    .line 151
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-virtual {v7}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 152
    .local v4, "in":Ljava/io/BufferedReader;
    const/4 v8, 0x0

    .line 153
    .local v8, "str":Ljava/lang/String;
    :goto_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 156
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;)Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v10, v8, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 176
    .end local v3    # "image":Landroid/graphics/Bitmap;
    .end local v4    # "in":Ljava/io/BufferedReader;
    .end local v7    # "picURL":Ljava/net/URL;
    .end local v8    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 137
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 139
    .local v1, "e1":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 158
    .end local v1    # "e1":Ljava/lang/Exception;
    .restart local v3    # "image":Landroid/graphics/Bitmap;
    .restart local v4    # "in":Ljava/io/BufferedReader;
    .restart local v7    # "picURL":Ljava/net/URL;
    .restart local v8    # "str":Ljava/lang/String;
    :cond_1
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 175
    .end local v4    # "in":Ljava/io/BufferedReader;
    .end local v8    # "str":Ljava/lang/String;
    :goto_3
    invoke-virtual {v7}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 161
    :cond_2
    new-instance v10, Landroid/os/Handler;

    iget-object v11, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;

    invoke-virtual {v11}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v11, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1$1;

    invoke-direct {v11, p0, v3}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1$1;-><init>(Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1;Landroid/graphics/Bitmap;)V

    const-wide/16 v12, 0x1f4

    invoke-virtual {v10, v11, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3
.end method
