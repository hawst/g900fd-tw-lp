.class public Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "MultipleMessageWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
        ">;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field protected static final TRIMMED_MAX_MESSAGE_LENGTH:I = 0x1e


# instance fields
.field protected context:Landroid/content/Context;

.field protected listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field protected mMultipleMessages:Landroid/widget/ListView;

.field private messages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->context:Landroid/content/Context;

    .line 47
    return-void
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 30
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 3
    .param p1, "m"    # Ljava/util/List;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 64
    move-object v0, p1

    .line 65
    .local v0, "msgs":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageType;>;"
    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->messages:Ljava/util/List;

    .line 66
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 67
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->messages:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 68
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->mMultipleMessages:Landroid/widget/ListView;

    new-instance v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;

    invoke-direct {v2, p0, v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 78
    :cond_0
    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 52
    sget v0, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->mMultipleMessages:Landroid/widget/ListView;

    .line 53
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 181
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->mMultipleMessages:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 59
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 60
    return-void
.end method
