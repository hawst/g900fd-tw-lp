.class Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;
.super Ljava/lang/Object;
.source "LocalSearchWidget.java"

# interfaces
.implements Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WidgetResponseReceivedListenerImpl"
.end annotation


# instance fields
.field dialog:Landroid/app/Dialog;

.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;Landroid/app/Dialog;)V
    .locals 0
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 502
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    .line 503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 504
    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->dialog:Landroid/app/Dialog;

    .line 505
    return-void
.end method


# virtual methods
.method public onRequestFailed()V
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$3;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$3;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 562
    :cond_0
    return-void
.end method

.method public onRequestFailed(Lcom/vlingo/core/internal/ResourceIdProvider$string;)V
    .locals 0
    .param p1, "prompt"    # Lcom/vlingo/core/internal/ResourceIdProvider$string;

    .prologue
    .line 568
    return-void
.end method

.method public onRequestScheduled()V
    .locals 2

    .prologue
    .line 546
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$2;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 552
    :cond_0
    return-void
.end method

.method public onResponseReceived()V
    .locals 4

    .prologue
    .line 510
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$400(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getLocalSearchDetailsListing()Ljava/util/Vector;

    move-result-object v0

    .line 511
    .local v0, "listings":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/vlingo/core/internal/localsearch/LocalSearchListing;>;"
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 512
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .line 513
    .local v1, "reviewListing":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 514
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;

    invoke-direct {v3, p0, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 542
    .end local v1    # "reviewListing":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    :cond_0
    return-void
.end method
