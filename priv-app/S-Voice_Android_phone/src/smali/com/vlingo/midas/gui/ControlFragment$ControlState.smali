.class public final enum Lcom/vlingo/midas/gui/ControlFragment$ControlState;
.super Ljava/lang/Enum;
.source "ControlFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ControlFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ControlState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/gui/ControlFragment$ControlState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum DIALOG_BUSY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum SHOW_MIC:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum START_LISTENING_ANIM_TUTORIAL:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum TUTORIAL_IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field public static final enum TUTORIAL_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 77
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "ASR_GETTING_READY"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "ASR_LISTENING"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "ASR_THINKING"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "ASR_POST_RESPONSE"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "ASR_EDITING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "DIALOG_BUSY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->DIALOG_BUSY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "TUTORIAL_IDLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->TUTORIAL_IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "TUTORIAL_LISTENING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->TUTORIAL_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "START_LISTENING_ANIM_TUTORIAL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->START_LISTENING_ANIM_TUTORIAL:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const-string/jumbo v1, "SHOW_MIC"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->SHOW_MIC:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->DIALOG_BUSY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->TUTORIAL_IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->TUTORIAL_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->START_LISTENING_ANIM_TUTORIAL:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->SHOW_MIC:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->$VALUES:[Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/gui/ControlFragment$ControlState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    const-class v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/gui/ControlFragment$ControlState;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->$VALUES:[Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v0}, [Lcom/vlingo/midas/gui/ControlFragment$ControlState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    return-object v0
.end method
