.class Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;
.super Landroid/widget/LinearLayout;
.source "VoicePromptUserConfirmation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConfirmationView"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;
    }
.end annotation


# instance fields
.field private dont_ask:Landroid/widget/CheckBox;

.field private mDensity:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "_switch"    # Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    .prologue
    .line 171
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 173
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->dialog_voice_prompt_confirmation:I

    invoke-static {v3, v4, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 174
    const-string/jumbo v3, "system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v3}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 175
    .local v1, "samsungSans_text":Landroid/graphics/Typeface;
    sget v3, Lcom/vlingo/midas/R$id;->text:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 178
    .local v2, "text":Landroid/widget/TextView;
    sget v3, Lcom/vlingo/midas/R$id;->text:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;->OFF:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    if-ne p2, v4, :cond_2

    sget v4, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_for_off:I

    :goto_0
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 181
    sget v3, Lcom/vlingo/midas/R$id;->dont_ask:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->dont_ask:Landroid/widget/CheckBox;

    .line 183
    if-eqz v2, :cond_0

    .line 184
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 186
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->dont_ask:Landroid/widget/CheckBox;

    if-eqz v3, :cond_1

    .line 187
    iget-object v3, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 190
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    iput v3, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->mDensity:F

    .line 191
    iget-object v3, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v3

    const/high16 v4, 0x41000000    # 8.0f

    iget v5, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->mDensity:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v4, v4

    add-int v0, v3, v4

    .line 192
    .local v0, "leftPadding":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->dont_ask:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v3, v0, v4, v5, v6}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 196
    iget-object v3, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->dont_ask:Landroid/widget/CheckBox;

    new-instance v4, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$1;

    invoke-direct {v4, p0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$1;-><init>(Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    return-void

    .line 178
    .end local v0    # "leftPadding":I
    :cond_2
    sget v4, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_for_on:I

    goto :goto_0
.end method


# virtual methods
.method public DontAsk()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->dont_ask:Landroid/widget/CheckBox;

    return-object v0
.end method
