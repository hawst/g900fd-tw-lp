.class Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;
.super Ljava/lang/Object;
.source "LocalSearchWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->getDetailLocalSearchView()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 352
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.AcceptedText"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 353
    .local v3, "in":Landroid/content/Intent;
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v5, v5, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$500(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v5

    new-instance v6, Lcom/vlingo/core/internal/recognition/acceptedtext/LocalSearchAcceptedText;

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v7, v7, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
    invoke-static {v7}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$400(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCurrentSearchQuery()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;->MAP:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/vlingo/core/internal/recognition/acceptedtext/LocalSearchAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 355
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getFullAddress()Ljava/lang/String;

    move-result-object v0

    .line 356
    .local v0, "address":Ljava/lang/String;
    const/4 v4, 0x0

    .line 357
    .local v4, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 358
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->isENaviEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 359
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getLatitude()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v6}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getLongitude()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getShowMapIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 373
    :goto_0
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v5, v5, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 374
    return-void

    .line 360
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasBaiduMaps()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 361
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getLatitude()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v6}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getLongitude()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v7}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v8}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getFullAddress()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v6, v7, v8}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getMarkerMapIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_0

    .line 364
    :cond_1
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string/jumbo v5, "com.autonavi.xmgd.action.SHOWMAP"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 365
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string/jumbo v5, "target"

    const-string/jumbo v6, "\n"

    const-string/jumbo v7, ""

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 368
    :cond_2
    const-string/jumbo v5, " "

    const-string/jumbo v6, "+"

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 369
    .local v2, "encodedAddr":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "geo:0,0?q=="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 370
    .local v1, "arg":Ljava/lang/String;
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 371
    .restart local v4    # "intent":Landroid/content/Intent;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_0
.end method
