.class Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;
.super Ljava/lang/Object;
.source "AlwaysOnSettingsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 171
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->unalbe_to_set_wakeup_using_bt_toast:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 188
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string/jumbo v2, "WakeupCommandPreference1"

    invoke-virtual {v1, v2, v4}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "isRecorded"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 176
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 177
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "WelCome"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 178
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 180
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 181
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "trainType"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 182
    const-string/jumbo v1, "fromAlwaysOnSettings"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 183
    const-string/jumbo v1, "svoice"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 184
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-virtual {v1, v0, v3}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
