.class public Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;
.super Landroid/os/Handler;
.source "EditWhatYouSaidHelpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TutorialHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V
    .locals 1
    .param p2, "out"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .prologue
    .line 2055
    iput-object p1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2056
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 2057
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2061
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .line 2062
    .local v0, "o":Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    if-nez v0, :cond_0

    .line 2097
    :goto_0
    return-void

    .line 2065
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2067
    :pswitch_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->READY:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setGuideState(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2068
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->READY:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->AdjustHelpVisibility(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2069
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->READY:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->AdjustHelpPosition(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2070
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->READY:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->StartHelpAnimation(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2072
    # getter for: Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    invoke-static {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->access$700(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doScrollInEditMode(I)V

    goto :goto_0

    .line 2076
    :pswitch_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setGuideState(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2077
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->AdjustHelpVisibility(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2078
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->AdjustHelpPosition(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2079
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->StartHelpAnimation(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2081
    # getter for: Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    invoke-static {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->access$700(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doScrollInEditMode(I)V

    goto :goto_0

    .line 2085
    :pswitch_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->DONE:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setGuideState(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2086
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->DONE:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->AdjustHelpVisibility(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2087
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->DONE:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->StartHelpAnimation(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2089
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iget-object v1, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    const/4 v2, 0x4

    const-wide/16 v3, 0xdac

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 2093
    :pswitch_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->FINISH:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setGuideState(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2094
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->finish()V

    goto :goto_0

    .line 2065
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
