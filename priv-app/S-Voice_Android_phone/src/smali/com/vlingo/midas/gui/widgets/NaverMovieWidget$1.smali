.class Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$1;
.super Ljava/lang/Object;
.source "NaverMovieWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 189
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;

    iget-object v3, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v3}, Lcom/vlingo/midas/naver/NaverMovieItem;->getPosterUrl()Ljava/lang/String;

    move-result-object v2

    .line 193
    .local v2, "url":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 194
    .local v1, "picURL":Ljava/net/URL;
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v4

    # setter for: Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mPosterImage:Landroid/graphics/Bitmap;
    invoke-static {v3, v4}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->access$002(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 201
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;)Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;->sendEmptyMessage(I)Z

    .line 203
    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    .end local v1    # "picURL":Ljava/net/URL;
    :goto_0
    return-void

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;)Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;->sendEmptyMessage(I)Z

    .line 206
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
