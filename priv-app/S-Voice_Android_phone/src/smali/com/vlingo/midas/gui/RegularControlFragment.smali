.class public Lcom/vlingo/midas/gui/RegularControlFragment;
.super Lcom/vlingo/midas/gui/RegularControlFragmentBase;
.source "RegularControlFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;
.implements Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/RegularControlFragment$OnChangeOffPortListener;,
        Lcom/vlingo/midas/gui/RegularControlFragment$OnChangePortListener;
    }
.end annotation


# instance fields
.field LOG_TAG:Ljava/lang/String;

.field private onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

.field private onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;-><init>()V

    .line 39
    const-string/jumbo v0, "RegularControlFragment"

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->LOG_TAG:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 44
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 231
    return-void
.end method


# virtual methods
.method public OnCoverStateChange(Z)V
    .locals 2
    .param p1, "isCoverOpen"    # Z

    .prologue
    .line 248
    if-eqz p1, :cond_1

    .line 249
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->setMicStatusText()V

    .line 250
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->toggle_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->toggle_btn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->removeMicStatusText()V

    .line 255
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->toggle_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->toggle_btn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method getThninkingBtnOver()I
    .locals 1

    .prologue
    .line 193
    sget v0, Lcom/vlingo/midas/R$drawable;->btn_overlay_thinking:I

    return v0
.end method

.method getThninkingMicAnimationList()I
    .locals 1

    .prologue
    .line 188
    sget v0, Lcom/vlingo/midas/R$xml;->mic_thinking_animation_list:I

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 99
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->onActivityCreated(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->control_full_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->fullContainer:Landroid/widget/RelativeLayout;

    .line 103
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->menu_layout_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->layoutMenuBtn:Landroid/widget/LinearLayout;

    .line 107
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 108
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->mic_btn_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->layoutMicBtn:Landroid/widget/LinearLayout;

    .line 109
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->mic_btn_layout_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    .line 110
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->mainControlLL:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->mainControlLL:Landroid/widget/LinearLayout;

    .line 111
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->mic_status_txt:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->micStatusText:Landroid/widget/TextView;

    .line 112
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_toggle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->toggle_btn:Landroid/widget/ImageView;

    .line 113
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->layoutMicBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 124
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 125
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/MicViewKK;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 126
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_mic_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->reco_listeningMic:Landroid/widget/ImageView;

    .line 127
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_backlayer:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/MicViewKK;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->reco_listeningBtnBackLayer:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 129
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->hour_glass_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->mHourGlassImg:Landroid/widget/ImageView;

    .line 130
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_idle_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->reco_idleBtn:Landroid/widget/ImageView;

    .line 131
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->mic_idle_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->reco_idleMic:Landroid/widget/ImageView;

    .line 132
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking_kitkat_bg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    .line 133
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking_kitkat_bg1:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->reco_thinkingBtnBg1:Landroid/widget/ImageButton;

    .line 142
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v0, :cond_1

    .line 143
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "manually_prompt_on_in_talkback"

    invoke-static {v0, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 145
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/RegularControlFragment;->onPromptOnClick(Z)V

    .line 149
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-nez v0, :cond_2

    .line 150
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_menu_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->menuBtn_Kitkat:Landroid/widget/ImageView;

    .line 153
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragment;->setmActivityCreated(Z)V

    .line 154
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->setNormalPanel()V

    .line 158
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    if-nez v0, :cond_3

    .line 159
    new-instance v0, Lcom/vlingo/midas/gui/RegularControlFragment$OnChangePortListener;

    invoke-direct {v0, p0, v4}, Lcom/vlingo/midas/gui/RegularControlFragment$OnChangePortListener;-><init>(Lcom/vlingo/midas/gui/RegularControlFragment;Lcom/vlingo/midas/gui/RegularControlFragment$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 160
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    if-nez v0, :cond_4

    .line 161
    new-instance v0, Lcom/vlingo/midas/gui/RegularControlFragment$OnChangeOffPortListener;

    invoke-direct {v0, p0, v4}, Lcom/vlingo/midas/gui/RegularControlFragment$OnChangeOffPortListener;-><init>(Lcom/vlingo/midas/gui/RegularControlFragment;Lcom/vlingo/midas/gui/RegularControlFragment$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 163
    :cond_4
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    if-eqz v0, :cond_5

    .line 164
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 165
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 172
    :cond_5
    :goto_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 173
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->fullContainer:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_6

    .line 174
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 175
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setFocusableInTouchMode(Z)V

    .line 178
    :cond_6
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->mainControlLL:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_7

    .line 179
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->mainControlLL:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 180
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->mainControlLL:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    .line 184
    :cond_7
    return-void

    .line 117
    :cond_8
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 119
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->layoutMenuBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 121
    :cond_9
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->layoutMenuBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 138
    :cond_a
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->reco_thinkingBtn:Landroid/widget/ImageView;

    .line 139
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_idle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->reco_idleBtn:Landroid/widget/ImageView;

    goto/16 :goto_1

    .line 167
    :cond_b
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    goto :goto_2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 198
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 200
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    if-eqz v0, :cond_0

    .line 201
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->onCreate(Landroid/os/Bundle;)V

    .line 76
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-static {p0}, Lcom/vlingo/midas/util/MiniModeUtils;->addListener(Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;)V

    .line 81
    invoke-static {p0}, Lcom/vlingo/midas/util/SViewCoverUtils;->addListener(Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;)V

    .line 82
    sget v1, Lcom/vlingo/midas/R$layout;->control_view:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 83
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/vlingo/midas/gui/RegularControlFragment$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/RegularControlFragment$1;-><init>(Lcom/vlingo/midas/gui/RegularControlFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 90
    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 210
    invoke-super {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->onDestroy()V

    .line 211
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    if-eqz v0, :cond_0

    .line 212
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->removeListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 213
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->removeListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 215
    :cond_0
    invoke-static {p0}, Lcom/vlingo/midas/util/MiniModeUtils;->removeListener(Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;)V

    .line 216
    invoke-static {p0}, Lcom/vlingo/midas/util/SViewCoverUtils;->removeListener(Lcom/vlingo/midas/util/SViewCoverUtils$OnSViewCoverStateChangeListener;)V

    .line 217
    iput-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 218
    iput-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 219
    return-void
.end method

.method public onMiniModeChange(Z)V
    .locals 0
    .param p1, "isMiniMode"    # Z

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->setMicStatusText()V

    .line 243
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/RegularControlFragment;->setToggleBtnVisiblity(Z)V

    .line 244
    return-void
.end method

.method public onPromptOffClick(Z)V
    .locals 2
    .param p1, "manually"    # Z

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getVoicePrompt()Lcom/vlingo/core/internal/settings/VoicePrompt;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 61
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v0

    if-nez v0, :cond_2

    .line 62
    if-eqz p1, :cond_0

    .line 63
    const-string/jumbo v0, "manually_prompt_on_in_talkback"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 65
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->setManually(Z)V

    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    if-nez v0, :cond_1

    .line 67
    new-instance v0, Lcom/vlingo/midas/gui/RegularControlFragment$OnChangeOffPortListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/gui/RegularControlFragment$OnChangeOffPortListener;-><init>(Lcom/vlingo/midas/gui/RegularControlFragment;Lcom/vlingo/midas/gui/RegularControlFragment$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 68
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeOffListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/settings/VoicePrompt;->on()V

    .line 70
    :cond_2
    return-void
.end method

.method public onPromptOnClick(Z)V
    .locals 2
    .param p1, "manually"    # Z

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragment;->getVoicePrompt()Lcom/vlingo/core/internal/settings/VoicePrompt;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 49
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    const-string/jumbo v0, "manually_prompt_on_in_talkback"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 51
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->setManually(Z)V

    .line 52
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/vlingo/midas/gui/RegularControlFragment$OnChangePortListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/gui/RegularControlFragment$OnChangePortListener;-><init>(Lcom/vlingo/midas/gui/RegularControlFragment;Lcom/vlingo/midas/gui/RegularControlFragment$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 54
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragment;->onChangeListenerPort:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/settings/VoicePrompt;->off()V

    .line 56
    :cond_1
    return-void
.end method
