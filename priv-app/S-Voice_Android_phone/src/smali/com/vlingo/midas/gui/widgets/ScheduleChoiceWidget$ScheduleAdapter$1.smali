.class Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;
.super Ljava/lang/Object;
.source "ScheduleChoiceWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;I)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;

    iput p2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 186
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 187
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    const-string/jumbo v1, "choice"

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->val$position:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 191
    const-string/jumbo v2, "uri"

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;

    # getter for: Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mEvents:Ljava/util/List;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->access$100(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;)Ljava/util/List;

    move-result-object v1

    iget v3, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->val$position:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEventID()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    const-string/jumbo v2, "title"

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;

    # getter for: Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mEvents:Ljava/util/List;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->access$100(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;)Ljava/util/List;

    move-result-object v1

    iget v3, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->val$position:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    const-string/jumbo v2, "beginTime"

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;

    # getter for: Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mEvents:Ljava/util/List;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->access$100(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;)Ljava/util/List;

    move-result-object v1

    iget v3, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->val$position:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 194
    const-string/jumbo v2, "endTime"

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;

    # getter for: Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mEvents:Ljava/util/List;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->access$100(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;)Ljava/util/List;

    move-result-object v1

    iget v3, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->val$position:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 196
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 197
    return-void
.end method
