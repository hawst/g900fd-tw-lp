.class public Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;
.super Landroid/os/Handler;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ShadowThreadHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogFragment;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;)V
    .locals 0

    .prologue
    .line 3154
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 3158
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 3238
    :goto_0
    :pswitch_0
    return-void

    .line 3163
    :pswitch_1
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, v1, Lcom/vlingo/midas/gui/DialogFragment;->TopViewbounds:Landroid/graphics/Rect;

    .line 3164
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/DialogFragment;->getdialogScrollView()Landroid/widget/LinearLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3165
    .local v0, "v1":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 3167
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->TopViewbounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 3171
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->TopViewbounds:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v2, Lcom/vlingo/midas/gui/DialogFragment;->TopViewbounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->pxTop:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/DialogFragment;->access$2200(Lcom/vlingo/midas/gui/DialogFragment;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 3172
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->TopViewbounds:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v2, Lcom/vlingo/midas/gui/DialogFragment;->TopViewbounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->pxTop:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/DialogFragment;->access$2200(Lcom/vlingo/midas/gui/DialogFragment;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 3173
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, v1, Lcom/vlingo/midas/gui/DialogFragment;->scrollBoundsTop:Landroid/graphics/Rect;

    .line 3174
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v2, Lcom/vlingo/midas/gui/DialogFragment;->scrollBoundsTop:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 3175
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->scrollBoundsTop:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v2, Lcom/vlingo/midas/gui/DialogFragment;->TopViewbounds:Landroid/graphics/Rect;

    invoke-static {v1, v2}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/DialogFragment;->getdialogScrollView()Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_3

    .line 3178
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # invokes: Lcom/vlingo/midas/gui/DialogFragment;->hideShadowTop()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$2300(Lcom/vlingo/midas/gui/DialogFragment;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3188
    .end local v0    # "v1":Landroid/view/View;
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iput-object v4, v1, Lcom/vlingo/midas/gui/DialogFragment;->TopViewbounds:Landroid/graphics/Rect;

    .line 3189
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iput-object v4, v1, Lcom/vlingo/midas/gui/DialogFragment;->scrollBoundsTop:Landroid/graphics/Rect;

    goto/16 :goto_0

    .line 3182
    .restart local v0    # "v1":Landroid/view/View;
    :cond_3
    :try_start_1
    sget-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorial:Z

    if-nez v1, :cond_2

    .line 3183
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # invokes: Lcom/vlingo/midas/gui/DialogFragment;->addShadowTop()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$2400(Lcom/vlingo/midas/gui/DialogFragment;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 3186
    .end local v0    # "v1":Landroid/view/View;
    :catch_0
    move-exception v1

    goto :goto_1

    .line 3194
    :pswitch_2
    :try_start_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, v1, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    .line 3195
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->ShadowView:Landroid/view/View;

    .line 3196
    .restart local v0    # "v1":Landroid/view/View;
    if-eqz v0, :cond_4

    .line 3198
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 3200
    :cond_4
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-boolean v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->setShadowWhileNotAnim:Z

    if-eqz v1, :cond_6

    .line 3204
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v2, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->pxDownNotAnim:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/DialogFragment;->access$2500(Lcom/vlingo/midas/gui/DialogFragment;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 3205
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v2, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->pxDownNotAnim:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/DialogFragment;->access$2500(Lcom/vlingo/midas/gui/DialogFragment;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 3214
    :goto_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, v1, Lcom/vlingo/midas/gui/DialogFragment;->scrollBoundsBottom:Landroid/graphics/Rect;

    .line 3215
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v2, Lcom/vlingo/midas/gui/DialogFragment;->scrollBoundsBottom:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 3216
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->scrollBoundsBottom:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v2, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    invoke-static {v1, v2}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 3219
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/DialogFragment;->hideShadowBottom()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 3229
    .end local v0    # "v1":Landroid/view/View;
    :cond_5
    :goto_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iput-object v4, v1, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    .line 3230
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iput-object v4, v1, Lcom/vlingo/midas/gui/DialogFragment;->scrollBoundsBottom:Landroid/graphics/Rect;

    goto/16 :goto_0

    .line 3211
    .restart local v0    # "v1":Landroid/view/View;
    :cond_6
    :try_start_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v2, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->pxDownAnim:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/DialogFragment;->access$2600(Lcom/vlingo/midas/gui/DialogFragment;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 3212
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v2, Lcom/vlingo/midas/gui/DialogFragment;->BottomViewbounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->pxDownAnim:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/DialogFragment;->access$2600(Lcom/vlingo/midas/gui/DialogFragment;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 3227
    .end local v0    # "v1":Landroid/view/View;
    :catch_1
    move-exception v1

    goto :goto_3

    .line 3223
    .restart local v0    # "v1":Landroid/view/View;
    :cond_7
    if-eqz v0, :cond_5

    sget-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorial:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->shouldContinueBelow:I

    if-nez v1, :cond_5

    .line 3224
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # invokes: Lcom/vlingo/midas/gui/DialogFragment;->addShadowBottom()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$2700(Lcom/vlingo/midas/gui/DialogFragment;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    .line 3234
    .end local v0    # "v1":Landroid/view/View;
    :pswitch_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/DialogFragment;->setShadowBottom()V

    goto/16 :goto_0

    .line 3158
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
