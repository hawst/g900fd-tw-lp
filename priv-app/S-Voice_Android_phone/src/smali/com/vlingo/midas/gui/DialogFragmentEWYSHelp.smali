.class public Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
.super Lcom/vlingo/midas/gui/ContentFragment;
.source "DialogFragmentEWYSHelp.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
.implements Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;
.implements Lcom/vlingo/midas/gui/OnEditListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$5;,
        Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$DialogFragmentHandler;,
        Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;,
        Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;,
        Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;,
        Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;,
        Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;,
        Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MyDummyView;,
        Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$TabletType;
    }
.end annotation


# static fields
.field private static final LANDSCAPE_MODE:I = 0x1

.field public static LOG_TAG:Ljava/lang/String; = null

.field private static final MAX_DUMMY_COUNT:I = 0x5

.field private static final MAX_RESUME_CONTROL_POLLS:I = 0x3

.field private static final MSG_ADD_SAVED_TO_MEMO_BUBBLE:I = 0x2

.field private static final POTRAIT_MODE:I = 0x0

.field private static final REMAIN_DUMMY_COUNT:I = 0x2

.field private static final RESUME_CONTOL_POLL_DURATION:I = 0x2ee

.field public static final SCROLL_TO_BOTTOM:I = 0x1

.field public static final SCROLL_TO_TOP:I

.field public static mHelpAnimLayoutLand:Landroid/widget/LinearLayout;

.field public static mHelpAnimLayoutPtrt:Landroid/widget/LinearLayout;

.field public static mHelpEditBubbleLand:Landroid/widget/TextView;

.field public static mHelpEditBubblePtrt:Landroid/widget/TextView;

.field public static mHelpEditLandVisibility:I

.field public static mHelpEditPtrtVisibility:I

.field static mNewTxt:Ljava/lang/String;

.field public static prevHelpEditLandVisibility:I

.field public static prevHelpEditPtrtVisibility:I

.field private static tts:Landroid/speech/tts/TextToSpeech;

.field private static viewOrientation:I


# instance fields
.field private final ANIMATE_FLIP_UP_DOWN_DURATION:I

.field public DBG:Z

.field private buffer:Landroid/view/View;

.field private currentTablet:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$TabletType;

.field private dialogScrollContent:Landroid/widget/LinearLayout;

.field private dialogScrollView:Landroid/widget/ScrollView;

.field ewysHelpActivity:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

.field private fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

.field isBtnAndWakeBubble:Z

.field private isEditStarted:Z

.field private isFullScreen:Z

.field private isWolfWidgetExist:Z

.field private lastBubbleIsGreeting:Z

.field private mActivityCreated:Z

.field private mHandler:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$DialogFragmentHandler;

.field private mHelpWidgetExist:Z

.field private mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

.field private mTimer:Ljava/util/Timer;

.field private menuVar:Z

.field private moveListBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private openCloseThreshold:F

.field private pullTab:Landroid/widget/ImageView;

.field private screenDensityDPI:F

.field private timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

.field private trackingTouch:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    const-string/jumbo v0, "DialogFragmentEWYSHelp"

    sput-object v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    .line 94
    sput v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->viewOrientation:I

    .line 113
    sput v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mHelpEditPtrtVisibility:I

    .line 114
    sput v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mHelpEditLandVisibility:I

    .line 115
    sput v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->prevHelpEditPtrtVisibility:I

    .line 116
    sput v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->prevHelpEditLandVisibility:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ContentFragment;-><init>()V

    .line 70
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->DBG:Z

    .line 80
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->menuVar:Z

    .line 88
    const/16 v0, 0x12c

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->ANIMATE_FLIP_UP_DOWN_DURATION:I

    .line 97
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mHelpWidgetExist:Z

    .line 98
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isWolfWidgetExist:Z

    .line 99
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isBtnAndWakeBubble:Z

    .line 101
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    .line 104
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isEditStarted:Z

    .line 122
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$TabletType;->OTHERS:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->currentTablet:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$TabletType;

    .line 1965
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$DialogFragmentHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$DialogFragmentHandler;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mHandler:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$DialogFragmentHandler;

    return-void
.end method

.method static synthetic access$1000(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$1100()Landroid/speech/tts/TextToSpeech;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->tts:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method static synthetic access$1102(Landroid/speech/tts/TextToSpeech;)Landroid/speech/tts/TextToSpeech;
    .locals 0
    .param p0, "x0"    # Landroid/speech/tts/TextToSpeech;

    .prologue
    .line 66
    sput-object p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->tts:Landroid/speech/tts/TextToSpeech;

    return-object p0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .param p1, "x1"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doScrollAnim(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;F)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .param p1, "x1"    # F

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->setDialogHeight(F)V

    return-void
.end method

.method static synthetic access$702(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isFullScreen:Z

    return p1
.end method

.method static synthetic access$900(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    return-object v0
.end method

.method static synthetic access$902(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    return-object p1
.end method

.method private animateDialogExpand()V
    .locals 6

    .prologue
    .line 1284
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRefreshRate()F

    move-result v2

    .line 1286
    .local v2, "refreshRate":F
    const v0, 0x3e99999a    # 0.3f

    mul-float v3, v0, v2

    .line 1288
    .local v3, "frames":F
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1289
    .local v4, "initialHeight":I
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->getBottom()I

    move-result v0

    sub-int/2addr v0, v4

    int-to-float v0, v0

    div-float v5, v0, v3

    .line 1291
    .local v5, "delta":F
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;FFIF)V

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->start()V

    .line 1293
    return-void
.end method

.method private checkOrientation()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 310
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    if-ne v0, v2, :cond_0

    .line 311
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->viewOrientation:I

    .line 316
    :goto_0
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->setHelpAnimResources()V

    .line 317
    return-void

    .line 313
    :cond_0
    sput v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->viewOrientation:I

    goto :goto_0
.end method

.method private doScrollAnim(I)V
    .locals 2
    .param p1, "height"    # I

    .prologue
    .line 978
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    new-instance v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$3;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$3;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 984
    return-void
.end method

.method private getReservedHeight()I
    .locals 1

    .prologue
    .line 1325
    const/4 v0, 0x0

    return v0
.end method

.method private helpScrollAnim()V
    .locals 1

    .prologue
    .line 987
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V

    .line 988
    .local v0, "anim":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->start()V

    .line 989
    return-void
.end method

.method private isEnableASREditing(Ljava/lang/String;)Z
    .locals 3
    .param p1, "Lang"    # Ljava/lang/String;

    .prologue
    .line 1968
    const-string/jumbo v1, "All"

    const-string/jumbo v2, "All"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1969
    const/4 v0, 0x1

    .line 1973
    :goto_0
    return v0

    .line 1970
    :cond_0
    const-string/jumbo v1, "asr.editing.enabled.languages"

    const-string/jumbo v2, "All"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 1973
    .local v0, "enableASREdting":Z
    goto :goto_0
.end method

.method private isEqual(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "stringID"    # I

    .prologue
    .line 1367
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1368
    const/4 v0, 0x1

    .line 1370
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeOldContent()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 1156
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    .line 1161
    .local v5, "numViews":I
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v8}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v8

    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v9

    if-le v8, v9, :cond_0

    const/4 v3, 0x1

    .line 1165
    .local v3, "minTotalViewHeight":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 1166
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1167
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1187
    :goto_1
    return-void

    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v3    # "minTotalViewHeight":Z
    :cond_0
    move v3, v7

    .line 1161
    goto :goto_0

    .line 1171
    .restart local v3    # "minTotalViewHeight":Z
    :cond_1
    const/4 v0, 0x0

    .line 1172
    .local v0, "height":I
    add-int/lit8 v1, v5, -0x1

    .local v1, "i":I
    :goto_2
    if-ltz v1, :cond_2

    .line 1173
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1174
    .local v6, "v":Landroid/view/View;
    invoke-virtual {v6, v7, v7}, Landroid/view/View;->measure(II)V

    .line 1175
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v0, v8

    .line 1172
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 1178
    .end local v6    # "v":Landroid/view/View;
    :cond_2
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1179
    .restart local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v7}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v7

    sub-int v4, v7, v0

    .line 1181
    .local v4, "newHeight":I
    if-gez v4, :cond_3

    .line 1182
    const/16 v7, 0x28

    iput v7, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1186
    :goto_3
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 1184
    :cond_3
    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_3
.end method

.method private setDialogHeight(F)V
    .locals 2
    .param p1, "y"    # F

    .prologue
    .line 1222
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    new-instance v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$4;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$4;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;F)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 1230
    return-void
.end method

.method private setHelpAnimResources()V
    .locals 0

    .prologue
    .line 343
    return-void
.end method

.method private submitTextRequest(I)V
    .locals 4
    .param p1, "strId"    # I

    .prologue
    .line 147
    sget-object v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "submitTextRequest()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    sget-object v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "text = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getTag()Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "dialogState":Ljava/lang/String;
    sget-object v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "dialogState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->startUserFlow(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 155
    return-void
.end method


# virtual methods
.method public addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;
    .locals 12
    .param p1, "type"    # Lcom/vlingo/midas/gui/DialogBubble$BubbleType;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "fillScreen"    # Z
    .param p4, "isGreetingHack"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v11, 0x0

    .line 423
    if-eqz p4, :cond_0

    iget-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->lastBubbleIsGreeting:Z

    if-nez v9, :cond_1

    :cond_0
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-nez v9, :cond_2

    .line 564
    :cond_1
    :goto_0
    return-object v2

    .line 428
    :cond_2
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_4

    .line 429
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x2

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 432
    .local v5, "myView":Landroid/view/View;
    instance-of v9, v5, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MyDummyView;

    if-eqz v9, :cond_3

    .line 433
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x3

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 435
    :cond_3
    instance-of v9, v5, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v9, :cond_4

    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne p1, v9, :cond_4

    move-object v9, v5

    .line 437
    check-cast v9, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v9

    sget-object v10, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v9, v10, :cond_4

    move-object v9, v5

    .line 438
    check-cast v9, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v9, p1, p2}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    .line 439
    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 444
    .end local v5    # "myView":Landroid/view/View;
    :cond_4
    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne p1, v9, :cond_5

    iget-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    if-eqz v9, :cond_5

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 449
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x2

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    instance-of v9, v9, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MyDummyView;

    if-nez v9, :cond_5

    .line 450
    new-instance v9, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MyDummyView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MyDummyView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->addDummyView(Landroid/view/View;)V

    .line 453
    :cond_5
    move/from16 v0, p4

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->lastBubbleIsGreeting:Z

    .line 454
    const/4 v2, 0x0

    .line 456
    .local v2, "db":Lcom/vlingo/midas/gui/DialogBubble;
    :try_start_0
    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne p1, v9, :cond_6

    .line 457
    invoke-virtual {p0, p2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->replaceUserEditBubble(Ljava/lang/String;)Lcom/vlingo/midas/gui/DialogBubble;

    move-result-object v2

    .line 458
    if-eqz v2, :cond_6

    .line 459
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Lcom/vlingo/midas/gui/DialogBubble;->setReplaceAvailable(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 560
    :catch_0
    move-exception v3

    .line 561
    .local v3, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 463
    .end local v3    # "e":Ljava/lang/NullPointerException;
    :cond_6
    if-nez v2, :cond_7

    .line 464
    :try_start_1
    sget v6, Lcom/vlingo/midas/R$layout;->dialog_bubble:I

    .line 465
    .local v6, "resId":I
    sget-object v9, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$5;->$SwitchMap$com$vlingo$midas$gui$DialogBubble$BubbleType:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 478
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v6, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v2, v0

    .line 479
    invoke-virtual {v2, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnSlideTextListenr(Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;)V

    .line 484
    .end local v6    # "resId":I
    :cond_7
    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne p1, v9, :cond_b

    .line 485
    const/4 v8, 0x0

    .line 486
    .local v8, "view":Landroid/view/View;
    const/4 v1, 0x0

    .line 488
    .local v1, "countChild":I
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 490
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v1, :cond_9

    .line 491
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    instance-of v9, v8, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v9, :cond_8

    .line 492
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v9

    sget-object v10, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v9, v10, :cond_8

    .line 493
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/vlingo/midas/gui/DialogBubble;->setOnEditListener(Lcom/vlingo/midas/gui/OnEditListener;)V

    .line 494
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/vlingo/midas/gui/DialogBubble;->setEditable(Z)V

    .line 495
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/vlingo/midas/gui/DialogBubble;->setTalkbackString(Z)V

    .line 490
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 467
    .end local v1    # "countChild":I
    .end local v4    # "i":I
    .end local v8    # "view":Landroid/view/View;
    .restart local v6    # "resId":I
    :pswitch_0
    sget v6, Lcom/vlingo/midas/R$layout;->dialog_bubble_user_s:I

    .line 468
    goto :goto_1

    .line 472
    :pswitch_1
    sget v6, Lcom/vlingo/midas/R$layout;->dialog_bubble_vlingo_s:I

    .line 473
    goto :goto_1

    .line 475
    :pswitch_2
    sget v6, Lcom/vlingo/midas/R$layout;->dialog_bubble_wakeup_s:I

    goto :goto_1

    .line 500
    .end local v6    # "resId":I
    .restart local v1    # "countChild":I
    .restart local v4    # "i":I
    .restart local v8    # "view":Landroid/view/View;
    :cond_9
    invoke-virtual {v2, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnEditListener(Lcom/vlingo/midas/gui/OnEditListener;)V

    .line 501
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isAsrEditingEnabled()Z

    move-result v7

    .line 502
    .local v7, "supportedLang":Z
    invoke-virtual {v2, v7}, Lcom/vlingo/midas/gui/DialogBubble;->setEditable(Z)V

    .line 503
    invoke-virtual {v2, v7}, Lcom/vlingo/midas/gui/DialogBubble;->setTalkbackString(Z)V

    .line 504
    invoke-virtual {v2, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnSlideTextListenr(Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;)V

    .line 505
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->setTutorialMode()V

    .line 510
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x2

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 511
    instance-of v9, v8, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v9, :cond_b

    .line 512
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v9

    sget-object v10, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v9, v10, :cond_a

    .line 517
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x4

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 520
    :cond_a
    instance-of v9, v8, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v9, :cond_b

    .line 521
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v9

    sget-object v10, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v9, v10, :cond_b

    .line 525
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/DialogBubble;->isReplaceAvailable()Z

    move-result v9

    if-eqz v9, :cond_b

    .line 526
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v8}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 546
    .end local v1    # "countChild":I
    .end local v4    # "i":I
    .end local v7    # "supportedLang":Z
    .end local v8    # "view":Landroid/view/View;
    :cond_b
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v9}, Ljava/util/Queue;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 548
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v9, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 550
    invoke-virtual {v2, p1, p2}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    .line 551
    invoke-virtual {p0, v2, p3}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    goto/16 :goto_0

    .line 557
    :cond_c
    invoke-virtual {v2, p1, p2, p3}, Lcom/vlingo/midas/gui/DialogBubble;->saveParam(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;Z)V

    .line 558
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v9, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 465
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public addDummyView(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 699
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 700
    return-void
.end method

.method public addHelpChoiceWidget()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 572
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    if-nez v2, :cond_0

    .line 592
    :goto_0
    return-void

    .line 576
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 577
    .local v0, "dvfsLockIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.sec.android.intent.action.DVFS_BOOSTER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 578
    const-string/jumbo v2, "PACKAGE"

    const-string/jumbo v3, "com.vlingo.midas"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579
    const-string/jumbo v2, "DURATION"

    const-string/jumbo v3, "2000"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 580
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 582
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mHelpWidgetExist:Z

    if-eqz v2, :cond_1

    .line 583
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->removeAlreadyExistingHelpScreen()V

    .line 586
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$layout;->widget_help_choice:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    .line 588
    .local v1, "v":Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;
    iput-boolean v5, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mHelpWidgetExist:Z

    .line 590
    invoke-virtual {p0, v1, v5}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 591
    invoke-static {v5}, Lcom/vlingo/midas/settings/MidasSettings;->setHelpVisible(Z)V

    goto :goto_0
.end method

.method public addHelpWidget(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 600
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    if-nez v2, :cond_0

    .line 614
    :goto_0
    return-void

    .line 604
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 605
    .local v0, "dvfsLockIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.sec.android.intent.action.DVFS_BOOSTER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 606
    const-string/jumbo v2, "PACKAGE"

    const-string/jumbo v3, "com.vlingo.midas"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 607
    const-string/jumbo v2, "DURATION"

    const-string/jumbo v3, "2000"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 608
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 610
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$layout;->widget_help:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/widgets/HelpWidget;

    .line 612
    .local v1, "v":Lcom/vlingo/midas/gui/widgets/HelpWidget;
    invoke-virtual {v1, p1}, Lcom/vlingo/midas/gui/widgets/HelpWidget;->setAddView(Landroid/content/Intent;)V

    .line 613
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public addMainPrompt()V
    .locals 5

    .prologue
    .line 1302
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getHomeMainPrompt()Ljava/lang/String;

    move-result-object v1

    .line 1305
    .local v1, "mainPrompt":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0, v2, v1, v3, v4}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    .line 1308
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    .line 1311
    .local v0, "cf":Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isActivityCreated()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1319
    :cond_0
    :goto_0
    return-void

    .line 1314
    :cond_1
    if-nez v0, :cond_0

    .line 1315
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->DBG:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "cf is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addTimerWidget()V
    .locals 4

    .prologue
    .line 631
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    if-nez v1, :cond_0

    .line 637
    :goto_0
    return-void

    .line 634
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$layout;->widget_timer:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;

    .line 636
    .local v0, "v":Lcom/vlingo/midas/gui/widgets/TimerWidget;
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public addTutorialWidget()V
    .locals 0

    .prologue
    .line 2003
    return-void
.end method

.method public addYouCanSayWidget()V
    .locals 4

    .prologue
    .line 618
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$layout;->widget_you_can_say:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;

    .line 620
    .local v0, "v":Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setAddView()V

    .line 621
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 622
    return-void
.end method

.method public cancelAsrEditing()V
    .locals 5

    .prologue
    .line 709
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    if-eqz v3, :cond_2

    .line 710
    const/4 v0, 0x0

    .line 711
    .local v0, "child":Landroid/view/View;
    const/4 v1, 0x0

    .line 712
    .local v1, "countChild":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_0

    .line 713
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .line 715
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 716
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v3, v0, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v3, :cond_1

    move-object v3, v0

    .line 717
    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v3

    sget-object v4, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v3, v4, :cond_1

    move-object v3, v0

    .line 718
    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->isUserTextReadyToTouch()Z

    move-result v3

    if-nez v3, :cond_1

    move-object v3, v0

    .line 721
    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->setUserTextReadyToTouch()V

    .line 715
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 727
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "countChild":I
    .end local v2    # "i":I
    :cond_2
    return-void
.end method

.method public checkWakeUpView(Landroid/view/View;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 915
    instance-of v0, p1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/vlingo/midas/gui/DialogBubble;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v0, v1, :cond_0

    .line 917
    const/4 v0, 0x1

    .line 919
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cleanupPreviousBubbles()V
    .locals 6

    .prologue
    .line 670
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    const/16 v5, 0xa

    if-le v4, v5, :cond_1

    .line 672
    const/4 v2, 0x0

    .line 673
    .local v2, "totalDummyCount":I
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 674
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 675
    .local v3, "v1":Landroid/view/View;
    instance-of v4, v3, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MyDummyView;

    if-eqz v4, :cond_0

    .line 676
    add-int/lit8 v2, v2, 0x1

    .line 677
    const/4 v4, 0x5

    if-lt v2, v4, :cond_0

    .line 678
    add-int/lit8 v1, v0, -0x2

    .local v1, "i1":I
    :goto_1
    if-ltz v1, :cond_1

    .line 679
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 678
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 673
    .end local v1    # "i1":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 686
    .end local v0    # "i":I
    .end local v2    # "totalDummyCount":I
    .end local v3    # "v1":Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected doAddView(Landroid/view/View;Z)V
    .locals 13
    .param p1, "v"    # Landroid/view/View;
    .param p2, "fillScreen"    # Z

    .prologue
    .line 741
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isAdded()Z

    move-result v9

    if-eqz v9, :cond_1c

    .line 742
    const/4 v1, 0x0

    .line 743
    .local v1, "btnPadding":I
    const/4 v5, 0x0

    .line 748
    .local v5, "isWolframFlag":Z
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->isFocusable()Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->isFocusableInTouchMode()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 749
    :cond_0
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setFocusable(Z)V

    .line 750
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 754
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->cancelAsrEditing()V

    .line 757
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->removeReallyWakeupBubble()V

    .line 758
    if-eqz p2, :cond_2

    iget-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isFullScreen:Z

    if-nez v9, :cond_2

    .line 759
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->animateDialogExpand()V

    .line 760
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isFullScreen:Z

    .line 763
    :cond_2
    instance-of v9, p1, Lcom/vlingo/midas/gui/Widget;

    if-eqz v9, :cond_8

    .line 764
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "NaverWidget"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_12

    .line 765
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isActivityCreated()Z

    move-result v9

    if-eqz v9, :cond_10

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v10, 0x2f0

    if-ne v9, v10, :cond_3

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v10, 0x500

    if-eq v9, v10, :cond_4

    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v10, 0x4d0

    if-ne v9, v10, :cond_10

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v10, 0x320

    if-ne v9, v10, :cond_10

    .line 771
    :cond_4
    const/4 v9, 0x0

    const/16 v10, 0xa

    const/4 v11, 0x0

    const/16 v12, 0x14

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/view/View;->setPadding(IIII)V

    .line 787
    :cond_5
    :goto_0
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    add-int/lit8 v4, v9, -0x1

    .local v4, "i":I
    :goto_1
    if-ltz v4, :cond_8

    .line 788
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 789
    .local v7, "v1":Landroid/view/View;
    instance-of v9, v7, Lcom/vlingo/midas/gui/Widget;

    if-eqz v9, :cond_13

    .line 790
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "ButtonWidget"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "WolframAlphaWidget"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 792
    const/4 v5, 0x1

    .line 793
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isWolfWidgetExist:Z

    .line 794
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v8

    .line 795
    .local v8, "v1Height":I
    div-int/lit8 v9, v8, 0x4

    sub-int v1, v8, v9

    .line 797
    .end local v8    # "v1Height":I
    :cond_6
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    check-cast v7, Lcom/vlingo/midas/gui/Widget;

    .end local v7    # "v1":Landroid/view/View;
    invoke-virtual {v7}, Lcom/vlingo/midas/gui/Widget;->isWidgetReplaceable()Z

    move-result v9

    if-nez v9, :cond_7

    move-object v9, p1

    check-cast v9, Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/Widget;->isPreviousSameWidgetReplaceable()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 800
    :cond_7
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v4}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 809
    .end local v4    # "i":I
    :cond_8
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->checkWakeUpView(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 810
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 811
    .local v2, "cnt":I
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    add-int/lit8 v10, v2, -0x3

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 812
    .restart local v7    # "v1":Landroid/view/View;
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "ButtonWidget"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 813
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isBtnAndWakeBubble:Z

    .line 817
    .end local v2    # "cnt":I
    .end local v7    # "v1":Landroid/view/View;
    :cond_9
    instance-of v9, p1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v9, :cond_c

    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_c

    .line 820
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isActivityCreated()Z

    move-result v9

    if-eqz v9, :cond_14

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v10, 0x2f0

    if-ne v9, v10, :cond_a

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v10, 0x500

    if-eq v9, v10, :cond_b

    :cond_a
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v10, 0x4d0

    if-ne v9, v10, :cond_14

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v10, 0x320

    if-ne v9, v10, :cond_14

    .line 825
    :cond_b
    const/4 v9, 0x0

    const/16 v10, 0xa

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/view/View;->setPadding(IIII)V

    .line 832
    :cond_c
    :goto_2
    instance-of v9, p1, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-eqz v9, :cond_f

    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_f

    .line 834
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isActivityCreated()Z

    move-result v9

    if-eqz v9, :cond_17

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v10, 0x2f0

    if-ne v9, v10, :cond_d

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v10, 0x500

    if-eq v9, v10, :cond_e

    :cond_d
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v10, 0x4d0

    if-ne v9, v10, :cond_17

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v10, 0x320

    if-ne v9, v10, :cond_17

    .line 839
    :cond_e
    const/4 v9, 0x0

    const/16 v10, 0xa

    const/4 v11, 0x0

    const/16 v12, 0x14

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/view/View;->setPadding(IIII)V

    .line 845
    :cond_f
    :goto_3
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v9, p1, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 847
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->removeOldContent()V

    .line 854
    instance-of v9, p1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MyDummyView;

    if-eqz v9, :cond_18

    .line 904
    .end local v1    # "btnPadding":I
    .end local v5    # "isWolframFlag":Z
    :goto_4
    return-void

    .line 773
    .restart local v1    # "btnPadding":I
    .restart local v5    # "isWolframFlag":Z
    :cond_10
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "ButtonWidget"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 775
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v10, 0x780

    if-ne v9, v10, :cond_11

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v10, 0x438

    if-ne v9, v10, :cond_11

    .line 778
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1c

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_0

    .line 780
    :cond_11
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1c

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_0

    .line 785
    :cond_12
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x14

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_0

    .line 787
    .restart local v4    # "i":I
    .restart local v7    # "v1":Landroid/view/View;
    :cond_13
    add-int/lit8 v4, v4, -0x1

    goto/16 :goto_1

    .line 826
    .end local v4    # "i":I
    .end local v7    # "v1":Landroid/view/View;
    :cond_14
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v9

    if-nez v9, :cond_15

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v9

    if-eqz v9, :cond_16

    .line 827
    :cond_15
    const/4 v9, 0x0

    const/16 v10, 0x1c

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_2

    .line 829
    :cond_16
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_2

    .line 841
    :cond_17
    const/4 v9, 0x0

    const/16 v10, 0x14

    const/4 v11, 0x0

    const/16 v12, 0x28

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_3

    .line 857
    :cond_18
    instance-of v9, p1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MyDummyView;

    if-nez v9, :cond_1b

    .line 858
    const/4 v3, 0x0

    .line 859
    .local v3, "height":I
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 860
    .local v6, "r":Landroid/graphics/Rect;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->checkWakeUpView(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_1b

    .line 861
    const/4 v7, 0x0

    .line 862
    .restart local v7    # "v1":Landroid/view/View;
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    add-int/lit8 v4, v9, -0x1

    .restart local v4    # "i":I
    :goto_5
    if-lez v4, :cond_1a

    .line 863
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 864
    instance-of v9, v7, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MyDummyView;

    if-nez v9, :cond_19

    instance-of v9, v7, Lcom/vlingo/midas/gui/widgets/ButtonWidget;

    if-nez v9, :cond_19

    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    if-eq v7, v9, :cond_19

    if-ne v7, p1, :cond_1a

    .line 862
    :cond_19
    add-int/lit8 v4, v4, -0x1

    goto :goto_5

    .line 873
    :cond_1a
    if-eqz v7, :cond_1b

    .line 874
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 877
    invoke-virtual {v7, v6}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 895
    .end local v3    # "height":I
    .end local v4    # "i":I
    .end local v6    # "r":Landroid/graphics/Rect;
    .end local v7    # "v1":Landroid/view/View;
    :cond_1b
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isWolfWidgetExist:Z

    .line 901
    .end local v1    # "btnPadding":I
    .end local v5    # "isWolframFlag":Z
    :goto_6
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V

    .line 902
    .local v0, "anim":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->start()V

    goto/16 :goto_4

    .line 898
    .end local v0    # "anim":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;
    :cond_1c
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    goto :goto_6
.end method

.method public doScroll()V
    .locals 4

    .prologue
    .line 929
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 930
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    new-instance v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$2;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 937
    :cond_0
    return-void
.end method

.method public doScrollInEditMode(I)V
    .locals 1
    .param p1, "direction"    # I

    .prologue
    .line 992
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;I)V

    .line 993
    .local v0, "est":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->start()V

    .line 994
    return-void
.end method

.method findLastBubbleByType(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;)Lcom/vlingo/midas/gui/DialogBubble;
    .locals 3
    .param p1, "type"    # Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    .prologue
    .line 1483
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_1

    .line 1484
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 1485
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1486
    .local v1, "v1":Landroid/view/View;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 1488
    check-cast v1, Lcom/vlingo/midas/gui/DialogBubble;

    .line 1491
    .end local v0    # "i":I
    .end local v1    # "v1":Landroid/view/View;
    :goto_1
    return-object v1

    .line 1484
    .restart local v0    # "i":I
    .restart local v1    # "v1":Landroid/view/View;
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1491
    .end local v0    # "i":I
    .end local v1    # "v1":Landroid/view/View;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isActivityCreated()Z
    .locals 1

    .prologue
    .line 1985
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x1

    const/4 v9, 0x0

    .line 216
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/ContentFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 218
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iput-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->ewysHelpActivity:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .line 222
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v7

    sget-object v8, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v7, v8, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v7

    sget-object v8, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v7, v8, :cond_1

    .line 223
    :cond_0
    sget-object v7, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$TabletType;

    iput-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->currentTablet:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$TabletType;

    .line 225
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v7, v7

    iput v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->screenDensityDPI:F

    .line 227
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$id;->dialog_full_container:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    iput-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    .line 228
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$id;->dialog_scroll_content:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    .line 229
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$id;->dialog_scrollview:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ScrollView;

    iput-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    .line 230
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$id;->pull_tab:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->pullTab:Landroid/widget/ImageView;

    .line 231
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    sget v8, Lcom/vlingo/midas/R$id;->buffer:I

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    .line 233
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v4

    .line 234
    .local v4, "languageApplication":Ljava/lang/String;
    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->fragmentLanguage:Ljava/lang/String;

    .line 239
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->checkOrientation()V

    .line 241
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    new-instance v8, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$1;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$1;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    iput-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->trackingTouch:Z

    .line 250
    const/4 v7, 0x0

    iput v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->openCloseThreshold:F

    .line 251
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    invoke-virtual {v7, p0}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 253
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->animateDialogExpand()V

    .line 254
    iput-boolean v11, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isFullScreen:Z

    .line 256
    const/4 v3, 0x0

    .line 257
    .local v3, "isNeedShowTimer":Z
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$id;->widget_timer:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/TimerWidget;

    iput-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    .line 259
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    if-eqz v7, :cond_2

    .line 260
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    invoke-virtual {v7, p1}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->onRestoreInstanceState(Landroid/os/Bundle;)Z

    move-result v3

    .line 262
    if-eqz v3, :cond_2

    .line 263
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->addTimerWidget()V

    .line 267
    :cond_2
    new-instance v7, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$1;)V

    iput-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->moveListBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 268
    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v7, "LIST_MOVE_ACTION"

    invoke-direct {v2, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 269
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v7, "LIST_MOVE_ACTION"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 270
    const-string/jumbo v7, "com.vlingo.LANGUAGE_CHANGED"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 271
    const-string/jumbo v7, "SHOW_LIST_ACTION"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 272
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->moveListBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v7, v8, v2}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 275
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v7

    sget-object v8, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v7, v8, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->registerTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 288
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->prepareBubble()V

    .line 290
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    sget v8, Lcom/vlingo/midas/R$id;->prompt_user_textview:I

    invoke-virtual {v7, v8}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 291
    .local v6, "v":Landroid/view/View;
    if-eqz v6, :cond_3

    .line 293
    :try_start_0
    const-string/jumbo v7, "android.widget.EditText"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 294
    .local v0, "EditTextClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v7, "setWritingBuddyEnabled"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    sget-object v10, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v10, v8, v9

    invoke-virtual {v0, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 295
    .local v5, "setHandWritingDisabled":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    .end local v0    # "EditTextClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5    # "setHandWritingDisabled":Ljava/lang/reflect/Method;
    :cond_3
    :goto_0
    iput-boolean v11, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    .line 306
    return-void

    .line 296
    :catch_0
    move-exception v1

    .line 297
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v7, "EditWhatYouSaidHelpActivity"

    const-string/jumbo v8, "handwriting buddy exception caught"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x1

    .line 949
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/ContentFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 951
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_0

    .line 972
    :goto_0
    return-void

    .line 954
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->setDialogContent()V

    .line 956
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v1, v2, Landroid/content/res/Configuration;->orientation:I

    .line 957
    .local v1, "mOrientation":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    if-ne v1, v3, :cond_1

    .line 958
    const/4 v2, 0x0

    sput v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->viewOrientation:I

    .line 965
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isEnableASREditing(Ljava/lang/String;)Z

    move-result v0

    .line 972
    .local v0, "enableASREdting":Z
    goto :goto_0

    .line 960
    .end local v0    # "enableASREdting":Z
    :cond_1
    sput v3, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->viewOrientation:I

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 138
    sget v1, Lcom/vlingo/midas/R$layout;->dialog_view_ewys_help:I

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 140
    .local v0, "v":Landroid/view/View;
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isFullScreen:Z

    .line 142
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 359
    invoke-super {p0}, Lcom/vlingo/midas/gui/ContentFragment;->onDestroy()V

    .line 360
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    .line 361
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isEditStarted:Z

    .line 362
    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->setHelpVisible(Z)V

    .line 364
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->moveListBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 365
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->moveListBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 368
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v0, v1, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 370
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v0, v1, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 372
    return-void
.end method

.method public onEditCanceled(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 1868
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .line 1869
    .local v1, "ewysHelpActivity":Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->onEditCanceled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1900
    :cond_0
    :goto_0
    return-void

    .line 1872
    :cond_1
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    if-eqz v2, :cond_3

    .line 1873
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    .line 1877
    .local v0, "cf":Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isActivityCreated()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1879
    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 1883
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->removeOldContent()V

    .line 1886
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1887
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 1888
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setFocusable(Z)V

    .line 1897
    .end local v0    # "cf":Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
    :cond_3
    if-eqz v1, :cond_0

    .line 1898
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->showControlFragment()V

    goto :goto_0
.end method

.method public onEditFinished(Landroid/view/View;)V
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1719
    const/4 v7, 0x0

    .line 1721
    .local v7, "newTxt":Ljava/lang/String;
    iget-boolean v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    if-nez v10, :cond_1

    .line 1859
    :cond_0
    :goto_0
    return-void

    .line 1724
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .line 1725
    .local v5, "ewysHelpActivity":Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getCurrGuideState()Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    move-result-object v10

    sget-object v11, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    if-ne v10, v11, :cond_0

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->onEditFinished()Z

    move-result v10

    if-nez v10, :cond_0

    :cond_2
    move-object v10, p1

    .line 1730
    check-cast v10, Lcom/vlingo/midas/gui/DialogBubble;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/vlingo/midas/gui/DialogBubble;->setEditable(Z)V

    .line 1731
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isEditStarted:Z

    .line 1752
    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v6, v10, -0x1

    .local v6, "i":I
    :goto_1
    if-ltz v6, :cond_5

    .line 1753
    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1760
    .local v1, "child":Landroid/view/View;
    instance-of v10, v1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v10, :cond_3

    move-object v10, v1

    .line 1761
    check-cast v10, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v10

    sget-object v11, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v10, v11, :cond_4

    .line 1752
    .end local v1    # "child":Landroid/view/View;
    :cond_3
    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    .line 1766
    .restart local v1    # "child":Landroid/view/View;
    :cond_4
    check-cast v1, Lcom/vlingo/midas/gui/DialogBubble;

    .end local v1    # "child":Landroid/view/View;
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v10

    sget-object v11, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v10, v11, :cond_3

    move-object v2, p1

    .line 1769
    check-cast v2, Lcom/vlingo/midas/gui/DialogBubble;

    .line 1772
    .local v2, "db":Lcom/vlingo/midas/gui/DialogBubble;
    const-string/jumbo v10, "%s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mNewTxt:Ljava/lang/String;

    .line 1773
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->replaceOriginalText()V

    .line 1800
    .end local v2    # "db":Lcom/vlingo/midas/gui/DialogBubble;
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$layout;->dialog_bubble_user_s:I

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v3, v10

    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    .line 1801
    .local v3, "db1":Lcom/vlingo/midas/gui/DialogBubble;
    invoke-virtual {v3, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnSlideTextListenr(Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;)V

    .line 1802
    sget-object v10, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    sget-object v11, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mNewTxt:Ljava/lang/String;

    invoke-virtual {v3, v10, v11}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    .line 1803
    const/4 v10, 0x0

    invoke-virtual {p0, v3, v10}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 1813
    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mHandler:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$DialogFragmentHandler;

    const/4 v11, 0x2

    const-wide/16 v12, 0x7d0

    invoke-virtual {v10, v11, v12, v13}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$DialogFragmentHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1816
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->removeOldContent()V

    .line 1817
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isEditStarted:Z

    .line 1819
    const/4 v8, 0x0

    .line 1820
    .local v8, "nextViewTotalHeight":I
    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v11, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v11

    add-int/lit8 v11, v11, -0x2

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 1821
    .local v9, "view":Landroid/view/View;
    instance-of v10, v9, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v10, :cond_6

    move-object v10, v9

    .line 1822
    check-cast v10, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v10

    sget-object v11, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v10, v11, :cond_6

    .line 1823
    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v11, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v11

    add-int/lit8 v11, v11, -0x3

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 1827
    :cond_6
    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1828
    .local v0, "b":I
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v10

    sub-int v8, v0, v10

    .line 1830
    const/16 v4, 0x320

    .line 1831
    .local v4, "delay":I
    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v10}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v10

    if-le v8, v10, :cond_7

    .line 1834
    const/16 v4, 0x514

    .line 1854
    :cond_7
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v10

    if-nez v10, :cond_0

    .line 1855
    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 1856
    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/view/View;->setFocusable(Z)V

    .line 1857
    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0
.end method

.method public onEditStarted(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1668
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z

    if-eqz v2, :cond_0

    .line 1673
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .line 1674
    .local v1, "ewysHelpActivity":Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->onEditStarted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1692
    .end local v1    # "ewysHelpActivity":Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    :cond_0
    :goto_0
    return-void

    .line 1678
    .restart local v1    # "ewysHelpActivity":Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->removeWakeupBubble()V

    .line 1680
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    .line 1684
    .local v0, "cf":Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isActivityCreated()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1685
    sget-object v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "(cf != null && cf.isActivityCreated())"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1687
    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 1690
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isEditStarted:Z

    goto :goto_0
.end method

.method protected onIdle()V
    .locals 0

    .prologue
    .line 1982
    return-void
.end method

.method public onLanguageChanged()V
    .locals 3

    .prologue
    .line 646
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->fragmentLanguage:Ljava/lang/String;

    .line 650
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->resetAllContent()V

    .line 654
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 655
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 656
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 658
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 347
    invoke-super {p0}, Lcom/vlingo/midas/gui/ContentFragment;->onPause()V

    .line 348
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 351
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 1633
    invoke-super {p0}, Lcom/vlingo/midas/gui/ContentFragment;->onResume()V

    .line 1634
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/midas/util/MiniModeUtils;->setSVoiceMiniMode(Z)V

    .line 1635
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->setDialogContent()V

    .line 1636
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 381
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/ContentFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 382
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->widget_timer:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    .line 383
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 386
    :cond_0
    return-void
.end method

.method public onSlideTextEnd(Ljava/lang/String;)V
    .locals 5
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 1384
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1386
    .local v0, "db":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1389
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 1392
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1394
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1396
    .local v1, "newDb":Landroid/view/View;
    instance-of v2, v1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v2, :cond_1

    .line 1398
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v2, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-object v2, v1

    .line 1401
    check-cast v2, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v3, v1

    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v4

    move-object v3, v1

    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    move-object v2, v1

    .line 1404
    check-cast v2, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->isFillScreen()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 1413
    .end local v1    # "newDb":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 1408
    .restart local v1    # "newDb":Landroid/view/View;
    :cond_1
    if-eqz v1, :cond_0

    .line 1409
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public onSlideTextStart(Ljava/lang/String;)V
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 1424
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1426
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/vlingo/midas/gui/Widget;

    if-eqz v1, :cond_0

    .line 1428
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1429
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1430
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 1433
    .end local v0    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onTaskWaitingToStart(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    .locals 6
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;
    .param p2, "control"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 399
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    monitor-enter v1

    .line 400
    :try_start_0
    iput-object p2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .line 401
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mTimer:Ljava/util/Timer;

    .line 403
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$1;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x2ee

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 405
    return-void

    .line 401
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 1198
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1199
    .local v0, "action":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 1200
    .local v1, "y":F
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1201
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->help_invalid_action:I

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1202
    :cond_0
    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->pullTab:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTop()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->pullTab:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getBottom()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v1, v2

    if-gez v2, :cond_2

    .line 1204
    iput-boolean v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->trackingTouch:Z

    .line 1205
    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->openCloseThreshold:F

    .line 1206
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->setDialogHeight(F)V

    .line 1218
    :cond_1
    :goto_0
    return v4

    .line 1207
    :cond_2
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->trackingTouch:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_3

    if-ne v0, v4, :cond_5

    .line 1209
    :cond_3
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->trackingTouch:Z

    .line 1210
    iget v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->openCloseThreshold:F

    cmpl-float v2, v1, v2

    if-lez v2, :cond_4

    .line 1211
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->animateDialogExpand()V

    goto :goto_0

    .line 1212
    :cond_4
    iget v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->openCloseThreshold:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_1

    .line 1213
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 1215
    :cond_5
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->trackingTouch:Z

    if-eqz v2, :cond_1

    .line 1216
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->setDialogHeight(F)V

    goto :goto_0
.end method

.method protected onUserCancel()V
    .locals 0

    .prologue
    .line 1978
    return-void
.end method

.method public prepareBubble()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 160
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->dialog_bubble_vlingo_s:I

    invoke-static {v3, v4, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    .line 162
    .local v0, "db":Lcom/vlingo/midas/gui/DialogBubble;
    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnSlideTextListenr(Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;)V

    .line 163
    sget-object v3, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getHomeMainPrompt()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0, v0, v6}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 168
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->dialog_bubble_user_s:I

    invoke-static {v3, v4, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v0, v3

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    .line 171
    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnEditListener(Lcom/vlingo/midas/gui/OnEditListener;)V

    .line 173
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isAsrEditingEnabled()Z

    move-result v1

    .line 174
    .local v1, "supportedLang":Z
    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogBubble;->setEditable(Z)V

    .line 175
    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogBubble;->setTalkbackString(Z)V

    .line 176
    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnSlideTextListenr(Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;)V

    .line 178
    sget-object v3, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->guide_ewys_memo_prompt:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    .line 179
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogBubble;->setTutorialMode()V

    .line 180
    invoke-virtual {p0, v0, v6}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 198
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->widget_memo:I

    invoke-static {v3, v4, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 199
    .local v2, "v":Landroid/view/View;
    sget v3, Lcom/vlingo/midas/R$id;->msg_body:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    check-cast v3, Landroid/widget/TextView;

    sget v4, Lcom/vlingo/midas/R$string;->guide_ewys_memo_content:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 200
    invoke-virtual {p0, v2, v6}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 202
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->dialog_bubble_vlingo_s:I

    invoke-static {v3, v4, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .end local v0    # "db":Lcom/vlingo/midas/gui/DialogBubble;
    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    .line 204
    .restart local v0    # "db":Lcom/vlingo/midas/gui/DialogBubble;
    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnSlideTextListenr(Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;)V

    .line 205
    sget-object v3, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->core_car_tts_MEMO_SAVED:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    .line 206
    invoke-virtual {p0, v0, v6}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 208
    return-void
.end method

.method public removeAlreadyExistingHelpScreen()V
    .locals 3

    .prologue
    .line 1548
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 1549
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1550
    .local v1, "v1":Landroid/view/View;
    if-eqz v1, :cond_1

    instance-of v2, v1, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-eqz v2, :cond_1

    .line 1551
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1555
    .end local v1    # "v1":Landroid/view/View;
    :cond_0
    return-void

    .line 1548
    .restart local v1    # "v1":Landroid/view/View;
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public removeReallyWakeupBubble()V
    .locals 2

    .prologue
    .line 1515
    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->findLastBubbleByType(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;)Lcom/vlingo/midas/gui/DialogBubble;

    move-result-object v0

    .line 1516
    .local v0, "db":Lcom/vlingo/midas/gui/DialogBubble;
    if-eqz v0, :cond_0

    .line 1517
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1519
    :cond_0
    return-void
.end method

.method public removeWakeupBubble()V
    .locals 2

    .prologue
    .line 1501
    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->findLastBubbleByType(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;)Lcom/vlingo/midas/gui/DialogBubble;

    move-result-object v0

    .line 1502
    .local v0, "db":Lcom/vlingo/midas/gui/DialogBubble;
    if-eqz v0, :cond_0

    .line 1503
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogBubble;->setVisibility(I)V

    .line 1505
    :cond_0
    return-void
.end method

.method public replaceUserEditBubble(Ljava/lang/String;)Lcom/vlingo/midas/gui/DialogBubble;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1528
    const/4 v0, 0x0

    .line 1529
    .local v0, "db":Lcom/vlingo/midas/gui/DialogBubble;
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 1530
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1531
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_0

    instance-of v3, v2, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v3, :cond_0

    move-object v3, v2

    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->isReplaceAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 1533
    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    .line 1534
    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/DialogBubble;->setText(Ljava/lang/String;)V

    .line 1535
    sget-object v3, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {v0, v3}, Lcom/vlingo/midas/gui/DialogBubble;->setType(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;)V

    move-object v3, v0

    .line 1539
    .end local v2    # "v":Landroid/view/View;
    :goto_1
    return-object v3

    .line 1529
    .restart local v2    # "v":Landroid/view/View;
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 1539
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public resetAllContent()V
    .locals 3

    .prologue
    .line 1442
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1443
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 1446
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1447
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 1454
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-nez v1, :cond_2

    .line 1480
    :goto_0
    return-void

    .line 1456
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1459
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1460
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getReservedHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1461
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1462
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->buffer:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1464
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->lastBubbleIsGreeting:Z

    .line 1466
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 1479
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->prepareBubble()V

    goto :goto_0
.end method

.method public resetAllContentForTutorial(Z)V
    .locals 0
    .param p1, "isForTutorial"    # Z

    .prologue
    .line 1997
    return-void
.end method

.method public setDialogContent()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 1639
    const/4 v1, 0x0

    .line 1640
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->isDialogMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1644
    :cond_0
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v1    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1645
    .restart local v1    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v5, v5, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1646
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v3, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1655
    :goto_0
    return-void

    .line 1649
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$dimen;->land_scrolldialog_marginleft:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1650
    .local v2, "marginLeft":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$dimen;->dialogview_width:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1651
    .local v0, "dialogWidth":I
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v1    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {v1, v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1652
    .restart local v1    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1653
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v3, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setMenuVar(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 1990
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->menuVar:Z

    .line 1991
    return-void
.end method

.method public startAnimationFlipDown(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v6, 0x1

    .line 1356
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v3, v1, v2

    .line 1357
    .local v3, "centerX":F
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v4, v1, v2

    .line 1358
    .local v4, "centerY":F
    new-instance v0, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v5, 0x439b0000    # 310.0f

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;-><init>(FFFFFZZ)V

    .line 1360
    .local v0, "rotation":Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;
    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setDuration(J)V

    .line 1361
    invoke-virtual {v0, v6}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setFillAfter(Z)V

    .line 1362
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1363
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1364
    return-void
.end method

.method public startAnimationFlipUp(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    const/high16 v2, 0x40000000    # 2.0f

    .line 1337
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v3, v1, v2

    .line 1338
    .local v3, "centerX":F
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v4, v1, v2

    .line 1339
    .local v4, "centerY":F
    new-instance v0, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;

    const/high16 v1, -0x3d4c0000    # -90.0f

    const/4 v2, 0x0

    const/high16 v5, 0x439b0000    # 310.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;-><init>(FFFFFZZ)V

    .line 1341
    .local v0, "rotation":Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;
    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setDuration(J)V

    .line 1342
    invoke-virtual {v0, v7}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setFillAfter(Z)V

    .line 1343
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1344
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1345
    return-void
.end method
