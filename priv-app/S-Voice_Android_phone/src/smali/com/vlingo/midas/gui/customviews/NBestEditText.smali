.class public Lcom/vlingo/midas/gui/customviews/NBestEditText;
.super Landroid/widget/EditText;
.source "NBestEditText.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private choices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private nbest:Lcom/vlingo/sdk/recognition/NBestData;

.field private popup:Landroid/widget/ListPopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->popup:Landroid/widget/ListPopupWindow;

    .line 22
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->choices:Ljava/util/List;

    .line 26
    invoke-direct {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->init()V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->popup:Landroid/widget/ListPopupWindow;

    .line 22
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->choices:Ljava/util/List;

    .line 31
    invoke-direct {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->init()V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->popup:Landroid/widget/ListPopupWindow;

    .line 22
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->choices:Ljava/util/List;

    .line 36
    invoke-direct {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->init()V

    .line 37
    return-void
.end method

.method private init()V
    .locals 0

    .prologue
    .line 44
    invoke-virtual {p0, p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 45
    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->choices:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->popup:Landroid/widget/ListPopupWindow;

    if-nez v0, :cond_1

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->choices:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 82
    .local v3, "selectedValue":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->getSelectionStart()I

    move-result v7

    .line 83
    .local v7, "start":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->getSelectionEnd()I

    move-result v6

    .line 84
    .local v6, "end":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v7, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 86
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->popup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->getSelectionStart()I

    move-result v1

    .line 50
    .local v1, "startSelection":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->getSelectionEnd()I

    move-result v0

    .line 51
    .local v0, "endSelection":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 52
    .local v2, "text":Landroid/text/Editable;
    if-nez v2, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v3

    .line 55
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->nbest:Lcom/vlingo/sdk/recognition/NBestData;

    if-eqz v4, :cond_0

    .line 58
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->nbest:Lcom/vlingo/sdk/recognition/NBestData;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1, v0}, Lcom/vlingo/sdk/recognition/NBestData;->getWordChoices(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->choices:Ljava/util/List;

    .line 60
    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->choices:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->choices:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 64
    new-instance v3, Landroid/widget/ListPopupWindow;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->popup:Landroid/widget/ListPopupWindow;

    .line 65
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->popup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v3, p0}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 67
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->popup:Landroid/widget/ListPopupWindow;

    new-instance v4, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/NBestEditText;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x1090003

    const v7, 0x1020014

    iget-object v8, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->choices:Ljava/util/List;

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 68
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->popup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v3, p0}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 70
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->popup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v3}, Landroid/widget/ListPopupWindow;->show()V

    .line 72
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public setNbest(Lcom/vlingo/sdk/recognition/NBestData;)V
    .locals 0
    .param p1, "nbest"    # Lcom/vlingo/sdk/recognition/NBestData;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/vlingo/midas/gui/customviews/NBestEditText;->nbest:Lcom/vlingo/sdk/recognition/NBestData;

    .line 41
    return-void
.end method
