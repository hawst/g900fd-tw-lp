.class Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget$WeatherSevenDayWidgetHandler;
.super Landroid/os/Handler;
.source "WeatherSevenDayWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WeatherSevenDayWidgetHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;

    .prologue
    .line 604
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 605
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget$WeatherSevenDayWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 606
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 609
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget$WeatherSevenDayWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;

    .line 610
    .local v0, "o":Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;
    if-eqz v0, :cond_0

    .line 613
    # invokes: Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->setValuesForcastWeatherOneDay()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->access$000(Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;)V

    .line 614
    # invokes: Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->setValuesForcastWeather()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->access$100(Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;)V

    .line 615
    # invokes: Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->setTalkbackString()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;->access$200(Lcom/vlingo/midas/gui/widgets/WeatherSevenDayWidget;)V

    .line 617
    :cond_0
    return-void
.end method
