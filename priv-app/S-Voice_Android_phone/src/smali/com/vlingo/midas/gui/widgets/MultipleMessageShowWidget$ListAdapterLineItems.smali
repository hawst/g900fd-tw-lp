.class Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;
.super Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;
.source "MultipleMessageShowWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListAdapterLineItems"
.end annotation


# instance fields
.field private final list:Ljava/util/List;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/messages/MessageSenderSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p3, "lineItems":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;

    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;Landroid/content/Context;Ljava/util/List;)V

    .line 73
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->list:Ljava/util/List;

    .line 74
    return-void
.end method

.method private getDisplayMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Z)Ljava/lang/String;
    .locals 5
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .param p2, "showMessageBody"    # Z

    .prologue
    const/16 v4, 0x1e

    .line 78
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;->context:Landroid/content/Context;

    invoke-virtual {p1, v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getDisplayableMessageText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "body":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 81
    move-object v1, v0

    .line 83
    .local v1, "msg":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_0

    .line 84
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 89
    :cond_0
    :goto_0
    return-object v1

    .line 87
    .end local v1    # "msg":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->core_car_safereader_hidden_message_body:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "msg":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 94
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->list:Ljava/util/List;

    .line 95
    .local v7, "lineItems":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/messages/MessageSenderSummary;

    .line 96
    .local v6, "lineItem":Lcom/vlingo/core/internal/messages/MessageSenderSummary;
    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getAlert()Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    move-result-object v0

    .line 98
    .local v0, "alert":Lcom/vlingo/core/internal/safereader/SafeReaderAlert;
    if-nez p2, :cond_0

    .line 99
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;->getContext()Landroid/content/Context;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$layout;->item_message_details:I

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 102
    :cond_0
    sget v10, Lcom/vlingo/midas/R$id;->from:I

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 103
    .local v5, "from":Landroid/widget/TextView;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v8

    .line 104
    .local v8, "sender":Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 105
    invoke-virtual {v0}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getAddress()Ljava/lang/String;

    move-result-object v8

    .line 107
    :cond_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;

    iget-object v11, v11, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;->context:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    sget v12, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ": "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    sget v10, Lcom/vlingo/midas/R$id;->count:I

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 111
    .local v3, "count":Landroid/widget/TextView;
    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getCount()I

    move-result v2

    .line 112
    .local v2, "c":I
    if-eqz v3, :cond_2

    .line 113
    const/4 v10, 0x1

    if-le v2, v10, :cond_3

    .line 114
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Lcom/vlingo/core/internal/messages/MessageSenderSummary;->getCount()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :cond_2
    :goto_0
    sget v10, Lcom/vlingo/midas/R$id;->body:I

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .local v1, "body":Landroid/widget/TextView;
    move-object v9, v0

    .line 124
    check-cast v9, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 125
    .local v9, "smsMMSAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    const/4 v10, 0x1

    invoke-direct {p0, v9, v10}, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->getDisplayMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Z)Ljava/lang/String;

    move-result-object v4

    .line 126
    .local v4, "displayMessage":Ljava/lang/String;
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    new-instance v10, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems$1;

    invoke-direct {v10, p0, v0, v2}, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems$1;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;Lcom/vlingo/core/internal/safereader/SafeReaderAlert;I)V

    invoke-virtual {p2, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->getCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ne p1, v10, :cond_4

    .line 142
    sget v10, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v10}, Landroid/view/View;->setBackgroundResource(I)V

    .line 146
    :goto_1
    return-object p2

    .line 116
    .end local v1    # "body":Landroid/widget/TextView;
    .end local v4    # "displayMessage":Ljava/lang/String;
    .end local v9    # "smsMMSAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_3
    const/16 v10, 0x8

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 144
    .restart local v1    # "body":Landroid/widget/TextView;
    .restart local v4    # "displayMessage":Ljava/lang/String;
    .restart local v9    # "smsMMSAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    :cond_4
    sget v10, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v10}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method
