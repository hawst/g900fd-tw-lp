.class public Lcom/vlingo/midas/gui/ConversationActivity$AdminReceiver;
.super Landroid/app/admin/DeviceAdminReceiver;
.source "ConversationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ConversationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AdminReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6135
    invoke-direct {p0}, Landroid/app/admin/DeviceAdminReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisabled(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 6138
    const/4 v0, 0x2

    sput v0, Lcom/vlingo/midas/gui/ConversationActivity;->lockTheDevice:I

    .line 6139
    invoke-super {p0, p1, p2}, Landroid/app/admin/DeviceAdminReceiver;->onDisabled(Landroid/content/Context;Landroid/content/Intent;)V

    .line 6140
    return-void
.end method

.method public onEnabled(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 6144
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->mDPM:Landroid/app/admin/DevicePolicyManager;

    if-eqz v0, :cond_0

    sget v0, Lcom/vlingo/midas/gui/ConversationActivity;->lockTheDevice:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 6145
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->lockNow()V

    .line 6146
    const/4 v0, 0x2

    sput v0, Lcom/vlingo/midas/gui/ConversationActivity;->lockTheDevice:I

    .line 6148
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/admin/DeviceAdminReceiver;->onEnabled(Landroid/content/Context;Landroid/content/Intent;)V

    .line 6149
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 6154
    invoke-super {p0, p1, p2}, Landroid/app/admin/DeviceAdminReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 6155
    return-void
.end method
