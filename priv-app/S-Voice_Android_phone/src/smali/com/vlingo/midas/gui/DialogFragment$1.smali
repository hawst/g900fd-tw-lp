.class Lcom/vlingo/midas/gui/DialogFragment$1;
.super Ljava/lang/Object;
.source "DialogFragment.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogFragment;

.field final synthetic val$AddingView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1007
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragment$1;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iput-object p2, p0, Lcom/vlingo/midas/gui/DialogFragment$1;->val$AddingView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "anim"    # Landroid/animation/Animator;

    .prologue
    .line 1010
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "anim"    # Landroid/animation/Animator;

    .prologue
    .line 1013
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$1;->val$AddingView:Landroid/view/View;

    instance-of v0, v0, Lcom/vlingo/midas/gui/Widget;

    if-eqz v0, :cond_0

    .line 1014
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$1;->val$AddingView:Landroid/view/View;

    check-cast v0, Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/Widget;->onAnimationEnd()V

    .line 1016
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "anim"    # Landroid/animation/Animator;

    .prologue
    .line 1019
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "anim"    # Landroid/animation/Animator;

    .prologue
    .line 1022
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$1;->val$AddingView:Landroid/view/View;

    instance-of v0, v0, Lcom/vlingo/midas/gui/Widget;

    if-eqz v0, :cond_0

    .line 1023
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1024
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$1;->val$AddingView:Landroid/view/View;

    check-cast v0, Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/Widget;->onAnimationStart()V

    .line 1034
    :cond_0
    :goto_0
    return-void

    .line 1026
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/vlingo/midas/gui/DialogFragment$1$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/DialogFragment$1$1;-><init>(Lcom/vlingo/midas/gui/DialogFragment$1;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
