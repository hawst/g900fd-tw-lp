.class Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;
.super Landroid/os/Handler;
.source "EditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/customviews/EditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EditTextHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/customviews/EditText;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/customviews/EditText;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/customviews/EditText;

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 58
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 59
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 62
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/EditText;

    .line 63
    .local v0, "o":Lcom/vlingo/midas/gui/customviews/EditText;
    if-eqz v0, :cond_0

    .line 64
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 70
    :pswitch_0
    # getter for: Lcom/vlingo/midas/gui/customviews/EditText;->mIsKeypadShow:Z
    invoke-static {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->access$000(Lcom/vlingo/midas/gui/customviews/EditText;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    # invokes: Lcom/vlingo/midas/gui/customviews/EditText;->setEditUI(Z)V
    invoke-static {v0, v2}, Lcom/vlingo/midas/gui/customviews/EditText;->access$100(Lcom/vlingo/midas/gui/customviews/EditText;Z)V

    .line 72
    # invokes: Lcom/vlingo/midas/gui/customviews/EditText;->editCanceled()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->access$200(Lcom/vlingo/midas/gui/customviews/EditText;)V

    goto :goto_0

    .line 76
    :pswitch_1
    # getter for: Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;
    invoke-static {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->access$300(Lcom/vlingo/midas/gui/customviews/EditText;)Lcom/vlingo/midas/gui/OnEditListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 77
    # invokes: Lcom/vlingo/midas/gui/customviews/EditText;->setEditUI(Z)V
    invoke-static {v0, v2}, Lcom/vlingo/midas/gui/customviews/EditText;->access$100(Lcom/vlingo/midas/gui/customviews/EditText;Z)V

    .line 78
    # invokes: Lcom/vlingo/midas/gui/customviews/EditText;->editCanceled()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->access$200(Lcom/vlingo/midas/gui/customviews/EditText;)V

    goto :goto_0

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
