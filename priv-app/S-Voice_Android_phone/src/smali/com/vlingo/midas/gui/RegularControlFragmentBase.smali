.class public abstract Lcom/vlingo/midas/gui/RegularControlFragmentBase;
.super Lcom/vlingo/midas/gui/ControlFragment;
.source "RegularControlFragmentBase.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/RegularControlFragmentBase$6;,
        Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;
    }
.end annotation


# static fields
.field static prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;


# instance fields
.field private DEBUG_UNLOCK_STEP:I

.field private final DEBUG_UNLOCK_TIMEOUT_MS:I

.field private DEBUG_UNLOCK_TIMESTAMP:J

.field LOG_TAG:Ljava/lang/String;

.field private animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

.field protected blinker:Landroid/view/animation/Animation;

.field private dialogFragment:Lcom/vlingo/midas/gui/ContentFragment;

.field protected fullContainer:Landroid/widget/RelativeLayout;

.field protected helpBtn_Kitkat:Landroid/widget/ImageView;

.field protected layoutMenuBtn:Landroid/widget/LinearLayout;

.field protected layoutMicBtn:Landroid/widget/LinearLayout;

.field protected layoutMicBtnKitkat:Landroid/widget/LinearLayout;

.field protected layoutPromptBtn:Landroid/widget/LinearLayout;

.field protected mHourGlassImg:Landroid/widget/ImageView;

.field protected mMicClickAnim:Landroid/graphics/drawable/AnimationDrawable;

.field private mThink_animation:Landroid/view/animation/Animation;

.field private mThinking_bar_anim:Landroid/view/animation/Animation;

.field protected mainControlLL:Landroid/widget/LinearLayout;

.field protected menuBtn_Kitkat:Landroid/widget/ImageView;

.field private micButtonStateListener:Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;

.field protected micStatusText:Landroid/widget/TextView;

.field protected reco_idleBtn:Landroid/widget/ImageView;

.field protected reco_idleMic:Landroid/widget/ImageView;

.field protected reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

.field protected reco_listeningBtnBackLayer:Lcom/vlingo/midas/gui/customviews/MicViewBase;

.field protected reco_listeningMic:Landroid/widget/ImageView;

.field protected reco_thinkingBtn:Landroid/widget/ImageView;

.field protected reco_thinkingBtnBg:Landroid/widget/ImageButton;

.field protected reco_thinkingBtnBg1:Landroid/widget/ImageButton;

.field private stretchOut:Landroid/view/animation/Animation;

.field protected toggle_btn:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragment;-><init>()V

    .line 46
    const-string/jumbo v0, "RegularControlFragmentBase"

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->LOG_TAG:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_STEP:I

    .line 49
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_TIMEOUT_MS:I

    .line 50
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_TIMESTAMP:J

    .line 58
    iput-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mMicClickAnim:Landroid/graphics/drawable/AnimationDrawable;

    .line 79
    iput-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->stretchOut:Landroid/view/animation/Animation;

    .line 84
    new-instance v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;

    invoke-direct {v0, p0, v2}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;-><init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micButtonStateListener:Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;

    .line 1026
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)J
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_TIMESTAMP:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/gui/RegularControlFragmentBase;J)J
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/RegularControlFragmentBase;
    .param p1, "x1"    # J

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_TIMESTAMP:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    .prologue
    .line 44
    iget v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_STEP:I

    return v0
.end method

.method static synthetic access$202(Lcom/vlingo/midas/gui/RegularControlFragmentBase;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/RegularControlFragmentBase;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_STEP:I

    return p1
.end method

.method static synthetic access$208(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    .prologue
    .line 44
    iget v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_STEP:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_STEP:I

    return v0
.end method

.method private cancelAllActivities()V
    .locals 1

    .prologue
    .line 323
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 324
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 325
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->clearUpdateType()V

    .line 326
    return-void
.end method

.method private delegateViewForTalkBack(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 557
    if-eqz p1, :cond_0

    .line 558
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 560
    :cond_0
    return-void
.end method

.method private removeMicTouchForIdleMic()V
    .locals 1

    .prologue
    .line 577
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578
    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->removeMicTouchTalkBack()V

    .line 580
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 581
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 583
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->removeDelegateFromLastScreen()V

    goto :goto_0
.end method

.method private removeMicTouchForThinkingMic()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 589
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 591
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 592
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 594
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->removeMicTouchTalkBack()V

    .line 607
    :cond_0
    :goto_0
    return-void

    .line 596
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 597
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 600
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0
.end method

.method private removeMicTouchIdleTalkBack()V
    .locals 1

    .prologue
    .line 531
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 533
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 534
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mainControlLL:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 536
    :cond_0
    return-void
.end method

.method private removeMicTouchTalkBack()V
    .locals 1

    .prologue
    .line 565
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 567
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 568
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 569
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 570
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 571
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtn:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 572
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mainControlLL:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 574
    :cond_0
    return-void
.end method

.method private setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;
    .locals 1

    .prologue
    .line 927
    new-instance v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$3;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$3;-><init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)V

    return-object v0
.end method

.method private updateContentDescriptions()V
    .locals 3

    .prologue
    .line 126
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->talkback_mic_button_recognizing:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->talkback_mic_button_idle:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 131
    return-void
.end method


# virtual methods
.method protected blinkAnim()V
    .locals 4

    .prologue
    .line 615
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    if-nez v1, :cond_1

    .line 665
    :cond_0
    :goto_0
    return-void

    .line 620
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtnBackLayer:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setVisibility(I)V

    .line 621
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->s_voice_eq_bg:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 622
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    iget-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->stretchOut:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setAnimation(Landroid/view/animation/Animation;)V

    .line 625
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 626
    .local v0, "micShown":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v1, 0x10e

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 627
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 629
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->stretchOut:Landroid/view/animation/Animation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->start()V

    .line 630
    invoke-virtual {v0}, Landroid/view/animation/AlphaAnimation;->start()V

    goto :goto_0
.end method

.method protected disableHelpWidgetButton()V
    .locals 2

    .prologue
    .line 742
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->isActivityCreated()Z

    move-result v1

    if-nez v1, :cond_1

    .line 749
    :cond_0
    :goto_0
    return-void

    .line 746
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 747
    .local v0, "fa":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method protected disableMicInTutorial(Z)V
    .locals 3
    .param p1, "disable"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 996
    if-eqz p1, :cond_0

    .line 997
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 998
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1003
    :goto_0
    return-void

    .line 1000
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1001
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected enableHelpWidgetButton()V
    .locals 2

    .prologue
    .line 731
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->isActivityCreated()Z

    move-result v1

    if-nez v1, :cond_1

    .line 738
    :cond_0
    :goto_0
    return-void

    .line 735
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 736
    .local v0, "fa":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public fadeInMicLayout(ZI)V
    .locals 5
    .param p1, "visible"    # Z
    .param p2, "duration"    # I

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 937
    if-eqz p1, :cond_0

    .line 940
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$anim;->fade_in:I

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 941
    .local v0, "fadeIn":Landroid/view/animation/Animation;
    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 942
    new-instance v1, Lcom/vlingo/midas/gui/RegularControlFragmentBase$4;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$4;-><init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)V

    .line 957
    .local v1, "listner":Landroid/view/animation/Animation$AnimationListener;
    iget-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 958
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 959
    iget-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 963
    .end local v0    # "fadeIn":Landroid/view/animation/Animation;
    .end local v1    # "listner":Landroid/view/animation/Animation$AnimationListener;
    :goto_0
    return-void

    .line 961
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto :goto_0
.end method

.method abstract getThninkingBtnOver()I
.end method

.method abstract getThninkingMicAnimationList()I
.end method

.method public hideMic()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 966
    iget-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 967
    iget-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 968
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$anim;->fade_out:I

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 969
    .local v0, "fadeOut":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 970
    new-instance v1, Lcom/vlingo/midas/gui/RegularControlFragmentBase$5;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$5;-><init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)V

    .line 983
    .local v1, "listner":Landroid/view/animation/Animation$AnimationListener;
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 984
    iget-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 985
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 272
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 273
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_kitkat:I

    if-eq v0, v1, :cond_0

    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_kitkat_land:I

    if-eq v0, v1, :cond_0

    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_mic_kitkat:I

    if-ne v0, v1, :cond_2

    .line 278
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->endpointReco()V

    .line 320
    :cond_1
    :goto_0
    return-void

    .line 279
    :cond_2
    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking:I

    if-eq v0, v1, :cond_3

    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking_land:I

    if-eq v0, v1, :cond_3

    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking_kitkat:I

    if-eq v0, v1, :cond_3

    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking_kitkat_bg:I

    if-ne v0, v1, :cond_5

    .line 285
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->resetThinkingState()V

    .line 286
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 287
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-eq v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v1, v2, :cond_1

    .line 289
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    goto :goto_0

    .line 291
    :cond_5
    sget v1, Lcom/vlingo/midas/R$id;->btn_idle:I

    if-eq v0, v1, :cond_6

    sget v1, Lcom/vlingo/midas/R$id;->btn_idle_land:I

    if-eq v0, v1, :cond_6

    sget v1, Lcom/vlingo/midas/R$id;->btn_idle_kitkat:I

    if-eq v0, v1, :cond_6

    sget v1, Lcom/vlingo/midas/R$id;->btn_idle_kitkat_land:I

    if-ne v0, v1, :cond_8

    .line 304
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v1

    if-nez v1, :cond_7

    .line 305
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->resetThinkingState()V

    .line 306
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->cancelAllActivities()V

    goto :goto_0

    .line 308
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 309
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->cancelAllActivities()V

    .line 310
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    goto :goto_0

    .line 313
    :cond_8
    sget v1, Lcom/vlingo/midas/R$id;->btn_menu_kitkat:I

    if-ne v0, v1, :cond_9

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v1, :cond_9

    .line 314
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->openOptionsMenu()V

    goto :goto_0

    .line 315
    :cond_9
    sget v1, Lcom/vlingo/midas/R$id;->btn_toggle:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v1, :cond_1

    .line 316
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->switchToFullScreen()V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 392
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/ControlFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 396
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->isActivityCreated()Z

    move-result v1

    if-nez v1, :cond_1

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 400
    .local v0, "fa":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v1, :cond_0

    .line 403
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v1, :cond_2

    .line 404
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 405
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 408
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 409
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ContentFragment;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->dialogFragment:Lcom/vlingo/midas/gui/ContentFragment;

    .line 89
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/ControlFragment;->onCreate(Landroid/os/Bundle;)V

    .line 120
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$anim;->blinker:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->blinker:Landroid/view/animation/Animation;

    .line 121
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$anim;->stretch_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->stretchOut:Landroid/view/animation/Animation;

    .line 122
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 759
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 760
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 763
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v0, :cond_1

    .line 764
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 766
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 767
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 769
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 770
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 771
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 773
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 774
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-nez v0, :cond_4

    .line 775
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->helpBtn_Kitkat:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 776
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->helpBtn_Kitkat:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 777
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->menuBtn_Kitkat:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 778
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->menuBtn_Kitkat:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 781
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v0, :cond_5

    .line 782
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->reCycle()V

    .line 784
    :cond_5
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtnBackLayer:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v0, :cond_6

    .line 785
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtnBackLayer:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->reCycle()V

    .line 788
    :cond_6
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->unbindDrawables(Landroid/view/View;)V

    .line 789
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 790
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtnBackLayer:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 791
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    .line 792
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    .line 793
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    .line 794
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    .line 795
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    .line 796
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtn:Landroid/widget/LinearLayout;

    .line 797
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    .line 798
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mMicClickAnim:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_7

    .line 799
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mMicClickAnim:Landroid/graphics/drawable/AnimationDrawable;

    .line 801
    :cond_7
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 803
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    .line 805
    :cond_8
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    .line 806
    invoke-super {p0}, Lcom/vlingo/midas/gui/ControlFragment;->onDestroy()V

    .line 807
    return-void
.end method

.method public onLanguageChanged()V
    .locals 3

    .prologue
    .line 699
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 700
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->talkback_mic_button_recognizing:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 703
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->talkback_mic_button_idle:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 704
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->dialogFragment:Lcom/vlingo/midas/gui/ContentFragment;

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->stopSpotting()V

    .line 139
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->isVoicePromptShowing(Z)V

    .line 140
    invoke-super {p0}, Lcom/vlingo/midas/gui/ControlFragment;->onPause()V

    .line 141
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 145
    invoke-super {p0}, Lcom/vlingo/midas/gui/ControlFragment;->onStop()V

    .line 146
    return-void
.end method

.method public performClickFromDriveControl()Z
    .locals 1

    .prologue
    .line 812
    const/4 v0, 0x0

    return v0
.end method

.method public removeControlPanel()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->fullContainer:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 266
    return-void
.end method

.method public removeDelegateFromLastScreen()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 540
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 541
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 548
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 553
    :cond_1
    return-void
.end method

.method public removeMic()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 988
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 989
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 990
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 991
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 992
    return-void
.end method

.method public removeMicStatusText()V
    .locals 2

    .prologue
    .line 848
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 849
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 851
    :cond_0
    return-void
.end method

.method public removeMicStatusTextForTutorial(Z)V
    .locals 2
    .param p1, "hide"    # Z

    .prologue
    .line 856
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 857
    if-eqz p1, :cond_1

    .line 858
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 863
    :cond_0
    :goto_0
    return-void

    .line 860
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method

.method protected resetThinkingState()V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 372
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-nez v0, :cond_2

    .line 373
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getThninkingBtnOver()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 387
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 388
    return-void

    .line 376
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 377
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    .line 379
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 380
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 383
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg1:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 384
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg1:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public setBlueHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 816
    return-void
.end method

.method protected setButtonIdle(Z)V
    .locals 5
    .param p1, "enabled"    # Z

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 414
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->mic_animation_container:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/ListeningView;

    .line 415
    .local v0, "listeningview":Lcom/vlingo/midas/gui/customviews/ListeningView;
    if-eqz v0, :cond_0

    .line 416
    invoke-virtual {v0, v3}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 420
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg1:Landroid/widget/ImageButton;

    if-eqz v1, :cond_1

    .line 421
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg1:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 422
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtnBackLayer:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v1, :cond_2

    .line 423
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtnBackLayer:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v1, v3}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setVisibility(I)V

    .line 425
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v1, :cond_3

    .line 426
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v1, v3}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setVisibility(I)V

    .line 427
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->resetListeningAnimation()V

    .line 429
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_4

    .line 430
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 433
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 435
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    .line 437
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 439
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    if-eqz v1, :cond_6

    .line 440
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 445
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg1:Landroid/widget/ImageButton;

    if-eqz v1, :cond_7

    .line 446
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg1:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 447
    :cond_7
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    if-eqz v1, :cond_8

    .line 449
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 454
    :cond_8
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_9

    .line 455
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 456
    :cond_9
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_a

    .line 457
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 463
    :cond_a
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_b

    .line 464
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->talkback_mic_button_idle:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 465
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 466
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 467
    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->getLatestWidget()Lcom/vlingo/midas/gui/Widget;

    move-result-object v1

    instance-of v1, v1, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-nez v1, :cond_b

    .line 472
    :cond_b
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->removeMicTouchForIdleMic()V

    .line 474
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micButtonStateListener:Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->onSetButtonIdle()V

    .line 475
    return-void
.end method

.method protected setButtonListening()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 478
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setVisibility(I)V

    .line 482
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 483
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 485
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 487
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 488
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 491
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 492
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 498
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 499
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 502
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 503
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 506
    :cond_5
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v0, :cond_6

    .line 507
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0, v3}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setEnabled(Z)V

    .line 511
    :cond_6
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->removeMicTouchTalkBack()V

    .line 514
    :cond_7
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 515
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 518
    :cond_8
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    .line 519
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 522
    :cond_9
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v0, :cond_a

    .line 523
    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->getLatestWidget()Lcom/vlingo/midas/gui/Widget;

    move-result-object v0

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-nez v0, :cond_a

    .line 524
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->requestFocus()Z

    .line 527
    :cond_a
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micButtonStateListener:Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->onSetButtonListening()V

    .line 528
    return-void
.end method

.method protected setButtonThinking()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 668
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setVisibility(I)V

    .line 670
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->resetListeningAnimation()V

    .line 672
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtnBackLayer:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v0, :cond_1

    .line 673
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtnBackLayer:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setVisibility(I)V

    .line 675
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 676
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 679
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 680
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 681
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 682
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 683
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 684
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 685
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 689
    :cond_5
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 690
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 692
    :cond_6
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->removeMicTouchForThinkingMic()V

    .line 694
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micButtonStateListener:Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->onSetButtonThinking()V

    .line 695
    return-void
.end method

.method public setFakeSvoiceMiniForTutorial(Z)V
    .locals 4
    .param p1, "doset"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1007
    if-eqz p1, :cond_1

    .line 1008
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1009
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1010
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->expand:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1011
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1024
    :goto_0
    return-void

    .line 1014
    :cond_0
    const-string/jumbo v0, "Kabra-->"

    const-string/jumbo v1, "toggle btn null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1016
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 1017
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1018
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->expand:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1019
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1022
    :cond_2
    const-string/jumbo v0, "Kabra-->"

    const-string/jumbo v1, "toggle btn null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setMicStatusText()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 820
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->isActivityCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 845
    :cond_0
    :goto_0
    return-void

    .line 823
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 825
    sget-object v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragment$ControlState:[I

    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentData;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 827
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 828
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_speak_now:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 831
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 832
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->drive_greeting_processing:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 835
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 838
    :pswitch_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 839
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->tutorial_display_tap_mic:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 825
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setNormalPanel()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 156
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 158
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->removeMicTouchIdleTalkBack()V

    .line 161
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningMic:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 176
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 179
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 183
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 187
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 188
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    :cond_5
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 190
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 192
    :cond_6
    invoke-static {}, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen()Z

    move-result v0

    if-nez v0, :cond_8

    .line 193
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    .line 194
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 195
    :cond_7
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    .line 196
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->micStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 201
    :cond_8
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-nez v0, :cond_9

    .line 202
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->menuBtn_Kitkat:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->helpBtn_Kitkat:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->menuBtn_Kitkat:Landroid/widget/ImageView;

    new-instance v1, Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;-><init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 230
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->helpBtn_Kitkat:Landroid/widget/ImageView;

    new-instance v1, Lcom/vlingo/midas/gui/RegularControlFragmentBase$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$2;-><init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 252
    :cond_9
    return-void
.end method

.method protected setPrompt()V
    .locals 0

    .prologue
    .line 754
    return-void
.end method

.method protected setThinkingAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 333
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getThninkingMicAnimationList()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 335
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 336
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 363
    :goto_0
    return-void

    .line 339
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$anim;->translate_bar_anim:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mThinking_bar_anim:Landroid/view/animation/Animation;

    .line 340
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 341
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mHourGlassImg:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->mThinking_bar_anim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 345
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 346
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 347
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->blinker:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 349
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg1:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 350
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtnBg1:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 361
    :cond_3
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->removeMicTouchForThinkingMic()V

    goto :goto_0
.end method

.method protected setToggleBtnVisiblity(Z)V
    .locals 2
    .param p1, "isMiniMode"    # Z

    .prologue
    .line 255
    if-eqz p1, :cond_1

    .line 256
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->toggle_btn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showRMSChange(I)V
    .locals 2
    .param p1, "rmsValue"    # I

    .prologue
    .line 707
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->isActivityCreated()Z

    move-result v0

    if-nez v0, :cond_1

    .line 720
    :cond_0
    :goto_0
    return-void

    .line 716
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setRMSValue(F)V

    goto :goto_0
.end method

.method public showSpectrum([I)V
    .locals 1
    .param p1, "spectrum"    # [I

    .prologue
    .line 724
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->isActivityCreated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 727
    :cond_0
    return-void
.end method
