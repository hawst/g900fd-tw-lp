.class Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SmartNotificationWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SmartNotificationListAdapter"
.end annotation


# instance fields
.field private eventList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/gui/widgets/ScheduleEventData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/gui/widgets/ScheduleEventData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 215
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/midas/gui/widgets/ScheduleEventData;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 216
    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->eventList:Ljava/util/ArrayList;

    .line 217
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 226
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 231
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v8, 0x1e

    const/4 v7, 0x3

    const/4 v4, 0x0

    .line 237
    if-nez p2, :cond_2

    .line 238
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->access$000(Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    sget v5, Lcom/vlingo/midas/R$layout;->item_smart_notification:I

    invoke-virtual {v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 240
    new-instance v1, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;

    invoke-direct {v1}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;-><init>()V

    .line 241
    .local v1, "holder":Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;
    sget v3, Lcom/vlingo/midas/R$id;->event_title:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;->title:Landroid/widget/TextView;

    .line 242
    sget v3, Lcom/vlingo/midas/R$id;->event_time:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;->time:Landroid/widget/TextView;

    .line 243
    sget v3, Lcom/vlingo/midas/R$id;->event_divider:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v1, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;->divider:Landroid/view/View;

    .line 245
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 250
    :goto_0
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    .line 252
    .local v0, "data":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getType()I

    move-result v3

    if-ne v3, v7, :cond_6

    .line 253
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getTitle()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, ""

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    move-object p2, v4

    .line 285
    .end local p2    # "view":Landroid/view/View;
    :cond_1
    :goto_1
    return-object p2

    .line 247
    .end local v0    # "data":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .end local v1    # "holder":Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;
    .restart local p2    # "view":Landroid/view/View;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;

    .restart local v1    # "holder":Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;
    goto :goto_0

    .line 257
    .restart local v0    # "data":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :cond_3
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;

    iget-object v3, v3, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->list1:Ljava/util/ArrayList;

    if-eqz v3, :cond_4

    .line 258
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;

    iget-object v3, v3, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->list1:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;

    iget v6, v6, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bdayEvents:I

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    :cond_4
    :goto_2
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getStartDate()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    const-string/jumbo v3, ""

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getStartDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_5
    move-object p2, v4

    .line 271
    goto :goto_1

    .line 261
    :cond_6
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 263
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$string;->my_event:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 266
    :cond_7
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 273
    :cond_8
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;->time:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getStartDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getType()I

    move-result v3

    if-ne v3, v7, :cond_9

    .line 276
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;->time:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 278
    :cond_9
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-nez v3, :cond_1

    .line 279
    sget v3, Lcom/vlingo/midas/R$id;->event_title:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 281
    .local v2, "topPad":Landroid/widget/TextView;
    const/16 v3, 0x21

    const/4 v4, 0x0

    invoke-virtual {v2, v8, v3, v4, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_1
.end method
