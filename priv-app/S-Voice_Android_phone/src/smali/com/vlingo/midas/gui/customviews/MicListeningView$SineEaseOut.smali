.class Lcom/vlingo/midas/gui/customviews/MicListeningView$SineEaseOut;
.super Ljava/lang/Object;
.source "MicListeningView.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/customviews/MicListeningView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SineEaseOut"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/customviews/MicListeningView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningView$1;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/vlingo/midas/gui/customviews/MicListeningView$SineEaseOut;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 3
    .param p1, "input"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 75
    div-float v0, p1, v2

    const v1, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    mul-float/2addr v0, v2

    const/4 v1, 0x0

    add-float/2addr v0, v1

    return v0
.end method
