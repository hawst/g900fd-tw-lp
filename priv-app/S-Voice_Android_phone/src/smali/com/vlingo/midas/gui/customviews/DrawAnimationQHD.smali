.class public Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;
.super Ljava/lang/Object;
.source "DrawAnimationQHD.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V
    .locals 6
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "cornerBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "squareImage"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p3, "mRms"    # F

    .prologue
    const/high16 v5, 0x420c0000    # 35.0f

    const/high16 v4, 0x41700000    # 15.0f

    const/high16 v3, 0x42be0000    # 95.0f

    const/high16 v1, 0x425c0000    # 55.0f

    const/4 v2, 0x0

    .line 176
    const/high16 v0, 0x41f80000    # 31.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 177
    const v0, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 178
    const v0, 0x43938000    # 295.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 179
    const/high16 v0, 0x438d0000    # 282.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 180
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x438a0000    # 276.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 181
    const/high16 v0, 0x438d0000    # 282.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 182
    const/high16 v0, 0x42e60000    # 115.0f

    const v1, 0x43938000    # 295.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 183
    const/high16 v0, 0x43070000    # 135.0f

    const v1, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    const/high16 v0, 0x42140000    # 37.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 187
    const v0, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 188
    const v0, 0x438b8000    # 279.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 189
    const v0, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 190
    const/high16 v0, 0x42960000    # 75.0f

    const v1, 0x43818000    # 259.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 191
    const v0, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 192
    const/high16 v0, 0x42e60000    # 115.0f

    const v1, 0x438b8000    # 279.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 193
    const/high16 v0, 0x43070000    # 135.0f

    const v1, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 195
    const/16 v0, 0xf

    const/16 v1, 0x122

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 196
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 198
    :cond_2
    const/high16 v0, 0x422c0000    # 43.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_3

    .line 199
    const/high16 v0, 0x43590000    # 217.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 200
    const/high16 v0, 0x436c0000    # 236.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 201
    const/high16 v0, 0x43730000    # 243.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 202
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x436c0000    # 236.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 203
    const/high16 v0, 0x43590000    # 217.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 204
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x43730000    # 243.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 205
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x436c0000    # 236.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 207
    const/16 v0, 0xf

    const/16 v1, 0xfa

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 208
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 211
    :cond_3
    const/high16 v0, 0x42440000    # 49.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    .line 212
    const/high16 v0, 0x43590000    # 217.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 213
    const/high16 v0, 0x43460000    # 198.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 214
    const/high16 v0, 0x433f0000    # 191.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 215
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x43460000    # 198.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 216
    const/high16 v0, 0x43590000    # 217.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 217
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x43590000    # 217.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 218
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x433f0000    # 191.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 220
    const/16 v0, 0xf

    const/16 v1, 0xdc

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 221
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 224
    :cond_4
    cmpg-float v0, p3, v1

    if-gez v0, :cond_5

    .line 225
    const/high16 v0, 0x432c0000    # 172.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 226
    const/high16 v0, 0x43380000    # 184.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 227
    const/high16 v0, 0x433f0000    # 191.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 228
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x43380000    # 184.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 229
    const/high16 v0, 0x43250000    # 165.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 230
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x433f0000    # 191.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 231
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x43380000    # 184.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 233
    const/16 v0, 0xf

    const/16 v1, 0xc8

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 234
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 236
    :cond_5
    const/high16 v0, 0x42740000    # 61.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6

    .line 237
    const/high16 v0, 0x43110000    # 145.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 238
    const/high16 v0, 0x431e0000    # 158.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 239
    const/high16 v0, 0x43250000    # 165.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 240
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x431e0000    # 158.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 241
    const/high16 v0, 0x430b0000    # 139.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 242
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x43250000    # 165.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 243
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x431e0000    # 158.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 245
    const/16 v0, 0xf

    const/16 v1, 0xaa

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 246
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 248
    :cond_6
    const/high16 v0, 0x42860000    # 67.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_7

    .line 249
    const/high16 v0, 0x42ee0000    # 119.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 250
    const/high16 v0, 0x430b0000    # 139.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 251
    const/high16 v0, 0x42fc0000    # 126.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 252
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x42e20000    # 113.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 253
    const/high16 v0, 0x42fc0000    # 126.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 254
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x43040000    # 132.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 255
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x42fc0000    # 126.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 257
    const/16 v0, 0xf

    const/16 v1, 0x8c

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 258
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 260
    :cond_7
    const/high16 v0, 0x42920000    # 73.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_8

    .line 261
    const/high16 v0, 0x42ae0000    # 87.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 262
    const/high16 v0, 0x42e20000    # 113.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 263
    const/high16 v0, 0x42ee0000    # 119.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 264
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x42fc0000    # 126.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 265
    const/high16 v0, 0x42ee0000    # 119.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 266
    const/high16 v0, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 267
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x42ae0000    # 87.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 269
    const/16 v0, 0xf

    const/16 v1, 0x82

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 270
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 273
    :cond_8
    const/high16 v0, 0x429e0000    # 79.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_9

    .line 274
    const/high16 v0, 0x42740000    # 61.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 275
    const/high16 v0, 0x42ae0000    # 87.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 276
    const/high16 v0, 0x42ae0000    # 87.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 277
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x42940000    # 74.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 278
    const/high16 v0, 0x42ae0000    # 87.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 279
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 280
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x42740000    # 61.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 282
    const/16 v0, 0xf

    const/16 v1, 0x5a

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 283
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 287
    :cond_9
    const/high16 v0, 0x429e0000    # 79.0f

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 288
    const/16 v0, 0xf

    const/16 v1, 0x28

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 289
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method public static drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V
    .locals 7
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "cornerBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "squareImage"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p3, "mRms"    # F

    .prologue
    const/high16 v6, 0x432f0000    # 175.0f

    const/high16 v5, 0x430b0000    # 139.0f

    const/high16 v4, 0x42be0000    # 95.0f

    const/high16 v3, 0x425c0000    # 55.0f

    const/4 v2, 0x0

    .line 9
    const/high16 v0, 0x41f80000    # 31.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 10
    const/high16 v0, 0x420c0000    # 35.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 11
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 12
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 13
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 14
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 15
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x43140000    # 148.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 16
    const/high16 v0, 0x431b0000    # 155.0f

    const/high16 v1, 0x43110000    # 145.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 17
    const/high16 v0, 0x43110000    # 145.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 18
    const/high16 v0, 0x43430000    # 195.0f

    const/high16 v1, 0x43140000    # 148.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 19
    const/high16 v0, 0x43570000    # 215.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 20
    const/high16 v0, 0x436b0000    # 235.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 21
    const/high16 v0, 0x437f0000    # 255.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 22
    const v0, 0x43898000    # 275.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 23
    const v0, 0x43938000    # 295.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    const/high16 v0, 0x42140000    # 37.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 25
    const/high16 v0, 0x420c0000    # 35.0f

    const/high16 v1, 0x43030000    # 131.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 26
    invoke-virtual {p0, p1, v3, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 27
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x43060000    # 134.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 28
    const/high16 v0, 0x43000000    # 128.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 29
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x43060000    # 134.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 30
    const/high16 v0, 0x43070000    # 135.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 31
    const/high16 v0, 0x431b0000    # 155.0f

    const/high16 v1, 0x43060000    # 134.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 32
    const/high16 v0, 0x43000000    # 128.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 33
    const/high16 v0, 0x43430000    # 195.0f

    const/high16 v1, 0x43060000    # 134.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 34
    const/high16 v0, 0x43570000    # 215.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 35
    const/high16 v0, 0x436b0000    # 235.0f

    const/high16 v1, 0x43060000    # 134.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 36
    const/high16 v0, 0x437f0000    # 255.0f

    const/high16 v1, 0x43000000    # 128.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 37
    const v0, 0x43898000    # 275.0f

    const/high16 v1, 0x43060000    # 134.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 38
    const v0, 0x43938000    # 295.0f

    const/high16 v1, 0x43000000    # 128.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 39
    :cond_2
    const/high16 v0, 0x422c0000    # 43.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_3

    .line 40
    const/high16 v0, 0x420c0000    # 35.0f

    const/high16 v1, 0x42ea0000    # 117.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 41
    const/high16 v0, 0x43010000    # 129.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 42
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x430d0000    # 141.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 43
    const/high16 v0, 0x43010000    # 129.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 44
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42ea0000    # 117.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 45
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x43010000    # 129.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 46
    const/high16 v0, 0x431b0000    # 155.0f

    const/high16 v1, 0x430d0000    # 141.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 47
    const/high16 v0, 0x43010000    # 129.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 48
    const/high16 v0, 0x43430000    # 195.0f

    const/high16 v1, 0x42ea0000    # 117.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 49
    const/high16 v0, 0x43570000    # 215.0f

    const/high16 v1, 0x43010000    # 129.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 50
    const/high16 v0, 0x436b0000    # 235.0f

    const/high16 v1, 0x430d0000    # 141.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 51
    const/high16 v0, 0x437f0000    # 255.0f

    const/high16 v1, 0x43010000    # 129.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 52
    const v0, 0x43898000    # 275.0f

    const/high16 v1, 0x430d0000    # 141.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 53
    const v0, 0x43938000    # 295.0f

    const/high16 v1, 0x42ea0000    # 117.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 55
    const/16 v0, 0x23

    const/16 v1, 0x96

    const/16 v2, 0x140

    const/16 v3, 0xaa

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 56
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 58
    :cond_3
    const/high16 v0, 0x42440000    # 49.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    .line 59
    const/high16 v0, 0x420c0000    # 35.0f

    const/high16 v1, 0x42e00000    # 112.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 60
    const/high16 v0, 0x42ec0000    # 118.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 61
    const/high16 v0, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 62
    const/high16 v0, 0x42e00000    # 112.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 63
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42ec0000    # 118.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 64
    const/high16 v0, 0x43070000    # 135.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 65
    const/high16 v0, 0x431b0000    # 155.0f

    const/high16 v1, 0x42ec0000    # 118.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 66
    const/high16 v0, 0x42d40000    # 106.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 67
    invoke-virtual {p0, p1, v6, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 68
    const/high16 v0, 0x43430000    # 195.0f

    const/high16 v1, 0x42ec0000    # 118.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 69
    const/high16 v0, 0x43570000    # 215.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 70
    const/high16 v0, 0x436b0000    # 235.0f

    const/high16 v1, 0x42ec0000    # 118.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 71
    const/high16 v0, 0x437f0000    # 255.0f

    const/high16 v1, 0x42d40000    # 106.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 72
    const v0, 0x43898000    # 275.0f

    const/high16 v1, 0x42ec0000    # 118.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 73
    const v0, 0x43938000    # 295.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 74
    const/16 v0, 0x23

    const/16 v1, 0x8c

    const/16 v2, 0x140

    const/16 v3, 0xaa

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 75
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 77
    :cond_4
    cmpg-float v0, p3, v3

    if-gez v0, :cond_5

    .line 78
    const/high16 v0, 0x420c0000    # 35.0f

    const/high16 v1, 0x42d60000    # 107.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 79
    const/high16 v0, 0x42ee0000    # 119.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 80
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x42d60000    # 107.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 81
    const/high16 v0, 0x42ee0000    # 119.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 82
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42d60000    # 107.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 83
    const/high16 v0, 0x43070000    # 135.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 84
    const/high16 v0, 0x431b0000    # 155.0f

    const/high16 v1, 0x42d60000    # 107.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 85
    const/high16 v0, 0x42e20000    # 113.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 86
    const/high16 v0, 0x43430000    # 195.0f

    const/high16 v1, 0x42d60000    # 107.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 87
    const/high16 v0, 0x43570000    # 215.0f

    const/high16 v1, 0x42e20000    # 113.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 88
    const/high16 v0, 0x436b0000    # 235.0f

    const/high16 v1, 0x42ee0000    # 119.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 89
    const/high16 v0, 0x437f0000    # 255.0f

    const/high16 v1, 0x42e20000    # 113.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 90
    const v0, 0x43898000    # 275.0f

    const/high16 v1, 0x42d60000    # 107.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 91
    const v0, 0x43938000    # 295.0f

    const/high16 v1, 0x42d60000    # 107.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 93
    const/16 v0, 0x23

    const/16 v1, 0x8c

    const/16 v2, 0x140

    const/16 v3, 0xaa

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 94
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 96
    :cond_5
    const/high16 v0, 0x42740000    # 61.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6

    .line 97
    const/high16 v0, 0x420c0000    # 35.0f

    const/high16 v1, 0x42c00000    # 96.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 98
    const/high16 v0, 0x42d80000    # 108.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 99
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x42d80000    # 108.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 100
    const/high16 v0, 0x42c00000    # 96.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 101
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42a80000    # 84.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 102
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x42c00000    # 96.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 103
    const/high16 v0, 0x431b0000    # 155.0f

    const/high16 v1, 0x42d80000    # 108.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 104
    const/high16 v0, 0x42d80000    # 108.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 105
    const/high16 v0, 0x43430000    # 195.0f

    const/high16 v1, 0x42c00000    # 96.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 106
    const/high16 v0, 0x43570000    # 215.0f

    const/high16 v1, 0x42a80000    # 84.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 107
    const/high16 v0, 0x436b0000    # 235.0f

    const/high16 v1, 0x42c00000    # 96.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 108
    const/high16 v0, 0x437f0000    # 255.0f

    const/high16 v1, 0x42d80000    # 108.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 109
    const v0, 0x43898000    # 275.0f

    const/high16 v1, 0x42d80000    # 108.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 110
    const v0, 0x43938000    # 295.0f

    const/high16 v1, 0x42c00000    # 96.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 112
    const/16 v0, 0x23

    const/16 v1, 0x78

    const/16 v2, 0x140

    const/16 v3, 0xaa

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 113
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 114
    :cond_6
    const/high16 v0, 0x42860000    # 67.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_7

    .line 115
    const/high16 v0, 0x420c0000    # 35.0f

    const/high16 v1, 0x42920000    # 73.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 116
    const/high16 v0, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 117
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x42c20000    # 97.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 118
    const/high16 v0, 0x42c20000    # 97.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 119
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 120
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x42920000    # 73.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 121
    const/high16 v0, 0x431b0000    # 155.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 122
    const/high16 v0, 0x42c20000    # 97.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 123
    const/high16 v0, 0x43430000    # 195.0f

    const/high16 v1, 0x42c20000    # 97.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 124
    const/high16 v0, 0x43570000    # 215.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 125
    const/high16 v0, 0x436b0000    # 235.0f

    const/high16 v1, 0x42920000    # 73.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 126
    const/high16 v0, 0x437f0000    # 255.0f

    const/high16 v1, 0x42920000    # 73.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 127
    const v0, 0x43898000    # 275.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 128
    const v0, 0x43938000    # 295.0f

    const/high16 v1, 0x42c20000    # 97.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 130
    const/16 v0, 0x23

    const/16 v1, 0x64

    const/16 v2, 0x140

    const/16 v3, 0xaa

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 131
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 132
    :cond_7
    const/high16 v0, 0x42920000    # 73.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_8

    .line 133
    const/high16 v0, 0x420c0000    # 35.0f

    const/high16 v1, 0x42ac0000    # 86.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 134
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 135
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x42940000    # 74.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 136
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 137
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42ac0000    # 86.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 138
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x42940000    # 74.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 139
    const/high16 v0, 0x431b0000    # 155.0f

    const/high16 v1, 0x42780000    # 62.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 140
    const/high16 v0, 0x42780000    # 62.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 141
    const/high16 v0, 0x43430000    # 195.0f

    const/high16 v1, 0x42940000    # 74.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 142
    const/high16 v0, 0x43570000    # 215.0f

    const/high16 v1, 0x42ac0000    # 86.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 143
    const/high16 v0, 0x436b0000    # 235.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 144
    const/high16 v0, 0x437f0000    # 255.0f

    const/high16 v1, 0x42940000    # 74.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 145
    const v0, 0x43898000    # 275.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 146
    const v0, 0x43938000    # 295.0f

    const/high16 v1, 0x42ac0000    # 86.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 148
    const/16 v0, 0x23

    const/16 v1, 0x5a

    const/16 v2, 0x140

    const/16 v3, 0xaa

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 149
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 150
    :cond_8
    const/high16 v0, 0x429e0000    # 79.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_9

    .line 151
    const/high16 v0, 0x420c0000    # 35.0f

    const/high16 v1, 0x42600000    # 56.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 152
    const/high16 v0, 0x424c0000    # 51.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 153
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x424c0000    # 51.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 154
    const/high16 v0, 0x42920000    # 73.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 155
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42600000    # 56.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 156
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x424c0000    # 51.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 157
    const/high16 v0, 0x431b0000    # 155.0f

    const/high16 v1, 0x42600000    # 56.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 158
    const/high16 v0, 0x42920000    # 73.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 159
    const/high16 v0, 0x43430000    # 195.0f

    const/high16 v1, 0x424c0000    # 51.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 160
    const/high16 v0, 0x43570000    # 215.0f

    const/high16 v1, 0x424c0000    # 51.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 161
    const/high16 v0, 0x436b0000    # 235.0f

    const/high16 v1, 0x42920000    # 73.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 162
    const/high16 v0, 0x437f0000    # 255.0f

    const/high16 v1, 0x42600000    # 56.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 163
    const v0, 0x43898000    # 275.0f

    const/high16 v1, 0x424c0000    # 51.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 164
    const v0, 0x43938000    # 295.0f

    const/high16 v1, 0x42600000    # 56.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 166
    const/16 v0, 0x23

    const/16 v1, 0x50

    const/16 v2, 0x140

    const/16 v3, 0xaa

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 167
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 168
    :cond_9
    const/high16 v0, 0x429e0000    # 79.0f

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 170
    const/16 v0, 0x23

    const/16 v1, 0x28

    const/16 v2, 0x140

    const/16 v3, 0xaa

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 171
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method
