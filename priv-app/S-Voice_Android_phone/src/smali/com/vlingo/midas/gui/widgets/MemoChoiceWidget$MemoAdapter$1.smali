.class Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$1;
.super Ljava/lang/Object;
.source "MemoChoiceWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;I)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;

    iput p2, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 184
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 185
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    const-string/jumbo v2, "choice"

    iget v3, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$1;->val$position:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 187
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;

    # getter for: Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->memoList:Ljava/util/List;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->access$100(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;)Ljava/util/List;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$1;->val$position:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/memo/Memo;

    .line 188
    .local v1, "memo":Lcom/vlingo/core/internal/memo/Memo;
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 189
    return-void
.end method
