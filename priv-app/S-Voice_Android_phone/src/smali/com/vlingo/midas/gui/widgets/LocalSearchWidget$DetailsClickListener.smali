.class Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;
.super Ljava/lang/Object;
.source "LocalSearchWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetailsClickListener"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;
    }
.end annotation


# instance fields
.field layoutReviews:Landroid/widget/LinearLayout;

.field private listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

.field listingProgressBar:Landroid/widget/ProgressBar;

.field private originalProvider:Ljava/lang/String;

.field providerView:Landroid/widget/TextView;

.field textReviews:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)V
    .locals 1
    .param p2, "listing"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .line 181
    invoke-virtual {p2}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getProvider()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->originalProvider:Ljava/lang/String;

    .line 182
    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->originalProvider:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->originalProvider:Ljava/lang/String;

    return-object p1
.end method

.method private getDetailLocalSearchView()Landroid/view/View;
    .locals 33

    .prologue
    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v30

    sget v31, Lcom/vlingo/midas/R$layout;->item_local_search_detail:I

    const/16 v32, 0x0

    invoke-virtual/range {v30 .. v32}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    .line 197
    .local v12, "detailView":Landroid/view/View;
    sget v30, Lcom/vlingo/midas/R$id;->text_title:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/TextView;

    .line 198
    .local v25, "titleView":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->text_tagline:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    .line 199
    .local v24, "taglineView":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->text_address_line1:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 200
    .local v5, "address1View":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->text_address_line2:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 201
    .local v6, "address2View":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->text_distance:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 202
    .local v14, "distanceView":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->text_sponsored:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 203
    .local v22, "sponsoredView":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->text_phone_number:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 205
    .local v20, "phoneNumberView":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->text_synopsis:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 207
    .local v23, "synopsisView":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->ratings:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/RatingBar;

    .line 209
    .local v21, "ratingsView":Landroid/widget/RatingBar;
    sget v30, Lcom/vlingo/midas/R$id;->progress_listing:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/ProgressBar;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listingProgressBar:Landroid/widget/ProgressBar;

    .line 211
    sget v30, Lcom/vlingo/midas/R$id;->button_call:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    .line 212
    .local v7, "buttonCall":Landroid/widget/ImageButton;
    sget v30, Lcom/vlingo/midas/R$id;->button_map:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    .line 213
    .local v8, "buttonMap":Landroid/widget/ImageButton;
    sget v30, Lcom/vlingo/midas/R$id;->button_navigate:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageButton;

    .line 214
    .local v9, "buttonNavigate":Landroid/widget/ImageButton;
    sget v30, Lcom/vlingo/midas/R$id;->button_web:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageButton;

    .line 216
    .local v11, "buttonWeb":Landroid/widget/ImageButton;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setAppCallIcon()V

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppCallIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    if-eqz v30, :cond_0

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppCallIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 220
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setAppMapIcon()V

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppMapIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    if-eqz v30, :cond_1

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppMapIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v8, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 224
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setAppNavigationIcon()V

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    if-eqz v30, :cond_2

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 228
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setAppWebIcon()V

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppWebIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    if-eqz v30, :cond_3

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppWebIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 232
    :cond_3
    sget v30, Lcom/vlingo/midas/R$id;->text_label1:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    .line 233
    .local v26, "viewcall":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->text_label2:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/TextView;

    .line 234
    .local v27, "viewmap":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->text_label3:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/TextView;

    .line 235
    .local v28, "viewnavigate":Landroid/widget/TextView;
    sget v30, Lcom/vlingo/midas/R$id;->text_label4:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/TextView;

    .line 237
    .local v29, "viewweb":Landroid/widget/TextView;
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->isENaviEnabled()Z

    move-result v30

    if-nez v30, :cond_9

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v30

    if-eqz v30, :cond_9

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasBaiduMaps()Z

    move-result v30

    if-nez v30, :cond_9

    .line 239
    if-eqz v8, :cond_4

    .line 240
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v8, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 241
    :cond_4
    if-eqz v9, :cond_5

    .line 242
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 243
    :cond_5
    if-eqz v27, :cond_6

    .line 244
    const/16 v30, 0x8

    move-object/from16 v0, v27

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 245
    :cond_6
    if-eqz v28, :cond_7

    .line 246
    const/16 v30, 0x8

    move-object/from16 v0, v28

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 269
    :cond_7
    :goto_0
    sget v30, Lcom/vlingo/midas/R$id;->layout_action_buttons_2:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    .line 270
    .local v18, "layoutSecondaryButtons":Landroid/widget/LinearLayout;
    sget v30, Lcom/vlingo/midas/R$id;->layout_action_button_labels_2:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    .line 272
    .local v17, "layoutSecondaryButtonCaptions":Landroid/widget/LinearLayout;
    sget v30, Lcom/vlingo/midas/R$id;->button_reserve:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    .line 274
    .local v10, "buttonReserve":Landroid/widget/ImageButton;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getName()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    const/4 v15, 0x0

    .line 281
    .local v15, "hasDetails":Z
    sget v30, Lcom/vlingo/midas/R$id;->layout_details:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    .line 283
    .local v16, "layoutDetails":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getCaption()Ljava/lang/String;

    move-result-object v30

    if-eqz v30, :cond_c

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getCaption()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    const/16 v30, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 286
    const/4 v15, 0x1

    .line 291
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getSynopsis()Ljava/lang/String;

    move-result-object v30

    if-eqz v30, :cond_d

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getSynopsis()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    const/16 v30, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 294
    const/4 v15, 0x1

    .line 299
    :goto_2
    if-eqz v15, :cond_e

    .line 300
    const/16 v30, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 306
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getReserveUrl()Ljava/lang/String;

    move-result-object v30

    if-eqz v30, :cond_f

    .line 308
    const/16 v30, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 309
    const/16 v30, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 315
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getAddressLine1()Ljava/lang/String;

    move-result-object v3

    .line 316
    .local v3, "addr1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getAddressLine2()Ljava/lang/String;

    move-result-object v4

    .line 317
    .local v4, "addr2":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getDistanceString()Ljava/lang/String;

    move-result-object v13

    .line 319
    .local v13, "distance":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_10

    .line 320
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 325
    :goto_5
    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_11

    .line 326
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 331
    :goto_6
    invoke-static {v13}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_12

    .line 332
    invoke-virtual {v14, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 338
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getRating()D

    move-result-wide v30

    move-wide/from16 v0, v30

    double-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v21

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->isSponsored()Z

    move-result v30

    if-eqz v30, :cond_13

    .line 341
    const/16 v30, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 346
    :goto_8
    const/16 v30, 0x8

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 350
    new-instance v30, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$1;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)V

    move-object/from16 v0, v30

    invoke-virtual {v8, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 376
    new-instance v30, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)V

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 404
    new-instance v30, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$3;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$3;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)V

    move-object/from16 v0, v30

    invoke-virtual {v10, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getPhoneNumber()Ljava/lang/String;

    move-result-object v30

    if-eqz v30, :cond_15

    invoke-static {}, Lcom/vlingo/core/internal/util/CorePackageInfoProvider;->hasDialing()Z

    move-result v30

    if-eqz v30, :cond_15

    .line 421
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppCallIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    if-eqz v30, :cond_14

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppCallIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 425
    :goto_9
    new-instance v30, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getPhoneNumber()Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v30 .. v32}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 437
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getClickUrl()Ljava/lang/String;

    move-result-object v30

    if-nez v30, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getUrl()Ljava/lang/String;

    move-result-object v30

    if-eqz v30, :cond_18

    .line 438
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppWebIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    if-eqz v30, :cond_17

    .line 439
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppWebIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 442
    :goto_b
    new-instance v30, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$5;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$5;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)V

    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 473
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listingProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v30, v0

    const/16 v31, 0x8

    invoke-virtual/range {v30 .. v31}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 474
    sget v30, Lcom/vlingo/midas/R$id;->text_reviews:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/TextView;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->textReviews:Landroid/widget/TextView;

    .line 475
    sget v30, Lcom/vlingo/midas/R$id;->layout_reviews:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/LinearLayout;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->layoutReviews:Landroid/widget/LinearLayout;

    .line 476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->layoutReviews:Landroid/widget/LinearLayout;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 478
    sget v30, Lcom/vlingo/midas/R$id;->text_provider:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/TextView;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->providerView:Landroid/widget/TextView;

    .line 479
    return-object v12

    .line 250
    .end local v3    # "addr1":Ljava/lang/String;
    .end local v4    # "addr2":Ljava/lang/String;
    .end local v10    # "buttonReserve":Landroid/widget/ImageButton;
    .end local v13    # "distance":Ljava/lang/String;
    .end local v15    # "hasDetails":Z
    .end local v16    # "layoutDetails":Landroid/widget/LinearLayout;
    .end local v17    # "layoutSecondaryButtonCaptions":Landroid/widget/LinearLayout;
    .end local v18    # "layoutSecondaryButtons":Landroid/widget/LinearLayout;
    :cond_9
    sget v30, Lcom/vlingo/midas/R$id;->text_provider:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/TextView;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->providerView:Landroid/widget/TextView;

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v31, v0

    sget v30, Lcom/vlingo/midas/R$id;->chineselogo_localsearch:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/ImageView;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->china_localsearch:Landroid/widget/ImageView;

    .line 252
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v30

    if-nez v30, :cond_b

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->china_localsearch:Landroid/widget/ImageView;

    move-object/from16 v30, v0

    if-eqz v30, :cond_a

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->china_localsearch:Landroid/widget/ImageView;

    move-object/from16 v30, v0

    const/16 v31, 0x8

    invoke-virtual/range {v30 .. v31}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 256
    :cond_a
    new-instance v19, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v30, -0x2

    const/16 v31, -0x2

    move-object/from16 v0, v19

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 257
    .local v19, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v30, 0x3

    sget v31, Lcom/vlingo/midas/R$id;->layout_reviews:I

    move-object/from16 v0, v19

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->providerView:Landroid/widget/TextView;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 262
    .end local v19    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_b
    sget v30, Lcom/vlingo/midas/R$id;->text_provider:I

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/TextView;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->providerView:Landroid/widget/TextView;

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->providerView:Landroid/widget/TextView;

    move-object/from16 v30, v0

    if-eqz v30, :cond_7

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->providerView:Landroid/widget/TextView;

    move-object/from16 v30, v0

    const/16 v31, 0x8

    invoke-virtual/range {v30 .. v31}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 288
    .restart local v10    # "buttonReserve":Landroid/widget/ImageButton;
    .restart local v15    # "hasDetails":Z
    .restart local v16    # "layoutDetails":Landroid/widget/LinearLayout;
    .restart local v17    # "layoutSecondaryButtonCaptions":Landroid/widget/LinearLayout;
    .restart local v18    # "layoutSecondaryButtons":Landroid/widget/LinearLayout;
    :cond_c
    const/16 v30, 0x8

    move-object/from16 v0, v24

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 296
    :cond_d
    const/16 v30, 0x8

    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 303
    :cond_e
    const/16 v30, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_3

    .line 311
    :cond_f
    const/16 v30, 0x8

    move-object/from16 v0, v18

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 312
    const/16 v30, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_4

    .line 323
    .restart local v3    # "addr1":Ljava/lang/String;
    .restart local v4    # "addr2":Ljava/lang/String;
    .restart local v13    # "distance":Ljava/lang/String;
    :cond_10
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 329
    :cond_11
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 335
    :cond_12
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 343
    :cond_13
    const/16 v30, 0x8

    move-object/from16 v0, v22

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    .line 424
    :cond_14
    sget v30, Lcom/vlingo/midas/R$drawable;->btn_call:I

    move/from16 v0, v30

    invoke-virtual {v7, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_9

    .line 428
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppCallIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    if-eqz v30, :cond_16

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppCallIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->convertToGrayscale(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 432
    :goto_d
    new-instance v30, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$4;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$4;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)V

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_a

    .line 431
    :cond_16
    sget v30, Lcom/vlingo/midas/R$drawable;->btn_call_disabled:I

    move/from16 v0, v30

    invoke-virtual {v7, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_d

    .line 441
    :cond_17
    sget v30, Lcom/vlingo/midas/R$drawable;->btn_browser:I

    move/from16 v0, v30

    invoke-virtual {v11, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_b

    .line 463
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppWebIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    if-eqz v30, :cond_19

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    move-object/from16 v30, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;
    invoke-static/range {v30 .. v30}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getAppWebIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->convertToGrayscale(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 467
    :goto_e
    new-instance v30, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$6;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$6;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)V

    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_c

    .line 466
    :cond_19
    sget v30, Lcom/vlingo/midas/R$drawable;->btn_browser_disabled:I

    move/from16 v0, v30

    invoke-virtual {v11, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_e
.end method


# virtual methods
.method protected convertToGrayscale(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 484
    new-instance v1, Landroid/graphics/ColorMatrix;

    invoke-direct {v1}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 485
    .local v1, "matrix":Landroid/graphics/ColorMatrix;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 487
    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v0, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 489
    .local v0, "filter":Landroid/graphics/ColorMatrixColorFilter;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 491
    return-object p1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "convertView"    # Landroid/view/View;

    .prologue
    .line 186
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    new-instance v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v4}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Landroid/content/Context;)V

    # setter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;
    invoke-static {v1, v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$002(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;)Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    .line 187
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$400(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    new-instance v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;
    invoke-static {v4}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$000(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;Landroid/app/Dialog;)V

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->executeDetailRequest(Lcom/vlingo/core/internal/localsearch/LocalSearchListing;Lcom/vlingo/core/internal/vcs/WidgetResponseReceivedListener;)V

    .line 188
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->getDetailLocalSearchView()Landroid/view/View;

    move-result-object v0

    .line 189
    .local v0, "detailView":Landroid/view/View;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 190
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$000(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;->requestWindowFeature(I)Z

    .line 191
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$000(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;->setContentView(Landroid/view/View;)V

    .line 192
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$000(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;->show()V

    .line 193
    return-void
.end method
