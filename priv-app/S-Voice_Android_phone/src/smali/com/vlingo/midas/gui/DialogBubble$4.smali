.class Lcom/vlingo/midas/gui/DialogBubble$4;
.super Ljava/lang/Object;
.source "DialogBubble.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogBubble;

.field final synthetic val$text:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogBubble;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogBubble$4;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    iput-object p2, p0, Lcom/vlingo/midas/gui/DialogBubble$4;->val$text:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 250
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$4;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$500(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/EditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble$4;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogBubble;->access$500(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/EditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogBubble$4;->val$text:Ljava/lang/String;

    invoke-interface {v0, v3, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 251
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$4;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogBubble;->setTalkbackString(Z)V

    .line 252
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$4;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$500(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/EditText;

    move-result-object v0

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 266
    return-void
.end method
