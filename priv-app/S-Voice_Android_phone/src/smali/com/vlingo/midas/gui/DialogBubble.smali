.class public Lcom/vlingo/midas/gui/DialogBubble;
.super Landroid/widget/LinearLayout;
.source "DialogBubble.java"

# interfaces
.implements Lcom/vlingo/midas/gui/OnEditListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/DialogBubble$7;,
        Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;,
        Lcom/vlingo/midas/gui/DialogBubble$AnimationTask;,
        Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;,
        Lcom/vlingo/midas/gui/DialogBubble$TabletType;,
        Lcom/vlingo/midas/gui/DialogBubble$BubbleType;
    }
.end annotation


# static fields
.field private static final ANIM_SET_TEXT_COLOUR:I = 0x2

.field private static final FADEOUT_ANIM_DURATION:I = 0x14d

.field private static final INPUT_MAX_LENGTH:I = 0x96

.field private static final SCALE_ANIMATION_END:I = 0x1

.field private static final SCALE_ANIM_DURATION:I = 0x12c


# instance fields
.field private final DELAY_SHOW_TEXT:I

.field bTutorialMode:Z

.field bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

.field private bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

.field private bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

.field private bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;

.field private currentTablet:Lcom/vlingo/midas/gui/DialogBubble$TabletType;

.field density:I

.field private fillScreen:Z

.field private mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

.field private mHandler:Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;

.field private mIsReplaceAvailable:Z

.field private mIsSetInputFilter:Z

.field private mListener:Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;

.field private msg:Landroid/os/Message;

.field private orgText:Ljava/lang/String;

.field private screenDensityDPI:F

.field private startTime:J

.field private text:Ljava/lang/String;

.field private textContainer:Landroid/widget/LinearLayout;

.field private timer:Ljava/util/Timer;

.field private type:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

.field private user_body:Landroid/widget/LinearLayout;

.field private vlingo_body:Landroid/widget/LinearLayout;

.field private wakeup_body:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const/16 v0, 0x12c

    iput v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->DELAY_SHOW_TEXT:I

    .line 79
    sget-object v0, Lcom/vlingo/midas/gui/DialogBubble$TabletType;->OTHERS:Lcom/vlingo/midas/gui/DialogBubble$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->currentTablet:Lcom/vlingo/midas/gui/DialogBubble$TabletType;

    .line 117
    new-instance v0, Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;-><init>(Lcom/vlingo/midas/gui/DialogBubble;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mHandler:Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;

    .line 600
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bTutorialMode:Z

    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/DialogBubble;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mListener:Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/DialogBubble$BubbleType;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->type:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mHandler:Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/gui/DialogBubble;)Landroid/os/Message;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->msg:Landroid/os/Message;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/midas/gui/DialogBubble;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method private setDimensionsIfSantos()V
    .locals 7

    .prologue
    const/high16 v6, 0x428c0000    # 70.0f

    const/high16 v5, 0x41800000    # 16.0f

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/high16 v3, 0x43200000    # 160.0f

    .line 277
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->currentTablet:Lcom/vlingo/midas/gui/DialogBubble$TabletType;

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/DialogBubble$TabletType;

    if-ne v0, v1, :cond_2

    .line 279
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_5

    .line 281
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 283
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_3

    .line 284
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v6

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 285
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x13

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 291
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 295
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_4

    .line 296
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v6

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 297
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x5

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 303
    :goto_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 334
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 335
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 336
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_9

    .line 337
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x6e

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 338
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x42ce0000    # 103.0f

    iget v2, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 339
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x3

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 345
    :goto_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 349
    :cond_2
    return-void

    .line 287
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 288
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v6

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 289
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x13

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto/16 :goto_0

    .line 299
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v6

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 300
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 301
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x5

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto :goto_1

    .line 306
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_1

    .line 308
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    .line 309
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 310
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_7

    .line 311
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 312
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x13

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 318
    :goto_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 320
    :cond_6
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 322
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_8

    .line 323
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 324
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x5

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 330
    :goto_5
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 314
    :cond_7
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 315
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 316
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x13

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto :goto_4

    .line 326
    :cond_8
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 327
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 328
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x5

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto :goto_5

    .line 341
    :cond_9
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 342
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x40000000    # 2.0f

    iget v2, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 343
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x3

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto/16 :goto_3
.end method

.method private submitTextRequest()V
    .locals 3

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogBubble;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 512
    .local v0, "dialogState":Ljava/lang/Object;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->startUserFlow(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 513
    return-void
.end method


# virtual methods
.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->type:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    return-object v0
.end method

.method public initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V
    .locals 4
    .param p1, "type"    # Lcom/vlingo/midas/gui/DialogBubble$BubbleType;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 163
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogBubble;->type:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    .line 164
    iput-object p2, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    .line 166
    sget-object v0, Lcom/vlingo/midas/gui/DialogBubble$7;->$SwitchMap$com$vlingo$midas$gui$DialogBubble$BubbleType:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 243
    sget v0, Lcom/vlingo/midas/R$id;->dialog_bubble_user_s:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->textContainer:Landroid/widget/LinearLayout;

    .line 245
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->user_body:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    new-instance v1, Lcom/vlingo/midas/gui/DialogBubble$4;

    invoke-direct {v1, p0, p2}, Lcom/vlingo/midas/gui/DialogBubble$4;-><init>(Lcom/vlingo/midas/gui/DialogBubble;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->post(Ljava/lang/Runnable;)Z

    .line 268
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setImportantForAccessibility(I)V

    .line 270
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v2}, Lcom/vlingo/midas/gui/DialogBubble;->startBubbleAnimation(FF)V

    .line 274
    :goto_0
    return-void

    .line 172
    :pswitch_0
    sget v0, Lcom/vlingo/midas/R$id;->dialog_bubble_vlingo_s:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->textContainer:Landroid/widget/LinearLayout;

    .line 174
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->vlingo_body:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    new-instance v1, Lcom/vlingo/midas/gui/DialogBubble$1;

    invoke-direct {v1, p0, p2}, Lcom/vlingo/midas/gui/DialogBubble$1;-><init>(Lcom/vlingo/midas/gui/DialogBubble;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->post(Ljava/lang/Runnable;)Z

    .line 186
    invoke-virtual {p0, v2, v2}, Lcom/vlingo/midas/gui/DialogBubble;->startBubbleAnimation(FF)V

    goto :goto_0

    .line 190
    :pswitch_1
    sget v0, Lcom/vlingo/midas/R$id;->dialog_bubble_wakeup_s:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->textContainer:Landroid/widget/LinearLayout;

    .line 193
    const-string/jumbo v0, "car_word_spotter_enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->wakeup_body:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 198
    :goto_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;

    new-instance v1, Lcom/vlingo/midas/gui/DialogBubble$2;

    invoke-direct {v1, p0, p2}, Lcom/vlingo/midas/gui/DialogBubble$2;-><init>(Lcom/vlingo/midas/gui/DialogBubble;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->post(Ljava/lang/Runnable;)Z

    .line 223
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;

    new-instance v1, Lcom/vlingo/midas/gui/DialogBubble$3;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/DialogBubble$3;-><init>(Lcom/vlingo/midas/gui/DialogBubble;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 239
    invoke-virtual {p0, v2, v2}, Lcom/vlingo/midas/gui/DialogBubble;->startBubbleAnimation(FF)V

    goto :goto_0

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->wakeup_body:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 166
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isFillScreen()Z
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->fillScreen:Z

    return v0
.end method

.method public isNeededRefresh(Ljava/lang/String;)Z
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 593
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 594
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595
    const/4 v0, 0x0

    .line 597
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isReplaceAvailable()Z
    .locals 1

    .prologue
    .line 410
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mIsReplaceAvailable:Z

    return v0
.end method

.method public isUserTextReadyToTouch()Z
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->isReadyToTouch()Z

    move-result v0

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 423
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 424
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogBubble;->setDimensionsIfSantos()V

    .line 425
    return-void
.end method

.method public onEditCanceled(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 496
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->orgText:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 498
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->orgText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 499
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/DialogBubble;->setEditable(Z)V

    .line 506
    :cond_1
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 507
    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    .line 508
    :cond_2
    return-void

    .line 501
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eq v0, p0, :cond_1

    .line 502
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    invoke-interface {v0, p0}, Lcom/vlingo/midas/gui/OnEditListener;->onEditCanceled(Landroid/view/View;)V

    .line 503
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/DialogBubble;->setEditable(Z)V

    goto :goto_0
.end method

.method public onEditFinished(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 461
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 492
    :goto_0
    return-void

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->orgText:Ljava/lang/String;

    .line 466
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->orgText:Ljava/lang/String;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 469
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    .line 472
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bTutorialMode:Z

    if-nez v0, :cond_1

    .line 473
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogBubble;->submitTextRequest()V

    .line 480
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eqz v0, :cond_2

    .line 481
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    invoke-interface {v0, p0}, Lcom/vlingo/midas/gui/OnEditListener;->onEditFinished(Landroid/view/View;)V

    .line 490
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->isEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->setTalkbackString(Z)V

    .line 491
    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    goto :goto_0

    .line 484
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->orgText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 485
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->setEditable(Z)V

    .line 486
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eqz v0, :cond_2

    .line 487
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    invoke-interface {v0, p0}, Lcom/vlingo/midas/gui/OnEditListener;->onEditCanceled(Landroid/view/View;)V

    goto :goto_1
.end method

.method public onEditStarted(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 430
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->mIsSetInputFilter:Z

    if-nez v1, :cond_1

    .line 432
    const/16 v0, 0x96

    .line 433
    .local v0, "max":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x96

    if-lt v1, v2, :cond_0

    .line 434
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit16 v0, v1, 0x96

    .line 437
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    new-array v2, v5, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, v0}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 439
    iput-boolean v5, p0, Lcom/vlingo/midas/gui/DialogBubble;->mIsSetInputFilter:Z

    .line 441
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->orgText:Ljava/lang/String;

    .line 445
    .end local v0    # "max":I
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    .line 447
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/EditText;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/EditText;->setSelection(I)V

    .line 450
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 451
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn(Z)V

    .line 453
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eqz v1, :cond_2

    .line 454
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    invoke-interface {v1, p0}, Lcom/vlingo/midas/gui/OnEditListener;->onEditStarted(Landroid/view/View;)V

    .line 456
    :cond_2
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->screenDensityDPI:F

    .line 131
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_1

    .line 132
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/DialogBubble$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/DialogBubble$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->currentTablet:Lcom/vlingo/midas/gui/DialogBubble$TabletType;

    .line 136
    :cond_1
    sget v0, Lcom/vlingo/midas/R$id;->prompt_vlingo_textview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;

    .line 138
    sget v0, Lcom/vlingo/midas/R$id;->prompt_user_textview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/EditText;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    .line 139
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    if-eqz v0, :cond_2

    .line 140
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/EditText;->setOnEditListener(Lcom/vlingo/midas/gui/OnEditListener;)V

    .line 141
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    const-string/jumbo v1, "disableVoiceInput=true;disableEmoticonInput=true"

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setImeOptions(I)V

    .line 145
    :cond_2
    sget v0, Lcom/vlingo/midas/R$id;->prompt_wakeup_textview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;

    .line 146
    sget v0, Lcom/vlingo/midas/R$id;->full_container_dialogbubble:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->textContainer:Landroid/widget/LinearLayout;

    .line 147
    sget v0, Lcom/vlingo/midas/R$id;->vlingo_body:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->vlingo_body:Landroid/widget/LinearLayout;

    .line 148
    sget v0, Lcom/vlingo/midas/R$id;->user_body:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->user_body:Landroid/widget/LinearLayout;

    .line 149
    sget v0, Lcom/vlingo/midas/R$id;->wakeup_body:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->wakeup_body:Landroid/widget/LinearLayout;

    .line 150
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogBubble;->setDimensionsIfSantos()V

    .line 151
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogBubble;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int/lit16 v0, v0, 0xa0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->density:I

    .line 152
    return-void
.end method

.method public replaceOriginalText()V
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->orgText:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogBubble;->setText(Ljava/lang/String;)V

    .line 536
    return-void
.end method

.method public saveParam(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "type"    # Lcom/vlingo/midas/gui/DialogBubble$BubbleType;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "fillScreen"    # Z

    .prologue
    .line 155
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogBubble;->type:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    .line 156
    iput-object p2, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    .line 157
    iput-boolean p3, p0, Lcom/vlingo/midas/gui/DialogBubble;->fillScreen:Z

    .line 158
    return-void
.end method

.method public setEditable(Z)V
    .locals 2
    .param p1, "isEditable"    # Z

    .prologue
    .line 414
    if-eqz p1, :cond_0

    .line 415
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setEnabled(Z)V

    .line 419
    :goto_0
    return-void

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setOnEditListener(Lcom/vlingo/midas/gui/OnEditListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/midas/gui/OnEditListener;

    .prologue
    .line 394
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogBubble;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    .line 395
    return-void
.end method

.method public setOnSlideTextListenr(Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;

    .prologue
    .line 390
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogBubble;->mListener:Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;

    .line 391
    return-void
.end method

.method public setReplaceAvailable(Z)V
    .locals 0
    .param p1, "replace"    # Z

    .prologue
    .line 528
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/DialogBubble;->mIsReplaceAvailable:Z

    .line 529
    return-void
.end method

.method public setTalkbackString(Z)V
    .locals 4
    .param p1, "editable"    # Z

    .prologue
    .line 570
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    new-instance v1, Lcom/vlingo/midas/gui/DialogBubble$6;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/midas/gui/DialogBubble$6;-><init>(Lcom/vlingo/midas/gui/DialogBubble;Z)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/midas/gui/customviews/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 590
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 516
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;

    .line 517
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 518
    return-void
.end method

.method public setTutorialMode()V
    .locals 1

    .prologue
    .line 602
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bTutorialMode:Z

    .line 603
    return-void
.end method

.method public setType(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;)V
    .locals 0
    .param p1, "type"    # Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    .prologue
    .line 521
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogBubble;->type:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    .line 522
    return-void
.end method

.method public setUserTextReadyToTouch()V
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/EditText;->setReadyToTouch()V

    .line 560
    return-void
.end method

.method public startBubbleAnimation(FF)V
    .locals 13
    .param p1, "pivotX"    # F
    .param p2, "pivotY"    # F

    .prologue
    const-wide/16 v11, 0x12c

    const v1, 0x3dcccccd    # 0.1f

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 355
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v6, p1

    move v7, v5

    move v8, p2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 358
    .local v0, "scaleAnimVlingo":Landroid/view/animation/ScaleAnimation;
    invoke-virtual {v0, v11, v12}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 359
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    invoke-direct {v10, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 360
    .local v10, "fadeOut":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v10, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 361
    new-instance v9, Landroid/view/animation/AnimationSet;

    invoke-direct {v9, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 362
    .local v9, "animSetVlingo":Landroid/view/animation/AnimationSet;
    new-instance v1, Lcom/vlingo/midas/gui/DialogBubble$5;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/DialogBubble$5;-><init>(Lcom/vlingo/midas/gui/DialogBubble;)V

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 379
    invoke-virtual {v9, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 380
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 381
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 382
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->textContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 383
    invoke-virtual {v9}, Landroid/view/animation/AnimationSet;->getStartTime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->startTime:J

    .line 384
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->mHandler:Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->msg:Landroid/os/Message;

    .line 385
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->timer:Ljava/util/Timer;

    .line 386
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble;->timer:Ljava/util/Timer;

    new-instance v2, Lcom/vlingo/midas/gui/DialogBubble$AnimationTask;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/DialogBubble$AnimationTask;-><init>(Lcom/vlingo/midas/gui/DialogBubble;)V

    iget-wide v3, p0, Lcom/vlingo/midas/gui/DialogBubble;->startTime:J

    add-long/2addr v3, v11

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 387
    return-void
.end method
