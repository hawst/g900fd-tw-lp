.class public abstract Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
.super Landroid/support/v4/app/Fragment;
.source "ControlFragmentEWYSHelp.java"

# interfaces
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;,
        Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;,
        Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;,
        Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;,
        Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;,
        Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;
    }
.end annotation


# static fields
.field public static final BT_START_REC_DELAY_TIME:J = 0x7d0L

.field public static LOG_TAG:Ljava/lang/String;

.field private static final MSG_START_SPOTTER:I


# instance fields
.field public DBG:Z

.field private audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;

.field protected callback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

.field private mActivityCreated:Z

.field private mActivityPaused:Z

.field private mHandler:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;

.field private mKm:Landroid/app/KeyguardManager;

.field private mPhraseSpotted:Ljava/lang/String;

.field private mTaskOnFinishGreetingTTS:Ljava/lang/Runnable;

.field private m_isRecognitionPostBTConnect:Z

.field private playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-string/jumbo v0, "ControlFragmentEWYSHelp"

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 59
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->DBG:Z

    .line 71
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mActivityCreated:Z

    .line 72
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mActivityPaused:Z

    .line 76
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->m_isRecognitionPostBTConnect:Z

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mPhraseSpotted:Ljava/lang/String;

    .line 173
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;-><init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mHandler:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;

    .line 625
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;)Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;

    return-object v0
.end method

.method public static getRandom(II)I
    .locals 6
    .param p0, "minValue"    # I
    .param p1, "maxValue"    # I

    .prologue
    .line 678
    int-to-double v0, p0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    add-int/lit8 v4, p1, 0x1

    sub-int/2addr v4, p0

    int-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private getTTSOnIdleList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 714
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getTTSOnIdleList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private isActivityPaused()Z
    .locals 1

    .prologue
    .line 730
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mActivityPaused:Z

    return v0
.end method

.method private isStartRecoOnSpotterStop()Z
    .locals 1

    .prologue
    .line 698
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->isStartRecoOnSpotterStop()Z

    move-result v0

    return v0
.end method

.method private setPaused(Z)V
    .locals 1
    .param p1, "isPaused"    # Z

    .prologue
    .line 710
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->setPaused(Z)V

    .line 711
    return-void
.end method

.method private setStartRecoOnSpotterStop(Z)V
    .locals 1
    .param p1, "startRecoOnSpotterStop"    # Z

    .prologue
    .line 702
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->setStartRecoOnSpotterStop(Z)V

    .line 703
    return-void
.end method

.method private setTTSOnIdleList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 718
    .local p1, "mTTSOnIdleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->setTTSOnIdleList(Ljava/util/ArrayList;)V

    .line 719
    return-void
.end method


# virtual methods
.method public announceTTS(Ljava/lang/String;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 632
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 633
    return-void
.end method

.method public announceTTSOnDialogIdle(Ljava/lang/String;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 636
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->announceTTS(Ljava/lang/String;)V

    .line 642
    :goto_0
    return-void

    .line 640
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getTTSOnIdleList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public avoidWakeUpCommandTTS()V
    .locals 2

    .prologue
    .line 301
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->stopSpotting()V

    .line 303
    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->scheduleStartSpotter(J)V

    .line 305
    :cond_0
    return-void
.end method

.method public cancelTTS()V
    .locals 1

    .prologue
    .line 645
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 646
    return-void
.end method

.method protected disableHelpWidgetButton()V
    .locals 0

    .prologue
    .line 662
    return-void
.end method

.method protected enableHelpWidgetButton()V
    .locals 0

    .prologue
    .line 660
    return-void
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 473
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method public getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentGuideState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;
    .locals 1

    .prologue
    .line 690
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getCurrentGuideState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;
    .locals 1

    .prologue
    .line 682
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v0

    return-object v0
.end method

.method public getPhraseSpotted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 742
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mPhraseSpotted:Ljava/lang/String;

    return-object v0
.end method

.method public getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 221
    const-string/jumbo v1, "samsung_wakeup_engine_enable"

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->DBG:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "initPhraseSpotter Samsung Seamless and Samsung Multiple Wake-up"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 223
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v2, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 224
    invoke-static {}, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->getPhraseSpotterParameters()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 233
    .local v0, "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :goto_0
    return-object v0

    .line 225
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_1
    const-string/jumbo v1, "samsung_multi_engine_enable"

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 226
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->DBG:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "initPhraseSpotter Sensory Seamless and Samsung Multiple Wake-up"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_2
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v2, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 228
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    goto :goto_0

    .line 230
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_3
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->DBG:Z

    if-eqz v1, :cond_4

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "initPhraseSpotter Sensory Seamless Only"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    goto :goto_0
.end method

.method protected getVoicePrompt()Lcom/vlingo/core/internal/settings/VoicePrompt;
    .locals 3

    .prologue
    .line 656
    new-instance v0, Lcom/vlingo/core/internal/settings/VoicePrompt;

    invoke-direct {v0}, Lcom/vlingo/core/internal/settings/VoicePrompt;-><init>()V

    new-instance v1, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->registerDelegate(Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    move-result-object v0

    return-object v0
.end method

.method protected initPhraseSpotter()V
    .locals 2

    .prologue
    .line 215
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 216
    return-void
.end method

.method public isActivityCreated()Z
    .locals 1

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->ismActivityCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 706
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->isPaused()Z

    move-result v0

    return v0
.end method

.method protected ismActivityCreated()Z
    .locals 1

    .prologue
    .line 722
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mActivityCreated:Z

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 180
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 181
    invoke-static {}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->getInstance()Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    .line 182
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-string/jumbo v5, "keyguard"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/KeyguardManager;

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mKm:Landroid/app/KeyguardManager;

    .line 184
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 185
    .local v2, "res":Landroid/content/res/Resources;
    const-string/jumbo v4, "car_word_spotter_enabled"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 187
    .local v3, "spottingEnabled":Z
    new-instance v4, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$1;)V

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;

    .line 188
    new-instance v4, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$1;

    invoke-direct {v4, p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$1;-><init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;)V

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 205
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 206
    .local v0, "inf":Landroid/content/IntentFilter;
    const-string/jumbo v4, "com.vlingo.client.app.action.AUDIO_FOCUS_CHANGED"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;

    invoke-virtual {v4, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 209
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 210
    .local v1, "playMusicIntentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v4, "com.sec.android.app.music.musicservicecommand.pause"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 212
    return-void
.end method

.method public abstract onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 282
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 284
    iput-object v2, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 287
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 288
    iput-object v2, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 290
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 291
    return-void
.end method

.method public onIdle()V
    .locals 0

    .prologue
    .line 670
    return-void
.end method

.method public onLanguageChanged()V
    .locals 0

    .prologue
    .line 669
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 238
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 239
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setPaused(Z)V

    .line 241
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->destroy()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :goto_0
    return-void

    .line 242
    :catch_0
    move-exception v0

    .line 243
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 0
    .param p1, "phrase"    # Ljava/lang/String;

    .prologue
    .line 507
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mPhraseSpotted:Ljava/lang/String;

    .line 508
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 6
    .param p1, "phrase"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, -0x1

    .line 514
    const/4 v0, -0x1

    .line 515
    .local v0, "iResult":I
    const/4 v1, -0x1

    .line 517
    .local v1, "intType":I
    invoke-static {p1}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->isCustomWakeupWordPhrase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 518
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 523
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 550
    :goto_1
    if-eq v1, v5, :cond_6

    .line 552
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/voiceshell/MultipleWakeUp;->getMultipleWakeUpIntent(ILandroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 553
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 554
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->startActivity(Landroid/content/Intent;)V

    .line 555
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->stopSpotting()V

    .line 562
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_2
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    .line 563
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 564
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 565
    return-void

    .line 520
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 525
    :pswitch_0
    invoke-direct {p0, v4}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setStartRecoOnSpotterStop(Z)V

    goto :goto_1

    .line 528
    :pswitch_1
    invoke-direct {p0, v4}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setStartRecoOnSpotterStop(Z)V

    goto :goto_1

    .line 531
    :pswitch_2
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->DBG:Z

    if-eqz v3, :cond_2

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v4, "onPhraseSpotted(), command 1"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    :cond_2
    const-string/jumbo v3, "kew_wake_up_and_auto_function1"

    invoke-static {v3, v5}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 533
    goto :goto_1

    .line 535
    :pswitch_3
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->DBG:Z

    if-eqz v3, :cond_3

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v4, "onPhraseSpotted(), command 2"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    :cond_3
    const-string/jumbo v3, "kew_wake_up_and_auto_function2"

    invoke-static {v3, v5}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 537
    goto :goto_1

    .line 539
    :pswitch_4
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->DBG:Z

    if-eqz v3, :cond_4

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v4, "onPhraseSpotted(), command 3"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    :cond_4
    const-string/jumbo v3, "kew_wake_up_and_auto_function3"

    invoke-static {v3, v5}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 541
    goto :goto_1

    .line 543
    :pswitch_5
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->DBG:Z

    if-eqz v3, :cond_5

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v4, "onPhraseSpotted(), command 4"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    :cond_5
    const-string/jumbo v3, "kew_wake_up_and_auto_function4"

    invoke-static {v3, v5}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 545
    goto/16 :goto_1

    .line 559
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->stopSpotting()V

    goto :goto_2

    .line 523
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onPhraseSpotterStarted()V
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->callback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    invoke-interface {v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;->onStartSpotting()V

    .line 503
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 1

    .prologue
    .line 484
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isStartRecoOnSpotterStop()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 485
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setStartRecoOnSpotterStop(Z)V

    .line 486
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$2;-><init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 498
    :goto_0
    return-void

    .line 493
    :cond_0
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$3;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$3;-><init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 255
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 256
    invoke-direct {p0, v3}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setPaused(Z)V

    .line 258
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    if-ne v1, v2, :cond_0

    .line 259
    const-string/jumbo v1, "key_social_login_attemp_for_resume"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 260
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 261
    .local v0, "fa":Landroid/support/v4/app/FragmentActivity;
    instance-of v1, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    if-nez v1, :cond_0

    .line 262
    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->scheduleStartSpotter(J)V

    .line 269
    .end local v0    # "fa":Landroid/support/v4/app/FragmentActivity;
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setPrompt()V

    .line 270
    return-void

    .line 265
    :cond_1
    const-string/jumbo v1, "key_social_login_attemp_for_resume"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 1
    .param p1, "phrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 571
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->stopSpotting()V

    .line 573
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$4;

    invoke-direct {v0, p0, p2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$4;-><init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 578
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 276
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 277
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 278
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 249
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 250
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 251
    return-void
.end method

.method abstract performClickFromDriveControl()Z
.end method

.method protected resetThinkingState()V
    .locals 0

    .prologue
    .line 667
    return-void
.end method

.method public scheduleStartSpotter(J)V
    .locals 2
    .param p1, "delay"    # J

    .prologue
    const/4 v1, 0x0

    .line 308
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mHandler:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;->removeMessages(I)V

    .line 309
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mHandler:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;

    invoke-virtual {v0, v1, p1, p2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 310
    return-void
.end method

.method public setActivityPaused(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 734
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mActivityPaused:Z

    .line 735
    return-void
.end method

.method public abstract setBlueHeight(I)V
.end method

.method protected setButtonIdle(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 663
    return-void
.end method

.method protected setButtonListening()V
    .locals 0

    .prologue
    .line 664
    return-void
.end method

.method protected setButtonThinking()V
    .locals 0

    .prologue
    .line 665
    return-void
.end method

.method public setCallback(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    .prologue
    .line 622
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->callback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    .line 623
    return-void
.end method

.method public setCurrentGuideState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;)V
    .locals 1
    .param p1, "currentState"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    .prologue
    .line 694
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->setCurrentState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;)V

    .line 695
    return-void
.end method

.method public setCurrentState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V
    .locals 1
    .param p1, "currentState"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    .prologue
    .line 686
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentDataEWYSHelp;->setCurrentState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 687
    return-void
.end method

.method setGuideState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;)V
    .locals 2
    .param p1, "newState"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isActivityCreated()Z

    move-result v0

    if-nez v0, :cond_1

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isActivityPaused()Z

    move-result v0

    if-nez v0, :cond_0

    .line 402
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$GuideState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 415
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setCurrentGuideState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;)V

    goto :goto_0

    .line 402
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected setPrompt()V
    .locals 0

    .prologue
    .line 272
    return-void
.end method

.method protected setResponseMode()V
    .locals 0

    .prologue
    .line 668
    return-void
.end method

.method setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V
    .locals 5
    .param p1, "newState"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 314
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isActivityCreated()Z

    move-result v1

    if-nez v1, :cond_0

    .line 392
    :goto_0
    return-void

    .line 317
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isActivityPaused()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 318
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 319
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    goto :goto_0

    .line 323
    :cond_1
    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$ControlState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 391
    :cond_2
    :goto_1
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setCurrentState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    goto :goto_0

    .line 325
    :pswitch_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mTaskOnFinishGreetingTTS:Ljava/lang/Runnable;

    if-eqz v1, :cond_3

    .line 326
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mTaskOnFinishGreetingTTS:Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 327
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mTaskOnFinishGreetingTTS:Ljava/lang/Runnable;

    goto :goto_0

    .line 331
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->enableHelpWidgetButton()V

    .line 332
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->resetThinkingState()V

    .line 333
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setButtonIdle(Z)V

    .line 335
    :goto_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getTTSOnIdleList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 336
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getTTSOnIdleList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 337
    .local v0, "tts":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->announceTTS(Ljava/lang/String;)V

    goto :goto_2

    .line 340
    .end local v0    # "tts":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 346
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 347
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    if-eq v1, v2, :cond_5

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isPaused()Z

    move-result v1

    if-nez v1, :cond_5

    .line 348
    const-wide/16 v1, 0x258

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->scheduleStartSpotter(J)V

    .line 350
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    invoke-interface {v1}, Lcom/vlingo/core/internal/display/WakeLockManager;->releaseWakeLock()V

    goto :goto_1

    .line 353
    :pswitch_1
    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setButtonIdle(Z)V

    .line 354
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setButtonListening()V

    .line 355
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    invoke-interface {v1}, Lcom/vlingo/core/internal/display/WakeLockManager;->acquireWakeLock()V

    goto :goto_1

    .line 358
    :pswitch_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setButtonListening()V

    .line 359
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->disableHelpWidgetButton()V

    .line 360
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->resetThinkingState()V

    .line 364
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    invoke-interface {v1}, Lcom/vlingo/core/internal/display/WakeLockManager;->acquireWakeLock()V

    goto :goto_1

    .line 367
    :pswitch_3
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->disableHelpWidgetButton()V

    .line 368
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setButtonThinking()V

    .line 369
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->resetThinkingState()V

    .line 370
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setThinkingAnimation()V

    .line 371
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    invoke-interface {v1}, Lcom/vlingo/core/internal/display/WakeLockManager;->acquireWakeLock()V

    goto/16 :goto_1

    .line 374
    :pswitch_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->resetThinkingState()V

    .line 375
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setResponseMode()V

    goto/16 :goto_1

    .line 378
    :pswitch_5
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->stopSpotting()V

    goto/16 :goto_1

    .line 381
    :pswitch_6
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 382
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->stopSpotting()V

    .line 384
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->resetThinkingState()V

    .line 385
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    if-eq v1, v2, :cond_2

    .line 386
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setButtonIdle(Z)V

    goto/16 :goto_1

    .line 323
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setTaskOnFinishGreetingTTS(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 738
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mTaskOnFinishGreetingTTS:Ljava/lang/Runnable;

    .line 739
    return-void
.end method

.method protected setThinkingAnimation()V
    .locals 0

    .prologue
    .line 666
    return-void
.end method

.method protected setmActivityCreated(Z)V
    .locals 0
    .param p1, "mActivityCreated"    # Z

    .prologue
    .line 726
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mActivityCreated:Z

    .line 727
    return-void
.end method

.method public abstract showRMSChange(I)V
.end method

.method public abstract showSpectrum([I)V
.end method

.method public startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 7
    .param p1, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 420
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isPaused()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 470
    :cond_0
    :goto_0
    return-void

    .line 423
    :cond_1
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->m_isRecognitionPostBTConnect:Z

    if-nez v3, :cond_0

    .line 427
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isActivityCreated()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 431
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->callback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    invoke-interface {v3}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;->onRecognitionStarted()V

    .line 433
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 434
    invoke-direct {p0, v5}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setStartRecoOnSpotterStop(Z)V

    .line 435
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->stopSpotting()V

    goto :goto_0

    .line 441
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 443
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 444
    .local v0, "baseContext":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    .line 445
    .local v2, "isBtHeadsetConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v1

    .line 446
    .local v1, "isBluetoothAudioSupported":Z
    if-ne v2, v5, :cond_3

    if-eq v1, v5, :cond_5

    .line 447
    :cond_3
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4, v6}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 458
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 459
    if-ne v2, v5, :cond_4

    if-eq v1, v5, :cond_0

    .line 460
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    instance-of v3, v3, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    if-eqz v3, :cond_6

    .line 461
    sget-object v3, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    goto :goto_0

    .line 449
    :cond_5
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v3

    const/4 v4, 0x6

    invoke-virtual {v3, v4, v6}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 455
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startScoOnStartRecognition()V

    goto :goto_1

    .line 463
    :cond_6
    sget-object v3, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    goto :goto_0

    .line 467
    :cond_7
    sget-object v3, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    goto :goto_0
.end method

.method public startSpotting()V
    .locals 4

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setPrompt()V

    .line 586
    new-instance v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$5;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$5;-><init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;)V

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 592
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mKm:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 598
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v1

    if-nez v1, :cond_0

    .line 599
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->startPhraseSpotting()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 604
    :catch_0
    move-exception v0

    .line 605
    .local v0, "e":Ljava/lang/IllegalStateException;
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->DBG:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected stopSpotting()V
    .locals 2

    .prologue
    .line 613
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->mHandler:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;->removeMessages(I)V

    .line 614
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 615
    return-void
.end method
