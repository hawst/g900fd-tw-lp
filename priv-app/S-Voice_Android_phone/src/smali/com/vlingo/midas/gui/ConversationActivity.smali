.class public Lcom/vlingo/midas/gui/ConversationActivity;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "ConversationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;
.implements Lcom/vlingo/core/internal/audio/MicAnimationListener;
.implements Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;
.implements Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;
.implements Lcom/vlingo/core/internal/util/ADMFeatureListener;
.implements Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/ConversationActivity$25;,
        Lcom/vlingo/midas/gui/ConversationActivity$AdminReceiver;,
        Lcom/vlingo/midas/gui/ConversationActivity$IncomingCallReceiver;,
        Lcom/vlingo/midas/gui/ConversationActivity$ENaviResolveLocationReciever;,
        Lcom/vlingo/midas/gui/ConversationActivity$UiFocusBroadcastReceiver;,
        Lcom/vlingo/midas/gui/ConversationActivity$TimeWidgetBroadcastReceiver;,
        Lcom/vlingo/midas/gui/ConversationActivity$HelpWidgetBroadcastReceiver;,
        Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;,
        Lcom/vlingo/midas/gui/ConversationActivity$AlarmConceptCheck;,
        Lcom/vlingo/midas/gui/ConversationActivity$Task;,
        Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;,
        Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;,
        Lcom/vlingo/midas/gui/ConversationActivity$TabletType;
    }
.end annotation


# static fields
.field public static final ACTION_SAFEREADER_LAUNCH:Ljava/lang/String; = "ACTION_SAFEREADER_LAUNCH"

.field private static final ACTION_SEC_HELP:Ljava/lang/String; = "com.samsung.helphub.HELP"

.field public static final AUTO_LISTEN:Ljava/lang/String; = "AUTO_LISTEN"

.field public static final DISPLAY_FROM_BACKGROUND:Ljava/lang/String; = "displayFromBackground"

.field private static final DRIVING_WAKE_LOCK_NAME:Ljava/lang/String; = "SVoiceDrivingScreenSaver"

.field public static final EXTRA_MESSAGE_LIST:Ljava/lang/String; = "EXTRA_MESSAGE_LIST"

.field public static final EXTRA_TIME_SENT:Ljava/lang/String; = "EXTRA_TIME_SENT"

.field private static final FILE_PICKER_RESULT:I = 0x1

.field private static final FINISH_SVOICE_COMMAND:I = 0xf

.field private static final HANDSFREE_SECURED_LOCK_ENABLE_POP:Ljava/lang/String; = "handsfree_secured_lock_enable_pop"

.field private static final HANDSFREE_SECURED_LOCK_POP:Ljava/lang/String; = "handsfree_secured_lock_pop"

.field private static final HELP_APPLICATION_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.helphub"

.field public static final IS_FIRST_LAUNCH:Ljava/lang/String; = "IS_FIRST_LAUNCH"

.field private static final KEY_VOICE_PROMPT_PREFERENCE:Ljava/lang/String; = "voice_prompt_preference_value"

.field public static final LOCKSCREEN_BIOMETRIC_WEAK_FALLBACK:Ljava/lang/String; = "lockscreen.biometric_weak_fallback"

.field static LOG_TAG:Ljava/lang/String; = null

.field private static final MSG_ADD_BUBBLE:I = 0x0

.field private static final MSG_ALL_LOGIN_FLOW:I = 0xb

.field private static final MSG_CHECK_ABNORMAL_ONPAUSE:I = 0x13

.field private static final MSG_CLEAR_DISMISS_KEYGUARD_FLAG:I = 0x3

.field private static final MSG_DELAYED_VOICEWAKEUP_STOP:I = 0x11

.field private static final MSG_DISABLE_MIC_FOR_TOTORIAL:I = 0x22b

.field private static final MSG_DISMISS_KEYGUARD:I = 0x12

.field private static final MSG_ENABLE_MIC_AFTER_TUTORIAL:I = 0xe

.field private static final MSG_FACEBOOK_LOGIN_FLOW:I = 0x6

.field private static final MSG_HIDE_MIC_FOR_TUTORIAL:I = 0x22c

.field private static final MSG_LCD_OFF_ALWAYSMICON:I = 0x8

.field private static final MSG_REMOVE_LOCATION_LISTENER:I = 0x4

.field private static final MSG_SHOW_MIC_LAYOUT:I = 0x14

.field private static final MSG_START_RECORDING:I = 0x2

.field private static final MSG_START_SPOTTING:I = 0x1

.field private static final MSG_STOP_MIC_ANIMATION:I = 0xd

.field private static final MSG_TWITTER_LOGIN_FLOW:I = 0x7

.field private static final MSG_WEIBO_LOGIN_FLOW:I = 0x9

.field private static final NORMAL_LAUNCH_COUNT:Ljava/lang/String; = "normal_launch_count"

.field private static final OPEN_SVOICE_COMMAND:I = 0x10

.field public static final PRESENT_EDITOR:Ljava/lang/String; = "PRESENT_EDITOR"

.field public static final RESUME_FROM_BACKGROUND:Ljava/lang/String; = "resumeFromBackground"

.field public static final SAFEREADER_INTENT_EXPIRATION_TIMEOUT:J = 0x1388L

.field public static final SEND_ACTION:Ljava/lang/String; = "com.vlingo.midas.action.SEND"

.field public static final SHOW_LIST_ACTION:Ljava/lang/String; = "SHOW_LIST_ACTION"

.field private static final VERIFICATION_LOG_TAG:Ljava/lang/String; = "VerificationLog"

.field private static final VOICE_WAKE_UP_DO_NOT_SHOW_AGAIN_PREF_FILENAME:Ljava/lang/String; = "preferences_voice_wake_up"

.field public static controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

.field public static dayType:I

.field public static doLaunchTutorial:Z

.field public static isAfterTutorial:Z

.field public static isSvoiceLaunchedFromDriveLink:Z

.field public static isTutorial:Z

.field static isTutorialStartedFromIUX:Z

.field private static isVoicePromptOn:Z

.field private static islatestMusicWidget:Z

.field static latestWidget:Lcom/vlingo/midas/gui/Widget;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vlingo/midas/gui/Widget",
            "<*>;"
        }
    .end annotation
.end field

.field public static lockTheDevice:I

.field public static mDPM:Landroid/app/admin/DevicePolicyManager;

.field public static mDeviceAdminName:Landroid/content/ComponentName;

.field private static mIsStartedCustomWakeUpSetting:Z

.field private static mTheme:I

.field private static smCurrentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

.field public static smart:Z

.field public static threadVar:Z

.field public static titlebar:Landroid/app/ActionBar;

.field public static turnScreenOff:Z


# instance fields
.field private final MAIN_MENU_WHAT_CAN_I_SAY:I

.field private final MENU_DELETE_PERSONAL_DATA:I

.field private final MENU_HELP:I

.field private final MENU_HELP_DIALOG:I

.field private final MENU_LAUNCH_HELP_APPLICATION:I

.field private final MENU_TUTORIAL:I

.field private final MENU_USE_SPEAKER:I

.field private final MENU_VOICE_PROMPT_OFF:I

.field private final MENU_VOICE_PROMPT_ON:I

.field final MENU_YOUCANSAY:I

.field private final TAG:Ljava/lang/String;

.field private actionReceived:Ljava/lang/String;

.field private asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

.field private audioManager:Landroid/media/AudioManager;

.field private autoStartOnResume:Z

.field private bDrivingMode:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

.field private checkForAbnormalonPause:Z

.field private contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

.field private controlContainer:Landroid/view/View;

.field controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

.field private controlViewContainer:Landroid/view/View;

.field private count:I

.field private coverViewGUI:Z

.field private currentTablet:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

.field cursor:Landroid/database/Cursor;

.field private dialogContainer:Landroid/view/View;

.field private dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

.field display:Landroid/view/Display;

.field private displayFromBackground:Z

.field private doneTosAndIux:Z

.field private eNaviResolveLocationReciever:Landroid/content/BroadcastReceiver;

.field public fullContainer:Landroid/widget/RelativeLayout;

.field private gestures:Landroid/view/GestureDetector;

.field private handleTextReqeust:Z

.field private hasFocus:Z

.field private helpContainer:Landroid/view/View;

.field private helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

.field private helpWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private help_layout:Landroid/widget/RelativeLayout;

.field private homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

.field public incomingCallReceiver:Landroid/content/BroadcastReceiver;

.field private isAlarmNewConcept:Landroid/content/BroadcastReceiver;

.field isFromBackPress:Z

.field private isHelpLaunched:Z

.field private isKModel:Z

.field private isKeyguardSecure:Z

.field private isPharseSpottingStoppedByTutorial:Z

.field private isWakeLockChanged:Z

.field private landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

.field launchCountPref:Landroid/content/SharedPreferences;

.field public lockInMiniMode:Z

.field private mAlwaysOnMicDetected:Z

.field private mAutoListeningmodeAlways:Z

.field mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mIsNeedToDismissSecuredLock:Z

.field public mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

.field private final mLocationListener:Landroid/location/LocationListener;

.field mScover:Lcom/samsung/android/sdk/cover/Scover;

.field mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

.field private messages:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/core/internal/safereader/SafeReaderAlert;",
            ">;"
        }
    .end annotation
.end field

.field public mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

.field public micBgMiniModeLayout:Landroid/view/View;

.field private miniModeTouchListener:Landroid/view/View$OnTouchListener;

.field private nluEventLogUtil:Lcom/vlingo/midas/util/log/NLUEventLogUtil;

.field private normalLaunchCount:I

.field private normal_layout:Landroid/widget/RelativeLayout;

.field private notificationManager:Lcom/vlingo/midas/notification/NotificationPopUpManager;

.field private observer:Lcom/vlingo/midas/gui/PBSchedulesObserver;

.field protected phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

.field prefForWishesEventAlert:Landroid/content/SharedPreferences;

.field private present_editor:Z

.field private regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

.field private releaseKeyguardByMsg:Z

.field private resumeFromBackground:Z

.field private screenDensityDPI:F

.field private screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field screenHeight:I

.field private startRecoBySCO:Z

.field private textRequest:Ljava/lang/String;

.field private timeWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private toLaunchInMiniMode:Z

.field private uiFocusBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field youcansayThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 211
    const-string/jumbo v0, "ConversationActivity"

    sput-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->LOG_TAG:Ljava/lang/String;

    .line 217
    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    .line 218
    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->doLaunchTutorial:Z

    .line 249
    sput v1, Lcom/vlingo/midas/gui/ConversationActivity;->lockTheDevice:I

    .line 257
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->threadVar:Z

    .line 264
    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    .line 265
    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorial:Z

    .line 284
    sget v0, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v0, Lcom/vlingo/midas/gui/ConversationActivity;->mTheme:I

    .line 288
    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->smart:Z

    .line 313
    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->islatestMusicWidget:Z

    .line 314
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    .line 351
    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->mIsStartedCustomWakeUpSetting:Z

    .line 5816
    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isVoicePromptOn:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 219
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->coverViewGUI:Z

    .line 248
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->lockInMiniMode:Z

    .line 266
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity$TabletType;->OTHERS:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->currentTablet:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

    .line 272
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->None:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->bDrivingMode:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    .line 285
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->handleTextReqeust:Z

    .line 286
    iput-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->textRequest:Ljava/lang/String;

    .line 287
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->present_editor:Z

    .line 308
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    .line 309
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isHelpLaunched:Z

    .line 310
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->displayFromBackground:Z

    .line 311
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->resumeFromBackground:Z

    .line 312
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isWakeLockChanged:Z

    .line 318
    const/16 v0, 0x9

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->MENU_USE_SPEAKER:I

    .line 322
    const/16 v0, 0xd

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->MENU_HELP:I

    .line 323
    const/16 v0, 0xe

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->MENU_HELP_DIALOG:I

    .line 326
    const/16 v0, 0x11

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->MAIN_MENU_WHAT_CAN_I_SAY:I

    .line 327
    const/16 v0, 0x13

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->MENU_LAUNCH_HELP_APPLICATION:I

    .line 328
    const/16 v0, 0x14

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->MENU_VOICE_PROMPT_OFF:I

    .line 329
    const/16 v0, 0x15

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->MENU_VOICE_PROMPT_ON:I

    .line 330
    const/16 v0, 0x16

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->MENU_TUTORIAL:I

    .line 335
    const/16 v0, 0x3e7

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->MENU_DELETE_PERSONAL_DATA:I

    .line 337
    const/16 v0, 0x17

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->MENU_YOUCANSAY:I

    .line 354
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isKeyguardSecure:Z

    .line 356
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->doneTosAndIux:Z

    .line 365
    iput v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normalLaunchCount:I

    .line 370
    const-string/jumbo v0, "ConversationActivity"

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->TAG:Ljava/lang/String;

    .line 399
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->startRecoBySCO:Z

    .line 400
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->releaseKeyguardByMsg:Z

    .line 401
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->hasFocus:Z

    .line 416
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->toLaunchInMiniMode:Z

    .line 423
    iput-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->display:Landroid/view/Display;

    .line 424
    iput v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->screenHeight:I

    .line 629
    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    .line 1805
    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity$5;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ConversationActivity$5;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    .line 2844
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isFromBackPress:Z

    .line 4306
    iput v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->count:I

    .line 5444
    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity$19;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ConversationActivity$19;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mLocationListener:Landroid/location/LocationListener;

    .line 5891
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isPharseSpottingStoppedByTutorial:Z

    .line 6114
    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity$24;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ConversationActivity$24;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->miniModeTouchListener:Landroid/view/View$OnTouchListener;

    .line 6135
    return-void
.end method

.method private CheckDataConnection(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 5718
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/gui/DataCheckActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5719
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "intent"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 5720
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    .line 5721
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5723
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->finish()V

    .line 5725
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->reRegisterPhraseSpottingAfterTutorial()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/ConversationActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->doDismissKeyguard()V

    return-void
.end method

.method static synthetic access$102(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 206
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setCoverLayout()V

    return-void
.end method

.method static synthetic access$1700(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setViewCoverLayout()V

    return-void
.end method

.method static synthetic access$1800(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->startOtherAppAfterAccept()V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/ConversationActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAutoListeningmodeAlways:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/vlingo/midas/gui/ConversationActivity;)Lcom/vlingo/midas/gui/DialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    return-object v0
.end method

.method static synthetic access$202(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 206
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAutoListeningmodeAlways:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/vlingo/midas/gui/ConversationActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isKModel:Z

    return v0
.end method

.method static synthetic access$2200(Lcom/vlingo/midas/gui/ConversationActivity;)Lcom/vlingo/midas/notification/NotificationPopUpManager;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->notificationManager:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 206
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->startRecoBySCO:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 206
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->present_editor:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/vlingo/midas/gui/ConversationActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 206
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->retireLastWidget()V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/ConversationActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->isAppForeground()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/ConversationActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isHelpLaunched:Z

    return v0
.end method

.method static synthetic access$402(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 206
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isHelpLaunched:Z

    return p1
.end method

.method static synthetic access$502(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 206
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->checkForAbnormalonPause:Z

    return p1
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->clearDismissKeyguardFlag()V

    return-void
.end method

.method static synthetic access$700(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->removeLocationListener()V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/midas/gui/ConversationActivity;)Lcom/vlingo/midas/gui/ContentFragment;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->stopVoiceWakeupParam()V

    return-void
.end method

.method private acquireWakeLock(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 5308
    const-string/jumbo v5, "power"

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 5317
    .local v2, "pm":Landroid/os/PowerManager;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v5

    if-eqz v5, :cond_4

    const-wide/16 v3, 0xfa0

    .line 5320
    .local v3, "timeout":J
    :goto_0
    const-string/jumbo v5, "isThisComeFromHomeKeyDoubleClickConcept"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 5323
    .local v1, "isFromDoubleClick":Z
    const-string/jumbo v5, "doForceTurnOnLCD"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 5326
    .local v0, "doForceTurnOnLCD":Z
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isWakeLockChanged:Z

    if-eqz v5, :cond_2

    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v5

    if-nez v5, :cond_2

    const-string/jumbo v5, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 5332
    :cond_2
    const v5, 0x3000001a

    const-string/jumbo v6, "VoiceTalkTemp"

    invoke-virtual {v2, v5, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 5336
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isWakeLockChanged:Z

    .line 5338
    :cond_3
    return-void

    .line 5317
    .end local v0    # "doForceTurnOnLCD":Z
    .end local v1    # "isFromDoubleClick":Z
    .end local v3    # "timeout":J
    :cond_4
    const-wide/16 v3, 0x258

    goto :goto_0
.end method

.method private cancelAllActivities()V
    .locals 2

    .prologue
    .line 5800
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 5801
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->clearUpdateType()V

    .line 5802
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->endpointReco()V

    .line 5803
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 5804
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->userCancel()V

    .line 5805
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 5806
    const-string/jumbo v0, "use_voice_prompt"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isVoicePromptOn:Z

    .line 5811
    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isVoicePromptOn:Z

    if-eqz v0, :cond_0

    .line 5812
    const-string/jumbo v0, "use_voice_prompt"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 5814
    :cond_0
    return-void
.end method

.method private checkDataUsagePopup(Landroid/content/Intent;Z)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isFromHomeKey"    # Z

    .prologue
    .line 5701
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5703
    :cond_0
    const/4 v0, 0x0

    .line 5704
    .local v0, "dialogProcessed":Z
    if-nez p2, :cond_1

    .line 5705
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/DataCheckActivity;->DIALOG_PROCESSED:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 5709
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->isAllDontAskEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 5710
    if-nez v0, :cond_2

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isProcessing()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v1

    if-nez v1, :cond_2

    .line 5711
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity;->CheckDataConnection(Landroid/content/Intent;)V

    .line 5715
    .end local v0    # "dialogProcessed":Z
    :cond_2
    return-void
.end method

.method private checkFragmentLanguage()V
    .locals 8

    .prologue
    .line 2327
    const-string/jumbo v5, "language"

    const-string/jumbo v6, "en-US"

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2329
    .local v1, "newLanguageApplication":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/ContentFragment;->getFragmentLanguage()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/ContentFragment;->getFragmentLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2332
    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/ContentFragment;->onLanguageChanged()V

    .line 2333
    sget-object v5, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v5, :cond_0

    .line 2334
    sget-object v5, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/ControlFragment;->onLanguageChanged()V

    .line 2336
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    .line 2337
    .local v2, "titlebar":Landroid/app/ActionBar;
    if-eqz v2, :cond_1

    .line 2338
    sget v5, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 2353
    .end local v2    # "titlebar":Landroid/app/ActionBar;
    :cond_1
    move-object v0, p0

    .line 2354
    .local v0, "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    if-eqz v0, :cond_2

    .line 2355
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    const v6, 0x102002c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->more_options:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2363
    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->currentTablet:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

    sget-object v6, Lcom/vlingo/midas/gui/ConversationActivity$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

    if-ne v5, v6, :cond_5

    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$bool;->help_fragment_supported:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2364
    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/HelpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/HelpFragment;->getCurrentLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2366
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    .line 2367
    .restart local v2    # "titlebar":Landroid/app/ActionBar;
    if-eqz v2, :cond_3

    .line 2368
    sget v5, Lcom/vlingo/midas/R$mipmap;->svoice:I

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setIcon(I)V

    .line 2370
    :cond_3
    if-eqz v2, :cond_4

    .line 2371
    sget v5, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 2375
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 2376
    .local v3, "transaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v3, v5}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 2377
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2379
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v5

    invoke-direct {p0, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->initFragments(Z)V

    .line 2380
    new-instance v5, Lcom/vlingo/midas/gui/HelpFragment;

    invoke-direct {v5}, Lcom/vlingo/midas/gui/HelpFragment;-><init>()V

    iput-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    .line 2381
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    .line 2382
    .local v4, "transaction1":Landroid/support/v4/app/FragmentTransaction;
    sget v5, Lcom/vlingo/midas/R$id;->help_layout:I

    iget-object v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 2383
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2386
    .end local v2    # "titlebar":Landroid/app/ActionBar;
    .end local v3    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    .end local v4    # "transaction1":Landroid/support/v4/app/FragmentTransaction;
    :cond_5
    return-void

    .line 2358
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method private clearDismissKeyguardFlag()V
    .locals 2

    .prologue
    .line 5270
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5271
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x400000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 5274
    :cond_0
    return-void
.end method

.method private configureAutoStart(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1624
    if-nez p1, :cond_1

    .line 1660
    :cond_0
    :goto_0
    return-void

    .line 1635
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isKModel:Z

    if-eqz v0, :cond_3

    :cond_2
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1637
    :cond_3
    const-string/jumbo v0, "home_auto_listen"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1643
    :cond_4
    const-string/jumbo v0, "home_auto_listen"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "AUTO_LISTEN"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "key_popup_window_opened"

    invoke-static {v0, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    .line 1648
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1653
    const-string/jumbo v0, "resumeFromBackground"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->resumeFromBackground:Z

    .line 1655
    const-string/jumbo v0, "resumeFromBackground"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "displayFromBackground"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_5
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    if-nez v0, :cond_7

    :goto_2
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->displayFromBackground:Z

    goto :goto_0

    :cond_6
    move v0, v2

    .line 1643
    goto :goto_1

    :cond_7
    move v1, v2

    .line 1655
    goto :goto_2
.end method

.method private dereferenceObjects()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2806
    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    .line 2807
    sput-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    .line 2808
    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    .line 2809
    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    .line 2810
    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    .line 2811
    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    .line 2812
    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    .line 2813
    sput-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    .line 2814
    return-void
.end method

.method private disableTransculentTheme()V
    .locals 5

    .prologue
    .line 6090
    :try_start_0
    const-class v3, Landroid/app/Activity;

    const-string/jumbo v4, "convertFromTranslucent"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 6091
    .local v0, "convertFromTranslucent":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 6092
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 6093
    const-string/jumbo v2, "ConversationActivity"

    const-string/jumbo v3, "convertFromTranslucent for call pop-up"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 6103
    .end local v0    # "convertFromTranslucent":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 6094
    :catch_0
    move-exception v1

    .line 6095
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 6096
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 6097
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 6098
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 6099
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 6100
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 6101
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private doDismissKeyguard()V
    .locals 10

    .prologue
    .line 1573
    const/4 v5, 0x0

    .line 1575
    .local v5, "serviceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    const-string/jumbo v6, "android.os.ServiceManager"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 1581
    :goto_0
    if-eqz v5, :cond_0

    .line 1582
    const/4 v3, 0x0

    .line 1584
    .local v3, "getService":Ljava/lang/reflect/Method;
    :try_start_1
    const-string/jumbo v6, "getService"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 1591
    :goto_1
    if-eqz v3, :cond_0

    .line 1592
    const/4 v1, 0x0

    .line 1594
    .local v1, "binder":Landroid/os/IBinder;
    const/4 v6, 0x0

    const/4 v7, 0x1

    :try_start_2
    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v9, "window"

    aput-object v9, v7, v8

    invoke-virtual {v3, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Landroid/os/IBinder;

    move-object v1, v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4

    .line 1606
    :goto_2
    if-eqz v1, :cond_0

    .line 1607
    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v4

    .line 1610
    .local v4, "iwm":Landroid/view/IWindowManager;
    if-eqz v4, :cond_0

    .line 1612
    :try_start_3
    invoke-interface {v4}, Landroid/view/IWindowManager;->dismissKeyguard()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_5

    .line 1621
    .end local v1    # "binder":Landroid/os/IBinder;
    .end local v3    # "getService":Ljava/lang/reflect/Method;
    .end local v4    # "iwm":Landroid/view/IWindowManager;
    :cond_0
    :goto_3
    return-void

    .line 1576
    :catch_0
    move-exception v2

    .line 1578
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1586
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v3    # "getService":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v2

    .line 1588
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 1595
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v1    # "binder":Landroid/os/IBinder;
    :catch_2
    move-exception v2

    .line 1597
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 1598
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v2

    .line 1600
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 1601
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v2

    .line 1603
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_2

    .line 1613
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v4    # "iwm":Landroid/view/IWindowManager;
    :catch_5
    move-exception v2

    .line 1615
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3
.end method

.method private enableGestures()V
    .locals 2

    .prologue
    .line 3561
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/vlingo/midas/gui/ConversationActivity$10;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/ConversationActivity$10;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->gestures:Landroid/view/GestureDetector;

    .line 3575
    return-void
.end method

.method public static getControlFragment()Lcom/vlingo/midas/gui/ControlFragment;
    .locals 1

    .prologue
    .line 5667
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    return-object v0
.end method

.method public static getLatestWidget()Lcom/vlingo/midas/gui/Widget;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/midas/gui/Widget",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 4684
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    return-object v0
.end method

.method private goToSvoiceMainScreenFromTutorial(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 5927
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    if-eqz v1, :cond_0

    .line 5928
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 5931
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 5932
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 5934
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->voice_talk_bg_tile:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 5935
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 5936
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 5938
    :cond_2
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_3

    .line 5939
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/ControlFragment;->setFakeSvoiceMiniForTutorial(Z)V

    .line 5942
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v1, :cond_4

    .line 5943
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->cancelTimer()V

    .line 5944
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    invoke-virtual {v1, v6}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 5946
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->invalidateOptionsMenu()V

    .line 5948
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 5949
    invoke-static {p1, v5}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    move-result-object v0

    .line 5953
    .local v0, "mSVoiceTutorialWidgetObj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :goto_0
    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->destroy()V

    .line 5954
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_5

    .line 5955
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/ControlFragment;->removeMicStatusTextForTutorial(Z)V

    .line 5956
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    if-eqz v1, :cond_6

    .line 5957
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0x22c

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 5958
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0x22b

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 5959
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 5961
    :cond_6
    sput-boolean v4, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    .line 5962
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_7

    .line 5963
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/16 v2, 0x4b0

    invoke-virtual {v1, v4, v2}, Lcom/vlingo/midas/gui/ControlFragment;->fadeInMicLayout(ZI)V

    .line 5964
    :cond_7
    sput-boolean v4, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialFromSettings:Z

    .line 5965
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 5966
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/DialogFragment;->resetAllContentForTutorial(Z)V

    .line 5967
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->incomingCallReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_8

    .line 5968
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->incomingCallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 5969
    iput-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->incomingCallReceiver:Landroid/content/BroadcastReceiver;

    .line 5974
    :cond_8
    sget-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isVoicePromptOn:Z

    if-eqz v1, :cond_9

    .line 5975
    const-string/jumbo v1, "use_voice_prompt"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 5977
    :cond_9
    return-void

    .line 5951
    .end local v0    # "mSVoiceTutorialWidgetObj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :cond_a
    invoke-static {p1, v5}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    move-result-object v0

    .restart local v0    # "mSVoiceTutorialWidgetObj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    goto :goto_0
.end method

.method private initFragments(Z)V
    .locals 13
    .param p1, "driveModeEnable"    # Z

    .prologue
    .line 3682
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_c

    const/4 v6, 0x1

    .line 3683
    .local v6, "isPortrait":Z
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_d

    const/4 v5, 0x1

    .line 3686
    .local v5, "isLandscape":Z
    :goto_1
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 3687
    :cond_0
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    if-nez v10, :cond_1

    .line 3688
    sget v10, Lcom/vlingo/midas/R$layout;->main_normal:I

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-static {p0, v10, v11}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3689
    sget v10, Lcom/vlingo/midas/R$id;->normal_layout:I

    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    .line 3690
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 3691
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->miniModeTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 3694
    :cond_1
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    if-nez v10, :cond_2

    .line 3695
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    sget v11, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    .line 3697
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v10, v11}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v10

    check-cast v10, Lcom/vlingo/midas/gui/DialogFragment;

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    .line 3701
    if-eqz v6, :cond_12

    .line 3703
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v10

    if-eqz v10, :cond_10

    .line 3705
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    if-nez v10, :cond_3

    .line 3706
    sget v10, Lcom/vlingo/midas/R$layout;->main_normal_control_view:I

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v10, v11}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3707
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v10, v11}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v10

    check-cast v10, Lcom/vlingo/midas/gui/RegularControlFragment;

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    .line 3708
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    invoke-virtual {v10, v11}, Lcom/vlingo/midas/gui/RegularControlFragment;->setCallback(Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;)V

    .line 3712
    :cond_3
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->help_layout:Landroid/widget/RelativeLayout;

    if-nez v10, :cond_4

    .line 3713
    sget v11, Lcom/vlingo/midas/R$layout;->help_list_view:I

    sget v10, Lcom/vlingo/midas/R$id;->full_container:I

    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    invoke-static {p0, v11, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 3714
    .local v3, "helpLayout":Landroid/view/View;
    sget v10, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->help_layout:Landroid/widget/RelativeLayout;

    .line 3717
    .end local v3    # "helpLayout":Landroid/view/View;
    :cond_4
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpContainer:Landroid/view/View;

    if-nez v10, :cond_5

    .line 3718
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->help_layout:Landroid/widget/RelativeLayout;

    sget v11, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpContainer:Landroid/view/View;

    .line 3725
    :cond_5
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    if-nez v10, :cond_6

    .line 3727
    new-instance v10, Lcom/vlingo/midas/gui/HelpFragment;

    invoke-direct {v10}, Lcom/vlingo/midas/gui/HelpFragment;-><init>()V

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    .line 3728
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v7

    .line 3729
    .local v7, "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    sget v10, Lcom/vlingo/midas/R$id;->help_layout:I

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v7, v10, v11}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 3730
    invoke-virtual {v7}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 3733
    .end local v7    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_6
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v10

    sget-object v11, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v10, v11, :cond_f

    .line 3734
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$bool;->help_fragment_supported:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 3735
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v10, 0x439d0000    # 314.0f

    iget v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->screenDensityDPI:F

    const/high16 v12, 0x43200000    # 160.0f

    div-float/2addr v11, v12

    mul-float/2addr v10, v11

    float-to-int v10, v10

    const/4 v11, -0x1

    invoke-direct {v4, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3738
    .local v4, "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v10, 0xb

    invoke-virtual {v4, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3739
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpContainer:Landroid/view/View;

    invoke-virtual {v10, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3740
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v8, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3743
    .local v8, "normalLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v10, 0x9

    invoke-virtual {v8, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3744
    const/4 v10, 0x0

    sget v11, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v8, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 3745
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v10, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3746
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    sget v11, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlViewContainer:Landroid/view/View;

    .line 3747
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v0, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3749
    .local v0, "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v10, 0xc

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3750
    const/16 v10, 0xe

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3751
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlViewContainer:Landroid/view/View;

    invoke-virtual {v10, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3752
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct {v9, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3754
    .local v9, "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v10, 0x2

    sget v11, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v9, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 3755
    const/16 v10, 0xe

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3756
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v10, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3818
    .end local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v4    # "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "normalLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_7
    :goto_2
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    sput-object v10, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    .line 3819
    sget v10, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    .line 3876
    :cond_8
    :goto_3
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    if-nez v10, :cond_9

    .line 3877
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    .line 3902
    :cond_9
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-nez v10, :cond_a

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 3903
    sget v10, Lcom/vlingo/midas/R$layout;->mic_animation_view:I

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v10, v11}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3904
    sget v10, Lcom/vlingo/midas/R$id;->mic_animation_container:I

    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/vlingo/midas/gui/customviews/ListeningView;

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    .line 3905
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->setClipChildren(Z)V

    .line 3908
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v10, :cond_a

    .line 3909
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_19

    .line 3910
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    new-instance v11, Lcom/vlingo/midas/gui/ConversationActivity$11;

    invoke-direct {v11, p0}, Lcom/vlingo/midas/gui/ConversationActivity$11;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v10, v11}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 3921
    :cond_a
    :goto_4
    if-eqz v6, :cond_1a

    .line 3922
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    if-eqz v10, :cond_b

    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v10

    if-eqz v10, :cond_b

    .line 3923
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/RegularControlFragment;->getView()Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->bringToFront()V

    .line 3932
    :cond_b
    :goto_5
    return-void

    .line 3682
    .end local v5    # "isLandscape":Z
    .end local v6    # "isPortrait":Z
    :cond_c
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 3683
    .restart local v6    # "isPortrait":Z
    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 3758
    .restart local v5    # "isLandscape":Z
    :cond_e
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v7

    .line 3759
    .restart local v7    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v7, v10}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 3760
    invoke-virtual {v7}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 3761
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    .line 3765
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    sget v11, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlViewContainer:Landroid/view/View;

    .line 3766
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v0, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3768
    .restart local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v10, 0xc

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3770
    const/16 v10, 0xe

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3771
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlViewContainer:Landroid/view/View;

    invoke-virtual {v10, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3772
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct {v9, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3774
    .restart local v9    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v10, 0x2

    sget v11, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v9, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 3776
    const/16 v10, 0xa

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3777
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v10, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 3780
    .end local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    .end local v9    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_f
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v10

    sget-object v11, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v10, v11, :cond_7

    .line 3781
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v7

    .line 3782
    .restart local v7    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v7, v10}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 3783
    invoke-virtual {v7}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 3784
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    .line 3785
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    sget v11, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlViewContainer:Landroid/view/View;

    .line 3786
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v0, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3788
    .restart local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v10, 0xc

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3789
    const/16 v10, 0x9

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3790
    const/16 v10, 0xe

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3791
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 3802
    .end local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_10
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    if-nez v10, :cond_7

    .line 3803
    sget v10, Lcom/vlingo/midas/R$layout;->main_normal_control_view:I

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v10, v11}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3804
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v10, v11}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v10

    check-cast v10, Lcom/vlingo/midas/gui/RegularControlFragment;

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    .line 3805
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    invoke-virtual {v10, v11}, Lcom/vlingo/midas/gui/RegularControlFragment;->setCallback(Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;)V

    .line 3807
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct {v9, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3809
    .restart local v9    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v10

    if-eqz v10, :cond_11

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v10

    if-eqz v10, :cond_11

    .line 3810
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v9    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v9, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3813
    .restart local v9    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_11
    const/4 v10, 0x2

    sget v11, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v9, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 3814
    const/16 v10, 0xe

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3815
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v10, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 3820
    .end local v9    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_12
    if-eqz v5, :cond_8

    .line 3821
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v10

    if-eqz v10, :cond_18

    .line 3823
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    if-nez v10, :cond_13

    .line 3824
    sget v10, Lcom/vlingo/midas/R$layout;->main_land_control_view:I

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v10, v11}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3825
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v10, v11}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v10

    check-cast v10, Lcom/vlingo/midas/gui/LandRegularControlFragment;

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    .line 3826
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    invoke-virtual {v10, v11}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->setCallback(Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;)V

    .line 3829
    :cond_13
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->help_layout:Landroid/widget/RelativeLayout;

    if-nez v10, :cond_14

    .line 3830
    sget v11, Lcom/vlingo/midas/R$layout;->help_list_view:I

    sget v10, Lcom/vlingo/midas/R$id;->full_container:I

    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    invoke-static {p0, v11, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3831
    sget v10, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->help_layout:Landroid/widget/RelativeLayout;

    .line 3833
    :cond_14
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpContainer:Landroid/view/View;

    if-nez v10, :cond_15

    .line 3834
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->help_layout:Landroid/widget/RelativeLayout;

    sget v11, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpContainer:Landroid/view/View;

    .line 3836
    :cond_15
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    if-nez v10, :cond_16

    .line 3838
    new-instance v10, Lcom/vlingo/midas/gui/HelpFragment;

    invoke-direct {v10}, Lcom/vlingo/midas/gui/HelpFragment;-><init>()V

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    .line 3839
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v7

    .line 3840
    .restart local v7    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    sget v10, Lcom/vlingo/midas/R$id;->help_layout:I

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v7, v10, v11}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 3841
    invoke-virtual {v7}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 3843
    .end local v7    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_16
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$dimen;->helpfragment_width:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v2, v10

    .line 3845
    .local v2, "helpFragmentWidth":I
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    invoke-direct {v4, v2, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3847
    .restart local v4    # "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v10, 0xb

    invoke-virtual {v4, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3848
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpContainer:Landroid/view/View;

    invoke-virtual {v10, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3849
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v8, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3852
    .restart local v8    # "normalLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v10, 0x9

    invoke-virtual {v8, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3853
    const/4 v10, 0x0

    sget v11, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v8, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 3854
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v10, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3855
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    sget v11, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlViewContainer:Landroid/view/View;

    .line 3856
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v0, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3858
    .restart local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v10, 0xc

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3859
    const/16 v10, 0xe

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3860
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlViewContainer:Landroid/view/View;

    invoke-virtual {v10, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3861
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct {v1, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3863
    .local v1, "dialogParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v10, 0x2

    sget v11, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v1, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 3864
    const/16 v10, 0xe

    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3865
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v10, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3873
    .end local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "dialogParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "helpFragmentWidth":I
    .end local v4    # "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "normalLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_17
    :goto_6
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    sput-object v10, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    .line 3874
    sget v10, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    goto/16 :goto_3

    .line 3867
    :cond_18
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    if-nez v10, :cond_17

    .line 3868
    sget v10, Lcom/vlingo/midas/R$layout;->main_land_control_view:I

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-static {p0, v10, v11}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3869
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v10, v11}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v10

    check-cast v10, Lcom/vlingo/midas/gui/LandRegularControlFragment;

    iput-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    .line 3870
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    iget-object v11, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    invoke-virtual {v10, v11}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->setCallback(Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;)V

    goto :goto_6

    .line 3916
    :cond_19
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto/16 :goto_4

    .line 3925
    :cond_1a
    if-eqz v5, :cond_b

    .line 3926
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    if-eqz v10, :cond_b

    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v10

    if-eqz v10, :cond_b

    .line 3927
    iget-object v10, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->bringToFront()V

    goto/16 :goto_5
.end method

.method private isAllDontAskEnabled()Z
    .locals 1

    .prologue
    .line 5683
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isDontAskMobileEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isDontAskWlanEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5684
    const/4 v0, 0x1

    .line 5687
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isAppForeground()Z
    .locals 5

    .prologue
    .line 6038
    const-string/jumbo v3, "activity"

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 6039
    .local v1, "mActivityManager":Landroid/app/ActivityManager;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 6041
    .local v2, "runningTask":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 6043
    .local v0, "ar":Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    return v3
.end method

.method private isForeground()Z
    .locals 5

    .prologue
    .line 6028
    const-string/jumbo v3, "activity"

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 6029
    .local v1, "mActivityManager":Landroid/app/ActivityManager;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 6031
    .local v2, "runningTask":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 6033
    .local v0, "ar":Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    return v3
.end method

.method private isHelpApplicationExist()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5637
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 5638
    .local v3, "pm":Landroid/content/pm/PackageManager;
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v7, "com.samsung.helphub.HELP"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 5641
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 5662
    :cond_0
    :goto_0
    return v4

    .line 5644
    :cond_1
    const/4 v2, 0x0

    .line 5647
    .local v2, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string/jumbo v7, "com.samsung.helphub"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 5653
    if-eqz v2, :cond_0

    .line 5654
    iget v6, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-eq v6, v5, :cond_0

    .line 5657
    iget v6, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v7, 0x2

    if-lt v6, v7, :cond_0

    move v4, v5

    .line 5658
    goto :goto_0

    .line 5649
    :catch_0
    move-exception v1

    .line 5650
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private isTalkbackEnabledInSettings()Z
    .locals 2

    .prologue
    .line 2304
    const-string/jumbo v1, "accessibility"

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 2305
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method public static isTutorialOn()Z
    .locals 1

    .prologue
    .line 5884
    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    return v0
.end method

.method private needToDismissLockScreen()Z
    .locals 2

    .prologue
    .line 5298
    const-string/jumbo v1, "keyguard"

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 5299
    .local v0, "keyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    return v1
.end method

.method private onResumeFromBackground(Z)V
    .locals 7
    .param p1, "resume"    # Z

    .prologue
    .line 5574
    invoke-static {}, Lcom/vlingo/midas/phrasespotter/SeamlessRecoService;->getDialogObjects()Ljava/util/ArrayList;

    move-result-object v1

    .line 5576
    .local v1, "dialogObjects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;>;"
    if-nez v1, :cond_1

    .line 5591
    :cond_0
    :goto_0
    return-void

    .line 5579
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;

    .line 5580
    .local v0, "d":Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;
    invoke-interface {v0, p0}, Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;->execute(Lcom/vlingo/midas/gui/ConversationActivity;)V

    goto :goto_1

    .line 5582
    .end local v0    # "d":Lcom/vlingo/midas/phrasespotter/dialog/DialogObject;
    :cond_2
    if-eqz p1, :cond_0

    .line 5583
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 5584
    .local v2, "handler":Landroid/os/Handler;
    new-instance v4, Lcom/vlingo/midas/gui/ConversationActivity$20;

    invoke-direct {v4, p0}, Lcom/vlingo/midas/gui/ConversationActivity$20;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    const-wide/16 v5, 0x12c

    invoke-virtual {v2, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private processSafeReaderMessages(Landroid/content/Intent;)Z
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 5199
    iput-boolean v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    .line 5201
    const-string/jumbo v4, "EXTRA_TIME_SENT"

    const-wide/16 v5, 0x0

    invoke-virtual {p1, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    .line 5204
    .local v1, "timeSent":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v1

    const-wide/16 v6, 0x1388

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 5205
    const-string/jumbo v3, "EXTRA_MESSAGE_LIST"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 5207
    .local v0, "replyMessages":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->messages:Ljava/util/LinkedList;

    .line 5209
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->messages:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 5210
    const/4 v3, 0x1

    .line 5212
    .end local v0    # "replyMessages":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    :cond_0
    return v3
.end method

.method private reRegisterPhraseSpottingAfterTutorial()V
    .locals 2

    .prologue
    .line 5980
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isPharseSpottingStoppedByTutorial:Z

    if-eqz v0, :cond_0

    .line 5982
    const-string/jumbo v0, "car_word_spotter_enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 5983
    sget-object v0, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v1, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 5984
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isPharseSpottingStoppedByTutorial:Z

    .line 5986
    :cond_0
    return-void
.end method

.method private removeLocationListener()V
    .locals 3

    .prologue
    .line 655
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 657
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 658
    const-string/jumbo v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    .line 660
    .local v1, "lm":Landroid/location/LocationManager;
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 662
    .end local v1    # "lm":Landroid/location/LocationManager;
    :cond_0
    return-void
.end method

.method private retireLastWidget()V
    .locals 1

    .prologue
    .line 4518
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;

    if-eqz v0, :cond_1

    .line 4529
    :cond_0
    :goto_0
    return-void

    .line 4520
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;

    if-nez v0, :cond_0

    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    if-nez v0, :cond_0

    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    if-nez v0, :cond_0

    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;

    if-nez v0, :cond_0

    .line 4523
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-nez v0, :cond_0

    .line 4526
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v0, :cond_0

    .line 4527
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/Widget;->retire()V

    goto :goto_0
.end method

.method private setActionBarTalkback()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1536
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v3, "action_bar_title"

    const-string/jumbo v4, "id"

    const-string/jumbo v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 1537
    .local v1, "titleId":I
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1538
    .local v0, "actionBarTitleView":Landroid/widget/TextView;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialOn()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1539
    new-instance v2, Lcom/vlingo/midas/gui/ConversationActivity$4;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/ConversationActivity$4;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 1551
    :cond_0
    :goto_0
    return-void

    .line 1545
    :cond_1
    if-eqz v0, :cond_0

    .line 1546
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 1547
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 1548
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    goto :goto_0
.end method

.method private setCoverLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 1445
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    .line 1446
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1447
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lcom/samsung/android/sdk/cover/ScoverManager;->setCoverModeToWindow(Landroid/view/Window;I)V

    .line 1448
    sput-boolean v3, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    .line 1449
    invoke-static {v3}, Lcom/vlingo/midas/util/SViewCoverUtils;->setSViewCoverState(Z)V

    .line 1450
    iput-boolean v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->coverViewGUI:Z

    .line 1451
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->hideActionBar(Landroid/app/ActionBar;)V

    .line 1478
    :goto_0
    return-void

    .line 1453
    :cond_0
    sget-boolean v1, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    if-eqz v1, :cond_1

    .line 1454
    sput-boolean v3, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    .line 1455
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 1456
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->showingNotifications()Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "keyguard"

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1460
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessage(I)Z

    .line 1463
    :cond_1
    iput-boolean v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->coverViewGUI:Z

    .line 1464
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1465
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x200000

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1466
    invoke-static {v5}, Lcom/vlingo/midas/util/SViewCoverUtils;->setSViewCoverState(Z)V

    .line 1467
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1468
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->switchToFullScreen()V

    .line 1469
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->showActionBar(Landroid/app/ActionBar;)V

    goto :goto_0

    .line 1471
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1472
    .local v0, "attributes":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v1, v1, 0x600

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1473
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1474
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setActionBarTransparent(Landroid/app/ActionBar;)V

    .line 1475
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->afterViews()V

    goto :goto_0
.end method

.method private setDrivingMode(Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;)V
    .locals 0
    .param p1, "bDrivingMode"    # Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    .prologue
    .line 5602
    iput-object p1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->bDrivingMode:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    .line 5603
    return-void
.end method

.method private setHelpFragmentVisibility()V
    .locals 2

    .prologue
    .line 1790
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1791
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->help_layout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1794
    :goto_0
    return-void

    .line 1793
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->help_layout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public static setLatestWidget(Lcom/vlingo/midas/gui/Widget;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/midas/gui/Widget",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 4678
    .local p0, "latestWidget":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<*>;"
    instance-of v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;

    if-eqz v0, :cond_0

    .line 4679
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->islatestMusicWidget:Z

    .line 4680
    :cond_0
    sput-object p0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    .line 4681
    return-void
.end method

.method private setMicAnimation()V
    .locals 1

    .prologue
    .line 1800
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v0, :cond_0

    .line 1801
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setItsPosition()V

    .line 1803
    :cond_0
    return-void
.end method

.method private setViewCoverLayout()V
    .locals 10

    .prologue
    const/high16 v9, 0x200000

    const/high16 v8, 0x80000

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 1482
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    .line 1483
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1484
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v3, v4, v6}, Lcom/samsung/android/sdk/cover/ScoverManager;->setCoverModeToWindow(Landroid/view/Window;I)V

    .line 1485
    iput-boolean v7, p0, Lcom/vlingo/midas/gui/ConversationActivity;->coverViewGUI:Z

    .line 1486
    sput-boolean v5, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    .line 1487
    const-string/jumbo v3, "keyguard"

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    .line 1488
    .local v2, "keyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {v2}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "secure_voice_wake_up"

    invoke-static {v3, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1489
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/Window;->addFlags(I)V

    .line 1490
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/view/Window;->addFlags(I)V

    .line 1491
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->afterViews()V

    .line 1492
    invoke-static {v5}, Lcom/vlingo/midas/util/SViewCoverUtils;->setSViewCoverState(Z)V

    .line 1493
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->hideActionBar(Landroid/app/ActionBar;)V

    .line 1495
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 1496
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 1533
    .end local v2    # "keyguardManager":Landroid/app/KeyguardManager;
    :cond_2
    :goto_0
    return-void

    .line 1498
    :cond_3
    iput-boolean v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->coverViewGUI:Z

    .line 1499
    sget-boolean v3, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    if-eqz v3, :cond_4

    .line 1500
    sput-boolean v5, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    .line 1501
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v3, v6}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 1503
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->showingNotifications()Z

    move-result v1

    .line 1504
    .local v1, "isShowingNotifications":Z
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v3

    if-nez v3, :cond_4

    if-nez v1, :cond_4

    const-string/jumbo v3, "keyguard"

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/KeyguardManager;

    invoke-virtual {v3}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1508
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1515
    .end local v1    # "isShowingNotifications":Z
    :cond_4
    :goto_1
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    if-nez v3, :cond_5

    .line 1516
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/Window;->clearFlags(I)V

    .line 1517
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/view/Window;->clearFlags(I)V

    .line 1519
    :cond_5
    invoke-static {v7}, Lcom/vlingo/midas/util/SViewCoverUtils;->setSViewCoverState(Z)V

    .line 1520
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1521
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->switchToFullScreen()V

    .line 1522
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->showActionBar(Landroid/app/ActionBar;)V

    .line 1523
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1524
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->show()V

    goto :goto_0

    .line 1526
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1527
    .local v0, "attributes":Landroid/view/WindowManager$LayoutParams;
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v3, v3, 0x600

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1528
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1529
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->setActionBarTransparent(Landroid/app/ActionBar;)V

    .line 1530
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->afterViews()V

    goto/16 :goto_0

    .line 1511
    .end local v0    # "attributes":Landroid/view/WindowManager$LayoutParams;
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public static shouldReportError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;)Z
    .locals 2
    .param p0, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .prologue
    const/4 v0, 0x0

    .line 4532
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SPEECH_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    if-ne v1, p0, :cond_1

    .line 4542
    :cond_0
    :goto_0
    return v0

    .line 4537
    :cond_1
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NO_MATCH:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    if-eq v1, p0, :cond_0

    .line 4542
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startOtherAppAfterAccept()V
    .locals 2

    .prologue
    .line 1555
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1556
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.midas.action.TOS_ACCEPTED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1557
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1558
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1563
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1560
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private startTutorial()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 5818
    const-string/jumbo v2, "ConversationActivity"

    const-string/jumbo v3, "startTutorial"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5819
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    if-eqz v2, :cond_8

    .line 5820
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 5821
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 5822
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->transparent:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 5831
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5832
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->bg_tutorial:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 5833
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->transparent:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 5835
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->invalidateOptionsMenu()V

    .line 5836
    sput-boolean v5, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorial:Z

    .line 5837
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    if-eqz v2, :cond_1

    .line 5838
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogFragment;->setDialogContent()V

    .line 5843
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 5844
    invoke-static {p0, v6}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    move-result-object v1

    .line 5849
    .local v1, "tutorialObj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :goto_1
    invoke-virtual {v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->destroy()V

    .line 5850
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    const-string/jumbo v3, "#E5008892"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 5851
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->cancelAllActivities()V

    .line 5852
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 5853
    new-instance v2, Lcom/vlingo/midas/gui/ConversationActivity$IncomingCallReceiver;

    invoke-direct {v2, p0, v6}, Lcom/vlingo/midas/gui/ConversationActivity$IncomingCallReceiver;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Lcom/vlingo/midas/gui/ConversationActivity$1;)V

    iput-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->incomingCallReceiver:Landroid/content/BroadcastReceiver;

    .line 5854
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 5855
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "android.intent.action.PHONE_STATE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 5856
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->incomingCallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 5858
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->audioManager:Landroid/media/AudioManager;

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5859
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v3, "voice_wakeup_mic=off"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 5860
    const-string/jumbo v2, "Always"

    const-string/jumbo v3, "setParameters, voice_wakeup_mic=off"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5861
    sput-boolean v7, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    .line 5864
    :cond_2
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_4

    .line 5865
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5866
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->removeMic()V

    .line 5867
    :cond_3
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v5}, Lcom/vlingo/midas/gui/ControlFragment;->removeMicStatusTextForTutorial(Z)V

    .line 5870
    :cond_4
    const-string/jumbo v2, "car_word_spotter_enabled"

    invoke-static {v2, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 5871
    const-string/jumbo v2, "car_word_spotter_enabled"

    invoke-static {v2, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 5872
    iput-boolean v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isPharseSpottingStoppedByTutorial:Z

    .line 5874
    :cond_5
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v2, v5}, Lcom/vlingo/midas/gui/ContentFragment;->resetAllContentForTutorial(Z)V

    .line 5875
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ContentFragment;->addTutorialWidget()V

    .line 5876
    sput-boolean v5, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    .line 5877
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setActionBarTalkback()V

    .line 5878
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 5879
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 5881
    :cond_6
    return-void

    .line 5824
    .end local v0    # "filter":Landroid/content/IntentFilter;
    .end local v1    # "tutorialObj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :cond_7
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->show()V

    goto/16 :goto_0

    .line 5827
    :cond_8
    const-string/jumbo v2, "KABRA -->"

    const-string/jumbo v3, "Title bar is NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 5846
    :cond_9
    invoke-static {p0, v6}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    move-result-object v1

    .restart local v1    # "tutorialObj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    goto/16 :goto_1
.end method

.method private stopVoiceWakeupParam()V
    .locals 2

    .prologue
    .line 4881
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v1, "voice_wakeup_mic=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 4882
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_wakeup_mic=off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4883
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    .line 4884
    return-void
.end method

.method private unlockScreenIfLocked()V
    .locals 4

    .prologue
    .line 5189
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 5190
    .local v0, "keyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5191
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 5192
    .local v1, "window":Landroid/view/Window;
    const/high16 v2, 0x400000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 5193
    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 5194
    const/high16 v2, 0x200000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 5196
    .end local v1    # "window":Landroid/view/Window;
    :cond_0
    return-void
.end method

.method private unpackSavedInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1728
    if-eqz p1, :cond_0

    .line 1729
    const-string/jumbo v0, "autoStartOnResume"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    .line 1732
    :cond_0
    return-void
.end method


# virtual methods
.method public addBlueBubble()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4165
    const-string/jumbo v1, "car_word_spotter_enabled"

    invoke-static {v1, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 4168
    .local v0, "spottingEnabled":Z
    if-nez v0, :cond_1

    .line 4191
    :cond_0
    :goto_0
    return-void

    .line 4184
    :cond_1
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v1, v2, :cond_0

    .line 4185
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 4186
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v4, v2, v3}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 4187
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    iput-boolean v5, v1, Lcom/vlingo/midas/gui/DialogFragment;->setShadowWhileNotAnim:Z

    goto :goto_0
.end method

.method public addHelpChoiceWidget()V
    .locals 1

    .prologue
    .line 4153
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 4154
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->addHelpChoiceWidget()V

    .line 4155
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->addBlueBubble()V

    .line 4156
    return-void
.end method

.method public addHelpWidget(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4159
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 4160
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ContentFragment;->addHelpWidget(Landroid/content/Intent;)V

    .line 4161
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->addBlueBubble()V

    .line 4162
    return-void
.end method

.method public addWidget(Lcom/vlingo/midas/gui/Widget;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/midas/gui/Widget",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 4629
    .local p1, "widget":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TT;>;"
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->retireLastWidget()V

    .line 4630
    invoke-static {p1}, Lcom/vlingo/midas/gui/ConversationActivity;->setLatestWidget(Lcom/vlingo/midas/gui/Widget;)V

    .line 4632
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    instance-of v2, v2, Lcom/vlingo/midas/gui/widgets/ClockWidget;

    if-eqz v2, :cond_1

    .line 4633
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 4635
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "android.intent.action.TIME_TICK"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 4636
    const-string/jumbo v2, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 4637
    const-string/jumbo v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 4639
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->timeWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 4652
    .end local v1    # "filter":Landroid/content/IntentFilter;
    :goto_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    if-eqz v2, :cond_0

    .line 4653
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v2, p1}, Lcom/vlingo/midas/gui/ContentFragment;->addWidget(Landroid/view/View;)V

    .line 4656
    :cond_0
    return-void

    .line 4643
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->timeWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 4644
    const-string/jumbo v2, "ConversationActivity"

    const-string/jumbo v3, "TimerReceiver unregistered successfully"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4646
    :catch_0
    move-exception v0

    .line 4647
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string/jumbo v2, "ConversationActivity"

    const-string/jumbo v3, "TimerReceiver not register yet"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method afterViews()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1672
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 1673
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    const/16 v4, 0x3c

    invoke-virtual {v3, v7, v4, v7, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 1674
    invoke-static {v7}, Lcom/vlingo/midas/util/SViewCoverUtils;->setCoverGUI(Z)V

    .line 1675
    const-string/jumbo v3, "window"

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 1676
    .local v2, "mWinMgr":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 1678
    .local v0, "displayHeight":I
    int-to-double v3, v0

    const-wide v5, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v3, v5

    double-to-int v1, v3

    .line 1679
    .local v1, "height":I
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->coverViewGUI:Z

    if-nez v3, :cond_1

    .line 1682
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-static {}, Lcom/vlingo/midas/gui/AnimationUtils;->inFromBottomAnimation()Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1683
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    invoke-static {}, Lcom/vlingo/midas/gui/AnimationUtils;->inFromBottomAnimation()Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1684
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    invoke-static {}, Lcom/vlingo/midas/gui/AnimationUtils;->inFromBottomAnimation()Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1686
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1687
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1688
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$drawable;->bg_miniapp:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1689
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$drawable;->transparent:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1690
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v7, v1, v7, v7}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 1704
    .end local v0    # "displayHeight":I
    .end local v1    # "height":I
    .end local v2    # "mWinMgr":Landroid/view/WindowManager;
    :cond_0
    :goto_0
    return-void

    .line 1692
    .restart local v0    # "displayHeight":I
    .restart local v1    # "height":I
    .restart local v2    # "mWinMgr":Landroid/view/WindowManager;
    :cond_1
    invoke-static {v8}, Lcom/vlingo/midas/util/SViewCoverUtils;->setCoverGUI(Z)V

    .line 1693
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1694
    int-to-double v3, v0

    const-wide v5, 0x3fbeb851eb851eb8L    # 0.12

    mul-double/2addr v3, v5

    double-to-int v1, v3

    .line 1695
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1696
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    const-string/jumbo v4, "#E5008892"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1697
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/cover/ScoverState;->getType()I

    move-result v3

    if-ne v3, v8, :cond_2

    .line 1698
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v7, v7, v7, v1}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    goto :goto_0

    .line 1699
    :cond_2
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/cover/ScoverState;->getType()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_0

    .line 1700
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->hide()V

    goto :goto_0
.end method

.method public changeToDriveMode(Z)V
    .locals 3
    .param p1, "driveModeEnable"    # Z

    .prologue
    .line 4010
    const/4 p1, 0x0

    .line 4017
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4018
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWakeupLock(Z)V

    .line 4027
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Regular:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->setDrivingMode(Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;)V

    .line 4063
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment;->getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 4065
    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 5895
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialFromSettings:Z

    if-eqz v0, :cond_1

    .line 5896
    :cond_0
    const/4 v0, 0x1

    .line 5898
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 3528
    const/4 v0, 0x0

    .line 3546
    .local v0, "isHandled":Z
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->gestures:Landroid/view/GestureDetector;

    if-eqz v1, :cond_0

    .line 3547
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->gestures:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3548
    const/4 v1, 0x1

    .line 3556
    :goto_0
    return v1

    .line 3553
    :cond_0
    :try_start_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    move v1, v0

    .line 3556
    goto :goto_0

    .line 3554
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public enteredDomain(Lcom/vlingo/core/internal/domain/DomainName;)V
    .locals 0
    .param p1, "domain"    # Lcom/vlingo/core/internal/domain/DomainName;

    .prologue
    .line 6133
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 2933
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->finish()V

    .line 2934
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2935
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->setRequestedOrientation(I)V

    .line 2937
    :cond_0
    return-void
.end method

.method public finishTutorial(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 5917
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity;->goToSvoiceMainScreenFromTutorial(Landroid/content/Context;)V

    .line 5918
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->reRegisterPhraseSpottingAfterTutorial()V

    .line 5919
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setActionBarTalkback()V

    .line 5920
    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialStartedFromIUX:Z

    if-eqz v0, :cond_0

    .line 5921
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->checkVoiceWakeupFeature()V

    .line 5923
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialStartedFromIUX:Z

    .line 5924
    return-void
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 4514
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWakeupLock(Z)V
    .locals 4
    .param p1, "acquire"    # Z

    .prologue
    .line 3651
    const-string/jumbo v1, "power"

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 3652
    .local v0, "pm":Landroid/os/PowerManager;
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 3653
    const/4 v1, 0x6

    const-string/jumbo v2, "SVoiceDrivingScreenSaver"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 3655
    :cond_0
    if-eqz p1, :cond_2

    .line 3657
    :try_start_0
    const-string/jumbo v1, "getFieldID"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pm newWakeLock acq"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " isHeld = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3658
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3659
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 3679
    :cond_1
    :goto_0
    return-void

    .line 3667
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_3

    .line 3668
    const-string/jumbo v1, "getFieldID"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pm newWakeLock rel "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " isHeld = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3672
    :goto_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3673
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mDrivingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 3675
    :catch_0
    move-exception v1

    goto :goto_0

    .line 3670
    :cond_3
    const-string/jumbo v1, "getFieldID"

    const-string/jumbo v2, "pm newWakeLock rel but null "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 3661
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public handleTextRequestIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 5039
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->handleTextReqeust:Z

    .line 5040
    if-eqz p1, :cond_0

    .line 5041
    const-string/jumbo v0, "PRESENT_EDITOR"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->present_editor:Z

    .line 5042
    const-string/jumbo v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->textRequest:Ljava/lang/String;

    .line 5044
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->textRequest:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->present_editor:Z

    if-nez v0, :cond_2

    .line 5045
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5046
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "dm_main"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5047
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->textRequest:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->startUserFlow(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 5050
    :cond_2
    return-void
.end method

.method public handsfreeModeSecurelockNoti(Z)V
    .locals 13
    .param p1, "isSecuredLock"    # Z

    .prologue
    .line 4069
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    .line 4070
    .local v4, "inflater":Landroid/view/LayoutInflater;
    sget v9, Lcom/vlingo/midas/R$layout;->custom_alert_dialog:I

    const/4 v10, 0x0

    invoke-virtual {v4, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 4071
    .local v2, "checkboxLayout":Landroid/view/View;
    move v5, p1

    .line 4072
    .local v5, "isLocksecured":Z
    sget v9, Lcom/vlingo/midas/R$id;->checkBox:I

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 4074
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const/16 v9, 0xa

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v10

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v11

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v12

    invoke-virtual {v1, v9, v10, v11, v12}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 4076
    new-instance v9, Lcom/vlingo/midas/gui/ConversationActivity$12;

    invoke-direct {v9, p0}, Lcom/vlingo/midas/gui/ConversationActivity$12;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v1, v9}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4081
    new-instance v9, Lcom/vlingo/midas/gui/ConversationActivity$13;

    invoke-direct {v9, p0}, Lcom/vlingo/midas/gui/ConversationActivity$13;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v1, v9}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 4087
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 4088
    .local v6, "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 4089
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$string;->handsfree_mode_popup_enable:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4090
    .local v3, "enable":Ljava/lang/String;
    const v9, 0x104000a

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 4091
    .local v7, "ok":Ljava/lang/String;
    sget v9, Lcom/vlingo/midas/R$id;->custom_dialog_warning_textView:I

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 4093
    .local v8, "text":Landroid/widget/TextView;
    if-eqz v5, :cond_1

    .line 4094
    sget v9, Lcom/vlingo/midas/R$string;->setting_wakeup_secure_title:I

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 4095
    sget v9, Lcom/vlingo/midas/R$string;->handsfree_mode_securelock_popup_message:I

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 4101
    :goto_0
    if-eqz v5, :cond_2

    .end local v3    # "enable":Ljava/lang/String;
    :goto_1
    new-instance v9, Lcom/vlingo/midas/gui/ConversationActivity$14;

    invoke-direct {v9, p0, v1, v5}, Lcom/vlingo/midas/gui/ConversationActivity$14;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Landroid/widget/CheckBox;Z)V

    invoke-virtual {v6, v3, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 4118
    if-eqz v5, :cond_0

    .line 4119
    const/high16 v9, 0x1040000

    new-instance v10, Lcom/vlingo/midas/gui/ConversationActivity$15;

    invoke-direct {v10, p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$15;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v6, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 4133
    :cond_0
    new-instance v9, Lcom/vlingo/midas/gui/ConversationActivity$16;

    invoke-direct {v9, p0}, Lcom/vlingo/midas/gui/ConversationActivity$16;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 4143
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 4144
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 4145
    new-instance v9, Lcom/vlingo/midas/gui/ConversationActivity$17;

    invoke-direct {v9, p0}, Lcom/vlingo/midas/gui/ConversationActivity$17;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 4149
    return-void

    .line 4097
    .end local v0    # "alertDialog":Landroid/app/AlertDialog;
    .restart local v3    # "enable":Ljava/lang/String;
    :cond_1
    sget v9, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 4098
    sget v9, Lcom/vlingo/midas/R$string;->handsfree_mode_voicewakeup_popup_message:I

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_2
    move-object v3, v7

    .line 4101
    goto :goto_1
.end method

.method public hideControlFragment()V
    .locals 3

    .prologue
    .line 5437
    :try_start_0
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    .line 5438
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5442
    :cond_0
    :goto_0
    return-void

    .line 5439
    :catch_0
    move-exception v0

    .line 5440
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public hideMusic()V
    .locals 1

    .prologue
    .line 5783
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragment;->hideMusic()V

    .line 5784
    return-void
.end method

.method public initFlow()V
    .locals 3

    .prologue
    .line 1841
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 1842
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->releaseFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;)V

    .line 1844
    invoke-static {}, Lcom/vlingo/midas/util/ConfigurationUtility;->generateConfigPairs()Ljava/util/HashMap;

    move-result-object v0

    .line 1845
    .local v0, "userProperties":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/gui/WidgetBuilder;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/WidgetBuilder;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v1, p0, p0, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->initFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 1847
    return-void
.end method

.method public isDrivingMode()Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;
    .locals 1

    .prologue
    .line 4659
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->bDrivingMode:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    return-object v0
.end method

.method public isPassedAllInitSteps()Z
    .locals 1

    .prologue
    .line 5607
    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->isTOSRequired()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->requiresIUX()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5609
    :cond_0
    const/4 v0, 0x1

    .line 5610
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStartedForCustomWakeUpSetting()Z
    .locals 1

    .prologue
    .line 4891
    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->mIsStartedCustomWakeUpSetting:Z

    return v0
.end method

.method public onASRRecorderClosed()V
    .locals 3

    .prologue
    .line 5740
    const-string/jumbo v1, "ConversationActivity"

    const-string/jumbo v2, "[LatencyCheck] onASRRecorderClosed()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5741
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 5742
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->setVolumeNormal(Landroid/content/Context;)V

    .line 5744
    return-void
.end method

.method public onASRRecorderOpened()V
    .locals 2

    .prologue
    .line 5731
    const-string/jumbo v0, "ConversationActivity"

    const-string/jumbo v1, "[LatencyCheck] onASRRecorderOpened()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5735
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 6
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 453
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v3, "action_mode_close_button"

    const-string/jumbo v4, "id"

    const-string/jumbo v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 455
    .local v0, "doneButtonId":I
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 456
    .local v1, "doneLayout":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 457
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->done:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 460
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/16 v6, 0xd

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 3361
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/midas/ui/VLActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 3362
    const-string/jumbo v5, "is_move_to_svoice"

    invoke-static {v5, v3}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_1

    move v2, v3

    .line 3378
    .local v2, "toTutorial":Z
    :goto_0
    if-eqz v2, :cond_2

    if-ne p1, v6, :cond_2

    .line 3379
    sput-boolean v3, Lcom/vlingo/midas/gui/ConversationActivity;->doLaunchTutorial:Z

    .line 3404
    :cond_0
    :goto_1
    return-void

    .end local v2    # "toTutorial":Z
    :cond_1
    move v2, v4

    .line 3362
    goto :goto_0

    .line 3380
    .restart local v2    # "toTutorial":Z
    :cond_2
    if-ne p1, v6, :cond_3

    .line 3381
    sput-boolean v4, Lcom/vlingo/midas/gui/ConversationActivity;->doLaunchTutorial:Z

    .line 3386
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    if-eqz v3, :cond_0

    .line 3387
    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogFragment;->resetAllContent()V

    goto :goto_1

    .line 3389
    :cond_3
    const/16 v3, 0x76

    if-ne p1, v3, :cond_0

    .line 3390
    const-string/jumbo v3, "result"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3393
    .local v1, "result":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->processResult(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    if-nez v3, :cond_4

    .line 3397
    sget v3, Lcom/vlingo/midas/R$string;->enavi_unable_find_location:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3398
    .local v0, "errror":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->showVlingoText(Ljava/lang/String;)V

    .line 3399
    sget-object v3, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v3, v0}, Lcom/vlingo/midas/gui/ControlFragment;->announceTTS(Ljava/lang/String;)V

    goto :goto_1

    .line 3401
    .end local v0    # "errror":Ljava/lang/String;
    :cond_4
    invoke-static {v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->processResult(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x0

    .line 2851
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->isForeground()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2853
    const-wide/16 v3, 0x12c

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2858
    :cond_0
    :goto_0
    iput-boolean v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->lockInMiniMode:Z

    .line 2859
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isFromBackPress:Z

    .line 2861
    sget-boolean v3, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    if-nez v3, :cond_1

    .line 2862
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->finishAffinity()V

    .line 2865
    :cond_1
    sget-boolean v3, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    if-eqz v3, :cond_5

    sget-boolean v3, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialFromSettings:Z

    if-eqz v3, :cond_5

    .line 2866
    invoke-virtual {p0, p0}, Lcom/vlingo/midas/gui/ConversationActivity;->finishTutorial(Landroid/content/Context;)V

    .line 2867
    sget-object v3, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v3, :cond_2

    .line 2868
    sget-object v3, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v4, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v3, v4}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 2879
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setActionBarTalkback()V

    .line 2880
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialOn()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2881
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/Window;->addFlags(I)V

    .line 2889
    :cond_3
    :goto_2
    sget-boolean v3, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialStartedFromIUX:Z

    if-eqz v3, :cond_4

    .line 2890
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2891
    .local v2, "mDefaultSharedPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2892
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v3, "showing_notifications"

    invoke-static {v3, v5}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 2893
    const-string/jumbo v3, "is_move_to_svoice"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2894
    const-string/jumbo v3, "is_tutorial_completed"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2895
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2896
    const-string/jumbo v3, "all_notifications_accepted"

    invoke-static {v3, v5}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 2898
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "mDefaultSharedPrefs":Landroid/content/SharedPreferences;
    :cond_4
    return-void

    .line 2854
    :catch_0
    move-exception v0

    .line 2855
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 2871
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_5
    :try_start_1
    sget-object v3, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->NORMAL:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    invoke-static {v3}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->appCloseType(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;)V

    .line 2872
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onBackPressed()V

    .line 2874
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 2875
    :catch_1
    move-exception v3

    goto :goto_1

    .line 2883
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/Window;->clearFlags(I)V

    .line 2884
    sget-object v3, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v3, :cond_3

    .line 2885
    sget-object v3, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragment;->removeDelegateFromLastScreen()V

    goto :goto_2
.end method

.method public onBluetoothServiceConnected()V
    .locals 3

    .prologue
    .line 5672
    invoke-static {}, Lcom/vlingo/midas/util/ConfigurationUtility;->generateConfigPairs()Ljava/util/HashMap;

    move-result-object v0

    .line 5673
    .local v0, "userProperties":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->displayFromBackground:Z

    if-nez v1, :cond_0

    .line 5674
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/gui/WidgetBuilder;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/WidgetBuilder;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v1, p0, p0, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->initFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 5680
    :goto_0
    return-void

    .line 5677
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/gui/WidgetBuilder;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/WidgetBuilder;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v1, p0, p0, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->stealFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    goto :goto_0
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 5990
    const-string/jumbo v0, "ConversationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCallStateChanged() - state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5991
    packed-switch p1, :pswitch_data_0

    .line 6012
    :cond_0
    :goto_0
    return-void

    .line 5993
    :pswitch_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 5994
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    goto :goto_0

    .line 5997
    :pswitch_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5998
    invoke-static {p0, v3}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->unMuteTTS()V

    .line 6002
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->isForeground()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->hasFocus:Z

    if-eqz v0, :cond_0

    .line 6003
    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity$23;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ConversationActivity$23;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    const-wide/16 v1, 0x514

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 6000
    :cond_1
    invoke-static {p0, v3}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->unMuteTTS()V

    goto :goto_1

    .line 5991
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 5364
    new-instance v1, Landroid/widget/PopupMenu;

    invoke-direct {v1, p0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 5365
    .local v1, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 5366
    .local v0, "menu":Landroid/view/Menu;
    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 5367
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 5369
    sget-boolean v2, Lcom/vlingo/midas/settings/debug/DebugSettings;->SHOW_DEBUG:Z

    if-eqz v2, :cond_0

    .line 5370
    const/16 v2, 0x3e8

    const-string/jumbo v3, "Debug Settings"

    invoke-interface {v0, v5, v2, v7, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 5375
    const/16 v2, 0xd

    const/4 v3, 0x5

    const-string/jumbo v4, "Help"

    invoke-interface {v0, v5, v2, v3, v4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 5403
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->menu_setting:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v5, v6, v7, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->menu_setting_icon:I

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 5407
    invoke-virtual {v1, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 5409
    sget-boolean v2, Lcom/vlingo/midas/settings/debug/DebugSettings;->SHOW_DEBUG:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "cradle_connect"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_2

    .line 5413
    :cond_1
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    .line 5417
    :goto_0
    return-void

    .line 5415
    :cond_2
    invoke-interface {v0, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->onMenuItemClick(Landroid/view/MenuItem;)Z

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1740
    if-eqz p1, :cond_0

    .line 1741
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1780
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setOrientationDrivingMode()V

    .line 1781
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$bool;->help_fragment_supported:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1782
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setHelpFragmentVisibility()V

    .line 1784
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1785
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setMicAnimation()V

    .line 1787
    :cond_2
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 38
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 692
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onAppLaunched()V

    .line 694
    const-string/jumbo v34, "VerificationLog"

    const-string/jumbo v35, "onCreate"

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    const-string/jumbo v34, "svoicedebugging"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "Focus onCreate() of Svoice "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v34

    move-object/from16 v0, v34

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    move/from16 v34, v0

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->screenDensityDPI:F

    .line 708
    new-instance v34, Lcom/vlingo/midas/gui/PBSchedulesObserver;

    new-instance v35, Landroid/os/Handler;

    invoke-direct/range {v35 .. v35}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/PBSchedulesObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->observer:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    .line 709
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v34

    sget-object v35, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->INSTANCES_CONTENT_URI:Landroid/net/Uri;

    const/16 v36, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->observer:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    move-object/from16 v37, v0

    invoke-virtual/range {v34 .. v37}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 714
    new-instance v34, Lcom/vlingo/midas/gui/ConversationActivity$1;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$1;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-static/range {v34 .. v34}, Lcom/vlingo/midas/ui/HomeKeyListener;->onHomePressed(Ljava/lang/Runnable;)Lcom/vlingo/midas/ui/HomeKeyListener;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    .line 721
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v34

    const/16 v35, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->checkDataUsagePopup(Landroid/content/Intent;Z)V

    .line 723
    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->isTOSRequired()Z

    move-result v34

    if-nez v34, :cond_0

    .line 724
    sget-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->threadVar:Z

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_0

    .line 726
    const/16 v34, 0x0

    sput-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->threadVar:Z

    .line 727
    new-instance v34, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;-><init>(Landroid/content/Context;)V

    sput-object v34, Lcom/vlingo/midas/gui/DialogFragment;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    .line 728
    new-instance v34, Ljava/lang/Thread;

    new-instance v35, Lcom/vlingo/midas/gui/ConversationActivity$Task;

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$Task;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-direct/range {v34 .. v35}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->youcansayThread:Ljava/lang/Thread;

    .line 729
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->youcansayThread:Ljava/lang/Thread;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/lang/Thread;->start()V

    .line 733
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v34

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->isKModel:Z

    .line 734
    new-instance v21, Landroid/util/DisplayMetrics;

    invoke-direct/range {v21 .. v21}, Landroid/util/DisplayMetrics;-><init>()V

    .line 735
    .local v21, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 736
    const/16 v34, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Landroid/content/Context;Z)I

    .line 737
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v34

    sget v35, Lcom/vlingo/midas/R$integer;->device_type:I

    invoke-virtual/range {v34 .. v35}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v34

    invoke-static/range {v34 .. v34}, Lcom/vlingo/midas/settings/MidasSettings;->setDeviceType(I)V

    .line 738
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v34

    const-string/jumbo v35, "audio"

    invoke-virtual/range {v34 .. v35}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Landroid/media/AudioManager;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->audioManager:Landroid/media/AudioManager;

    .line 744
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v14

    .line 745
    .local v14, "intent":Landroid/content/Intent;
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->needToDismissLockScreen()Z

    move-result v34

    sput-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->turnScreenOff:Z

    .line 747
    const-string/jumbo v34, "from_always_mic_on"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z

    .line 756
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z

    move/from16 v34, v0

    if-eqz v34, :cond_1

    .line 757
    const/16 v34, 0x1

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->mAutoListeningmodeAlways:Z

    .line 758
    const/16 v34, 0x1

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    .line 760
    const-string/jumbo v34, "wake_up_phrase_as_string"

    move-object/from16 v0, v34

    invoke-virtual {v14, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 776
    .local v33, "wakeUpPhraseAsString":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v34

    new-instance v35, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    const/16 v36, 0x0

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    move/from16 v2, v36

    invoke-direct {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;-><init>(Ljava/lang/String;Z)V

    invoke-virtual/range {v34 .. v35}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->setWakeUpContext(Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;)V

    .line 779
    .end local v33    # "wakeUpPhraseAsString":Ljava/lang/String;
    :cond_1
    const-string/jumbo v34, "keyguard"

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/app/KeyguardManager;

    .line 780
    .local v16, "keyguardManager":Landroid/app/KeyguardManager;
    if-eqz v16, :cond_2

    invoke-virtual/range {v16 .. v16}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v34

    if-nez v34, :cond_3

    :cond_2
    sget-boolean v34, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->wasScreenOff:Z

    if-eqz v34, :cond_4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    move/from16 v34, v0

    if-eqz v34, :cond_4

    .line 782
    :cond_3
    const/16 v34, 0x1

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->checkForAbnormalonPause:Z

    .line 783
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    move-object/from16 v34, v0

    const/16 v35, 0x13

    invoke-virtual/range {v34 .. v35}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 784
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    move-object/from16 v34, v0

    const/16 v35, 0x13

    const-wide/16 v36, 0xfa0

    invoke-virtual/range {v34 .. v37}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 786
    invoke-virtual/range {v16 .. v16}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v34

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->isKeyguardSecure:Z

    .line 787
    const/16 v34, 0x0

    sput-boolean v34, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->wasScreenOff:Z

    .line 791
    :cond_4
    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v34, v0

    move/from16 v0, v34

    float-to-double v0, v0

    move-wide/from16 v34, v0

    const-wide/high16 v36, 0x3ff0000000000000L    # 1.0

    cmpl-double v34, v34, v36

    if-nez v34, :cond_5

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v34, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v35, v0

    add-int v34, v34, v35

    const/16 v35, 0x7f0

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_5

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v34, v0

    const v35, 0x4315d32c

    invoke-static/range {v34 .. v35}, Ljava/lang/Float;->compare(FF)I

    move-result v34

    if-nez v34, :cond_5

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v34, v0

    const v35, 0x431684be

    invoke-static/range {v34 .. v35}, Ljava/lang/Float;->compare(FF)I

    move-result v34

    if-eqz v34, :cond_6

    :cond_5
    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v34, v0

    move/from16 v0, v34

    float-to-double v0, v0

    move-wide/from16 v34, v0

    const-wide/high16 v36, 0x3ff0000000000000L    # 1.0

    cmpl-double v34, v34, v36

    if-nez v34, :cond_2f

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v34, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v35, v0

    add-int v34, v34, v35

    const/16 v35, 0x820

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_2f

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v34, v0

    const v35, 0x4315d2f2

    invoke-static/range {v34 .. v35}, Ljava/lang/Float;->compare(FF)I

    move-result v34

    if-nez v34, :cond_2f

    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v34, v0

    const v35, 0x4316849c

    invoke-static/range {v34 .. v35}, Ljava/lang/Float;->compare(FF)I

    move-result v34

    if-nez v34, :cond_2f

    .line 797
    :cond_6
    sget v34, Lcom/vlingo/midas/R$style;->DialogSlideAnim:I

    sput v34, Lcom/vlingo/midas/gui/ConversationActivity;->mTheme:I

    .line 832
    :cond_7
    :goto_0
    sget v34, Lcom/vlingo/midas/gui/ConversationActivity;->mTheme:I

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->setTheme(I)V

    .line 834
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v34

    if-nez v34, :cond_38

    .line 835
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v34

    const/16 v35, 0x400

    const/16 v36, 0x400

    invoke-virtual/range {v34 .. v36}, Landroid/view/Window;->setFlags(II)V

    .line 843
    :cond_8
    :goto_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v34

    if-eqz v34, :cond_9

    .line 844
    const/16 v34, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->setRequestedOrientation(I)V

    .line 847
    :cond_9
    invoke-super/range {p0 .. p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 849
    const-string/jumbo v34, "should_wakeup_enabled"

    const/16 v35, 0x0

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 853
    const/16 v34, 0x0

    sput-boolean v34, Lcom/vlingo/midas/settings/debug/DebugSettings;->SHOW_DEBUG:Z

    .line 856
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v34

    if-eqz v34, :cond_a

    .line 857
    const/16 v24, 0x0

    .line 858
    .local v24, "recoveryIntent":Landroid/content/Intent;
    const-string/jumbo v34, "intent"

    move-object/from16 v0, v34

    invoke-virtual {v14, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v24

    .end local v24    # "recoveryIntent":Landroid/content/Intent;
    check-cast v24, Landroid/content/Intent;

    .line 860
    .restart local v24    # "recoveryIntent":Landroid/content/Intent;
    if-eqz v24, :cond_a

    .line 861
    move-object/from16 v14, v24

    .line 866
    .end local v24    # "recoveryIntent":Landroid/content/Intent;
    :cond_a
    const-string/jumbo v34, "drivelink"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    if-eqz v34, :cond_b

    .line 867
    const/16 v34, 0x1

    sput-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    .line 869
    :cond_b
    const-string/jumbo v34, "launch_after_terms_of_service_acceptance_package"

    move-object/from16 v0, v34

    invoke-virtual {v14, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 870
    .local v22, "pkg":Ljava/lang/String;
    if-eqz v22, :cond_c

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v34

    if-lez v34, :cond_c

    .line 871
    const/16 v34, 0x1

    sput-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    .line 877
    :cond_c
    move-object/from16 v0, p0

    invoke-super {v0, v14}, Lcom/vlingo/midas/ui/VLActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 879
    const/16 v34, 0x0

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->handleTextReqeust:Z

    .line 886
    if-eqz v22, :cond_d

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v34

    if-gtz v34, :cond_e

    :cond_d
    sget-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v34, :cond_f

    .line 887
    :cond_e
    const/16 v34, 0x1

    invoke-static/range {v34 .. v34}, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->setLaunchedForTosAcceptOtherApp(Z)V

    .line 888
    const-string/jumbo v34, "former_tos_acceptance_state"

    const-string/jumbo v35, "reminder_needed"

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    :cond_f
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v34

    if-eqz v34, :cond_10

    .line 892
    const-string/jumbo v34, "headset_mode"

    const/16 v35, 0x0

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 895
    :cond_10
    if-eqz v14, :cond_13

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v34

    if-nez v34, :cond_13

    .line 900
    invoke-virtual {v14}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 901
    .local v4, "action":Ljava/lang/String;
    invoke-virtual {v14}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v29

    .line 903
    .local v29, "type":Ljava/lang/String;
    const-string/jumbo v34, "com.vlingo.midas.action.SEND"

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_39

    if-eqz v29, :cond_39

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v34

    if-nez v34, :cond_11

    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v34

    if-eqz v34, :cond_39

    .line 905
    :cond_11
    const-string/jumbo v34, "text/plain"

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_13

    .line 906
    const/16 v34, 0x1

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->handleTextReqeust:Z

    .line 907
    const-string/jumbo v34, "android.intent.extra.TEXT"

    move-object/from16 v0, v34

    invoke-virtual {v14, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 909
    .local v26, "sharedText":Ljava/lang/String;
    if-eqz v26, :cond_12

    .line 910
    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->textRequest:Ljava/lang/String;

    .line 912
    :cond_12
    const-string/jumbo v34, "PRESENT_EDITOR"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->present_editor:Z

    .line 937
    .end local v4    # "action":Ljava/lang/String;
    .end local v26    # "sharedText":Ljava/lang/String;
    .end local v29    # "type":Ljava/lang/String;
    :cond_13
    :goto_2
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerInit()Z

    .line 938
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->addListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 939
    const-string/jumbo v34, "isLaunchFromTutorialIUX"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    sput-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialStartedFromIUX:Z

    .line 944
    sget v34, Lcom/vlingo/midas/gui/ConversationActivity;->mTheme:I

    sget v35, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_14

    .line 946
    const/16 v34, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->requestWindowFeature(I)Z

    .line 947
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v34

    const v35, 0x1000400

    invoke-virtual/range {v34 .. v35}, Landroid/view/Window;->addFlags(I)V

    .line 952
    :cond_14
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v34

    if-eqz v34, :cond_15

    sget v34, Lcom/vlingo/midas/gui/ConversationActivity;->mTheme:I

    sget v35, Lcom/vlingo/midas/R$style;->MidasBlackMainTheme:I

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_15

    .line 953
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v34

    sput-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    .line 954
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    sget v35, Lcom/vlingo/midas/R$string;->app_name:I

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 955
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 956
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v34

    if-eqz v34, :cond_15

    .line 957
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    invoke-static/range {v34 .. v34}, Lcom/vlingo/midas/settings/MidasSettings;->hideActionBar(Landroid/app/ActionBar;)V

    .line 961
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->isKModel:Z

    move/from16 v34, v0

    if-eqz v34, :cond_16

    .line 964
    const-string/jumbo v34, "car_iux_intro_required"

    const/16 v35, 0x0

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 965
    const/16 v34, 0x0

    invoke-static/range {v34 .. v34}, Lcom/vlingo/midas/iux/IUXManager;->setIUXIntroRequired(Z)V

    .line 966
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v34

    if-eqz v34, :cond_16

    .line 967
    const/16 v34, 0x1

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->doneTosAndIux:Z

    .line 970
    :cond_16
    sget v34, Lcom/vlingo/midas/gui/ConversationActivity;->mTheme:I

    sget v35, Lcom/vlingo/midas/R$style;->actionBarStyle:I

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_18

    .line 971
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v34

    sput-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    .line 972
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    sget v35, Lcom/vlingo/midas/R$string;->app_name:I

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 973
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->currentTablet:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

    move-object/from16 v34, v0

    sget-object v35, Lcom/vlingo/midas/gui/ConversationActivity$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_3b

    .line 974
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    if-eqz v34, :cond_18

    .line 975
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v34

    if-eqz v34, :cond_17

    .line 976
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v34

    const/16 v35, 0x400

    invoke-virtual/range {v34 .. v35}, Landroid/view/Window;->addFlags(I)V

    .line 978
    :cond_17
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 989
    :cond_18
    :goto_3
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v20

    .line 990
    .local v20, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v34

    const-string/jumbo v35, "privateFlags"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v23

    .line 991
    .local v23, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v34

    const-string/jumbo v35, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v27

    .line 992
    .local v27, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v34

    const-string/jumbo v35, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    .line 994
    .local v10, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v8, 0x0

    .local v8, "currentPrivateFlags":I
    const/16 v32, 0x0

    .local v32, "valueofFlagsEnableStatusBar":I
    const/16 v31, 0x0

    .line 995
    .local v31, "valueofFlagsDisableTray":I
    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 996
    move-object/from16 v0, v27

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v32

    .line 997
    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v31

    .line 999
    or-int v8, v8, v32

    .line 1000
    or-int v8, v8, v31

    .line 1002
    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v8}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 1003
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1013
    .end local v8    # "currentPrivateFlags":I
    .end local v10    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v20    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v23    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v27    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v31    # "valueofFlagsDisableTray":I
    .end local v32    # "valueofFlagsEnableStatusBar":I
    :goto_4
    sget v34, Lcom/vlingo/midas/R$layout;->main:I

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->setContentView(I)V

    .line 1014
    sget v34, Lcom/vlingo/midas/R$id;->mic_background_mini_mode:I

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    .line 1016
    sget v34, Lcom/vlingo/midas/R$id;->full_container:I

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    .line 1019
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v34

    if-eqz v34, :cond_19

    .line 1020
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->display:Landroid/view/Display;

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->display:Landroid/view/Display;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/view/Display;->getHeight()I

    move-result v34

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->screenHeight:I

    .line 1023
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v9

    .line 1024
    .local v9, "currentWindow":Landroid/view/Window;
    invoke-virtual {v9}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v20

    .line 1026
    .restart local v20    # "lp":Landroid/view/WindowManager$LayoutParams;
    const/16 v34, -0x1

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1027
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->screenHeight:I

    move/from16 v34, v0

    move/from16 v0, v34

    move-object/from16 v1, v20

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1028
    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1031
    .end local v9    # "currentWindow":Landroid/view/Window;
    .end local v20    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_19
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v34

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->initFragments(Z)V

    .line 1033
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v34, :cond_1a

    .line 1034
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual/range {v34 .. v34}, Lcom/vlingo/midas/gui/ControlFragment;->cancelTTS()V

    .line 1038
    :cond_1a
    const/16 v34, 0x5

    move/from16 v0, v34

    new-array v12, v0, [Ljava/lang/String;

    const/16 v34, 0x0

    sget-object v35, Lcom/vlingo/midas/MidasADMController;->AGGRESSIVE_NOISE_CANCELLATION:Ljava/lang/String;

    aput-object v35, v12, v34

    const/16 v34, 0x1

    sget-object v35, Lcom/vlingo/midas/MidasADMController;->ENDPOINT_DETECTION:Ljava/lang/String;

    aput-object v35, v12, v34

    const/16 v34, 0x2

    sget-object v35, Lcom/vlingo/midas/MidasADMController;->EYES_FREE:Ljava/lang/String;

    aput-object v35, v12, v34

    const/16 v34, 0x3

    sget-object v35, Lcom/vlingo/midas/MidasADMController;->DRIVING_MODE_GUI:Ljava/lang/String;

    aput-object v35, v12, v34

    const/16 v34, 0x4

    sget-object v35, Lcom/vlingo/midas/MidasADMController;->TALKBACK:Ljava/lang/String;

    aput-object v35, v12, v34

    .line 1044
    .local v12, "featuresToListen":[Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getADMController()Lcom/vlingo/core/internal/util/ADMController;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v12}, Lcom/vlingo/core/internal/util/ADMController;->addListener(Lcom/vlingo/core/internal/util/ADMFeatureListener;[Ljava/lang/String;)V

    .line 1047
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/view/Display;->getRotation()I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-eq v0, v1, :cond_1b

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/view/Display;->getRotation()I

    move-result v34

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_1c

    .line 1052
    :cond_1b
    const/16 v34, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1056
    :cond_1c
    const-string/jumbo v34, "key_popup_window_opened"

    const/16 v35, 0x0

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1058
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->init()V

    .line 1059
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v34

    if-eqz v34, :cond_1d

    .line 1060
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->init()V

    .line 1063
    :cond_1d
    new-instance v34, Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-direct/range {v34 .. v34}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    .line 1064
    new-instance v34, Lcom/vlingo/midas/util/log/NLUEventLogUtil;

    invoke-direct/range {v34 .. v34}, Lcom/vlingo/midas/util/log/NLUEventLogUtil;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->nluEventLogUtil:Lcom/vlingo/midas/util/log/NLUEventLogUtil;

    .line 1065
    const/16 v34, 0x0

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->startRecoBySCO:Z

    .line 1066
    if-eqz v14, :cond_23

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v34

    if-nez v34, :cond_23

    .line 1071
    const-string/jumbo v34, "isSecure"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v15

    .line 1075
    .local v15, "isSecure":Z
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v34

    if-nez v34, :cond_1e

    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v34

    if-eqz v34, :cond_22

    .line 1076
    :cond_1e
    invoke-virtual {v14}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v34

    if-eqz v34, :cond_22

    const-string/jumbo v34, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {v14}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_1f

    const-string/jumbo v34, "isThisComeFromHomeKeyDoubleClickConcept"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    if-eqz v34, :cond_21

    :cond_1f
    const-string/jumbo v34, "com.sec.action.SVOICE"

    invoke-virtual {v14}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_20

    const-string/jumbo v34, "isThisComeFromHomeKeyDoubleClickConcept"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    if-eqz v34, :cond_21

    :cond_20
    const-string/jumbo v34, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {v14}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_21

    const-string/jumbo v34, "android.speech.action.WEB_SEARCH"

    invoke-virtual {v14}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_22

    .line 1090
    :cond_21
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual/range {v34 .. v34}, Lcom/vlingo/midas/gui/ControlFragment;->checkMissedEvents()Z

    move-result v34

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->startRecoBySCO:Z

    .line 1100
    :cond_22
    if-nez v15, :cond_23

    .line 1101
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v34

    const/high16 v35, 0x400000

    invoke-virtual/range {v34 .. v35}, Landroid/view/Window;->addFlags(I)V

    .line 1104
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->doDismissKeyguard()V

    .line 1110
    .end local v15    # "isSecure":Z
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->getSettingKey()Ljava/lang/String;

    move-result-object v34

    const/16 v35, 0x1

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->nluEventLogUtil:Lcom/vlingo/midas/util/log/NLUEventLogUtil;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/vlingo/midas/util/log/NLUEventLogUtil;->getSettingKey()Ljava/lang/String;

    move-result-object v34

    const/16 v35, 0x1

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1115
    new-instance v34, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    move-object/from16 v2, v35

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Lcom/vlingo/midas/gui/ConversationActivity$1;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1116
    new-instance v34, Lcom/vlingo/midas/gui/ConversationActivity$HelpWidgetBroadcastReceiver;

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    move-object/from16 v2, v35

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$HelpWidgetBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Lcom/vlingo/midas/gui/ConversationActivity$1;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->helpWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1117
    new-instance v34, Lcom/vlingo/midas/gui/ConversationActivity$TimeWidgetBroadcastReceiver;

    const/16 v35, 0x0

    invoke-direct/range {v34 .. v35}, Lcom/vlingo/midas/gui/ConversationActivity$TimeWidgetBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ConversationActivity$1;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->timeWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1118
    new-instance v34, Lcom/vlingo/midas/gui/ConversationActivity$UiFocusBroadcastReceiver;

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    move-object/from16 v2, v35

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$UiFocusBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Lcom/vlingo/midas/gui/ConversationActivity$1;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->uiFocusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1119
    new-instance v34, Lcom/vlingo/midas/gui/ConversationActivity$ENaviResolveLocationReciever;

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    move-object/from16 v2, v35

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ENaviResolveLocationReciever;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Lcom/vlingo/midas/gui/ConversationActivity$1;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->eNaviResolveLocationReciever:Landroid/content/BroadcastReceiver;

    .line 1121
    new-instance v25, Landroid/content/IntentFilter;

    invoke-direct/range {v25 .. v25}, Landroid/content/IntentFilter;-><init>()V

    .line 1122
    .local v25, "screenoff":Landroid/content/IntentFilter;
    const-string/jumbo v34, "android.intent.action.SCREEN_OFF"

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1123
    const-string/jumbo v34, "com.samsung.cover.OPEN"

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->helpWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v34, v0

    new-instance v35, Landroid/content/IntentFilter;

    const-string/jumbo v36, "com.vlingo.midas.gui.widgets.HelpChoiceWidget"

    invoke-direct/range {v35 .. v36}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->uiFocusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v34, v0

    new-instance v35, Landroid/content/IntentFilter;

    const-string/jumbo v36, "com.vlingo.midas.ACTION_UIFOCUS_CHANGED"

    invoke-direct/range {v35 .. v36}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->eNaviResolveLocationReciever:Landroid/content/BroadcastReceiver;

    move-object/from16 v34, v0

    new-instance v35, Landroid/content/IntentFilter;

    const-string/jumbo v36, "com.vlingo.midas.ACTION_ENAVI_RESOLVE_LOCATION"

    invoke-direct/range {v35 .. v36}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1134
    const-string/jumbo v34, "samsung_wakeup_data_enable"

    const/16 v35, 0x0

    invoke-static/range {v34 .. v35}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v34

    if-nez v34, :cond_24

    .line 1136
    const-string/jumbo v34, "samsung_wakeup_data_enable"

    const/16 v35, 0x1

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1139
    :cond_24
    const-string/jumbo v34, "barge_in_enabled"

    const/16 v35, 0x0

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1178
    sget-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->mIsStartedCustomWakeUpSetting:Z

    if-nez v34, :cond_25

    .line 1179
    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->isTOSRequired()Z

    move-result v34

    if-eqz v34, :cond_3c

    .line 1219
    :cond_25
    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/vlingo/midas/gui/ConversationActivity;->configureAutoStart(Landroid/content/Intent;)V

    .line 1221
    if-eqz v14, :cond_26

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v34

    if-nez v34, :cond_26

    .line 1226
    const-string/jumbo v34, "ACTION_SAFEREADER_LAUNCH"

    invoke-virtual {v14}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_26

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isIUXComplete()Z

    move-result v34

    if-eqz v34, :cond_26

    .line 1228
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/vlingo/midas/gui/ConversationActivity;->processSafeReaderMessages(Landroid/content/Intent;)Z

    move-result v34

    if-eqz v34, :cond_26

    .line 1229
    const/16 v34, 0x1

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->displayFromBackground:Z

    .line 1230
    const/16 v34, 0x1

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->resumeFromBackground:Z

    .line 1233
    new-instance v13, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v34

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lcom/vlingo/midas/VlingoApplication;->getMainActivityClass()Ljava/lang/Class;

    move-result-object v35

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-direct {v13, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1235
    .local v13, "i":Landroid/content/Intent;
    const/high16 v34, 0x10000000

    move/from16 v0, v34

    invoke-virtual {v13, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1236
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/gui/ConversationActivity;->setIntent(Landroid/content/Intent;)V

    .line 1241
    .end local v13    # "i":Landroid/content/Intent;
    :cond_26
    const/16 v34, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->setVolumeControlStream(I)V

    .line 1252
    sget v34, Lcom/vlingo/midas/R$xml;->debug_settings:I

    const/16 v35, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-static {v0, v1, v2}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;IZ)V

    .line 1253
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isLocationEnabled()Z

    move-result v19

    .line 1254
    .local v19, "locationEnabled":Z
    if-eqz v19, :cond_27

    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->isTOSRequired()Z

    move-result v34

    if-nez v34, :cond_27

    .line 1255
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->requestLocationUpdate()V

    .line 1258
    :cond_27
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->addListener(Lcom/vlingo/core/internal/audio/MicAnimationListener;)V

    .line 1261
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v34

    if-eqz v34, :cond_28

    .line 1262
    new-instance v34, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct/range {v34 .. v34}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    .line 1264
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1269
    :goto_6
    new-instance v34, Lcom/samsung/android/sdk/cover/ScoverManager;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 1270
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    .line 1272
    new-instance v34, Lcom/vlingo/midas/gui/ConversationActivity$2;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$2;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 1279
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setViewCoverLayout()V

    .line 1282
    :cond_28
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v34

    if-eqz v34, :cond_3f

    .line 1283
    const-string/jumbo v34, "widget_display_max"

    const/16 v35, 0x3

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 1284
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v34

    if-eqz v34, :cond_29

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v34

    const-string/jumbo v35, "supported_bvra"

    const/16 v36, 0x0

    invoke-static/range {v34 .. v36}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v34

    if-nez v34, :cond_29

    .line 1287
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v34

    sget v35, Lcom/vlingo/midas/R$string;->bvra_unsupported_toast:I

    const/16 v36, 0x1

    invoke-static/range {v34 .. v36}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/widget/Toast;->show()V

    .line 1299
    :cond_29
    :goto_7
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v34

    if-eqz v34, :cond_41

    .line 1300
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v34, :cond_2a

    .line 1301
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Lcom/vlingo/midas/gui/ControlFragment;->onPromptOnClick(Z)V

    .line 1303
    :cond_2a
    const-string/jumbo v34, "Listen"

    invoke-static/range {v34 .. v34}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->unregisterHandler(Ljava/lang/String;)V

    .line 1325
    :goto_8
    invoke-static {}, Lcom/vlingo/midas/news/NewsManager;->init()V

    .line 1326
    new-instance v34, Lcom/vlingo/midas/notification/NotificationPopUpManager;

    new-instance v35, Lcom/vlingo/midas/gui/ConversationActivity$3;

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$3;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    new-instance v36, Lcom/vlingo/midas/notification/MidasNotificationPopUpFactory;

    invoke-direct/range {v36 .. v36}, Lcom/vlingo/midas/notification/MidasNotificationPopUpFactory;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    move-object/from16 v2, v35

    move-object/from16 v3, v36

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/midas/notification/NotificationPopUpManager;-><init>(Landroid/app/Activity;Landroid/os/Handler;Lcom/vlingo/core/internal/notification/NotificationPopUpFactory;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->notificationManager:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    .line 1368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->notificationManager:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->setNotificationPopUpManager(Lcom/vlingo/core/internal/notification/NotificationPopUpManagerInterface;)V

    .line 1376
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setActionBarTalkback()V

    .line 1379
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v34

    if-eqz v34, :cond_2c

    .line 1380
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v34

    if-eqz v34, :cond_46

    .line 1381
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v34

    sget-object v35, Lcom/vlingo/midas/gui/DataCheckActivity;->DIALOG_PROCESSED:Ljava/lang/String;

    const/16 v36, 0x0

    invoke-virtual/range {v34 .. v36}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    if-nez v34, :cond_2b

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v34

    if-eqz v34, :cond_2c

    .line 1383
    :cond_2b
    new-instance v30, Lcom/samsung/samsungapps/UpdateManager;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/samsungapps/UpdateManager;-><init>(Landroid/content/Context;)V

    .line 1384
    .local v30, "updateManager":Lcom/samsung/samsungapps/UpdateManager;
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/samsungapps/UpdateManager;->startUpdateCheck()V

    .line 1392
    .end local v30    # "updateManager":Lcom/samsung/samsungapps/UpdateManager;
    :cond_2c
    :goto_9
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getPhoneStateListener()Landroid/telephony/PhoneStateListener;

    move-result-object v34

    check-cast v34, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    .line 1394
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v34

    const-string/jumbo v35, "from_always_mic_on"

    const/16 v36, 0x0

    invoke-virtual/range {v34 .. v36}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    if-eqz v34, :cond_47

    .line 1395
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v34

    const-string/jumbo v35, "com.vlingo.midas"

    const-string/jumbo v36, "LAUN"

    const-string/jumbo v37, "WAKEUP"

    invoke-static/range {v34 .. v37}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    :cond_2d
    :goto_a
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v34

    if-eqz v34, :cond_2e

    .line 1417
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->afterViews()V

    .line 1418
    sget-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->turnScreenOff:Z

    if-eqz v34, :cond_2e

    .line 1419
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual/range {v34 .. v34}, Lcom/vlingo/midas/gui/ControlFragment;->resetFinishMessage()V

    .line 1424
    :cond_2e
    new-instance v34, Lcom/vlingo/midas/gui/ConversationActivity$AlarmConceptCheck;

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    move-object/from16 v2, v35

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$AlarmConceptCheck;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Lcom/vlingo/midas/gui/ConversationActivity$1;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->isAlarmNewConcept:Landroid/content/BroadcastReceiver;

    .line 1425
    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    .line 1426
    .local v6, "alarmVersionCheckIntent":Landroid/content/IntentFilter;
    const-string/jumbo v34, "android.intent.action.RESPONSE_ALARM_BACKUPVERSION"

    move-object/from16 v0, v34

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->isAlarmNewConcept:Landroid/content/BroadcastReceiver;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v6}, Lcom/vlingo/midas/gui/ConversationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1429
    new-instance v5, Landroid/content/Intent;

    const-string/jumbo v34, "com.sec.android.clockpackage.REQUEST_ALARM_BACKUPVERSION"

    move-object/from16 v0, v34

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1430
    .local v5, "alarmIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1431
    return-void

    .line 803
    .end local v5    # "alarmIntent":Landroid/content/Intent;
    .end local v6    # "alarmVersionCheckIntent":Landroid/content/IntentFilter;
    .end local v12    # "featuresToListen":[Ljava/lang/String;
    .end local v19    # "locationEnabled":Z
    .end local v22    # "pkg":Ljava/lang/String;
    .end local v25    # "screenoff":Landroid/content/IntentFilter;
    :cond_2f
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v34

    if-eqz v34, :cond_30

    .line 804
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->currentTablet:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

    .line 805
    sget v34, Lcom/vlingo/midas/R$style;->actionBarStyle:I

    sput v34, Lcom/vlingo/midas/gui/ConversationActivity;->mTheme:I

    goto/16 :goto_0

    .line 807
    :cond_30
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v34

    if-eqz v34, :cond_37

    .line 808
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->actionReceived:Ljava/lang/String;

    .line 809
    const/16 v34, 0x0

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->toLaunchInMiniMode:Z

    .line 810
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->actionReceived:Ljava/lang/String;

    move-object/from16 v34, v0

    if-eqz v34, :cond_32

    .line 811
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->actionReceived:Ljava/lang/String;

    move-object/from16 v34, v0

    const-string/jumbo v35, "com.sec.action.SVOICE"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-nez v34, :cond_31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->actionReceived:Ljava/lang/String;

    move-object/from16 v34, v0

    const-string/jumbo v35, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-nez v34, :cond_31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->actionReceived:Ljava/lang/String;

    move-object/from16 v34, v0

    const-string/jumbo v35, "android.intent.action.VOICE_COMMAND"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_35

    :cond_31
    const/16 v34, 0x1

    :goto_b
    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->toLaunchInMiniMode:Z

    .line 814
    :cond_32
    sget v34, Lcom/vlingo/midas/R$style;->MidasBlackMainTheme:I

    sput v34, Lcom/vlingo/midas/gui/ConversationActivity;->mTheme:I

    .line 815
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v34

    const-string/jumbo v35, "isThisComeFromHomeKeyDoubleClickConcept"

    const/16 v36, 0x0

    invoke-virtual/range {v34 .. v36}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    if-nez v34, :cond_34

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    move/from16 v34, v0

    if-eqz v34, :cond_33

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->isKeyguardSecure:Z

    move/from16 v34, v0

    if-eqz v34, :cond_34

    :cond_33
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->toLaunchInMiniMode:Z

    move/from16 v34, v0

    if-eqz v34, :cond_36

    :cond_34
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v34

    if-eqz v34, :cond_36

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v34

    if-eqz v34, :cond_36

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v34

    if-nez v34, :cond_36

    .line 818
    const/16 v34, 0x1

    invoke-static/range {v34 .. v34}, Lcom/vlingo/midas/util/MiniModeUtils;->setSVoiceMiniMode(Z)V

    .line 819
    new-instance v34, Landroid/content/Intent;

    const-string/jumbo v35, "com.vlingo.midas.action.SVOICE_LAUNCHED"

    invoke-direct/range {v34 .. v35}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 820
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isProcessing()Z

    move-result v34

    if-nez v34, :cond_7

    .line 821
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v34

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v35

    sget v36, Lcom/vlingo/midas/R$drawable;->transparent:I

    invoke-virtual/range {v35 .. v36}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 811
    :cond_35
    const/16 v34, 0x0

    goto :goto_b

    .line 825
    :cond_36
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->disableTransculentTheme()V

    .line 826
    const/16 v34, 0x0

    invoke-static/range {v34 .. v34}, Lcom/vlingo/midas/util/MiniModeUtils;->setSVoiceMiniMode(Z)V

    goto/16 :goto_0

    .line 829
    :cond_37
    sget v34, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v34, Lcom/vlingo/midas/gui/ConversationActivity;->mTheme:I

    goto/16 :goto_0

    .line 836
    :cond_38
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCoverOpened()Z

    move-result v34

    if-eqz v34, :cond_8

    .line 837
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    .line 838
    .local v7, "attributes":Landroid/view/WindowManager$LayoutParams;
    iget v0, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    move/from16 v34, v0

    move/from16 v0, v34

    or-int/lit16 v0, v0, 0x600

    move/from16 v34, v0

    move/from16 v0, v34

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 839
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v7}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto/16 :goto_1

    .line 915
    .end local v7    # "attributes":Landroid/view/WindowManager$LayoutParams;
    .restart local v4    # "action":Ljava/lang/String;
    .restart local v22    # "pkg":Ljava/lang/String;
    .restart local v29    # "type":Ljava/lang/String;
    :cond_39
    const-string/jumbo v34, "ACTION_SAFEREADER_LAUNCH"

    invoke-virtual {v14}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_3a

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isIUXComplete()Z

    move-result v34

    if-eqz v34, :cond_3a

    .line 917
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v34

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->processSafeReaderMessages(Landroid/content/Intent;)Z

    .line 918
    const-string/jumbo v34, "keyguard"

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/app/KeyguardManager;

    .line 919
    .local v17, "km":Landroid/app/KeyguardManager;
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v34

    if-eqz v34, :cond_13

    invoke-virtual/range {v17 .. v17}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v34

    if-eqz v34, :cond_13

    .line 921
    const/16 v34, 0x1

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->releaseKeyguardByMsg:Z

    goto/16 :goto_2

    .line 927
    .end local v17    # "km":Landroid/app/KeyguardManager;
    :cond_3a
    const-string/jumbo v34, "SETTING_VOICE_LOCK"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    if-eqz v34, :cond_13

    .line 928
    const/16 v34, 0x1

    sput-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->mIsStartedCustomWakeUpSetting:Z

    .line 929
    const-string/jumbo v34, "IUX_CUSTOM_WAKEUP"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v35

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v34

    if-eqz v34, :cond_13

    goto/16 :goto_2

    .line 981
    .end local v4    # "action":Ljava/lang/String;
    .end local v29    # "type":Ljava/lang/String;
    :cond_3b
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    if-eqz v34, :cond_18

    .line 982
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    const/16 v35, 0xe

    invoke-virtual/range {v34 .. v35}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_3

    .line 1007
    :catch_0
    move-exception v11

    .line 1010
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 1187
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v12    # "featuresToListen":[Ljava/lang/String;
    .restart local v25    # "screenoff":Landroid/content/IntentFilter;
    :cond_3c
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/vlingo/midas/ServiceManager;->startLocalServices()Z

    .line 1189
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->requiresIUX()Z

    move-result v34

    if-nez v34, :cond_3d

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v34

    if-nez v34, :cond_3e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->isKModel:Z

    move/from16 v34, v0

    if-nez v34, :cond_3e

    .line 1197
    :cond_3d
    sget-boolean v34, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-static {v0, v1}, Lcom/vlingo/midas/iux/IUXManager;->processIUX(Landroid/app/Activity;Z)V

    .line 1198
    const/16 v34, 0x1

    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->doneTosAndIux:Z

    .line 1199
    :cond_3e
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v34

    if-eqz v34, :cond_25

    .line 1200
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->showPreferences()V

    goto/16 :goto_5

    .line 1290
    .restart local v19    # "locationEnabled":Z
    :cond_3f
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v34

    if-eqz v34, :cond_40

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v34

    const-string/jumbo v35, "supported_bvra"

    const/16 v36, 0x0

    invoke-static/range {v34 .. v36}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v34

    if-nez v34, :cond_40

    .line 1293
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v34

    sget v35, Lcom/vlingo/midas/R$string;->bvra_unsupported_toast:I

    const/16 v36, 0x1

    invoke-static/range {v34 .. v36}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v28

    .line 1295
    .local v28, "toast":Landroid/widget/Toast;
    invoke-virtual/range {v28 .. v28}, Landroid/widget/Toast;->show()V

    .line 1297
    .end local v28    # "toast":Landroid/widget/Toast;
    :cond_40
    const-string/jumbo v34, "widget_display_max"

    const/16 v35, 0x5

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto/16 :goto_7

    .line 1305
    :cond_41
    const-string/jumbo v34, "is_voice_prompt_manual_change"

    const/16 v35, 0x0

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v34

    const/16 v35, 0x15

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_43

    .line 1306
    const-string/jumbo v34, "voice_prompt_preference_value"

    const/16 v35, 0x15

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 1307
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v34, :cond_42

    .line 1308
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Lcom/vlingo/midas/gui/ControlFragment;->onPromptOnClick(Z)V

    .line 1310
    :cond_42
    const-string/jumbo v34, "is_voice_prompt_manual_change"

    const/16 v35, 0x0

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto/16 :goto_8

    .line 1313
    :cond_43
    const-string/jumbo v34, "voice_prompt_preference_value"

    const/16 v35, 0x14

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v34

    const/16 v35, 0x14

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_45

    .line 1314
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v34, :cond_44

    .line 1315
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Lcom/vlingo/midas/gui/ControlFragment;->onPromptOffClick(Z)V

    .line 1322
    :cond_44
    :goto_c
    const-string/jumbo v34, "Listen"

    const-class v35, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ListenHandler;

    invoke-static/range {v34 .. v35}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Ljava/lang/String;Ljava/lang/Class;)V

    goto/16 :goto_8

    .line 1318
    :cond_45
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v34, :cond_44

    .line 1319
    sget-object v34, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Lcom/vlingo/midas/gui/ControlFragment;->onPromptOnClick(Z)V

    goto :goto_c

    .line 1387
    :cond_46
    new-instance v30, Lcom/samsung/samsungapps/UpdateManager;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/samsungapps/UpdateManager;-><init>(Landroid/content/Context;)V

    .line 1388
    .restart local v30    # "updateManager":Lcom/samsung/samsungapps/UpdateManager;
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/samsungapps/UpdateManager;->startUpdateCheck()V

    goto/16 :goto_9

    .line 1398
    .end local v30    # "updateManager":Lcom/samsung/samsungapps/UpdateManager;
    :cond_47
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    .line 1399
    .local v18, "launchMethod":Ljava/lang/String;
    if-eqz v18, :cond_4a

    .line 1400
    const-string/jumbo v34, "com.sec.action.SVOICE"

    move-object/from16 v0, v18

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_48

    .line 1401
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v34

    const-string/jumbo v35, "com.vlingo.midas"

    const-string/jumbo v36, "LAUN"

    const-string/jumbo v37, "HOMEKEY"

    invoke-static/range {v34 .. v37}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1403
    :cond_48
    const-string/jumbo v34, "android.intent.action.VOICE_COMMAND"

    move-object/from16 v0, v18

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_49

    .line 1404
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v34

    const-string/jumbo v35, "com.vlingo.midas"

    const-string/jumbo v36, "LAUN"

    const-string/jumbo v37, "BT"

    invoke-static/range {v34 .. v37}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1406
    :cond_49
    const-string/jumbo v34, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    move-object/from16 v0, v18

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_2d

    .line 1407
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v34

    const-string/jumbo v35, "com.vlingo.midas"

    const-string/jumbo v36, "LAUN"

    const-string/jumbo v37, "EARPHONE"

    invoke-static/range {v34 .. v37}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1412
    :cond_4a
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v34

    const-string/jumbo v35, "com.vlingo.midas"

    const-string/jumbo v36, "LAUN"

    const-string/jumbo v37, "ICON"

    invoke-static/range {v34 .. v37}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1266
    .end local v18    # "launchMethod":Ljava/lang/String;
    :catch_1
    move-exception v34

    goto/16 :goto_6

    .line 1265
    :catch_2
    move-exception v34

    goto/16 :goto_6

    .line 1004
    .end local v12    # "featuresToListen":[Ljava/lang/String;
    .end local v19    # "locationEnabled":Z
    .end local v25    # "screenoff":Landroid/content/IntentFilter;
    :catch_3
    move-exception v34

    goto/16 :goto_4
.end method

.method public onCreateDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2828
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->onCreateDescription()V

    .line 2829
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onCreateDescription()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2695
    const-string/jumbo v1, "svoicedebugging"

    const-string/jumbo v2, "Focus onDestroy() of Svoice"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2700
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->unbindFacebookService(Landroid/content/Context;)V

    .line 2704
    sget-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->turnScreenOff:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->lockInMiniMode:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/SViewCoverUtils;->isCoverGUISet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2705
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->voiceWakeUpLCDOff()V

    .line 2708
    :cond_0
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2709
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->unbindTwitterService(Landroid/content/Context;)V

    .line 2711
    :cond_1
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 2712
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->smCurrentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

    if-ne v1, p0, :cond_2

    .line 2713
    sput-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->smCurrentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

    .line 2717
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->observer:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2720
    const/4 v1, 0x1

    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->threadVar:Z

    .line 2721
    sput-object v4, Lcom/vlingo/midas/gui/DialogFragment;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    .line 2722
    sput-object v4, Lcom/vlingo/midas/gui/DialogFragment;->eventList:Ljava/util/ArrayList;

    .line 2723
    sput-object v4, Lcom/vlingo/midas/gui/DialogFragment;->birthdayList:Ljava/util/ArrayList;

    .line 2725
    invoke-static {}, Lcom/vlingo/midas/news/NewsManager;->deinit()V

    .line 2726
    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->setMusicLaunchedFromSvoice(Z)V

    .line 2727
    invoke-static {v3}, Lcom/vlingo/midas/util/MiniModeUtils;->setMiniModeValueinDB(Z)V

    .line 2730
    sput-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    .line 2732
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onDestroy()V

    .line 2734
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0x22c

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2735
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0x22b

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2736
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2737
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2738
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2739
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2740
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2742
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSeamlessWakeupStarted()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2743
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->stopSeamlessVoiceWakeup()V

    .line 2744
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->startVoiceWakeup()V

    .line 2745
    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->setSeamlessWakeupStarted(Z)V

    .line 2748
    :cond_3
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->removeLocationListener()V

    .line 2750
    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->unlockLockscreen(Z)V

    .line 2752
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->releaseFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;)V

    .line 2760
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerDestroy()V

    .line 2762
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_4

    .line 2763
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->screenEventBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2766
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_5

    .line 2767
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2769
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->timeWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_6

    .line 2771
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->timeWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2772
    const-string/jumbo v1, "ConversationActivity"

    const-string/jumbo v2, "TimerReceiver unregistered successfully"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2777
    :goto_0
    iput-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->timeWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 2779
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->uiFocusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_7

    .line 2780
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->uiFocusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2782
    :cond_7
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->eNaviResolveLocationReciever:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_8

    .line 2783
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->eNaviResolveLocationReciever:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2785
    :cond_8
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getADMController()Lcom/vlingo/core/internal/util/ADMController;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vlingo/core/internal/util/ADMController;->removeListener(Lcom/vlingo/core/internal/util/ADMFeatureListener;)V

    .line 2787
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_a

    .line 2795
    :goto_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->dereferenceObjects()V

    .line 2796
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 2797
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->removeListener(Lcom/vlingo/core/internal/audio/MicAnimationListener;)V

    .line 2799
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isAlarmNewConcept:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_9

    .line 2800
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isAlarmNewConcept:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2801
    iput-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isAlarmNewConcept:Landroid/content/BroadcastReceiver;

    .line 2803
    :cond_9
    return-void

    .line 2774
    :catch_0
    move-exception v0

    .line 2775
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string/jumbo v1, "ConversationActivity"

    const-string/jumbo v2, "TimerReceiver not register yet"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2791
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_a
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$color;->solid_black:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto :goto_1
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 6017
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 6021
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6022
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 6023
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessage(I)Z

    .line 6025
    :cond_0
    return-void
.end method

.method public onHomePressed()V
    .locals 3

    .prologue
    .line 2903
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2905
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.app.action.CLOSE_APPLICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2906
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2907
    return-void
.end method

.method public onInterceptStartReco()Z
    .locals 1

    .prologue
    .line 4508
    const/4 v0, 0x0

    return v0
.end method

.method public onLayoutInflated()V
    .locals 6

    .prologue
    .line 6107
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int v1, v2, v3

    .line 6108
    .local v1, "total":I
    int-to-double v2, v1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    const-wide v4, 0x3fe6666666666666L    # 0.7

    cmpl-double v2, v2, v4

    if-lez v2, :cond_1

    const/4 v0, 0x1

    .line 6109
    .local v0, "shouldResize":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 6112
    :cond_0
    return-void

    .line 6108
    .end local v0    # "shouldResize":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 5422
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 5423
    const/4 v0, 0x0

    return v0
.end method

.method public onMicAnimationData([I)V
    .locals 0
    .param p1, "v"    # [I

    .prologue
    .line 5599
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v11, 0x13

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 5054
    const-string/jumbo v6, "svoicedebugging"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Focus onNewIntent() of Svoice "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5057
    const-string/jumbo v6, "isLaunchFromTutorialIUX"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    sput-boolean v6, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialStartedFromIUX:Z

    .line 5058
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v6

    if-eqz v6, :cond_0

    sget-boolean v6, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialStartedFromIUX:Z

    if-eqz v6, :cond_0

    .line 5059
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$drawable;->voice_talk_bg_tile:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 5062
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/midas/VlingoApplication;->isAppInForeground()Z

    move-result v6

    if-nez v6, :cond_1

    .line 5063
    invoke-direct {p0, p1, v8}, Lcom/vlingo/midas/gui/ConversationActivity;->checkDataUsagePopup(Landroid/content/Intent;Z)V

    .line 5067
    :cond_1
    sget-object v6, Lcom/vlingo/midas/gui/ConversationActivity;->smCurrentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

    if-ne v6, p0, :cond_2

    const-string/jumbo v6, "isThisComeFromHomeKeyDoubleClickConcept"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 5070
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 5073
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplication()Landroid/app/Application;

    move-result-object v6

    check-cast v6, Lcom/vlingo/midas/VlingoApplication;

    invoke-virtual {v6}, Lcom/vlingo/midas/VlingoApplication;->isSocialLogin()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 5074
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 5077
    :cond_3
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 5078
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 5079
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    .line 5081
    .local v4, "type":Ljava/lang/String;
    const-string/jumbo v6, "displayFromBackground"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    const-string/jumbo v6, "resumeFromBackground"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 5083
    sget-object v6, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v6, :cond_4

    .line 5084
    sget-object v6, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v6}, Lcom/vlingo/midas/gui/ControlFragment;->cancelTTS()V

    .line 5088
    :cond_4
    const-string/jumbo v6, "from_always_mic_on"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z

    .line 5090
    iget-boolean v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z

    if-eqz v6, :cond_5

    .line 5092
    const-string/jumbo v6, "wake_up_phrase_as_string"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 5093
    .local v5, "wakeUpPhraseAsString":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v6

    new-instance v9, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;

    invoke-direct {v9, v5, v7}, Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v6, v9}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->setWakeUpContext(Lcom/vlingo/core/internal/dialogmanager/WakeUpContext;)V

    .line 5094
    iput-boolean v8, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAutoListeningmodeAlways:Z

    .line 5095
    iput-boolean v8, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    .line 5098
    .end local v5    # "wakeUpPhraseAsString":Ljava/lang/String;
    :cond_5
    const-string/jumbo v6, "keyguard"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/KeyguardManager;

    .line 5099
    .local v3, "keyguardManager":Landroid/app/KeyguardManager;
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v6

    if-nez v6, :cond_7

    :cond_6
    sget-boolean v6, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->wasScreenOff:Z

    if-eqz v6, :cond_8

    iget-boolean v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    if-eqz v6, :cond_8

    .line 5101
    :cond_7
    iput-boolean v8, p0, Lcom/vlingo/midas/gui/ConversationActivity;->checkForAbnormalonPause:Z

    .line 5102
    iget-object v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v6, v11}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 5103
    iget-object v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const-wide/16 v9, 0xfa0

    invoke-virtual {v6, v11, v9, v10}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 5105
    invoke-virtual {v3}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v6

    iput-boolean v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isKeyguardSecure:Z

    .line 5106
    sput-boolean v7, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->wasScreenOff:Z

    .line 5109
    :cond_8
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCoverOpened()Z

    move-result v6

    if-eqz v6, :cond_9

    iget-boolean v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z

    if-eqz v6, :cond_9

    .line 5110
    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/ConversationActivity;->unlockLockscreen(Z)V

    .line 5113
    :cond_9
    sget-object v6, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v6, :cond_a

    .line 5115
    sget-object v6, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v6}, Lcom/vlingo/midas/gui/Widget;->onStop()V

    .line 5119
    :cond_a
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v6

    if-eqz v6, :cond_e

    .line 5120
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->actionReceived:Ljava/lang/String;

    .line 5121
    iput-boolean v7, p0, Lcom/vlingo/midas/gui/ConversationActivity;->toLaunchInMiniMode:Z

    .line 5122
    iget-object v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->actionReceived:Ljava/lang/String;

    if-eqz v6, :cond_c

    .line 5123
    iget-object v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->actionReceived:Ljava/lang/String;

    const-string/jumbo v9, "com.sec.action.SVOICE"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_b

    iget-object v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->actionReceived:Ljava/lang/String;

    const-string/jumbo v9, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_b

    iget-object v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->actionReceived:Ljava/lang/String;

    const-string/jumbo v9, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_12

    :cond_b
    move v6, v8

    :goto_0
    iput-boolean v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->toLaunchInMiniMode:Z

    .line 5126
    :cond_c
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string/jumbo v8, "isThisComeFromHomeKeyDoubleClickConcept"

    invoke-virtual {v6, v8, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-nez v6, :cond_d

    iget-boolean v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    if-nez v6, :cond_d

    iget-boolean v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->toLaunchInMiniMode:Z

    if-eqz v6, :cond_e

    :cond_d
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v6

    if-eqz v6, :cond_e

    .line 5128
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v8, "com.vlingo.midas.action.SVOICE_LAUNCHED"

    invoke-direct {v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/gui/ConversationActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 5132
    :cond_e
    iput-boolean v7, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isWakeLockChanged:Z

    .line 5133
    const-string/jumbo v6, "com.vlingo.midas.action.SEND"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    if-eqz v4, :cond_13

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v6

    if-nez v6, :cond_f

    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v6

    if-eqz v6, :cond_13

    .line 5135
    :cond_f
    const-string/jumbo v6, "text/plain"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 5136
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity;->handleTextRequestIntent(Landroid/content/Intent;)V

    .line 5181
    :cond_10
    :goto_1
    iget-object v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v6}, Lcom/vlingo/midas/gui/ContentFragment;->cancelAsrEditing()V

    .line 5183
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v6

    if-eqz v6, :cond_11

    invoke-static {}, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen()Z

    move-result v6

    if-nez v6, :cond_11

    .line 5184
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setViewCoverLayout()V

    .line 5186
    :cond_11
    return-void

    :cond_12
    move v6, v7

    .line 5123
    goto :goto_0

    .line 5138
    :cond_13
    const-string/jumbo v6, "ACTION_SAFEREADER_LAUNCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isIUXComplete()Z

    move-result v6

    if-eqz v6, :cond_14

    .line 5140
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->unlockScreenIfLocked()V

    .line 5141
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity;->processSafeReaderMessages(Landroid/content/Intent;)Z

    goto :goto_1

    .line 5143
    :cond_14
    const-string/jumbo v6, "isSecure"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 5144
    .local v2, "isSecure":Z
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_19

    const-string/jumbo v6, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    const-string/jumbo v6, "isThisComeFromHomeKeyDoubleClickConcept"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_17

    :cond_15
    const-string/jumbo v6, "com.sec.action.SVOICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    const-string/jumbo v6, "isThisComeFromHomeKeyDoubleClickConcept"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_17

    :cond_16
    const-string/jumbo v6, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_17

    const-string/jumbo v6, "android.speech.action.WEB_SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    :cond_17
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v6

    if-nez v6, :cond_18

    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v6

    if-eqz v6, :cond_19

    .line 5157
    :cond_18
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v6

    if-nez v6, :cond_19

    .line 5161
    sget-object v6, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v6, :cond_19

    .line 5162
    sget-object v6, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v6}, Lcom/vlingo/midas/gui/ControlFragment;->checkMissedEvents()Z

    move-result v6

    iput-boolean v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->startRecoBySCO:Z

    .line 5165
    :cond_19
    if-nez v2, :cond_1a

    .line 5166
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/high16 v7, 0x400000

    invoke-virtual {v6, v7}, Landroid/view/Window;->addFlags(I)V

    .line 5170
    iget-object v6, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v7, 0x12

    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v7, v8, v9}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 5173
    :cond_1a
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity;->acquireWakeLock(Landroid/content/Intent;)V

    .line 5175
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 5176
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity;->configureAutoStart(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 2975
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v11

    .line 2977
    .local v11, "returnVal":Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 3322
    const/4 v11, 0x0

    .line 3326
    :cond_0
    :goto_0
    :sswitch_0
    return v11

    .line 2979
    :sswitch_1
    new-instance v7, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/settings/debug/DebugSettings;

    invoke-direct {v7, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2980
    .local v7, "i":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v7, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2981
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v7, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2984
    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "key"

    const-string/jumbo v3, "gottajibboo"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2985
    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    .line 2986
    const/4 v11, 0x1

    .line 2987
    goto :goto_0

    .line 2991
    .end local v7    # "i":Landroid/content/Intent;
    :sswitch_2
    const/4 v11, 0x1

    .line 2992
    goto :goto_0

    .line 2997
    :sswitch_3
    const/4 v11, 0x1

    .line 2998
    goto :goto_0

    .line 3110
    :sswitch_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3111
    new-instance v7, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {v7, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3115
    .restart local v7    # "i":Landroid/content/Intent;
    :goto_1
    const-string/jumbo v2, "is_start_option_menu"

    invoke-virtual {v7, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3116
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3117
    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    .line 3118
    :cond_1
    const/4 v11, 0x1

    .line 3119
    goto :goto_0

    .line 3113
    .end local v7    # "i":Landroid/content/Intent;
    :cond_2
    new-instance v7, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    invoke-direct {v7, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v7    # "i":Landroid/content/Intent;
    goto :goto_1

    .line 3122
    .end local v7    # "i":Landroid/content/Intent;
    :sswitch_5
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "cradle_enable"

    invoke-static {v2, v3, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_3

    .line 3127
    .local v6, "PhoneSpeakerState":I
    :goto_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "cradle_enable"

    invoke-static {v1, v2, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 3130
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 3131
    .local v10, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.intent.action.INTERNAL_SPEAKER"

    invoke-virtual {v10, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3134
    const-string/jumbo v1, "state"

    invoke-virtual {v10, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3135
    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 3136
    const/4 v11, 0x1

    .line 3137
    goto :goto_0

    .end local v6    # "PhoneSpeakerState":I
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_3
    move v6, v1

    .line 3122
    goto :goto_2

    .line 3153
    :sswitch_6
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->showHelp()V

    goto/16 :goto_0

    .line 3157
    :sswitch_7
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->addHelpChoiceWidget()V

    goto/16 :goto_0

    .line 3161
    :sswitch_8
    new-instance v7, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/help/WhatCanISayScreen;

    invoke-direct {v7, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3163
    .restart local v7    # "i":Landroid/content/Intent;
    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    .line 3164
    const/4 v11, 0x1

    .line 3165
    goto/16 :goto_0

    .line 3197
    .end local v7    # "i":Landroid/content/Intent;
    :sswitch_9
    invoke-static {p0}, Lcom/vlingo/midas/util/HelpHubCommon;->isResourceAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 3198
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3199
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isHelpLaunched:Z

    .line 3202
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.samsung.helphub"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 3203
    .local v9, "info":Landroid/content/pm/PackageInfo;
    const/4 v10, 0x0

    .line 3204
    .restart local v10    # "intent":Landroid/content/Intent;
    iget v1, v9, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v1, v1, 0xa

    packed-switch v1, :pswitch_data_0

    .line 3234
    new-instance v10, Landroid/content/Intent;

    .end local v10    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.samsung.helphub.HELP"

    invoke-direct {v10, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3235
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "helphub:section"

    const-string/jumbo v2, "svoice"

    invoke-virtual {v10, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3236
    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    .line 3248
    .end local v9    # "info":Landroid/content/pm/PackageInfo;
    .end local v10    # "intent":Landroid/content/Intent;
    :goto_3
    :pswitch_0
    const/4 v11, 0x1

    .line 3249
    goto/16 :goto_0

    .line 3215
    .restart local v9    # "info":Landroid/content/pm/PackageInfo;
    .restart local v10    # "intent":Landroid/content/Intent;
    :pswitch_1
    new-instance v10, Landroid/content/Intent;

    .end local v10    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.samsung.helphub.HELP"

    invoke-direct {v10, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3216
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "helphub:section"

    const-string/jumbo v2, "svoice"

    invoke-virtual {v10, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3218
    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    .line 3239
    .end local v9    # "info":Landroid/content/pm/PackageInfo;
    .end local v10    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    goto :goto_3

    .line 3228
    .restart local v9    # "info":Landroid/content/pm/PackageInfo;
    .restart local v10    # "intent":Landroid/content/Intent;
    :pswitch_2
    new-instance v10, Landroid/content/Intent;

    .end local v10    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.samsung.helphub.HELP"

    invoke-direct {v10, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3229
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "helphub:appid"

    const-string/jumbo v2, "s_voice"

    invoke-virtual {v10, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3231
    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 3244
    .end local v9    # "info":Landroid/content/pm/PackageInfo;
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_4
    new-instance v10, Landroid/content/Intent;

    const-string/jumbo v1, "com.samsung.helphub.HELP"

    invoke-direct {v10, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3245
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "helphub:section"

    const-string/jumbo v2, "svoice"

    invoke-virtual {v10, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3246
    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    .line 3251
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-static {p0}, Lcom/vlingo/midas/util/HelpHubCommon;->requestDownloadingResource(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 3256
    :sswitch_a
    invoke-static {}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->getShouldAskValue()Z

    move-result v2

    if-nez v2, :cond_6

    .line 3257
    const-string/jumbo v2, "voice_prompt_preference_value"

    const/16 v3, 0x15

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 3258
    :cond_6
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_7

    .line 3259
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v1}, Lcom/vlingo/midas/gui/ControlFragment;->onPromptOnClick(Z)V

    .line 3261
    :cond_7
    invoke-static {v1}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->isVoicePromptShowing(Z)V

    goto/16 :goto_0

    .line 3264
    :sswitch_b
    invoke-static {}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->getShouldAskValue()Z

    move-result v2

    if-nez v2, :cond_8

    .line 3265
    const-string/jumbo v2, "voice_prompt_preference_value"

    const/16 v3, 0x14

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 3266
    :cond_8
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_9

    .line 3267
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v1}, Lcom/vlingo/midas/gui/ControlFragment;->onPromptOffClick(Z)V

    .line 3269
    :cond_9
    invoke-static {v1}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->isVoicePromptShowing(Z)V

    goto/16 :goto_0

    .line 3273
    :sswitch_c
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogFragment;->isInEditMode()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 3274
    const-string/jumbo v2, "input_method"

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    .line 3275
    .local v8, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v12

    .line 3276
    .local v12, "view":Landroid/view/View;
    if-eqz v12, :cond_a

    .line 3277
    invoke-virtual {v12}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v8, v2, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 3282
    .end local v8    # "imm":Landroid/view/inputmethod/InputMethodManager;
    .end local v12    # "view":Landroid/view/View;
    :cond_a
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->startTutorial()V

    .line 3283
    sput-boolean v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialFromSettings:Z

    goto/16 :goto_0

    .line 3289
    :sswitch_d
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3291
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    iput v1, v2, Lcom/vlingo/midas/gui/DialogFragment;->tutorialShadow:I

    .line 3292
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    iput-boolean v6, v1, Lcom/vlingo/midas/gui/DialogFragment;->isYouCanSay:Z

    .line 3293
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    iput-boolean v6, v1, Lcom/vlingo/midas/gui/DialogFragment;->setShadowWhileNotAnim:Z

    .line 3294
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->addHelpChoiceWidget()V

    goto/16 :goto_0

    .line 3305
    :sswitch_e
    new-instance v0, Lcom/vlingo/midas/datadeletion/ConfirmDialog;

    sget v1, Lcom/vlingo/midas/R$string;->user_data_deletion_confirm_title:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->user_data_deletion_confirm:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->cancel:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->ok_uc:I

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->user_data_deletion_confirm_server_failed:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/midas/datadeletion/ConfirmDialog;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3310
    .local v0, "confirmDialog":Lcom/vlingo/midas/datadeletion/ConfirmDialog;
    invoke-virtual {v0, p0}, Lcom/vlingo/midas/datadeletion/ConfirmDialog;->showConfirmDialog(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2977
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x9 -> :sswitch_5
        0xd -> :sswitch_6
        0xe -> :sswitch_7
        0x11 -> :sswitch_8
        0x13 -> :sswitch_9
        0x14 -> :sswitch_a
        0x15 -> :sswitch_b
        0x16 -> :sswitch_c
        0x17 -> :sswitch_d
        0x3e7 -> :sswitch_e
        0x3e8 -> :sswitch_1
        0x3e9 -> :sswitch_2
        0x3ea -> :sswitch_3
        0x102002c -> :sswitch_0
    .end sparse-switch

    .line 3204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onPause()V
    .locals 12

    .prologue
    const/16 v11, 0xf

    const/16 v10, 0x8

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2403
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpottingRightNow()V

    .line 2404
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v4, v5}, Landroid/view/Window;->clearFlags(I)V

    .line 2405
    const-string/jumbo v4, "svoicedebugging"

    const-string/jumbo v5, "Focus onPause() of Svoice"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2414
    invoke-static {p0}, Lcom/vlingo/midas/settings/MidasSettings;->resetVolumeNormal(Landroid/content/Context;)V

    .line 2415
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v4}, Lcom/vlingo/midas/ui/HomeKeyListener;->onPause()V

    .line 2416
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocusInSync()V

    .line 2417
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v4, :cond_0

    .line 2418
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v4, v9}, Lcom/vlingo/midas/gui/ControlFragment;->setTaskOnFinishGreetingTTS(Ljava/lang/Runnable;)V

    .line 2420
    :cond_0
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->removeListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 2421
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->stop()V

    .line 2422
    sget-boolean v4, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    if-eqz v4, :cond_9

    .line 2423
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->incomingCallReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v4, :cond_1

    .line 2424
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->incomingCallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/ConversationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2425
    iput-object v9, p0, Lcom/vlingo/midas/gui/ConversationActivity;->incomingCallReceiver:Landroid/content/BroadcastReceiver;

    .line 2431
    :cond_1
    sget-boolean v4, Lcom/vlingo/midas/gui/ConversationActivity;->isVoicePromptOn:Z

    if-eqz v4, :cond_2

    .line 2432
    const-string/jumbo v4, "use_voice_prompt"

    invoke-static {v4, v8}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 2434
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->reRegisterPhraseSpottingAfterTutorial()V

    .line 2435
    sput-boolean v7, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    .line 2436
    sput-boolean v7, Lcom/vlingo/midas/gui/ConversationActivity;->doLaunchTutorial:Z

    .line 2437
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 2438
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v4, :cond_3

    .line 2439
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v4, v7}, Lcom/vlingo/midas/gui/ControlFragment;->removeMicStatusTextForTutorial(Z)V

    .line 2440
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v5, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 2442
    :cond_3
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v4, v7}, Lcom/vlingo/midas/gui/DialogFragment;->resetAllContentForTutorial(Z)V

    .line 2443
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    if-eqz v4, :cond_4

    .line 2444
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 2447
    :cond_4
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v4, :cond_5

    .line 2448
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/16 v5, 0x4b0

    invoke-virtual {v4, v7, v5}, Lcom/vlingo/midas/gui/ControlFragment;->fadeInMicLayout(ZI)V

    .line 2480
    :cond_5
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_6

    .line 2481
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2483
    :cond_6
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v4, :cond_7

    .line 2484
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v4, v7}, Lcom/vlingo/midas/gui/ControlFragment;->setFakeSvoiceMiniForTutorial(Z)V

    .line 2486
    :cond_7
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$drawable;->voice_talk_bg_tile:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2487
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    if-eqz v4, :cond_8

    .line 2488
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2492
    :cond_8
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v4

    if-eqz v4, :cond_18

    .line 2493
    invoke-static {p0, v9}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    move-result-object v3

    .line 2498
    .local v3, "mSVoiceTutorialWidgetObj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :goto_1
    invoke-virtual {v3}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->destroy()V

    .line 2500
    .end local v3    # "mSVoiceTutorialWidgetObj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :cond_9
    sget-boolean v4, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v4, :cond_a

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2501
    invoke-static {v8}, Lcom/vlingo/midas/iux/IUXManager;->setIUXComplete(Z)V

    .line 2502
    invoke-static {v7}, Lcom/vlingo/midas/iux/IUXManager;->setIUXIntroRequired(Z)V

    .line 2503
    sput-boolean v7, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    .line 2506
    :cond_a
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    if-eqz v4, :cond_b

    .line 2507
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    invoke-virtual {v4, p0}, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->removeListener(Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;)V

    .line 2510
    :cond_b
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onPause()V

    .line 2512
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 2513
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 2515
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2516
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v4, v8}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2517
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v4, v7}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2518
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v4, v10}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2520
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v5, 0xd

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2521
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v5, 0x22b

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2522
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v5, 0x22c

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2523
    iput-boolean v7, p0, Lcom/vlingo/midas/gui/ConversationActivity;->displayFromBackground:Z

    .line 2524
    iput-boolean v7, p0, Lcom/vlingo/midas/gui/ConversationActivity;->resumeFromBackground:Z

    .line 2526
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->isFinishing()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2527
    iput-boolean v7, p0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    .line 2531
    :cond_c
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->smCurrentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

    if-ne v4, p0, :cond_d

    iget-boolean v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->releaseKeyguardByMsg:Z

    if-nez v4, :cond_d

    .line 2532
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v5

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/VlingoApplication;

    invoke-virtual {v4}, Lcom/vlingo/midas/VlingoApplication;->isSocialLogin()Z

    move-result v4

    invoke-virtual {v5, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn(Z)V

    .line 2535
    :cond_d
    iput-boolean v7, p0, Lcom/vlingo/midas/gui/ConversationActivity;->releaseKeyguardByMsg:Z

    .line 2536
    sget-boolean v4, Lcom/vlingo/midas/gui/ConversationActivity;->mIsStartedCustomWakeUpSetting:Z

    if-eqz v4, :cond_e

    .line 2537
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->finish()V

    .line 2540
    :cond_e
    iget-boolean v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->doneTosAndIux:Z

    if-nez v4, :cond_f

    sget-boolean v4, Lcom/vlingo/midas/gui/ConversationActivity;->mIsStartedCustomWakeUpSetting:Z

    if-eqz v4, :cond_f

    .line 2541
    sput-boolean v7, Lcom/vlingo/midas/gui/ConversationActivity;->mIsStartedCustomWakeUpSetting:Z

    .line 2543
    :cond_f
    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/ConversationActivity;->getWakeupLock(Z)V

    .line 2545
    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/ConversationActivity;->setControlFragmentActivityPaused(Z)V

    .line 2548
    iget-boolean v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->checkForAbnormalonPause:Z

    if-nez v4, :cond_11

    .line 2549
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->stopSeamlessVoiceWakeup()V

    .line 2550
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->startVoiceWakeup()V

    .line 2552
    iget-boolean v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAutoListeningmodeAlways:Z

    if-nez v4, :cond_10

    const-string/jumbo v4, "should_wakeup_enabled"

    invoke-static {v4, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 2554
    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/ConversationActivity;->unlockLockscreen(Z)V

    .line 2556
    :cond_10
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v4

    if-eqz v4, :cond_11

    .line 2559
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v4, v11}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2560
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const-wide/16 v5, 0x1f4

    invoke-virtual {v4, v11, v5, v6}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 2565
    :cond_11
    invoke-static {v7}, Lcom/vlingo/midas/settings/MidasSettings;->setForegroundState(Z)V

    .line 2566
    invoke-static {}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->getInstance()Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/core/internal/display/WakeLockManager;->releaseWakeLock()V

    .line 2568
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x10

    invoke-virtual {v4, v5}, Landroid/view/Window;->clearFlags(I)V

    .line 2570
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 2571
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v4, :cond_12

    .line 2572
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 2577
    :cond_12
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "input_method"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 2579
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2584
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_2
    return-void

    .line 2450
    :cond_13
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->invalidateOptionsMenu()V

    .line 2451
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/DialogFragment;->resetScreen()V

    .line 2453
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    if-eqz v4, :cond_14

    .line 2454
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2456
    :cond_14
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v4, :cond_15

    .line 2457
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    invoke-virtual {v4, v10}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 2462
    :cond_15
    iget-boolean v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->isFromBackPress:Z

    if-nez v4, :cond_17

    .line 2463
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v4, :cond_16

    .line 2464
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v4, v7, v7}, Lcom/vlingo/midas/gui/ControlFragment;->fadeInMicLayout(ZI)V

    .line 2465
    sget-object v4, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v5, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 2467
    :cond_16
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v4, v7}, Lcom/vlingo/midas/gui/DialogFragment;->resetAllContentForTutorial(Z)V

    .line 2474
    :cond_17
    invoke-static {v7}, Lcom/vlingo/midas/settings/MidasSettings;->setAllNotificationsAccepted(Z)V

    .line 2475
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2476
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "OnBack"

    invoke-virtual {v2, v4, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2477
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2495
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_18
    invoke-static {p0, v9}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    move-result-object v3

    .restart local v3    # "mSVoiceTutorialWidgetObj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    goto/16 :goto_1

    .line 2580
    .end local v3    # "mSVoiceTutorialWidgetObj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :catch_0
    move-exception v0

    .line 2582
    .local v0, "e1":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x4

    const/16 v7, 0x17

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 3415
    sget-boolean v2, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorial:Z

    if-nez v2, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3511
    :cond_0
    :goto_0
    return v6

    .line 3418
    :cond_1
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 3419
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 3420
    .local v0, "inflater":Landroid/view/MenuInflater;
    sget v2, Lcom/vlingo/midas/R$menu;->main_activity_actions:I

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 3422
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    .line 3424
    .local v1, "returnVal":Z
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 3426
    sget-boolean v2, Lcom/vlingo/midas/settings/debug/DebugSettings;->SHOW_DEBUG:Z

    if-eqz v2, :cond_3

    .line 3427
    const/16 v2, 0x3e8

    const-string/jumbo v3, "Debug Settings"

    invoke-interface {p1, v5, v2, v8, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 3430
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSpeakerID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 3432
    const/16 v2, 0x3e7

    const/16 v3, 0x63

    const-string/jumbo v4, "Delete All Personal Data"

    invoke-interface {p1, v5, v2, v3, v4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 3439
    :cond_2
    const/16 v2, 0xd

    sget v3, Lcom/vlingo/midas/R$string;->help:I

    invoke-interface {p1, v5, v2, v9, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 3465
    const/4 v1, 0x1

    .line 3468
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->menu_setting:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v5, v6, v9, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->menu_setting_icon:I

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 3473
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->isHelpApplicationExist()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3474
    const/16 v2, 0x13

    const/4 v3, 0x6

    sget v4, Lcom/vlingo/midas/R$string;->help:I

    invoke-interface {p1, v5, v2, v3, v4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->tw_action_bar_icon_help_holo_light:I

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 3478
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 3479
    :cond_5
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 3480
    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->disable_voice_prompt:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v5, v2, v6, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 3481
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 3482
    const/16 v2, 0x14

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3490
    :cond_6
    :goto_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v2

    if-nez v2, :cond_7

    .line 3491
    const/16 v2, 0x16

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->tutorial_preview:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v5, v2, v8, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 3494
    :cond_7
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$bool;->help_fragment_supported:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-nez v2, :cond_8

    .line 3495
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->example_commands:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v5, v7, v2, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 3496
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    .line 3497
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3506
    :cond_8
    :goto_2
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSpeakerID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 3510
    :cond_9
    const/4 v1, 0x1

    .line 3511
    goto/16 :goto_0

    .line 3484
    :cond_a
    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->enable_voice_prompt:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v5, v2, v6, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 3485
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 3486
    const/16 v2, 0x15

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 3499
    :cond_b
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method

.method public onRecoCancelled()V
    .locals 2

    .prologue
    .line 4664
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v0, :cond_0

    .line 4667
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-eq v0, v1, :cond_0

    .line 4668
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v0, v1, :cond_1

    .line 4669
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V

    .line 4675
    :cond_0
    :goto_0
    return-void

    .line 4671
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->BUSY:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V

    goto :goto_0
.end method

.method public onRecoToneStarting(Z)J
    .locals 2
    .param p1, "startTone"    # Z

    .prologue
    .line 5629
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->count:I

    .line 5630
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onRecoToneStopped(Z)V
    .locals 4
    .param p1, "startTone"    # Z

    .prologue
    .line 5749
    const-string/jumbo v1, "ConversationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "[LatencyCheck] onRecoToneStopped("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5750
    const/4 v0, 0x0

    .line 5751
    .local v0, "controlState":Lcom/vlingo/midas/gui/ControlFragment$ControlState;
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    .line 5752
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v0

    .line 5755
    :cond_0
    if-eqz p1, :cond_4

    .line 5756
    if-eqz v0, :cond_1

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-eq v0, v1, :cond_2

    :cond_1
    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v0, v1, :cond_3

    .line 5758
    :cond_2
    new-instance v1, Lcom/vlingo/midas/gui/ConversationActivity$21;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/ConversationActivity$21;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    const-wide/16 v2, 0x12c

    invoke-static {v1, v2, v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 5780
    :cond_3
    :goto_0
    return-void

    .line 5770
    :cond_4
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_5

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v1, v2, :cond_5

    .line 5771
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    goto :goto_0

    .line 5773
    :cond_5
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_3

    .line 5774
    const-string/jumbo v1, "ConversationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "CurrentState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " is skipped."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 5027
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v0, :cond_0

    .line 5028
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/midas/gui/Widget;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 5032
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;->STOPPED:Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    if-eq p2, v0, :cond_1

    .line 5033
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->clearUpdateType()V

    .line 5034
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/midas/util/SocialUtils;->setIntentAfterTTS(Landroid/content/Intent;)V

    .line 5036
    :cond_1
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 6
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    const/16 v5, 0xb

    const-wide/16 v3, 0x64

    .line 4977
    const/4 v0, 0x1

    .line 4978
    .local v0, "clearUpdateType":Z
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v1, :cond_0

    .line 4979
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v1, p1}, Lcom/vlingo/midas/gui/Widget;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 4982
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4983
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getUpdateType()Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->WEIBO_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    if-ne v1, v2, :cond_3

    .line 4984
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 5004
    :goto_0
    if-eqz v0, :cond_1

    .line 5005
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->clearUpdateType()V

    .line 5008
    :cond_1
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment;->isStartRecoByEarEvent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5009
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->setNullTask()V

    .line 5010
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessage(I)Z

    .line 5011
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setStartRecoByEarEvent(Z)V

    .line 5013
    :cond_2
    return-void

    .line 4985
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getUpdateType()Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->ALL:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    if-ne v1, v2, :cond_4

    .line 4986
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v1, v5, v3, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 4989
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 4992
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getUpdateType()Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->FACEBOOK_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    if-ne v1, v2, :cond_6

    .line 4993
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 4994
    :cond_6
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getUpdateType()Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->TWITTER_ONLY:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    if-ne v1, v2, :cond_7

    .line 4995
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 4996
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getUpdateType()Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/util/SocialUtils$UpdateType;->ALL:Lcom/vlingo/midas/util/SocialUtils$UpdateType;

    if-ne v1, v2, :cond_8

    .line 4997
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v1, v5, v3, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 5000
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 5017
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v0, :cond_0

    .line 5018
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/midas/gui/Widget;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    .line 5021
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->clearUpdateType()V

    .line 5022
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vlingo/midas/util/SocialUtils;->setIntentAfterTTS(Landroid/content/Intent;)V

    .line 5023
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 4970
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v0, :cond_0

    .line 4971
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/Widget;->onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 4973
    :cond_0
    return-void
.end method

.method protected onRestart()V
    .locals 2

    .prologue
    .line 437
    const-string/jumbo v0, "svoicedebugging"

    const-string/jumbo v1, "Focus onRestart() of Svoice"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 439
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    sget v1, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 440
    :cond_0
    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->smart:Z

    if-eqz v0, :cond_1

    .line 441
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragment;->resetAllContent()V

    .line 442
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->smart:Z

    .line 447
    :cond_1
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onRestart()V

    .line 448
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1720
    const-string/jumbo v0, "svoicedebugging"

    const-string/jumbo v1, "Focus onRestoreInstanceState() of Svoice"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1723
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity;->unpackSavedInstanceState(Landroid/os/Bundle;)V

    .line 1724
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1725
    return-void
.end method

.method public onResultsNoAction()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4622
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->lockInMiniMode:Z

    .line 4623
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->onRecognitionResult(Ljava/lang/String;)V

    .line 4624
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->nluEventLogUtil:Lcom/vlingo/midas/util/log/NLUEventLogUtil;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/util/log/NLUEventLogUtil;->onRecognitionResult(Ljava/lang/String;)V

    .line 4625
    return-void
.end method

.method protected onResume()V
    .locals 25

    .prologue
    .line 1850
    invoke-super/range {p0 .. p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 1851
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->invalidateOptionsMenu()V

    .line 1854
    const-string/jumbo v21, "VerificationLog"

    const-string/jumbo v22, "onResume"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1856
    const-string/jumbo v21, "svoicedebugging"

    const-string/jumbo v22, "Focus onResume() of Svoice"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1862
    sget-object v21, Lcom/vlingo/midas/gui/ConversationActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "appVersion = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    new-instance v23, Lcom/vlingo/midas/Version;

    invoke-direct/range {v23 .. v23}, Lcom/vlingo/midas/Version;-><init>()V

    invoke-virtual/range {v23 .. v23}, Lcom/vlingo/midas/Version;->getVersion()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1864
    sget-boolean v21, Lcom/vlingo/midas/gui/ConversationActivity;->turnScreenOff:Z

    if-nez v21, :cond_0

    .line 1865
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->needToDismissLockScreen()Z

    move-result v21

    sput-boolean v21, Lcom/vlingo/midas/gui/ConversationActivity;->turnScreenOff:Z

    .line 1868
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setOrientationDrivingMode()V

    .line 1870
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/samsungutils/utils/OpenAppNameManager;->registerHandler(Landroid/os/Handler;)V

    .line 1872
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v21

    if-eqz v21, :cond_1

    .line 1873
    invoke-static/range {p0 .. p0}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->setLayoutInflateListener(Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;)V

    .line 1876
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    move-object/from16 v21, v0

    const/16 v22, 0xf

    invoke-virtual/range {v21 .. v22}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 1883
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onUiShown()V

    .line 1884
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/vlingo/midas/ServiceManager;->startLmttVoconService(Z)Z

    .line 1886
    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    .line 1887
    .local v12, "lastSafereaderTime":Ljava/util/Date;
    const-string/jumbo v21, "car_safereader_last_saferead_time"

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v22

    invoke-static/range {v21 .. v23}, Lcom/vlingo/midas/settings/MidasSettings;->setLong(Ljava/lang/String;J)V

    .line 1889
    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/settings/MidasSettings;->setForegroundState(Z)V

    .line 1891
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->setControlFragmentActivityPaused(Z)V

    .line 1892
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 1893
    invoke-static {}, Lcom/vlingo/midas/util/ConfigurationUtility;->generateConfigPairs()Ljava/util/HashMap;

    move-result-object v17

    .line 1894
    .local v17, "userProperties":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->displayFromBackground:Z

    move/from16 v21, v0

    if-nez v21, :cond_1e

    .line 1895
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v21

    new-instance v22, Lcom/vlingo/midas/gui/WidgetBuilder;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    move-object/from16 v3, v17

    move-object/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->initFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 1902
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1f

    .line 1903
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCoverOpened()Z

    move-result v21

    if-eqz v21, :cond_2

    .line 1904
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->unlockLockscreen(Z)V

    .line 1910
    :cond_2
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->launchCountPref:Landroid/content/SharedPreferences;

    .line 1911
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->launchCountPref:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    const-string/jumbo v22, "normal_launch_count"

    const/16 v23, 0x0

    invoke-interface/range {v21 .. v23}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v21

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->normalLaunchCount:I

    .line 1912
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->launchCountPref:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v13

    .line 1913
    .local v13, "launchEditor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v21, "normal_launch_count"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->normalLaunchCount:I

    move/from16 v22, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1914
    invoke-interface {v13}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1922
    invoke-static {}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->needNotifications()Z

    move-result v21

    if-eqz v21, :cond_21

    .line 1925
    const/16 v7, 0x320

    .line 1926
    .local v7, "delay":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->isKModel:Z

    move/from16 v21, v0

    if-eqz v21, :cond_7

    .line 1932
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v21, v0

    if-eqz v21, :cond_3

    .line 1933
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/midas/gui/DialogFragment;->resetScreen()V

    .line 1935
    :cond_3
    sget-object v21, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    if-eqz v21, :cond_4

    .line 1936
    sget-object v21, Lcom/vlingo/midas/gui/ConversationActivity;->titlebar:Landroid/app/ActionBar;

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/settings/MidasSettings;->hideActionBar(Landroid/app/ActionBar;)V

    .line 1941
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    move-object/from16 v21, v0

    if-eqz v21, :cond_5

    .line 1942
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 1944
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    move-object/from16 v21, v0

    if-eqz v21, :cond_6

    .line 1945
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setVisibility(I)V

    .line 1948
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 1949
    .local v5, "action":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->isKeyguardSecure:Z

    move/from16 v21, v0

    if-nez v21, :cond_20

    if-eqz v5, :cond_20

    const-string/jumbo v21, "com.sec.action.SVOICE"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_20

    .line 1950
    const/16 v7, 0x96

    .line 1963
    .end local v5    # "action":Ljava/lang/String;
    :cond_7
    :goto_2
    new-instance v8, Landroid/content/Intent;

    const-class v21, Lcom/vlingo/midas/gui/WelcomePage;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1964
    .local v8, "intent":Landroid/content/Intent;
    new-instance v21, Lcom/vlingo/midas/gui/ConversationActivity$6;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v8}, Lcom/vlingo/midas/gui/ConversationActivity$6;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Landroid/content/Intent;)V

    int-to-long v0, v7

    move-wide/from16 v22, v0

    invoke-static/range {v21 .. v23}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 2012
    .end local v7    # "delay":I
    .end local v8    # "intent":Landroid/content/Intent;
    :cond_8
    :goto_3
    sput-object p0, Lcom/vlingo/midas/gui/ConversationActivity;->smCurrentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

    .line 2014
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 2020
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v21

    if-eqz v21, :cond_9

    sget-object v21, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v21, :cond_9

    .line 2021
    sget-object v21, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/midas/gui/ControlFragment;->setMicStatusText()V

    .line 2027
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    move-object/from16 v21, v0

    if-eqz v21, :cond_a

    .line 2028
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->phoneListener:Lcom/vlingo/core/internal/audio/PhoneListenerImpl;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/audio/PhoneListenerImpl;->addListener(Lcom/vlingo/core/internal/audio/PhoneStateListenerCallback;)V

    .line 2033
    :cond_a
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v10

    .line 2035
    .local v10, "isDrivingMode":Z
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v21

    if-nez v21, :cond_b

    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v21

    if-eqz v21, :cond_f

    .line 2036
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->isFinishing()Z

    move-result v21

    if-nez v21, :cond_f

    .line 2043
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    move/from16 v21, v0

    if-eqz v21, :cond_f

    .line 2044
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v9

    .line 2045
    .local v9, "isBtHeadsetConnected":Z
    if-eqz v9, :cond_c

    if-eqz v10, :cond_d

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->startRecoBySCO:Z

    move/from16 v21, v0

    if-eqz v21, :cond_f

    .line 2046
    :cond_d
    new-instance v15, Lcom/vlingo/midas/gui/ConversationActivity$7;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/vlingo/midas/gui/ConversationActivity$7;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    .line 2075
    .local v15, "r":Ljava/lang/Runnable;
    sget-object v21, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v21, :cond_f

    .line 2076
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->startRecoBySCO:Z

    move/from16 v21, v0

    if-eqz v21, :cond_e

    .line 2077
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v21

    const/16 v22, 0x6

    const/16 v23, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v15}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->setTaskOnGetAudioFocus(IILjava/lang/Runnable;)V

    .line 2084
    :cond_e
    sget-object v21, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/vlingo/midas/gui/ControlFragment;->setTaskOnFinishGreetingTTS(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2094
    .end local v9    # "isBtHeadsetConnected":Z
    .end local v15    # "r":Ljava/lang/Runnable;
    :cond_f
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 2095
    .restart local v8    # "intent":Landroid/content/Intent;
    const-string/jumbo v21, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_10

    const-string/jumbo v21, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_12

    .line 2097
    :cond_10
    const-string/jumbo v21, "isSecure"

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    .line 2098
    .local v11, "isSecure":Z
    if-nez v11, :cond_11

    .line 2099
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v21

    const/high16 v22, 0x400000

    invoke-virtual/range {v21 .. v22}, Landroid/view/Window;->addFlags(I)V

    .line 2102
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->doDismissKeyguard()V

    .line 2104
    :cond_11
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/vlingo/midas/gui/ConversationActivity;->acquireWakeLock(Landroid/content/Intent;)V

    .line 2106
    .end local v11    # "isSecure":Z
    :cond_12
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/AudioPlayerProxy;->addListener(Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;)V

    .line 2111
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->sendPendingEvents()V

    .line 2119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 2121
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v21

    move-object/from16 v0, v21

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_22

    .line 2122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    move-object/from16 v21, v0

    if-eqz v21, :cond_13

    .line 2123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/vlingo/midas/gui/RegularControlFragment;->setCallback(Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;)V

    .line 2129
    :cond_13
    :goto_5
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v21

    if-nez v21, :cond_14

    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v21

    if-eqz v21, :cond_16

    .line 2130
    :cond_14
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getSvoiceLaunchedFromOutside()Z

    move-result v21

    if-eqz v21, :cond_15

    .line 2136
    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/settings/MidasSettings;->setSvoiceLaunchedFromOutside(Z)V

    .line 2137
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->finish()V

    .line 2140
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->handleTextReqeust:Z

    move/from16 v21, v0

    if-eqz v21, :cond_23

    .line 2141
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->handleTextRequestIntent(Landroid/content/Intent;)V

    .line 2186
    :cond_16
    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->checkFragmentLanguage()V

    .line 2188
    sget v21, Lcom/vlingo/midas/R$id;->mainControlLL:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/LinearLayout;

    .line 2195
    .local v14, "mainControlLL":Landroid/widget/LinearLayout;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v21

    if-eqz v21, :cond_17

    .line 2196
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setMicAnimation()V

    .line 2198
    :cond_17
    if-eqz v14, :cond_18

    .line 2199
    invoke-virtual {v14}, Landroid/widget/LinearLayout;->invalidate()V

    .line 2211
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->messages:Ljava/util/LinkedList;

    move-object/from16 v21, v0

    if-eqz v21, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->messages:Ljava/util/LinkedList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_19

    .line 2212
    new-instance v6, Ljava/util/LinkedList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->messages:Ljava/util/LinkedList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v6, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 2213
    .local v6, "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->messages:Ljava/util/LinkedList;

    .line 2214
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->handleAlert(Ljava/util/LinkedList;)V

    .line 2217
    .end local v6    # "alerts":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/vlingo/core/internal/safereader/SafeReaderAlert;>;"
    :cond_19
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplication()Landroid/app/Application;

    move-result-object v21

    check-cast v21, Lcom/vlingo/midas/VlingoApplication;

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/vlingo/midas/VlingoApplication;->setSocialLogin(Z)V

    .line 2237
    sget-boolean v21, Lcom/vlingo/midas/gui/ConversationActivity;->doLaunchTutorial:Z

    if-eqz v21, :cond_27

    .line 2238
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->startTutorial()V

    .line 2251
    :cond_1a
    :goto_7
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->setActionBarTalkback()V

    .line 2252
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v21

    if-eqz v21, :cond_28

    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialOn()Z

    move-result v21

    if-eqz v21, :cond_28

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_28

    .line 2253
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v21

    const/16 v22, 0x10

    invoke-virtual/range {v21 .. v22}, Landroid/view/Window;->addFlags(I)V

    .line 2261
    :cond_1b
    :goto_8
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v21

    if-eqz v21, :cond_1c

    .line 2264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 2265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    move-object/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v22

    const/16 v23, 0x1

    invoke-virtual/range {v21 .. v23}, Lcom/samsung/android/sdk/cover/ScoverManager;->setCoverModeToWindow(Landroid/view/Window;I)V

    .line 2267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    .line 2268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    move-object/from16 v21, v0

    if-eqz v21, :cond_29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v21

    if-nez v21, :cond_29

    .line 2270
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/settings/MidasSettings;->hideActionBar(Landroid/app/ActionBar;)V

    .line 2271
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v21

    if-eqz v21, :cond_1c

    .line 2272
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/app/ActionBar;->hide()V

    .line 2285
    :cond_1c
    :goto_9
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v21

    if-eqz v21, :cond_1d

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v21

    if-nez v21, :cond_1d

    .line 2288
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v20

    .line 2289
    .local v20, "window":Landroid/view/Window;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const-string/jumbo v22, "action_bar"

    const-string/jumbo v23, "id"

    const-string/jumbo v24, "android"

    invoke-virtual/range {v21 .. v24}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    .line 2290
    .local v16, "resId":I
    invoke-virtual/range {v20 .. v20}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/view/ViewGroup;

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup;

    .line 2291
    .local v18, "v":Landroid/view/ViewGroup;
    if-eqz v18, :cond_1d

    .line 2292
    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    .line 2293
    .local v19, "v1":Landroid/view/View;
    if-eqz v19, :cond_1d

    .line 2294
    check-cast v19, Landroid/widget/TextView;

    .end local v19    # "v1":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    sget v22, Lcom/vlingo/midas/R$string;->more_btn:I

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2300
    .end local v16    # "resId":I
    .end local v18    # "v":Landroid/view/ViewGroup;
    .end local v20    # "window":Landroid/view/Window;
    :cond_1d
    :goto_a
    const-string/jumbo v21, "VerificationLog"

    const-string/jumbo v22, "Executed"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2301
    return-void

    .line 1898
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v10    # "isDrivingMode":Z
    .end local v13    # "launchEditor":Landroid/content/SharedPreferences$Editor;
    .end local v14    # "mainControlLL":Landroid/widget/LinearLayout;
    :cond_1e
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v21

    new-instance v22, Lcom/vlingo/midas/gui/WidgetBuilder;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    move-object/from16 v3, v17

    move-object/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->stealFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    goto/16 :goto_0

    .line 1907
    :cond_1f
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->stopVoiceWakeup()V

    goto/16 :goto_1

    .line 1952
    .restart local v5    # "action":Ljava/lang/String;
    .restart local v7    # "delay":I
    .restart local v13    # "launchEditor":Landroid/content/SharedPreferences$Editor;
    :cond_20
    const/16 v7, 0x1e

    goto/16 :goto_2

    .line 2006
    .end local v5    # "action":Ljava/lang/String;
    .end local v7    # "delay":I
    :cond_21
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->languageSupportListCheck()V

    .line 2008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    move-object/from16 v21, v0

    if-eqz v21, :cond_8

    .line 2009
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 2124
    .restart local v8    # "intent":Landroid/content/Intent;
    .restart local v10    # "isDrivingMode":Z
    :cond_22
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v21

    move-object/from16 v0, v21

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_13

    .line 2125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    move-object/from16 v21, v0

    if-eqz v21, :cond_13

    .line 2126
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->setCallback(Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;)V

    goto/16 :goto_5

    .line 2142
    :cond_23
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    move/from16 v21, v0

    if-eqz v21, :cond_26

    .line 2143
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->startRecoBySCO:Z

    move/from16 v21, v0

    if-nez v21, :cond_16

    .line 2144
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v9

    .line 2145
    .restart local v9    # "isBtHeadsetConnected":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->audioManager:Landroid/media/AudioManager;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v21

    if-nez v21, :cond_24

    if-eqz v9, :cond_24

    if-nez v10, :cond_24

    .line 2151
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v21

    const/16 v22, 0x6

    const/16 v23, 0x2

    new-instance v24, Lcom/vlingo/midas/gui/ConversationActivity$8;

    invoke-direct/range {v24 .. v25}, Lcom/vlingo/midas/gui/ConversationActivity$8;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual/range {v21 .. v24}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->setTaskOnGetAudioFocus(IILjava/lang/Runnable;)V

    goto/16 :goto_6

    .line 2169
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    invoke-virtual/range {v21 .. v22}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2170
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v21

    if-nez v21, :cond_16

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v21

    if-eqz v21, :cond_25

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mAutoListeningmodeAlways:Z

    move/from16 v21, v0

    if-eqz v21, :cond_16

    :cond_25
    invoke-static {}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->showingNotifications()Z

    move-result v21

    if-nez v21, :cond_16

    .line 2173
    sget-object v21, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v21, :cond_16

    sget-object v21, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/midas/gui/ControlFragment;->isStartRecoByEarEvent()Z

    move-result v21

    if-nez v21, :cond_16

    .line 2174
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->setNullTask()V

    .line 2175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    invoke-virtual/range {v21 .. v22}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_6

    .line 2180
    .end local v9    # "isBtHeadsetConnected":Z
    :cond_26
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->displayFromBackground:Z

    move/from16 v21, v0

    if-eqz v21, :cond_16

    .line 2181
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->resumeFromBackground:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->onResumeFromBackground(Z)V

    goto/16 :goto_6

    .line 2243
    .restart local v14    # "mainControlLL":Landroid/widget/LinearLayout;
    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1a

    .line 2244
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/vlingo/midas/gui/RegularControlFragment;->removeMicStatusTextForTutorial(Z)V

    goto/16 :goto_7

    .line 2255
    :cond_28
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v21

    const/16 v22, 0x10

    invoke-virtual/range {v21 .. v22}, Landroid/view/Window;->clearFlags(I)V

    .line 2256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1b

    .line 2257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/midas/gui/RegularControlFragment;->removeDelegateFromLastScreen()V

    goto/16 :goto_8

    .line 2274
    :cond_29
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v21

    if-eqz v21, :cond_2a

    .line 2275
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/settings/MidasSettings;->setActionBarTransparent(Landroid/app/ActionBar;)V

    goto/16 :goto_9

    .line 2277
    :cond_2a
    sget-boolean v21, Lcom/vlingo/midas/gui/ConversationActivity;->doLaunchTutorial:Z

    if-nez v21, :cond_1c

    .line 2278
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->switchToFullScreen()V

    .line 2279
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/settings/MidasSettings;->showActionBar(Landroid/app/ActionBar;)V

    goto/16 :goto_9

    .line 2296
    :catch_0
    move-exception v21

    goto/16 :goto_a

    .line 2088
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v14    # "mainControlLL":Landroid/widget/LinearLayout;
    :catch_1
    move-exception v21

    goto/16 :goto_4
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1709
    const-string/jumbo v0, "svoicedebugging"

    const-string/jumbo v1, "Focus onSaveInstanceState() of Svoice"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1713
    const-string/jumbo v0, "autoStartOnResume"

    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1714
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1715
    return-void
.end method

.method public onScoConnected()V
    .locals 2

    .prologue
    .line 5342
    const-string/jumbo v0, "ConversationActivity"

    const-string/jumbo v1, "[LatencyCheck] onScoConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5345
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->setVolumeControlStream(I)V

    .line 5346
    return-void
.end method

.method public onScoDisconnected()V
    .locals 2

    .prologue
    .line 5350
    const-string/jumbo v0, "ConversationActivity"

    const-string/jumbo v1, "[LatencyCheck] onScoDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5353
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->setVolumeControlStream(I)V

    .line 5354
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->disconnectScoByIdle()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5355
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    .line 5356
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 5357
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 5359
    :cond_0
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 4898
    if-nez p2, :cond_1

    .line 4904
    :cond_0
    :goto_0
    return-void

    .line 4901
    :cond_1
    const-string/jumbo v0, "language"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4902
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->resetAllContent()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2590
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2591
    sget v0, Lcom/vlingo/midas/R$anim;->activity_transition_anim:I

    sget v1, Lcom/vlingo/midas/R$anim;->activity_transition_anim:I

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->overridePendingTransition(II)V

    .line 2597
    :cond_0
    const-string/jumbo v0, "svoicedebugging"

    const-string/jumbo v1, "super.onStart() in onStart() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2598
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onStart()V

    .line 2599
    const-string/jumbo v0, "svoicedebugging"

    const-string/jumbo v1, "super.onStart() in onStart() is finished"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2603
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/vlingo/midas/uifocus/UiFocusActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v0, v1}, Lcom/vlingo/midas/uifocus/UiFocusUtil;->onForegrounded(Landroid/content/Context;ZLandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2604
    iput-boolean v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z

    .line 2608
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/VlingoApplication;->setIsInForeground(Z)V

    .line 2609
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2611
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 2614
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/vlingo/midas/ServiceManager;->startSafeReaderService(Z)Z

    .line 2618
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "seamless_pause"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2620
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2623
    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity$9;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ConversationActivity$9;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity$9;->start()V

    .line 2637
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;Z)V
    .locals 8
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    const/16 v3, 0x14

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 5475
    const/4 v0, 0x0

    .line 5477
    .local v0, "doCommit":Z
    sget-object v2, Lcom/vlingo/midas/MidasADMController;->AGGRESSIVE_NOISE_CANCELLATION:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5478
    if-eqz p2, :cond_0

    .line 5571
    :cond_0
    :goto_0
    return-void

    .line 5487
    :cond_1
    sget-object v2, Lcom/vlingo/midas/MidasADMController;->ENDPOINT_DETECTION:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5488
    if-eqz p2, :cond_0

    goto :goto_0

    .line 5497
    :cond_2
    sget-object v2, Lcom/vlingo/midas/MidasADMController;->EYES_FREE:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5498
    if-eqz p2, :cond_3

    .line 5501
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    const-string/jumbo v3, "isEyesFree"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeUserProperties(Ljava/lang/String;)V

    .line 5502
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    const-string/jumbo v3, "isEyesFree"

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->addUserProperties(Ljava/lang/String;Ljava/lang/String;)V

    .line 5504
    const-string/jumbo v2, "is_eyes_free_mode"

    invoke-static {v2, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 5509
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    const-string/jumbo v3, "isEyesFree"

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeUserProperties(Ljava/lang/String;)V

    .line 5510
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    const-string/jumbo v3, "isEyesFree"

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->addUserProperties(Ljava/lang/String;Ljava/lang/String;)V

    .line 5512
    const-string/jumbo v2, "is_eyes_free_mode"

    invoke-static {v2, v6}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 5515
    :cond_4
    sget-object v2, Lcom/vlingo/midas/MidasADMController;->DRIVING_MODE_GUI:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 5516
    if-eqz p2, :cond_6

    .line 5519
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "supported_bvra"

    invoke-static {v2, v3, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_5

    .line 5522
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->bvra_unsupported_toast:I

    invoke-static {v2, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 5525
    :cond_5
    const-string/jumbo v2, "widget_display_max"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 5526
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    goto/16 :goto_0

    .line 5529
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "supported_bvra"

    invoke-static {v2, v3, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_7

    .line 5532
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->bvra_unsupported_toast:I

    invoke-static {v2, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 5534
    .local v1, "toast":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 5540
    .end local v1    # "toast":Landroid/widget/Toast;
    :cond_7
    const-string/jumbo v2, "widget_display_max"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 5541
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    goto/16 :goto_0

    .line 5544
    :cond_8
    sget-object v2, Lcom/vlingo/midas/MidasADMController;->TALKBACK:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5545
    if-eqz p2, :cond_a

    .line 5548
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_9

    .line 5549
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v6}, Lcom/vlingo/midas/gui/ControlFragment;->onPromptOnClick(Z)V

    .line 5551
    :cond_9
    const-string/jumbo v2, "Listen"

    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->unregisterHandler(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5553
    :cond_a
    const-string/jumbo v2, "voice_prompt_preference_value"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v3, :cond_c

    .line 5556
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_b

    .line 5557
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v6}, Lcom/vlingo/midas/gui/ControlFragment;->onPromptOffClick(Z)V

    .line 5559
    :cond_b
    const-string/jumbo v2, "Listen"

    const-class v3, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ListenHandler;

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Ljava/lang/String;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 5563
    :cond_c
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_d

    .line 5564
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v6}, Lcom/vlingo/midas/gui/ControlFragment;->onPromptOnClick(Z)V

    .line 5566
    :cond_d
    const-string/jumbo v2, "Listen"

    invoke-static {v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->unregisterHandler(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2647
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    invoke-static {v0}, Lcom/vlingo/midas/uifocus/UiFocusUtil;->onBackgrounded(Z)V

    .line 2648
    const-string/jumbo v0, "svoicedebugging"

    const-string/jumbo v1, "Focus onStop() of Svoice"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2653
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "seamless_resume"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2654
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->notificationManager:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    if-eqz v0, :cond_0

    .line 2655
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->notificationManager:Lcom/vlingo/midas/notification/NotificationPopUpManager;

    invoke-virtual {v0}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->dismissNotifications()V

    .line 2658
    :cond_0
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onStop()V

    .line 2659
    invoke-static {}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->getInstance()Lcom/vlingo/core/internal/audio/RemoteControlManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/RemoteControlManager;->destroy()V

    .line 2663
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/VlingoApplication;->setIsInForeground(Z)V

    .line 2664
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2665
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 2668
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/ServiceManager;->stopSafereaderService(Z)V

    .line 2671
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v0, :cond_2

    .line 2672
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->cancelTTS()V

    .line 2674
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/4 v1, 0x3

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 2675
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v0, :cond_3

    .line 2677
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/Widget;->onStop()V

    .line 2680
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2681
    invoke-static {}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->removeLayoutInflateListener()V

    .line 2684
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->currentTablet:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/ConversationActivity$TabletType;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    if-eqz v0, :cond_5

    .line 2685
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    .line 2688
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2689
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->disableTransculentTheme()V

    .line 2691
    :cond_6
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 3519
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->gestures:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    .line 3520
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->gestures:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 3522
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 2836
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onTrimMemory(I)V

    .line 2837
    const/16 v0, 0xf

    if-ne p1, v0, :cond_1

    .line 2838
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    if-eqz v0, :cond_0

    .line 2839
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragment;->cleanupPreviousBubblesForOnTrimMemory()V

    .line 2840
    :cond_0
    const-string/jumbo v0, "onTrimMemory = "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ConversationActivity onTrimMemory = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2842
    :cond_1
    return-void
.end method

.method public onUserInteraction()V
    .locals 3

    .prologue
    .line 4690
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onUserInteraction()V

    .line 4691
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 4693
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4694
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment;->removeMessages()V

    .line 4700
    :cond_0
    :goto_0
    return-void

    .line 4696
    :cond_1
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4698
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment;->resetFinishMessage()V

    goto :goto_0
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 2820
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->onUserLeaveHint()V

    .line 2821
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onUserLeaveHint()V

    .line 2822
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 5
    .param p1, "hasFocus"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 5226
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->hasFocus:Z

    .line 5227
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onWindowFocusChanged(Z)V

    .line 5228
    const/4 v0, 0x0

    .line 5233
    .local v0, "delay":I
    if-eqz p1, :cond_1

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v1, v2, :cond_1

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "key_social_login_attemp_for_user_leave_hint"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5242
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 5244
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isDrivingMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5253
    const/16 v0, 0x5dc

    .line 5257
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 5258
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    int-to-long v2, v0

    invoke-virtual {v1, v4, v2, v3}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 5267
    :goto_1
    return-void

    .line 5255
    :cond_0
    const/16 v0, 0x2bc

    goto :goto_0

    .line 5262
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 5263
    const-string/jumbo v1, "key_social_login_attemp_for_user_leave_hint"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public onWindowStatusChanged(ZZZ)V
    .locals 4
    .param p1, "isMaximized"    # Z
    .param p2, "isMinimized"    # Z
    .param p3, "isPinup"    # Z

    .prologue
    .line 5217
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 5218
    .local v0, "str":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " / isMaximized : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5219
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " / isMinimized : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5220
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " / isPinup : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5221
    const-string/jumbo v1, "Hello"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onWindowStatusChanged is called : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5222
    return-void
.end method

.method public promptUser(Ljava/lang/String;)V
    .locals 2
    .param p1, "_msg"    # Ljava/lang/String;

    .prologue
    .line 4204
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4208
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 4210
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragment;->announceTTS(Ljava/lang/String;)V

    .line 4211
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity;->showVlingoText(Ljava/lang/String;)V

    .line 4212
    return-void
.end method

.method public requestLocationUpdate()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    .line 633
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 635
    .local v7, "context":Landroid/content/Context;
    if-eqz v7, :cond_0

    .line 636
    const-string/jumbo v1, "location"

    invoke-virtual {v7, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 638
    .local v0, "lm":Landroid/location/LocationManager;
    const-string/jumbo v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v6

    .line 640
    .local v6, "bResult":Z
    const/4 v1, 0x1

    if-ne v6, v1, :cond_0

    .line 643
    const-string/jumbo v1, "network"

    const-wide/16 v2, 0x64

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 647
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v1, v8}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 648
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v8, v2, v3}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 652
    .end local v0    # "lm":Landroid/location/LocationManager;
    .end local v6    # "bResult":Z
    :cond_0
    return-void
.end method

.method public setControlFragmentActivityPaused(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 5618
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    if-eqz v0, :cond_0

    .line 5619
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->setActivityPaused(Z)V

    .line 5621
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    if-eqz v0, :cond_1

    .line 5622
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/RegularControlFragment;->setActivityPaused(Z)V

    .line 5624
    :cond_1
    return-void
.end method

.method public setDoLaunchTutorial(Z)V
    .locals 0
    .param p1, "doLaunch"    # Z

    .prologue
    .line 5888
    sput-boolean p1, Lcom/vlingo/midas/gui/ConversationActivity;->doLaunchTutorial:Z

    .line 5889
    return-void
.end method

.method protected setOrientationDrivingMode()V
    .locals 2

    .prologue
    .line 2389
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    .line 2390
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v0

    .line 2394
    .local v0, "state":Lcom/vlingo/midas/gui/ControlFragment$ControlState;
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->changeToDriveMode(Z)V

    .line 2395
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 2399
    .end local v0    # "state":Lcom/vlingo/midas/gui/ControlFragment$ControlState;
    :cond_0
    return-void
.end method

.method public showControlFragment()V
    .locals 3

    .prologue
    .line 5428
    :try_start_0
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    .line 5429
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5433
    :cond_0
    :goto_0
    return-void

    .line 5430
    :catch_0
    move-exception v0

    .line 5431
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V
    .locals 3
    .param p1, "newState"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    .prologue
    const/16 v2, 0x80

    .line 4474
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity$25;->$SwitchMap$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 4504
    :goto_0
    return-void

    .line 4476
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v0, :cond_0

    .line 4477
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 4483
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v0, :cond_1

    .line 4484
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 4486
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v0, :cond_2

    .line 4489
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->onIdle()V

    .line 4490
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 4492
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->stopSeamlessVoiceWakeup()V

    goto :goto_0

    .line 4497
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 4498
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v0, :cond_3

    .line 4499
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->DIALOG_BUSY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 4501
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0

    .line 4474
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
    .locals 6
    .param p1, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4220
    sget-boolean v2, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    if-eqz v2, :cond_0

    .line 4221
    const/4 v1, 0x0

    .line 4222
    .local v1, "obj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4223
    invoke-static {p0, v3}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    move-result-object v1

    .line 4228
    :goto_0
    invoke-virtual {v1, v5}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->setTutorialCompleted(Z)V

    .line 4229
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/ConversationActivity;->setDoLaunchTutorial(Z)V

    .line 4230
    invoke-virtual {p0, p0}, Lcom/vlingo/midas/gui/ConversationActivity;->finishTutorial(Landroid/content/Context;)V

    .line 4251
    .end local v1    # "obj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :cond_0
    invoke-static {p1, p0}, Lcom/vlingo/midas/util/ErrorCodeUtils;->getLocalizedMessageForErrorCode(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 4258
    .local v0, "localizedMessage":Ljava/lang/String;
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_1

    invoke-static {p1}, Lcom/vlingo/midas/gui/ConversationActivity;->shouldReportError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4259
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    sget-object v3, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->ERROR:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {v2, v3, v0, v5, v4}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    .line 4261
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v0}, Lcom/vlingo/midas/gui/ControlFragment;->announceTTS(Ljava/lang/String;)V

    .line 4263
    :cond_1
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-nez v2, :cond_2

    .line 4264
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->initFragments(Z)V

    .line 4266
    :cond_2
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_3

    .line 4267
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 4268
    :cond_3
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-virtual {v2}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->onRecognitionError()V

    .line 4269
    return-void

    .line 4225
    .end local v0    # "localizedMessage":Ljava/lang/String;
    .restart local v1    # "obj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :cond_4
    invoke-static {p0, v3}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    move-result-object v1

    goto :goto_0
.end method

.method public showHelp()V
    .locals 2

    .prologue
    .line 3637
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/help/HelpScreen;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3638
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    .line 3639
    return-void
.end method

.method showPreferences()V
    .locals 3

    .prologue
    .line 1663
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1664
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "is_start_option_menu"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1665
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    .line 1666
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1667
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->finish()V

    .line 1669
    :cond_0
    return-void
.end method

.method public showRMSChange(I)V
    .locals 2
    .param p1, "rmsValue"    # I

    .prologue
    .line 4314
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4317
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragment;->showRMSChange(I)V

    .line 4347
    :cond_0
    :goto_0
    return-void

    .line 4319
    :cond_1
    iget v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->count:I

    .line 4338
    iget v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->count:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v0, :cond_0

    .line 4339
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 4340
    const/16 v0, 0x41

    if-lt p1, v0, :cond_2

    .line 4341
    const/16 p1, 0x58

    .line 4344
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->update(I)V

    goto :goto_0
.end method

.method public showReceivedResults(Lcom/vlingo/core/internal/logging/EventLog;)V
    .locals 2
    .param p1, "results"    # Lcom/vlingo/core/internal/logging/EventLog;

    .prologue
    .line 4608
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v0, :cond_0

    .line 4609
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 4611
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v0, :cond_1

    .line 4614
    :cond_1
    if-eqz p1, :cond_2

    .line 4615
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/logging/EventLog;->getAsrLogMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->onRecognitionResult(Ljava/lang/String;)V

    .line 4616
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->nluEventLogUtil:Lcom/vlingo/midas/util/log/NLUEventLogUtil;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/logging/EventLog;->getNluLogMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/util/log/NLUEventLogUtil;->onRecognitionResult(Ljava/lang/String;)V

    .line 4618
    :cond_2
    return-void
.end method

.method public showRecoStateChange(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 5
    .param p1, "newState"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 4416
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity$25;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 4470
    :cond_0
    :goto_0
    return-void

    .line 4418
    :pswitch_0
    sget-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    if-eqz v1, :cond_1

    .line 4420
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4421
    invoke-static {p0, v4}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    move-result-object v0

    .line 4425
    .local v0, "obj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :goto_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->setTutorialCompleted(Z)V

    .line 4426
    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->setDoLaunchTutorial(Z)V

    .line 4427
    invoke-virtual {p0, p0}, Lcom/vlingo/midas/gui/ConversationActivity;->finishTutorial(Landroid/content/Context;)V

    .line 4432
    .end local v0    # "obj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 4433
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_2

    .line 4434
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 4436
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_3

    .line 4439
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-virtual {v1}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->onRecognitionStart()V

    .line 4441
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    if-eqz v1, :cond_0

    .line 4442
    sput-boolean v3, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->startCheckingListeningState:Z

    goto :goto_0

    .line 4423
    :cond_4
    invoke-static {p0, v4}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    move-result-object v0

    .restart local v0    # "obj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    goto :goto_1

    .line 4446
    .end local v0    # "obj":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :pswitch_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v1, :cond_5

    .line 4447
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 4454
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-virtual {v1}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->onRecognitionComplete()V

    .line 4456
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->isDrivingMode()Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    if-eq v1, v2, :cond_6

    .line 4461
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->startThinkingAnimation()V

    .line 4465
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->stopSeamlessVoiceWakeup()V

    goto :goto_0

    .line 4416
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected showSettings()V
    .locals 0

    .prologue
    .line 3633
    return-void
.end method

.method public showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V
    .locals 6
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "nbest"    # Lcom/vlingo/sdk/recognition/NBestData;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4547
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->retireLastWidget()V

    .line 4548
    const-string/jumbo v1, "last_asr_result"

    invoke-static {v1, p1}, Lcom/vlingo/midas/settings/MidasSettings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4550
    const-string/jumbo v1, "getFieldID"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "conversationactivity-showUserText "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4553
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    sget-object v2, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {v1, v2, p1, v5, v4}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    move-result-object v0

    .line 4555
    .local v0, "db":Lcom/vlingo/midas/gui/DialogBubble;
    if-eqz v0, :cond_0

    .line 4556
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getPreservedState()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogBubble;->setTag(Ljava/lang/Object;)V

    .line 4558
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4559
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    iput-boolean v4, v1, Lcom/vlingo/midas/gui/DialogFragment;->isYouCanSay:Z

    .line 4560
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    iput v5, v1, Lcom/vlingo/midas/gui/DialogFragment;->tutorialShadow:I

    .line 4562
    :cond_1
    return-void
.end method

.method public showVlingoText(Ljava/lang/String;)V
    .locals 7
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 4566
    move-object v1, p1

    .line 4575
    .local v1, "textMini":Ljava/lang/String;
    const-string/jumbo v2, "getFieldID"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "conversationactivity-showVlingoText  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4579
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->isDrivingMode()Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    move-result-object v2

    sget-object v3, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    if-ne v2, v3, :cond_1

    .line 4599
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4600
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v2, v6}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 4601
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v2, v6}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessage(I)Z

    .line 4602
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/vlingo/midas/gui/DialogFragment;->setShadowWhileNotAnim:Z

    .line 4604
    :cond_0
    return-void

    .line 4583
    :cond_1
    const/4 v0, 0x0

    .line 4584
    .local v0, "delay":I
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4585
    const/16 v0, 0x3e8

    .line 4586
    :cond_2
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/vlingo/midas/gui/ConversationActivity$18;

    invoke-direct {v3, p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$18;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;Ljava/lang/String;)V

    int-to-long v4, v0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public showWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;
    .locals 5
    .param p1, "warning"    # Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4283
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;->WARNING_NOTHING_RECOGNIZED:Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;

    if-ne p1, v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4284
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    sget-object v2, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WARNING:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {v1, v2, p2, v4, v3}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    .line 4286
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->reco_warning_nothing_recognized:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4287
    .local v0, "clientString":Ljava/lang/String;
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/ControlFragment;->announceTTS(Ljava/lang/String;)V

    .line 4288
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    .line 4289
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 4290
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;->Reprompted:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;

    .line 4297
    .end local v0    # "clientString":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 4291
    :cond_0
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;->WARNING_NOTHING_RECOGNIZED:Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;

    if-eq p1, v1, :cond_1

    .line 4292
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    sget-object v2, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WARNING:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {v1, v2, p2, v4, v3}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    .line 4294
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1, p2}, Lcom/vlingo/midas/gui/ControlFragment;->announceTTS(Ljava/lang/String;)V

    .line 4295
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;->ShowedWarning:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;

    goto :goto_0

    .line 4297
    :cond_1
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;->Noop:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2911
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/ui/HomeKeyListener;->startActivity(Landroid/content/Intent;)V

    .line 2912
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->startActivity(Landroid/content/Intent;)V

    .line 2913
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 2919
    const/16 v0, 0xd

    if-eq p2, v0, :cond_0

    .line 2920
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/midas/ui/HomeKeyListener;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2923
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/ui/VLActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2928
    :goto_0
    return-void

    .line 2925
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;I)V
    .locals 1
    .param p1, "child"    # Landroid/app/Activity;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "requestCode"    # I

    .prologue
    .line 2942
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/midas/ui/HomeKeyListener;->startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;I)V

    .line 2943
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/midas/ui/VLActivity;->startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2946
    :goto_0
    return-void

    .line 2944
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V
    .locals 1
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "requestCode"    # I

    .prologue
    .line 2962
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/midas/ui/HomeKeyListener;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V

    .line 2964
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/midas/ui/VLActivity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2967
    :goto_0
    return-void

    .line 2965
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public startActivityIfNeeded(Landroid/content/Intent;I)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 2951
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/midas/ui/HomeKeyListener;->startActivityIfNeeded(Landroid/content/Intent;I)V

    .line 2952
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/ui/VLActivity;->startActivityIfNeeded(Landroid/content/Intent;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2955
    :goto_0
    return v0

    .line 2953
    :catch_0
    move-exception v0

    .line 2955
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startThinkingAnimation()V
    .locals 4

    .prologue
    const/16 v3, 0xd

    .line 3935
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    if-eqz v0, :cond_0

    .line 3936
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/vlingo/midas/gui/DialogFragment;->setShadowWhileNotAnim:Z

    .line 3939
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3940
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    if-eqz v0, :cond_1

    .line 3941
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    const/4 v1, 0x1

    iput v1, v0, Lcom/vlingo/midas/gui/DialogFragment;->shouldContinueBelow:I

    .line 3942
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragment;->hideShadowBottom()V

    .line 3946
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v0, v3}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 3947
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v3, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 3948
    return-void
.end method

.method public startVoiceWakeup()V
    .locals 2

    .prologue
    .line 4851
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSeamlessWakeupStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4853
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 4855
    sget-boolean v0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->callFromSVoice:Z

    if-nez v0, :cond_0

    .line 4856
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.vlingo.midas.ACTION_VOICEWAKEUP_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 4860
    :cond_0
    return-void
.end method

.method public stopSeamlessVoiceWakeup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4832
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->audioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    if-eqz v0, :cond_0

    .line 4834
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v1, "seamless_voice=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 4835
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, seamless_voice=off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4836
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    .line 4838
    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->setSeamlessWakeupStarted(Z)V

    .line 4840
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v1, "voice_wakeup_mic=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 4841
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_wakeup_mic=off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4843
    sput-boolean v2, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    .line 4845
    :cond_0
    return-void
.end method

.method public stopThinkingAnimation()V
    .locals 4

    .prologue
    const/16 v3, 0xd

    .line 3951
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v0, v1, :cond_0

    .line 3952
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v0, v3}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 3953
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const-wide/16 v1, 0x28

    invoke-virtual {v0, v3, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 3957
    :cond_0
    return-void
.end method

.method public stopVoiceWakeup()V
    .locals 5

    .prologue
    const/16 v4, 0x11

    .line 4866
    const-string/jumbo v1, "keyguard"

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 4867
    .local v0, "keyguardManager":Landroid/app/KeyguardManager;
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mAlwaysOnMicDetected:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSeamlessWakeupStarted()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4870
    sget-boolean v1, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    if-eqz v1, :cond_1

    .line 4871
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->stopVoiceWakeupParam()V

    .line 4878
    :cond_0
    :goto_0
    return-void

    .line 4873
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 4874
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v4, v2, v3}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public switchToFullScreen()V
    .locals 7

    .prologue
    const/16 v6, 0x400

    const/4 v4, -0x1

    const/4 v5, 0x0

    .line 6048
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 6049
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x200

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 6050
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 6053
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->display:Landroid/view/Display;

    .line 6054
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->display:Landroid/view/Display;

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->screenHeight:I

    .line 6056
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 6057
    .local v0, "currentWindow":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 6059
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 6060
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 6061
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 6064
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->transparent:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 6065
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->controlContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->transparent:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 6066
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 6067
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 6069
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_talk_bg_tile:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 6070
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_talk_bg_tile:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 6071
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 6072
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_1

    .line 6073
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->removeMessages()V

    .line 6076
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5, v5, v5, v5}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 6077
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v2, v5, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 6078
    invoke-static {v5}, Lcom/vlingo/midas/util/MiniModeUtils;->setSVoiceMiniMode(Z)V

    .line 6080
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/cover/ScoverState;->getType()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    .line 6081
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->show()V

    .line 6083
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCallActive()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 6084
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->disableTransculentTheme()V

    .line 6087
    :cond_3
    return-void
.end method

.method public toggleDriveMode()V
    .locals 3

    .prologue
    .line 3642
    const/4 v0, 0x0

    .line 3645
    .local v0, "driveFlag":Z
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    .line 3646
    .local v1, "state":Lcom/vlingo/midas/gui/ControlFragment$ControlState;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->changeToDriveMode(Z)V

    .line 3647
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v1}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 3648
    return-void
.end method

.method public tutorialMicListeningAnim()V
    .locals 2

    .prologue
    .line 5786
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v0, :cond_0

    .line 5787
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 5788
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/ListeningView;->initListeningAnimForTutorial()V

    .line 5789
    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorial:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5790
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    new-instance v1, Lcom/vlingo/midas/gui/ConversationActivity$22;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/ConversationActivity$22;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 5797
    :cond_0
    return-void
.end method

.method public unlockLockscreen(Z)V
    .locals 5
    .param p1, "unlock"    # Z

    .prologue
    const/4 v4, 0x0

    .line 2309
    const-string/jumbo v2, "secure_voice_wake_up"

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDSPWakeupEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2311
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2312
    .local v0, "win":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2313
    .local v1, "winParams":Landroid/view/WindowManager$LayoutParams;
    if-eqz p1, :cond_1

    .line 2314
    const-string/jumbo v2, "should_wakeup_enabled"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 2315
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, 0x80000

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 2320
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2323
    .end local v0    # "win":Landroid/view/Window;
    .end local v1    # "winParams":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    iput-boolean v4, p0, Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z

    .line 2324
    return-void

    .line 2317
    .restart local v0    # "win":Landroid/view/Window;
    .restart local v1    # "winParams":Landroid/view/WindowManager$LayoutParams;
    :cond_1
    const-string/jumbo v2, "should_wakeup_enabled"

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 2318
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, -0x80001

    and-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 5614
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->onUserCancel()V

    .line 5615
    return-void
.end method

.method public voiceWakeUpLCDOff()V
    .locals 3

    .prologue
    .line 6160
    const-string/jumbo v1, "device_policy"

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DevicePolicyManager;

    sput-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->mDPM:Landroid/app/admin/DevicePolicyManager;

    .line 6161
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/vlingo/midas/gui/ConversationActivity$AdminReceiver;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sput-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->mDeviceAdminName:Landroid/content/ComponentName;

    .line 6162
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->mDPM:Landroid/app/admin/DevicePolicyManager;

    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->mDeviceAdminName:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/app/admin/DevicePolicyManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6163
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.app.action.ADD_DEVICE_ADMIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6165
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "android.app.extra.DEVICE_ADMIN"

    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->mDeviceAdminName:Landroid/content/ComponentName;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 6167
    const/4 v1, 0x1

    sput v1, Lcom/vlingo/midas/gui/ConversationActivity;->lockTheDevice:I

    .line 6168
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    .line 6172
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 6170
    :cond_0
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v1}, Landroid/app/admin/DevicePolicyManager;->lockNow()V

    goto :goto_0
.end method
