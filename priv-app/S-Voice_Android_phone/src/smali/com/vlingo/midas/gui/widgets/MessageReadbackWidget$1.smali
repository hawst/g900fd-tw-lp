.class Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$1;
.super Ljava/lang/Object;
.source "MessageReadbackWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v2, 0x8

    .line 46
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    if-eqz v1, :cond_1

    .line 47
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 48
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;)Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->retire()V

    .line 52
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 53
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Reply"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 56
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method
