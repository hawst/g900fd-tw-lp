.class public Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;
.super Landroid/widget/RelativeLayout;
.source "MicListeningAnimationView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView$1;,
        Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView$SineEaseOut;
    }
.end annotation


# static fields
.field private static final SINE_OUT:Landroid/view/animation/Interpolator;


# instance fields
.field protected animCount:I

.field protected layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

.field protected layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

.field protected layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

.field protected layer4:Lcom/vlingo/midas/gui/customviews/MicListeningView;

.field protected layer5:Lcom/vlingo/midas/gui/customviews/MicListeningView;

.field private mAnimator:Landroid/view/ViewPropertyAnimator;

.field protected mRmsBefore:I

.field protected micImages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/gui/customviews/MicListeningView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView$SineEaseOut;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView$SineEaseOut;-><init>(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView$1;)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->micImages:Ljava/util/ArrayList;

    .line 19
    iput v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->animCount:I

    .line 21
    iput v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->mRmsBefore:I

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->micImages:Ljava/util/ArrayList;

    .line 27
    return-void
.end method


# virtual methods
.method public cancelAllAnimations()V
    .locals 3

    .prologue
    .line 61
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->micImages:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/customviews/MicListeningView;

    .line 62
    .local v1, "mImage":Lcom/vlingo/midas/gui/customviews/MicListeningView;
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->cancelAnimation()V

    goto :goto_0

    .line 64
    .end local v1    # "mImage":Lcom/vlingo/midas/gui/customviews/MicListeningView;
    :cond_0
    return-void
.end method

.method public cancelAnimation()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 47
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 49
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->SINE_OUT:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 51
    :cond_0
    return-void
.end method

.method public cancelTimer()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public micListeningAnimation(I)V
    .locals 0
    .param p1, "rmsNext"    # I

    .prologue
    .line 35
    return-void
.end method

.method public resetCount()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->animCount:I

    .line 68
    return-void
.end method

.method public scaleAnimation(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 45
    return-void
.end method

.method public setMicAnimation()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public showMicListeningAnimation(I)V
    .locals 0
    .param p1, "rmsValue"    # I

    .prologue
    .line 39
    return-void
.end method
