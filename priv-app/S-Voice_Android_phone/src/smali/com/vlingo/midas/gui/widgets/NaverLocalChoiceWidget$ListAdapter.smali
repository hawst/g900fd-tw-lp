.class Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;
.super Landroid/widget/BaseAdapter;
.source "NaverLocalChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$1;

    .prologue
    .line 180
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    .line 184
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->localSearchListings:Ljava/util/List;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 185
    .local v0, "naturalSize":I
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    invoke-virtual {v2, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->getLimitedCount(I)I

    move-result v1

    .line 186
    .local v1, "toReturn":I
    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 191
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->localSearchListings:Ljava/util/List;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 196
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 204
    if-nez p2, :cond_3

    .line 205
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$layout;->item_naver_local_search:I

    invoke-virtual {v6, v7, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 206
    new-instance v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;

    const/4 v6, 0x0

    invoke-direct {v3, v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$1;)V

    .line 207
    .local v3, "holder":Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;
    sget v6, Lcom/vlingo/midas/R$id;->ls_item_name:I

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->name:Landroid/widget/TextView;

    .line 208
    sget v6, Lcom/vlingo/midas/R$id;->ls_item_rate_img:I

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RatingBar;

    iput-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->rate_img:Landroid/widget/RatingBar;

    .line 209
    sget v6, Lcom/vlingo/midas/R$id;->ls_item_review_count:I

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->review_count_num:Landroid/widget/TextView;

    .line 210
    sget v6, Lcom/vlingo/midas/R$id;->ls_item_distance:I

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->distance:Landroid/widget/TextView;

    .line 211
    sget v6, Lcom/vlingo/midas/R$id;->ls_item_category:I

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->category:Landroid/widget/TextView;

    .line 212
    sget v6, Lcom/vlingo/midas/R$id;->ls_item_address:I

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->address:Landroid/widget/TextView;

    .line 213
    sget v6, Lcom/vlingo/midas/R$id;->ls_item_call:I

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->call:Landroid/widget/ImageButton;

    .line 214
    sget v6, Lcom/vlingo/midas/R$id;->local_search_info:I

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->content:Landroid/view/View;

    .line 215
    sget v6, Lcom/vlingo/midas/R$id;->movie_divider:I

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->mDivider:Landroid/view/View;

    .line 216
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 217
    sget v6, Lcom/vlingo/midas/R$id;->logo_nhn:I

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->logo:Landroid/widget/ImageView;

    .line 219
    :cond_0
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 224
    :goto_0
    new-instance v4, Lcom/vlingo/midas/naver/NaverLocalItem;

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->localSearchListings:Ljava/util/List;
    invoke-static {v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Hashtable;

    invoke-direct {v4, v6}, Lcom/vlingo/midas/naver/NaverLocalItem;-><init>(Ljava/util/Hashtable;)V

    .line 226
    .local v4, "item":Lcom/vlingo/midas/naver/NaverLocalItem;
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverLocalItem;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->rate_img:Landroid/widget/RatingBar;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverLocalItem;->getRating()Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    const/high16 v8, 0x41200000    # 10.0f

    div-float/2addr v7, v8

    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->rate_img:Landroid/widget/RatingBar;

    invoke-virtual {v8}, Landroid/widget/RatingBar;->getNumStars()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    invoke-virtual {v6, v7}, Landroid/widget/RatingBar;->setRating(F)V

    .line 230
    const/4 v5, 0x0

    .line 231
    .local v5, "review_count":I
    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverLocalItem;->getReviewCount()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 232
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->review_count_num:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->distance:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverLocalItem;->getDistance()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mMainLocation:Ljava/lang/String;
    invoke-static {v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->access$400(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverLocalItem;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/vlingo/midas/naver/NaverLocalItem;->getSeparatedLocation(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 244
    .local v0, "addressString":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverLocalItem;->getDistance()Ljava/lang/String;

    move-result-object v2

    .line 250
    .local v2, "distance":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 251
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->address:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    :cond_1
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->category:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverLocalItem;->getCategory()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverLocalItem;->getDetailUrl()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->detailLink:Ljava/lang/String;
    invoke-static {v3, v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->access$502(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;Ljava/lang/String;)Ljava/lang/String;

    .line 256
    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverLocalItem;->getPhoneUrl()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    .line 257
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->call:Landroid/widget/ImageButton;

    invoke-virtual {v6, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 260
    :goto_1
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->content:Landroid/view/View;

    new-instance v7, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter$1;

    invoke-direct {v7, p0}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    const/4 v1, 0x2

    .line 276
    .local v1, "density":I
    sget v6, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 277
    if-nez p1, :cond_5

    .line 341
    :cond_2
    :goto_2
    return-object p2

    .line 221
    .end local v0    # "addressString":Ljava/lang/String;
    .end local v1    # "density":I
    .end local v2    # "distance":Ljava/lang/String;
    .end local v3    # "holder":Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;
    .end local v4    # "item":Lcom/vlingo/midas/naver/NaverLocalItem;
    .end local v5    # "review_count":I
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;

    .restart local v3    # "holder":Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;
    goto/16 :goto_0

    .line 259
    .restart local v0    # "addressString":Ljava/lang/String;
    .restart local v2    # "distance":Ljava/lang/String;
    .restart local v4    # "item":Lcom/vlingo/midas/naver/NaverLocalItem;
    .restart local v5    # "review_count":I
    :cond_4
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->call:Landroid/widget/ImageButton;

    new-instance v7, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$CallClickListener;

    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverLocalItem;->getPhoneUrl()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$CallClickListener;-><init>(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 307
    .restart local v1    # "density":I
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-lt p1, v6, :cond_2

    .line 309
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->mDivider:Landroid/view/View;

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    .line 310
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 311
    iget-object v6, v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->logo:Landroid/widget/ImageView;

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method
