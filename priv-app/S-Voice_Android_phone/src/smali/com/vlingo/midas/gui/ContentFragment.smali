.class public abstract Lcom/vlingo/midas/gui/ContentFragment;
.super Landroid/support/v4/app/Fragment;
.source "ContentFragment.java"


# static fields
.field public static final EXTRA_LIST_HEIGHT:Ljava/lang/String; = "EXTRA_LIST_HEIGHT"

.field public static final FHW_RESULT_STRING:Ljava/lang/String; = "FHW_RESULT_STRING"

.field public static final LIST_MOVE_ACTION:Ljava/lang/String; = "LIST_MOVE_ACTION"

.field public static final SHOW_LIST_ACTION:Ljava/lang/String; = "SHOW_LIST_ACTION"


# instance fields
.field private calledOnConfigrationChanged:Z

.field protected fragmentLanguage:Ljava/lang/String;

.field protected final mRunningQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field protected final mWaitingQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 25
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ContentFragment;->mWaitingQueue:Ljava/util/Queue;

    .line 27
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ContentFragment;->mRunningQueue:Ljava/util/Queue;

    return-void
.end method


# virtual methods
.method public abstract addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;
.end method

.method public abstract addDummyView(Landroid/view/View;)V
.end method

.method public abstract addHelpChoiceWidget()V
.end method

.method public abstract addHelpWidget(Landroid/content/Intent;)V
.end method

.method public abstract addMainPrompt()V
.end method

.method public abstract addTimerWidget()V
.end method

.method public abstract addTutorialWidget()V
.end method

.method protected addView(Landroid/view/View;Z)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "fillScreen"    # Z

    .prologue
    .line 128
    iget-object v0, p0, Lcom/vlingo/midas/gui/ContentFragment;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/midas/gui/ContentFragment;->doAddView(Landroid/view/View;Z)V

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ContentFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addWidget(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 104
    move-object v1, p1

    .line 105
    .local v1, "viewMini":Landroid/view/View;
    instance-of v2, p1, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-eqz v2, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ContentFragment;->addHelpChoiceWidget()V

    .line 123
    :goto_0
    return-void

    .line 107
    :cond_0
    instance-of v2, p1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;

    if-eqz v2, :cond_1

    .line 108
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ContentFragment;->addYouCanSayWidget()V

    goto :goto_0

    .line 110
    :cond_1
    const/4 v0, 0x0

    .line 111
    .local v0, "delay":I
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    const/16 v0, 0x5dc

    .line 113
    :cond_2
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/vlingo/midas/gui/ContentFragment$1;

    invoke-direct {v3, p0, v1}, Lcom/vlingo/midas/gui/ContentFragment$1;-><init>(Lcom/vlingo/midas/gui/ContentFragment;Landroid/view/View;)V

    int-to-long v4, v0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 121
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->setHelpVisible(Z)V

    goto :goto_0
.end method

.method public abstract addYouCanSayWidget()V
.end method

.method public abstract cancelAsrEditing()V
.end method

.method public abstract checkWakeUpView(Landroid/view/View;)Z
.end method

.method public abstract cleanupPreviousBubbles()V
.end method

.method protected abstract doAddView(Landroid/view/View;Z)V
.end method

.method public abstract doScroll()V
.end method

.method public getFragmentLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vlingo/midas/gui/ContentFragment;->fragmentLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public hideMusic()V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public isCalledOnConfigrationChanged()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ContentFragment;->calledOnConfigrationChanged:Z

    return v0
.end method

.method protected abstract onIdle()V
.end method

.method public abstract onLanguageChanged()V
.end method

.method protected abstract onUserCancel()V
.end method

.method public abstract removeAlreadyExistingHelpScreen()V
.end method

.method public abstract removeReallyWakeupBubble()V
.end method

.method public abstract removeWakeupBubble()V
.end method

.method public abstract replaceUserEditBubble(Ljava/lang/String;)Lcom/vlingo/midas/gui/DialogBubble;
.end method

.method public abstract resetAllContent()V
.end method

.method public abstract resetAllContentForTutorial(Z)V
.end method

.method public setCalledOnConfigrationChanged(Z)V
    .locals 0
    .param p1, "calledOnConfigrationChanged"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ContentFragment;->calledOnConfigrationChanged:Z

    .line 98
    return-void
.end method

.method public setMenu()V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public setMenuVar(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 145
    return-void
.end method

.method public abstract startAnimationFlipDown(Landroid/view/View;)V
.end method

.method public abstract startAnimationFlipUp(Landroid/view/View;)V
.end method
