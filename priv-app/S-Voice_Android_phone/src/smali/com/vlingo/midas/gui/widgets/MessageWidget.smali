.class public Lcom/vlingo/midas/gui/widgets/MessageWidget;
.super Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;
.source "MessageWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final DETAIL:I

.field private final EDIT:I

.field private final RETIRED:I

.field private cancelBtn:Landroid/widget/Button;

.field private isClicked:Z

.field private sendBtn:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->isClicked:Z

    .line 28
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->EDIT:I

    .line 29
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->DETAIL:I

    .line 30
    iput v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->RETIRED:I

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/MessageWidget;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MessageWidget;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/gui/widgets/MessageWidget;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MessageWidget;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/MessageWidget;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MessageWidget;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 5
    .param p1, "messageType"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const/4 v4, 0x0

    .line 84
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 86
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 97
    :cond_0
    if-eqz p2, :cond_1

    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->SendButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->CancelButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 98
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->retire()V

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 102
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_to:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_to:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 23
    check-cast p1, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->isClicked:Z

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageContainer:Landroid/widget/LinearLayout;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->editInNativeSmsApp()V

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageContainer:Landroid/widget/LinearLayout;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->openNativeSmsApp()V

    .line 114
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->retire()V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->onFinishInflate()V

    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    sget v0, Lcom/vlingo/midas/R$id;->btn_send:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    .line 41
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/MessageWidget$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/MessageWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    :cond_0
    sget v0, Lcom/vlingo/midas/R$id;->btn_cancel:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    .line 62
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/MessageWidget$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget$2;-><init>(Lcom/vlingo/midas/gui/widgets/MessageWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    :cond_1
    return-void
.end method

.method public retire()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v1, 0x8

    .line 134
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageButtonContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 143
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->titleTo:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 147
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_to:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 151
    :goto_0
    return-void

    .line 149
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public retireCancel()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v1, 0x8

    .line 153
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->retire()V

    .line 155
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageButtonContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->titleTo:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 168
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_to:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
