.class public Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "NormalNewsWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/midas/news/NewsItem;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private LOG_TAG:Ljava/lang/String;

.field private detail:Landroid/widget/TextView;

.field private headline:Landroid/widget/TextView;

.field private logoView:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mMore:Lcom/vlingo/midas/gui/customviews/Button;

.field private mNewsItem:Lcom/vlingo/midas/news/NewsItem;

.field private updateDate:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-string/jumbo v0, "DrivingNewsWidget"

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->LOG_TAG:Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method

.method private makeCorrectDateString(Lcom/vlingo/midas/news/NewsItem;)Ljava/lang/String;
    .locals 9
    .param p1, "first"    # Lcom/vlingo/midas/news/NewsItem;

    .prologue
    const/4 v8, 0x5

    .line 83
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 84
    .local v3, "newsTime":Ljava/util/Calendar;
    invoke-virtual {p1}, Lcom/vlingo/midas/news/NewsItem;->getTimeCreated()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 85
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 87
    .local v4, "now":Ljava/util/Calendar;
    const-string/jumbo v5, "h:mm aa"

    .line 88
    .local v5, "timeFormatString":Ljava/lang/String;
    const-string/jumbo v1, "EEEE, MMMM d, h:mm aa"

    .line 89
    .local v1, "dateTimeFormatString":Ljava/lang/String;
    const-string/jumbo v0, "EEE. dd LLL"

    .line 91
    .local v0, "dateFormatString":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/midas/settings/MidasSettings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v2

    .line 93
    .local v2, "mCurrentLocale":Ljava/util/Locale;
    sget-object v6, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v2, v6}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 94
    const-string/jumbo v0, "LLL. dd EEE"

    .line 96
    :cond_0
    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v3, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    if-ne v6, v7, :cond_1

    .line 97
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$string;->drivine_mode_news_updated:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "h:mm aa"

    invoke-static {v7, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 100
    :goto_0
    return-object v6

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$string;->drivine_mode_news_updated:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method


# virtual methods
.method public fillupMessage(Lcom/vlingo/midas/news/NewsItem;)V
    .locals 2
    .param p1, "first"    # Lcom/vlingo/midas/news/NewsItem;

    .prologue
    .line 62
    if-eqz p1, :cond_0

    .line 63
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->headline:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/vlingo/midas/news/NewsItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->detail:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/vlingo/midas/news/NewsItem;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->updateDate:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->makeCorrectDateString(Lcom/vlingo/midas/news/NewsItem;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    invoke-virtual {p1}, Lcom/vlingo/midas/news/NewsItem;->getNewsCP()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 67
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->logoView:Landroid/widget/ImageView;

    sget v1, Lcom/vlingo/midas/R$drawable;->news_united_logo:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 71
    :goto_0
    invoke-virtual {p1}, Lcom/vlingo/midas/news/NewsItem;->getAuthor()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 72
    sget v0, Lcom/vlingo/midas/R$id;->news_cp_who:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    :cond_0
    :goto_1
    return-void

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->logoView:Landroid/widget/ImageView;

    sget v1, Lcom/vlingo/midas/R$drawable;->news_flipboard_logo:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 74
    :cond_2
    sget v0, Lcom/vlingo/midas/R$id;->news_cp_who:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/vlingo/midas/news/NewsItem;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public initialize(Lcom/vlingo/midas/news/NewsItem;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 1
    .param p1, "object"    # Lcom/vlingo/midas/news/NewsItem;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->mNewsItem:Lcom/vlingo/midas/news/NewsItem;

    .line 108
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->mNewsItem:Lcom/vlingo/midas/news/NewsItem;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->fillupMessage(Lcom/vlingo/midas/news/NewsItem;)V

    .line 109
    return-void
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 24
    check-cast p1, Lcom/vlingo/midas/news/NewsItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->initialize(Lcom/vlingo/midas/news/NewsItem;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 123
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->mNewsItem:Lcom/vlingo/midas/news/NewsItem;

    if-eqz v1, :cond_1

    .line 124
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->mNewsItem:Lcom/vlingo/midas/news/NewsItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/news/NewsItem;->getNewsIntent()Landroid/content/Intent;

    move-result-object v0

    .line 125
    .local v0, "launchNews":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 126
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 127
    :cond_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 128
    invoke-virtual {p1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 130
    .end local v0    # "launchNews":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 79
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 47
    sget v0, Lcom/vlingo/midas/R$id;->normal_news_headline:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->headline:Landroid/widget/TextView;

    .line 48
    sget v0, Lcom/vlingo/midas/R$id;->normal_news_detail:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->detail:Landroid/widget/TextView;

    .line 49
    sget v0, Lcom/vlingo/midas/R$id;->normal_news_updateDate:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->updateDate:Landroid/widget/TextView;

    .line 50
    sget v0, Lcom/vlingo/midas/R$id;->normal_news_cp_logo:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->logoView:Landroid/widget/ImageView;

    .line 51
    sget v0, Lcom/vlingo/midas/R$id;->btn_more_news:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->mMore:Lcom/vlingo/midas/gui/customviews/Button;

    .line 52
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->mMore:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 54
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->mMore:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {p0, p0}, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
