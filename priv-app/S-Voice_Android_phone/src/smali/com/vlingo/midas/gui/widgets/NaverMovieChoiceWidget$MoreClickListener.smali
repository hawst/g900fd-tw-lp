.class Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MoreClickListener;
.super Ljava/lang/Object;
.source "NaverMovieChoiceWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoreClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

.field private final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;Ljava/lang/String;)V
    .locals 0
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MoreClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MoreClickListener;->url:Ljava/lang/String;

    .line 183
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 196
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MoreClickListener;->url:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 197
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 198
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MoreClickListener;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 199
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MoreClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 201
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
