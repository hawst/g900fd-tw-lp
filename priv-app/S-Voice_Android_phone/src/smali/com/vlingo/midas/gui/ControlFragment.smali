.class public abstract Lcom/vlingo/midas/gui/ControlFragment;
.super Landroid/support/v4/app/Fragment;
.source "ControlFragment.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/ControlFragment$8;,
        Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;,
        Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;,
        Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;,
        Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;,
        Lcom/vlingo/midas/gui/ControlFragment$ControlState;
    }
.end annotation


# static fields
.field public static final BT_START_REC_DELAY_TIME:J = 0x7d0L

.field private static final FINISH_ACTIVITY:I = 0x1

.field private static final MSG_START_SPOTTER:I

.field private static count:I

.field private static maxTTSPlayCount:I

.field public static startTimer:Z


# instance fields
.field private NUMBER_TTS_IS_PLAYED:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;

.field protected callback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

.field private countTimer:Lcom/vlingo/midas/gui/CountTimer;

.field private mActivityCreated:Z

.field private mActivityPaused:Z

.field private mDefaultSharedPrefs:Landroid/content/SharedPreferences;

.field protected mDelayBeforePhraseSpotter:Z

.field private mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

.field private mKm:Landroid/app/KeyguardManager;

.field private mScreenTimeoutVal:I

.field private mSetToIdleByEditCancel:Z

.field private mTaskOnFinishGreetingTTS:Ljava/lang/Runnable;

.field private mTextToSpeechBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;

.field private mTts:Landroid/speech/tts/TextToSpeech;

.field private m_isRecognitionPostBTConnect:Z

.field private playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private shouldFinish:Z

.field public standardFlag:Z

.field private startRecoByEarEvent:Z

.field private timesTTSPlayed:I

.field private toListening:Z

.field private ttsStringToPlay:Ljava/lang/String;

.field private wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 103
    sput v0, Lcom/vlingo/midas/gui/ControlFragment;->count:I

    .line 105
    sput-boolean v0, Lcom/vlingo/midas/gui/ControlFragment;->startTimer:Z

    .line 108
    const/16 v0, 0xa

    sput v0, Lcom/vlingo/midas/gui/ControlFragment;->maxTTSPlayCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 85
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mActivityCreated:Z

    .line 86
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mActivityPaused:Z

    .line 89
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mDelayBeforePhraseSpotter:Z

    .line 91
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->m_isRecognitionPostBTConnect:Z

    .line 99
    const-string/jumbo v0, "count_for_tts_play"

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->NUMBER_TTS_IS_PLAYED:Ljava/lang/String;

    .line 100
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->startRecoByEarEvent:Z

    .line 101
    iput-object v2, p0, Lcom/vlingo/midas/gui/ControlFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 104
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->standardFlag:Z

    .line 106
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->shouldFinish:Z

    .line 107
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->toListening:Z

    .line 109
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->ttsStringToPlay:Ljava/lang/String;

    .line 112
    iput-object v2, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 117
    const-string/jumbo v0, "ControlFragment"

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->TAG:Ljava/lang/String;

    .line 216
    iput-object v2, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;

    .line 302
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;-><init>(Lcom/vlingo/midas/gui/ControlFragment;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    .line 1117
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/ControlFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ControlFragment;

    .prologue
    .line 70
    iget v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->timesTTSPlayed:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 70
    sget v0, Lcom/vlingo/midas/gui/ControlFragment;->maxTTSPlayCount:I

    return v0
.end method

.method static synthetic access$1002(Lcom/vlingo/midas/gui/ControlFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ControlFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragment;->m_isRecognitionPostBTConnect:Z

    return p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/ControlFragment;)Lcom/vlingo/midas/gui/CountTimer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ControlFragment;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->countTimer:Lcom/vlingo/midas/gui/CountTimer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/ControlFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ControlFragment;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->shouldFinish:Z

    return v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/ControlFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ControlFragment;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->toListening:Z

    return v0
.end method

.method static synthetic access$402(Lcom/vlingo/midas/gui/ControlFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ControlFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragment;->toListening:Z

    return p1
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/ControlFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ControlFragment;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragment;->setMicStateToListening()V

    return-void
.end method

.method static synthetic access$702(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 70
    sput p0, Lcom/vlingo/midas/gui/ControlFragment;->count:I

    return p0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/gui/ControlFragment;)Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/ControlFragment;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;

    return-object v0
.end method

.method public static getRandom(II)I
    .locals 6
    .param p0, "minValue"    # I
    .param p1, "maxValue"    # I

    .prologue
    .line 1217
    int-to-double v0, p0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    add-int/lit8 v4, p1, 0x1

    sub-int/2addr v4, p0

    int-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private getTTSOnIdleList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1245
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentData;->getTTSOnIdleList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private isActivityPaused()Z
    .locals 1

    .prologue
    .line 1261
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mActivityPaused:Z

    return v0
.end method

.method private isStartRecoOnSpotterStop()Z
    .locals 1

    .prologue
    .line 1229
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentData;->isStartRecoOnSpotterStop()Z

    move-result v0

    return v0
.end method

.method private playDefaultTTS(Ljava/lang/String;)V
    .locals 5
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 234
    iget-object v2, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v2, :cond_0

    .line 235
    new-instance v2, Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;

    invoke-direct {v2, p0, v3}, Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ControlFragment;Lcom/vlingo/midas/gui/ControlFragment$1;)V

    iput-object v2, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;

    .line 236
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 237
    .local v1, "ttsfilter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 239
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 240
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 241
    iget-object v2, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTts:Landroid/speech/tts/TextToSpeech;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v3, v4}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 242
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragment;->updateTTSCountValue()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    .end local v1    # "ttsfilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 243
    .restart local v1    # "ttsfilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "ControlFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Error while TTS speak : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setMicStateToListening()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1137
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->standardFlag:Z

    if-ne v0, v1, :cond_0

    .line 1138
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->shouldFinish:Z

    .line 1139
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->toListening:Z

    .line 1140
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->setToListening()V

    .line 1142
    :cond_0
    return-void
.end method

.method private setPaused(Z)V
    .locals 1
    .param p1, "isPaused"    # Z

    .prologue
    .line 1241
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentData;->setPaused(Z)V

    .line 1242
    return-void
.end method

.method private setStartRecoOnSpotterStop(Z)V
    .locals 1
    .param p1, "startRecoOnSpotterStop"    # Z

    .prologue
    .line 1233
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentData;->setStartRecoOnSpotterStop(Z)V

    .line 1234
    return-void
.end method

.method private setTTSOnIdleList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1249
    .local p1, "mTTSOnIdleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentData;->setTTSOnIdleList(Ljava/util/ArrayList;)V

    .line 1250
    return-void
.end method

.method private updateTTSCountValue()V
    .locals 3

    .prologue
    .line 821
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 822
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->timesTTSPlayed:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->timesTTSPlayed:I

    .line 823
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->NUMBER_TTS_IS_PLAYED:Ljava/lang/String;

    iget v2, p0, Lcom/vlingo/midas/gui/ControlFragment;->timesTTSPlayed:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 824
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 825
    return-void
.end method


# virtual methods
.method public announceTTS(Ljava/lang/String;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 1130
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ControlFragment;->playDefaultTTS(Ljava/lang/String;)V

    .line 1134
    :goto_0
    return-void

    .line 1133
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public announceTTSMiniMode()V
    .locals 1

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->ttsStringToPlay:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ControlFragment;->announceTTS(Ljava/lang/String;)V

    .line 1146
    return-void
.end method

.method public announceTTSOnDialogIdle(Ljava/lang/String;)V
    .locals 1
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 1150
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/ControlFragment;->announceTTS(Ljava/lang/String;)V

    .line 1156
    :goto_0
    return-void

    .line 1154
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getTTSOnIdleList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public avoidWakeUpCommandTTS()V
    .locals 2

    .prologue
    .line 608
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopSpotting()V

    .line 610
    const-wide/16 v0, 0x1770

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/gui/ControlFragment;->scheduleStartSpotter(J)V

    .line 612
    :cond_0
    return-void
.end method

.method protected blinkAnim()V
    .locals 0

    .prologue
    .line 1200
    return-void
.end method

.method public cancelTTS()V
    .locals 1

    .prologue
    .line 1159
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 1160
    return-void
.end method

.method public checkMissedEvents()Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 402
    const-string/jumbo v5, "listen_over_bluetooth"

    invoke-static {v5, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 403
    .local v4, "useBluetoothForAudio":Z
    const-string/jumbo v5, "headset_mode"

    invoke-static {v5, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 404
    .local v1, "checkScheduleEnabled":Z
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-string/jumbo v6, "audio"

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 405
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v2

    .line 406
    .local v2, "isWiredHeadsetOn":Z
    const/4 v3, 0x0

    .line 407
    .local v3, "micOpenBySco":Z
    if-nez v2, :cond_3

    if-eqz v4, :cond_3

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 408
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v5

    if-nez v5, :cond_2

    .line 409
    if-eqz v1, :cond_0

    .line 410
    iput-boolean v7, p0, Lcom/vlingo/midas/gui/ControlFragment;->m_isRecognitionPostBTConnect:Z

    .line 413
    :cond_0
    const/4 v3, 0x1

    .line 416
    new-instance v5, Lcom/vlingo/midas/gui/ControlFragment$2;

    invoke-direct {v5, p0}, Lcom/vlingo/midas/gui/ControlFragment$2;-><init>(Lcom/vlingo/midas/gui/ControlFragment;)V

    const-wide/16 v6, 0x7d0

    invoke-static {v5, v6, v7}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->runTaskOnBluetoothAudioOn(Ljava/lang/Runnable;J)V

    .line 453
    :cond_1
    :goto_0
    return v3

    .line 443
    :cond_2
    if-eqz v1, :cond_1

    .line 444
    invoke-static {p0}, Lcom/vlingo/midas/util/CheckPhoneEvents;->getInstance(Lcom/vlingo/midas/gui/ControlFragment;)Lcom/vlingo/midas/util/CheckPhoneEvents;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/util/CheckPhoneEvents;->check()Z

    goto :goto_0

    .line 447
    :cond_3
    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 448
    invoke-static {p0}, Lcom/vlingo/midas/util/CheckPhoneEvents;->getInstance(Lcom/vlingo/midas/gui/ControlFragment;)Lcom/vlingo/midas/util/CheckPhoneEvents;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/util/CheckPhoneEvents;->check()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 449
    iput-boolean v7, p0, Lcom/vlingo/midas/gui/ControlFragment;->startRecoByEarEvent:Z

    goto :goto_0
.end method

.method protected disableHelpWidgetButton()V
    .locals 0

    .prologue
    .line 1196
    return-void
.end method

.method protected disableMicInTutorial(Z)V
    .locals 0
    .param p1, "disable"    # Z

    .prologue
    .line 1278
    return-void
.end method

.method protected enableHelpWidgetButton()V
    .locals 0

    .prologue
    .line 1194
    return-void
.end method

.method public fadeInMicLayout(ZI)V
    .locals 0
    .param p1, "flag"    # Z
    .param p2, "duration"    # I

    .prologue
    .line 1282
    return-void
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 918
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method public getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;
    .locals 1

    .prologue
    .line 604
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;
    .locals 1

    .prologue
    .line 1221
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentData;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v0

    return-object v0
.end method

.method public getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 381
    const-string/jumbo v1, "samsung_wakeup_engine_enable"

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v2, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 385
    invoke-static {}, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;->getPhraseSpotterParameters()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .line 396
    .local v0, "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :goto_0
    return-object v0

    .line 386
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_0
    const-string/jumbo v1, "samsung_multi_engine_enable"

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 389
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v2, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 390
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    goto :goto_0

    .line 394
    .end local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v0

    .restart local v0    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    goto :goto_0
.end method

.method protected getVoicePrompt()Lcom/vlingo/core/internal/settings/VoicePrompt;
    .locals 3

    .prologue
    .line 1170
    new-instance v0, Lcom/vlingo/core/internal/settings/VoicePrompt;

    invoke-direct {v0}, Lcom/vlingo/core/internal/settings/VoicePrompt;-><init>()V

    new-instance v1, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->registerDelegate(Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    move-result-object v0

    return-object v0
.end method

.method protected hideMic()V
    .locals 0

    .prologue
    .line 1277
    return-void
.end method

.method protected initPhraseSpotter()V
    .locals 2

    .prologue
    .line 375
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getPhraseSpotterParams()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 376
    return-void
.end method

.method public isActivityCreated()Z
    .locals 1

    .prologue
    .line 1041
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->ismActivityCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 1237
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentData;->isPaused()Z

    move-result v0

    return v0
.end method

.method public isStartRecoByEarEvent()Z
    .locals 1

    .prologue
    .line 457
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->startRecoByEarEvent:Z

    return v0
.end method

.method protected ismActivityCreated()Z
    .locals 1

    .prologue
    .line 1253
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mActivityCreated:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 369
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 371
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 372
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 309
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 310
    invoke-static {}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->getInstance()Lcom/vlingo/core/internal/display/WakeLockManager;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    .line 311
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-string/jumbo v5, "keyguard"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/KeyguardManager;

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->mKm:Landroid/app/KeyguardManager;

    .line 313
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 314
    .local v2, "res":Landroid/content/res/Resources;
    sget v4, Lcom/vlingo/midas/R$string;->svoice_is_listening_speak_now:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->ttsStringToPlay:Ljava/lang/String;

    .line 315
    const-string/jumbo v4, "car_word_spotter_enabled"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 330
    .local v3, "spottingEnabled":Z
    new-instance v4, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ControlFragment;Lcom/vlingo/midas/gui/ControlFragment$1;)V

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;

    .line 331
    new-instance v4, Lcom/vlingo/midas/gui/ControlFragment$1;

    invoke-direct {v4, p0}, Lcom/vlingo/midas/gui/ControlFragment$1;-><init>(Lcom/vlingo/midas/gui/ControlFragment;)V

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 352
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 353
    .local v0, "inf":Landroid/content/IntentFilter;
    const-string/jumbo v4, "com.vlingo.client.app.action.AUDIO_FOCUS_CHANGED"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 354
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/gui/ControlFragment;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;

    invoke-virtual {v4, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 356
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 357
    .local v1, "playMusicIntentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v4, "com.sec.android.app.music.musicservicecommand.pause"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 358
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/gui/ControlFragment;->playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 360
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen()Z

    move-result v4

    if-nez v4, :cond_1

    .line 361
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 362
    iget-object v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    iget-object v5, p0, Lcom/vlingo/midas/gui/ControlFragment;->NUMBER_TTS_IS_PLAYED:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->timesTTSPlayed:I

    .line 363
    new-instance v4, Lcom/vlingo/midas/gui/CountTimer;

    const-wide/16 v5, 0xbb8

    const-wide/16 v7, 0x1

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/vlingo/midas/gui/CountTimer;-><init>(JJ)V

    iput-object v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->countTimer:Lcom/vlingo/midas/gui/CountTimer;

    .line 365
    :cond_1
    return-void
.end method

.method public abstract onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 571
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;

    if-eqz v1, :cond_0

    .line 572
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/ControlFragment;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 573
    iput-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment;->audioFocusChangeBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;

    .line 575
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 576
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/ControlFragment;->playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 577
    iput-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment;->playMusicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 580
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v1, :cond_2

    .line 581
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 582
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTts:Landroid/speech/tts/TextToSpeech;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 587
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;

    if-eqz v1, :cond_3

    .line 588
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 590
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 591
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTextToSpeechBroadcastReceiver:Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 596
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->removeMessages(I)V

    .line 597
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 598
    return-void

    .line 584
    :catch_0
    move-exception v0

    .line 585
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "ControlFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error while shutting down the TTS engine : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 592
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 593
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string/jumbo v1, "ControlFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error while unregistering the receiver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onIdle()V
    .locals 0

    .prologue
    .line 1209
    return-void
.end method

.method public onInit(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 136
    if-nez p1, :cond_0

    .line 137
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 139
    :cond_0
    return-void
.end method

.method public onLanguageChanged()V
    .locals 0

    .prologue
    .line 1208
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 481
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 482
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/ControlFragment;->setPaused(Z)V

    .line 483
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->destroy()V

    .line 484
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->standardFlag:Z

    .line 485
    sput-boolean v1, Lcom/vlingo/midas/gui/ControlFragment;->startTimer:Z

    .line 486
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->shouldFinish:Z

    .line 487
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->toListening:Z

    .line 488
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->countTimer:Lcom/vlingo/midas/gui/CountTimer;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->countTimer:Lcom/vlingo/midas/gui/CountTimer;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/CountTimer;->cancel()V

    .line 492
    :cond_0
    return-void
.end method

.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 0
    .param p1, "phrase"    # Ljava/lang/String;

    .prologue
    .line 955
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 8
    .param p1, "phrase"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 963
    const/4 v0, -0x1

    .line 964
    .local v0, "iResult":I
    const/4 v1, -0x1

    .line 966
    .local v1, "intType":I
    invoke-static {p1}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->isCustomWakeupWordPhrase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 967
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 972
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "com.vlingo.midas"

    const-string/jumbo v5, "WKID"

    invoke-static {v3, v4, v5}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    packed-switch v0, :pswitch_data_0

    .line 1004
    :goto_1
    if-eq v1, v6, :cond_2

    .line 1006
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/voiceshell/MultipleWakeUp;->getMultipleWakeUpIntent(ILandroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 1007
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 1008
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/ControlFragment;->startActivity(Landroid/content/Intent;)V

    .line 1009
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopSpotting()V

    .line 1016
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_2
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    .line 1017
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 1018
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 1019
    return-void

    .line 969
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 975
    :pswitch_0
    invoke-direct {p0, v7}, Lcom/vlingo/midas/gui/ControlFragment;->setStartRecoOnSpotterStop(Z)V

    goto :goto_1

    .line 978
    :pswitch_1
    invoke-direct {p0, v7}, Lcom/vlingo/midas/gui/ControlFragment;->setStartRecoOnSpotterStop(Z)V

    goto :goto_1

    .line 983
    :pswitch_2
    const-string/jumbo v3, "kew_wake_up_and_auto_function1"

    invoke-static {v3, v6}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 984
    goto :goto_1

    .line 988
    :pswitch_3
    const-string/jumbo v3, "kew_wake_up_and_auto_function2"

    invoke-static {v3, v6}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 989
    goto :goto_1

    .line 993
    :pswitch_4
    const-string/jumbo v3, "kew_wake_up_and_auto_function3"

    invoke-static {v3, v6}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 994
    goto :goto_1

    .line 998
    :pswitch_5
    const-string/jumbo v3, "kew_wake_up_and_auto_function4"

    invoke-static {v3, v6}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 999
    goto :goto_1

    .line 1013
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopSpotting()V

    goto :goto_2

    .line 973
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onPhraseSpotterStarted()V
    .locals 1

    .prologue
    .line 951
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->callback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    invoke-interface {v0}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;->onStartSpotting()V

    .line 952
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 1

    .prologue
    .line 931
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragment;->isStartRecoOnSpotterStop()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 932
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/ControlFragment;->setStartRecoOnSpotterStop(Z)V

    .line 933
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$3;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ControlFragment$3;-><init>(Lcom/vlingo/midas/gui/ControlFragment;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 945
    :goto_0
    return-void

    .line 940
    :cond_0
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$4;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ControlFragment$4;-><init>(Lcom/vlingo/midas/gui/ControlFragment;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onPromptOffClick(Z)V
    .locals 0
    .param p1, "manually"    # Z

    .prologue
    .line 1191
    return-void
.end method

.method public onPromptOnClick(Z)V
    .locals 0
    .param p1, "manually"    # Z

    .prologue
    .line 1180
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 513
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 514
    invoke-direct {p0, v5}, Lcom/vlingo/midas/gui/ControlFragment;->setPaused(Z)V

    .line 515
    const/4 v0, 0x0

    .line 516
    .local v0, "delay":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v3

    sget-object v4, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v3, v4, :cond_0

    .line 517
    const-string/jumbo v3, "key_social_login_attemp_for_resume"

    invoke-static {v3, v5}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_3

    .line 518
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 519
    .local v2, "fa":Landroid/support/v4/app/FragmentActivity;
    instance-of v3, v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    if-nez v3, :cond_0

    .line 520
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isDrivingMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 529
    const/16 v0, 0x5dc

    .line 540
    :goto_0
    int-to-long v3, v0

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/midas/gui/ControlFragment;->scheduleStartSpotter(J)V

    .line 549
    .end local v2    # "fa":Landroid/support/v4/app/FragmentActivity;
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->setPrompt()V

    .line 552
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "screen_off_timeout"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/vlingo/midas/gui/ControlFragment;->mScreenTimeoutVal:I

    .line 554
    iget v3, p0, Lcom/vlingo/midas/gui/ControlFragment;->mScreenTimeoutVal:I

    invoke-static {v3}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->setScreenTimeoutVal(I)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    :goto_2
    return-void

    .line 531
    .restart local v2    # "fa":Landroid/support/v4/app/FragmentActivity;
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 532
    const/16 v0, 0x384

    goto :goto_0

    .line 534
    :cond_2
    const/16 v0, 0x12c

    goto :goto_0

    .line 545
    .end local v2    # "fa":Landroid/support/v4/app/FragmentActivity;
    :cond_3
    const-string/jumbo v3, "key_social_login_attemp_for_resume"

    invoke-static {v3, v5}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 555
    :catch_0
    move-exception v1

    .line 556
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_2
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 3
    .param p1, "phrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 1026
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "com.vlingo.midas"

    const-string/jumbo v2, "WKID"

    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1029
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopSpotting()V

    .line 1031
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 1033
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$5;

    invoke-direct {v0, p0, p2}, Lcom/vlingo/midas/gui/ControlFragment$5;-><init>(Lcom/vlingo/midas/gui/ControlFragment;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 1038
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 565
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 566
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 567
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 496
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 497
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopTTS()V

    .line 498
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 499
    return-void
.end method

.method abstract performClickFromDriveControl()Z
.end method

.method protected removeDelegateFromLastScreen()V
    .locals 0

    .prologue
    .line 1281
    return-void
.end method

.method public removeMessages()V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->removeMessages(I)V

    .line 295
    return-void
.end method

.method protected removeMic()V
    .locals 0

    .prologue
    .line 1279
    return-void
.end method

.method protected removeMicStatusText()V
    .locals 0

    .prologue
    .line 1275
    return-void
.end method

.method public removeMicStatusTextForTutorial(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 1283
    return-void
.end method

.method public resetFinishMessage()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 298
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    invoke-virtual {v0, v3}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->removeMessages(I)V

    .line 299
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v3, v1, v2}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 300
    return-void
.end method

.method protected resetThinkingState()V
    .locals 0

    .prologue
    .line 1206
    return-void
.end method

.method public scheduleStartSpotter(J)V
    .locals 2
    .param p1, "delay"    # J

    .prologue
    const/4 v1, 0x0

    .line 621
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->removeMessages(I)V

    .line 622
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    invoke-virtual {v0, v1, p1, p2}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 623
    return-void
.end method

.method public setActivityPaused(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 1265
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mActivityPaused:Z

    .line 1266
    return-void
.end method

.method public abstract setBlueHeight(I)V
.end method

.method protected setButtonIdle(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 1197
    return-void
.end method

.method protected setButtonIdleForTutorial()V
    .locals 0

    .prologue
    .line 1202
    return-void
.end method

.method protected setButtonListening()V
    .locals 0

    .prologue
    .line 1199
    return-void
.end method

.method protected setButtonListeningForTutorial()V
    .locals 0

    .prologue
    .line 1203
    return-void
.end method

.method protected setButtonThinking()V
    .locals 0

    .prologue
    .line 1201
    return-void
.end method

.method protected setButtonThinkingForTutorial()V
    .locals 0

    .prologue
    .line 1204
    return-void
.end method

.method public setButtonsHidden(Z)V
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->btn_menu_kitkat:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 468
    .local v0, "menuBtn_Kitkat":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 469
    if-eqz p1, :cond_1

    .line 470
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 475
    :cond_0
    :goto_0
    return-void

    .line 472
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setCallback(Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    .prologue
    .line 1114
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragment;->callback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    .line 1115
    return-void
.end method

.method public setCurrentState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V
    .locals 1
    .param p1, "currentState"    # Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    .prologue
    .line 1225
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentData;->setCurrentState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 1226
    return-void
.end method

.method public setFakeSvoiceMiniForTutorial(Z)V
    .locals 0
    .param p1, "doset"    # Z

    .prologue
    .line 1280
    return-void
.end method

.method public setIsRecognitionPostBtConnect(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 816
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragment;->m_isRecognitionPostBTConnect:Z

    .line 817
    return-void
.end method

.method protected setMicIdleAnimForKTutorial()V
    .locals 0

    .prologue
    .line 1198
    return-void
.end method

.method protected setMicStatusText()V
    .locals 0

    .prologue
    .line 1272
    return-void
.end method

.method protected setPrompt()V
    .locals 0

    .prologue
    .line 561
    return-void
.end method

.method protected setResponseMode()V
    .locals 0

    .prologue
    .line 1207
    return-void
.end method

.method public setStartRecoByEarEvent(Z)V
    .locals 0
    .param p1, "isByEar"    # Z

    .prologue
    .line 461
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragment;->startRecoByEarEvent:Z

    .line 462
    return-void
.end method

.method public setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V
    .locals 8
    .param p1, "newState"    # Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    .prologue
    const-wide/16 v6, 0x258

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 629
    const-string/jumbo v1, "Vlingo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->isActivityCreated()Z

    move-result v1

    if-nez v1, :cond_1

    .line 813
    :cond_0
    :goto_0
    return-void

    .line 634
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragment;->isActivityPaused()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 636
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 637
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->remoteHasUiFocus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 638
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    goto :goto_0

    .line 642
    :cond_2
    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$8;->$SwitchMap$com$vlingo$midas$gui$ControlFragment$ControlState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 811
    :cond_3
    :goto_1
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/ControlFragment;->setCurrentState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 812
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->setMicStatusText()V

    goto :goto_0

    .line 645
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->resetThinkingState()V

    .line 646
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/ControlFragment;->setButtonIdle(Z)V

    .line 647
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopSpotting()V

    goto :goto_1

    .line 651
    :pswitch_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->resetThinkingState()V

    .line 653
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->setButtonListening()V

    .line 654
    invoke-virtual {p0, v4, v4}, Lcom/vlingo/midas/gui/ControlFragment;->fadeInMicLayout(ZI)V

    .line 655
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopSpotting()V

    goto :goto_1

    .line 661
    :pswitch_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setVolumeNormal(Landroid/content/Context;)V

    .line 666
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTaskOnFinishGreetingTTS:Ljava/lang/Runnable;

    if-eqz v1, :cond_4

    .line 667
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTaskOnFinishGreetingTTS:Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 668
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTaskOnFinishGreetingTTS:Ljava/lang/Runnable;

    goto :goto_0

    .line 672
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->enableHelpWidgetButton()V

    .line 673
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->resetThinkingState()V

    .line 674
    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/ControlFragment;->setButtonIdle(Z)V

    .line 676
    :goto_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getTTSOnIdleList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 677
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getTTSOnIdleList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 680
    .local v0, "tts":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ControlFragment;->announceTTS(Ljava/lang/String;)V

    goto :goto_2

    .line 689
    .end local v0    # "tts":Ljava/lang/String;
    :cond_5
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mSetToIdleByEditCancel:Z

    if-nez v1, :cond_a

    .line 690
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isMusicEchoCancellerSupported()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 691
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 699
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->getInstance()Lcom/vlingo/core/internal/associatedservice/UiFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/associatedservice/UiFocusManager;->remoteHasUiFocus()Z

    move-result v1

    if-nez v1, :cond_7

    .line 700
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->stopScoOnIdle()V

    .line 702
    :cond_7
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-eq v1, v2, :cond_8

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->isPaused()Z

    move-result v1

    if-nez v1, :cond_8

    .line 705
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isDrivingMode()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_9

    .line 706
    const-wide/16 v1, 0x5dc

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->scheduleStartSpotter(J)V

    .line 712
    :cond_8
    :goto_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    invoke-interface {v1}, Lcom/vlingo/core/internal/display/WakeLockManager;->releaseWakeLock()V

    goto/16 :goto_1

    .line 708
    :cond_9
    invoke-virtual {p0, v6, v7}, Lcom/vlingo/midas/gui/ControlFragment;->scheduleStartSpotter(J)V

    goto :goto_3

    .line 721
    :cond_a
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v1, v2, :cond_0

    .line 722
    invoke-virtual {p0, v6, v7}, Lcom/vlingo/midas/gui/ControlFragment;->scheduleStartSpotter(J)V

    goto/16 :goto_1

    .line 736
    :pswitch_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    invoke-interface {v1}, Lcom/vlingo/core/internal/display/WakeLockManager;->acquireWakeLock()V

    goto/16 :goto_1

    .line 740
    :pswitch_4
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->countTimer:Lcom/vlingo/midas/gui/CountTimer;

    if-eqz v1, :cond_c

    sget-boolean v1, Lcom/vlingo/midas/gui/ControlFragment;->startTimer:Z

    if-nez v1, :cond_c

    iget v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->timesTTSPlayed:I

    sget v2, Lcom/vlingo/midas/gui/ControlFragment;->maxTTSPlayCount:I

    if-ge v1, v2, :cond_c

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v1

    if-nez v1, :cond_b

    invoke-static {}, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen()Z

    move-result v1

    if-nez v1, :cond_c

    .line 741
    :cond_b
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->countTimer:Lcom/vlingo/midas/gui/CountTimer;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/CountTimer;->start()Landroid/os/CountDownTimer;

    .line 742
    sput-boolean v5, Lcom/vlingo/midas/gui/ControlFragment;->startTimer:Z

    .line 743
    iput-boolean v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->standardFlag:Z

    .line 745
    :cond_c
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    invoke-virtual {v1, v5}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->removeMessages(I)V

    .line 747
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_e

    sget-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isAfterTutorial:Z

    if-eqz v1, :cond_d

    sget-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialStartedFromIUX:Z

    if-eqz v1, :cond_e

    .line 749
    :cond_d
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->blinkAnim()V

    .line 751
    :cond_e
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 752
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->blinkAnim()V

    .line 754
    :cond_f
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->setButtonListening()V

    .line 756
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->disableHelpWidgetButton()V

    .line 757
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->resetThinkingState()V

    .line 773
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    invoke-interface {v1}, Lcom/vlingo/core/internal/display/WakeLockManager;->acquireWakeLock()V

    goto/16 :goto_1

    .line 778
    :pswitch_5
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->disableHelpWidgetButton()V

    .line 779
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->setButtonThinking()V

    .line 782
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->resetThinkingState()V

    .line 783
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->setThinkingAnimation()V

    .line 784
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->wakeLockManager:Lcom/vlingo/core/internal/display/WakeLockManager;

    invoke-interface {v1}, Lcom/vlingo/core/internal/display/WakeLockManager;->acquireWakeLock()V

    goto/16 :goto_1

    .line 787
    :pswitch_6
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 788
    iput-boolean v4, p0, Lcom/vlingo/midas/gui/ControlFragment;->standardFlag:Z

    .line 789
    sput-boolean v4, Lcom/vlingo/midas/gui/ControlFragment;->startTimer:Z

    .line 791
    :cond_10
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->resetThinkingState()V

    .line 792
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->setResponseMode()V

    goto/16 :goto_1

    .line 795
    :pswitch_7
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopSpotting()V

    goto/16 :goto_1

    .line 798
    :pswitch_8
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 799
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopSpotting()V

    .line 801
    :cond_11
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->resetThinkingState()V

    .line 802
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-eq v1, v2, :cond_12

    .line 803
    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/ControlFragment;->setButtonIdle(Z)V

    .line 805
    :cond_12
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 806
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    invoke-virtual {v1, v5}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->removeMessages(I)V

    goto/16 :goto_1

    .line 642
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public setTaskOnFinishGreetingTTS(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 1269
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTaskOnFinishGreetingTTS:Ljava/lang/Runnable;

    .line 1270
    return-void
.end method

.method protected setThinkingAnimation()V
    .locals 0

    .prologue
    .line 1205
    return-void
.end method

.method public setToIdle()V
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ControlFragment;->setButtonIdle(Z)V

    .line 289
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 290
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 291
    return-void
.end method

.method setToIdleByEditCancel(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 615
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mSetToIdleByEditCancel:Z

    .line 616
    return-void
.end method

.method public setToListening()V
    .locals 3

    .prologue
    .line 281
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 282
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    .line 283
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 285
    return-void
.end method

.method protected setmActivityCreated(Z)V
    .locals 0
    .param p1, "mActivityCreated"    # Z

    .prologue
    .line 1257
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragment;->mActivityCreated:Z

    .line 1258
    return-void
.end method

.method public abstract showRMSChange(I)V
.end method

.method public abstract showSpectrum([I)V
.end method

.method public startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 10
    .param p1, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    const/4 v9, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 830
    const/4 v4, 0x0

    .line 832
    .local v4, "noStartRecognition":Z
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->isPaused()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 833
    const/4 v4, 0x1

    .line 848
    :cond_0
    :goto_0
    if-eqz v4, :cond_6

    .line 849
    iget-object v7, p0, Lcom/vlingo/midas/gui/ControlFragment;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "startRecognition() return, micStream = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p1, :cond_5

    :goto_1
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    if-eqz p1, :cond_1

    .line 852
    :try_start_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 915
    :cond_1
    :goto_2
    return-void

    .line 834
    :cond_2
    iget-boolean v7, p0, Lcom/vlingo/midas/gui/ControlFragment;->m_isRecognitionPostBTConnect:Z

    if-eqz v7, :cond_3

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 837
    const/4 v4, 0x1

    goto :goto_0

    .line 838
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->isActivityCreated()Z

    move-result v7

    if-nez v7, :cond_4

    .line 841
    const/4 v4, 0x1

    goto :goto_0

    .line 842
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCoverOpened()Z

    move-result v7

    if-nez v7, :cond_0

    .line 845
    const/4 v4, 0x1

    goto :goto_0

    :cond_5
    move v5, v6

    .line 849
    goto :goto_1

    .line 853
    :catch_0
    move-exception v1

    .line 854
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 860
    .end local v1    # "e":Ljava/io/IOException;
    :cond_6
    iget-object v7, p0, Lcom/vlingo/midas/gui/ControlFragment;->callback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    if-eqz v7, :cond_7

    .line 861
    iget-object v7, p0, Lcom/vlingo/midas/gui/ControlFragment;->callback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    invoke-interface {v7}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;->onRecognitionStarted()V

    .line 863
    :cond_7
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 866
    invoke-direct {p0, v5}, Lcom/vlingo/midas/gui/ControlFragment;->setStartRecoOnSpotterStop(Z)V

    .line 867
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopSpotting()V

    .line 869
    if-eqz p1, :cond_1

    .line 871
    :try_start_1
    invoke-virtual {p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 872
    :catch_1
    move-exception v1

    .line 873
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 883
    .end local v1    # "e":Ljava/io/IOException;
    :cond_8
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->stopSpotting()V

    .line 885
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 886
    .local v0, "baseContext":Landroid/content/Context;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v3

    .line 887
    .local v3, "isBtHeadsetConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v2

    .line 888
    .local v2, "isBluetoothAudioSupported":Z
    if-ne v3, v5, :cond_9

    if-eq v2, v5, :cond_b

    .line 889
    :cond_9
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v7

    const/4 v8, 0x3

    invoke-virtual {v7, v8, v9}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 900
    :goto_3
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->startUserFlow(Lcom/vlingo/core/internal/audio/MicrophoneStream;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 901
    if-ne v3, v5, :cond_a

    if-eq v2, v5, :cond_1

    .line 902
    :cond_a
    sget-object v5, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    goto :goto_2

    .line 891
    :cond_b
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v7

    const/4 v8, 0x6

    invoke-virtual {v7, v8, v9}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 897
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->startScoOnStartRecognition()V

    goto :goto_3

    .line 904
    :cond_c
    sget-object v7, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 905
    iget-object v7, p0, Lcom/vlingo/midas/gui/ControlFragment;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "startRecognition() return false from startUserFlow(), micStream = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p1, :cond_d

    :goto_4
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    if-eqz p1, :cond_1

    .line 908
    :try_start_2
    invoke-virtual {p1}, Lcom/vlingo/core/internal/audio/MicrophoneStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_2

    .line 909
    :catch_2
    move-exception v1

    .line 910
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .end local v1    # "e":Ljava/io/IOException;
    :cond_d
    move v5, v6

    .line 905
    goto :goto_4
.end method

.method public startSpotting()V
    .locals 3

    .prologue
    .line 1048
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ControlFragment;->setPrompt()V

    .line 1049
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$6;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ControlFragment$6;-><init>(Lcom/vlingo/midas/gui/ControlFragment;)V

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 1055
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mKm:Landroid/app/KeyguardManager;

    if-eqz v0, :cond_2

    .line 1057
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mKm:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string/jumbo v0, "should_wakeup_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1093
    :cond_1
    :goto_0
    return-void

    .line 1066
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1069
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragment$7;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/ControlFragment$7;-><init>(Lcom/vlingo/midas/gui/ControlFragment;)V

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1087
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected stopSpotting()V
    .locals 2

    .prologue
    .line 1099
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mHandler:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->removeMessages(I)V

    .line 1106
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 1107
    return-void
.end method

.method public stopTTS()V
    .locals 1

    .prologue
    .line 503
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 505
    :catch_0
    move-exception v0

    goto :goto_0
.end method
