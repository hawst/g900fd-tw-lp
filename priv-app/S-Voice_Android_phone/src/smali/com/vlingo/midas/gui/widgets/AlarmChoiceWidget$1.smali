.class synthetic Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;
.super Ljava/lang/Object;
.source "AlarmChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$vlingo$core$internal$dialogmanager$util$WidgetUtil$WidgetKey:[I

.field static final synthetic $SwitchMap$com$vlingo$core$internal$util$Alarm$AlarmStatus:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 508
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->values()[Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;->$SwitchMap$com$vlingo$core$internal$dialogmanager$util$WidgetUtil$WidgetKey:[I

    :try_start_0
    sget-object v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;->$SwitchMap$com$vlingo$core$internal$dialogmanager$util$WidgetUtil$WidgetKey:[I

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowAlarmChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;->$SwitchMap$com$vlingo$core$internal$dialogmanager$util$WidgetUtil$WidgetKey:[I

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    .line 301
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->values()[Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;->$SwitchMap$com$vlingo$core$internal$util$Alarm$AlarmStatus:[I

    :try_start_2
    sget-object v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;->$SwitchMap$com$vlingo$core$internal$util$Alarm$AlarmStatus:[I

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    :try_start_3
    sget-object v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;->$SwitchMap$com$vlingo$core$internal$util$Alarm$AlarmStatus:[I

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->SNOOZE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    :try_start_4
    sget-object v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;->$SwitchMap$com$vlingo$core$internal$util$Alarm$AlarmStatus:[I

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->SUBDUE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;->$SwitchMap$com$vlingo$core$internal$util$Alarm$AlarmStatus:[I

    sget-object v1, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->INACTIVE:Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    move-exception v0

    goto :goto_5

    :catch_1
    move-exception v0

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_2

    .line 508
    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v0

    goto :goto_0
.end method
