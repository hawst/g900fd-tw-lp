.class Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ControlFragmentEWYSHelp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FocusChangeBroadcastReceiver"
.end annotation


# instance fields
.field private audioFocusLossTask:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$1;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;)V

    return-void
.end method

.method private resetTask()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;->audioFocusLossTask:Ljava/lang/Runnable;

    .line 139
    return-void
.end method

.method private runAudioFocusLossTask()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;->audioFocusLossTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;->audioFocusLossTask:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 144
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;->resetTask()V

    .line 146
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 95
    const-string/jumbo v3, "com.vlingo.client.app.extra.FOCUS_CHANGE"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 96
    .local v0, "focusChange":I
    const-string/jumbo v3, "com.vlingo.client.app.extra.FOCUS_CHANGE_FROM"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "focusChangeFrom":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 123
    :pswitch_1
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v3

    sget-object v4, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    if-ne v3, v4, :cond_2

    .line 124
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 126
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;->runAudioFocusLossTask()V

    goto :goto_0

    .line 106
    :pswitch_2
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 107
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 108
    .local v2, "frActivity":Landroid/support/v4/app/FragmentActivity;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 101
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public setOnAudioFocusLossTask(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$FocusChangeBroadcastReceiver;->audioFocusLossTask:Ljava/lang/Runnable;

    .line 135
    return-void
.end method
