.class public Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;
.super Landroid/widget/RelativeLayout;
.source "SmartNotificationWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;
    }
.end annotation


# static fields
.field private static final CATEGORY_SIZE:I = 0x3


# instance fields
.field Birthday:Ljava/lang/String;

.field public alldayCounting:I

.field alldayExtra1:I

.field alldayExtra2:I

.field alldaymin:I

.field alldaymin2:I

.field arr:[I

.field arr1:[I

.field bcount:I

.field bdayEvents:I

.field birthdayLen:I

.field count:I

.field eventList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/gui/widgets/ScheduleEventData;",
            ">;"
        }
    .end annotation
.end field

.field flag1:I

.field item:I

.field len:I

.field list1:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private lvContainer:Landroid/widget/LinearLayout;

.field private lvContainer2:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

.field scheduledEvents:I

.field scount:I

.field se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

.field sharedPrefStopCount1:I

.field sharedPrefStopCount2:I

.field sharedPrefStopCount3:I

.field temp:Ljava/lang/String;

.field temp1:Ljava/lang/String;

.field temp2:Ljava/lang/String;

.field public totalAllday:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bdayEvents:I

    .line 32
    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->scheduledEvents:I

    .line 33
    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->alldayCounting:I

    .line 34
    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->totalAllday:I

    .line 36
    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->flag1:I

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bcount:I

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->scount:I

    .line 39
    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->sharedPrefStopCount1:I

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->sharedPrefStopCount2:I

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->sharedPrefStopCount3:I

    .line 40
    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->alldayExtra1:I

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->alldayExtra2:I

    .line 45
    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->count:I

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->item:I

    .line 48
    iput v1, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->alldaymin:I

    iput v1, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->alldaymin2:I

    .line 52
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->mContext:Landroid/content/Context;

    .line 53
    new-instance v0, Lcom/vlingo/midas/ui/PackageInfoProvider;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private addListItemsView(Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;II)Z
    .locals 6
    .param p1, "adapter"    # Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;
    .param p2, "i"    # I
    .param p3, "count"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 184
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 185
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p1, p2, v5, v5}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 186
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    const/4 v3, 0x3

    if-le p3, v3, :cond_1

    .line 206
    :cond_0
    :goto_0
    return v2

    .line 189
    :cond_1
    iget v3, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->flag1:I

    if-nez v3, :cond_3

    .line 191
    iget v3, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->scount:I

    if-lez v3, :cond_2

    .line 193
    sget v3, Lcom/vlingo/midas/R$id;->linear_schedule:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 195
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->lvContainer2:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 206
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 199
    :cond_3
    iget v3, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bcount:I

    if-lez v3, :cond_4

    .line 201
    sget v3, Lcom/vlingo/midas/R$id;->widget_smart_title:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 202
    sget v3, Lcom/vlingo/midas/R$id;->widget_smart_list_bday:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 204
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->lvContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 58
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 59
    sget v0, Lcom/vlingo/midas/R$id;->widget_smart_list:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->lvContainer2:Landroid/widget/LinearLayout;

    .line 60
    sget v0, Lcom/vlingo/midas/R$id;->widget_smart_list_bday:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->lvContainer:Landroid/widget/LinearLayout;

    .line 61
    sget v0, Lcom/vlingo/midas/R$id;->widget_smart_title:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->personal_briefing_title_birthday:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    sget v0, Lcom/vlingo/midas/R$id;->widget_smart_title1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->personal_briefing_title_schedule:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    return-void
.end method

.method public setAddView()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    .line 128
    :try_start_0
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->lvContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 129
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->lvContainer2:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 131
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$string;->title_birthday:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->Birthday:Ljava/lang/String;

    .line 132
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->Birthday:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    iput v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->birthdayLen:I

    .line 134
    sget-object v5, Lcom/vlingo/midas/gui/DialogFragment;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    .line 135
    sget-object v5, Lcom/vlingo/midas/gui/DialogFragment;->eventList:Ljava/util/ArrayList;

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    .line 136
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->list1:Ljava/util/ArrayList;

    if-nez v5, :cond_0

    .line 137
    sget-object v5, Lcom/vlingo/midas/gui/DialogFragment;->birthdayList:Ljava/util/ArrayList;

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->list1:Ljava/util/ArrayList;

    .line 138
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->setCalendar()V

    .line 139
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->setSchedule()V

    .line 141
    new-instance v0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v5}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;Ljava/util/ArrayList;)V

    .line 142
    .local v0, "adapter":Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;->getCount()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 143
    const/4 v5, 0x0

    iput v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->flag1:I

    .line 144
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    .line 145
    .local v1, "data":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getType()I

    move-result v4

    .line 146
    .local v4, "type":I
    packed-switch v4, :pswitch_data_0

    .line 170
    :cond_1
    :goto_1
    :pswitch_0
    iget v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bdayEvents:I

    if-ne v5, v10, :cond_4

    iget v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->scheduledEvents:I

    if-ne v5, v10, :cond_4

    .line 174
    .end local v1    # "data":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .end local v4    # "type":I
    :cond_2
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->list1:Ljava/util/ArrayList;

    .line 181
    .end local v0    # "adapter":Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;
    .end local v3    # "i":I
    :goto_2
    return-void

    .line 149
    .restart local v0    # "adapter":Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;
    .restart local v1    # "data":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v3    # "i":I
    .restart local v4    # "type":I
    :pswitch_1
    const/4 v5, 0x1

    iput v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->flag1:I

    .line 150
    iget v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bcount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bcount:I

    .line 151
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->list1:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 152
    iget v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bdayEvents:I

    if-ge v5, v9, :cond_1

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->list1:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget v6, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bdayEvents:I

    if-le v5, v6, :cond_1

    iget v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bdayEvents:I

    invoke-direct {p0, v0, v3, v5}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->addListItemsView(Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;II)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 153
    iget v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bdayEvents:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->bdayEvents:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 176
    .end local v0    # "adapter":Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;
    .end local v1    # "data":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .end local v3    # "i":I
    .end local v4    # "type":I
    :catch_0
    move-exception v2

    .line 178
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v5, "smartNotification"

    const-string/jumbo v6, "Exception in getView method"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 159
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "adapter":Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;
    .restart local v1    # "data":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v3    # "i":I
    .restart local v4    # "type":I
    :pswitch_2
    :try_start_1
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getMill()J

    move-result-wide v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-gtz v5, :cond_3

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getAllday()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 161
    :cond_3
    iget v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->scount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->scount:I

    .line 162
    iget v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->scheduledEvents:I

    if-ge v5, v9, :cond_1

    iget v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->scheduledEvents:I

    invoke-direct {p0, v0, v3, v5}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->addListItemsView(Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget$SmartNotificationListAdapter;II)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 163
    iget v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->scheduledEvents:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->scheduledEvents:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 142
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method setCalendar()V
    .locals 9

    .prologue
    .line 69
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-direct {v0, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 70
    .local v0, "date":Ljava/util/Date;
    const-string/jumbo v7, "EEEE"

    invoke-static {v7, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 71
    .local v2, "dayOfTheWeek":Ljava/lang/String;
    const-string/jumbo v7, "MMMM"

    invoke-static {v7, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 72
    .local v3, "stringMonth":Ljava/lang/String;
    const-string/jumbo v7, "dd"

    invoke-static {v7, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 73
    .local v1, "day":Ljava/lang/String;
    sget v7, Lcom/vlingo/midas/R$id;->widget_smart_date:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 74
    .local v4, "text":Landroid/widget/TextView;
    sget v7, Lcom/vlingo/midas/R$id;->widget_you_can_smart_say_day:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 75
    .local v5, "textDay":Landroid/widget/TextView;
    sget v7, Lcom/vlingo/midas/R$id;->widget_you_can_smart_say_month:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 76
    .local v6, "textMonth":Landroid/widget/TextView;
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    return-void
.end method

.method setSchedule()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    .line 86
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v7}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getMill()J

    move-result-wide v4

    .line 88
    .local v4, "maxVal":J
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v0, v7, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_4

    .line 91
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v7}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getType()I

    move-result v7

    if-eq v7, v9, :cond_1

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v7}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getAllday()Z

    move-result v7

    if-nez v7, :cond_1

    .line 94
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v7}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getMill()J

    move-result-wide v4

    .line 95
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    .line 96
    .local v2, "max":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    move v3, v0

    .line 97
    .local v3, "maxItem":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "j":I
    :goto_1
    if-ltz v1, :cond_0

    .line 99
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v7}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getType()I

    move-result v7

    if-eq v7, v9, :cond_3

    .line 101
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v7}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getType()I

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v7}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getAllday()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 103
    move v3, v1

    .line 104
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "max":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    check-cast v2, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    .line 115
    .restart local v2    # "max":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :cond_0
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    .line 116
    .local v6, "temp":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 117
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v3, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 88
    .end local v1    # "j":I
    .end local v2    # "max":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .end local v3    # "maxItem":I
    .end local v6    # "temp":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 107
    .restart local v1    # "j":I
    .restart local v2    # "max":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v3    # "maxItem":I
    :cond_2
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v7}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getMill()J

    move-result-wide v7

    cmp-long v7, v7, v4

    if-lez v7, :cond_3

    .line 109
    move v3, v1

    .line 110
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "max":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    check-cast v2, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    .line 111
    .restart local v2    # "max":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-virtual {v7}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getMill()J

    move-result-wide v4

    .line 97
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 122
    .end local v1    # "j":I
    .end local v2    # "max":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .end local v3    # "maxItem":I
    :cond_4
    return-void
.end method
