.class Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ControlFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ControlFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TTSBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ControlFragment;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/ControlFragment;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/ControlFragment;Lcom/vlingo/midas/gui/ControlFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/ControlFragment;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/ControlFragment$1;

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ControlFragment;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 221
    if-nez p2, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentData;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->DIALOG_BUSY:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-eq v1, v2, :cond_0

    .line 227
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment$TTSBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    # invokes: Lcom/vlingo/midas/gui/ControlFragment;->setMicStateToListening()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/ControlFragment;->access$500(Lcom/vlingo/midas/gui/ControlFragment;)V

    goto :goto_0
.end method
