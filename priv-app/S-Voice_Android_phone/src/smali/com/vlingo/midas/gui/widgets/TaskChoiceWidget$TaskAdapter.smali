.class Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;
.super Landroid/widget/BaseAdapter;
.source "TaskChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TaskAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private mEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p3, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleTask;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 89
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 90
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->mEvents:Ljava/util/List;

    .line 91
    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->context:Landroid/content/Context;

    .line 92
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 96
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->mEvents:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 97
    .local v0, "toReturn":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;->getLimitedCount(I)I

    move-result v1

    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->mEvents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 107
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 114
    if-nez p2, :cond_1

    .line 115
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v9, Lcom/vlingo/midas/R$layout;->item_schedule_choice:I

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 117
    new-instance v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;)V

    .line 118
    .local v6, "holder":Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;
    sget v8, Lcom/vlingo/midas/R$id;->schedule_title:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 119
    sget v8, Lcom/vlingo/midas/R$id;->schedule_date:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->date:Landroid/widget/TextView;

    .line 120
    sget v8, Lcom/vlingo/midas/R$id;->schedule_time:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->time:Landroid/widget/TextView;

    .line 121
    sget v8, Lcom/vlingo/midas/R$id;->divider:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->divider:Landroid/view/View;

    .line 123
    invoke-virtual {p2, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 128
    :goto_0
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->mEvents:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 131
    .local v5, "event":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    invoke-virtual {v5}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 132
    iget-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->title:Landroid/widget/TextView;

    sget v9, Lcom/vlingo/midas/R$string;->my_event:I

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 137
    :goto_1
    invoke-virtual {v5}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBegin()J

    move-result-wide v0

    .line 139
    .local v0, "begin":J
    const-wide/16 v8, 0x0

    cmp-long v8, v0, v8

    if-eqz v8, :cond_4

    .line 140
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v2

    .line 141
    .local v2, "currentLocale":Ljava/util/Locale;
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 142
    .local v4, "dateValue":Ljava/util/Date;
    invoke-static {v4, v2}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetDate(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 143
    .local v3, "dateString":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_3

    .line 144
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 145
    invoke-virtual {v5}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v8

    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v10

    invoke-static {v8, v9, v10, v2}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetTime(JZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 146
    .local v7, "reminderString":Ljava/lang/String;
    iget-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->time:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    .end local v7    # "reminderString":Ljava/lang/String;
    :goto_2
    iget-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    .end local v2    # "currentLocale":Ljava/util/Locale;
    .end local v3    # "dateString":Ljava/lang/String;
    .end local v4    # "dateValue":Ljava/util/Date;
    :goto_3
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 156
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->mEvents:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne p1, v8, :cond_0

    .line 157
    iget-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 159
    :cond_0
    sget v8, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v8}, Landroid/view/View;->setBackgroundResource(I)V

    .line 171
    :goto_4
    new-instance v8, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$1;

    invoke-direct {v8, p0, p1}, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;I)V

    invoke-virtual {p2, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    return-object p2

    .line 125
    .end local v0    # "begin":J
    .end local v5    # "event":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .end local v6    # "holder":Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;

    .restart local v6    # "holder":Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;
    goto/16 :goto_0

    .line 134
    .restart local v5    # "event":Lcom/vlingo/core/internal/schedule/ScheduleTask;
    :cond_2
    iget-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 148
    .restart local v0    # "begin":J
    .restart local v2    # "currentLocale":Ljava/util/Locale;
    .restart local v3    # "dateString":Ljava/lang/String;
    .restart local v4    # "dateValue":Ljava/util/Date;
    :cond_3
    iget-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->time:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 152
    .end local v2    # "currentLocale":Ljava/util/Locale;
    .end local v3    # "dateString":Ljava/lang/String;
    .end local v4    # "dateValue":Ljava/util/Date;
    :cond_4
    iget-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->time:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 161
    :cond_5
    if-nez p1, :cond_6

    .line 162
    sget v8, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    invoke-virtual {p2, v8}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4

    .line 163
    :cond_6
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->mEvents:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne p1, v8, :cond_7

    .line 164
    iget-object v8, v6, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 165
    sget v8, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v8}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4

    .line 167
    :cond_7
    sget v8, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v8}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4
.end method
