.class public abstract Lcom/vlingo/midas/gui/Widget;
.super Landroid/widget/RelativeLayout;
.source "Widget.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Type:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/RelativeLayout;",
        "Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener;"
    }
.end annotation


# static fields
.field public static multiWidgetItemsUltimateMax:I


# instance fields
.field private final ANIMATE_TRANSLATE_UP_DURATION:I

.field private haveBeenAnimatedBefore:Z

.field private isPlayingAudio:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getMultiWidgetItemsUltimateMax()I

    move-result v0

    sput v0, Lcom/vlingo/midas/gui/Widget;->multiWidgetItemsUltimateMax:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const/16 v0, 0x362

    iput v0, p0, Lcom/vlingo/midas/gui/Widget;->ANIMATE_TRANSLATE_UP_DURATION:I

    .line 41
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/Widget;->isPlayingAudio:Z

    .line 42
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/Widget;->haveBeenAnimatedBefore:Z

    .line 46
    return-void
.end method

.method private disableEnableControls(ZLandroid/view/ViewGroup;)V
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "vg"    # Landroid/view/ViewGroup;

    .prologue
    .line 106
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v1, 0x0

    .line 107
    .local v1, "countChild":I
    if-eqz p2, :cond_0

    .line 108
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 110
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 111
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 112
    .local v0, "child":Landroid/view/View;
    instance-of v3, v0, Landroid/widget/RatingBar;

    if-eqz v3, :cond_2

    .line 116
    :goto_1
    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    .line 117
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "child":Landroid/view/View;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/gui/Widget;->disableEnableControls(ZLandroid/view/ViewGroup;)V

    .line 110
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 115
    .restart local v0    # "child":Landroid/view/View;
    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    .line 120
    .end local v0    # "child":Landroid/view/View;
    :cond_3
    return-void
.end method

.method public static getMultiWidgetItemsUltimateMax()I
    .locals 1

    .prologue
    .line 56
    sget v0, Lcom/vlingo/midas/gui/Widget;->multiWidgetItemsUltimateMax:I

    return v0
.end method


# virtual methods
.method protected getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "Original"    # Ljava/lang/CharSequence;
    .param p2, "widgetState"    # I

    .prologue
    .line 88
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    move-object v0, p1

    .line 89
    .local v0, "talkbackString":Ljava/lang/CharSequence;
    packed-switch p2, :pswitch_data_0

    .line 100
    :goto_0
    :pswitch_0
    return-object v0

    .line 93
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/Widget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->talkback_bubble_editable:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    goto :goto_0

    .line 96
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/Widget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->talkback_widget_detail:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public abstract initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TType;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation
.end method

.method public isPlayingAudio()Z
    .locals 1

    .prologue
    .line 52
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/Widget;->isPlayingAudio:Z

    return v0
.end method

.method public isPreviousSameWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 259
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public abstract isTranslated()Z
.end method

.method public abstract isWidgetReplaceable()Z
.end method

.method protected measureListviewHeight(Landroid/widget/ListView;IZ)V
    .locals 10
    .param p1, "listView"    # Landroid/widget/ListView;
    .param p2, "heightResource"    # I
    .param p3, "doUpdate"    # Z

    .prologue
    .line 214
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 216
    .local v7, "v":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 218
    .local v5, "params":Landroid/view/ViewGroup$LayoutParams;
    const/4 v2, 0x0

    .line 219
    .local v2, "height":F
    invoke-virtual {p1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    .line 220
    .local v1, "count":I
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 221
    .local v3, "listAdapter":Landroid/widget/ListAdapter;
    if-eqz v3, :cond_0

    .line 222
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    .line 223
    .local v4, "newCount":I
    move v1, v4

    .line 226
    .end local v4    # "newCount":I
    :cond_0
    if-eqz v7, :cond_2

    .line 227
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v8

    mul-int/2addr v8, v1

    int-to-float v2, v8

    .line 232
    :goto_0
    invoke-virtual {p1}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v6

    .line 233
    .local v6, "topPadding":I
    invoke-virtual {p1}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v0

    .line 235
    .local v0, "bottomPadding":I
    int-to-float v8, v6

    add-float/2addr v8, v2

    int-to-float v9, v0

    add-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 237
    invoke-virtual {p1}, Landroid/widget/ListView;->getHeight()I

    move-result v8

    iget v9, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v8, v9, :cond_1

    .line 238
    invoke-virtual {p1, v5}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 239
    if-eqz p3, :cond_1

    .line 240
    iget v8, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v9, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v8, v9}, Lcom/vlingo/midas/gui/Widget;->setMeasuredDimension(II)V

    .line 243
    :cond_1
    return-void

    .line 229
    .end local v0    # "bottomPadding":I
    .end local v6    # "topPadding":I
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/Widget;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    int-to-float v9, v1

    mul-float v2, v8, v9

    goto :goto_0
.end method

.method public mustBeShown()Z
    .locals 1

    .prologue
    .line 76
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public onAnimationEnd()V
    .locals 0

    .prologue
    .line 250
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    return-void
.end method

.method public onAnimationStart()V
    .locals 0

    .prologue
    .line 249
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    return-void
.end method

.method public onRecognitionStarted()V
    .locals 0

    .prologue
    .line 208
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    return-void
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 204
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/Widget;->isPlayingAudio:Z

    .line 205
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 194
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/Widget;->isPlayingAudio:Z

    .line 195
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 199
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/Widget;->isPlayingAudio:Z

    .line 200
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 189
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/Widget;->isPlayingAudio:Z

    .line 190
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 123
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 125
    if-lez p2, :cond_1

    .line 126
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/Widget;->isTranslated()Z

    move-result v0

    if-nez v0, :cond_1

    .line 135
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_1

    .line 136
    :cond_0
    invoke-virtual {p0, p0}, Lcom/vlingo/midas/gui/Widget;->startAnimationTranslate(Landroid/view/View;)V

    .line 141
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 146
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    return-void
.end method

.method public processSystemMessage(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 247
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    return-void
.end method

.method protected promptUser(Ljava/lang/String;)V
    .locals 7
    .param p1, "_msg"    # Ljava/lang/String;

    .prologue
    .line 150
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const-string/jumbo v0, "UserPrompt: present context does not support user prompting"

    .line 154
    .local v0, "no_TTS":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/Widget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string/jumbo v3, "promptUser"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 155
    .local v1, "tts":Ljava/lang/reflect/Method;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/Widget;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    .end local v1    # "tts":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 165
    :catch_0
    move-exception v2

    goto :goto_0

    .line 162
    :catch_1
    move-exception v2

    goto :goto_0

    .line 159
    :catch_2
    move-exception v2

    goto :goto_0

    .line 156
    :catch_3
    move-exception v2

    goto :goto_0
.end method

.method public retire()V
    .locals 1

    .prologue
    .line 63
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v0, 0x0

    invoke-direct {p0, v0, p0}, Lcom/vlingo/midas/gui/Widget;->disableEnableControls(ZLandroid/view/ViewGroup;)V

    .line 64
    return-void
.end method

.method public startAnimationTranslate(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .local p0, "this":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TType;>;"
    const/4 v5, 0x1

    .line 173
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/Widget;->haveBeenAnimatedBefore:Z

    if-nez v1, :cond_0

    .line 174
    const-string/jumbo v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x0

    aput v3, v2, v5

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 175
    .local v0, "translateWidgetAnimation":Landroid/animation/ValueAnimator;
    const-wide/16 v1, 0x362

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 176
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 177
    iput-boolean v5, p0, Lcom/vlingo/midas/gui/Widget;->haveBeenAnimatedBefore:Z

    .line 179
    .end local v0    # "translateWidgetAnimation":Landroid/animation/ValueAnimator;
    :cond_0
    return-void
.end method
