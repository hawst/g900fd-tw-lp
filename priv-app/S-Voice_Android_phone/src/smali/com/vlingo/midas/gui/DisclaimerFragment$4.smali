.class Lcom/vlingo/midas/gui/DisclaimerFragment$4;
.super Ljava/lang/Object;
.source "DisclaimerFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/DisclaimerFragment;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DisclaimerFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DisclaimerFragment;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment$4;->this$0:Lcom/vlingo/midas/gui/DisclaimerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 294
    iget-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment$4;->this$0:Lcom/vlingo/midas/gui/DisclaimerFragment;

    # getter for: Lcom/vlingo/midas/gui/DisclaimerFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->access$000(Lcom/vlingo/midas/gui/DisclaimerFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 295
    if-eqz p2, :cond_0

    .line 296
    iget-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment$4;->this$0:Lcom/vlingo/midas/gui/DisclaimerFragment;

    const/4 v1, 0x1

    # setter for: Lcom/vlingo/midas/gui/DisclaimerFragment;->isAgreeBtnChecked:Z
    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/DisclaimerFragment;->access$102(Lcom/vlingo/midas/gui/DisclaimerFragment;Z)Z

    .line 297
    iget-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment$4;->this$0:Lcom/vlingo/midas/gui/DisclaimerFragment;

    # invokes: Lcom/vlingo/midas/gui/DisclaimerFragment;->screenWhenConfirmed()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->access$200(Lcom/vlingo/midas/gui/DisclaimerFragment;)V

    .line 302
    :goto_0
    return-void

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment$4;->this$0:Lcom/vlingo/midas/gui/DisclaimerFragment;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/gui/DisclaimerFragment;->isAgreeBtnChecked:Z
    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/DisclaimerFragment;->access$102(Lcom/vlingo/midas/gui/DisclaimerFragment;Z)Z

    .line 300
    iget-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment$4;->this$0:Lcom/vlingo/midas/gui/DisclaimerFragment;

    # invokes: Lcom/vlingo/midas/gui/DisclaimerFragment;->screenWhenDisagree()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->access$300(Lcom/vlingo/midas/gui/DisclaimerFragment;)V

    goto :goto_0
.end method
