.class Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;
.super Landroid/os/Handler;
.source "ControlFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ControlFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ControlFragmentHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/ControlFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ControlFragment;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/ControlFragment;

    .prologue
    .line 251
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 252
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 253
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 256
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ControlFragment;

    .line 257
    .local v0, "o":Lcom/vlingo/midas/gui/ControlFragment;
    if-eqz v0, :cond_0

    .line 258
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 260
    :pswitch_0
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->isPaused()Z

    move-result v1

    if-nez v1, :cond_0

    .line 261
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->startSpotting()V

    goto :goto_0

    .line 266
    :pswitch_1
    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/gui/ControlFragment;->count:I
    invoke-static {v1}, Lcom/vlingo/midas/gui/ControlFragment;->access$702(I)I

    .line 267
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    const-string/jumbo v1, "SVoice::ControlFragment"

    const-string/jumbo v2, "Finishing SVoice as 5 seconds mini mode timer is expired!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
