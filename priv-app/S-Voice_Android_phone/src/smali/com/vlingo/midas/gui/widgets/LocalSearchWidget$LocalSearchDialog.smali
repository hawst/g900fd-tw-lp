.class Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;
.super Landroid/app/Dialog;
.source "LocalSearchWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalSearchDialog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    .line 78
    invoke-direct {p0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 79
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 83
    const-string/jumbo v0, "key_popup_window_opened"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 84
    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    .line 85
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 89
    const-string/jumbo v0, "key_popup_window_opened"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 90
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 91
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 95
    const-string/jumbo v0, "key_popup_window_opened"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 96
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;
    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$002(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;)Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    .line 97
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 98
    return-void
.end method
