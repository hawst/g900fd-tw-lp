.class public Lcom/vlingo/midas/gui/widgets/TaskWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "TaskWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/schedule/ScheduleTask;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnTouchListener;"
    }
.end annotation


# instance fields
.field private button_divider:Landroid/view/View;

.field private contentString:Ljava/lang/String;

.field private isClicked:Z

.field isComplete:Z

.field isSaveButton:Z

.field private lookupMode:Z

.field private mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mBottomReminder:Landroid/view/View;

.field private mCancel:Lcom/vlingo/midas/gui/customviews/Button;

.field private mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

.field private mDate:Landroid/widget/TextView;

.field private mReminder:Landroid/widget/TextView;

.field private mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

.field private mTitle:Landroid/widget/TextView;

.field private taskContainer:Landroid/widget/LinearLayout;

.field private task_button:Landroid/widget/LinearLayout;

.field private task_row_dueto:Landroid/widget/LinearLayout;

.field private task_row_reminder:Landroid/widget/LinearLayout;

.field private task_row_title:Landroid/widget/LinearLayout;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->isSaveButton:Z

    .line 64
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->lookupMode:Z

    .line 65
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->isClicked:Z

    .line 70
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->contentString:Ljava/lang/String;

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/TaskWidget;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/TaskWidget;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/TaskWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/TaskWidget;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    return-object v0
.end method

.method private getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "month"    # Ljava/lang/String;

    .prologue
    .line 377
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->cradle_month:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    .end local p1    # "month":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "month":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->cradle_month:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private launchTask()V
    .locals 4

    .prologue
    .line 341
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.INSERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 342
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "vnd.android.cursor.item/event"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 343
    const-string/jumbo v1, "task"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 344
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    const-string/jumbo v1, "beginTime"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBegin()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 346
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 347
    return-void
.end method

.method private launchTaskDetail()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 351
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getEventID()J

    move-result-wide v1

    cmp-long v1, v1, v4

    if-gtz v1, :cond_0

    .line 373
    :goto_0
    return-void

    .line 354
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 355
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 356
    const-string/jumbo v1, "com.android.calendar"

    const-string/jumbo v2, "com.android.calendar.detail.TaskInfoActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 361
    :goto_1
    const-string/jumbo v1, "vnd.android.cursor.item/event"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getEventID()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 363
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    const-string/jumbo v1, "duedate"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBegin()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 365
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v1

    cmp-long v1, v1, v4

    if-gtz v1, :cond_2

    .line 366
    const-string/jumbo v1, "reminder"

    const-string/jumbo v2, "Off"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    :goto_2
    const-string/jumbo v1, "groupid"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getGroupid()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 371
    const-string/jumbo v1, "description"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 359
    :cond_1
    const-string/jumbo v1, "com.android.calendar"

    const-string/jumbo v2, "com.android.calendar.task.TaskInfoActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 368
    :cond_2
    const-string/jumbo v1, "reminderdatetime"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_2
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/schedule/ScheduleTask;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 26
    .param p1, "task"    # Lcom/vlingo/core/internal/schedule/ScheduleTask;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 141
    if-nez p1, :cond_0

    .line 298
    :goto_0
    return-void

    .line 143
    :cond_0
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/widgets/TaskWidget;->lookupMode:Z

    .line 144
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTask:Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .line 145
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 146
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getCompleted()Z

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/widgets/TaskWidget;->isComplete:Z

    .line 148
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getTitle()Ljava/lang/String;

    move-result-object v20

    .line 149
    .local v20, "title":Ljava/lang/String;
    if-eqz v20, :cond_1

    .line 150
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->isComplete:Z

    move/from16 v23, v0

    if-eqz v23, :cond_5

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTitle:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTitle:Landroid/widget/TextView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v24

    or-int/lit8 v24, v24, 0x10

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTitle:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    :cond_1
    :goto_1
    const-wide/16 v7, 0x0

    .line 162
    .local v7, "dueDate":J
    sget-object v23, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    move-object/from16 v0, p3

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_2

    sget-object v23, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    move-object/from16 v0, p3

    move-object/from16 v1, v23

    if-ne v0, v1, :cond_6

    .line 163
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getBegin()J

    move-result-wide v7

    .line 167
    :goto_2
    const-string/jumbo v5, ""

    .line 168
    .local v5, "dateDescription":Ljava/lang/String;
    const-string/jumbo v16, ""

    .line 170
    .local v16, "reminderDateDescription":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    .line 171
    .local v3, "currentLocale":Ljava/util/Locale;
    const-wide/16 v23, 0x0

    cmp-long v23, v7, v23

    if-eqz v23, :cond_3

    .line 172
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    const-string/jumbo v24, "date_format"

    invoke-static/range {v23 .. v24}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 174
    .local v12, "phone_date_format":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 175
    .local v9, "dueDateCal":Ljava/util/Calendar;
    new-instance v23, Ljava/util/Date;

    move-object/from16 v0, v23

    invoke-direct {v0, v7, v8}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 177
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    .line 178
    .local v22, "year":Ljava/lang/String;
    const/16 v23, 0x5

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->get(I)I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 179
    .local v6, "day":Ljava/lang/String;
    const/16 v23, 0x2

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1, v3}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v11

    .line 180
    .local v11, "month":Ljava/lang/String;
    const/16 v23, 0x7

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1, v3}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v21

    .line 181
    .local v21, "week":Ljava/lang/String;
    const/16 v23, 0x7

    const/16 v24, 0x2

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1, v3}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    .line 184
    .local v10, "fullWeek":Ljava/lang/String;
    sget-object v23, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_9

    .line 185
    if-eqz v12, :cond_7

    const-string/jumbo v23, "yyyy-MM-dd"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 186
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->cradle_year:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 208
    .local v4, "date":Ljava/lang/String;
    :goto_3
    new-instance v5, Ljava/lang/String;

    .end local v5    # "dateDescription":Ljava/lang/String;
    invoke-direct {v5, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 209
    .restart local v5    # "dateDescription":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-virtual {v5, v0, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 210
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->isComplete:Z

    move/from16 v23, v0

    if-eqz v23, :cond_10

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mDate:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mDate:Landroid/widget/TextView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v24

    or-int/lit8 v24, v24, 0x10

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mDate:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    .end local v4    # "date":Ljava/lang/String;
    .end local v6    # "day":Ljava/lang/String;
    .end local v9    # "dueDateCal":Ljava/util/Calendar;
    .end local v10    # "fullWeek":Ljava/lang/String;
    .end local v11    # "month":Ljava/lang/String;
    .end local v12    # "phone_date_format":Ljava/lang/String;
    .end local v21    # "week":Ljava/lang/String;
    .end local v22    # "year":Ljava/lang/String;
    :cond_3
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getReminderTime()J

    move-result-wide v13

    .line 219
    .local v13, "reminder":J
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->isComplete:Z

    move/from16 v23, v0

    if-eqz v23, :cond_11

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_row_reminder:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mBottomReminder:Landroid/view/View;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->setVisibility(I)V

    .line 270
    :goto_5
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTitle:Landroid/widget/TextView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getContext()Landroid/content/Context;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->dueto:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/TaskWidget;->contentString:Ljava/lang/String;

    .line 271
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getContext()Landroid/content/Context;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->reminder:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 273
    if-eqz p2, :cond_4

    :try_start_0
    sget-object v23, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ConfirmButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v23

    if-nez v23, :cond_1c

    .line 275
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_button:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    move-object/from16 v23, v0

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_row_reminder:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->taskContainer:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->contentString:Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/security/InvalidParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 291
    :catch_0
    move-exception v23

    goto/16 :goto_0

    .line 155
    .end local v3    # "currentLocale":Ljava/util/Locale;
    .end local v5    # "dateDescription":Ljava/lang/String;
    .end local v7    # "dueDate":J
    .end local v13    # "reminder":J
    .end local v16    # "reminderDateDescription":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTitle:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    invoke-static/range {v20 .. v20}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_1

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTitle:Landroid/widget/TextView;

    move-object/from16 v23, v0

    sget v24, Lcom/vlingo/midas/R$string;->my_event:I

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 165
    .restart local v7    # "dueDate":J
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleTask;->getUtcDueDate()J

    move-result-wide v7

    goto/16 :goto_2

    .line 187
    .restart local v3    # "currentLocale":Ljava/util/Locale;
    .restart local v5    # "dateDescription":Ljava/lang/String;
    .restart local v6    # "day":Ljava/lang/String;
    .restart local v9    # "dueDateCal":Ljava/util/Calendar;
    .restart local v10    # "fullWeek":Ljava/lang/String;
    .restart local v11    # "month":Ljava/lang/String;
    .restart local v12    # "phone_date_format":Ljava/lang/String;
    .restart local v16    # "reminderDateDescription":Ljava/lang/String;
    .restart local v21    # "week":Ljava/lang/String;
    .restart local v22    # "year":Ljava/lang/String;
    :cond_7
    if-eqz v12, :cond_8

    const-string/jumbo v23, "MM-dd-yyyy"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 188
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->cradle_year:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_3

    .line 190
    .end local v4    # "date":Ljava/lang/String;
    :cond_8
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->cradle_year:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_3

    .line 191
    .end local v4    # "date":Ljava/lang/String;
    :cond_9
    sget-object v23, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_c

    .line 192
    if-eqz v12, :cond_b

    const-string/jumbo v23, "yyyy-MM-dd"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_a

    const-string/jumbo v23, "MM-dd-yyyy"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 194
    :cond_a
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->cradle_year:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " ("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_3

    .line 196
    .end local v4    # "date":Ljava/lang/String;
    :cond_b
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " ("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_3

    .line 197
    .end local v4    # "date":Ljava/lang/String;
    :cond_c
    sget-object v23, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 198
    new-instance v23, Ljava/util/Date;

    move-object/from16 v0, v23

    invoke-direct {v0, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetDate(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_3

    .line 200
    .end local v4    # "date":Ljava/lang/String;
    :cond_d
    if-eqz v12, :cond_e

    const-string/jumbo v23, "yyyy-MM-dd"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_e

    .line 201
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_3

    .line 202
    .end local v4    # "date":Ljava/lang/String;
    :cond_e
    if-eqz v12, :cond_f

    const-string/jumbo v23, "MM-dd-yyyy"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 203
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_3

    .line 205
    .end local v4    # "date":Ljava/lang/String;
    :cond_f
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_3

    .line 214
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mDate:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 223
    .end local v4    # "date":Ljava/lang/String;
    .end local v6    # "day":Ljava/lang/String;
    .end local v9    # "dueDateCal":Ljava/util/Calendar;
    .end local v10    # "fullWeek":Ljava/lang/String;
    .end local v11    # "month":Ljava/lang/String;
    .end local v12    # "phone_date_format":Ljava/lang/String;
    .end local v21    # "week":Ljava/lang/String;
    .end local v22    # "year":Ljava/lang/String;
    .restart local v13    # "reminder":J
    :cond_11
    const-wide/16 v23, 0x0

    cmp-long v23, v13, v23

    if-gtz v23, :cond_12

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mReminder:Landroid/widget/TextView;

    move-object/from16 v23, v0

    const-string/jumbo v24, ""

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 226
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    const-string/jumbo v24, "date_format"

    invoke-static/range {v23 .. v24}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 228
    .restart local v12    # "phone_date_format":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v15

    .line 229
    .local v15, "reminderCal":Ljava/util/Calendar;
    new-instance v23, Ljava/util/Date;

    move-object/from16 v0, v23

    invoke-direct {v0, v13, v14}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 231
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Ljava/util/Calendar;->get(I)I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    .line 232
    .restart local v22    # "year":Ljava/lang/String;
    const/16 v23, 0x5

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Ljava/util/Calendar;->get(I)I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 233
    .restart local v6    # "day":Ljava/lang/String;
    const/16 v23, 0x2

    const/16 v24, 0x1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v25

    move/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v15, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v11

    .line 234
    .restart local v11    # "month":Ljava/lang/String;
    const/16 v23, 0x7

    const/16 v24, 0x1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v25

    move/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v15, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v21

    .line 235
    .restart local v21    # "week":Ljava/lang/String;
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Ljava/util/Calendar;->get(I)I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    .line 236
    .local v17, "reminderHour":Ljava/lang/String;
    const/16 v23, 0xc

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Ljava/util/Calendar;->get(I)I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    .line 237
    .local v18, "reminderMinute":Ljava/lang/String;
    const/16 v23, 0x7

    const/16 v24, 0x2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v25

    move/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v15, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    .line 240
    .restart local v10    # "fullWeek":Ljava/lang/String;
    sget-object v23, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_15

    .line 241
    if-eqz v12, :cond_13

    const-string/jumbo v23, "yyyy-MM-dd"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_13

    .line 242
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->cradle_year:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 263
    .restart local v4    # "date":Ljava/lang/String;
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v23

    move/from16 v0, v23

    invoke-static {v13, v14, v0, v3}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetTime(JZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v19

    .line 264
    .local v19, "reminderString":Ljava/lang/String;
    new-instance v16, Ljava/lang/String;

    .end local v16    # "reminderDateDescription":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 265
    .restart local v16    # "reminderDateDescription":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mReminder:Landroid/widget/TextView;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string/jumbo v25, ", "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 243
    .end local v4    # "date":Ljava/lang/String;
    .end local v19    # "reminderString":Ljava/lang/String;
    :cond_13
    if-eqz v12, :cond_14

    const-string/jumbo v23, "MM-dd-yyyy"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 244
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->cradle_year:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_6

    .line 246
    .end local v4    # "date":Ljava/lang/String;
    :cond_14
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->cradle_year:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_6

    .line 247
    .end local v4    # "date":Ljava/lang/String;
    :cond_15
    sget-object v23, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_18

    .line 248
    if-eqz v12, :cond_17

    const-string/jumbo v23, "yyyy-MM-dd"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_16

    const-string/jumbo v23, "MM-dd-yyyy"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_17

    .line 250
    :cond_16
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->cradle_year:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " ("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_6

    .line 252
    .end local v4    # "date":Ljava/lang/String;
    :cond_17
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getKoreanMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " ("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_6

    .line 253
    .end local v4    # "date":Ljava/lang/String;
    :cond_18
    sget-object v23, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_19

    .line 254
    new-instance v23, Ljava/util/Date;

    move-object/from16 v0, v23

    invoke-direct {v0, v13, v14}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v23

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetDate(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_6

    .line 256
    .end local v4    # "date":Ljava/lang/String;
    :cond_19
    if-eqz v12, :cond_1a

    const-string/jumbo v23, "yyyy-MM-dd"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1a

    .line 257
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_6

    .line 258
    .end local v4    # "date":Ljava/lang/String;
    :cond_1a
    if-eqz v12, :cond_1b

    const-string/jumbo v23, "MM-dd-yyyy"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1b

    .line 259
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_6

    .line 261
    .end local v4    # "date":Ljava/lang/String;
    :cond_1b
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "date":Ljava/lang/String;
    goto/16 :goto_6

    .line 285
    .end local v4    # "date":Ljava/lang/String;
    .end local v6    # "day":Ljava/lang/String;
    .end local v10    # "fullWeek":Ljava/lang/String;
    .end local v11    # "month":Ljava/lang/String;
    .end local v12    # "phone_date_format":Ljava/lang/String;
    .end local v15    # "reminderCal":Ljava/util/Calendar;
    .end local v17    # "reminderHour":Ljava/lang/String;
    .end local v18    # "reminderMinute":Ljava/lang/String;
    .end local v21    # "week":Ljava/lang/String;
    .end local v22    # "year":Ljava/lang/String;
    :cond_1c
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V

    .line 288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_row_reminder:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->taskContainer:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->contentString:Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/security/InvalidParameterException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 294
    :catch_1
    move-exception v23

    goto/16 :goto_0
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 47
    check-cast p1, Lcom/vlingo/core/internal/schedule/ScheduleTask;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->initialize(Lcom/vlingo/core/internal/schedule/ScheduleTask;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 467
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->isClicked:Z

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 302
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 304
    .local v0, "id":I
    sget v2, Lcom/vlingo/midas/R$id;->task_confirm:I

    if-ne v0, v2, :cond_1

    .line 306
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    if-eqz v2, :cond_0

    .line 307
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->isClicked:Z

    .line 308
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->lookupMode:Z

    .line 310
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->retire()V

    .line 311
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    sget v2, Lcom/vlingo/midas/R$id;->btn_cancel_task:I

    if-ne v0, v2, :cond_0

    .line 324
    :try_start_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    if-eqz v2, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->retire()V

    .line 326
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 327
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 330
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v2

    goto :goto_0

    .line 333
    :catch_1
    move-exception v2

    goto :goto_0

    .line 318
    :catch_2
    move-exception v2

    goto :goto_0

    .line 315
    :catch_3
    move-exception v2

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 78
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 80
    sget v0, Lcom/vlingo/midas/R$id;->task_row_title:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_row_title:Landroid/widget/LinearLayout;

    .line 81
    sget v0, Lcom/vlingo/midas/R$id;->task_row_dueto:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_row_dueto:Landroid/widget/LinearLayout;

    .line 82
    sget v0, Lcom/vlingo/midas/R$id;->task_row_reminder:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_row_reminder:Landroid/widget/LinearLayout;

    .line 83
    sget v0, Lcom/vlingo/midas/R$id;->task_button:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_button:Landroid/widget/LinearLayout;

    .line 85
    sget v0, Lcom/vlingo/midas/R$id;->reminder_bottom:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mBottomReminder:Landroid/view/View;

    .line 87
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_row_title:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 88
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_row_dueto:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 89
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_row_reminder:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 91
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    sget v0, Lcom/vlingo/midas/R$id;->task_divider:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->view:Landroid/view/View;

    .line 93
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->view:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95
    sget v0, Lcom/vlingo/midas/R$id;->task_button_divider:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->button_divider:Landroid/view/View;

    .line 96
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->button_divider:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 104
    :cond_0
    :try_start_0
    sget v0, Lcom/vlingo/midas/R$id;->task_title:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTitle:Landroid/widget/TextView;

    .line 105
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mTitle:Landroid/widget/TextView;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/TaskWidget$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/TaskWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 119
    :cond_1
    sget v0, Lcom/vlingo/midas/R$id;->task_date:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mDate:Landroid/widget/TextView;

    .line 120
    sget v0, Lcom/vlingo/midas/R$id;->task_reminder_date:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mReminder:Landroid/widget/TextView;

    .line 121
    sget v0, Lcom/vlingo/midas/R$id;->task_confirm:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    .line 122
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_2
    sget v0, Lcom/vlingo/midas/R$id;->btn_cancel_task:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    .line 126
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    if-eqz v0, :cond_3

    .line 127
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :cond_3
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 408
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 426
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    if-eqz v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 428
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->launchTask()V

    .line 434
    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 455
    :cond_1
    return v1

    .line 430
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->launchTaskDetail()V

    .line 431
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TaskWidget;->retire()V

    goto :goto_0
.end method

.method public retire()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 472
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->retire()V

    .line 473
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    if-eqz v0, :cond_1

    .line 477
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V

    .line 479
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_button:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 480
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TaskWidget;->task_button:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 482
    :cond_2
    return-void
.end method
