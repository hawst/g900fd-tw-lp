.class public Lcom/vlingo/midas/gui/widgets/TimerWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "TimerWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/TimerWidget$TimerIntentReceiver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnTouchListener;",
        "Landroid/view/View$OnFocusChangeListener;"
    }
.end annotation


# static fields
.field public static final ACTION_TIMER_NONE:Ljava/lang/String; = "com.sec.android.app.clockpackage.timer.ticker.TIMER_NONE"

.field public static final ACTION_TIMER_RESET:Ljava/lang/String; = "com.sec.android.app.clockpackage.timer.ticker.TIMER_RESET"

.field public static final ACTION_TIMER_START:Ljava/lang/String; = "com.sec.android.app.clockpackage.timer.ticker.TIMER_START"

.field public static final ACTION_TIMER_STOP:Ljava/lang/String; = "com.sec.android.app.clockpackage.timer.ticker.TIMER_STOP"

.field public static final COUNT_DOWN_ADJUSTMENT:J = 0x12cL

.field private static final COUNT_DOWN_INTERVAL:J = 0x3e8L

.field public static final HOUR:I = 0x0

.field public static final INPUT_CMD:Ljava/lang/String; = "Input command"

.field public static final JAPAN_FONT_SIZE:F = 18.34f

.field public static final KEY_REMAIN_TIME:Ljava/lang/String; = "timer_remain_time"

.field public static final KEY_SEND_TIME:Ljava/lang/String; = "timer_send_time"

.field public static final MINUTE:I = 0x1

.field public static final NOTIFY_TIMER_CMD_RESULT:Ljava/lang/String; = "com.sec.android.app.clockpackage.timer.NOTIFY_TIMER_CMD_RESULT"

.field public static final RESET:I = 0x3

.field public static final RESULT:Ljava/lang/String; = "Result"

.field public static final RESULT_INPUT_MILLIS:Ljava/lang/String; = "Input Millis"

.field public static final RESULT_REMAIN_MILLIS:Ljava/lang/String; = "Remain Millis"

.field public static final RESULT_SUCCESS:Ljava/lang/String; = "Success"

.field public static final SECOND:I = 0x2

.field public static final SEND_ACTION_TIMER_LAUNCH:Ljava/lang/String; = "com.sec.android.app.clockpackage.TIMER_ACTION"

.field public static final SEND_ACTION_TIMER_RESET:Ljava/lang/String; = "com.sec.android.app.clockpackage.timer.TIMER_RESET"

.field public static final SEND_ACTION_TIMER_RESTART:Ljava/lang/String; = "com.sec.android.app.clockpackage.timer.TIMER_RESTART"

.field public static final SEND_ACTION_TIMER_SET_TIME:Ljava/lang/String; = "com.sec.android.app.clockpackage.timer.TIMER_SET_TIME"

.field public static final SEND_ACTION_TIMER_START:Ljava/lang/String; = "com.sec.android.app.clockpackage.timer.TIMER_START"

.field public static final SEND_ACTION_TIMER_STOP:Ljava/lang/String; = "com.sec.android.app.clockpackage.timer.TIMER_STOP"

.field public static final STARTED:I = 0x1

.field public static final STOPPED:I = 0x2

.field private static final TIMER_MAXIMUM_TIME:I = 0x15752618

.field public static final TIMER_NOTI_ID:I = 0x14a9d

.field private static final TIMER_WIDGET_PROPERTY:Ljava/lang/String; = "embeddedTimer"

.field private static canceled:Z

.field private static mFirstColon:Landroid/widget/ImageView;

.field static mHourPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

.field static mHourPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

.field static mMinutePostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

.field static mMinutePrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

.field private static mSecondColon:Landroid/widget/ImageView;

.field static mSecondPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

.field static mSecondPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

.field static needStartTimer:Z

.field public static sColon:Landroid/graphics/drawable/Drawable;

.field public static sColonPressed:Landroid/graphics/drawable/Drawable;

.field public static sEnabledNumber:[Landroid/graphics/drawable/Drawable;

.field public static selectedTimeNumber:[Landroid/graphics/drawable/Drawable;

.field public static timeNumber:[Landroid/graphics/drawable/Drawable;


# instance fields
.field private context:Landroid/content/Context;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mBackgroundNumberLayout:Landroid/widget/RelativeLayout;

.field private mHeadTextHour:Landroid/widget/TextView;

.field private mHeadTextMin:Landroid/widget/TextView;

.field private mHeadTextSec:Landroid/widget/TextView;

.field private mHourBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

.field private mMinuteBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

.field private mSecondBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

.field private mTimerTextLayout:Landroid/widget/RelativeLayout;

.field private res:Landroid/content/res/Resources;

.field private timerContainer:Landroid/widget/RelativeLayout;

.field private widgetTimer:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 92
    sput-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    .line 93
    sput-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    .line 121
    sput-boolean v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->canceled:Z

    .line 122
    sput-boolean v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->needStartTimer:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->res:Landroid/content/res/Resources;

    .line 126
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->context:Landroid/content/Context;

    .line 128
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->res:Landroid/content/res/Resources;

    .line 129
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/TimerWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/TimerWidget;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->stop()V

    return-void
.end method

.method static synthetic access$100()V
    .locals 0

    .prologue
    .line 43
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->updateTime()V

    return-void
.end method

.method static synthetic access$200()V
    .locals 0

    .prologue
    .line 43
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->initTimeView()V

    return-void
.end method

.method static synthetic access$300(J)V
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 43
    invoke-static {p0, p1}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setTimer(J)V

    return-void
.end method

.method private cancel()V
    .locals 2

    .prologue
    .line 340
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->stopTimerApp()V

    .line 341
    sget-object v0, Lcom/vlingo/midas/gui/timer/TimerManager;->timer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 342
    sget-object v0, Lcom/vlingo/midas/gui/timer/TimerManager;->timer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 344
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V

    .line 345
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->widgetTimer:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 346
    return-void
.end method

.method private getTimerAccessibilityString()Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 299
    const-string/jumbo v0, ""

    .line 301
    .local v0, "talkbackString":Ljava/lang/String;
    sget v1, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    if-lez v1, :cond_0

    .line 302
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->res:Landroid/content/res/Resources;

    sget v3, Lcom/vlingo/midas/R$plurals;->plurals_hours:I

    sget v4, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    new-array v5, v8, [Ljava/lang/Object;

    sget v6, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 304
    :cond_0
    sget v1, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    if-lez v1, :cond_1

    .line 305
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->res:Landroid/content/res/Resources;

    sget v3, Lcom/vlingo/midas/R$plurals;->plurals_minutes:I

    sget v4, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    new-array v5, v8, [Ljava/lang/Object;

    sget v6, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 308
    :cond_1
    sget v1, Lcom/vlingo/midas/gui/timer/TimerManager;->second:I

    if-lez v1, :cond_2

    .line 309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->res:Landroid/content/res/Resources;

    sget v3, Lcom/vlingo/midas/R$plurals;->plurals_seconds:I

    sget v4, Lcom/vlingo/midas/gui/timer/TimerManager;->second:I

    new-array v5, v8, [Ljava/lang/Object;

    sget v6, Lcom/vlingo/midas/gui/timer/TimerManager;->second:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 313
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->res:Landroid/content/res/Resources;

    sget v2, Lcom/vlingo/midas/R$string;->timer_widget_time_info:I

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static initTimeView()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 241
    invoke-static {}, Lcom/vlingo/midas/gui/timer/TimerManager;->initTime()V

    .line 243
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget v2, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    sget v3, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    sget v4, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setEnableNumber(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;IIII)V

    .line 245
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinutePrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinutePostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget v2, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    sget v3, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    sget v4, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setEnableNumber(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;IIII)V

    .line 246
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget v2, Lcom/vlingo/midas/gui/timer/TimerManager;->second:I

    sget v3, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    sget v4, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    const/4 v5, 0x2

    invoke-static/range {v0 .. v5}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setEnableNumber(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;IIII)V

    .line 248
    sget v0, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    if-lez v0, :cond_0

    .line 249
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 250
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColonPressed:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 251
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 252
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColonPressed:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 270
    :goto_0
    return-void

    .line 253
    :cond_0
    sget v0, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    if-lez v0, :cond_1

    .line 254
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 255
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 256
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 257
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColonPressed:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 259
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 260
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 261
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 262
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 265
    :cond_2
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget v2, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setTimeToView(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;I)V

    .line 266
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinutePrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinutePostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget v2, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setTimeToView(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;I)V

    .line 267
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget v2, Lcom/vlingo/midas/gui/timer/TimerManager;->second:I

    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setTimeToView(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;I)V

    goto :goto_0
.end method

.method private initTimerView()V
    .locals 1

    .prologue
    .line 274
    sget v0, Lcom/vlingo/midas/R$id;->timer_hour_bg:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/timer/TimeImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

    .line 275
    sget v0, Lcom/vlingo/midas/R$id;->timer_minute_bg:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/timer/TimeImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinuteBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

    .line 276
    sget v0, Lcom/vlingo/midas/R$id;->timer_second_bg:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/timer/TimeImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

    .line 278
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/timer/TimeImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 279
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/timer/TimeImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 280
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinuteBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/timer/TimeImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 281
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinuteBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/timer/TimeImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 282
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/timer/TimeImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 283
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondBackground:Lcom/vlingo/midas/gui/timer/TimeImageView;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/timer/TimeImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 285
    sget v0, Lcom/vlingo/midas/R$id;->timer_hour_prefix:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/timer/NumberImageView;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    .line 286
    sget v0, Lcom/vlingo/midas/R$id;->timer_hour_postfix:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/timer/NumberImageView;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    .line 287
    sget v0, Lcom/vlingo/midas/R$id;->timer_minute_prefix:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/timer/NumberImageView;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinutePrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    .line 288
    sget v0, Lcom/vlingo/midas/R$id;->timer_minute_postfix:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/timer/NumberImageView;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinutePostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    .line 289
    sget v0, Lcom/vlingo/midas/R$id;->timer_second_prefix:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/timer/NumberImageView;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    .line 290
    sget v0, Lcom/vlingo/midas/R$id;->timer_second_postfix:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/timer/NumberImageView;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    .line 292
    sget v0, Lcom/vlingo/midas/R$id;->head_text_hr:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHeadTextHour:Landroid/widget/TextView;

    .line 293
    sget v0, Lcom/vlingo/midas/R$id;->head_text_min:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHeadTextMin:Landroid/widget/TextView;

    .line 294
    sget v0, Lcom/vlingo/midas/R$id;->head_text_sec:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHeadTextSec:Landroid/widget/TextView;

    .line 295
    return-void
.end method

.method private launchTimer()V
    .locals 3

    .prologue
    .line 396
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.clockpackage.TIMER_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 397
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.clockpackage"

    const-string/jumbo v2, "com.sec.android.app.clockpackage.ClockPackage"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 398
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 399
    return-void
.end method

.method private declared-synchronized preLoadImage()V
    .locals 4

    .prologue
    .line 422
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 458
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 426
    :cond_1
    const/16 v0, 0xa

    :try_start_1
    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    .line 427
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_00:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 428
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_01:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 429
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_02:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 430
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_03:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 431
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_04:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 432
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_05:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 433
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_06:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 434
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_07:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 435
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_08:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 436
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_09:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 438
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 442
    const/16 v0, 0xa

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    .line 443
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_press_00:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 444
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_press_01:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 445
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_press_02:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 446
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_press_03:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 447
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_press_04:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 448
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_press_05:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 449
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_press_06:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 450
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_press_07:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 451
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_press_08:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 452
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->timer_number_press_09:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 454
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColon:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 457
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$drawable;->timer_number_dot:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColon:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private reset()V
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/vlingo/midas/gui/timer/TimerManager;->setState(I)V

    .line 335
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->initTimeView()V

    .line 336
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->updateTimerApp()V

    .line 337
    return-void
.end method

.method private restartTimerApp()V
    .locals 2

    .prologue
    .line 390
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.clockpackage.timer.TIMER_RESTART"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 391
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 392
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 393
    return-void
.end method

.method public static setEnableNumber(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;IIII)V
    .locals 1
    .param p0, "preNumber"    # Lcom/vlingo/midas/gui/timer/NumberImageView;
    .param p1, "postNumber"    # Lcom/vlingo/midas/gui/timer/NumberImageView;
    .param p2, "time"    # I
    .param p3, "hour"    # I
    .param p4, "min"    # I
    .param p5, "hms"    # I

    .prologue
    .line 522
    const/4 v0, 0x2

    if-ne p5, v0, :cond_2

    .line 523
    if-nez p3, :cond_1

    if-nez p4, :cond_1

    if-nez p2, :cond_1

    .line 524
    div-int/lit8 v0, p2, 0xa

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumber(I)V

    .line 525
    rem-int/lit8 v0, p2, 0xa

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumber(I)V

    .line 547
    :cond_0
    :goto_0
    return-void

    .line 527
    :cond_1
    div-int/lit8 v0, p2, 0xa

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumberEnable(I)V

    .line 528
    rem-int/lit8 v0, p2, 0xa

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumberEnable(I)V

    goto :goto_0

    .line 530
    :cond_2
    const/4 v0, 0x1

    if-ne p5, v0, :cond_5

    .line 531
    if-nez p3, :cond_3

    if-eqz p2, :cond_4

    .line 532
    :cond_3
    div-int/lit8 v0, p2, 0xa

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumberEnable(I)V

    .line 533
    rem-int/lit8 v0, p2, 0xa

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumberEnable(I)V

    goto :goto_0

    .line 535
    :cond_4
    div-int/lit8 v0, p2, 0xa

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumber(I)V

    .line 536
    rem-int/lit8 v0, p2, 0xa

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumber(I)V

    goto :goto_0

    .line 538
    :cond_5
    if-nez p5, :cond_0

    .line 539
    if-eqz p2, :cond_6

    .line 540
    div-int/lit8 v0, p2, 0xa

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumberEnable(I)V

    .line 541
    rem-int/lit8 v0, p2, 0xa

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumberEnable(I)V

    goto :goto_0

    .line 543
    :cond_6
    div-int/lit8 v0, p2, 0xa

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumber(I)V

    .line 544
    rem-int/lit8 v0, p2, 0xa

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumber(I)V

    goto :goto_0
.end method

.method private static setTimeToView(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;I)V
    .locals 1
    .param p0, "prefix"    # Lcom/vlingo/midas/gui/timer/NumberImageView;
    .param p1, "postfix"    # Lcom/vlingo/midas/gui/timer/NumberImageView;
    .param p2, "time"    # I

    .prologue
    .line 515
    div-int/lit8 v0, p2, 0xa

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumber(I)V

    .line 516
    rem-int/lit8 v0, p2, 0xa

    invoke-virtual {p1, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setNumber(I)V

    .line 517
    return-void
.end method

.method private static setTimer(J)V
    .locals 5
    .param p0, "time"    # J

    .prologue
    .line 349
    new-instance v0, Lcom/vlingo/midas/gui/widgets/TimerWidget$2;

    const-wide/16 v1, 0x12c

    add-long/2addr v1, p0

    const-wide/16 v3, 0x3e8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vlingo/midas/gui/widgets/TimerWidget$2;-><init>(JJ)V

    sput-object v0, Lcom/vlingo/midas/gui/timer/TimerManager;->timer:Landroid/os/CountDownTimer;

    .line 364
    return-void
.end method

.method private startTimerApp()V
    .locals 2

    .prologue
    .line 378
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.clockpackage.timer.TIMER_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 379
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 380
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 381
    return-void
.end method

.method private stop()V
    .locals 0

    .prologue
    .line 330
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->stopTimerApp()V

    .line 331
    return-void
.end method

.method private stopTimerApp()V
    .locals 2

    .prologue
    .line 384
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.clockpackage.timer.TIMER_STOP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 385
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 386
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 387
    return-void
.end method

.method private static updateTime()V
    .locals 12

    .prologue
    const-wide/32 v7, 0x36ee80

    const-wide/32 v5, 0xea60

    const/4 v11, 0x0

    .line 462
    sget-wide v0, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    div-long/2addr v0, v7

    long-to-int v3, v0

    .line 463
    .local v3, "hour":I
    sget-wide v0, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    rem-long/2addr v0, v7

    div-long/2addr v0, v5

    long-to-int v4, v0

    .line 464
    .local v4, "minute":I
    sget-wide v0, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    rem-long/2addr v0, v5

    const-wide/16 v5, 0x3e8

    div-long/2addr v0, v5

    long-to-int v2, v0

    .line 466
    .local v2, "second":I
    sget v0, Lcom/vlingo/midas/gui/timer/TimerManager;->second:I

    if-eq v2, v0, :cond_0

    .line 467
    sput v2, Lcom/vlingo/midas/gui/timer/TimerManager;->second:I

    .line 468
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 469
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    const/4 v5, 0x2

    invoke-static/range {v0 .. v5}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setEnableNumber(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;IIII)V

    .line 477
    :goto_0
    sget v0, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    if-eq v4, v0, :cond_0

    .line 478
    sput v4, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    .line 479
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 480
    sget-object v5, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinutePrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinutePostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    const/4 v10, 0x1

    move v7, v4

    move v8, v3

    move v9, v4

    invoke-static/range {v5 .. v10}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setEnableNumber(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;IIII)V

    .line 481
    if-lez v4, :cond_2

    .line 482
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 483
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColonPressed:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 495
    :goto_1
    sget v0, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    if-eq v3, v0, :cond_0

    .line 496
    sput v3, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    .line 497
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 498
    sget-object v5, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    const/4 v10, 0x0

    move v7, v3

    move v8, v3

    move v9, v4

    invoke-static/range {v5 .. v10}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setEnableNumber(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;IIII)V

    .line 499
    if-lez v3, :cond_4

    .line 500
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 501
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColonPressed:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 512
    :cond_0
    :goto_2
    return-void

    .line 471
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setTimeToView(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;I)V

    goto :goto_0

    .line 485
    :cond_2
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 486
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 489
    :cond_3
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinutePrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mMinutePostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    invoke-static {v0, v1, v4}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setTimeToView(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;I)V

    goto :goto_1

    .line 503
    :cond_4
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 504
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 507
    :cond_5
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourPrefix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mHourPostfix:Lcom/vlingo/midas/gui/timer/NumberImageView;

    invoke-static {v0, v1, v3}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->setTimeToView(Lcom/vlingo/midas/gui/timer/NumberImageView;Lcom/vlingo/midas/gui/timer/NumberImageView;I)V

    goto :goto_2
.end method

.method private updateTimerApp()V
    .locals 3

    .prologue
    .line 367
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.clockpackage.timer.TIMER_RESET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 368
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 369
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.clockpackage.timer.TIMER_SET_TIME"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 370
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "Hour"

    sget v2, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 371
    const-string/jumbo v1, "Minute"

    sget v2, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 372
    const-string/jumbo v1, "Second"

    sget v2, Lcom/vlingo/midas/gui/timer/TimerManager;->second:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 373
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 374
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 375
    return-void
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 43
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->initialize(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 10
    .param p1, "time"    # Ljava/lang/String;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const-wide/32 v8, 0x15752618

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 133
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 135
    if-eqz p2, :cond_0

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerCancelCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    sput-boolean v6, Lcom/vlingo/midas/gui/widgets/TimerWidget;->canceled:Z

    .line 137
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->cancel()V

    .line 196
    :goto_0
    return-void

    .line 140
    :cond_0
    sput-boolean v7, Lcom/vlingo/midas/gui/widgets/TimerWidget;->canceled:Z

    .line 145
    sget v2, Lcom/vlingo/midas/R$id;->timer_body:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/vlingo/midas/gui/widgets/TimerWidget$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/TimerWidget;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 165
    invoke-static {p1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 167
    :try_start_0
    invoke-static {p1}, Lcom/vlingo/core/internal/schedule/DateUtil;->getTimeFromCanonicalTimeString(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 170
    .local v0, "d":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTimezoneOffset()I

    move-result v2

    const v3, -0xea60

    mul-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    add-long/2addr v2, v4

    sput-wide v2, Lcom/vlingo/midas/gui/timer/TimerManager;->inputMillis:J

    .line 171
    sget-wide v2, Lcom/vlingo/midas/gui/timer/TimerManager;->inputMillis:J

    cmp-long v2, v2, v8

    if-lez v2, :cond_1

    .line 172
    const-wide/32 v2, 0x15752618

    sput-wide v2, Lcom/vlingo/midas/gui/timer/TimerManager;->inputMillis:J

    .line 174
    :cond_1
    sget-wide v2, Lcom/vlingo/midas/gui/timer/TimerManager;->inputMillis:J

    sput-wide v2, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    .line 176
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->reset()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .end local v0    # "d":Ljava/util/Date;
    :cond_2
    :goto_1
    if-eqz p2, :cond_3

    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerResetCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 183
    :cond_3
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->reset()V

    .line 184
    sput-boolean v7, Lcom/vlingo/midas/gui/widgets/TimerWidget;->needStartTimer:Z

    .line 194
    :cond_4
    :goto_2
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timerContainer:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getTimerAccessibilityString()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 195
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v4, "embeddedTimer"

    const-string/jumbo v5, "true"

    invoke-virtual {v2, v3, v4, v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->addWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 177
    :catch_0
    move-exception v1

    .line 178
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 185
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerRestartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 186
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->reset()V

    .line 187
    sput-boolean v6, Lcom/vlingo/midas/gui/widgets/TimerWidget;->needStartTimer:Z

    goto :goto_2

    .line 188
    :cond_6
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerStartCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 190
    sput-boolean v6, Lcom/vlingo/midas/gui/widgets/TimerWidget;->needStartTimer:Z

    goto :goto_2

    .line 191
    :cond_7
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->TimerStopCmd:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 192
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->stop()V

    goto :goto_2
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 556
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 551
    sget-boolean v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->canceled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationEnd()V
    .locals 1

    .prologue
    .line 220
    sget-boolean v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->needStartTimer:Z

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->needStartTimer:Z

    .line 222
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->start()V

    .line 224
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 403
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 404
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->timer_body:I

    if-ne v0, v1, :cond_0

    .line 406
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->launchTimer()V

    .line 409
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 202
    sget v0, Lcom/vlingo/midas/R$id;->timer_body:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timerContainer:Landroid/widget/RelativeLayout;

    .line 203
    sget v0, Lcom/vlingo/midas/R$id;->widget_timer:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->widgetTimer:Landroid/widget/RelativeLayout;

    .line 204
    sget v0, Lcom/vlingo/midas/R$id;->timer_number_background:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mBackgroundNumberLayout:Landroid/widget/RelativeLayout;

    .line 205
    sget v0, Lcom/vlingo/midas/R$id;->timer_head_text:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mTimerTextLayout:Landroid/widget/RelativeLayout;

    .line 207
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$drawable;->timer_number_press_dot:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sColonPressed:Landroid/graphics/drawable/Drawable;

    .line 209
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mBackgroundNumberLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 210
    sget v0, Lcom/vlingo/midas/R$id;->first_colon:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mFirstColon:Landroid/widget/ImageView;

    .line 211
    sget v0, Lcom/vlingo/midas/R$id;->second_colon:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->mSecondColon:Landroid/widget/ImageView;

    .line 213
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->preLoadImage()V

    .line 214
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->initTimerView()V

    .line 215
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->initTimeView()V

    .line 216
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Z

    .prologue
    .line 413
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x1

    .line 229
    sget v1, Lcom/vlingo/midas/gui/timer/TimerManager;->state:I

    if-ne v1, v0, :cond_0

    .line 232
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 238
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    .line 417
    const/4 v0, 0x0

    return v0
.end method

.method public start()V
    .locals 1

    .prologue
    .line 317
    sget v0, Lcom/vlingo/midas/gui/timer/TimerManager;->state:I

    packed-switch v0, :pswitch_data_0

    .line 324
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->startTimerApp()V

    .line 327
    :goto_0
    :pswitch_0
    return-void

    .line 321
    :pswitch_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->restartTimerApp()V

    goto :goto_0

    .line 317
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
