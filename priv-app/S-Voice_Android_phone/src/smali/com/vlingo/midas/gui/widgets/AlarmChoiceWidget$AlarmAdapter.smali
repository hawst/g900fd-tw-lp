.class Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;
.super Landroid/widget/BaseAdapter;
.source "AlarmChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlarmAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final mAlarms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;"
        }
    .end annotation
.end field

.field private final mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p3, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 105
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 106
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->mAlarms:Ljava/util/List;

    .line 107
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    .line 111
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->mAlarms:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 112
    .local v0, "naturalSize":I
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v2, v0}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getLimitedCount(I)I

    move-result v1

    .line 113
    .local v1, "toReturn":I
    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->mAlarms:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 123
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 18
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 129
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v15, Lcom/vlingo/midas/R$layout;->item_alarm_choice:I

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 130
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    aput v16, v14, v15

    .line 131
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x1

    const/16 v16, 0x0

    aput v16, v14, v15

    .line 132
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x2

    const/16 v16, 0x0

    aput v16, v14, v15

    .line 133
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x3

    const/16 v16, 0x0

    aput v16, v14, v15

    .line 134
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x4

    const/16 v16, 0x0

    aput v16, v14, v15

    .line 135
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x5

    const/16 v16, 0x0

    aput v16, v14, v15

    .line 136
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x6

    const/16 v16, 0x0

    aput v16, v14, v15

    .line 137
    new-instance v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;)V

    .line 138
    .local v6, "holder":Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;
    sget v14, Lcom/vlingo/midas/R$id;->alarm_text_container:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/LinearLayout;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->textContainer:Landroid/widget/LinearLayout;

    .line 139
    sget v14, Lcom/vlingo/midas/R$id;->alarm_day_container:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/LinearLayout;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->dayContainer:Landroid/widget/LinearLayout;

    .line 140
    sget v14, Lcom/vlingo/midas/R$id;->today_text:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->day_text:Landroid/widget/TextView;

    .line 141
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_center:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/LinearLayout;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->itemAlarmCenter:Landroid/widget/LinearLayout;

    .line 142
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_subject:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 143
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_time_layout:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/LinearLayout;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mTimeLayout:Landroid/widget/LinearLayout;

    .line 144
    sget v14, Lcom/vlingo/midas/R$id;->alarm_repeat:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mIsRepeat:Landroid/widget/ImageView;

    .line 145
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_hour:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mHourText:Landroid/widget/TextView;

    .line 146
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_min:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mMinText:Landroid/widget/TextView;

    .line 147
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_colon:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mColon:Landroid/widget/TextView;

    .line 148
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_ampmnext:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    .line 149
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_ampmprevious:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    .line 150
    sget v14, Lcom/vlingo/midas/R$id;->alarm_enable:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->enableAlarm:Landroid/widget/ImageView;

    .line 151
    sget v14, Lcom/vlingo/midas/R$id;->alarm_divider:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mAlarmDivider:Landroid/view/View;

    .line 153
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_sunday:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mSun:Landroid/widget/TextView;

    .line 154
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_monday:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mMon:Landroid/widget/TextView;

    .line 155
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_tuesday:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mTue:Landroid/widget/TextView;

    .line 156
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_wednesday:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mWed:Landroid/widget/TextView;

    .line 157
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_thursday:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mThu:Landroid/widget/TextView;

    .line 158
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_friday:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mFri:Landroid/widget/TextView;

    .line 159
    sget v14, Lcom/vlingo/midas/R$id;->alarm_item_saturday:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mSat:Landroid/widget/TextView;

    .line 160
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 161
    const-string/jumbo v14, "system/fonts/SamsungSans-Num3T.ttf"

    invoke-static {v14}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v9

    .line 162
    .local v9, "samsungSans":Landroid/graphics/Typeface;
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mHourText:Landroid/widget/TextView;

    if-eqz v14, :cond_0

    .line 163
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mHourText:Landroid/widget/TextView;

    invoke-virtual {v14, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 164
    :cond_0
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mMinText:Landroid/widget/TextView;

    if-eqz v14, :cond_1

    .line 165
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mMinText:Landroid/widget/TextView;

    invoke-virtual {v14, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 166
    :cond_1
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mColon:Landroid/widget/TextView;

    if-eqz v14, :cond_2

    .line 167
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mColon:Landroid/widget/TextView;

    invoke-virtual {v14, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 169
    .end local v9    # "samsungSans":Landroid/graphics/Typeface;
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 171
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "holder":Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;
    check-cast v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;

    .line 173
    .restart local v6    # "holder":Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->mAlarms:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v14, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/core/internal/util/Alarm;

    .line 175
    .local v2, "alarm":Lcom/vlingo/core/internal/util/Alarm;
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->title:Landroid/widget/TextView;

    if-eqz v14, :cond_3

    .line 176
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->getName()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_17

    .line 177
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    if-lez v14, :cond_16

    .line 178
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    :cond_3
    :goto_0
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->isWeeklyRepeating()Z

    move-result v14

    if-eqz v14, :cond_18

    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mIsRepeat:Landroid/widget/ImageView;

    if-eqz v14, :cond_18

    .line 186
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mIsRepeat:Landroid/widget/ImageView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 196
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->getHour()I

    move-result v15

    # setter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14, v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$002(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)I

    .line 197
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->getMinute()I

    move-result v15

    # setter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mMin:I
    invoke-static {v14, v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$102(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)I

    .line 198
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v15

    # setter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I
    invoke-static {v14, v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$202(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)I

    .line 199
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v14

    if-eqz v14, :cond_20

    .line 200
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "ru-RU"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1c

    .line 201
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    if-ltz v14, :cond_19

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/4 v15, 0x4

    if-ge v14, v15, :cond_19

    .line 202
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm_night:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    .line 203
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm_night:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    .line 254
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$300(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v10

    .line 255
    .local v10, "timeFormat24State":Z
    const-string/jumbo v11, ""

    .line 256
    .local v11, "timeLayoutContentDescription":Ljava/lang/String;
    if-eqz v10, :cond_28

    .line 257
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 259
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mMin:I
    invoke-static {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 274
    :goto_3
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mTimeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v14, v11}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 276
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xa

    if-ge v14, v15, :cond_2c

    if-eqz v10, :cond_2c

    .line 277
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mHourText:Landroid/widget/TextView;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "0"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    move-object/from16 v16, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I
    invoke-static/range {v16 .. v16}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    :goto_4
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mColon:Landroid/widget/TextView;

    const-string/jumbo v15, ":"

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mMin:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xa

    if-ge v14, v15, :cond_2f

    .line 296
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mMinText:Landroid/widget/TextView;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "0"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    move-object/from16 v16, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mMin:I
    invoke-static/range {v16 .. v16}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    :goto_5
    sget-object v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;->$SwitchMap$com$vlingo$core$internal$util$Alarm$AlarmStatus:[I

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->getAlarmStatus()Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;

    move-result-object v15

    invoke-virtual {v15}, Lcom/vlingo/core/internal/util/Alarm$AlarmStatus;->ordinal()I

    move-result v15

    aget v14, v14, v15

    packed-switch v14, :pswitch_data_0

    .line 324
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->enableAlarm:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$drawable;->voice_talk_alarm_ic_off:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 331
    :goto_6
    :pswitch_0
    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/Alarm;->getDayMask()I

    move-result v7

    .line 333
    .local v7, "mask":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$color;->alarm_active_day_color:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 335
    .local v4, "color":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 336
    .local v3, "calendar":Ljava/util/Calendar;
    const/4 v14, 0x7

    invoke-virtual {v3, v14}, Ljava/util/Calendar;->get(I)I

    move-result v14

    add-int/lit8 v5, v14, -0x1

    .line 337
    .local v5, "day":I
    const/high16 v14, 0x1000000

    and-int/2addr v14, v7

    if-eqz v14, :cond_6

    .line 338
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x0

    const/16 v16, 0x1

    aput v16, v14, v15

    .line 339
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mSun:Landroid/widget/TextView;

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 340
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 341
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mSun:Landroid/widget/TextView;

    sget-object v15, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 344
    :cond_5
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mSun:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->full_sun:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 347
    :cond_6
    const/high16 v14, 0x100000

    and-int/2addr v14, v7

    if-eqz v14, :cond_8

    .line 348
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x1

    const/16 v16, 0x1

    aput v16, v14, v15

    .line 349
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mMon:Landroid/widget/TextView;

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 350
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 351
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mMon:Landroid/widget/TextView;

    sget-object v15, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 354
    :cond_7
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mMon:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->full_mon:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 357
    :cond_8
    const/high16 v14, 0x10000

    and-int/2addr v14, v7

    if-eqz v14, :cond_a

    .line 358
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x2

    const/16 v16, 0x1

    aput v16, v14, v15

    .line 359
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mTue:Landroid/widget/TextView;

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 360
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_9

    .line 361
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mTue:Landroid/widget/TextView;

    sget-object v15, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 363
    :cond_9
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mTue:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->full_thu:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 366
    :cond_a
    and-int/lit16 v14, v7, 0x1000

    if-eqz v14, :cond_c

    .line 367
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x3

    const/16 v16, 0x1

    aput v16, v14, v15

    .line 368
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mWed:Landroid/widget/TextView;

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 369
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 370
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mWed:Landroid/widget/TextView;

    sget-object v15, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 372
    :cond_b
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mWed:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->full_wed:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 375
    :cond_c
    and-int/lit16 v14, v7, 0x100

    if-eqz v14, :cond_e

    .line 376
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x4

    const/16 v16, 0x1

    aput v16, v14, v15

    .line 377
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mThu:Landroid/widget/TextView;

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 378
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_d

    .line 379
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mThu:Landroid/widget/TextView;

    sget-object v15, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 381
    :cond_d
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mThu:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->full_thu:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 384
    :cond_e
    and-int/lit8 v14, v7, 0x10

    if-eqz v14, :cond_10

    .line 385
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x5

    const/16 v16, 0x1

    aput v16, v14, v15

    .line 386
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mFri:Landroid/widget/TextView;

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 387
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_f

    .line 388
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mFri:Landroid/widget/TextView;

    sget-object v15, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 390
    :cond_f
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mFri:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->full_fir:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 393
    :cond_10
    and-int/lit8 v14, v7, 0x1

    if-eqz v14, :cond_12

    .line 394
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    const/4 v15, 0x6

    const/16 v16, 0x1

    aput v16, v14, v15

    .line 395
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mSat:Landroid/widget/TextView;

    invoke-virtual {v14, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 396
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_11

    .line 397
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mSat:Landroid/widget/TextView;

    sget-object v15, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 399
    :cond_11
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mSat:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->full_sat:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 403
    :cond_12
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isNewAlarmConcept()Z

    move-result v14

    if-eqz v14, :cond_14

    .line 404
    move v12, v5

    .line 405
    .local v12, "today":I
    add-int/lit8 v13, v5, 0x1

    .line 407
    .local v13, "tomorrow":I
    const/4 v14, 0x6

    if-ne v12, v14, :cond_13

    .line 408
    const/4 v13, 0x0

    .line 410
    :cond_13
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    aget v14, v14, v12

    const/4 v15, 0x1

    if-ne v14, v15, :cond_30

    .line 411
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->day_text:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->today:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    .end local v12    # "today":I
    .end local v13    # "tomorrow":I
    :cond_14
    :goto_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->widgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$400(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    move-result-object v14

    sget-object v15, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    if-eq v14, v15, :cond_34

    .line 423
    if-nez p1, :cond_32

    .line 424
    sget v14, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setBackgroundResource(I)V

    .line 425
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->getCount()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_15

    .line 426
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mAlarmDivider:Landroid/view/View;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    .line 441
    :cond_15
    :goto_8
    new-instance v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v14, v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;ILcom/vlingo/core/internal/util/Alarm;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 460
    return-object p2

    .line 180
    .end local v3    # "calendar":Ljava/util/Calendar;
    .end local v4    # "color":I
    .end local v5    # "day":I
    .end local v7    # "mask":I
    .end local v10    # "timeFormat24State":Z
    .end local v11    # "timeLayoutContentDescription":Ljava/lang/String;
    :cond_16
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->title:Landroid/widget/TextView;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 182
    :cond_17
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->title:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->voice_input_control_alarm:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 188
    :cond_18
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isNewAlarmConcept()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 189
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->dayContainer:Landroid/widget/LinearLayout;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 190
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->textContainer:Landroid/widget/LinearLayout;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 191
    const-string/jumbo v14, "system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v14}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v9

    .line 192
    .restart local v9    # "samsungSans":Landroid/graphics/Typeface;
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->day_text:Landroid/widget/TextView;

    invoke-virtual {v14, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 193
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->day_text:Landroid/widget/TextView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 204
    .end local v9    # "samsungSans":Landroid/graphics/Typeface;
    :cond_19
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/4 v15, 0x4

    if-lt v14, v15, :cond_1a

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xc

    if-ge v14, v15, :cond_1a

    .line 205
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am_morning:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    .line 206
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am_morning:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 207
    :cond_1a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xc

    if-lt v14, v15, :cond_1b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0x11

    if-ge v14, v15, :cond_1b

    .line 208
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am_day:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    .line 209
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am_day:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 211
    :cond_1b
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm_evening:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    .line 212
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm_evening:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 215
    :cond_1c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xc

    if-le v14, v15, :cond_1d

    .line 216
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    .line 217
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 218
    :cond_1d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    if-nez v14, :cond_1e

    .line 219
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    .line 220
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 221
    :cond_1e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xc

    if-ne v14, v15, :cond_1f

    .line 222
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    .line 223
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 225
    :cond_1f
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    .line 226
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 230
    :cond_20
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "ru-RU"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_24

    .line 231
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    if-ltz v14, :cond_21

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/4 v15, 0x4

    if-ge v14, v15, :cond_21

    .line 232
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm_night:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 233
    :cond_21
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/4 v15, 0x4

    if-lt v14, v15, :cond_22

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xc

    if-ge v14, v15, :cond_22

    .line 234
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am_morning:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 235
    :cond_22
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xc

    if-lt v14, v15, :cond_23

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0x11

    if-ge v14, v15, :cond_23

    .line 236
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am_day:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 238
    :cond_23
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm_evening:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 241
    :cond_24
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xc

    if-le v14, v15, :cond_25

    .line 242
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 243
    :cond_25
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    if-nez v14, :cond_26

    .line 244
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 245
    :cond_26
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xc

    if-ne v14, v15, :cond_27

    .line 246
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->pm:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 248
    :cond_27
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->am:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 261
    .restart local v10    # "timeFormat24State":Z
    .restart local v11    # "timeLayoutContentDescription":Ljava/lang/String;
    :cond_28
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "ko-KR"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2b

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v14

    if-eqz v14, :cond_2b

    .line 262
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_previous:Landroid/widget/TextView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 265
    :goto_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    if-nez v14, :cond_29

    .line 266
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    const/16 v15, 0xc

    # setter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I
    invoke-static {v14, v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$202(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)I

    .line 268
    :cond_29
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xc

    if-le v14, v15, :cond_2a

    .line 269
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    const/16 v15, 0xc

    # -= operator for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I
    invoke-static {v14, v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$220(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)I

    .line 271
    :cond_2a
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I
    invoke-static {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mMin:I
    invoke-static {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_3

    .line 264
    :cond_2b
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->ampm_next:Landroid/widget/TextView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_9

    .line 279
    :cond_2c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v14

    const/16 v15, 0xa

    if-ge v14, v15, :cond_2e

    .line 280
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_2d

    .line 281
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mHourText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I
    invoke-static {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    const/high16 v14, 0x41a80000    # 21.0f

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v15

    iget v15, v15, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v14, v15

    float-to-int v8, v14

    .line 283
    .local v8, "padding":I
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->itemAlarmCenter:Landroid/widget/LinearLayout;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v14, v8, v15, v0, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto/16 :goto_4

    .line 285
    .end local v8    # "padding":I
    :cond_2d
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mHourText:Landroid/widget/TextView;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "0"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    move-object/from16 v16, v0

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I
    invoke-static/range {v16 .. v16}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 289
    :cond_2e
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mHourText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I
    invoke-static {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 298
    :cond_2f
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mMinText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mMin:I
    invoke-static {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 305
    :pswitch_1
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->enableAlarm:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$drawable;->voice_talk_alarm_ic_on:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_6

    .line 413
    .restart local v3    # "calendar":Ljava/util/Calendar;
    .restart local v4    # "color":I
    .restart local v5    # "day":I
    .restart local v7    # "mask":I
    .restart local v12    # "today":I
    .restart local v13    # "tomorrow":I
    :cond_30
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v14, v14, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    aget v14, v14, v13

    const/4 v15, 0x1

    if-ne v14, v15, :cond_31

    .line 414
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->day_text:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v15}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->tomorrow:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 417
    :cond_31
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->dayContainer:Landroid/widget/LinearLayout;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 418
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->textContainer:Landroid/widget/LinearLayout;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 419
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->day_text:Landroid/widget/TextView;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 428
    .end local v12    # "today":I
    .end local v13    # "tomorrow":I
    :cond_32
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->getCount()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    move/from16 v0, p1

    if-lt v0, v14, :cond_33

    .line 429
    sget v14, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setBackgroundResource(I)V

    .line 430
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mAlarmDivider:Landroid/view/View;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_8

    .line 432
    :cond_33
    sget v14, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_8

    .line 438
    :cond_34
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$ViewHolder;->mAlarmDivider:Landroid/view/View;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_8

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
