.class public Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;
.super Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;
.source "MemoChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/vlingo/core/internal/memo/Memo;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->context:Landroid/content/Context;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 34
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 3
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "memos":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 58
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 60
    if-nez p1, :cond_0

    .line 68
    :goto_0
    return-void

    .line 64
    :cond_0
    new-instance v0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;

    invoke-direct {v0, p0, p1}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;Ljava/util/List;)V

    .line 65
    .local v0, "adapter":Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 66
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 67
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onFinishInflate()V

    .line 52
    sget v0, Lcom/vlingo/midas/R$id;->memo_listview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->mListView:Landroid/widget/ListView;

    .line 53
    return-void
.end method

.method public onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 205
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->mListView:Landroid/widget/ListView;

    sget v1, Lcom/vlingo/midas/R$dimen;->item_memo_choice_height:I

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->measureListviewHeight(Landroid/widget/ListView;IZ)V

    .line 206
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onMeasure(II)V

    .line 207
    return-void
.end method
