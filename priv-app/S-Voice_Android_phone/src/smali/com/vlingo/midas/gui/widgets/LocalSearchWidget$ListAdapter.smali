.class Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;
.super Landroid/widget/BaseAdapter;
.source "LocalSearchWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)V
    .locals 0

    .prologue
    .line 573
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;

    .prologue
    .line 573
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    .line 576
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->localSearchListings:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$100(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 577
    .local v0, "naturalSize":I
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    invoke-virtual {v2, v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->getLimitedCount(I)I

    move-result v1

    .line 578
    .local v1, "toReturn":I
    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 583
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->localSearchListings:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$100(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 588
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 596
    if-nez p2, :cond_1

    .line 597
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v8}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$layout;->item_local_search:I

    const/4 v10, 0x0

    invoke-virtual {v8, v9, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 598
    new-instance v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;

    const/4 v8, 0x0

    invoke-direct {v3, v8}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;)V

    .line 599
    .local v3, "holder":Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;
    sget v8, Lcom/vlingo/midas/R$id;->ls_item_name:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->name:Landroid/widget/TextView;

    .line 600
    sget v8, Lcom/vlingo/midas/R$id;->ls_item_rate_img:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RatingBar;

    iput-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->rate_img:Landroid/widget/RatingBar;

    .line 601
    sget v8, Lcom/vlingo/midas/R$id;->ls_item_review_count:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->review_count_num:Landroid/widget/TextView;

    .line 602
    sget v8, Lcom/vlingo/midas/R$id;->ls_item_address:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->address:Landroid/widget/TextView;

    .line 603
    sget v8, Lcom/vlingo/midas/R$id;->ls_item_call:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    iput-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->call:Landroid/widget/ImageButton;

    .line 604
    sget v8, Lcom/vlingo/midas/R$id;->local_search_info:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->content:Landroid/view/View;

    .line 605
    sget v8, Lcom/vlingo/midas/R$id;->local_list_divider:I

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->divider:Landroid/view/View;

    .line 606
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 610
    :goto_0
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->localSearchListings:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$100(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    .line 613
    .local v1, "curListing":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 616
    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getRating()D

    move-result-wide v4

    .line 618
    .local v4, "rate":D
    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->rate_img:Landroid/widget/RatingBar;

    double-to-float v9, v4

    invoke-virtual {v8, v9}, Landroid/widget/RatingBar;->setRating(F)V

    .line 621
    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getReviewCount()I

    move-result v6

    .line 623
    .local v6, "review_count":I
    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->review_count_num:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 627
    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getFullAddress()Ljava/lang/String;

    move-result-object v0

    .line 628
    .local v0, "addressString":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getDistanceString()Ljava/lang/String;

    move-result-object v2

    .line 629
    .local v2, "distance":Ljava/lang/String;
    const-string/jumbo v8, "\n"

    invoke-virtual {v0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 630
    .local v7, "token":[Ljava/lang/String;
    array-length v8, v7

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    .line 631
    const/4 v8, 0x0

    aget-object v0, v7, v8

    .line 636
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 637
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 639
    :cond_0
    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->address:Landroid/widget/TextView;

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 642
    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-static {}, Lcom/vlingo/core/internal/util/CorePackageInfoProvider;->hasDialing()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 644
    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->call:Landroid/widget/ImageButton;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 645
    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->call:Landroid/widget/ImageButton;

    new-instance v9, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;

    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getPhoneNumber()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 655
    :goto_2
    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->content:Landroid/view/View;

    new-instance v9, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    invoke-direct {v9, v10, v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 657
    if-nez p1, :cond_4

    .line 658
    sget v8, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    invoke-virtual {p2, v8}, Landroid/view/View;->setBackgroundResource(I)V

    .line 665
    :goto_3
    return-object p2

    .line 608
    .end local v0    # "addressString":Ljava/lang/String;
    .end local v1    # "curListing":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .end local v2    # "distance":Ljava/lang/String;
    .end local v3    # "holder":Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;
    .end local v4    # "rate":D
    .end local v6    # "review_count":I
    .end local v7    # "token":[Ljava/lang/String;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;

    .restart local v3    # "holder":Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;
    goto/16 :goto_0

    .line 633
    .restart local v0    # "addressString":Ljava/lang/String;
    .restart local v1    # "curListing":Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    .restart local v2    # "distance":Ljava/lang/String;
    .restart local v4    # "rate":D
    .restart local v6    # "review_count":I
    .restart local v7    # "token":[Ljava/lang/String;
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    aget-object v9, v7, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v9, v7, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 648
    :cond_3
    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->call:Landroid/widget/ImageButton;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 649
    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->call:Landroid/widget/ImageButton;

    new-instance v9, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter$1;

    invoke-direct {v9, p0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;)V

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 659
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne p1, v8, :cond_5

    .line 660
    iget-object v8, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;->divider:Landroid/view/View;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 661
    sget v8, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v8}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3

    .line 663
    :cond_5
    sget v8, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v8}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3
.end method
