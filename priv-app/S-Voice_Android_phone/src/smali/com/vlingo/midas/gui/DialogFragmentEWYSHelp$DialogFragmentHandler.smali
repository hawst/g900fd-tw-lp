.class Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$DialogFragmentHandler;
.super Landroid/os/Handler;
.source "DialogFragmentEWYSHelp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DialogFragmentHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .prologue
    .line 1905
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1906
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$DialogFragmentHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 1907
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 1910
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$DialogFragmentHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .line 1911
    .local v8, "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1912
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1962
    :cond_0
    :goto_0
    return-void

    .line 1916
    :pswitch_0
    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v2, Lcom/vlingo/midas/R$layout;->widget_memo:I

    invoke-static {v0, v2, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 1919
    .local v9, "v1":Landroid/view/View;
    const/4 v3, 0x0

    .line 1920
    .local v3, "memoStr":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "zh_CN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1921
    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/vlingo/midas/R$string;->help_wcis_memo:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1926
    :goto_1
    const/4 v6, 0x0

    .line 1927
    .local v6, "contentStr":Ljava/lang/String;
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mNewTxt:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1928
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mNewTxt:Ljava/lang/String;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1930
    :cond_1
    if-eqz v6, :cond_4

    .line 1931
    sget v0, Lcom/vlingo/midas/R$id;->msg_body:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1935
    :goto_2
    invoke-virtual {v8, v9, v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 1937
    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v2, Lcom/vlingo/midas/R$layout;->dialog_bubble_vlingo_s:I

    invoke-static {v0, v2, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/DialogBubble;

    .line 1938
    .local v7, "db":Lcom/vlingo/midas/gui/DialogBubble;
    invoke-virtual {v7, v8}, Lcom/vlingo/midas/gui/DialogBubble;->setOnSlideTextListenr(Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;)V

    .line 1939
    sget-object v0, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/vlingo/midas/R$string;->core_car_tts_MEMO_SAVED:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v0, v2}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    .line 1940
    invoke-virtual {v8, v7, v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doAddView(Landroid/view/View;Z)V

    .line 1942
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->core_car_tts_MEMO_SAVED:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 1943
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isTOSAccepted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1952
    invoke-static {}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getInstance()Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    # setter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->tts:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$1102(Landroid/speech/tts/TextToSpeech;)Landroid/speech/tts/TextToSpeech;

    .line 1953
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->tts:Landroid/speech/tts/TextToSpeech;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$1100()Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1954
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->tts:Landroid/speech/tts/TextToSpeech;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$1100()Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->core_car_tts_MEMO_SAVED:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v10}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto/16 :goto_0

    .line 1922
    .end local v6    # "contentStr":Ljava/lang/String;
    .end local v7    # "db":Lcom/vlingo/midas/gui/DialogBubble;
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "fr_FR"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1923
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_memo:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1925
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/vlingo/midas/R$string;->help_wcis_memo:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1933
    .restart local v6    # "contentStr":Ljava/lang/String;
    :cond_4
    sget v0, Lcom/vlingo/midas/R$id;->msg_body:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    sget-object v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mNewTxt:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1912
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
