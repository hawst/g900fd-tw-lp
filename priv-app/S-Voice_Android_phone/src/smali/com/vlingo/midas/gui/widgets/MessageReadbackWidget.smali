.class public Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;
.super Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;
.source "MessageReadbackWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final DETAIL:I

.field private final RETIRED:I

.field private callbackBtn:Landroid/widget/Button;

.field private replyBtn:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->DETAIL:I

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->RETIRED:I

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 5
    .param p1, "messageType"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const/4 v4, 0x0

    const/16 v1, 0x8

    .line 83
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 85
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->msgBody:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 87
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->text:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->messageReadBackButtonContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    .line 95
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->messageReadBackButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 98
    :cond_3
    sget-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_4

    .line 99
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 102
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    .line 103
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_5
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 23
    check-cast p1, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->messageContainer:Landroid/widget/LinearLayout;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->openNativeSmsApp()V

    .line 147
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->onFinishInflate()V

    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->messageContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    sget v0, Lcom/vlingo/midas/R$id;->btn_reply:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    .line 40
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    :cond_0
    sget v0, Lcom/vlingo/midas/R$id;->btn_callback:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    .line 60
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget$2;-><init>(Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    :cond_1
    return-void
.end method

.method public retire()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 124
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->retire()V

    .line 126
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->replyBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->callbackBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->messageReadBackButtonContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->messageReadBackButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 138
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 140
    return-void
.end method
