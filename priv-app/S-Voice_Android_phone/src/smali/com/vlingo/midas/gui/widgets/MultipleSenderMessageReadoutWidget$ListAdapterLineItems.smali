.class Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;
.super Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;
.source "MultipleSenderMessageReadoutWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListAdapterLineItems"
.end annotation


# instance fields
.field DEFAULT_COLOR:[I

.field private DEFAULT_IMG:[I

.field private img_cnt:I

.field private final list:Ljava/util/List;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 7
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "lineItems":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 101
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    .line 102
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;Landroid/content/Context;Ljava/util/List;)V

    .line 86
    iput v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->img_cnt:I

    .line 87
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_01:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_02:I

    aput v1, v0, v3

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_03:I

    aput v1, v0, v4

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_04:I

    aput v1, v0, v5

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_05:I

    aput v1, v0, v6

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->DEFAULT_IMG:[I

    .line 94
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_1_bg_color:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_2_bg_color:I

    aput v1, v0, v3

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_3_bg_color:I

    aput v1, v0, v4

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_4_bg_color:I

    aput v1, v0, v5

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_5_bg_color:I

    aput v1, v0, v6

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->DEFAULT_COLOR:[I

    .line 103
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->list:Ljava/util/List;

    .line 104
    return-void
.end method

.method private getDisplayMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Z)Ljava/lang/String;
    .locals 6
    .param p1, "alert"    # Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .param p2, "showMessageBody"    # Z

    .prologue
    const/16 v5, 0x1e

    const/16 v4, 0x19

    const/4 v3, 0x0

    .line 108
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->context:Landroid/content/Context;

    invoke-virtual {p1, v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getDisplayableMessageText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "body":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 110
    move-object v1, v0

    .line 112
    .local v1, "msg":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-nez v2, :cond_1

    .line 113
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v5, :cond_0

    .line 114
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 128
    :cond_0
    :goto_0
    return-object v1

    .line 119
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_0

    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 126
    .end local v1    # "msg":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->core_car_safereader_hidden_message_body:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "msg":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 33
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->list:Ljava/util/List;

    move-object/from16 v28, v0

    .line 134
    .local v28, "lineItems":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v5

    .line 135
    .local v5, "size":I
    move-object/from16 v0, v28

    move/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;

    .line 137
    .local v27, "lineItem":Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;
    if-nez p2, :cond_0

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$layout;->item_message_details:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 141
    :cond_0
    sget v2, Lcom/vlingo/midas/R$id;->from:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 142
    .local v22, "from":Landroid/widget/TextView;
    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    sget v2, Lcom/vlingo/midas/R$id;->count:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 146
    .local v16, "count":Landroid/widget/TextView;
    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v7

    .line 147
    .local v7, "c":I
    if-eqz v16, :cond_1

    .line 148
    const/4 v2, 0x1

    if-le v7, v2, :cond_3

    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessageCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    :cond_1
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 156
    sget v2, Lcom/vlingo/midas/R$id;->message_image_contact:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageView;

    .line 157
    .local v15, "contact_image":Landroid/widget/ImageView;
    sget v2, Lcom/vlingo/midas/R$id;->date_msg:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    .line 158
    .local v31, "time":Landroid/widget/TextView;
    sget v2, Lcom/vlingo/midas/R$id;->message_row_title_line:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    .line 159
    .local v26, "lineDevider":Landroid/view/View;
    sget v2, Lcom/vlingo/midas/R$id;->relativelayout_1:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/RelativeLayout;

    .line 160
    .local v25, "layout1":Landroid/widget/RelativeLayout;
    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 162
    new-instance v17, Ljava/util/Date;

    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMostRecentTimestamp()J

    move-result-wide v2

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 163
    .local v17, "date":Ljava/util/Date;
    new-instance v20, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss"

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 164
    .local v20, "formatDate":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    .line 165
    .local v8, "InputDate":Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/core/internal/schedule/DateUtil;->isToday(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 166
    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v2

    const/16 v3, 0xa

    if-ge v2, v3, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 167
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    :goto_1
    const/4 v2, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    const/4 v2, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 179
    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v10

    .line 182
    .local v10, "alertQueue1":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getAddress()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->fetchContactIdFromPhoneNumber(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v14

    .line 183
    .local v14, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    const/16 v29, 0x0

    .line 184
    .local v29, "photo":Landroid/graphics/Bitmap;
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;

    .line 185
    .local v13, "contact":Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;
    iget-object v2, v13, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;->contactId:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v13, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;->contactName:Ljava/lang/String;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 186
    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v13, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;->contactName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 187
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, v13, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;->contactId:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v32

    .line 188
    .local v32, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, v32

    invoke-static {v2, v0}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v24

    .line 189
    .local v24, "is":Ljava/io/InputStream;
    if-nez v29, :cond_6

    .line 190
    invoke-static/range {v24 .. v24}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v29

    goto :goto_2

    .line 151
    .end local v8    # "InputDate":Ljava/lang/String;
    .end local v10    # "alertQueue1":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    .end local v13    # "contact":Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;
    .end local v14    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    .end local v15    # "contact_image":Landroid/widget/ImageView;
    .end local v17    # "date":Ljava/util/Date;
    .end local v20    # "formatDate":Ljava/text/SimpleDateFormat;
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v24    # "is":Ljava/io/InputStream;
    .end local v25    # "layout1":Landroid/widget/RelativeLayout;
    .end local v26    # "lineDevider":Landroid/view/View;
    .end local v29    # "photo":Landroid/graphics/Bitmap;
    .end local v31    # "time":Landroid/widget/TextView;
    .end local v32    # "uri":Landroid/net/Uri;
    :cond_3
    const/16 v2, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 169
    .restart local v8    # "InputDate":Ljava/lang/String;
    .restart local v15    # "contact_image":Landroid/widget/ImageView;
    .restart local v17    # "date":Ljava/util/Date;
    .restart local v20    # "formatDate":Ljava/text/SimpleDateFormat;
    .restart local v25    # "layout1":Landroid/widget/RelativeLayout;
    .restart local v26    # "lineDevider":Landroid/view/View;
    .restart local v31    # "time":Landroid/widget/TextView;
    :cond_4
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 172
    :cond_5
    new-instance v18, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "dd/MM"

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 173
    .local v18, "dateMonthFormat":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v21

    .line 174
    .local v21, "formatted":Ljava/lang/String;
    move-object/from16 v0, v31

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 197
    .end local v18    # "dateMonthFormat":Ljava/text/SimpleDateFormat;
    .end local v21    # "formatted":Ljava/lang/String;
    .restart local v10    # "alertQueue1":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    .restart local v14    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    .restart local v23    # "i$":Ljava/util/Iterator;
    .restart local v29    # "photo":Landroid/graphics/Bitmap;
    :cond_6
    if-eqz v29, :cond_9

    .line 198
    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 208
    :goto_3
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 209
    invoke-interface {v10}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 211
    .local v30, "smsMMSAlert1":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    if-eqz v30, :cond_7

    invoke-virtual/range {v30 .. v30}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "MMS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 212
    sget v2, Lcom/vlingo/midas/R$id;->date_msg_src:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 214
    .local v11, "attachedImage":Landroid/widget/ImageView;
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 219
    .end local v8    # "InputDate":Ljava/lang/String;
    .end local v10    # "alertQueue1":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    .end local v11    # "attachedImage":Landroid/widget/ImageView;
    .end local v14    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    .end local v15    # "contact_image":Landroid/widget/ImageView;
    .end local v17    # "date":Ljava/util/Date;
    .end local v20    # "formatDate":Ljava/text/SimpleDateFormat;
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v25    # "layout1":Landroid/widget/RelativeLayout;
    .end local v26    # "lineDevider":Landroid/view/View;
    .end local v29    # "photo":Landroid/graphics/Bitmap;
    .end local v30    # "smsMMSAlert1":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v31    # "time":Landroid/widget/TextView;
    :cond_7
    sget v2, Lcom/vlingo/midas/R$id;->body:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 221
    .local v12, "body":Landroid/widget/TextView;
    const-string/jumbo v19, ""

    .line 222
    .local v19, "displayMessage":Ljava/lang/String;
    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;->getMessagesFromSender()Ljava/util/LinkedList;

    move-result-object v9

    .line 223
    .local v9, "alertQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    invoke-interface {v9}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    .line 224
    .local v6, "smsMMSAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    if-eqz v6, :cond_8

    .line 225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    iget-boolean v2, v2, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->showMessageBody:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v2}, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->getDisplayMessage(Lcom/vlingo/core/internal/messages/SMSMMSAlert;Z)Ljava/lang/String;

    move-result-object v19

    .line 227
    :cond_8
    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    new-instance v2, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;

    move-object/from16 v3, p0

    move/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;IILcom/vlingo/core/internal/messages/SMSMMSAlert;I)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    if-nez p1, :cond_b

    .line 245
    sget v2, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 251
    :goto_4
    return-object p2

    .line 200
    .end local v6    # "smsMMSAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .end local v9    # "alertQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    .end local v12    # "body":Landroid/widget/TextView;
    .end local v19    # "displayMessage":Ljava/lang/String;
    .restart local v8    # "InputDate":Ljava/lang/String;
    .restart local v10    # "alertQueue1":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    .restart local v14    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    .restart local v15    # "contact_image":Landroid/widget/ImageView;
    .restart local v17    # "date":Ljava/util/Date;
    .restart local v20    # "formatDate":Ljava/text/SimpleDateFormat;
    .restart local v23    # "i$":Ljava/util/Iterator;
    .restart local v25    # "layout1":Landroid/widget/RelativeLayout;
    .restart local v26    # "lineDevider":Landroid/view/View;
    .restart local v29    # "photo":Landroid/graphics/Bitmap;
    .restart local v31    # "time":Landroid/widget/TextView;
    :cond_9
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->DEFAULT_COLOR:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->img_cnt:I

    rem-int/lit8 v3, v3, 0x5

    aget v2, v2, v3

    invoke-virtual {v15, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 206
    :goto_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->img_cnt:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->img_cnt:I

    goto/16 :goto_3

    .line 203
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->DEFAULT_IMG:[I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->img_cnt:I

    rem-int/lit8 v4, v4, 0x5

    aget v3, v3, v4

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v29

    .line 204
    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_5

    .line 246
    .end local v8    # "InputDate":Ljava/lang/String;
    .end local v10    # "alertQueue1":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    .end local v14    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    .end local v15    # "contact_image":Landroid/widget/ImageView;
    .end local v17    # "date":Ljava/util/Date;
    .end local v20    # "formatDate":Ljava/text/SimpleDateFormat;
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v25    # "layout1":Landroid/widget/RelativeLayout;
    .end local v26    # "lineDevider":Landroid/view/View;
    .end local v29    # "photo":Landroid/graphics/Bitmap;
    .end local v31    # "time":Landroid/widget/TextView;
    .restart local v6    # "smsMMSAlert":Lcom/vlingo/core/internal/messages/SMSMMSAlert;
    .restart local v9    # "alertQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    .restart local v12    # "body":Landroid/widget/TextView;
    .restart local v19    # "displayMessage":Ljava/lang/String;
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, p1

    if-lt v0, v2, :cond_c

    .line 247
    sget v2, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4

    .line 249
    :cond_c
    sget v2, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4
.end method
