.class Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems$1;
.super Ljava/lang/Object;
.source "MultipleMessageShowWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;

.field final synthetic val$alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

.field final synthetic val$c:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;Lcom/vlingo/core/internal/safereader/SafeReaderAlert;I)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems$1;->this$1:Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems$1;->val$alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    iput p3, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems$1;->val$c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 131
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 132
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    const-string/jumbo v1, "id"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems$1;->val$alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 134
    const-string/jumbo v1, "message_type"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems$1;->val$alert:Lcom/vlingo/core/internal/safereader/SafeReaderAlert;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/safereader/SafeReaderAlert;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const-string/jumbo v1, "from_read_messages"

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    const-string/jumbo v1, "item_count"

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems$1;->val$c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 137
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems$1;->this$1:Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 138
    return-void
.end method
