.class public Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;
.super Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;
.source "MicListeningAnimationViewPhone.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone$MyTimerTask;
    }
.end annotation


# instance fields
.field private mCount:I

.field private mRmsHandler:Landroid/os/Handler;

.field private myTimerTask:Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone$MyTimerTask;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 124
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->mCount:I

    .line 147
    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone$1;-><init>(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->mRmsHandler:Landroid/os/Handler;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;

    .prologue
    .line 20
    iget v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->mCount:I

    return v0
.end method

.method static synthetic access$008(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;

    .prologue
    .line 20
    iget v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->mCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->mCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->mRmsHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public cancelTimer()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 172
    :cond_0
    return-void
.end method

.method public micListeningAnimation(I)V
    .locals 13
    .param p1, "rmsNext"    # I

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const v3, 0x3ccccccd    # 0.025f

    const/4 v5, 0x1

    const v1, 0x3f4ccccd    # 0.8f

    .line 50
    iget v2, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->mRmsBefore:I

    int-to-float v2, v2

    mul-float v11, v2, v3

    .line 51
    .local v11, "scaleBefore":F
    int-to-float v2, p1

    mul-float v12, v2, v3

    .line 52
    .local v12, "scaleNext":F
    iput p1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->mRmsBefore:I

    .line 53
    mul-float/2addr v11, v11

    .line 54
    mul-float/2addr v12, v12

    .line 59
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v2, v1

    move v3, v1

    move v4, v1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 61
    .local v0, "ani":Landroid/view/animation/ScaleAnimation;
    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 62
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setRepeatCount(I)V

    .line 63
    invoke-virtual {v0, v5}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 64
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 65
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->micImages:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/midas/gui/customviews/MicListeningView;

    .line 66
    .local v10, "mImages":Lcom/vlingo/midas/gui/customviews/MicListeningView;
    invoke-virtual {v10, v0}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 68
    .end local v10    # "mImages":Lcom/vlingo/midas/gui/customviews/MicListeningView;
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationView;->onFinishInflate()V

    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->micImages:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->micImages:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->micImages:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->micImages:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer4:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->micImages:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer5:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method public scaleAnimation(F)V
    .locals 6
    .param p1, "ratio"    # F

    .prologue
    .line 141
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->mCount:I

    .line 142
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->timer:Ljava/util/Timer;

    .line 143
    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone$MyTimerTask;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone$MyTimerTask;-><init>(Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->myTimerTask:Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone$MyTimerTask;

    .line 145
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->myTimerTask:Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone$MyTimerTask;

    const-wide/16 v2, 0x190

    const-wide/16 v4, 0x82

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 146
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    if-eqz v0, :cond_2

    .line 117
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer4:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    if-eqz v0, :cond_3

    .line 119
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer4:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer5:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    if-eqz v0, :cond_4

    .line 121
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer5:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 122
    :cond_4
    return-void
.end method

.method public setMicAnimation()V
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 43
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 44
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 45
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 47
    return-void
.end method

.method public showMicListeningAnimation(I)V
    .locals 5
    .param p1, "rmsValue"    # I

    .prologue
    const/16 v4, 0x4a

    const/16 v3, 0x41

    const/16 v1, 0x3a

    const/16 v0, 0x32

    const v2, 0x3f99999a    # 1.2f

    .line 71
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->cancelAllAnimations()V

    .line 72
    if-ge p1, v0, :cond_1

    .line 73
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "New MicAnim Testing : level-1"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3f7851ec    # 0.97f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    if-lt p1, v0, :cond_2

    if-ge p1, v1, :cond_2

    .line 80
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "New MicAnim Testing : level-2"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3f8ccccd    # 1.1f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 82
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    goto :goto_0

    .line 86
    :cond_2
    if-lt p1, v1, :cond_3

    if-ge p1, v3, :cond_3

    .line 87
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "New MicAnim Testing : level-3"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3f933333    # 1.15f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 89
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3fa66666    # 1.3f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 90
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3fb9999a    # 1.45f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    goto :goto_0

    .line 93
    :cond_3
    if-lt p1, v3, :cond_4

    if-ge p1, v4, :cond_4

    .line 94
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "New MicAnim Testing : level-4"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3f947ae1    # 1.16f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 96
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3fa8f5c3    # 1.32f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 97
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3fbd70a4    # 1.48f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer4:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3fd1eb85    # 1.64f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    goto :goto_0

    .line 100
    :cond_4
    if-lt p1, v4, :cond_0

    .line 101
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "New MicAnim Testing : level-5"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer1:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 103
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer2:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3fb33333    # 1.4f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 104
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer3:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3fcccccd    # 1.6f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 105
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer4:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const v1, 0x3fe66666    # 1.8f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    .line 106
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningAnimationViewPhone;->layer5:Lcom/vlingo/midas/gui/customviews/MicListeningView;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->startScaleAnimation(F)V

    goto/16 :goto_0
.end method
