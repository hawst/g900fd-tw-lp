.class public Lcom/vlingo/midas/gui/widgets/WeatherWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "WeatherWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/WeatherWidget$WeatherWidgetHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/weather/WeatherAdaptor;",
        ">;"
    }
.end annotation


# static fields
.field private static final FIFTH_FORECAST_DAY:I = 0x5

.field private static final FIRST_FORECAST_DAY:I = 0x1

.field private static final FOURTH_FORECAST_DAY:I = 0x4

.field private static final SECOND_FORECAST_DAY:I = 0x2

.field private static final SIXTH_FORECAST_DAY:I = 0x6

.field private static final THIRD_FORECAST_DAY:I = 0x3

.field private static final ZEROTH_FORECAST_DAY:I

.field private static forecastArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private currentCondition:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private date1:Landroid/widget/TextView;

.field private date2:Landroid/widget/TextView;

.field private date3:Landroid/widget/TextView;

.field private date4:Landroid/widget/TextView;

.field private date5:Landroid/widget/TextView;

.field private date6:Landroid/widget/TextView;

.field private forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

.field private forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/vlingo/midas/gui/widgets/WeatherWidget$WeatherWidgetHandler;

.field private mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

.field private maxTemperature1:Landroid/widget/TextView;

.field private maxTemperature2:Landroid/widget/TextView;

.field private maxTemperature3:Landroid/widget/TextView;

.field private maxTemperature4:Landroid/widget/TextView;

.field private maxTemperature5:Landroid/widget/TextView;

.field private maxTemperature6:Landroid/widget/TextView;

.field private minTemperature1:Landroid/widget/TextView;

.field private minTemperature2:Landroid/widget/TextView;

.field private minTemperature3:Landroid/widget/TextView;

.field private minTemperature4:Landroid/widget/TextView;

.field private minTemperature5:Landroid/widget/TextView;

.field private minTemperature6:Landroid/widget/TextView;

.field private wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

.field private weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private weatherImage1:Landroid/widget/ImageView;

.field private weatherImage2:Landroid/widget/ImageView;

.field private weatherImage3:Landroid/widget/ImageView;

.field private weatherImage4:Landroid/widget/ImageView;

.field private weatherImage5:Landroid/widget/ImageView;

.field private weatherImage6:Landroid/widget/ImageView;

.field private weekday1:Landroid/widget/TextView;

.field private weekday2:Landroid/widget/TextView;

.field private weekday3:Landroid/widget/TextView;

.field private weekday4:Landroid/widget/TextView;

.field private weekday5:Landroid/widget/TextView;

.field private weekday6:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 346
    new-instance v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget$WeatherWidgetHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget$WeatherWidgetHandler;-><init>(Lcom/vlingo/midas/gui/widgets/WeatherWidget;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/WeatherWidget$WeatherWidgetHandler;

    .line 111
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->mContext:Landroid/content/Context;

    .line 112
    new-instance v0, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-direct {v0}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/WeatherWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/WeatherWidget;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->setValuesForcastWeather()V

    return-void
.end method

.method private setValuesForcastWeather()V
    .locals 11

    .prologue
    const/16 v10, 0x14

    const/16 v9, 0xb0

    .line 350
    new-instance v5, Lcom/vlingo/midas/util/WeatherResourceUtil;

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getCurrentWeatherCode()I

    move-result v6

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getProvider()Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/vlingo/midas/util/WeatherResourceUtil;-><init>(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)V

    .line 352
    .local v5, "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    .line 353
    .local v3, "locale":Ljava/util/Locale;
    const/4 v1, 0x0

    .line 354
    .local v1, "forecastArrayListSize":I
    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    .line 355
    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 356
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 357
    const-string/jumbo v6, "#d7d7d7"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    .line 358
    .local v4, "textColor":I
    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 360
    .local v0, "day":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-static {v6, v10}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    .line 361
    const-string/jumbo v6, "#ff6633"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    .line 365
    :cond_1
    :goto_1
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getWeatherCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->setWeatherCode(Ljava/lang/String;)V

    .line 367
    packed-switch v2, :pswitch_data_0

    .line 356
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 362
    :cond_2
    const/4 v6, 0x7

    invoke-static {v6, v10}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    .line 363
    const-string/jumbo v6, "#6699cc"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 369
    :pswitch_0
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday1:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday1:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 371
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date1:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage1:Landroid/widget/ImageView;

    invoke-virtual {v5}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 374
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature1:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 378
    :pswitch_1
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday2:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday2:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 380
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date2:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage2:Landroid/widget/ImageView;

    invoke-virtual {v5}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 382
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature2:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature2:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 386
    :pswitch_2
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday3:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 387
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday3:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 388
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date3:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 389
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage3:Landroid/widget/ImageView;

    invoke-virtual {v5}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 390
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature3:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 391
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature3:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 395
    :pswitch_3
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday4:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday4:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 397
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date4:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage4:Landroid/widget/ImageView;

    invoke-virtual {v5}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 399
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature4:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 400
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature4:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 404
    :pswitch_4
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday5:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday5:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 406
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date5:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage5:Landroid/widget/ImageView;

    invoke-virtual {v5}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 408
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature5:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 409
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature5:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 413
    :pswitch_5
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday6:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDays()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 414
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday6:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 415
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date6:Landroid/widget/TextView;

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 416
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage6:Landroid/widget/ImageView;

    invoke-virtual {v5}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 417
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature6:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 418
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature6:Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 422
    .end local v0    # "day":Ljava/lang/String;
    .end local v4    # "textColor":I
    :cond_3
    return-void

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/weather/WeatherAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "adaptor"    # Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 168
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 169
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    .line 170
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->onResponseReceived()V

    .line 173
    return-void
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 39
    check-cast p1, Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->initialize(Lcom/vlingo/core/internal/weather/WeatherAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 122
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday1:Landroid/widget/TextView;

    .line 123
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday2:Landroid/widget/TextView;

    .line 124
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday3:Landroid/widget/TextView;

    .line 125
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday4:Landroid/widget/TextView;

    .line 126
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday5:Landroid/widget/TextView;

    .line 127
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weekday6:Landroid/widget/TextView;

    .line 130
    sget v0, Lcom/vlingo/midas/R$id;->child_date1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date1:Landroid/widget/TextView;

    .line 131
    sget v0, Lcom/vlingo/midas/R$id;->child_date2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date2:Landroid/widget/TextView;

    .line 132
    sget v0, Lcom/vlingo/midas/R$id;->child_date3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date3:Landroid/widget/TextView;

    .line 133
    sget v0, Lcom/vlingo/midas/R$id;->child_date4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date4:Landroid/widget/TextView;

    .line 134
    sget v0, Lcom/vlingo/midas/R$id;->child_date5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date5:Landroid/widget/TextView;

    .line 135
    sget v0, Lcom/vlingo/midas/R$id;->child_date6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->date6:Landroid/widget/TextView;

    .line 137
    sget v0, Lcom/vlingo/midas/R$id;->child_image1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage1:Landroid/widget/ImageView;

    .line 138
    sget v0, Lcom/vlingo/midas/R$id;->child_image2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage2:Landroid/widget/ImageView;

    .line 139
    sget v0, Lcom/vlingo/midas/R$id;->child_image3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage3:Landroid/widget/ImageView;

    .line 140
    sget v0, Lcom/vlingo/midas/R$id;->child_image4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage4:Landroid/widget/ImageView;

    .line 141
    sget v0, Lcom/vlingo/midas/R$id;->child_image5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage5:Landroid/widget/ImageView;

    .line 142
    sget v0, Lcom/vlingo/midas/R$id;->child_image6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherImage6:Landroid/widget/ImageView;

    .line 144
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature1:Landroid/widget/TextView;

    .line 145
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature2:Landroid/widget/TextView;

    .line 146
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature3:Landroid/widget/TextView;

    .line 147
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature4:Landroid/widget/TextView;

    .line 148
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature5:Landroid/widget/TextView;

    .line 149
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->minTemperature6:Landroid/widget/TextView;

    .line 151
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature1:Landroid/widget/TextView;

    .line 152
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature2:Landroid/widget/TextView;

    .line 153
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature3:Landroid/widget/TextView;

    .line 154
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature4:Landroid/widget/TextView;

    .line 155
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature5:Landroid/widget/TextView;

    .line 156
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->maxTemperature6:Landroid/widget/TextView;

    .line 157
    return-void
.end method

.method public onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 163
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 164
    return-void
.end method

.method public declared-synchronized onResponseReceived()V
    .locals 31

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    new-instance v23, Ljava/text/SimpleDateFormat;

    const-string/jumbo v28, "EEE"

    sget-object v29, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 177
    .local v23, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v24, Ljava/text/SimpleDateFormat;

    const-string/jumbo v28, "yyyy-MM-dd"

    sget-object v29, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v24

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 179
    .local v24, "sdf2":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v28, v0

    if-eqz v28, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v28

    if-nez v28, :cond_1

    .line 183
    :cond_0
    new-instance v15, Landroid/content/Intent;

    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    .line 184
    .local v15, "intent":Landroid/content/Intent;
    const-string/jumbo v28, "com.vlingo.core.internal.dialogmanager.NoData"

    move-object/from16 v0, v28

    invoke-virtual {v15, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-interface {v0, v15, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    .end local v15    # "intent":Landroid/content/Intent;
    :goto_0
    monitor-exit p0

    return-void

    .line 189
    :cond_1
    const/16 v27, 0x0

    .line 190
    .local v27, "weatherDetailsCountChild":I
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v28, v0

    if-eqz v28, :cond_2

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v27

    .line 193
    :cond_2
    const/4 v13, 0x0

    .local v13, "index":I
    :goto_1
    move/from16 v0, v27

    if-ge v13, v0, :cond_a

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v3

    .line 195
    .local v3, "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    const-string/jumbo v28, "CurrentCondition"

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 198
    const-string/jumbo v28, "CurrentCondition"

    const/16 v29, 0x0

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 199
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-virtual {v3, v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->currentCondition:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->currentCondition:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/util/Map$Entry;

    goto :goto_2

    .line 208
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_3
    const-string/jumbo v28, "Forecasts"

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 212
    sget-object v28, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    if-eqz v28, :cond_4

    .line 213
    const/16 v28, 0x0

    sput-object v28, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    .line 215
    :cond_4
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    sput-object v28, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    .line 216
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 219
    const/4 v9, 0x0

    .line 220
    .local v9, "forecastsCountChild":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v9

    .line 222
    const/4 v14, 0x0

    .local v14, "index1":I
    :goto_3
    if-ge v14, v9, :cond_9

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v14}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v8

    .line 225
    .local v8, "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-virtual {v8, v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v28

    const/16 v29, 0x0

    invoke-virtual/range {v28 .. v29}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v7

    .line 228
    .local v7, "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    const/16 v28, 0x1

    move/from16 v0, v28

    invoke-virtual {v8, v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v28

    const/16 v29, 0x0

    invoke-virtual/range {v28 .. v29}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v22

    .line 231
    .local v22, "nit":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-virtual {v8}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v28

    const-string/jumbo v29, "ForecastDate"

    invoke-interface/range {v28 .. v29}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 232
    .local v4, "dateStr":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v28

    const-string/jumbo v29, "WeatherCode"

    invoke-interface/range {v28 .. v29}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    .local v26, "weatherCode":Ljava/lang/String;
    move-object v6, v4

    .line 240
    .local v6, "dateValue":Ljava/lang/String;
    :try_start_2
    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v25

    .line 241
    .local v25, "wDate":Ljava/util/Date;
    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 245
    .end local v25    # "wDate":Ljava/util/Date;
    :goto_4
    :try_start_3
    invoke-virtual {v7}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v28

    const-string/jumbo v29, "TempMax"

    invoke-interface/range {v28 .. v29}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 246
    .local v16, "max":Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v28

    const-string/jumbo v29, "TempMin"

    invoke-interface/range {v28 .. v29}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 256
    .local v18, "min":Ljava/lang/String;
    packed-switch v14, :pswitch_data_0

    .line 222
    :goto_5
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 265
    :pswitch_0
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v29, 0x5

    const/16 v30, 0x7

    move/from16 v0, v29

    move/from16 v1, v30

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string/jumbo v29, "/"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const/16 v29, 0x8

    const/16 v30, 0xa

    move/from16 v0, v29

    move/from16 v1, v30

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 266
    .local v5, "dateVal":Ljava/lang/String;
    new-instance v28, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct/range {v28 .. v29}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;-><init>(Lcom/vlingo/core/internal/weather/WeatherInfoUtil;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    .line 267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->setDays(Ljava/lang/String;)V

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->setDate(Ljava/lang/String;)V

    .line 269
    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v19, v0

    .line 270
    .local v19, "minTemp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v28, v0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string/jumbo v30, ""

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->setMinTemp(Ljava/lang/String;)V

    .line 271
    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v17, v0

    .line 272
    .local v17, "maxTemp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v28, v0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string/jumbo v30, ""

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->setMaxTemp(Ljava/lang/String;)V

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;->setWeatherCode(Ljava/lang/String;)V

    .line 274
    sget-object v28, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastArrayList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->forecastingWeekly:Lcom/vlingo/core/internal/weather/WeatherInfoUtil$ForecastingWeekly;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    const/16 v16, 0x0

    .line 277
    const/16 v18, 0x0

    .line 278
    const/4 v5, 0x0

    .line 279
    goto/16 :goto_5

    .line 283
    .end local v4    # "dateStr":Ljava/lang/String;
    .end local v5    # "dateVal":Ljava/lang/String;
    .end local v6    # "dateValue":Ljava/lang/String;
    .end local v7    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v8    # "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v9    # "forecastsCountChild":I
    .end local v14    # "index1":I
    .end local v16    # "max":Ljava/lang/String;
    .end local v17    # "maxTemp":I
    .end local v18    # "min":Ljava/lang/String;
    .end local v19    # "minTemp":I
    .end local v22    # "nit":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v26    # "weatherCode":Ljava/lang/String;
    :cond_5
    const-string/jumbo v28, "Images"

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 288
    const/4 v12, 0x0

    .line 292
    .local v12, "imagesCountChild":I
    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v12

    .line 294
    const/4 v14, 0x0

    .restart local v14    # "index1":I
    :goto_6
    if-ge v14, v12, :cond_9

    .line 295
    invoke-virtual {v3, v14}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v11

    .line 296
    .local v11, "image":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-virtual {v11}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/util/Map$Entry;

    goto :goto_7

    .line 294
    :cond_6
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 302
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "image":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v12    # "imagesCountChild":I
    .end local v14    # "index1":I
    :cond_7
    const-string/jumbo v28, "MoonPhases"

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 308
    const/16 v21, 0x0

    .line 309
    .local v21, "moonPhasesCountChild":I
    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v21

    .line 310
    const/4 v14, 0x0

    .restart local v14    # "index1":I
    :goto_8
    move/from16 v0, v21

    if-ge v14, v0, :cond_9

    .line 311
    invoke-virtual {v3, v14}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChild(I)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v20

    .line 313
    .local v20, "moonPhase":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :goto_9
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/util/Map$Entry;

    goto :goto_9

    .line 310
    :cond_8
    add-int/lit8 v14, v14, 0x1

    goto :goto_8

    .line 193
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v14    # "index1":I
    .end local v20    # "moonPhase":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v21    # "moonPhasesCountChild":I
    :cond_9
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 323
    .end local v3    # "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    const-string/jumbo v29, "com.vlingo.midas"

    const-string/jumbo v30, "WEAT"

    invoke-static/range {v28 .. v30}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 176
    .end local v13    # "index":I
    .end local v23    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v24    # "sdf2":Ljava/text/SimpleDateFormat;
    .end local v27    # "weatherDetailsCountChild":I
    :catchall_0
    move-exception v28

    monitor-exit p0

    throw v28

    .line 242
    .restart local v3    # "child":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v4    # "dateStr":Ljava/lang/String;
    .restart local v6    # "dateValue":Ljava/lang/String;
    .restart local v7    # "day":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v8    # "forecast":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v9    # "forecastsCountChild":I
    .restart local v13    # "index":I
    .restart local v14    # "index1":I
    .restart local v22    # "nit":Lcom/vlingo/core/internal/weather/WeatherElement;
    .restart local v23    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v24    # "sdf2":Ljava/text/SimpleDateFormat;
    .restart local v26    # "weatherCode":Ljava/lang/String;
    .restart local v27    # "weatherDetailsCountChild":I
    :catch_0
    move-exception v28

    goto/16 :goto_4

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
