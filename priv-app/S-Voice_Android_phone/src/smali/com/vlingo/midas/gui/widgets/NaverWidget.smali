.class public Lcom/vlingo/midas/gui/widgets/NaverWidget;
.super Lcom/vlingo/midas/gui/Widget;
.source "NaverWidget.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/Widget",
        "<",
        "Lcom/vlingo/midas/naver/NaverAdaptor;",
        ">;"
    }
.end annotation


# instance fields
.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mContentView:Landroid/widget/TextView;

.field private final mContext:Landroid/content/Context;

.field private wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/Widget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method private playTTSforNaver(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/midas/naver/NaverXMLParser;)V
    .locals 1
    .param p1, "handler"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p2, "xmlResult"    # Lcom/vlingo/midas/naver/NaverXMLParser;

    .prologue
    .line 211
    iget-object v0, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->ttsText:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->ttsText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 212
    iget-object v0, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->ttsText:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 214
    :cond_0
    return-void
.end method

.method private showVlingoTextAndTTSforNaver(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/midas/naver/NaverXMLParser;)V
    .locals 2
    .param p1, "handler"    # Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .param p2, "xmlResult"    # Lcom/vlingo/midas/naver/NaverXMLParser;

    .prologue
    .line 201
    iget-object v0, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->ttsText:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->ttsText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 202
    iget-object v0, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->ttsText:Ljava/lang/String;

    iget-object v1, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->ttsText:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->svoiceQuery:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->svoiceQuery:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 205
    iget-object v0, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->svoiceQuery:Ljava/lang/String;

    iget-object v1, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->ttsText:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 207
    :cond_1
    iget-object v0, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->query:Ljava/lang/String;

    iget-object v1, p2, Lcom/vlingo/midas/naver/NaverXMLParser;->ttsText:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showVlingoTextAndTTS(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public drawNHNLogo()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 217
    sget v5, Lcom/vlingo/midas/R$id;->naver_container:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 218
    .local v1, "gp":Landroid/view/ViewGroup;
    const/4 v0, 0x2

    .line 219
    .local v0, "density":I
    new-instance v4, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 220
    .local v4, "rl":Landroid/widget/RelativeLayout;
    new-instance v2, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContext:Landroid/content/Context;

    invoke-direct {v2, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 221
    .local v2, "logoView":Landroid/widget/ImageView;
    sget v5, Lcom/vlingo/midas/R$drawable;->by_naver:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 222
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v5, 0x74

    const/16 v6, 0x20

    invoke-direct {v3, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 223
    .local v3, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v5, 0xb

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 224
    const/16 v5, 0xa

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 225
    const/16 v5, 0x18

    invoke-virtual {v3, v7, v7, v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 227
    invoke-virtual {v4, v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 228
    invoke-virtual {v1, v4, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 231
    return-void
.end method

.method public initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 17
    .param p1, "adaptor"    # Lcom/vlingo/midas/naver/NaverAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 46
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/NaverWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 47
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/NaverWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    .line 50
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    if-eqz v13, :cond_15

    .line 52
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v13}, Lcom/vlingo/midas/naver/NaverAdaptor;->getVVSActionHandlerListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v6

    .line 57
    .local v6, "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v13}, Lcom/vlingo/midas/naver/NaverAdaptor;->getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v12

    .line 59
    .local v12, "xmlResult":Lcom/vlingo/midas/naver/NaverXMLParser;
    iget v13, v12, Lcom/vlingo/midas/naver/NaverXMLParser;->ErrorCode:I

    if-nez v13, :cond_14

    .line 61
    iget-object v8, v12, Lcom/vlingo/midas/naver/NaverXMLParser;->itemList:Ljava/util/List;

    .line 62
    .local v8, "itemList":Ljava/util/List;
    const/4 v4, 0x0

    .line 64
    .local v4, "ResId":I
    iget-object v13, v12, Lcom/vlingo/midas/naver/NaverXMLParser;->domain:Ljava/lang/String;

    const-string/jumbo v14, "MOVIE"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    if-eqz v8, :cond_5

    .line 66
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v12}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->playTTSforNaver(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/midas/naver/NaverXMLParser;)V

    .line 67
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_2

    .line 68
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContext:Landroid/content/Context;

    sget v15, Lcom/vlingo/midas/R$layout;->widget_movie:I

    sget v13, Lcom/vlingo/midas/R$id;->naver_container:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    invoke-static {v14, v15, v13}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 69
    .local v11, "vg":Landroid/view/ViewGroup;
    const/4 v10, 0x0

    .line 70
    .local v10, "v":Landroid/view/View;
    const/4 v5, 0x0

    .line 71
    .local v5, "countChild":I
    if-eqz v11, :cond_0

    .line 72
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    .line 74
    :cond_0
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-le v5, v7, :cond_9

    .line 76
    invoke-virtual {v11, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    instance-of v13, v10, Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    if-eqz v13, :cond_1

    move-object v13, v10

    .line 77
    check-cast v13, Lcom/vlingo/midas/gui/Widget;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v13, v0, v1, v2, v3}, Lcom/vlingo/midas/gui/Widget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 74
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 81
    .end local v5    # "countChild":I
    .end local v7    # "i":I
    .end local v10    # "v":Landroid/view/View;
    .end local v11    # "vg":Landroid/view/ViewGroup;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContext:Landroid/content/Context;

    sget v15, Lcom/vlingo/midas/R$layout;->widget_movie_choice:I

    sget v13, Lcom/vlingo/midas/R$id;->naver_container:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    invoke-static {v14, v15, v13}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 82
    .restart local v11    # "vg":Landroid/view/ViewGroup;
    const/4 v10, 0x0

    .line 83
    .restart local v10    # "v":Landroid/view/View;
    const/4 v5, 0x0

    .line 84
    .restart local v5    # "countChild":I
    if-eqz v11, :cond_3

    .line 85
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    .line 87
    :cond_3
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_1
    if-le v5, v7, :cond_9

    .line 89
    invoke-virtual {v11, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    instance-of v13, v10, Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    if-eqz v13, :cond_4

    move-object v13, v10

    .line 90
    check-cast v13, Lcom/vlingo/midas/gui/Widget;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v13, v0, v1, v2, v3}, Lcom/vlingo/midas/gui/Widget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 87
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 93
    .end local v5    # "countChild":I
    .end local v7    # "i":I
    .end local v10    # "v":Landroid/view/View;
    .end local v11    # "vg":Landroid/view/ViewGroup;
    :cond_5
    iget-object v13, v12, Lcom/vlingo/midas/naver/NaverXMLParser;->domain:Ljava/lang/String;

    const-string/jumbo v14, "REGION"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    if-eqz v8, :cond_a

    .line 94
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v12}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->playTTSforNaver(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/midas/naver/NaverXMLParser;)V

    .line 95
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_8

    .line 96
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContext:Landroid/content/Context;

    sget v15, Lcom/vlingo/midas/R$layout;->widget_naver_region:I

    sget v13, Lcom/vlingo/midas/R$id;->naver_container:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    invoke-static {v14, v15, v13}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 97
    .restart local v11    # "vg":Landroid/view/ViewGroup;
    const/4 v10, 0x0

    .line 98
    .restart local v10    # "v":Landroid/view/View;
    const/4 v5, 0x0

    .line 99
    .restart local v5    # "countChild":I
    if-eqz v11, :cond_6

    .line 100
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    .line 102
    :cond_6
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    if-le v5, v7, :cond_9

    .line 104
    invoke-virtual {v11, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    instance-of v13, v10, Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    if-eqz v13, :cond_7

    move-object v13, v10

    .line 105
    check-cast v13, Lcom/vlingo/midas/gui/Widget;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v13, v0, v1, v2, v3}, Lcom/vlingo/midas/gui/Widget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 102
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 110
    .end local v5    # "countChild":I
    .end local v7    # "i":I
    .end local v10    # "v":Landroid/view/View;
    .end local v11    # "vg":Landroid/view/ViewGroup;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->drawNHNLogo()V

    .line 111
    new-instance v9, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;

    invoke-direct {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;-><init>()V

    .line 112
    .local v9, "tphandler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
    invoke-virtual {v9, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 113
    iget-object v13, v12, Lcom/vlingo/midas/naver/NaverXMLParser;->url:Ljava/lang/String;

    if-eqz v13, :cond_9

    .line 114
    iget-object v13, v12, Lcom/vlingo/midas/naver/NaverXMLParser;->url:Ljava/lang/String;

    invoke-virtual {v9, v13}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->executeSearchIntentFromURL(Ljava/lang/String;)V

    .line 196
    .end local v4    # "ResId":I
    .end local v6    # "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .end local v8    # "itemList":Ljava/util/List;
    .end local v9    # "tphandler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
    .end local v12    # "xmlResult":Lcom/vlingo/midas/naver/NaverXMLParser;
    :cond_9
    :goto_3
    return-void

    .line 117
    .restart local v4    # "ResId":I
    .restart local v6    # "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .restart local v8    # "itemList":Ljava/util/List;
    .restart local v12    # "xmlResult":Lcom/vlingo/midas/naver/NaverXMLParser;
    :cond_a
    iget-object v13, v12, Lcom/vlingo/midas/naver/NaverXMLParser;->domain:Ljava/lang/String;

    const-string/jumbo v14, "LOCAL"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_10

    if-eqz v8, :cond_10

    .line 118
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v12}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->playTTSforNaver(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/midas/naver/NaverXMLParser;)V

    .line 119
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_d

    .line 121
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContext:Landroid/content/Context;

    sget v15, Lcom/vlingo/midas/R$layout;->widget_naver_local:I

    sget v13, Lcom/vlingo/midas/R$id;->naver_container:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    invoke-static {v14, v15, v13}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 122
    .restart local v11    # "vg":Landroid/view/ViewGroup;
    const/4 v10, 0x0

    .line 123
    .restart local v10    # "v":Landroid/view/View;
    const/4 v5, 0x0

    .line 124
    .restart local v5    # "countChild":I
    if-eqz v11, :cond_b

    .line 125
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    .line 127
    :cond_b
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_4
    if-le v5, v7, :cond_9

    .line 129
    invoke-virtual {v11, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    instance-of v13, v10, Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    if-eqz v13, :cond_c

    move-object v13, v10

    .line 130
    check-cast v13, Lcom/vlingo/midas/gui/Widget;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v13, v0, v1, v2, v3}, Lcom/vlingo/midas/gui/Widget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 127
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 133
    .end local v5    # "countChild":I
    .end local v7    # "i":I
    .end local v10    # "v":Landroid/view/View;
    .end local v11    # "vg":Landroid/view/ViewGroup;
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContext:Landroid/content/Context;

    sget v15, Lcom/vlingo/midas/R$layout;->widget_naver_local_choice:I

    sget v13, Lcom/vlingo/midas/R$id;->naver_container:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    invoke-static {v14, v15, v13}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 134
    .restart local v11    # "vg":Landroid/view/ViewGroup;
    const/4 v10, 0x0

    .line 135
    .restart local v10    # "v":Landroid/view/View;
    const/4 v5, 0x0

    .line 136
    .restart local v5    # "countChild":I
    if-eqz v11, :cond_e

    .line 137
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    .line 139
    :cond_e
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_5
    if-le v5, v7, :cond_9

    .line 141
    invoke-virtual {v11, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    instance-of v13, v10, Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    if-eqz v13, :cond_f

    move-object v13, v10

    .line 142
    check-cast v13, Lcom/vlingo/midas/gui/Widget;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v13, v0, v1, v2, v3}, Lcom/vlingo/midas/gui/Widget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 139
    :cond_f
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 145
    .end local v5    # "countChild":I
    .end local v7    # "i":I
    .end local v10    # "v":Landroid/view/View;
    .end local v11    # "vg":Landroid/view/ViewGroup;
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getType()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_13

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getType()Ljava/lang/String;

    move-result-object v13

    const-string/jumbo v14, "car:unknown:def"

    invoke-virtual {v13, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_13

    iget-object v13, v12, Lcom/vlingo/midas/naver/NaverXMLParser;->domain:Ljava/lang/String;

    const-string/jumbo v14, "WEB"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_13

    .line 146
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v12}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->playTTSforNaver(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/midas/naver/NaverXMLParser;)V

    .line 147
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContext:Landroid/content/Context;

    sget v15, Lcom/vlingo/midas/R$layout;->widget_button:I

    sget v13, Lcom/vlingo/midas/R$id;->naver_container:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    invoke-static {v14, v15, v13}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 148
    .restart local v11    # "vg":Landroid/view/ViewGroup;
    const/4 v10, 0x0

    .line 149
    .restart local v10    # "v":Landroid/view/View;
    const/4 v5, 0x0

    .line 150
    .restart local v5    # "countChild":I
    if-eqz v11, :cond_11

    .line 151
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    .line 153
    :cond_11
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_6
    if-le v5, v7, :cond_9

    .line 155
    invoke-virtual {v11, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    instance-of v13, v10, Lcom/vlingo/midas/gui/widgets/ButtonWidget;

    if-eqz v13, :cond_12

    move-object v13, v10

    .line 156
    check-cast v13, Lcom/vlingo/midas/gui/Widget;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContext:Landroid/content/Context;

    sget v15, Lcom/vlingo/midas/R$string;->core_search_web_label_button:I

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    new-instance v16, Lcom/vlingo/midas/gui/widgets/NaverWidget$1;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6, v12}, Lcom/vlingo/midas/gui/widgets/NaverWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/NaverWidget;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/midas/naver/NaverXMLParser;)V

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v13, v14, v15, v0, v1}, Lcom/vlingo/midas/gui/Widget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 153
    :cond_12
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 168
    .end local v5    # "countChild":I
    .end local v7    # "i":I
    .end local v10    # "v":Landroid/view/View;
    .end local v11    # "vg":Landroid/view/ViewGroup;
    :cond_13
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v12}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->playTTSforNaver(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/midas/naver/NaverXMLParser;)V

    .line 184
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->drawNHNLogo()V

    .line 186
    new-instance v9, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;

    invoke-direct {v9}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;-><init>()V

    .line 187
    .restart local v9    # "tphandler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
    invoke-virtual {v9, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 188
    iget-object v13, v12, Lcom/vlingo/midas/naver/NaverXMLParser;->url:Ljava/lang/String;

    if-eqz v13, :cond_9

    .line 189
    iget-object v13, v12, Lcom/vlingo/midas/naver/NaverXMLParser;->url:Ljava/lang/String;

    invoke-virtual {v9, v13}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->executeSearchIntentFromURL(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 192
    .end local v4    # "ResId":I
    .end local v8    # "itemList":Ljava/util/List;
    .end local v9    # "tphandler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->drawNHNLogo()V

    goto/16 :goto_3

    .line 195
    .end local v6    # "handler":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    .end local v12    # "xmlResult":Lcom/vlingo/midas/naver/NaverXMLParser;
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->drawNHNLogo()V

    goto/16 :goto_3
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 26
    check-cast p1, Lcom/vlingo/midas/naver/NaverAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x1

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/vlingo/midas/R$id;->content:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContentView:Landroid/widget/TextView;

    .line 41
    return-void
.end method

.method public onResponseReceived()V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContentView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getContent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget;->mContentView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    return-void
.end method
