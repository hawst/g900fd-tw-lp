.class Lcom/vlingo/midas/gui/ControlFragment$5;
.super Ljava/lang/Object;
.source "ControlFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/ControlFragment;->onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ControlFragment;

.field final synthetic val$micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ControlFragment;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 0

    .prologue
    .line 1033
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragment$5;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    iput-object p2, p0, Lcom/vlingo/midas/gui/ControlFragment$5;->val$micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment$5;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    iget-object v0, v0, Lcom/vlingo/midas/gui/ControlFragment;->callback:Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;

    invoke-interface {v0}, Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;->onStopSpotting()V

    .line 1036
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment$5;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragment$5;->val$micStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragment;->startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    .line 1037
    return-void
.end method
