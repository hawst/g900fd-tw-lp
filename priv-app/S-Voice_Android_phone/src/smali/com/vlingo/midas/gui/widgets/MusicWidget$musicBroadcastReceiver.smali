.class public Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MusicWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/MusicWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "musicBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/MusicWidget;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 142
    if-eqz p2, :cond_0

    .line 143
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "action":Ljava/lang/String;
    const-string/jumbo v10, "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 145
    const/4 v10, 0x0

    # setter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->isPlaying:Z
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$202(Z)Z

    .line 146
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v10

    if-nez v10, :cond_1

    .line 246
    .end local v1    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 149
    .restart local v1    # "action":Ljava/lang/String;
    :cond_1
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 150
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 151
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x3

    invoke-virtual {v10, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 152
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnImage:Landroid/widget/ImageView;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/widget/ImageView;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$drawable;->voice_music_volume_01:I

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 154
    :cond_2
    const-string/jumbo v10, "com.android.music.metachanged"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 155
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mTitle:Lcom/vlingo/midas/gui/customviews/TextView;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$300(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Lcom/vlingo/midas/gui/customviews/TextView;

    move-result-object v10

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    const-string/jumbo v12, "track"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/vlingo/midas/gui/customviews/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mArtist:Lcom/vlingo/midas/gui/customviews/TextView;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$400(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Lcom/vlingo/midas/gui/customviews/TextView;

    move-result-object v10

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    const-string/jumbo v12, "artist"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/vlingo/midas/gui/customviews/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    sget-object v10, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v10}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v5

    .line 160
    .local v5, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v7, 0x0

    .line 161
    .local v7, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v5, :cond_3

    .line 163
    :try_start_0
    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v7, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    .line 193
    :cond_3
    :goto_1
    const/4 v2, 0x0

    .line 194
    .local v2, "albumUri":Landroid/net/Uri;
    if-eqz v7, :cond_4

    .line 195
    :try_start_1
    sget-object v10, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v7, v10}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    .line 198
    :goto_2
    const-string/jumbo v10, "albumId"

    const-wide/16 v11, -0x1

    invoke-virtual {p2, v10, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    invoke-static {v2, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    .line 202
    .local v9, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 205
    .local v8, "res":Landroid/content/ContentResolver;
    :try_start_2
    invoke-virtual {v8, v9}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v6

    .line 206
    .local v6, "in":Ljava/io/InputStream;
    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 207
    .local v3, "artwork":Landroid/graphics/Bitmap;
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$500(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/widget/ImageView;

    move-result-object v10

    invoke-virtual {v10, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 209
    .end local v3    # "artwork":Landroid/graphics/Bitmap;
    .end local v6    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v4

    .line 210
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getMusicThumbnail()V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 218
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    .end local v8    # "res":Landroid/content/ContentResolver;
    .end local v9    # "uri":Landroid/net/Uri;
    :catch_1
    move-exception v4

    .line 219
    .local v4, "e":Ljava/lang/IllegalStateException;
    :try_start_4
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getMusicThumbnail()V

    .line 220
    invoke-virtual {v4}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 222
    .end local v4    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v10

    throw v10

    .line 164
    .end local v2    # "albumUri":Landroid/net/Uri;
    :catch_2
    move-exception v4

    .line 165
    .local v4, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v4}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1

    .line 166
    .end local v4    # "e":Ljava/lang/InstantiationException;
    :catch_3
    move-exception v4

    .line 167
    .local v4, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 197
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "albumUri":Landroid/net/Uri;
    :cond_4
    :try_start_5
    const-string/jumbo v10, "content://media/external/audio/albumart/"

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_2

    .line 212
    .restart local v8    # "res":Landroid/content/ContentResolver;
    .restart local v9    # "uri":Landroid/net/Uri;
    :catch_4
    move-exception v4

    .line 213
    .local v4, "e":Ljava/io/FileNotFoundException;
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getMusicThumbnail()V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 228
    .end local v2    # "albumUri":Landroid/net/Uri;
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    .end local v5    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v7    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    .end local v8    # "res":Landroid/content/ContentResolver;
    .end local v9    # "uri":Landroid/net/Uri;
    :cond_5
    const-string/jumbo v10, "com.sec.android.music.musicservicecommnad.mediainfo"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 231
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 234
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string/jumbo v11, "isPlaying"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 235
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string/jumbo v11, "isPlaying"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    # setter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->isPlaying:Z
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$202(Z)Z

    .line 236
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 238
    :cond_6
    const/4 v10, 0x0

    # setter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->isPlaying:Z
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$202(Z)Z

    .line 239
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 240
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 241
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x3

    invoke-virtual {v10, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 242
    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnImage:Landroid/widget/ImageView;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/widget/ImageView;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$drawable;->voice_music_volume_01:I

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 215
    .restart local v2    # "albumUri":Landroid/net/Uri;
    .restart local v5    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .restart local v7    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    .restart local v8    # "res":Landroid/content/ContentResolver;
    .restart local v9    # "uri":Landroid/net/Uri;
    :catch_5
    move-exception v10

    goto/16 :goto_0
.end method
