.class public Lcom/vlingo/midas/gui/widgets/MusicWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "MusicWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
        ">;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field public static final ACTION_MUSIC_CLOSE:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

.field private static final VOLUMNE_ANIMATION_IMAGE_FIRST:I = 0x1

.field private static final VOLUMNE_ANIMATION_IMAGE_SECOND:I = 0x2

.field private static final VOLUMNE_ANIMATION_IMAGE_THIRD:I = 0x3

.field private static isPlaying:Z


# instance fields
.field private final context:Landroid/content/Context;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mArtist:Lcom/vlingo/midas/gui/customviews/TextView;

.field private mAudioTemplateLayout:Landroid/widget/LinearLayout;

.field private mMusic:Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

.field private mThumbnail:Landroid/widget/ImageView;

.field private mTitle:Lcom/vlingo/midas/gui/customviews/TextView;

.field private mVolumnHandler:Landroid/os/Handler;

.field private mVolumnImage:Landroid/widget/ImageView;

.field private musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private shadow:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->isPlaying:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    new-instance v0, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/MusicWidget;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;

    .line 78
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->context:Landroid/content/Context;

    .line 79
    new-instance v0, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget$musicBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/widgets/MusicWidget;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 80
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "com.android.music.metachanged"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 82
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "com.sec.android.music.musicservicecommnad.mediainfo"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 84
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 86
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->setMusicLaunchedFromSvoice(Z)V

    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicWidget;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicWidget;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 50
    sput-boolean p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->isPlaying:Z

    return p0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Lcom/vlingo/midas/gui/customviews/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicWidget;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mTitle:Lcom/vlingo/midas/gui/customviews/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Lcom/vlingo/midas/gui/customviews/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicWidget;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mArtist:Lcom/vlingo/midas/gui/customviews/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicWidget;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method getMusicThumbnail()V
    .locals 5

    .prologue
    .line 253
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 254
    .local v0, "random":Ljava/util/Random;
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 255
    .local v1, "randomPicSelecter":I
    packed-switch v1, :pswitch_data_0

    .line 278
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_music_thumbnail_08:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 281
    :goto_0
    return-void

    .line 257
    :pswitch_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_music_thumbnail_01:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 260
    :pswitch_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_music_thumbnail_02:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 263
    :pswitch_2
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_music_thumbnail_03:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 266
    :pswitch_3
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_music_thumbnail_04:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 269
    :pswitch_4
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_music_thumbnail_05:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 272
    :pswitch_5
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_music_thumbnail_06:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 275
    :pswitch_6
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_music_thumbnail_07:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 255
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 50
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 288
    if-nez p1, :cond_0

    .line 291
    :cond_0
    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 336
    const/4 v0, 0x0

    .line 337
    .local v0, "playIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 338
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.sec.android.app.music"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 349
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 350
    return-void

    .line 340
    :cond_0
    sget-boolean v1, Lcom/vlingo/midas/gui/widgets/MusicWidget;->isPlaying:Z

    if-eqz v1, :cond_1

    .line 341
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "playIntent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.music.intent.action.PLAY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 346
    .restart local v0    # "playIntent":Landroid/content/Intent;
    :goto_1
    const-string/jumbo v1, "launchMusicPlayer"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 347
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 344
    :cond_1
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "playIntent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.music.intent.action.STOP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0    # "playIntent":Landroid/content/Intent;
    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 318
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onDetachedFromWindow()V

    .line 320
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 321
    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 325
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 326
    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 328
    :cond_1
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 92
    sget v0, Lcom/vlingo/midas/R$id;->audioTemplateRLayout:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mAudioTemplateLayout:Landroid/widget/LinearLayout;

    .line 94
    sget v0, Lcom/vlingo/midas/R$id;->audioImage:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mThumbnail:Landroid/widget/ImageView;

    .line 95
    sget v0, Lcom/vlingo/midas/R$id;->musicShadow:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->shadow:Landroid/view/View;

    .line 97
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->shadow:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->shadow:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    :cond_0
    sget v0, Lcom/vlingo/midas/R$id;->audioTitle:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mTitle:Lcom/vlingo/midas/gui/customviews/TextView;

    .line 101
    sget v0, Lcom/vlingo/midas/R$id;->audioArtist:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mArtist:Lcom/vlingo/midas/gui/customviews/TextView;

    .line 102
    sget v0, Lcom/vlingo/midas/R$id;->audioVolumnImage:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnImage:Landroid/widget/ImageView;

    .line 104
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget;->mAudioTemplateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 296
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->retire()V

    .line 297
    return-void
.end method

.method public onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 301
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 302
    return-void
.end method
