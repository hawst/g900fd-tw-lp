.class Lcom/vlingo/midas/gui/widgets/NaverWidget$1;
.super Ljava/lang/Object;
.source "NaverWidget.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/NaverWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/NaverWidget;

.field final synthetic val$handler:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field final synthetic val$xmlResult:Lcom/vlingo/midas/naver/NaverXMLParser;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverWidget;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/midas/naver/NaverXMLParser;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverWidget;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget$1;->val$handler:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget$1;->val$xmlResult:Lcom/vlingo/midas/naver/NaverXMLParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 159
    new-instance v0, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;

    invoke-direct {v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/DefaultWebSearchHandler;-><init>()V

    .line 160
    .local v0, "tphandler":Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget$1;->val$handler:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->setListener(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)V

    .line 161
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget$1;->val$xmlResult:Lcom/vlingo/midas/naver/NaverXMLParser;

    iget-object v1, v1, Lcom/vlingo/midas/naver/NaverXMLParser;->url:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverWidget$1;->val$xmlResult:Lcom/vlingo/midas/naver/NaverXMLParser;

    iget-object v1, v1, Lcom/vlingo/midas/naver/NaverXMLParser;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/clientdriven/WebSearchHandler;->executeSearchIntentFromURL(Ljava/lang/String;)V

    .line 164
    :cond_0
    return-void
.end method
