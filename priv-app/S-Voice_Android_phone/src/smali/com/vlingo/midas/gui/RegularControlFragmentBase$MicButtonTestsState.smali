.class Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;
.super Ljava/lang/Object;
.source "RegularControlFragmentBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/RegularControlFragmentBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MicButtonTestsState"
.end annotation


# instance fields
.field private enable:Z

.field final synthetic this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)V
    .locals 1

    .prologue
    .line 1026
    iput-object p1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1027
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->enable:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/RegularControlFragmentBase;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;

    .prologue
    .line 1026
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;-><init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)V

    return-void
.end method

.method private idleBtnTest()V
    .locals 2

    .prologue
    .line 1058
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    const-string/jumbo v1, "ZEP_MIC_BTN_IDLE"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->setContentDescription(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 1059
    return-void
.end method

.method private listeningBtnTest()V
    .locals 2

    .prologue
    .line 1062
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    const-string/jumbo v1, "ZEP_MIC_BTN_LISTENING"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->setContentDescription(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 1063
    return-void
.end method

.method private setContentDescription(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 0
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 1076
    if-eqz p1, :cond_0

    .line 1077
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1079
    :cond_0
    return-void
.end method

.method private setNotTestContentDescription(Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "view"    # Landroid/widget/ImageView;

    .prologue
    .line 1070
    if-eqz p1, :cond_0

    .line 1071
    const-string/jumbo v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1073
    :cond_0
    return-void
.end method

.method private thinkingBtnTest()V
    .locals 2

    .prologue
    .line 1066
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    const-string/jumbo v1, "ZEP_MIC_BTN_THINKING"

    invoke-direct {p0, v0, v1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->setContentDescription(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 1067
    return-void
.end method


# virtual methods
.method public onSetButtonIdle()V
    .locals 1

    .prologue
    .line 1030
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->enable:Z

    if-eqz v0, :cond_0

    .line 1031
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->setNotTestContentDescription(Landroid/widget/ImageView;)V

    .line 1032
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->setNotTestContentDescription(Landroid/widget/ImageView;)V

    .line 1033
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->idleBtnTest()V

    .line 1035
    :cond_0
    return-void
.end method

.method public onSetButtonListening()V
    .locals 1

    .prologue
    .line 1038
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->enable:Z

    if-eqz v0, :cond_0

    .line 1039
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->listeningBtnTest()V

    .line 1040
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->setNotTestContentDescription(Landroid/widget/ImageView;)V

    .line 1041
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->setNotTestContentDescription(Landroid/widget/ImageView;)V

    .line 1043
    :cond_0
    return-void
.end method

.method public onSetButtonThinking()V
    .locals 1

    .prologue
    .line 1046
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->enable:Z

    if-eqz v0, :cond_0

    .line 1047
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->setNotTestContentDescription(Landroid/widget/ImageView;)V

    .line 1048
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->thinkingBtnTest()V

    .line 1049
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->setNotTestContentDescription(Landroid/widget/ImageView;)V

    .line 1051
    :cond_0
    return-void
.end method

.method public setEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 1054
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$MicButtonTestsState;->enable:Z

    .line 1055
    return-void
.end method
