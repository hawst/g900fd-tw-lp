.class Lcom/vlingo/midas/gui/DataCheckActivity$3;
.super Ljava/lang/Object;
.source "DataCheckActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/DataCheckActivity;->createWlanDataCheckDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DataCheckActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DataCheckActivity;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/vlingo/midas/gui/DataCheckActivity$3;->this$0:Lcom/vlingo/midas/gui/DataCheckActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 165
    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v0}, Lcom/vlingo/midas/gui/ConversationActivity;-><init>()V

    .line 166
    .local v0, "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isLocationEnabled()Z

    move-result v1

    .line 167
    .local v1, "locationEnabled":Z
    if-eqz v1, :cond_0

    .line 168
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->requestLocationUpdate()V

    .line 170
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/DataCheckActivity$3;->this$0:Lcom/vlingo/midas/gui/DataCheckActivity;

    iget-object v3, p0, Lcom/vlingo/midas/gui/DataCheckActivity$3;->this$0:Lcom/vlingo/midas/gui/DataCheckActivity;

    # getter for: Lcom/vlingo/midas/gui/DataCheckActivity;->STEP_NEXT:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/DataCheckActivity;->access$100(Lcom/vlingo/midas/gui/DataCheckActivity;)I

    move-result v3

    # invokes: Lcom/vlingo/midas/gui/DataCheckActivity;->recreateView(I)V
    invoke-static {v2, v3}, Lcom/vlingo/midas/gui/DataCheckActivity;->access$200(Lcom/vlingo/midas/gui/DataCheckActivity;I)V

    .line 171
    return-void
.end method
