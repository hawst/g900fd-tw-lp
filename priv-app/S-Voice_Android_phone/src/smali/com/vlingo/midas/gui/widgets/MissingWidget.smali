.class public Lcom/vlingo/midas/gui/widgets/MissingWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "MissingWidget.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field objectInfo:Lcom/vlingo/midas/gui/customviews/TextView;

.field widgetName:Lcom/vlingo/midas/gui/customviews/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method


# virtual methods
.method public initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MissingWidget;->widgetName:Lcom/vlingo/midas/gui/customviews/TextView;

    invoke-virtual {p3}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    if-eqz p1, :cond_0

    .line 41
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MissingWidget;->objectInfo:Lcom/vlingo/midas/gui/customviews/TextView;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MissingWidget;->objectInfo:Lcom/vlingo/midas/gui/customviews/TextView;

    sget v1, Lcom/vlingo/midas/R$string;->object_is_null:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setText(I)V

    goto :goto_0
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/vlingo/midas/R$id;->widget_name:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MissingWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MissingWidget;->widgetName:Lcom/vlingo/midas/gui/customviews/TextView;

    .line 29
    sget v0, Lcom/vlingo/midas/R$id;->object_info:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MissingWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MissingWidget;->objectInfo:Lcom/vlingo/midas/gui/customviews/TextView;

    .line 31
    return-void
.end method
