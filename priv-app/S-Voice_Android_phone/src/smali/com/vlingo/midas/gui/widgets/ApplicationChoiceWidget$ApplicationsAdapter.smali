.class Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ApplicationChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApplicationsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private applist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mOldBounds:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 118
    .local p3, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;

    .line 119
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 116
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;->mOldBounds:Landroid/graphics/Rect;

    .line 120
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;->applist:Ljava/util/List;

    .line 121
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 207
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;->applist:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 208
    .local v0, "toReturn":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->getLimitedCount(I)I

    move-result v1

    return v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 125
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;->applist:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 128
    .local v3, "info":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    if-nez p2, :cond_1

    .line 129
    new-instance v0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;

    const/4 v4, 0x0

    invoke-direct {v0, v4}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$1;)V

    .line 130
    .local v0, "holder":Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 131
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget v4, Lcom/vlingo/midas/R$layout;->item_application_choice:I

    const/4 v5, 0x0

    invoke-virtual {v2, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 133
    sget v4, Lcom/vlingo/midas/R$id;->label:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;->textView:Landroid/widget/TextView;

    .line 134
    sget v4, Lcom/vlingo/midas/R$id;->item_application_choice_thumnail:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;->imageView:Landroid/widget/ImageView;

    .line 135
    sget v4, Lcom/vlingo/midas/R$id;->item_application_choice_divider:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;->divider:Landroid/view/View;

    .line 136
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 141
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->getActivityName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->getIconFromActivityName(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    invoke-static {v4, v5, v6}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 143
    .local v1, "icon":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 187
    iget-object v4, v0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 191
    :cond_0
    iget-object v4, v0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    if-nez p1, :cond_2

    .line 194
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 202
    :goto_1
    return-object p2

    .line 138
    .end local v0    # "holder":Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;
    .end local v1    # "icon":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;

    .restart local v0    # "holder":Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;
    goto :goto_0

    .line 195
    .restart local v1    # "icon":Landroid/graphics/drawable/Drawable;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne p1, v4, :cond_3

    .line 196
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 197
    iget-object v4, v0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;->divider:Landroid/view/View;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 199
    :cond_3
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method
