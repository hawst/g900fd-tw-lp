.class Lcom/vlingo/midas/gui/ConversationActivity$Task;
.super Ljava/lang/Object;
.source "ConversationActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ConversationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Task"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ConversationActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0

    .prologue
    .line 666
    iput-object p1, p0, Lcom/vlingo/midas/gui/ConversationActivity$Task;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 671
    :try_start_0
    const-string/jumbo v1, "PB Thread Start"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity$Task;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    sget-object v2, Lcom/vlingo/midas/gui/DialogFragment;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->getTodayInstances()Landroid/database/Cursor;

    move-result-object v2

    iput-object v2, v1, Lcom/vlingo/midas/gui/ConversationActivity;->cursor:Landroid/database/Cursor;

    .line 673
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity$Task;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v1, v1, Lcom/vlingo/midas/gui/ConversationActivity;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 674
    sget-object v1, Lcom/vlingo/midas/gui/DialogFragment;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$Task;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v2, v2, Lcom/vlingo/midas/gui/ConversationActivity;->cursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->getEventInfo(Landroid/database/Cursor;)V

    .line 675
    :cond_0
    sget-object v1, Lcom/vlingo/midas/gui/DialogFragment;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->getContacts()Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lcom/vlingo/midas/gui/DialogFragment;->birthdayList:Ljava/util/ArrayList;

    .line 676
    const-string/jumbo v1, "PB Thread complete"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    const/4 v1, 0x1

    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->threadVar:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 681
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity$Task;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v1, v1, Lcom/vlingo/midas/gui/ConversationActivity;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 682
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity$Task;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v1, v1, Lcom/vlingo/midas/gui/ConversationActivity;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 687
    :cond_1
    :goto_0
    return-void

    .line 678
    :catch_0
    move-exception v0

    .line 679
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v1, "SmartNotification"

    const-string/jumbo v2, "Exception Occurred in DialogFragment SmartNotification"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 681
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity$Task;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v1, v1, Lcom/vlingo/midas/gui/ConversationActivity;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 682
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity$Task;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v1, v1, Lcom/vlingo/midas/gui/ConversationActivity;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 681
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$Task;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v2, v2, Lcom/vlingo/midas/gui/ConversationActivity;->cursor:Landroid/database/Cursor;

    if-eqz v2, :cond_2

    .line 682
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$Task;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v2, v2, Lcom/vlingo/midas/gui/ConversationActivity;->cursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method
