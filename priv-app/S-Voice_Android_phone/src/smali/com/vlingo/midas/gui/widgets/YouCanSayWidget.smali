.class public Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;
.super Landroid/widget/RelativeLayout;
.source "YouCanSayWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolderKK;,
        Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;
    }
.end annotation


# static fields
.field public static final EXTRA_LIST_HEIGHT:Ljava/lang/String; = "EXTRA_LIST_HEIGHT"

.field public static final LIST_MOVE_ACTION:Ljava/lang/String; = "LIST_MOVE_ACTION"

.field private static final NORMAL_LAUNCH_COUNT:Ljava/lang/String; = "normal_launch_count"

.field public static NUMBER_OF_ITEMS:I


# instance fields
.field arrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field arrayString:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field count:I

.field private firstHelpView:Z

.field flag:I

.field private launchCount:I

.field lineTwo:Ljava/lang/String;

.field private locale:Ljava/util/Locale;

.field private lv:Landroid/widget/LinearLayout;

.field private lv2:Landroid/widget/LinearLayout;

.field private final mContext:Landroid/content/Context;

.field mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

.field private wcisData:Lcom/vlingo/midas/help/WCISData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x3

    sput v0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->NUMBER_OF_ITEMS:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x1

    .line 66
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    new-instance v0, Lcom/vlingo/midas/help/WCISData;

    invoke-direct {v0}, Lcom/vlingo/midas/help/WCISData;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->wcisData:Lcom/vlingo/midas/help/WCISData;

    .line 59
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->firstHelpView:Z

    .line 62
    iput v1, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->launchCount:I

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->locale:Ljava/util/Locale;

    .line 245
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->count:I

    .line 67
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->mContext:Landroid/content/Context;

    .line 68
    new-instance v0, Lcom/vlingo/midas/ui/PackageInfoProvider;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 69
    return-void
.end method

.method private getRandomNumber()[I
    .locals 7

    .prologue
    .line 223
    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    .line 224
    .local v4, "random":Ljava/util/Random;
    sget v5, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->NUMBER_OF_ITEMS:I

    new-array v3, v5, [I

    .line 227
    .local v3, "rNumber":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v3

    if-ge v1, v5, :cond_3

    .line 228
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->wcisData:Lcom/vlingo/midas/help/WCISData;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    aput v5, v3, v1

    .line 229
    const/4 v0, 0x1

    .line 230
    .local v0, "breakFlag":Z
    :cond_0
    if-eqz v0, :cond_2

    .line 231
    const/4 v0, 0x0

    .line 232
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    if-ge v2, v1, :cond_0

    .line 233
    aget v5, v3, v1

    aget v6, v3, v2

    if-ne v5, v6, :cond_1

    .line 234
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->wcisData:Lcom/vlingo/midas/help/WCISData;

    invoke-virtual {v5}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    aput v5, v3, v1

    .line 235
    const/4 v0, 0x1

    .line 232
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 227
    .end local v2    # "j":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 240
    .end local v0    # "breakFlag":Z
    :cond_3
    invoke-static {v3}, Ljava/util/Arrays;->sort([I)V

    .line 242
    return-object v3
.end method

.method private setLandscapeView()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 145
    const/high16 v4, 0x43fc0000    # 504.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v3, v4

    .line 146
    .local v3, "width":I
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 148
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$dimen;->you_can_say_padding_top_land:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v4, v5

    float-to-int v1, v4

    .line 149
    .local v1, "margin":I
    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v0, v4

    .line 150
    .local v0, "bottomMargin":I
    invoke-virtual {v2, v6, v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 151
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->lv2:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    .line 152
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->lv2:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    :cond_0
    return-void
.end method

.method private setPortraitView()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 158
    const/high16 v4, 0x43e10000    # 450.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v3, v4

    .line 159
    .local v3, "width":I
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 161
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$dimen;->you_can_say_padding_top_portrait:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v4, v5

    float-to-int v1, v4

    .line 162
    .local v1, "margin":I
    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v0, v4

    .line 163
    .local v0, "bottomMargin":I
    invoke-virtual {v2, v6, v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 164
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->lv2:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    .line 165
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->lv2:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 167
    :cond_0
    return-void
.end method

.method private setThreeItems()V
    .locals 5

    .prologue
    .line 115
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->arrayList:Ljava/util/ArrayList;

    .line 117
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->arrayList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 119
    const/4 v2, 0x0

    .line 120
    .local v2, "wcisDataSize":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->wcisData:Lcom/vlingo/midas/help/WCISData;

    if-eqz v3, :cond_2

    .line 121
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->wcisData:Lcom/vlingo/midas/help/WCISData;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/vlingo/midas/help/WCISData;->addItems(Landroid/content/Context;)V

    .line 122
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getRandomNumber()[I

    move-result-object v1

    .line 123
    .local v1, "randomNumber":[I
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->wcisData:Lcom/vlingo/midas/help/WCISData;

    invoke-virtual {v3}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 125
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 126
    const/4 v3, 0x0

    aget v3, v1, v3

    if-eq v0, v3, :cond_0

    const/4 v3, 0x1

    aget v3, v1, v3

    if-eq v0, v3, :cond_0

    const/4 v3, 0x2

    aget v3, v1, v3

    if-ne v0, v3, :cond_1

    .line 127
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->arrayList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->wcisData:Lcom/vlingo/midas/help/WCISData;

    invoke-virtual {v4}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131
    .end local v0    # "i":I
    .end local v1    # "randomNumber":[I
    :cond_2
    return-void
.end method

.method private setThreeItemsKK()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    .line 170
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->arrayString:Ljava/util/ArrayList;

    .line 171
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->arrayString:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 173
    new-instance v6, Ljava/util/Random;

    invoke-direct {v6}, Ljava/util/Random;-><init>()V

    .line 175
    .local v6, "random":Ljava/util/Random;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    const-string/jumbo v9, "normal_launch_count"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->launchCount:I

    .line 177
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 178
    .local v7, "res":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 179
    .local v4, "packageName":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 180
    .local v5, "prefixName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 182
    .local v1, "id":I
    iget v8, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->launchCount:I

    if-ge v8, v11, :cond_0

    .line 183
    const-string/jumbo v5, "ycs_ex_first_"

    .line 184
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    sget v8, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->NUMBER_OF_ITEMS:I

    if-gt v0, v8, :cond_5

    .line 185
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "string"

    invoke-virtual {v7, v8, v9, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 186
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->arrayString:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "\""

    const-string/jumbo v11, " "

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    .end local v0    # "i":I
    :cond_0
    const/4 v8, 0x3

    invoke-virtual {v6, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 190
    .local v3, "n":I
    if-nez v3, :cond_3

    .line 191
    const-string/jumbo v5, "ycs_ex_first_"

    .line 199
    :goto_1
    const/4 v0, 0x1

    .restart local v0    # "i":I
    :goto_2
    sget v8, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->NUMBER_OF_ITEMS:I

    if-gt v0, v8, :cond_5

    .line 200
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "string"

    invoke-virtual {v7, v8, v9, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 201
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasCarModeApp()Z

    move-result v8

    if-eqz v8, :cond_1

    const-string/jumbo v8, "ycs_ex_first_"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    if-ne v0, v11, :cond_1

    .line 202
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "_carmode"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "string"

    invoke-virtual {v7, v8, v9, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 203
    :cond_1
    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 204
    .local v2, "localString":Ljava/lang/String;
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->locale:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "ja_JP"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 205
    const-string/jumbo v8, "\u300c"

    const-string/jumbo v9, " "

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 206
    const-string/jumbo v8, "\u300d"

    const-string/jumbo v9, " "

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 208
    :cond_2
    const-string/jumbo v8, "\""

    const-string/jumbo v9, " "

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 209
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->arrayString:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 192
    .end local v0    # "i":I
    .end local v2    # "localString":Ljava/lang/String;
    :cond_3
    const/4 v8, 0x1

    if-ne v3, v8, :cond_4

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->isKDDImodels()Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/vlingo/midas/settings/MidasSettings;->isWifiOnly(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_4

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasCallEnabled()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasMessaging()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 194
    const-string/jumbo v5, "ycs_ex_second_"

    goto/16 :goto_1

    .line 196
    :cond_4
    const-string/jumbo v5, "ycs_ex_third_"

    goto/16 :goto_1

    .line 212
    .end local v3    # "n":I
    .restart local v0    # "i":I
    :cond_5
    return-void
.end method


# virtual methods
.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 364
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "accessibility"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 366
    .local v0, "mAccessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 367
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    .line 369
    :cond_0
    const/4 v1, 0x1

    .line 371
    :goto_0
    return v1

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method isKDDImodels()Z
    .locals 2

    .prologue
    .line 218
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 219
    .local v0, "build_model":Ljava/lang/String;
    const-string/jumbo v1, "SCL21"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 135
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 136
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 138
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setPortraitView()V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setLandscapeView()V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 73
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 75
    sget v0, Lcom/vlingo/midas/R$id;->widget_you_can_say_list:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->lv:Landroid/widget/LinearLayout;

    .line 76
    sget v0, Lcom/vlingo/midas/R$id;->widget_you_can_say_list_container:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->lv2:Landroid/widget/LinearLayout;

    .line 77
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->lv2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 80
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 82
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setPortraitView()V

    .line 89
    :cond_1
    :goto_0
    const-string/jumbo v0, "language"

    const-string/jumbo v1, "en-US"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->locale:Ljava/util/Locale;

    .line 91
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 92
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setThreeItemsKK()V

    .line 99
    :goto_1
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->firstHelpView:Z

    .line 101
    new-instance v0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 112
    return-void

    .line 84
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setLandscapeView()V

    goto :goto_0

    .line 94
    :cond_3
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setThreeItems()V

    goto :goto_1
.end method

.method public setAddView()V
    .locals 14

    .prologue
    .line 248
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v9

    if-nez v9, :cond_4

    .line 249
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->arrayList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 250
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 251
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    .line 253
    .local v5, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$layout;->item_you_can_say_widget:I

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 255
    .local v6, "row":Landroid/view/View;
    new-instance v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;

    const/4 v9, 0x0

    invoke-direct {v1, v9}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$1;)V

    .line 257
    .local v1, "holder":Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;
    sget v9, Lcom/vlingo/midas/R$id;->item_you_can_say_widget_title:I

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->title:Landroid/widget/TextView;

    .line 259
    sget v9, Lcom/vlingo/midas/R$id;->item_you_can_say_widget_command:I

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->command:Landroid/widget/TextView;

    .line 261
    sget v9, Lcom/vlingo/midas/R$id;->item_you_can_say_widget_examples:I

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->examples:Landroid/widget/TextView;

    .line 263
    sget v9, Lcom/vlingo/midas/R$id;->item_you_can_say_widget_divider1:I

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->divider:Landroid/view/View;

    .line 265
    invoke-virtual {v6, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 276
    const-string/jumbo v9, "EXTRA_LIST_TITLE"

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 277
    .local v8, "title":Ljava/lang/String;
    iget-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 280
    const-string/jumbo v9, "EXTRA_LIST_EXAMPLE"

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 282
    .local v0, "commandString":Ljava/lang/String;
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 283
    .local v7, "sb":Landroid/text/Spannable;
    new-instance v9, Landroid/text/style/AbsoluteSizeSpan;

    const/16 v10, 0x25

    const/4 v11, 0x1

    invoke-direct {v9, v10, v11}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/16 v12, 0x21

    invoke-interface {v7, v9, v10, v11, v12}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 285
    new-instance v9, Landroid/text/style/AbsoluteSizeSpan;

    const/16 v10, 0x16

    const/4 v11, 0x1

    invoke-direct {v9, v10, v11}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    const/4 v10, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    const/16 v12, 0x21

    invoke-interface {v7, v9, v10, v11, v12}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 288
    new-instance v9, Landroid/text/style/AbsoluteSizeSpan;

    const/16 v10, 0x25

    const/4 v11, 0x1

    invoke-direct {v9, v10, v11}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v11

    const/16 v12, 0x21

    invoke-interface {v7, v9, v10, v11, v12}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 291
    iget-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->command:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    .end local v0    # "commandString":Ljava/lang/String;
    .end local v7    # "sb":Landroid/text/Spannable;
    :goto_1
    const-string/jumbo v9, "EXTRA_EXAMPLE_LIST"

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v10, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->examples:Landroid/widget/TextView;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setDetailText(ILandroid/widget/TextView;)V

    .line 301
    const-string/jumbo v9, "EXTRA_EXAMPLE_LIST"

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    sget v10, Lcom/vlingo/midas/R$array;->wcis_music_examples:I

    if-ne v9, v10, :cond_0

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasRadio()Z

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_0

    .line 304
    sget v9, Lcom/vlingo/midas/R$array;->wcis_music_examples_no_radio:I

    iget-object v10, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->examples:Landroid/widget/TextView;

    invoke-virtual {p0, v9, v10}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setDetailText(ILandroid/widget/TextView;)V

    .line 307
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 308
    iget-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->divider:Landroid/view/View;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 310
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 312
    iget-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->title:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 313
    iget-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->command:Landroid/widget/TextView;

    const/16 v10, 0x11

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setGravity(I)V

    .line 314
    iget-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->divider:Landroid/view/View;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 317
    :cond_2
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->lv:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 293
    :cond_3
    iget-object v10, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;->command:Landroid/widget/TextView;

    const-string/jumbo v9, "EXTRA_LIST_EXAMPLE"

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 320
    .end local v1    # "holder":Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolder;
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v5    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v6    # "row":Landroid/view/View;
    .end local v8    # "title":Ljava/lang/String;
    :cond_4
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->arrayString:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 321
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 322
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 323
    .local v4, "item":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$layout;->item_you_can_say_kk_widget:I

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 325
    .restart local v6    # "row":Landroid/view/View;
    new-instance v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolderKK;

    const/4 v9, 0x0

    invoke-direct {v1, v9}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolderKK;-><init>(Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$1;)V

    .line 327
    .local v1, "holder":Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolderKK;
    sget v9, Lcom/vlingo/midas/R$id;->item_you_can_say_widget_command:I

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolderKK;->command:Landroid/widget/TextView;

    .line 329
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->locale:Ljava/util/Locale;

    invoke-virtual {v9}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "ja_JP"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 330
    iget-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolderKK;->command:Landroid/widget/TextView;

    sget v10, Lcom/vlingo/midas/R$drawable;->voice_help_font_qmarksleft_jp:I

    const/4 v11, 0x0

    sget v12, Lcom/vlingo/midas/R$drawable;->voice_help_font_qmarksright_jp:I

    const/4 v13, 0x0

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 334
    :cond_5
    invoke-virtual {v6, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 336
    iget-object v9, v1, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolderKK;->command:Landroid/widget/TextView;

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 337
    iget v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->count:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->count:I

    const/4 v10, 0x3

    if-ne v9, v10, :cond_6

    .line 338
    const/4 v9, 0x0

    iput v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->count:I

    .line 340
    :cond_6
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->lv:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 344
    .end local v1    # "holder":Lcom/vlingo/midas/gui/widgets/YouCanSayWidget$ViewHolderKK;
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v4    # "item":Ljava/lang/String;
    .end local v6    # "row":Landroid/view/View;
    :cond_7
    return-void
.end method

.method public setDetailText(ILandroid/widget/TextView;)V
    .locals 6
    .param p1, "textResId"    # I
    .param p2, "textViewResId"    # Landroid/widget/TextView;

    .prologue
    .line 347
    if-eqz p2, :cond_1

    .line 348
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 350
    .local v1, "dyk":Ljava/lang/StringBuffer;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 351
    .local v4, "s":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 353
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "s":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 356
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    .end local v1    # "dyk":Ljava/lang/StringBuffer;
    :cond_1
    return-void
.end method
