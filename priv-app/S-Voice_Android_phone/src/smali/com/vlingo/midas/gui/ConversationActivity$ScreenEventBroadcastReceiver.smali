.class Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ConversationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ConversationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenEventBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ConversationActivity;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0

    .prologue
    .line 4706
    iput-object p1, p0, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;Lcom/vlingo/midas/gui/ConversationActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/ConversationActivity$1;

    .prologue
    .line 4706
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 4709
    if-eqz p2, :cond_2

    .line 4710
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 4713
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4718
    const-string/jumbo v2, "running_timer"

    invoke-static {v2, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4719
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 4724
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z
    invoke-static {v2}, Lcom/vlingo/midas/gui/ConversationActivity;->access$2500(Lcom/vlingo/midas/gui/ConversationActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4725
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # setter for: Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z
    invoke-static {v2, v4}, Lcom/vlingo/midas/gui/ConversationActivity;->access$2502(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z

    .line 4734
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;
    invoke-static {v2}, Lcom/vlingo/midas/gui/ConversationActivity;->access$800(Lcom/vlingo/midas/gui/ConversationActivity;)Lcom/vlingo/midas/gui/ContentFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ContentFragment;->cancelAsrEditing()V

    .line 4736
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # invokes: Lcom/vlingo/midas/gui/ConversationActivity;->retireLastWidget()V
    invoke-static {v2}, Lcom/vlingo/midas/gui/ConversationActivity;->access$2600(Lcom/vlingo/midas/gui/ConversationActivity;)V

    .line 4738
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4739
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/high16 v3, 0x80000

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 4740
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/high16 v3, 0x200000

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 4741
    sput-boolean v4, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    .line 4764
    .end local v0    # "action":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 4743
    .restart local v0    # "action":Ljava/lang/String;
    :cond_3
    const-string/jumbo v2, "com.samsung.cover.OPEN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4744
    const-string/jumbo v2, "coverOpen"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 4746
    .local v1, "coverOpened":Z
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->isInitialStickyBroadcast()Z

    move-result v2

    if-nez v2, :cond_2

    .line 4750
    if-eqz v1, :cond_4

    .line 4751
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->mIsNeedToDismissSecuredLock:Z
    invoke-static {v2}, Lcom/vlingo/midas/gui/ConversationActivity;->access$2500(Lcom/vlingo/midas/gui/ConversationActivity;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4753
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->unlockLockscreen(Z)V

    goto :goto_0

    .line 4756
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 4758
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$ScreenEventBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v2, v4}, Lcom/vlingo/midas/gui/ConversationActivity;->unlockLockscreen(Z)V

    .line 4760
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    goto :goto_0
.end method
