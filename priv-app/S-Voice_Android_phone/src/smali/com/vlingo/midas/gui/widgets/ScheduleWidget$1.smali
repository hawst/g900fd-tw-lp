.class Lcom/vlingo/midas/gui/widgets/ScheduleWidget$1;
.super Ljava/lang/Object;
.source "ScheduleWidget.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/ScheduleWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/ScheduleWidget;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 96
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 97
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 98
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.BodyChange"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mTitle:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->access$000(Lcom/vlingo/midas/gui/widgets/ScheduleWidget;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 102
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
