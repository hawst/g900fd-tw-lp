.class public Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "ScheduleSingleWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnTouchListener;"
    }
.end annotation


# instance fields
.field checkLocation:Z

.field checkParticipants:Z

.field isParticipants:Z

.field isSaveButton:Z

.field isloaction:Z

.field protected mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mAttendee:Landroid/widget/TextView;

.field protected mCancel:Lcom/vlingo/midas/gui/customviews/Button;

.field protected mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

.field private mContext:Landroid/content/Context;

.field private mDay:Landroid/widget/TextView;

.field private mLocation:Landroid/widget/TextView;

.field private mMonth:Landroid/widget/TextView;

.field private mRowDate:Landroid/widget/LinearLayout;

.field private mRowLocation:Landroid/widget/LinearLayout;

.field private mRowLocationline:Landroid/view/View;

.field private mRowParticipants:Landroid/widget/LinearLayout;

.field private mRowParticipantsline:Landroid/view/View;

.field private mRowTitle:Landroid/widget/LinearLayout;

.field private mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

.field private mTime:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mWeek:Landroid/widget/TextView;

.field private participants_line:I

.field private scheduleContainer:Landroid/widget/LinearLayout;

.field private schedule_button:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const/16 v0, 0x37

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->participants_line:I

    .line 65
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->isloaction:Z

    .line 66
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->isParticipants:Z

    .line 67
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->checkParticipants:Z

    .line 68
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->checkLocation:Z

    .line 69
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->isSaveButton:Z

    .line 76
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mContext:Landroid/content/Context;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method private initialize(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)V
    .locals 23
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    .line 159
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 163
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 164
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTitle:Landroid/widget/TextView;

    sget v20, Lcom/vlingo/midas/R$string;->my_event:I

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(I)V

    .line 169
    :goto_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v3

    .line 170
    .local v3, "begin":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v5

    .line 172
    .local v5, "end":J
    const-wide/16 v20, 0x0

    cmp-long v7, v3, v20

    if-eqz v7, :cond_7

    .line 173
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v11

    .line 174
    .local v11, "beginCal":Ljava/util/Calendar;
    new-instance v7, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v11, v7}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 176
    const/4 v7, 0x7

    const/16 v20, 0x2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v21

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v11, v7, v0, v1}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v19

    .line 177
    .local v19, "week":Ljava/lang/String;
    const/4 v7, 0x5

    invoke-virtual {v11, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    .line 178
    .local v12, "day":Ljava/lang/String;
    const/4 v7, 0x2

    const/16 v20, 0x2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v21

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v11, v7, v0, v1}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v16

    .line 182
    .local v16, "month":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v7, v0, :cond_0

    .line 183
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "0"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 184
    :cond_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mDay:Landroid/widget/TextView;

    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mMonth:Landroid/widget/TextView;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mWeek:Landroid/widget/TextView;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v8

    .line 193
    .local v8, "currentLocale":Ljava/util/Locale;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAllDay()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 194
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTime:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    sget v21, Lcom/vlingo/midas/R$string;->all_day:I

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    .end local v8    # "currentLocale":Ljava/util/Locale;
    .end local v11    # "beginCal":Ljava/util/Calendar;
    .end local v12    # "day":Ljava/lang/String;
    .end local v16    # "month":Ljava/lang/String;
    .end local v19    # "week":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v15

    .line 210
    .local v15, "location":Ljava/lang/String;
    invoke-static {v15}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 211
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocationline:Landroid/view/View;

    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 212
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 213
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mLocation:Landroid/widget/TextView;

    const-string/jumbo v20, ""

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->checkLocation:Z

    .line 224
    :goto_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v9

    .line 225
    .local v9, "attendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v9, :cond_1

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_a

    .line 226
    :cond_1
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->checkParticipants:Z

    .line 227
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipantsline:Landroid/view/View;

    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 228
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipants:Landroid/widget/LinearLayout;

    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 229
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mAttendee:Landroid/widget/TextView;

    const-string/jumbo v20, ""

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mAttendee:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    const-string/jumbo v20, ""

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 250
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mLocation:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    const-string/jumbo v20, ""

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 251
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->isloaction:Z

    .line 256
    :cond_3
    :goto_4
    return-void

    .line 166
    .end local v3    # "begin":J
    .end local v5    # "end":J
    .end local v9    # "attendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v15    # "location":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 196
    .restart local v3    # "begin":J
    .restart local v5    # "end":J
    .restart local v8    # "currentLocale":Ljava/util/Locale;
    .restart local v11    # "beginCal":Ljava/util/Calendar;
    .restart local v12    # "day":Ljava/lang/String;
    .restart local v16    # "month":Ljava/lang/String;
    .restart local v19    # "week":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 197
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTime:Landroid/widget/TextView;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, " - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEndTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 199
    :cond_6
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v7

    invoke-static/range {v3 .. v8}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatScheduleTime(JJZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    .line 200
    .local v18, "timeString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTime:Landroid/widget/TextView;

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 205
    .end local v8    # "currentLocale":Ljava/util/Locale;
    .end local v11    # "beginCal":Ljava/util/Calendar;
    .end local v12    # "day":Ljava/lang/String;
    .end local v16    # "month":Ljava/lang/String;
    .end local v18    # "timeString":Ljava/lang/String;
    .end local v19    # "week":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTime:Landroid/widget/TextView;

    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 216
    .restart local v15    # "location":Ljava/lang/String;
    :cond_8
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->checkLocation:Z

    .line 217
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v7

    if-nez v7, :cond_9

    .line 218
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocationline:Landroid/view/View;

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 220
    :cond_9
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mLocation:Landroid/widget/TextView;

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 231
    .restart local v9    # "attendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_a
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v7

    if-nez v7, :cond_b

    .line 232
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipantsline:Landroid/view/View;

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 234
    :cond_b
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->checkParticipants:Z

    .line 235
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 236
    .local v17, "s":Ljava/lang/StringBuilder;
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v10, v7, -0x1

    .line 237
    .local v10, "attendeesSize":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_5
    if-ge v13, v10, :cond_c

    .line 238
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v9, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v20, ", "

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 240
    :cond_c
    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mAttendee:Landroid/widget/TextView;

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mAttendee:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    div-int/lit8 v14, v7, 0x1e

    .line 244
    .local v14, "line_count":I
    if-lez v14, :cond_2

    .line 245
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mAttendee:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->participants_line:I

    move/from16 v20, v0

    add-int/lit8 v21, v14, 0x1

    mul-int v20, v20, v21

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setHeight(I)V

    goto/16 :goto_3

    .line 253
    .end local v10    # "attendeesSize":I
    .end local v13    # "i":I
    .end local v14    # "line_count":I
    .end local v17    # "s":Ljava/lang/StringBuilder;
    :cond_d
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->isParticipants:Z

    goto/16 :goto_4
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/schedule/ScheduleEvent;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 2
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->initialize(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)V

    .line 136
    if-eqz p2, :cond_0

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ConfirmButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->schedule_button:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V
    :try_end_0
    .catch Ljava/security/InvalidParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 153
    :goto_0
    if-eqz p4, :cond_1

    .line 154
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 156
    :cond_1
    return-void

    .line 143
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V
    :try_end_1
    .catch Ljava/security/InvalidParameterException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 145
    :catch_0
    move-exception v0

    goto :goto_0

    .line 148
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 40
    check-cast p1, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->initialize(Lcom/vlingo/core/internal/schedule/ScheduleEvent;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 454
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 448
    const/4 v0, 0x0

    return v0
.end method

.method public launchSchedule()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 282
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Edit"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 283
    .local v1, "svintent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-interface {v2, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 284
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowDate:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 285
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 286
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 287
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipants:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 288
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 289
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.android.calendar"

    const-string/jumbo v3, "com.android.calendar.detail.EventInfoActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    const-string/jumbo v2, "vnd.android.cursor.item/event"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEventID()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 292
    const-string/jumbo v2, "title"

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 293
    const-string/jumbo v2, "beginTime"

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 294
    const-string/jumbo v2, "endTime"

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 295
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 296
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 260
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 262
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->schedule_confirm:I

    if-ne v0, v1, :cond_0

    .line 264
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    if-eqz v1, :cond_0

    .line 265
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 272
    :catch_0
    move-exception v1

    goto :goto_0

    .line 269
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 81
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 82
    sget v0, Lcom/vlingo/midas/R$id;->schedule_day:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mDay:Landroid/widget/TextView;

    .line 83
    sget v0, Lcom/vlingo/midas/R$id;->schedule_month:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mMonth:Landroid/widget/TextView;

    .line 84
    sget v0, Lcom/vlingo/midas/R$id;->schedule_week:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mWeek:Landroid/widget/TextView;

    .line 85
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_title:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    .line 86
    sget v0, Lcom/vlingo/midas/R$id;->schedule_title:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTitle:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTitle:Landroid/widget/TextView;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 98
    sget v0, Lcom/vlingo/midas/R$id;->schedule_time:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mTime:Landroid/widget/TextView;

    .line 99
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_location:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    .line 100
    sget v0, Lcom/vlingo/midas/R$id;->schedule_location:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mLocation:Landroid/widget/TextView;

    .line 101
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_participants:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipants:Landroid/widget/LinearLayout;

    .line 102
    sget v0, Lcom/vlingo/midas/R$id;->schedule_attendee:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mAttendee:Landroid/widget/TextView;

    .line 103
    sget v0, Lcom/vlingo/midas/R$id;->schedule_button:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->schedule_button:Landroid/widget/LinearLayout;

    .line 104
    sget v0, Lcom/vlingo/midas/R$id;->schedule_confirm:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    .line 105
    sget v0, Lcom/vlingo/midas/R$id;->btn_cancel_schedule:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    .line 106
    sget v0, Lcom/vlingo/midas/R$id;->schedule_container:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->scheduleContainer:Landroid/widget/LinearLayout;

    .line 107
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_participants_line:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipantsline:Landroid/view/View;

    .line 108
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_location_line:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocationline:Landroid/view/View;

    .line 111
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_date:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowDate:Landroid/widget/LinearLayout;

    .line 112
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocationline:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipantsline:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowDate:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 124
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 125
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 126
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipants:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 127
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 348
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 393
    invoke-virtual {p1, v3}, Landroid/view/View;->playSoundEffect(I)V

    .line 394
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->launchSchedule()V

    .line 396
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 397
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 398
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_date:I

    if-ne v0, v1, :cond_2

    .line 399
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowDate:Landroid/widget/LinearLayout;

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_screen_custom_popup_01:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 442
    .end local v0    # "id":I
    :cond_1
    :goto_0
    return v4

    .line 400
    .restart local v0    # "id":I
    :cond_2
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_title:I

    if-ne v0, v1, :cond_7

    .line 401
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->isloaction:Z

    if-ne v1, v4, :cond_3

    .line 403
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 405
    :cond_3
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->checkParticipants:Z

    if-ne v1, v4, :cond_5

    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->checkLocation:Z

    if-nez v1, :cond_5

    .line 407
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_4

    .line 408
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 410
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 414
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_6

    .line 415
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 417
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 419
    :cond_7
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_location:I

    if-ne v0, v1, :cond_c

    .line 420
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->isParticipants:Z

    if-ne v1, v4, :cond_9

    .line 421
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_8

    .line 422
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 424
    :cond_8
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 427
    :cond_9
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_a

    .line 428
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 430
    :cond_a
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_b

    .line 431
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 433
    :cond_b
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 435
    :cond_c
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_participants:I

    if-ne v0, v1, :cond_1

    .line 436
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipants:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 437
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_1

    .line 438
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method
