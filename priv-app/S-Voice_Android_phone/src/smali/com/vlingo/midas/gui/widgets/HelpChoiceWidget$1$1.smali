.class Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1$1;
.super Ljava/lang/Object;
.source "HelpChoiceWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;

.field final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1$1;->this$1:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1$1;->val$v:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 329
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 330
    .local v0, "location":[I
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1$1;->val$v:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 331
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1$1;->this$1:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # invokes: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getScrollView()Landroid/widget/ScrollView;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$500(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Landroid/widget/ScrollView;

    move-result-object v1

    .line 332
    .local v1, "scrollView":Landroid/widget/ScrollView;
    if-eqz v1, :cond_1

    .line 333
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1$1;->this$1:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # invokes: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->isLT03()Z
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$600(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v2

    if-nez v2, :cond_2

    .line 337
    :cond_0
    aget v2, v0, v3

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1$1;->this$1:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;

    iget-object v3, v3, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->offsetLt03:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    .line 347
    :cond_1
    :goto_0
    return-void

    .line 339
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 340
    aget v2, v0, v3

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1$1;->this$1:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;

    iget-object v3, v3, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->miniModeScrollOffSet:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$400(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    goto :goto_0

    .line 343
    :cond_3
    aget v2, v0, v3

    invoke-virtual {v1, v4, v2}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    goto :goto_0
.end method
