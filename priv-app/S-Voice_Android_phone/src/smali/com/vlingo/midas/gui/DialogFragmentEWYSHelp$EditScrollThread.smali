.class public Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;
.super Ljava/lang/Thread;
.source "DialogFragmentEWYSHelp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EditScrollThread"
.end annotation


# static fields
.field private static final ANIM_FRAME_DELAY:I = 0x5

.field private static final ANIM_FRAME_MAX:I = 0x14

.field private static final ANIM_FRAME_MIN:I = 0x1

.field private static final ANIM_FRAME_MIN_START_POINT:I = 0x14


# instance fields
.field private final dialogFragment:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;",
            ">;"
        }
    .end annotation
.end field

.field private mDirection:I

.field private mFromHeight:I

.field private final mToHeight:I


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;I)V
    .locals 1
    .param p1, "dialogFragment"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .param p2, "direction"    # I

    .prologue
    .line 1011
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1012
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    .line 1014
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$200(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    .line 1015
    iput p2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mDirection:I

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 1016
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$300(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mToHeight:I

    .line 1019
    :goto_0
    return-void

    .line 1018
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mToHeight:I

    goto :goto_0
.end method

.method private continueLoop()Z
    .locals 2

    .prologue
    .line 1022
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .line 1023
    .local v0, "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    if-nez v0, :cond_0

    .line 1024
    const/4 v1, 0x0

    .line 1025
    :goto_0
    return v1

    :cond_0
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$400(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Z

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x14

    .line 1030
    const/16 v2, 0x14

    .line 1031
    .local v2, "value":I
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->continueLoop()Z

    move-result v0

    .line 1033
    .local v0, "continueLoop":Z
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mDirection:I

    if-nez v3, :cond_4

    .line 1034
    :cond_0
    :goto_0
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mToHeight:I

    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    if-ge v3, v4, :cond_1

    if-eqz v0, :cond_1

    .line 1035
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .line 1036
    .local v1, "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    if-nez v1, :cond_2

    .line 1073
    .end local v1    # "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    :cond_1
    :goto_1
    return-void

    .line 1039
    .restart local v1    # "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    :cond_2
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    # invokes: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doScrollAnim(I)V
    invoke-static {v1, v3}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$500(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;I)V

    .line 1040
    const/4 v1, 0x0

    .line 1042
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    sub-int/2addr v3, v2

    iput v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    .line 1044
    const-wide/16 v3, 0x5

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1048
    :goto_2
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mToHeight:I

    sub-int/2addr v3, v4

    if-ge v3, v5, :cond_0

    .line 1049
    const/4 v2, 0x1

    goto :goto_0

    .line 1058
    :cond_3
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    # invokes: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doScrollAnim(I)V
    invoke-static {v1, v3}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$500(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;I)V

    .line 1059
    const/4 v1, 0x0

    .line 1061
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    .line 1063
    const-wide/16 v3, 0x5

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1068
    :goto_3
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mToHeight:I

    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    sub-int/2addr v3, v4

    if-ge v3, v5, :cond_4

    .line 1069
    const/4 v2, 0x1

    .line 1053
    .end local v1    # "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    :cond_4
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mToHeight:I

    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->mFromHeight:I

    if-le v3, v4, :cond_1

    if-eqz v0, :cond_1

    .line 1054
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$EditScrollThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .line 1055
    .restart local v1    # "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    if-nez v1, :cond_3

    goto :goto_1

    .line 1045
    :catch_0
    move-exception v3

    goto :goto_2

    .line 1064
    :catch_1
    move-exception v3

    goto :goto_3
.end method
