.class public Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;
.super Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;
.source "PhoneTypeSelectWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$1;,
        Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/vlingo/core/internal/contacts/ContactData;",
        ">;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private contactList:Landroid/widget/ListView;

.field private final context:Landroid/content/Context;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->context:Landroid/content/Context;

    .line 46
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 32
    check-cast p1, Ljava/util/Map;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->initialize(Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 6
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "contactInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 57
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 59
    if-eqz p1, :cond_0

    .line 60
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 61
    .local v2, "phoneLabels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 63
    .local v1, "contactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, p0, v4, v2, v1}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    .line 64
    .local v0, "adapter":Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->contactList:Landroid/widget/ListView;

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 65
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->contactList:Landroid/widget/ListView;

    invoke-virtual {v4, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 66
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->contactList:Landroid/widget/ListView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 67
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->notifyDataSetChanged()V

    .line 69
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.DataTransfered"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 70
    .local v3, "transferIntent":Landroid/content/Intent;
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->getDisplayCount(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p4, v3, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 72
    .end local v0    # "adapter":Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;
    .end local v1    # "contactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v2    # "phoneLabels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "transferIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onFinishInflate()V

    .line 51
    sget v0, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->contactList:Landroid/widget/ListView;

    .line 52
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 157
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 158
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string/jumbo v1, "choice"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 161
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 162
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 167
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->contactList:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 168
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onMeasure(II)V

    .line 169
    return-void
.end method
