.class public Lcom/vlingo/midas/gui/widgets/ScheduleWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "ScheduleWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnTouchListener;"
    }
.end annotation


# instance fields
.field checkLocation:Z

.field checkParticipants:Z

.field private context:Landroid/content/Context;

.field protected isClicked:Z

.field isParticipants:Z

.field isSaveButton:Z

.field isloaction:Z

.field protected mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mAttendee:Landroid/widget/TextView;

.field private mButtonDivider:Landroid/view/View;

.field protected mCancel:Lcom/vlingo/midas/gui/customviews/Button;

.field protected mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

.field private mDay:Landroid/widget/TextView;

.field private mLocation:Landroid/widget/TextView;

.field private mMonth:Landroid/widget/TextView;

.field private mRowDate:Landroid/widget/LinearLayout;

.field private mRowLocation:Landroid/widget/LinearLayout;

.field private mRowLocationline:Landroid/view/View;

.field private mRowParticipants:Landroid/widget/LinearLayout;

.field private mRowParticipantsline:Landroid/view/View;

.field private mRowTitle:Landroid/widget/LinearLayout;

.field private mRowTitleline:Landroid/view/View;

.field private mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

.field private mTime:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mWeek:Landroid/widget/TextView;

.field private participants_line:I

.field private scheduleContainer:Landroid/widget/LinearLayout;

.field protected schedule_button:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const/16 v0, 0x37

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->participants_line:I

    .line 69
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isloaction:Z

    .line 70
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isParticipants:Z

    .line 71
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkParticipants:Z

    .line 72
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkLocation:Z

    .line 73
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isSaveButton:Z

    .line 74
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isClicked:Z

    .line 82
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->context:Landroid/content/Context;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/ScheduleWidget;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/ScheduleWidget;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method private initialize(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)V
    .locals 20
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .prologue
    .line 177
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 181
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 182
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mTitle:Landroid/widget/TextView;

    sget v7, Lcom/vlingo/midas/R$string;->my_event:I

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 187
    :goto_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v2

    .line 188
    .local v2, "begin":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v4

    .line 190
    .local v4, "end":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-eqz v6, :cond_6

    .line 191
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 192
    .local v10, "beginCal":Ljava/util/Calendar;
    new-instance v6, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-direct {v6, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v6}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 194
    const/4 v6, 0x7

    const/4 v7, 0x2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v10, v6, v7, v0}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v17

    .line 195
    .local v17, "week":Ljava/lang/String;
    const/4 v6, 0x5

    invoke-virtual {v10, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    .line 196
    .local v11, "day":Ljava/lang/String;
    const/4 v6, 0x2

    const/4 v7, 0x2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v10, v6, v7, v0}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v15

    .line 201
    .local v15, "month":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 202
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 203
    :cond_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mDay:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mMonth:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v18, 0x0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mWeek:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v18, 0x0

    const/16 v19, 0x1

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAllDay()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 212
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mTime:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v18, Lcom/vlingo/midas/R$string;->all_day:I

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    .end local v10    # "beginCal":Ljava/util/Calendar;
    .end local v11    # "day":Ljava/lang/String;
    .end local v15    # "month":Ljava/lang/String;
    .end local v17    # "week":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getLocation()Ljava/lang/String;

    move-result-object v14

    .line 225
    .local v14, "location":Ljava/lang/String;
    invoke-static {v14}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 226
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 227
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 228
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mLocation:Landroid/widget/TextView;

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkLocation:Z

    .line 239
    :goto_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAttendeeNames()Ljava/util/List;

    move-result-object v8

    .line 242
    .local v8, "attendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v8, :cond_1

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/core/internal/schedule/ScheduleUtil;->canSaveAttendee(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 243
    :cond_1
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkParticipants:Z

    .line 244
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 245
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipants:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 246
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mAttendee:Landroid/widget/TextView;

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mAttendee:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 267
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mLocation:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 268
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isloaction:Z

    .line 273
    :cond_3
    :goto_4
    return-void

    .line 184
    .end local v2    # "begin":J
    .end local v4    # "end":J
    .end local v8    # "attendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v14    # "location":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 214
    .restart local v2    # "begin":J
    .restart local v4    # "end":J
    .restart local v10    # "beginCal":Ljava/util/Calendar;
    .restart local v11    # "day":Ljava/lang/String;
    .restart local v15    # "month":Ljava/lang/String;
    .restart local v17    # "week":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mTime:Landroid/widget/TextView;

    move-object/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v6

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatScheduleTime(JJZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 220
    .end local v10    # "beginCal":Ljava/util/Calendar;
    .end local v11    # "day":Ljava/lang/String;
    .end local v15    # "month":Ljava/lang/String;
    .end local v17    # "week":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mTime:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 231
    .restart local v14    # "location":Ljava/lang/String;
    :cond_7
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkLocation:Z

    .line 232
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v6

    if-nez v6, :cond_8

    .line 233
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 235
    :cond_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mLocation:Landroid/widget/TextView;

    invoke-virtual {v6, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 248
    .restart local v8    # "attendees":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_9
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v6

    if-nez v6, :cond_a

    .line 249
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 251
    :cond_a
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkParticipants:Z

    .line 252
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 253
    .local v16, "s":Ljava/lang/StringBuilder;
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v9, v6, -0x1

    .line 254
    .local v9, "attendeesSize":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_5
    if-ge v12, v9, :cond_b

    .line 255
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v8, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->getContext()Landroid/content/Context;

    move-result-object v7

    sget v18, Lcom/vlingo/midas/R$string;->comma:I

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->getContext()Landroid/content/Context;

    move-result-object v7

    sget v18, Lcom/vlingo/midas/R$string;->space:I

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    .line 257
    :cond_b
    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mAttendee:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mAttendee:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    div-int/lit8 v13, v6, 0x1e

    .line 261
    .local v13, "line_count":I
    if-lez v13, :cond_2

    .line 262
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mAttendee:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->participants_line:I

    add-int/lit8 v18, v13, 0x1

    mul-int v7, v7, v18

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setHeight(I)V

    goto/16 :goto_3

    .line 270
    .end local v9    # "attendeesSize":I
    .end local v12    # "i":I
    .end local v13    # "line_count":I
    .end local v16    # "s":Ljava/lang/StringBuilder;
    :cond_c
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isParticipants:Z

    goto/16 :goto_4
.end method

.method private launchScheduleDetail()V
    .locals 4

    .prologue
    .line 344
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getNewEventUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 345
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 346
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.android.calendar"

    const-string/jumbo v2, "com.android.calendar.detail.EventInfoActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    const-string/jumbo v1, "vnd.android.cursor.item/event"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getNewEventUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 349
    const-string/jumbo v1, "beginTime"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 350
    const-string/jumbo v1, "endTime"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 351
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 353
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method


# virtual methods
.method protected cancel()V
    .locals 3

    .prologue
    .line 305
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    if-eqz v1, :cond_0

    .line 306
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isClicked:Z

    .line 308
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->retire()V

    .line 309
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 310
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 311
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 316
    :catch_0
    move-exception v1

    goto :goto_0

    .line 313
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public initialize(Lcom/vlingo/core/internal/schedule/ScheduleEvent;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 2
    .param p1, "event"    # Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->initialize(Lcom/vlingo/core/internal/schedule/ScheduleEvent;)V

    .line 152
    if-eqz p2, :cond_0

    :try_start_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ConfirmButton:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->schedule_button:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V
    :try_end_0
    .catch Ljava/security/InvalidParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 171
    :goto_0
    if-eqz p4, :cond_1

    .line 172
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 174
    :cond_1
    return-void

    .line 161
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V
    :try_end_1
    .catch Ljava/security/InvalidParameterException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 163
    :catch_0
    move-exception v0

    goto :goto_0

    .line 166
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 42
    check-cast p1, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->initialize(Lcom/vlingo/core/internal/schedule/ScheduleEvent;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 516
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isClicked:Z

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 511
    const/4 v0, 0x1

    return v0
.end method

.method public launchSchedule()V
    .locals 3

    .prologue
    .line 325
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Edit"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 326
    .local v0, "svintent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mScheduleEvent:Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 341
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 277
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 279
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->schedule_confirm:I

    if-ne v0, v1, :cond_1

    .line 281
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    if-eqz v1, :cond_0

    .line 282
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isClicked:Z

    .line 284
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->retire()V

    .line 287
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 298
    :cond_1
    sget v1, Lcom/vlingo/midas/R$id;->btn_cancel_schedule:I

    if-ne v0, v1, :cond_0

    .line 299
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->cancel()V

    goto :goto_0

    .line 294
    :catch_0
    move-exception v1

    goto :goto_0

    .line 291
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 87
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 88
    sget v0, Lcom/vlingo/midas/R$id;->schedule_day:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mDay:Landroid/widget/TextView;

    .line 89
    sget v0, Lcom/vlingo/midas/R$id;->schedule_month:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mMonth:Landroid/widget/TextView;

    .line 90
    sget v0, Lcom/vlingo/midas/R$id;->schedule_week:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mWeek:Landroid/widget/TextView;

    .line 91
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_title:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    .line 92
    sget v0, Lcom/vlingo/midas/R$id;->schedule_title:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mTitle:Landroid/widget/TextView;

    .line 93
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mTitle:Landroid/widget/TextView;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/ScheduleWidget$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/ScheduleWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 104
    sget v0, Lcom/vlingo/midas/R$id;->schedule_time:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mTime:Landroid/widget/TextView;

    .line 105
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_location:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    .line 106
    sget v0, Lcom/vlingo/midas/R$id;->schedule_location:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mLocation:Landroid/widget/TextView;

    .line 107
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_participants:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipants:Landroid/widget/LinearLayout;

    .line 108
    sget v0, Lcom/vlingo/midas/R$id;->schedule_attendee:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mAttendee:Landroid/widget/TextView;

    .line 109
    sget v0, Lcom/vlingo/midas/R$id;->schedule_button:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->schedule_button:Landroid/widget/LinearLayout;

    .line 110
    sget v0, Lcom/vlingo/midas/R$id;->schedule_confirm:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    .line 111
    sget v0, Lcom/vlingo/midas/R$id;->btn_cancel_schedule:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    .line 112
    sget v0, Lcom/vlingo/midas/R$id;->schedule_container:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->scheduleContainer:Landroid/widget/LinearLayout;

    .line 113
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_participants_line:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    .line 114
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_location_line:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    .line 115
    sget v0, Lcom/vlingo/midas/R$id;->schedule_button_divider:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mButtonDivider:Landroid/view/View;

    .line 118
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_date:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowDate:Landroid/widget/LinearLayout;

    .line 119
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    sget v0, Lcom/vlingo/midas/R$id;->schedule_row_title_line:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowTitleline:Landroid/view/View;

    .line 124
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowTitleline:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mButtonDivider:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 136
    :cond_1
    sget v0, Lcom/vlingo/midas/R$id;->schedule_container:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->scheduleContainer:Landroid/widget/LinearLayout;

    .line 139
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowDate:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 140
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 141
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 142
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipants:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 143
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 357
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 358
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 359
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_date:I

    if-ne v0, v1, :cond_4

    .line 398
    .end local v0    # "id":I
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 399
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 400
    .restart local v0    # "id":I
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_date:I

    if-ne v0, v1, :cond_7

    .line 401
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneLowGUI()Z

    move-result v1

    if-nez v1, :cond_1

    .line 402
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowDate:Landroid/widget/LinearLayout;

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_screen_custom_popup_01:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 448
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    if-eqz v1, :cond_2

    .line 449
    invoke-virtual {p1, v3}, Landroid/view/View;->playSoundEffect(I)V

    .line 450
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/Button;->isShown()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 451
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->launchSchedule()V

    .line 458
    .end local v0    # "id":I
    :cond_2
    :goto_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 459
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 460
    .restart local v0    # "id":I
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_date:I

    if-ne v0, v1, :cond_11

    .line 461
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneLowGUI()Z

    move-result v1

    if-nez v1, :cond_3

    .line 462
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowDate:Landroid/widget/LinearLayout;

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_screen_custom_popup_01:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 506
    .end local v0    # "id":I
    :cond_3
    :goto_3
    return v4

    .line 360
    .restart local v0    # "id":I
    :cond_4
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_title:I

    if-ne v0, v1, :cond_5

    .line 361
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isloaction:Z

    if-eq v1, v4, :cond_0

    .line 370
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkParticipants:Z

    if-ne v1, v4, :cond_0

    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkLocation:Z

    if-nez v1, :cond_0

    goto :goto_0

    .line 380
    :cond_5
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_location:I

    if-ne v0, v1, :cond_6

    .line 382
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isParticipants:Z

    if-ne v1, v4, :cond_0

    goto :goto_0

    .line 395
    :cond_6
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_participants:I

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 404
    :cond_7
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_title:I

    if-ne v0, v1, :cond_a

    .line 405
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isloaction:Z

    if-ne v1, v4, :cond_8

    .line 407
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-nez v1, :cond_1

    .line 408
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_1

    .line 411
    :cond_8
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkParticipants:Z

    if-ne v1, v4, :cond_9

    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkLocation:Z

    if-nez v1, :cond_9

    .line 413
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_1

    .line 414
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 415
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_1

    .line 420
    :cond_9
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_1

    .line 421
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 422
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 425
    :cond_a
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_location:I

    if-ne v0, v1, :cond_f

    .line 426
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isParticipants:Z

    if-ne v1, v4, :cond_c

    .line 427
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_b

    .line 428
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 430
    :cond_b
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 433
    :cond_c
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_d

    .line 434
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 436
    :cond_d
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_e

    .line 437
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 439
    :cond_e
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 441
    :cond_f
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_participants:I

    if-ne v0, v1, :cond_1

    .line 442
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipants:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 443
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_1

    .line 444
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 453
    :cond_10
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->launchScheduleDetail()V

    .line 454
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->retire()V

    goto/16 :goto_2

    .line 464
    :cond_11
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_title:I

    if-ne v0, v1, :cond_16

    .line 465
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isloaction:Z

    if-ne v1, v4, :cond_12

    .line 467
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 469
    :cond_12
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkParticipants:Z

    if-ne v1, v4, :cond_14

    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->checkLocation:Z

    if-nez v1, :cond_14

    .line 471
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_13

    .line 472
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 474
    :cond_13
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 478
    :cond_14
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_15

    .line 479
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 481
    :cond_15
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowTitle:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 483
    :cond_16
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_location:I

    if-ne v0, v1, :cond_1b

    .line 484
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->isParticipants:Z

    if-ne v1, v4, :cond_18

    .line 485
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_17

    .line 486
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 488
    :cond_17
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 491
    :cond_18
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_19

    .line 492
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocationline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 494
    :cond_19
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_1a

    .line 495
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 497
    :cond_1a
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowLocation:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 499
    :cond_1b
    sget v1, Lcom/vlingo/midas/R$id;->schedule_row_participants:I

    if-ne v0, v1, :cond_3

    .line 500
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipants:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 501
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_3

    .line 502
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mRowParticipantsline:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method public retire()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 521
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->retire()V

    .line 522
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V

    .line 525
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    if-eqz v0, :cond_1

    .line 526
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->mCancel:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setVisibility(I)V

    .line 528
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->schedule_button:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 529
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->schedule_button:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 531
    :cond_2
    return-void
.end method
