.class public abstract Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;
.super Lcom/vlingo/midas/gui/Widget;
.source "AnswerQuestionWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/Widget",
        "<",
        "Lcom/vlingo/core/internal/questions/Answer;",
        ">;"
    }
.end annotation


# static fields
.field private static density:I


# instance fields
.field private mContentLayout:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private yValue:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/Widget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->yValue:I

    .line 52
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->mContext:Landroid/content/Context;

    .line 53
    return-void
.end method

.method private createImageContent(Lcom/vlingo/core/internal/questions/Answer$Subsection;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .locals 1
    .param p1, "subsection"    # Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .param p2, "layout"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->isUsingOverlays()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->createImageContentWithOverlays(Lcom/vlingo/core/internal/questions/Answer$Subsection;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->createImageContentWithoutOverlays(Lcom/vlingo/core/internal/questions/Answer$Subsection;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private createImageContentWithOverlays(Lcom/vlingo/core/internal/questions/Answer$Subsection;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .locals 11
    .param p1, "subsection"    # Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .param p2, "layout"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 211
    iget v7, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->yValue:I

    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->yValue:I

    .line 213
    new-instance v3, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 215
    .local v3, "insert":Landroid/widget/RelativeLayout;
    invoke-virtual {v3, p2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 216
    sget v7, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->density:I

    mul-int/lit8 v7, v7, 0xc

    sget v8, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->density:I

    mul-int/lit8 v8, v8, 0xc

    invoke-virtual {v3, v7, v9, v8, v9}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 217
    const/16 v7, 0x11

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 219
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v4, v10, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 221
    .local v4, "relative":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v4, v9, v9, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 222
    const/16 v7, 0xd

    invoke-virtual {v4, v7, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 224
    new-instance v5, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;-><init>(Landroid/content/Context;)V

    .line 226
    .local v5, "ri":Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;
    invoke-virtual {v5, v4}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 227
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setAdjustViewBounds(Z)V

    .line 228
    invoke-virtual {v5, v9, v9, v9, v9}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setPadding(IIII)V

    .line 229
    invoke-virtual {v5, v9}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setFocusable(Z)V

    .line 230
    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->requestLayout()V

    .line 232
    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 234
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getImage()Lcom/vlingo/core/internal/questions/DownloadableImage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/questions/DownloadableImage;->isDownloaded()Z

    move-result v7

    if-nez v7, :cond_0

    .line 236
    new-instance v2, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v2, v7}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 242
    .local v2, "indicator":Landroid/widget/ProgressBar;
    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 244
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    sget v8, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->density:I

    div-int/2addr v7, v8

    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getHeight()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    sget v9, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->density:I

    div-int/2addr v8, v9

    sget-object v9, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 245
    .local v1, "filler":Landroid/graphics/Bitmap;
    invoke-virtual {v5, v1}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 249
    const-string/jumbo v7, "wa.download.timeout"

    const-string/jumbo v8, "30"

    invoke-static {v7, v8}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    mul-int/lit16 v6, v7, 0x3e8

    .line 252
    .local v6, "timeout":I
    new-instance v0, Ljava/util/Timer;

    const-string/jumbo v7, "AnswerQuestionWidget:DownloadTimer"

    invoke-direct {v0, v7}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    .line 254
    .local v0, "downloadTimer":Ljava/util/Timer;
    new-instance v7, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$2;

    invoke-direct {v7, p0, v0, p1}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$2;-><init>(Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;Ljava/util/Timer;Lcom/vlingo/core/internal/questions/Answer$Subsection;)V

    int-to-long v8, v6

    invoke-virtual {v0, v7, v8, v9}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 262
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getImage()Lcom/vlingo/core/internal/questions/DownloadableImage;

    move-result-object v7

    new-instance v8, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;

    invoke-direct {v8, p0, v0, v2, v5}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;-><init>(Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;Ljava/util/Timer;Landroid/widget/ProgressBar;Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;)V

    invoke-virtual {v7, v8}, Lcom/vlingo/core/internal/questions/DownloadableImage;->addListener(Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;)Lcom/vlingo/core/internal/questions/DownloadableImage;

    .line 288
    .end local v0    # "downloadTimer":Ljava/util/Timer;
    .end local v1    # "filler":Landroid/graphics/Bitmap;
    .end local v2    # "indicator":Landroid/widget/ProgressBar;
    .end local v6    # "timeout":I
    :goto_0
    return-object v3

    .line 285
    :cond_0
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getImage()Lcom/vlingo/core/internal/questions/DownloadableImage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/questions/DownloadableImage;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private createImageContentWithoutOverlays(Lcom/vlingo/core/internal/questions/Answer$Subsection;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .locals 6
    .param p1, "subsection"    # Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .param p2, "layout"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v5, 0x0

    .line 160
    iget v3, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->yValue:I

    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->yValue:I

    .line 162
    new-instance v1, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;-><init>(Landroid/content/Context;)V

    .line 164
    .local v1, "ri":Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;
    invoke-virtual {v1, p2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 165
    invoke-virtual {v1, v5, v5, v5, v5}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setPadding(IIII)V

    .line 166
    invoke-virtual {v1, v5}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setFocusable(Z)V

    .line 167
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->requestLayout()V

    .line 169
    move-object v2, v1

    .line 171
    .local v2, "view":Landroid/view/View;
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getImage()Lcom/vlingo/core/internal/questions/DownloadableImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/questions/DownloadableImage;->isDownloaded()Z

    move-result v3

    if-nez v3, :cond_0

    .line 173
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 175
    .local v0, "indicator":Landroid/widget/ProgressBar;
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setVisibility(I)V

    .line 177
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getImage()Lcom/vlingo/core/internal/questions/DownloadableImage;

    move-result-object v3

    new-instance v4, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$1;

    invoke-direct {v4, p0, v1, v0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;Landroid/widget/ProgressBar;)V

    invoke-virtual {v3, v4}, Lcom/vlingo/core/internal/questions/DownloadableImage;->addListener(Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;)Lcom/vlingo/core/internal/questions/DownloadableImage;

    .line 194
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContentLayout()Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 196
    move-object v2, v0

    .line 202
    .end local v0    # "indicator":Landroid/widget/ProgressBar;
    :goto_0
    return-object v2

    .line 199
    :cond_0
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getImage()Lcom/vlingo/core/internal/questions/DownloadableImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/questions/DownloadableImage;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private createSection(Lcom/vlingo/core/internal/questions/Answer$Section;Landroid/widget/LinearLayout$LayoutParams;)V
    .locals 2
    .param p1, "section"    # Lcom/vlingo/core/internal/questions/Answer$Section;
    .param p2, "layout"    # Landroid/widget/LinearLayout$LayoutParams;

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContentLayout()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->createSectionBanner(Lcom/vlingo/core/internal/questions/Answer$Section;Landroid/widget/LinearLayout$LayoutParams;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 91
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->populateSection(Lcom/vlingo/core/internal/questions/Answer$Section;Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    return-void
.end method

.method private createSectionBanner(Lcom/vlingo/core/internal/questions/Answer$Section;Landroid/widget/LinearLayout$LayoutParams;)Landroid/widget/TextView;
    .locals 4
    .param p1, "section"    # Lcom/vlingo/core/internal/questions/Answer$Section;
    .param p2, "layout"    # Landroid/widget/LinearLayout$LayoutParams;

    .prologue
    const/4 v3, 0x0

    .line 95
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 97
    .local v0, "name":Landroid/widget/TextView;
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$color;->answer_question_title_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 103
    :goto_0
    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 104
    sget v1, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->density:I

    mul-int/lit8 v1, v1, 0x18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 105
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-nez v1, :cond_0

    .line 106
    sget v1, Lcom/vlingo/midas/R$drawable;->voice_talk_subtitle_bg:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 109
    :cond_0
    iget v1, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->yValue:I

    invoke-virtual {v0}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->yValue:I

    .line 111
    sget v1, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->density:I

    mul-int/lit8 v1, v1, 0xc

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 112
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    invoke-virtual {v0}, Landroid/widget/TextView;->requestLayout()V

    .line 115
    return-object v0

    .line 101
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$color;->solid_white:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private createTextualContent(Lcom/vlingo/core/internal/questions/Answer$Subsection;Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/TextView;
    .locals 5
    .param p1, "subsection"    # Lcom/vlingo/core/internal/questions/Answer$Subsection;
    .param p2, "layout"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v4, 0x0

    .line 131
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 132
    .local v1, "text":Landroid/widget/TextView;
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getText()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "s":Ljava/lang/String;
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$color;->answer_question_text_color:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 140
    :goto_0
    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 141
    iget v2, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->yValue:I

    invoke-virtual {v1}, Landroid/widget/TextView;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->yValue:I

    .line 142
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 143
    invoke-virtual {v1}, Landroid/widget/TextView;->requestLayout()V

    .line 144
    return-object v1

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$color;->color_qasubsection:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private isUsingOverlays()Z
    .locals 2

    .prologue
    .line 152
    const-string/jumbo v0, "wa.image.overlays"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private populateSection(Lcom/vlingo/core/internal/questions/Answer$Section;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 6
    .param p1, "section"    # Lcom/vlingo/core/internal/questions/Answer$Section;
    .param p2, "layout"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 119
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer$Section;->getSubsections()[Lcom/vlingo/core/internal/questions/Answer$Subsection;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/questions/Answer$Subsection;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 120
    .local v3, "subsection":Lcom/vlingo/core/internal/questions/Answer$Subsection;
    invoke-interface {v3}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->hasImage()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 121
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContentLayout()Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-direct {p0, v3, p2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->createImageContent(Lcom/vlingo/core/internal/questions/Answer$Subsection;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 119
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_1
    invoke-interface {v3}, Lcom/vlingo/core/internal/questions/Answer$Subsection;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContentLayout()Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-direct {p0, v3, p2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->createTextualContent(Lcom/vlingo/core/internal/questions/Answer$Subsection;Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 128
    .end local v3    # "subsection":Lcom/vlingo/core/internal/questions/Answer$Subsection;
    :cond_2
    return-void
.end method


# virtual methods
.method protected getContentLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->mContentLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method protected abstract getLayoutID()I
.end method

.method public initialize(Lcom/vlingo/core/internal/questions/Answer;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 8
    .param p1, "answer"    # Lcom/vlingo/core/internal/questions/Answer;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const/4 v7, 0x0

    .line 66
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v2, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 67
    .local v2, "layout":Landroid/widget/LinearLayout$LayoutParams;
    sget v5, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->density:I

    mul-int/lit8 v5, v5, 0xf

    sget v6, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->density:I

    mul-int/lit8 v6, v6, 0x5

    invoke-virtual {v2, v7, v5, v7, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 69
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContentLayout()Landroid/widget/LinearLayout;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 70
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContentLayout()Landroid/widget/LinearLayout;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 73
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer;->getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    .local v0, "arr$":[Lcom/vlingo/core/internal/questions/Answer$Section;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v0, v1

    .line 74
    .local v4, "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    invoke-interface {v4}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "Image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v4}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "Input interpretation"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 75
    :cond_0
    invoke-direct {p0, v4, v2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->createSection(Lcom/vlingo/core/internal/questions/Answer$Section;Landroid/widget/LinearLayout$LayoutParams;)V

    .line 76
    invoke-interface {v4}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "Image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 82
    .end local v4    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    :cond_1
    invoke-interface {p1}, Lcom/vlingo/core/internal/questions/Answer;->getInformationalSections()[Lcom/vlingo/core/internal/questions/Answer$Section;

    move-result-object v0

    array-length v3, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v0, v1

    .line 83
    .restart local v4    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    invoke-interface {v4}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "Image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-interface {v4}, Lcom/vlingo/core/internal/questions/Answer$Section;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "Input interpretation"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 84
    invoke-direct {p0, v4, v2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->createSection(Lcom/vlingo/core/internal/questions/Answer$Section;Landroid/widget/LinearLayout$LayoutParams;)V

    .line 82
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 73
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 87
    .end local v4    # "section":Lcom/vlingo/core/internal/questions/Answer$Section;
    :cond_4
    return-void
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 40
    check-cast p1, Lcom/vlingo/core/internal/questions/Answer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->initialize(Lcom/vlingo/core/internal/questions/Answer;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/vlingo/midas/gui/Widget;->onFinishInflate()V

    .line 58
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getLayoutID()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->mContentLayout:Landroid/widget/LinearLayout;

    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int/lit16 v0, v0, 0xa0

    sput v0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->density:I

    .line 60
    return-void
.end method
