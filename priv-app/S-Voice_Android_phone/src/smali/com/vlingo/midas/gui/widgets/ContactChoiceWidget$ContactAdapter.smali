.class public Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;
.super Landroid/widget/BaseAdapter;
.source "ContactChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ContactAdapter"
.end annotation


# instance fields
.field DEFAULT_COLOR:[I

.field private DEFAULT_IMG:[I

.field private final avatars:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final contactPhone:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private img_cnt:I

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 7
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p4, "avatars":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    .local p5, "phoneNumber":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 161
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 144
    iput v2, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->img_cnt:I

    .line 145
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_01:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_02:I

    aput v1, v0, v3

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_03:I

    aput v1, v0, v4

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_04:I

    aput v1, v0, v5

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_05:I

    aput v1, v0, v6

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->DEFAULT_IMG:[I

    .line 153
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_1_bg_color:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_2_bg_color:I

    aput v1, v0, v3

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_3_bg_color:I

    aput v1, v0, v4

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_4_bg_color:I

    aput v1, v0, v5

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_5_bg_color:I

    aput v1, v0, v6

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->DEFAULT_COLOR:[I

    .line 162
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->contacts:Ljava/util/List;

    .line 163
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->avatars:Ljava/util/List;

    .line 164
    iput-object p5, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->contactPhone:Ljava/util/List;

    .line 165
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 177
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 178
    .local v0, "toReturn":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;->getLimitedCount(I)I

    move-result v1

    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 172
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x4

    .line 183
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 184
    .local v1, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->avatars:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 185
    .local v0, "avatar":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->contactPhone:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 188
    .local v3, "phoneData":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 189
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$layout;->item_contact_choice:I

    invoke-static {v4, v5, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 190
    new-instance v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;

    invoke-direct {v2, v8}, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$1;)V

    .line 191
    .local v2, "holder":Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;
    sget v4, Lcom/vlingo/midas/R$id;->item_choice_container:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->choice_container:Landroid/widget/LinearLayout;

    .line 192
    sget v4, Lcom/vlingo/midas/R$id;->text_title:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->title:Landroid/widget/TextView;

    .line 193
    sget v4, Lcom/vlingo/midas/R$id;->image_contact:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->qcb:Landroid/widget/ImageView;

    .line 194
    sget v4, Lcom/vlingo/midas/R$id;->text_number:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->phone:Landroid/widget/TextView;

    .line 195
    sget v4, Lcom/vlingo/midas/R$id;->star_img:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->star_img:Landroid/view/View;

    .line 196
    sget v4, Lcom/vlingo/midas/R$id;->divider:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->divider:Landroid/view/View;

    .line 198
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->title:Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->SECRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 199
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->phone:Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->SECRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 200
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 204
    :goto_0
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v5, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->phone:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-boolean v4, v1, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    if-eqz v4, :cond_2

    .line 208
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->star_img:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 213
    :goto_1
    if-nez v0, :cond_5

    .line 214
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 215
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 216
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->DEFAULT_IMG:[I

    iget v6, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->img_cnt:I

    rem-int/lit8 v6, v6, 0x5

    aget v5, v5, v6

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 217
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;

    invoke-virtual {v4, v0}, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;->getRoundedShape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 218
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->qcb:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 219
    iget v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->img_cnt:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->img_cnt:I

    .line 228
    :goto_2
    iget v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->img_cnt:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->img_cnt:I

    .line 233
    :goto_3
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 234
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 235
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne p1, v4, :cond_0

    .line 236
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->divider:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 249
    :cond_0
    :goto_4
    new-instance v4, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter$1;

    invoke-direct {v4, p0, p1}, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;I)V

    invoke-virtual {p2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    return-object p2

    .line 202
    .end local v2    # "holder":Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;

    .restart local v2    # "holder":Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;
    goto :goto_0

    .line 210
    :cond_2
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->star_img:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 221
    :cond_3
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->qcb:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->DEFAULT_COLOR:[I

    rem-int/lit8 v6, p1, 0x5

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    .line 224
    :cond_4
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->DEFAULT_IMG:[I

    iget v6, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->img_cnt:I

    rem-int/lit8 v6, v6, 0x5

    aget v5, v5, v6

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 225
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;

    invoke-virtual {v4, v0}, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;->getRoundedShape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 226
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->qcb:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 230
    :cond_5
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->qcb:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3

    .line 239
    :cond_6
    if-nez p1, :cond_7

    .line 240
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4

    .line 241
    :cond_7
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne p1, v4, :cond_8

    .line 242
    iget-object v4, v2, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ViewHolder;->divider:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 243
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4

    .line 245
    :cond_8
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4
.end method
