.class public final enum Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;
.super Ljava/lang/Enum;
.source "EditWhatYouSaidHelpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppCarMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

.field public static final enum Driving:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

.field public static final enum None:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

.field public static final enum Regular:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 129
    new-instance v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    const-string/jumbo v1, "None"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;->None:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    new-instance v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    const-string/jumbo v1, "Driving"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    new-instance v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    const-string/jumbo v1, "Regular"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;->Regular:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    .line 128
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;->None:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;->Regular:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;->$VALUES:[Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 128
    const-class v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;->$VALUES:[Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    invoke-virtual {v0}, [Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;

    return-object v0
.end method
