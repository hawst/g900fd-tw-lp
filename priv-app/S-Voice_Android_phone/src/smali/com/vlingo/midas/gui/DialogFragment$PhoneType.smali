.class final enum Lcom/vlingo/midas/gui/DialogFragment$PhoneType;
.super Ljava/lang/Enum;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "PhoneType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/gui/DialogFragment$PhoneType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

.field public static final enum K_DEVICE:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

.field public static final enum OTHER_PHONES:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 151
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    const-string/jumbo v1, "K_DEVICE"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->K_DEVICE:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    new-instance v0, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    const-string/jumbo v1, "OTHER_PHONES"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->OTHER_PHONES:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    .line 150
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    sget-object v1, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->K_DEVICE:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->OTHER_PHONES:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->$VALUES:[Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/gui/DialogFragment$PhoneType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 150
    const-class v0, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/gui/DialogFragment$PhoneType;
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->$VALUES:[Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    invoke-virtual {v0}, [Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    return-object v0
.end method
