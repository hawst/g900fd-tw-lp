.class Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;
.super Ljava/lang/Object;
.source "RegularControlFragmentBase.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/RegularControlFragmentBase;->setNormalPanel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 210
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    # getter for: Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_TIMESTAMP:J
    invoke-static {v2}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->access$100(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3a98

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    # setter for: Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_STEP:I
    invoke-static {v0, v4}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->access$202(Lcom/vlingo/midas/gui/RegularControlFragmentBase;I)I

    .line 215
    :cond_0
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    # getter for: Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_STEP:I
    invoke-static {v1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->access$200(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 216
    sput-boolean v5, Lcom/vlingo/midas/settings/debug/DebugSettings;->SHOW_DEBUG:Z

    .line 217
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->debug_settings_show_enabled:I

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 226
    :goto_0
    return v4

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    # setter for: Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_STEP:I
    invoke-static {v0, v5}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->access$202(Lcom/vlingo/midas/gui/RegularControlFragmentBase;I)I

    .line 220
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$1;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    # setter for: Lcom/vlingo/midas/gui/RegularControlFragmentBase;->DEBUG_UNLOCK_TIMESTAMP:J
    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->access$102(Lcom/vlingo/midas/gui/RegularControlFragmentBase;J)J

    goto :goto_0
.end method
