.class public final enum Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;
.super Ljava/lang/Enum;
.source "ControlFragmentEWYSHelp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ControlState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

.field public static final enum ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

.field public static final enum ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

.field public static final enum ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

.field public static final enum ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

.field public static final enum ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

.field public static final enum DIALOG_BUSY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

.field public static final enum IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    const-string/jumbo v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    const-string/jumbo v1, "ASR_GETTING_READY"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    const-string/jumbo v1, "ASR_LISTENING"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    const-string/jumbo v1, "ASR_THINKING"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    const-string/jumbo v1, "ASR_POST_RESPONSE"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    const-string/jumbo v1, "ASR_EDITING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    const-string/jumbo v1, "DIALOG_BUSY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->DIALOG_BUSY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->DIALOG_BUSY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->$VALUES:[Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 62
    const-class v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->$VALUES:[Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v0}, [Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    return-object v0
.end method
