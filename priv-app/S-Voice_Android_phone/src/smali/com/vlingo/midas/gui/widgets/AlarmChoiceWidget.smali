.class public Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;
.super Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;
.source "AlarmChoiceWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;,
        Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/vlingo/core/internal/util/Alarm;",
        ">;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field private static final INTENT_EDIT_ALARM_ACTION:Ljava/lang/String; = "com.sec.android.clockpackage.EDIT_ALARM"

.field private static final INTENT_EDIT_ALARM_EXTRA:Ljava/lang/String; = "listitemId"


# instance fields
.field day_arr:[I

.field private mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mAlarmList:Landroid/widget/ListView;

.field private mContext:Landroid/content/Context;

.field private mHour:I

.field private mMin:I

.field private mTempHour:I

.field private savedAlarm:Z

.field private widgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->savedAlarm:Z

    .line 76
    const/4 v0, 0x7

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->day_arr:[I

    .line 60
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mContext:Landroid/content/Context;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    .prologue
    .line 40
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    .prologue
    .line 40
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mMin:I

    return v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mMin:I

    return p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    .prologue
    .line 40
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I

    return v0
.end method

.method static synthetic access$202(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I

    return p1
.end method

.method static synthetic access$220(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mTempHour:I

    return v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->widgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mAlarmList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->openAlarmApp(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    return-object v0
.end method

.method private openAlarmApp(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 508
    sget-object v2, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$1;->$SwitchMap$com$vlingo$core$internal$dialogmanager$util$WidgetUtil$WidgetKey:[I

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->widgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 547
    :cond_0
    :goto_0
    return-void

    .line 525
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.sec.android.clockpackage.EDIT_ALARM"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 526
    .local v0, "editIntent":Landroid/content/Intent;
    const-string/jumbo v2, "listitemId"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 527
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 531
    .end local v0    # "editIntent":Landroid/content/Intent;
    :pswitch_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->savedAlarm:Z

    if-eqz v2, :cond_0

    .line 532
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SET_ALARM"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 533
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v2, "android.intent.extra.alarm.MESSAGE"

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->core_default_alarm_title:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 537
    const-string/jumbo v2, "android.intent.extra.alarm.HOUR"

    iget v3, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mHour:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 538
    const-string/jumbo v2, "android.intent.extra.alarm.MINUTES"

    iget v3, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mMin:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 539
    const-string/jumbo v2, "android.intent.extra.alarm.SKIP_UI"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 540
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 508
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 40
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 3
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/Alarm;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "allAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/Alarm;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 82
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 83
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->widgetKey:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    .line 84
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {p3, v0}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->savedAlarm:Z

    .line 87
    :cond_0
    if-eqz p1, :cond_1

    .line 88
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mAlarmList:Landroid/widget/ListView;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 90
    :cond_1
    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 503
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 497
    const/4 v0, 0x0

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 96
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletHighGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mAlarmList:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->setListViewHeightBasedOnChildrens(Landroid/widget/ListView;Z)V

    .line 98
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onFinishInflate()V

    .line 66
    sget v0, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mAlarmList:Landroid/widget/ListView;

    .line 67
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mAlarmList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 68
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mAlarmList:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 69
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 492
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mAlarmList:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 74
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onMeasure(II)V

    .line 75
    return-void
.end method
