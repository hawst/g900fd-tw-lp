.class public Lcom/vlingo/midas/gui/customviews/MicListeningView;
.super Landroid/widget/ImageView;
.source "MicListeningView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/customviews/MicListeningView$SineEaseOut;
    }
.end annotation


# static fields
.field private static final SINE_OUT:Landroid/view/animation/Interpolator;


# instance fields
.field private duration:I

.field private isRestore:Z

.field private mAnimator:Landroid/view/ViewPropertyAnimator;

.field private mRatio:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicListeningView$SineEaseOut;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView$SineEaseOut;-><init>(Lcom/vlingo/midas/gui/customviews/MicListeningView$1;)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->SINE_OUT:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mRatio:F

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->isRestore:Z

    .line 18
    const/16 v0, 0x64

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->duration:I

    .line 22
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/customviews/MicListeningView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningView;

    .prologue
    .line 12
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->isRestore:Z

    return v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/gui/customviews/MicListeningView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/MicListeningView;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->isRestore:Z

    return p1
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 67
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/customviews/MicListeningView;->SINE_OUT:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 69
    :cond_0
    return-void
.end method

.method public startScaleAnimation(F)V
    .locals 3
    .param p1, "ratio"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 25
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    if-nez v0, :cond_0

    .line 26
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setScaleX(F)V

    .line 27
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->setScaleY(F)V

    .line 29
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicListeningView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    .line 30
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/vlingo/midas/gui/customviews/MicListeningView$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/customviews/MicListeningView$1;-><init>(Lcom/vlingo/midas/gui/customviews/MicListeningView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 55
    :cond_0
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->isRestore:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mRatio:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 62
    :goto_0
    return-void

    .line 58
    :cond_1
    iput p1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mRatio:F

    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 60
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->mAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/midas/gui/customviews/MicListeningView;->duration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/customviews/MicListeningView;->SINE_OUT:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method
