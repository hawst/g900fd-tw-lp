.class public Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "CMAWeatherSevenDayWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$1;,
        Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;,
        Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$CMAWeatherSevenDayWidgetHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final CITY_ID_LENGHT:I = 0x7

.field private static final FIFTH_FORECAST_DAY:I = 0x5

.field private static final FIRST_FORECAST_DAY:I = 0x1

.field private static final FOURTH_FORECAST_DAY:I = 0x4

.field private static final SECOND_FORECAST_DAY:I = 0x2

.field private static final THIRD_FORECAST_DAY:I = 0x3

.field private static final ZEROTH_FORECAST_DAY:I

.field private static forecastArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected accuWeather:Landroid/widget/ImageView;

.field protected accuWeather_Web:Landroid/widget/LinearLayout;

.field protected citiDetail:Landroid/widget/LinearLayout;

.field protected citi_name:Landroid/widget/TextView;

.field protected citi_name2:Landroid/widget/TextView;

.field private city:Ljava/lang/String;

.field private cityId:Ljava/lang/String;

.field private climateName:Ljava/lang/String;

.field protected climate_name:Landroid/widget/TextView;

.field private currentDateString:Ljava/lang/String;

.field private currentMax:Ljava/lang/String;

.field private currentMin:Ljava/lang/String;

.field private currentTemp:Ljava/lang/String;

.field private currentWeatherCode:Ljava/lang/String;

.field protected date1:Landroid/widget/TextView;

.field protected date2:Landroid/widget/TextView;

.field protected date3:Landroid/widget/TextView;

.field protected date4:Landroid/widget/TextView;

.field protected date5:Landroid/widget/TextView;

.field protected date6:Landroid/widget/TextView;

.field protected date_val:Ljava/lang/String;

.field protected date_value:Landroid/widget/TextView;

.field protected divider:Landroid/widget/TextView;

.field private forecastingWeekly:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

.field protected isNightmode:Z

.field protected layout1:Landroid/widget/LinearLayout;

.field protected layout2:Landroid/widget/LinearLayout;

.field protected layout3:Landroid/widget/LinearLayout;

.field protected layout4:Landroid/widget/LinearLayout;

.field protected layout5:Landroid/widget/LinearLayout;

.field protected layout6:Landroid/widget/LinearLayout;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field protected location:Ljava/lang/String;

.field protected location2:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field protected mCurrentTemp:Landroid/widget/TextView;

.field private mHandler:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$CMAWeatherSevenDayWidgetHandler;

.field protected mUnit_meter_C:Landroid/widget/ImageView;

.field protected mUnit_meter_F:Landroid/widget/ImageView;

.field protected max:Ljava/lang/String;

.field protected maxTemperature1:Landroid/widget/TextView;

.field protected maxTemperature2:Landroid/widget/TextView;

.field protected maxTemperature3:Landroid/widget/TextView;

.field protected maxTemperature4:Landroid/widget/TextView;

.field protected maxTemperature5:Landroid/widget/TextView;

.field protected maxTemperature6:Landroid/widget/TextView;

.field protected maxTemperatureOneDay1:Landroid/widget/TextView;

.field protected min:Ljava/lang/String;

.field protected minTemperature1:Landroid/widget/TextView;

.field protected minTemperature2:Landroid/widget/TextView;

.field protected minTemperature3:Landroid/widget/TextView;

.field protected minTemperature4:Landroid/widget/TextView;

.field protected minTemperature5:Landroid/widget/TextView;

.field protected minTemperature6:Landroid/widget/TextView;

.field protected minTemperatureOneDay1:Landroid/widget/TextView;

.field protected temp_unit:Ljava/lang/String;

.field private wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

.field protected weatherBackgroundImage:Landroid/widget/FrameLayout;

.field protected weatherBackgroundImageNew:Landroid/widget/FrameLayout;

.field private weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

.field protected weatherImage1:Landroid/widget/ImageView;

.field protected weatherImage2:Landroid/widget/ImageView;

.field protected weatherImage3:Landroid/widget/ImageView;

.field protected weatherImage4:Landroid/widget/ImageView;

.field protected weatherImage5:Landroid/widget/ImageView;

.field protected weatherImage6:Landroid/widget/ImageView;

.field protected weatherImageOneDay1:Landroid/widget/ImageView;

.field protected weekday1:Landroid/widget/TextView;

.field protected weekday2:Landroid/widget/TextView;

.field protected weekday3:Landroid/widget/TextView;

.field protected weekday4:Landroid/widget/TextView;

.field protected weekday5:Landroid/widget/TextView;

.field protected weekday6:Landroid/widget/TextView;

.field protected widgetCitiLinear:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->isNightmode:Z

    .line 151
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->cityId:Ljava/lang/String;

    .line 400
    new-instance v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$CMAWeatherSevenDayWidgetHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$CMAWeatherSevenDayWidgetHandler;-><init>(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$CMAWeatherSevenDayWidgetHandler;

    .line 156
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mContext:Landroid/content/Context;

    .line 158
    return-void
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->setValuesForecastWeatherOneDay()V

    return-void
.end method

.method static synthetic access$700(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->setValuesForecastWeather()V

    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->setTalkbackString()V

    return-void
.end method

.method private isLT03()Z
    .locals 3

    .prologue
    const/16 v2, 0xa00

    const/16 v1, 0x640

    .line 724
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v0, v2, :cond_2

    .line 728
    :cond_1
    const/4 v0, 0x1

    .line 730
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private openChinaWeatherWeb()V
    .locals 2

    .prologue
    .line 632
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 633
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "http://samsung.weather.com.cn/mobile/index.html"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 635
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 636
    return-void
.end method

.method private setPhenomenonWeatherCode(Lcom/vlingo/core/internal/weather/china/WeatherItem;)V
    .locals 1
    .param p1, "weatherItem"    # Lcom/vlingo/core/internal/weather/china/WeatherItem;

    .prologue
    .line 367
    iget-object v0, p1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherPhenomenon:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->climateName:Ljava/lang/String;

    .line 368
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->climateName:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherPhenomenon:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->climateName:Ljava/lang/String;

    .line 370
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->climateName:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->climateName:Ljava/lang/String;

    .line 374
    :cond_0
    iget-object v0, p1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentWeatherCode:Ljava/lang/String;

    .line 375
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentWeatherCode:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    iget-object v0, p1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentWeatherCode:Ljava/lang/String;

    .line 377
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentWeatherCode:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    const-string/jumbo v0, "99"

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentWeatherCode:Ljava/lang/String;

    .line 381
    :cond_1
    return-void
.end method

.method private setTalkbackString()V
    .locals 5

    .prologue
    .line 561
    const-string/jumbo v0, "C"

    .line 562
    .local v0, "tempUnit":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xb0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 564
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperatureOneDay1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 567
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 570
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 573
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 576
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature2:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature2:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 579
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature2:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature2:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 582
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature3:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature3:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 585
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature3:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature3:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 588
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature4:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature4:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 591
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature4:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature4:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 594
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature5:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature5:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 597
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature5:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature5:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 600
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature6:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature6:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 603
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature6:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature6:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 606
    return-void
.end method

.method private setValuesForecastWeather()V
    .locals 8

    .prologue
    const/16 v7, 0x14

    .line 465
    const-string/jumbo v5, "#585f64"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    .line 466
    .local v3, "textColor":I
    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    if-eqz v5, :cond_2

    .line 467
    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 468
    .local v1, "forecastArrayListSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 469
    const-string/jumbo v5, "#585f64"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    .line 470
    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDay()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$900(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v0

    .line 472
    .local v0, "day":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-static {v5, v7}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    .line 474
    const-string/jumbo v5, "#ea7445"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    .line 479
    :cond_0
    :goto_1
    new-instance v4, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getWeatherCode()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1000(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {v4, v5}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;-><init>(I)V

    .line 483
    .local v4, "wru":Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
    packed-switch v2, :pswitch_data_0

    .line 468
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 475
    .end local v4    # "wru":Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
    :cond_1
    const/4 v5, 0x7

    invoke-static {v5, v7}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    .line 477
    const-string/jumbo v5, "#585f64"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 485
    .restart local v4    # "wru":Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
    :pswitch_0
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday1:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDay()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$900(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 486
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday1:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 487
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date1:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 488
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage1:Landroid/widget/ImageView;

    invoke-virtual {v4}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 490
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature1:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMinTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1100(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 492
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature1:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1200(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 497
    :pswitch_1
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday2:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDay()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$900(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday2:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 499
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date2:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 500
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage2:Landroid/widget/ImageView;

    invoke-virtual {v4}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 502
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature2:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMinTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1100(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 504
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature2:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1200(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 509
    :pswitch_2
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday3:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDay()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$900(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 510
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday3:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 511
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date3:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 512
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage3:Landroid/widget/ImageView;

    invoke-virtual {v4}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 514
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature3:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMinTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1100(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 516
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature3:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1200(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 521
    :pswitch_3
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday4:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDay()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$900(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday4:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 523
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date4:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 524
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage4:Landroid/widget/ImageView;

    invoke-virtual {v4}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 526
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature4:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMinTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1100(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 528
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature4:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1200(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 532
    :pswitch_4
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday5:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDay()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$900(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday5:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 534
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date5:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 535
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage5:Landroid/widget/ImageView;

    invoke-virtual {v4}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 537
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature5:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMinTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1100(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 539
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature5:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1200(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 543
    :pswitch_5
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday6:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDay()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$900(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 544
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday6:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 545
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date6:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 546
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage6:Landroid/widget/ImageView;

    invoke-virtual {v4}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherDrawableSmallSize()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 548
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature6:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMinTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1100(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 550
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature6:Landroid/widget/TextView;

    sget-object v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$1200(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 556
    .end local v0    # "day":Ljava/lang/String;
    .end local v1    # "forecastArrayListSize":I
    .end local v2    # "i":I
    .end local v4    # "wru":Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
    :cond_2
    return-void

    .line 483
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setValuesForecastWeatherOneDay()V
    .locals 10

    .prologue
    const/16 v9, 0x63

    const/4 v8, 0x0

    const/16 v7, 0xb0

    const/16 v6, 0x8

    .line 404
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->citi_name:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->city:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date_value:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentDateString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->climate_name:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->climateName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMin:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 408
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 409
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMin:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 413
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMax:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 414
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 415
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMax:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 426
    :cond_1
    :goto_1
    new-instance v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentWeatherCode:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v2, v3}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;-><init>(I)V

    .line 428
    .local v2, "wru":Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
    invoke-virtual {v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherDrawable()I

    move-result v0

    .line 429
    .local v0, "weatherDrawableCode":I
    if-eq v0, v9, :cond_2

    .line 430
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImageOneDay1:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 433
    :cond_2
    invoke-virtual {v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherSevenBackgroundDrawable()I

    move-result v1

    .line 435
    .local v1, "weatherSevenBackgroundDrawableCode":I
    if-eq v1, v9, :cond_3

    .line 437
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->isNightmode:Z

    if-eqz v3, :cond_3

    .line 438
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherBackgroundImageNew:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->changeToNightBackground(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 448
    :cond_3
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentTemp:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 449
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentTemp:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 451
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mUnit_meter_C:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 452
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mUnit_meter_F:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 462
    :goto_2
    return-void

    .line 411
    .end local v0    # "weatherDrawableCode":I
    .end local v1    # "weatherSevenBackgroundDrawableCode":I
    .end local v2    # "wru":Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
    :cond_4
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMin:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 417
    :cond_5
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMax:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 418
    :cond_6
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentTemp:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 419
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 420
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentTemp:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 422
    :cond_7
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentTemp:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 455
    .restart local v0    # "weatherDrawableCode":I
    .restart local v1    # "weatherSevenBackgroundDrawableCode":I
    .restart local v2    # "wru":Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
    :cond_8
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMax:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 456
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mUnit_meter_C:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 457
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mUnit_meter_F:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 458
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v3

    if-nez v3, :cond_9

    .line 459
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mUnit_meter_C:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460
    :cond_9
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mUnit_meter_F:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 1
    .param p1, "adaptor"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 245
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 246
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    .line 247
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->onResponseReceived()V

    .line 249
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isNightMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->isNightmode:Z

    .line 250
    return-void
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 48
    check-cast p1, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->initialize(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 617
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 611
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 689
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 690
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->child_accuweather:I

    if-ne v0, v1, :cond_0

    .line 691
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->openChinaWeatherWeb()V

    .line 693
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v6, 0x0

    .line 697
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 699
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->isLT03()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 701
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$dimen;->widget_weather_left_right_margin:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 703
    .local v2, "citiRightMargin":I
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->citiDetail:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    .line 704
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$dimen;->widget_weather_citi_top_margin:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 706
    .local v3, "citiTopMargin":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$dimen;->widget_weather_citi_max_width:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 708
    .local v1, "citiMaxWidth":I
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->citiDetail:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6, v3, v2, v6}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 709
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->citiDetail:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout$LayoutParams;

    iput v1, v4, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 713
    .end local v1    # "citiMaxWidth":I
    .end local v3    # "citiTopMargin":I
    :cond_0
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    if-eqz v4, :cond_1

    .line 714
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$dimen;->widget_weather_accu_bottom_margin:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 716
    .local v0, "accuBottomMargin":I
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout$LayoutParams;

    iput v0, v4, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 717
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v4, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 720
    .end local v0    # "accuBottomMargin":I
    .end local v2    # "citiRightMargin":I
    :cond_1
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 167
    sget v0, Lcom/vlingo/midas/R$id;->child_layout1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout1:Landroid/widget/LinearLayout;

    .line 168
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    sget v0, Lcom/vlingo/midas/R$id;->child_layout2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout2:Landroid/widget/LinearLayout;

    .line 170
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    sget v0, Lcom/vlingo/midas/R$id;->child_layout3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout3:Landroid/widget/LinearLayout;

    .line 172
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout3:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    sget v0, Lcom/vlingo/midas/R$id;->child_layout4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout4:Landroid/widget/LinearLayout;

    .line 174
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout4:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    sget v0, Lcom/vlingo/midas/R$id;->child_layout5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout5:Landroid/widget/LinearLayout;

    .line 176
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout5:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    sget v0, Lcom/vlingo/midas/R$id;->child_layout6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout6:Landroid/widget/LinearLayout;

    .line 178
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->layout6:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday1:Landroid/widget/TextView;

    .line 182
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday2:Landroid/widget/TextView;

    .line 183
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday3:Landroid/widget/TextView;

    .line 184
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday4:Landroid/widget/TextView;

    .line 185
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday5:Landroid/widget/TextView;

    .line 186
    sget v0, Lcom/vlingo/midas/R$id;->child_weekday6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weekday6:Landroid/widget/TextView;

    .line 188
    sget v0, Lcom/vlingo/midas/R$id;->child_date1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date1:Landroid/widget/TextView;

    .line 189
    sget v0, Lcom/vlingo/midas/R$id;->child_date2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date2:Landroid/widget/TextView;

    .line 190
    sget v0, Lcom/vlingo/midas/R$id;->child_date3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date3:Landroid/widget/TextView;

    .line 191
    sget v0, Lcom/vlingo/midas/R$id;->child_date4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date4:Landroid/widget/TextView;

    .line 192
    sget v0, Lcom/vlingo/midas/R$id;->child_date5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date5:Landroid/widget/TextView;

    .line 193
    sget v0, Lcom/vlingo/midas/R$id;->child_date6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date6:Landroid/widget/TextView;

    .line 195
    sget v0, Lcom/vlingo/midas/R$id;->child_image1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage1:Landroid/widget/ImageView;

    .line 196
    sget v0, Lcom/vlingo/midas/R$id;->child_image2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage2:Landroid/widget/ImageView;

    .line 197
    sget v0, Lcom/vlingo/midas/R$id;->child_image3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage3:Landroid/widget/ImageView;

    .line 198
    sget v0, Lcom/vlingo/midas/R$id;->child_image4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage4:Landroid/widget/ImageView;

    .line 199
    sget v0, Lcom/vlingo/midas/R$id;->child_image5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage5:Landroid/widget/ImageView;

    .line 200
    sget v0, Lcom/vlingo/midas/R$id;->child_image6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImage6:Landroid/widget/ImageView;

    .line 202
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature1:Landroid/widget/TextView;

    .line 203
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature2:Landroid/widget/TextView;

    .line 204
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature3:Landroid/widget/TextView;

    .line 205
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature4:Landroid/widget/TextView;

    .line 206
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature5:Landroid/widget/TextView;

    .line 207
    sget v0, Lcom/vlingo/midas/R$id;->child_min_temp6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperature6:Landroid/widget/TextView;

    .line 209
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature1:Landroid/widget/TextView;

    .line 210
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp2:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature2:Landroid/widget/TextView;

    .line 211
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp3:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature3:Landroid/widget/TextView;

    .line 212
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp4:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature4:Landroid/widget/TextView;

    .line 213
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp5:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature5:Landroid/widget/TextView;

    .line 214
    sget v0, Lcom/vlingo/midas/R$id;->child_max_temp6:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperature6:Landroid/widget/TextView;

    .line 217
    sget v0, Lcom/vlingo/midas/R$id;->citi_details:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->citiDetail:Landroid/widget/LinearLayout;

    .line 218
    sget v0, Lcom/vlingo/midas/R$id;->citi_name:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->citi_name:Landroid/widget/TextView;

    .line 219
    sget v0, Lcom/vlingo/midas/R$id;->imageView_singledayweather:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherImageOneDay1:Landroid/widget/ImageView;

    .line 220
    sget v0, Lcom/vlingo/midas/R$id;->date_view:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->date_value:Landroid/widget/TextView;

    .line 221
    sget v0, Lcom/vlingo/midas/R$id;->climate_name:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->climate_name:Landroid/widget/TextView;

    .line 222
    sget v0, Lcom/vlingo/midas/R$id;->min_temp:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->minTemperatureOneDay1:Landroid/widget/TextView;

    .line 223
    sget v0, Lcom/vlingo/midas/R$id;->max_temp:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->maxTemperatureOneDay1:Landroid/widget/TextView;

    .line 224
    sget v0, Lcom/vlingo/midas/R$id;->present_temp:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    .line 225
    sget v0, Lcom/vlingo/midas/R$id;->unit_meter_F:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mUnit_meter_F:Landroid/widget/ImageView;

    .line 226
    sget v0, Lcom/vlingo/midas/R$id;->unit_meter_C:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mUnit_meter_C:Landroid/widget/ImageView;

    .line 227
    sget v0, Lcom/vlingo/midas/R$id;->divider_small:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->divider:Landroid/widget/TextView;

    .line 228
    sget v0, Lcom/vlingo/midas/R$id;->widget_citi:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherBackgroundImage:Landroid/widget/FrameLayout;

    .line 229
    sget v0, Lcom/vlingo/midas/R$id;->widget_citi:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherBackgroundImageNew:Landroid/widget/FrameLayout;

    .line 230
    sget v0, Lcom/vlingo/midas/R$id;->widget_citi_LinearLayout:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->widgetCitiLinear:Landroid/widget/LinearLayout;

    .line 232
    sget v0, Lcom/vlingo/midas/R$id;->child_accuweather:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    .line 233
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->accuWeather:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    return-void
.end method

.method public onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 239
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 240
    return-void
.end method

.method public onResponseReceived()V
    .locals 21

    .prologue
    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    if-nez v18, :cond_0

    .line 256
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 257
    .local v10, "intent":Landroid/content/Intent;
    const-string/jumbo v18, "com.vlingo.core.internal.dialogmanager.NoData"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v10, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 364
    .end local v10    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 261
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->city:Ljava/lang/String;

    .line 262
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v4

    .line 263
    .local v4, "currentLocale":Ljava/util/Locale;
    const/4 v5, 0x0

    .line 264
    .local v5, "currentWeatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    const/4 v2, 0x0

    .line 265
    .local v2, "currentDate":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getDate()Ljava/lang/String;

    move-result-object v3

    .line 266
    .local v3, "currentDateValue":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isToday()Z

    move-result v18

    if-nez v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isDatePlusSeven()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 268
    :cond_1
    new-instance v2, Ljava/util/Date;

    .end local v2    # "currentDate":Ljava/util/Date;
    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 269
    .restart local v2    # "currentDate":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "currentWeatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    check-cast v5, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    .line 270
    .restart local v5    # "currentWeatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->weatherPhenomenon:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->climateName:Ljava/lang/String;

    .line 272
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->temperature:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentTemp:Ljava/lang/String;

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->weatherCode:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentWeatherCode:Ljava/lang/String;

    .line 277
    :goto_1
    iget-object v0, v5, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMax:Ljava/lang/String;

    .line 278
    iget-object v0, v5, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMin:Ljava/lang/String;

    .line 302
    :cond_2
    :goto_2
    invoke-static {v2, v4}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetDate(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentDateString:Ljava/lang/String;

    .line 314
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    sput-object v18, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    .line 317
    const/16 v16, 0x0

    .line 318
    .local v16, "weatherElementSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v16

    .line 320
    :cond_3
    const/4 v9, 0x1

    .local v9, "index":I
    :goto_3
    move/from16 v0, v16

    if-ge v9, v0, :cond_b

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    .line 322
    .local v17, "weatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    if-eqz v17, :cond_5

    .line 323
    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/vlingo/core/internal/weather/china/WeatherItem;->date:Ljava/util/Date;

    .line 324
    .local v6, "date":Ljava/util/Date;
    new-instance v12, Ljava/text/SimpleDateFormat;

    const-string/jumbo v18, "EEE"

    move-object/from16 v0, v18

    invoke-direct {v12, v0, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 326
    .local v12, "sfd":Ljava/text/SimpleDateFormat;
    invoke-virtual {v12, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 327
    .local v8, "dayWeek":Ljava/lang/String;
    new-instance v12, Ljava/text/SimpleDateFormat;

    .end local v12    # "sfd":Ljava/text/SimpleDateFormat;
    const-string/jumbo v18, "MM/dd"

    sget-object v19, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v12, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 328
    .restart local v12    # "sfd":Ljava/text/SimpleDateFormat;
    invoke-virtual {v12, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 330
    .local v7, "dateString":Ljava/lang/String;
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v15, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    .line 331
    .local v15, "weatherCode":Ljava/lang/String;
    invoke-static {v15}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 332
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v15, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    .line 333
    invoke-static {v15}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 337
    const-string/jumbo v15, "99"

    .line 344
    :cond_4
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v13, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    .line 345
    .local v13, "tempMax":Ljava/lang/String;
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v14, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    .line 349
    .local v14, "tempMin":Ljava/lang/String;
    new-instance v18, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    const/16 v19, 0x0

    invoke-direct/range {v18 .. v19}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;-><init>(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$1;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    .line 350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->setDay(Ljava/lang/String;)V
    invoke-static {v0, v8}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$100(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;Ljava/lang/String;)V

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->setDate(Ljava/lang/String;)V
    invoke-static {v0, v7}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$200(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;Ljava/lang/String;)V

    .line 352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->setMinTemp(Ljava/lang/String;)V
    invoke-static {v0, v14}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$300(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;Ljava/lang/String;)V

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->setMaxTemp(Ljava/lang/String;)V
    invoke-static {v0, v13}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$400(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;Ljava/lang/String;)V

    .line 354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->setweatherCode(Ljava/lang/String;)V
    invoke-static {v0, v15}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->access$500(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;Ljava/lang/String;)V

    .line 355
    sget-object v18, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastArrayList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->forecastingWeekly:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    .end local v6    # "date":Ljava/util/Date;
    .end local v7    # "dateString":Ljava/lang/String;
    .end local v8    # "dayWeek":Ljava/lang/String;
    .end local v12    # "sfd":Ljava/text/SimpleDateFormat;
    .end local v13    # "tempMax":Ljava/lang/String;
    .end local v14    # "tempMin":Ljava/lang/String;
    .end local v15    # "weatherCode":Ljava/lang/String;
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_3

    .line 275
    .end local v9    # "index":I
    .end local v16    # "weatherElementSize":I
    .end local v17    # "weatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->setPhenomenonWeatherCode(Lcom/vlingo/core/internal/weather/china/WeatherItem;)V

    goto/16 :goto_1

    .line 281
    :cond_7
    :try_start_0
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string/jumbo v18, "yyyy-MM-dd"

    sget-object v19, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 283
    .local v11, "sdfDate":Ljava/text/SimpleDateFormat;
    invoke-virtual {v11, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 286
    .end local v11    # "sdfDate":Ljava/text/SimpleDateFormat;
    :goto_4
    const/16 v16, 0x0

    .line 287
    .restart local v16    # "weatherElementSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    if-eqz v18, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    move-object/from16 v18, v0

    if-eqz v18, :cond_8

    .line 288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v16

    .line 289
    :cond_8
    const/4 v9, 0x1

    .restart local v9    # "index":I
    :goto_5
    move/from16 v0, v16

    if-ge v9, v0, :cond_a

    .line 290
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/util/Date;->getDate()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherItem;->date:Ljava/util/Date;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/Date;->getDate()I

    move-result v18

    move/from16 v0, v19

    move/from16 v1, v18

    if-ne v0, v1, :cond_9

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "currentWeatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    check-cast v5, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    .line 289
    .restart local v5    # "currentWeatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    :cond_9
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 296
    :cond_a
    if-eqz v5, :cond_2

    .line 297
    iget-object v0, v5, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMax:Ljava/lang/String;

    .line 298
    iget-object v0, v5, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->currentMin:Ljava/lang/String;

    .line 299
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->setPhenomenonWeatherCode(Lcom/vlingo/core/internal/weather/china/WeatherItem;)V

    goto/16 :goto_2

    .line 361
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const-string/jumbo v19, "com.vlingo.midas"

    const-string/jumbo v20, "WEAT"

    invoke-static/range {v18 .. v20}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$CMAWeatherSevenDayWidgetHandler;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$CMAWeatherSevenDayWidgetHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 284
    .end local v9    # "index":I
    .end local v16    # "weatherElementSize":I
    :catch_0
    move-exception v18

    goto/16 :goto_4
.end method
