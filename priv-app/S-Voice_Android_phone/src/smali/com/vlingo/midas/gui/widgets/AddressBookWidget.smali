.class public Lcom/vlingo/midas/gui/widgets/AddressBookWidget;
.super Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;
.source "AddressBookWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/AddressBookWidget$1;,
        Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactMatch;",
        ">;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field public static contactslist:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contactList:Landroid/widget/ListView;

.field private displayContactsLimited:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->contactslist:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->displayContactsLimited:Ljava/util/List;

    .line 62
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->mContext:Landroid/content/Context;

    .line 63
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/AddressBookWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/AddressBookWidget;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    return-object v0
.end method


# virtual methods
.method public getRoundedShape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v8, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    .line 320
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v9

    if-nez v9, :cond_0

    .line 343
    .end local p1    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object p1

    .line 322
    .restart local p1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_1
    move-object p1, v8

    .line 323
    goto :goto_0

    .line 325
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$dimen;->contact_border_width:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v0, v9

    .line 326
    .local v0, "borderwidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    mul-int/lit8 v10, v0, 0x2

    add-int v7, v9, v10

    .line 327
    .local v7, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    mul-int/lit8 v10, v0, 0x2

    add-int v3, v9, v10

    .line 329
    .local v3, "height":I
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v3, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 330
    .local v2, "canvasBitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/BitmapShader;

    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v10, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v6, p1, v9, v10}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 331
    .local v6, "shader":Landroid/graphics/BitmapShader;
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 332
    .local v4, "paint":Landroid/graphics/Paint;
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 333
    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 335
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 336
    .local v1, "canvas":Landroid/graphics/Canvas;
    if-le v7, v3, :cond_3

    int-to-float v9, v3

    div-float v5, v9, v11

    .line 337
    .local v5, "radius":F
    :goto_1
    div-int/lit8 v9, v7, 0x2

    int-to-float v9, v9

    div-int/lit8 v10, v3, 0x2

    int-to-float v10, v10

    invoke-virtual {v1, v9, v10, v5, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 338
    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 339
    sget-object v8, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 340
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$color;->contact_border_color:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 341
    int-to-float v8, v0

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 342
    div-int/lit8 v8, v7, 0x2

    int-to-float v8, v8

    div-int/lit8 v9, v3, 0x2

    int-to-float v9, v9

    int-to-float v10, v0

    sub-float v10, v5, v10

    invoke-virtual {v1, v8, v9, v10, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    move-object p1, v2

    .line 343
    goto :goto_0

    .line 336
    .end local v5    # "radius":F
    :cond_3
    int-to-float v9, v7

    div-float v5, v9, v11

    goto :goto_1
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 47
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 17
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "displayContacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    invoke-super/range {p0 .. p4}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 83
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 84
    const/4 v2, 0x0

    .line 85
    .local v2, "adapter":Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;
    const/4 v5, 0x0

    .line 91
    .local v5, "count":I
    if-eqz p1, :cond_3

    .line 92
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v11, "sortedContactsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v14

    invoke-direct {v3, v14}, Ljava/util/ArrayList;-><init>(I)V

    .line 95
    .local v3, "avatars":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 96
    .local v4, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    sget-object v14, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->contactslist:Ljava/util/HashMap;

    iget-wide v15, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "count":I
    .local v6, "count":I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v14, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v15, v4, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 98
    .local v13, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    invoke-static {v14, v13}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v9

    .line 99
    .local v9, "is":Ljava/io/InputStream;
    invoke-static {v9}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 100
    .local v10, "photo":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_0

    .line 101
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getRoundedShape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 103
    :cond_0
    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v5, v6

    .line 104
    .end local v6    # "count":I
    .restart local v5    # "count":I
    goto :goto_0

    .line 110
    .end local v4    # "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    .end local v9    # "is":Ljava/io/InputStream;
    .end local v10    # "photo":Landroid/graphics/Bitmap;
    .end local v13    # "uri":Landroid/net/Uri;
    :cond_1
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 111
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 112
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->clear()V

    .line 113
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 115
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x6

    if-le v14, v15, :cond_6

    .line 116
    new-instance v14, Ljava/util/ArrayList;

    const/4 v15, 0x6

    invoke-direct {v14, v15}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->displayContactsLimited:Ljava/util/List;

    .line 118
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/4 v14, 0x6

    if-ge v7, v14, :cond_2

    .line 119
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->displayContactsLimited:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 121
    :cond_2
    new-instance v2, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;

    .end local v2    # "adapter":Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getContext()Landroid/content/Context;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->displayContactsLimited:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v14, v15, v3}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/AddressBookWidget;Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    .line 128
    .end local v3    # "avatars":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .end local v7    # "i":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v11    # "sortedContactsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .restart local v2    # "adapter":Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;
    :cond_3
    :goto_2
    if-eqz p1, :cond_4

    .line 129
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->contactList:Landroid/widget/ListView;

    invoke-virtual {v14, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 130
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->contactList:Landroid/widget/ListView;

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 131
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->contactList:Landroid/widget/ListView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 132
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->notifyDataSetChanged()V

    .line 135
    :cond_4
    new-instance v12, Landroid/content/Intent;

    const-string/jumbo v14, "com.vlingo.core.internal.dialogmanager.DataTransfered"

    invoke-direct {v12, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 138
    .local v12, "transferIntent":Landroid/content/Intent;
    if-eqz p1, :cond_5

    .line 139
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v14

    invoke-static {v14}, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->getDisplayCount(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, p4

    invoke-interface {v0, v12, v14}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 140
    :cond_5
    return-void

    .line 124
    .end local v12    # "transferIntent":Landroid/content/Intent;
    .restart local v3    # "avatars":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v11    # "sortedContactsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    :cond_6
    new-instance v2, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;

    .end local v2    # "adapter":Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getContext()Landroid/content/Context;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v2, v0, v14, v1, v3}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/AddressBookWidget;Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    .restart local v2    # "adapter":Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;
    goto :goto_2
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onFinishInflate()V

    .line 68
    sget v0, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->contactList:Landroid/widget/ListView;

    .line 69
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 300
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 301
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.ContactChoice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 302
    const-string/jumbo v1, "choice"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 304
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 305
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->contactList:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 76
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onMeasure(II)V

    .line 77
    return-void
.end method
