.class Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;
.super Landroid/os/Handler;
.source "ControlFragmentEWYSHelp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ControlFragmentHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    .prologue
    .line 152
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 153
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 154
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 157
    iget-object v1, p0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    .line 158
    .local v0, "o":Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
    if-eqz v0, :cond_0

    .line 159
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 161
    :pswitch_0
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->isPaused()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 159
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
