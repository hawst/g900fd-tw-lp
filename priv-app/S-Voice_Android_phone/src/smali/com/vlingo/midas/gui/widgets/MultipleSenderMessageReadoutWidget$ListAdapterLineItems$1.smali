.class Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;
.super Ljava/lang/Object;
.source "MultipleSenderMessageReadoutWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;

.field final synthetic val$c:I

.field final synthetic val$position:I

.field final synthetic val$size:I

.field final synthetic val$smsMMSAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;IILcom/vlingo/core/internal/messages/SMSMMSAlert;I)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->this$1:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;

    iput p2, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->val$position:I

    iput p3, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->val$size:I

    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->val$smsMMSAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    iput p5, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->val$c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 232
    iget v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->val$position:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->val$position:I

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->val$size:I

    if-lt v1, v2, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 235
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    const-string/jumbo v1, "id"

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->val$position:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 237
    const-string/jumbo v1, "message_type"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->val$smsMMSAlert:Lcom/vlingo/core/internal/messages/SMSMMSAlert;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/messages/SMSMMSAlert;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    const-string/jumbo v1, "from_read_messages"

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    const-string/jumbo v1, "item_count"

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->val$c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 240
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems$1;->this$1:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    goto :goto_0
.end method
