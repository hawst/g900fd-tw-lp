.class Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;
.super Ljava/lang/Object;
.source "LocalSearchWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->getDetailLocalSearchView()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)V
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 378
    new-instance v9, Landroid/content/Intent;

    const-string/jumbo v0, "com.vlingo.core.internal.dialogmanager.AcceptedText"

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 379
    .local v9, "in":Landroid/content/Intent;
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$500(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v0

    new-instance v1, Lcom/vlingo/core/internal/recognition/acceptedtext/LocalSearchAcceptedText;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$400(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCurrentSearchQuery()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;->NAV:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/vlingo/core/internal/recognition/acceptedtext/LocalSearchAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v9, v1}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 381
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getFullAddress()Ljava/lang/String;

    move-result-object v6

    .line 382
    .local v6, "address":Ljava/lang/String;
    const/4 v10, 0x0

    .line 383
    .local v10, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 384
    invoke-static {}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->isENaviEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getLatitude()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    float-to-double v0, v0

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getLongitude()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    float-to-double v2, v2

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v4}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getFullAddress()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ENaviNavHandler;->getNaviagateIntent(DDLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 399
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 400
    return-void

    .line 387
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasBaiduMaps()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getLatitude()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$2;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getLongitude()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/midas/dialogmanager/vvs/handlers/clientdriven/ChinaBaiduMapHandler;->getNaviagateIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    goto :goto_0

    .line 390
    :cond_1
    new-instance v10, Landroid/content/Intent;

    .end local v10    # "intent":Landroid/content/Intent;
    const-string/jumbo v0, "com.autonavi.xmgd.action.NAVIGATE"

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 391
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string/jumbo v0, "target"

    const-string/jumbo v1, "\n"

    const-string/jumbo v2, ""

    invoke-virtual {v6, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 394
    :cond_2
    const-string/jumbo v0, " "

    const-string/jumbo v1, "+"

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 395
    .local v8, "encodedAddr":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "google.navigation:q="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 396
    .local v7, "arg":Ljava/lang/String;
    new-instance v10, Landroid/content/Intent;

    .end local v10    # "intent":Landroid/content/Intent;
    const-string/jumbo v0, "android.intent.action.VIEW"

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 397
    .restart local v10    # "intent":Landroid/content/Intent;
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method
