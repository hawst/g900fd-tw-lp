.class Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;
.super Ljava/lang/Object;
.source "CMAWeatherSevenDayWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ForecastingWeekly"
.end annotation


# instance fields
.field private date:Ljava/lang/String;

.field private day:Ljava/lang/String;

.field private maxTemp:Ljava/lang/String;

.field private minTemp:Ljava/lang/String;

.field private weatherCode:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$1;

    .prologue
    .line 638
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 638
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->setDay(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    .prologue
    .line 638
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getWeatherCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    .prologue
    .line 638
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMinTemp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    .prologue
    .line 638
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getMaxTemp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 638
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->setDate(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 638
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->setMinTemp(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 638
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->setMaxTemp(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 638
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->setweatherCode(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;

    .prologue
    .line 638
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->getDay()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->day:Ljava/lang/String;

    return-object v0
.end method

.method private getMaxTemp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 679
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->maxTemp:Ljava/lang/String;

    return-object v0
.end method

.method private getMinTemp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->minTemp:Ljava/lang/String;

    return-object v0
.end method

.method private getWeatherCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->weatherCode:Ljava/lang/String;

    return-object v0
.end method

.method private setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 651
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->date:Ljava/lang/String;

    .line 652
    return-void
.end method

.method private setDay(Ljava/lang/String;)V
    .locals 0
    .param p1, "day"    # Ljava/lang/String;

    .prologue
    .line 647
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->day:Ljava/lang/String;

    .line 648
    return-void
.end method

.method private setMaxTemp(Ljava/lang/String;)V
    .locals 0
    .param p1, "maxTemp"    # Ljava/lang/String;

    .prologue
    .line 659
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->maxTemp:Ljava/lang/String;

    .line 660
    return-void
.end method

.method private setMinTemp(Ljava/lang/String;)V
    .locals 0
    .param p1, "minTemp"    # Ljava/lang/String;

    .prologue
    .line 663
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->minTemp:Ljava/lang/String;

    .line 664
    return-void
.end method

.method private setweatherCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "weatherCode"    # Ljava/lang/String;

    .prologue
    .line 655
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->weatherCode:Ljava/lang/String;

    .line 656
    return-void
.end method


# virtual methods
.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$ForecastingWeekly;->date:Ljava/lang/String;

    return-object v0
.end method
