.class Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;
.super Ljava/util/TimerTask;
.source "DialogFragmentEWYSHelp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResumeControlPollTask"
.end annotation


# instance fields
.field private mTimesWaited:I

.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V
    .locals 1

    .prologue
    .line 1557
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 1558
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->mTimesWaited:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$1;

    .prologue
    .line 1557
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1568
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    iget-object v2, v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    monitor-enter v2

    .line 1569
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .line 1570
    .local v0, "activity":Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    if-nez v1, :cond_1

    .line 1571
    :cond_0
    monitor-exit v2

    .line 1585
    :goto_0
    return-void

    .line 1574
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->mTimesWaited:I

    const/4 v3, 0x3

    if-le v1, v3, :cond_4

    .line 1576
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$900(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1577
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$900(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 1578
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    const/4 v3, 0x0

    # setter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1, v3}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$902(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .line 1580
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$1000(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 1584
    :goto_1
    monitor-exit v2

    goto :goto_0

    .end local v0    # "activity":Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1582
    .restart local v0    # "activity":Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    :cond_4
    :try_start_1
    iget v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->mTimesWaited:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$ResumeControlPollTask;->mTimesWaited:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
