.class public Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;
.super Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;
.source "LandRegularControlFragmentEWYSHelp.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp$TabletType;
    }
.end annotation


# static fields
.field public static LOG_TAG:Ljava/lang/String;


# instance fields
.field public DBGSC:Z

.field private currentTablet:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp$TabletType;

.field invalidToastListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string/jumbo v0, "LandRegularControlFragmentEWYSHelp"

    sput-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->DBGSC:Z

    .line 33
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp$TabletType;->OTHERS:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->currentTablet:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp$TabletType;

    .line 35
    new-instance v0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp$1;-><init>(Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->invalidToastListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method getThninkingBtnOver()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method getThninkingMicAnimationList()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x12

    .line 51
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->DBGSC:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "onActivityCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :cond_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->onActivityCreated(Landroid/os/Bundle;)V

    .line 57
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_2

    .line 58
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->currentTablet:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp$TabletType;

    .line 61
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->control_full_container_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->fullContainer:Landroid/widget/RelativeLayout;

    .line 63
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_kitkat_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/MicViewKK;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->reco_thinkingBtn:Landroid/widget/ImageView;

    .line 65
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_idle_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->invalidToastListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->fullContainer:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->invalidToastListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v0, v2, :cond_3

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v0, v1, :cond_4

    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v0, v2, :cond_4

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_4

    .line 75
    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->setmActivityCreated(Z)V

    .line 76
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->setNormalPanel()V

    .line 77
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    sget v0, Lcom/vlingo/midas/R$layout;->land_control_view_ewys_help:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
