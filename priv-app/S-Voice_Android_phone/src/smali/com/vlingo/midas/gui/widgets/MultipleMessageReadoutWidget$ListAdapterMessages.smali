.class public Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;
.super Landroid/widget/BaseAdapter;
.source "MultipleMessageReadoutWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ListAdapterMessages"
.end annotation


# instance fields
.field DEFAULT_COLOR:[I

.field private DEFAULT_IMG:[I

.field private img_cnt:I

.field private final list:Ljava/util/List;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;Landroid/content/Context;)V
    .locals 7
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 238
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 222
    iput v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->img_cnt:I

    .line 223
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_01:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_02:I

    aput v1, v0, v3

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_03:I

    aput v1, v0, v4

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_04:I

    aput v1, v0, v5

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_05:I

    aput v1, v0, v6

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->DEFAULT_IMG:[I

    .line 231
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_1_bg_color:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_2_bg_color:I

    aput v1, v0, v3

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_3_bg_color:I

    aput v1, v0, v4

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_4_bg_color:I

    aput v1, v0, v5

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_5_bg_color:I

    aput v1, v0, v6

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->DEFAULT_COLOR:[I

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->list:Ljava/util/List;

    .line 240
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 7
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "messages"    # Ljava/util/List;

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 242
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 222
    iput v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->img_cnt:I

    .line 223
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_01:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_02:I

    aput v1, v0, v3

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_03:I

    aput v1, v0, v4

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_04:I

    aput v1, v0, v5

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_05:I

    aput v1, v0, v6

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->DEFAULT_IMG:[I

    .line 231
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_1_bg_color:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_2_bg_color:I

    aput v1, v0, v3

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_3_bg_color:I

    aput v1, v0, v4

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_4_bg_color:I

    aput v1, v0, v5

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_5_bg_color:I

    aput v1, v0, v6

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->DEFAULT_COLOR:[I

    .line 243
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->list:Ljava/util/List;

    .line 244
    return-void
.end method

.method private getDisplayMessage(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Z)Ljava/lang/String;
    .locals 6
    .param p1, "message"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .param p2, "showMessageBody"    # Z

    .prologue
    const/16 v5, 0x1e

    const/16 v4, 0x19

    const/4 v3, 0x0

    .line 264
    if-eqz p2, :cond_2

    .line 265
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 267
    .local v0, "msg":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 268
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_0

    .line 269
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 279
    :cond_0
    :goto_0
    return-object v0

    .line 272
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v5, :cond_0

    .line 273
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 277
    .end local v0    # "msg":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->core_car_safereader_hidden_message_body:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "msg":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 248
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 249
    .local v0, "toReturn":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->getLimitedCount(I)I

    move-result v1

    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 254
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 259
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 25
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->list:Ljava/util/List;

    move-object/from16 v19, v0

    .line 286
    .local v19, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageType;>;"
    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .line 289
    .local v18, "message":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    if-nez p2, :cond_4

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->getContext()Landroid/content/Context;

    move-result-object v22

    sget v23, Lcom/vlingo/midas/R$layout;->item_message_details:I

    const/16 v24, 0x0

    invoke-static/range {v22 .. v24}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 291
    new-instance v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;

    const/16 v22, 0x0

    move-object/from16 v0, v22

    invoke-direct {v14, v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$1;)V

    .line 292
    .local v14, "holder":Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;
    sget v22, Lcom/vlingo/midas/R$id;->from:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    iput-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->from:Landroid/widget/TextView;

    .line 293
    sget v22, Lcom/vlingo/midas/R$id;->body:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    iput-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->body:Landroid/widget/TextView;

    .line 294
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v22

    if-eqz v22, :cond_1

    .line 295
    sget v22, Lcom/vlingo/midas/R$id;->relativelayout_1:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/RelativeLayout;

    .line 296
    .local v17, "layout1":Landroid/widget/RelativeLayout;
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 297
    sget v22, Lcom/vlingo/midas/R$id;->date_msg:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    iput-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->date:Landroid/widget/TextView;

    .line 298
    iget-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->date:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    sget v22, Lcom/vlingo/midas/R$id;->date_msg_src:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ImageView;

    move-object/from16 v0, v22

    iput-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->image_msg:Landroid/widget/ImageView;

    .line 301
    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getType()Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getType()Ljava/lang/String;

    move-result-object v22

    const-string/jumbo v23, "MMS"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 302
    iget-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->image_msg:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 304
    :cond_0
    sget v22, Lcom/vlingo/midas/R$id;->message_row_title_line:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    move-object/from16 v0, v22

    iput-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->divider:Landroid/view/View;

    .line 305
    iget-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->divider:Landroid/view/View;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    .line 306
    sget v22, Lcom/vlingo/midas/R$id;->message_image_contact:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ImageView;

    move-object/from16 v0, v22

    iput-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->contact_img:Landroid/widget/ImageView;

    .line 307
    iget-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->contact_img:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 309
    .end local v17    # "layout1":Landroid/widget/RelativeLayout;
    :cond_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 316
    :goto_0
    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getAddress()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_5

    .line 317
    iget-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->from:Landroid/widget/TextView;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " ("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getAddress()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    :goto_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v22

    if-eqz v22, :cond_8

    .line 324
    const/16 v20, 0x0

    .line 325
    .local v20, "photo":Landroid/graphics/Bitmap;
    sget v22, Lcom/vlingo/midas/R$id;->message_image_contact:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 326
    .local v7, "contact_image":Landroid/widget/ImageView;
    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v22

    const/16 v23, 0x0

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    invoke-virtual/range {v22 .. v22}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v22

    const-string/jumbo v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_2

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v22

    const/16 v23, 0x0

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    invoke-virtual/range {v22 .. v22}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_7

    .line 327
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    move-object/from16 v23, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v22

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    invoke-virtual/range {v22 .. v22}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->context:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->fetchContactIdFromPhoneNumber(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v6

    .line 329
    .local v6, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_6

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;

    .line 330
    .local v5, "contact":Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;
    iget-object v0, v5, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;->contactId:Ljava/lang/String;

    move-object/from16 v22, v0

    if-eqz v22, :cond_3

    iget-object v0, v5, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;->contactName:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string/jumbo v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_3

    .line 331
    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v22

    const/16 v23, 0x0

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    invoke-virtual/range {v22 .. v22}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getDisplayName()Ljava/lang/String;

    move-result-object v22

    iget-object v0, v5, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;->contactName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 332
    sget-object v22, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-object v0, v5, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;->contactId:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v22 .. v23}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v21

    .line 333
    .local v21, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->context:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v16

    .line 334
    .local v16, "is":Ljava/io/InputStream;
    if-nez v20, :cond_6

    .line 335
    invoke-static/range {v16 .. v16}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v20

    goto :goto_2

    .line 311
    .end local v5    # "contact":Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;
    .end local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    .end local v7    # "contact_image":Landroid/widget/ImageView;
    .end local v14    # "holder":Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v16    # "is":Ljava/io/InputStream;
    .end local v20    # "photo":Landroid/graphics/Bitmap;
    .end local v21    # "uri":Landroid/net/Uri;
    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;

    .restart local v14    # "holder":Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;
    goto/16 :goto_0

    .line 319
    :cond_5
    iget-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->from:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 342
    .restart local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    .restart local v7    # "contact_image":Landroid/widget/ImageView;
    .restart local v15    # "i$":Ljava/util/Iterator;
    .restart local v20    # "photo":Landroid/graphics/Bitmap;
    :cond_6
    if-eqz v20, :cond_a

    .line 343
    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 353
    :goto_3
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 355
    .end local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    .end local v15    # "i$":Ljava/util/Iterator;
    :cond_7
    new-instance v9, Ljava/util/Date;

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getTimeStamp()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-direct {v9, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 356
    .local v9, "date":Ljava/util/Date;
    new-instance v12, Ljava/text/SimpleDateFormat;

    const-string/jumbo v22, "yyyy-MM-dd HH:mm:ss"

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 358
    .local v12, "formatDate":Ljava/text/SimpleDateFormat;
    invoke-virtual {v12, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 360
    .local v4, "InputDate":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/core/internal/schedule/DateUtil;->isToday(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 361
    iget-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->date:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-static {v9}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    .end local v4    # "InputDate":Ljava/lang/String;
    .end local v7    # "contact_image":Landroid/widget/ImageView;
    .end local v9    # "date":Ljava/util/Date;
    .end local v12    # "formatDate":Ljava/text/SimpleDateFormat;
    .end local v20    # "photo":Landroid/graphics/Bitmap;
    :cond_8
    :goto_4
    sget v22, Lcom/vlingo/midas/R$id;->count:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 375
    .local v8, "count":Landroid/widget/TextView;
    if-eqz v8, :cond_9

    .line 376
    const/16 v22, 0x8

    move/from16 v0, v22

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 380
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->showMessageBody:Z

    move/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->getDisplayMessage(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Z)Ljava/lang/String;

    move-result-object v11

    .line 381
    .local v11, "displayMessage":Ljava/lang/String;
    iget-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->body:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    new-instance v22, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages$1;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages$1;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;ILcom/vlingo/core/internal/dialogmanager/types/MessageType;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 394
    if-nez p1, :cond_d

    .line 395
    sget v22, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 401
    :goto_5
    return-object p2

    .line 345
    .end local v8    # "count":Landroid/widget/TextView;
    .end local v11    # "displayMessage":Ljava/lang/String;
    .restart local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    .restart local v7    # "contact_image":Landroid/widget/ImageView;
    .restart local v15    # "i$":Ljava/util/Iterator;
    .restart local v20    # "photo":Landroid/graphics/Bitmap;
    :cond_a
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v22

    if-eqz v22, :cond_b

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->DEFAULT_COLOR:[I

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->img_cnt:I

    move/from16 v23, v0

    rem-int/lit8 v23, v23, 0x5

    aget v22, v22, v23

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 351
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->img_cnt:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->img_cnt:I

    goto/16 :goto_3

    .line 348
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->DEFAULT_IMG:[I

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->img_cnt:I

    move/from16 v24, v0

    rem-int/lit8 v24, v24, 0x5

    aget v23, v23, v24

    invoke-static/range {v22 .. v23}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 349
    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_6

    .line 364
    .end local v6    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    .end local v15    # "i$":Ljava/util/Iterator;
    .restart local v4    # "InputDate":Ljava/lang/String;
    .restart local v9    # "date":Ljava/util/Date;
    .restart local v12    # "formatDate":Ljava/text/SimpleDateFormat;
    :cond_c
    new-instance v10, Ljava/text/SimpleDateFormat;

    const-string/jumbo v22, "dd/MM"

    move-object/from16 v0, v22

    invoke-direct {v10, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 366
    .local v10, "dateMonthFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v10, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    .line 367
    .local v13, "formatted":Ljava/lang/String;
    iget-object v0, v14, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;->date:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 396
    .end local v4    # "InputDate":Ljava/lang/String;
    .end local v7    # "contact_image":Landroid/widget/ImageView;
    .end local v9    # "date":Ljava/util/Date;
    .end local v10    # "dateMonthFormat":Ljava/text/SimpleDateFormat;
    .end local v12    # "formatDate":Ljava/text/SimpleDateFormat;
    .end local v13    # "formatted":Ljava/lang/String;
    .end local v20    # "photo":Landroid/graphics/Bitmap;
    .restart local v8    # "count":Landroid/widget/TextView;
    .restart local v11    # "displayMessage":Ljava/lang/String;
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->getCount()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, p1

    move/from16 v1, v22

    if-lt v0, v1, :cond_e

    .line 397
    sget v22, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_5

    .line 399
    :cond_e
    sget v22, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_5
.end method
