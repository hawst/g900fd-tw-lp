.class Lcom/vlingo/midas/gui/widgets/WeatherWidget$WeatherWidgetHandler;
.super Landroid/os/Handler;
.source "WeatherWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/WeatherWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WeatherWidgetHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/widgets/WeatherWidget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/WeatherWidget;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/widgets/WeatherWidget;

    .prologue
    .line 332
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 333
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget$WeatherWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 334
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 337
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherWidget$WeatherWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/WeatherWidget;

    .line 338
    .local v0, "o":Lcom/vlingo/midas/gui/widgets/WeatherWidget;
    if-eqz v0, :cond_0

    .line 341
    # invokes: Lcom/vlingo/midas/gui/widgets/WeatherWidget;->setValuesForcastWeather()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/WeatherWidget;->access$000(Lcom/vlingo/midas/gui/widgets/WeatherWidget;)V

    .line 343
    :cond_0
    return-void
.end method
