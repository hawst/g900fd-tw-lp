.class Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DialogFragmentEWYSHelp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoveListBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V
    .locals 0

    .prologue
    .line 1588
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$1;

    .prologue
    .line 1588
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1591
    const/4 v0, 0x0

    .line 1592
    .local v0, "action":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 1593
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1595
    :cond_0
    const-string/jumbo v5, "LIST_MOVE_ACTION"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1596
    const-string/jumbo v5, "EXTRA_LIST_HEIGHT"

    const/4 v6, 0x0

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1597
    .local v1, "extra":I
    const/4 v2, 0x0

    .line 1599
    .local v2, "helpHeight":I
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$300(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    add-int/lit8 v3, v5, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 1600
    const/4 v4, 0x0

    .line 1601
    .local v4, "v1":Landroid/view/View;
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$300(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1602
    if-eqz v4, :cond_1

    .line 1603
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v2, v5

    .line 1604
    instance-of v5, v4, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-eqz v5, :cond_1

    .line 1605
    const/4 v3, 0x0

    .line 1606
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v2, v5

    .line 1599
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 1611
    .end local v4    # "v1":Landroid/view/View;
    :cond_2
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    iget-object v6, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$300(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v6

    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v7}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$200(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/ScrollView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ScrollView;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int/2addr v6, v1

    sub-int/2addr v6, v2

    # invokes: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doScrollAnim(I)V
    invoke-static {v5, v6}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$500(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;I)V

    .line 1623
    .end local v1    # "extra":I
    .end local v2    # "helpHeight":I
    .end local v3    # "i":I
    :cond_3
    :goto_1
    return-void

    .line 1614
    :cond_4
    const-string/jumbo v5, "com.vlingo.LANGUAGE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1616
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getHelpVisible()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1617
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->addHelpChoiceWidget()V

    goto :goto_1
.end method
