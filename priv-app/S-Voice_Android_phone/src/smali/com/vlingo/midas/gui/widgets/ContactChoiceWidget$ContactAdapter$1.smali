.class Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter$1;
.super Ljava/lang/Object;
.source "ContactChoiceWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;I)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;

    iput p2, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 252
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;->retire()V

    .line 254
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 255
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    const-string/jumbo v1, "choice"

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter$1;->val$position:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 257
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 258
    return-void
.end method
