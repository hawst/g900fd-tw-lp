.class Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;
.super Ljava/lang/Object;
.source "LocalSearchWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->onResponseReceived()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

.field final synthetic val$reviewListing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;Lcom/vlingo/core/internal/localsearch/LocalSearchListing;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->val$reviewListing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/16 v4, 0x8

    const/4 v6, 0x0

    .line 516
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 518
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->val$reviewListing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getReviews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 519
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->layoutReviews:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 520
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->textReviews:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 522
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->val$reviewListing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getReviews()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;

    .line 525
    .local v1, "review":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->layoutReviews:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v3, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v3, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->create(Landroid/content/Context;Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;)Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 528
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "review":Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->originalProvider:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$900(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 529
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v3, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getProvider()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->originalProvider:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$902(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;Ljava/lang/String;)Ljava/lang/String;

    .line 531
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->originalProvider:Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$900(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v2

    if-nez v2, :cond_2

    .line 532
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->providerView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v4, v4, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v4, v4, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->localsearch_powered_by:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v4, v4, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->originalProvider:Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$900(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 534
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->providerView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 538
    :goto_1
    return-void

    .line 536
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl$1;->this$2:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$WidgetResponseReceivedListenerImpl;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->providerView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
