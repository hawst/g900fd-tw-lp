.class public Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;
.super Landroid/widget/BaseAdapter;
.source "MemoChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MemoAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field private final memoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/memo/Memo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p2, "allMemoInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/memo/Memo;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 75
    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->memoList:Ljava/util/List;

    .line 77
    # getter for: Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->context:Landroid/content/Context;
    invoke-static {p1}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 78
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->memoList:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    .line 92
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->memoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 93
    .local v0, "naturalSize":I
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;

    invoke-virtual {v2, v0}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->getLimitedCount(I)I

    move-result v1

    .line 94
    .local v1, "toReturn":I
    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->memoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 87
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 99
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->memoList:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v14, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/memo/Memo;

    .line 103
    .local v7, "memo":Lcom/vlingo/core/internal/memo/Memo;
    if-nez p2, :cond_1

    .line 104
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v15, Lcom/vlingo/midas/R$layout;->item_memo_choice:I

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 106
    new-instance v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;)V

    .line 107
    .local v6, "holder":Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;
    sget v14, Lcom/vlingo/midas/R$id;->memo_content:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->content:Landroid/widget/TextView;

    .line 108
    sget v14, Lcom/vlingo/midas/R$id;->memo_date:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->date:Landroid/widget/TextView;

    .line 109
    sget v14, Lcom/vlingo/midas/R$id;->memo_time:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->time:Landroid/widget/TextView;

    .line 110
    sget v14, Lcom/vlingo/midas/R$id;->memo_divider:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    iput-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->divider:Landroid/view/View;

    .line 112
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 117
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 118
    sget v14, Lcom/vlingo/midas/R$drawable;->memo_list_selector_middle:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setBackgroundResource(I)V

    .line 119
    if-nez p1, :cond_2

    .line 136
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->context:Landroid/content/Context;
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;)Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v12

    .line 137
    .local v12, "timeFormat24State":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;

    invoke-virtual {v14}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string/jumbo v15, "date_format"

    invoke-static {v14, v15}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 138
    .local v11, "phoneDateFormat":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v2

    .line 139
    .local v2, "currentLocale":Ljava/util/Locale;
    invoke-virtual {v7}, Lcom/vlingo/core/internal/memo/Memo;->getDate()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 140
    .local v3, "date":J
    invoke-static {v3, v4, v11, v2}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatMemoDate(JLjava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 141
    .local v5, "dateString":Ljava/lang/String;
    invoke-static {v3, v4, v12, v2}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatMemoTime(JZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v13

    .line 142
    .local v13, "timeString":Ljava/lang/String;
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    const/4 v14, 0x0

    invoke-virtual {v7, v14}, Lcom/vlingo/core/internal/memo/Memo;->getMemoName(Z)Ljava/lang/String;

    move-result-object v8

    .line 145
    .local v8, "memoString":Ljava/lang/String;
    invoke-static {v8}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 146
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->content:Landroid/widget/TextView;

    sget v15, Lcom/vlingo/midas/R$string;->no_title:I

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(I)V

    .line 175
    :goto_2
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->time:Landroid/widget/TextView;

    invoke-virtual {v14, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    new-instance v14, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$1;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v14, v0, v1}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;I)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    return-object p2

    .line 114
    .end local v2    # "currentLocale":Ljava/util/Locale;
    .end local v3    # "date":J
    .end local v5    # "dateString":Ljava/lang/String;
    .end local v6    # "holder":Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;
    .end local v8    # "memoString":Ljava/lang/String;
    .end local v11    # "phoneDateFormat":Ljava/lang/String;
    .end local v12    # "timeFormat24State":Z
    .end local v13    # "timeString":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;

    .restart local v6    # "holder":Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;
    goto :goto_0

    .line 121
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->getCount()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    move/from16 v0, p1

    if-lt v0, v14, :cond_0

    .line 122
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->divider:Landroid/view/View;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 125
    :cond_3
    if-nez p1, :cond_4

    .line 126
    sget v14, Lcom/vlingo/midas/R$drawable;->memo_list_selector_top:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setBackgroundResource(I)V

    .line 127
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v15, 0x4

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 128
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->getCount()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    move/from16 v0, p1

    if-lt v0, v14, :cond_5

    .line 129
    sget v14, Lcom/vlingo/midas/R$drawable;->memo_list_selector_bottom:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setBackgroundResource(I)V

    .line 130
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 132
    :cond_5
    sget v14, Lcom/vlingo/midas/R$drawable;->memo_list_selector_middle:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 147
    .restart local v2    # "currentLocale":Ljava/util/Locale;
    .restart local v3    # "date":J
    .restart local v5    # "dateString":Ljava/lang/String;
    .restart local v8    # "memoString":Ljava/lang/String;
    .restart local v11    # "phoneDateFormat":Ljava/lang/String;
    .restart local v12    # "timeFormat24State":Z
    .restart local v13    # "timeString":Ljava/lang/String;
    :cond_6
    if-eqz v8, :cond_7

    const-string/jumbo v14, ".snb"

    invoke-virtual {v8, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 148
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->content:Landroid/widget/TextView;

    const/4 v15, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, -0x4

    move/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 150
    :cond_7
    if-eqz v8, :cond_9

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_9

    .line 157
    invoke-virtual {v7}, Lcom/vlingo/core/internal/memo/Memo;->getText()Ljava/lang/String;

    move-result-object v10

    .line 158
    .local v10, "memotext":Ljava/lang/String;
    invoke-static {v10}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 159
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->content:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->context:Landroid/content/Context;
    invoke-static {v15}, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget;)Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/vlingo/midas/R$string;->no_text:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 162
    :cond_8
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->content:Landroid/widget/TextView;

    invoke-virtual {v14, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 165
    .end local v10    # "memotext":Ljava/lang/String;
    :cond_9
    invoke-virtual {v7}, Lcom/vlingo/core/internal/memo/Memo;->getTitle()Ljava/lang/String;

    move-result-object v9

    .line 166
    .local v9, "memoTitle":Ljava/lang/String;
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 168
    invoke-virtual {v7}, Lcom/vlingo/core/internal/memo/Memo;->getContent()Ljava/lang/String;

    move-result-object v9

    .line 169
    invoke-static {v9}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 170
    invoke-virtual {v7}, Lcom/vlingo/core/internal/memo/Memo;->getText()Ljava/lang/String;

    move-result-object v9

    .line 172
    :cond_a
    iget-object v14, v6, Lcom/vlingo/midas/gui/widgets/MemoChoiceWidget$MemoAdapter$ViewHolder;->content:Landroid/widget/TextView;

    invoke-virtual {v14, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method
