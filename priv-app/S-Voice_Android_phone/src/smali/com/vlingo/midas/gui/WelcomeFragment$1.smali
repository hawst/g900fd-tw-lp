.class Lcom/vlingo/midas/gui/WelcomeFragment$1;
.super Ljava/lang/Object;
.source "WelcomeFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/WelcomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/WelcomeFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/WelcomeFragment;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vlingo/midas/gui/WelcomeFragment$1;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 63
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 58
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment$1;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    # getter for: Lcom/vlingo/midas/gui/WelcomeFragment;->mWelcomeTextLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WelcomeFragment;->access$100(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment$1;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    # getter for: Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/vlingo/midas/gui/WelcomeFragment;->access$000(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment$1;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    # getter for: Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WelcomeFragment;->access$200(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment$1;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    # getter for: Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/vlingo/midas/gui/WelcomeFragment;->access$000(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 51
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment$1;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    # getter for: Lcom/vlingo/midas/gui/WelcomeFragment;->mSvoiceTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WelcomeFragment;->access$300(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment$1;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    # getter for: Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/vlingo/midas/gui/WelcomeFragment;->access$000(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 52
    return-void
.end method
