.class public Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;
.super Landroid/widget/BaseAdapter;
.source "SocialNetworkChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ContactAdapter"
.end annotation


# instance fields
.field private final socialNetworkList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p3, "socialNetworkList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 72
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;->socialNetworkList:Ljava/util/List;

    .line 73
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;->socialNetworkList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;->socialNetworkList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 80
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x4

    .line 91
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;->socialNetworkList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 93
    .local v3, "socialService":Ljava/lang/String;
    if-nez p2, :cond_2

    .line 94
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->context:Landroid/content/Context;
    invoke-static {v4}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 95
    .local v1, "inflater":Landroid/view/LayoutInflater;
    sget v4, Lcom/vlingo/midas/R$layout;->item_social_type:I

    const/4 v5, 0x0

    invoke-virtual {v1, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 96
    new-instance v0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;

    invoke-direct {v0}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;-><init>()V

    .line 97
    .local v0, "holder":Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;
    sget v4, Lcom/vlingo/midas/R$id;->social_type:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;->type:Landroid/widget/TextView;

    .line 98
    sget v4, Lcom/vlingo/midas/R$id;->divider:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;->divider:Landroid/view/View;

    .line 99
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 105
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    invoke-static {v3}, Lcom/vlingo/midas/social/api/SocialNetworkType;->localizedSocialNetworkTypeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 107
    .local v2, "localizedSocialType":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 108
    iget-object v4, v0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;->type:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    :cond_0
    new-instance v4, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter$1;

    invoke-direct {v4, p0, p1}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;I)V

    invoke-virtual {p2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 123
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 124
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne p1, v4, :cond_1

    .line 125
    iget-object v4, v0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;->divider:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 138
    :cond_1
    :goto_1
    return-object p2

    .line 101
    .end local v0    # "holder":Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;
    .end local v2    # "localizedSocialType":Ljava/lang/String;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;

    .restart local v0    # "holder":Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;
    goto :goto_0

    .line 128
    .restart local v2    # "localizedSocialType":Ljava/lang/String;
    :cond_3
    if-nez p1, :cond_4

    .line 129
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 130
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne p1, v4, :cond_5

    .line 131
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 132
    iget-object v4, v0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;->divider:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 134
    :cond_5
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method
