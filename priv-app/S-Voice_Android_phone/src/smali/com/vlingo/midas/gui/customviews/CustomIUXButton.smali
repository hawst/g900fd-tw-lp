.class public Lcom/vlingo/midas/gui/customviews/CustomIUXButton;
.super Landroid/widget/LinearLayout;
.source "CustomIUXButton.java"


# instance fields
.field private mNextTxt:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    iput-object v7, p0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->mNextTxt:Landroid/widget/TextView;

    .line 20
    const-string/jumbo v5, "layout_inflater"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 21
    .local v4, "inflater":Landroid/view/LayoutInflater;
    sget v5, Lcom/vlingo/midas/R$layout;->tutorial_btn_next:I

    invoke-virtual {v4, v5, p0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 22
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Lcom/vlingo/midas/R$styleable;->CustomIUXButton:[I

    invoke-virtual {v5, p2, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 23
    .local v3, "customAttr":Landroid/content/res/TypedArray;
    invoke-virtual {v3, v8}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 24
    .local v2, "buttonText":Ljava/lang/String;
    invoke-virtual {v3, v9, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 25
    .local v0, "buttonColor":I
    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 27
    .local v1, "buttonRightDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v3, 0x0

    .line 28
    sget v5, Lcom/vlingo/midas/R$id;->next_btn_txt:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->mNextTxt:Landroid/widget/TextView;

    .line 29
    if-eqz v2, :cond_0

    .line 30
    iget-object v5, p0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->mNextTxt:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    :cond_0
    if-eqz v0, :cond_1

    .line 32
    iget-object v5, p0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->mNextTxt:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 33
    :cond_1
    if-eqz v1, :cond_2

    .line 34
    iget-object v5, p0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->mNextTxt:Landroid/widget/TextView;

    invoke-virtual {v5, v7, v7, v1, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 36
    :cond_2
    return-void
.end method


# virtual methods
.method public setButtonClicked()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->mNextTxt:Landroid/widget/TextView;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 44
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->setClickable(Z)V

    .line 45
    return-void
.end method

.method public setButtonText(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->mNextTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->mNextTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 51
    if-eqz p1, :cond_0

    .line 52
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->mNextTxt:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->mNextTxt:Landroid/widget/TextView;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method
