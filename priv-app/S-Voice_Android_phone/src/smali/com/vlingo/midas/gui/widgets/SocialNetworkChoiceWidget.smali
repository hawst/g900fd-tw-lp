.class public Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "SocialNetworkChoiceWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/String;",
        ">;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private serviceList:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->context:Landroid/content/Context;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 31
    check-cast p1, Ljava/util/ArrayList;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->initialize(Ljava/util/ArrayList;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/ArrayList;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 2
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "socialNetworkList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 60
    if-eqz p1, :cond_0

    .line 61
    new-instance v0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;Landroid/content/Context;Ljava/util/List;)V

    .line 62
    .local v0, "adapter":Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->serviceList:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->serviceList:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 64
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;->notifyDataSetChanged()V

    .line 66
    .end local v0    # "adapter":Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget$ContactAdapter;
    :cond_0
    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 49
    sget v0, Lcom/vlingo/midas/R$id;->service_list:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->serviceList:Landroid/widget/ListView;

    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->serviceList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 54
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->retire()V

    .line 150
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 151
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string/jumbo v1, "choice"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 154
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 155
    return-void
.end method

.method public onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->serviceList:Landroid/widget/ListView;

    sget v1, Lcom/vlingo/midas/R$dimen;->item_application_choice_height:I

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/SocialNetworkChoiceWidget;->measureListviewHeight(Landroid/widget/ListView;IZ)V

    .line 160
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 161
    return-void
.end method
