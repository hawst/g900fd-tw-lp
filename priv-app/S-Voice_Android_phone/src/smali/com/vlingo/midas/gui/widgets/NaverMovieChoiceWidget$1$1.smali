.class Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;
.super Ljava/lang/Object;
.source "NaverMovieChoiceWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;

.field final synthetic val$d:Landroid/graphics/drawable/Drawable;

.field final synthetic val$ind:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;ILandroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->this$1:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;

    iput p2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->val$ind:I

    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->val$d:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 133
    const/4 v0, 0x0

    .line 134
    .local v0, "abc":Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->this$1:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Landroid/widget/ListView;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->val$ind:I

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->val$d:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->this$1:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Landroid/widget/ListView;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->val$ind:I

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->this$1:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Landroid/widget/ListView;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->val$ind:I

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "abc":Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;
    check-cast v0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;

    .line 140
    .restart local v0    # "abc":Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;
    iget-object v1, v0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMoviePoster:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->val$d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 142
    iget-object v1, v0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMoviePoster:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 144
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->this$1:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Landroid/widget/ListView;

    move-result-object v1

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;->val$ind:I

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->text_Movie_Poster_Cover:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 149
    iget-object v1, v0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMoviePoster:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 152
    :cond_0
    return-void
.end method
