.class Lcom/vlingo/midas/gui/ConversationActivity$6;
.super Ljava/lang/Object;
.source "ConversationActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/ConversationActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ConversationActivity;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1964
    iput-object p1, p0, Lcom/vlingo/midas/gui/ConversationActivity$6;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iput-object p2, p0, Lcom/vlingo/midas/gui/ConversationActivity$6;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1967
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity$6;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->isKModel:Z
    invoke-static {v4}, Lcom/vlingo/midas/gui/ConversationActivity;->access$2100(Lcom/vlingo/midas/gui/ConversationActivity;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1970
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity$6;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->notificationManager:Lcom/vlingo/midas/notification/NotificationPopUpManager;
    invoke-static {v4}, Lcom/vlingo/midas/gui/ConversationActivity;->access$2200(Lcom/vlingo/midas/gui/ConversationActivity;)Lcom/vlingo/midas/notification/NotificationPopUpManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/notification/NotificationPopUpManager;->showNextNotification()V

    .line 2000
    :goto_0
    return-void

    .line 1976
    :cond_0
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity$6;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1977
    .local v1, "mDefaultSharedPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1978
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v4, "is_tutorial_completed"

    invoke-interface {v1, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 1979
    .local v3, "tutorialCompleted":Z
    if-nez v3, :cond_2

    .line 1980
    const-string/jumbo v4, "is_tutorial_completed"

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1981
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1985
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity$6;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1986
    .local v2, "mDefaultSharedPrefs1":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1987
    const-string/jumbo v4, "welcome from conversation"

    const/4 v5, 0x1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1988
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1989
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity$6;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/ConversationActivity;->isFinishing()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1990
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity$6;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v5, p0, Lcom/vlingo/midas/gui/ConversationActivity$6;->val$intent:Landroid/content/Intent;

    const/16 v6, 0xd

    invoke-virtual {v4, v5, v6}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1991
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/gui/ConversationActivity$6;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/ConversationActivity;->finish()V

    .line 1997
    .end local v2    # "mDefaultSharedPrefs1":Landroid/content/SharedPreferences;
    :cond_2
    const-string/jumbo v4, "is_tutorial_completed"

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1998
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
