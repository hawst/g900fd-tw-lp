.class public Lcom/vlingo/midas/gui/AnimationUtils;
.super Ljava/lang/Object;
.source "AnimationUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/AnimationUtils$AnimationListenerBase;,
        Lcom/vlingo/midas/gui/AnimationUtils$AnimationGroup;
    }
.end annotation


# static fields
.field public static final DURATION_DEFAULT:I = 0x12c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method

.method public static fadeInAnimation()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 84
    const/16 v0, 0x12c

    invoke-static {v0}, Lcom/vlingo/midas/gui/AnimationUtils;->fadeInAnimation(I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public static fadeInAnimation(I)Landroid/view/animation/Animation;
    .locals 3
    .param p0, "duration"    # I

    .prologue
    .line 92
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 93
    .local v0, "inFromLeft":Landroid/view/animation/Animation;
    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 94
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 95
    return-object v0
.end method

.method public static fadeOutAnimation()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 88
    const/16 v0, 0x12c

    invoke-static {v0}, Lcom/vlingo/midas/gui/AnimationUtils;->fadeOutAnimation(I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public static fadeOutAnimation(I)Landroid/view/animation/Animation;
    .locals 3
    .param p0, "duration"    # I

    .prologue
    .line 99
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 100
    .local v0, "inFromLeft":Landroid/view/animation/Animation;
    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 101
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 102
    return-object v0
.end method

.method public static inFromBottomAnimation()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 71
    const/16 v0, 0x12c

    invoke-static {v0}, Lcom/vlingo/midas/gui/AnimationUtils;->inFromBottomAnimation(I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public static inFromBottomAnimation(I)Landroid/view/animation/Animation;
    .locals 9
    .param p0, "duration"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 118
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v6, 0x3f800000    # 1.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 124
    .local v0, "inFromLeft":Landroid/view/animation/Animation;
    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 125
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 126
    return-object v0
.end method

.method public static inFromTopAnimation()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 67
    const/16 v0, 0x12c

    invoke-static {v0}, Lcom/vlingo/midas/gui/AnimationUtils;->inFromTopAnimation(I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public static inFromTopAnimation(I)Landroid/view/animation/Animation;
    .locals 9
    .param p0, "duration"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 106
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v6, -0x40800000    # -1.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 112
    .local v0, "inFromLeft":Landroid/view/animation/Animation;
    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 113
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 114
    return-object v0
.end method

.method public static outToBottomAnimation()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 79
    const/16 v0, 0x12c

    invoke-static {v0}, Lcom/vlingo/midas/gui/AnimationUtils;->outToBottomAnimation(I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public static outToBottomAnimation(I)Landroid/view/animation/Animation;
    .locals 9
    .param p0, "duration"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 142
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v8, 0x3f800000    # 1.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v6, v2

    move v7, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 148
    .local v0, "inFromLeft":Landroid/view/animation/Animation;
    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 149
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 150
    return-object v0
.end method

.method public static outToTopAnimation()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 75
    const/16 v0, 0x12c

    invoke-static {v0}, Lcom/vlingo/midas/gui/AnimationUtils;->outToTopAnimation(I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public static outToTopAnimation(I)Landroid/view/animation/Animation;
    .locals 9
    .param p0, "duration"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 130
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v8, -0x40800000    # -1.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v6, v2

    move v7, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 136
    .local v0, "inFromLeft":Landroid/view/animation/Animation;
    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 137
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 138
    return-object v0
.end method
