.class public Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "NaverLocalWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/midas/naver/NaverAdaptor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private btnCall:Landroid/widget/Button;

.field private btnFindRoute:Landroid/widget/Button;

.field private btnMore:Landroid/widget/Button;

.field private item:Lcom/vlingo/midas/naver/NaverLocalItem;

.field private mAddress:Landroid/widget/TextView;

.field private mCallNumber:Landroid/widget/TextView;

.field private mCategory:Landroid/widget/TextView;

.field private final mContext:Landroid/content/Context;

.field private mDistance:Landroid/widget/TextView;

.field private mMap:Landroid/widget/ImageView;

.field private mRate_img:Landroid/widget/RatingBar;

.field private mReview_count_num:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mhandler:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field private wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mContext:Landroid/content/Context;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;)Lcom/vlingo/midas/naver/NaverLocalItem;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mMap:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 6
    .param p1, "object"    # Lcom/vlingo/midas/naver/NaverAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const/16 v5, 0x8

    .line 121
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    .line 122
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverAdaptor;->getVVSActionHandlerListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mhandler:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 123
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverAdaptor;->getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v0

    .line 124
    .local v0, "result":Lcom/vlingo/midas/naver/NaverXMLParser;
    new-instance v3, Lcom/vlingo/midas/naver/NaverLocalItem;

    iget-object v2, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->itemList:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Hashtable;

    invoke-direct {v3, v2}, Lcom/vlingo/midas/naver/NaverLocalItem;-><init>(Ljava/util/Hashtable;)V

    iput-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    .line 126
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mTitle:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v3}, Lcom/vlingo/midas/naver/NaverLocalItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mAddress:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v3}, Lcom/vlingo/midas/naver/NaverLocalItem;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mCallNumber:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v3}, Lcom/vlingo/midas/naver/NaverLocalItem;->getPhoneUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    const/4 v1, 0x0

    .line 130
    .local v1, "review_count":I
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverLocalItem;->getReviewCount()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 131
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mReview_count_num:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mDistance:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v3}, Lcom/vlingo/midas/naver/NaverLocalItem;->getDistance()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mRate_img:Landroid/widget/RatingBar;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v3}, Lcom/vlingo/midas/naver/NaverLocalItem;->getRating()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mRate_img:Landroid/widget/RatingBar;

    invoke-virtual {v4}, Landroid/widget/RatingBar;->getNumStars()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/RatingBar;->setRating(F)V

    .line 134
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mCategory:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v3}, Lcom/vlingo/midas/naver/NaverLocalItem;->getCategory()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 200
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 201
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverLocalItem;->getPhoneUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 208
    :goto_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverLocalItem;->getRouteUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 215
    :goto_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverLocalItem;->getDetailUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 236
    :cond_0
    :goto_2
    return-void

    .line 204
    :cond_1
    sget v2, Lcom/vlingo/midas/R$id;->button_divider1:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 205
    sget v2, Lcom/vlingo/midas/R$id;->button_divider2:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 206
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnCall:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 211
    :cond_2
    sget v2, Lcom/vlingo/midas/R$id;->button_divider1:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 212
    sget v2, Lcom/vlingo/midas/R$id;->button_divider2:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 213
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnFindRoute:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 218
    :cond_3
    sget v2, Lcom/vlingo/midas/R$id;->button_divider1:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 219
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnMore:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 222
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverLocalItem;->getPhoneUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 226
    :goto_3
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverLocalItem;->getRouteUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 230
    :goto_4
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverLocalItem;->getDetailUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 233
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnMore:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 225
    :cond_5
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnCall:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3

    .line 229
    :cond_6
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnFindRoute:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_4
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 34
    check-cast p1, Lcom/vlingo/midas/naver/NaverAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 64
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 65
    .local v0, "sIntent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnFindRoute:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverLocalItem;->getRouteUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 67
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverLocalItem;->getRouteUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 81
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 82
    :cond_0
    return-void

    .line 68
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnMore:Landroid/widget/Button;

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverLocalItem;->getDetailUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 69
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverLocalItem;->getDetailUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    .line 74
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnCall:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverLocalItem;->getPhoneUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverLocalItem;->getPhoneUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/util/DialUtil;->getDialIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 99
    sget v0, Lcom/vlingo/midas/R$id;->text_Local_Main_Title:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mTitle:Landroid/widget/TextView;

    .line 100
    sget v0, Lcom/vlingo/midas/R$id;->image_Region_Map:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mMap:Landroid/widget/ImageView;

    .line 101
    sget v0, Lcom/vlingo/midas/R$id;->text_Local_Address:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mAddress:Landroid/widget/TextView;

    .line 102
    sget v0, Lcom/vlingo/midas/R$id;->text_Local_CallNumber:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mCallNumber:Landroid/widget/TextView;

    .line 103
    sget v0, Lcom/vlingo/midas/R$id;->btn_Local_Route:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnFindRoute:Landroid/widget/Button;

    .line 104
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnFindRoute:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    sget v0, Lcom/vlingo/midas/R$id;->btn_Local_More:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnMore:Landroid/widget/Button;

    .line 106
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnMore:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    sget v0, Lcom/vlingo/midas/R$id;->btn_Local_Call:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnCall:Landroid/widget/Button;

    .line 108
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnCall:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    sget v0, Lcom/vlingo/midas/R$id;->ls_item_rate_img:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mRate_img:Landroid/widget/RatingBar;

    .line 110
    sget v0, Lcom/vlingo/midas/R$id;->ls_item_review_count:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mReview_count_num:Landroid/widget/TextView;

    .line 111
    sget v0, Lcom/vlingo/midas/R$id;->text_Local_Distance:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mDistance:Landroid/widget/TextView;

    .line 112
    sget v0, Lcom/vlingo/midas/R$id;->ls_item_category:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mCategory:Landroid/widget/TextView;

    .line 114
    return-void
.end method

.method public retire()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 87
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->retire()V

    .line 88
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnMore:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnCall:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->btnFindRoute:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 91
    return-void
.end method
