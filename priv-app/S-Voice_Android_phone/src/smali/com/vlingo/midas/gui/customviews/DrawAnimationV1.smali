.class public Lcom/vlingo/midas/gui/customviews/DrawAnimationV1;
.super Ljava/lang/Object;
.source "DrawAnimationV1.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V
    .locals 7
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "cornerBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "squareImage"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p3, "mRms"    # F

    .prologue
    const/high16 v6, 0x42700000    # 60.0f

    const/high16 v5, 0x42f00000    # 120.0f

    const/high16 v4, 0x42c80000    # 100.0f

    const/high16 v3, 0x430c0000    # 140.0f

    const/4 v2, 0x0

    .line 135
    const/high16 v0, 0x41f80000    # 31.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 136
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 137
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 138
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 139
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 140
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 141
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 142
    const/high16 v0, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 144
    const/16 v0, 0x28

    const/16 v1, 0xaa

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 145
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    const/high16 v0, 0x42140000    # 37.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 148
    const/4 v0, 0x0

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 149
    const/high16 v0, 0x41a00000    # 20.0f

    const/high16 v1, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 150
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 151
    const/high16 v0, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 152
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 153
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 154
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 155
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 156
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 158
    const/16 v0, 0x28

    const/16 v1, 0xa0

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 159
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 160
    :cond_2
    const/high16 v0, 0x422c0000    # 43.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_3

    .line 161
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 162
    const/high16 v0, 0x41a00000    # 20.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 163
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 164
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 165
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 166
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 167
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 168
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 169
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 171
    const/16 v0, 0x28

    const/16 v1, 0x96

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 172
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 173
    :cond_3
    const/high16 v0, 0x42440000    # 49.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    .line 174
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 175
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 176
    invoke-virtual {p0, p1, v6, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 177
    invoke-virtual {p0, p1, v4, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 178
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 179
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 180
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 181
    const/high16 v0, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 182
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 183
    const/16 v0, 0x28

    const/16 v1, 0x8c

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 184
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 185
    :cond_4
    const/high16 v0, 0x425c0000    # 55.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_5

    .line 186
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 187
    const/high16 v0, 0x41a00000    # 20.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 188
    invoke-virtual {p0, p1, v6, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 189
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 190
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 191
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 192
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 193
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 194
    const/high16 v0, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 195
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 197
    const/16 v0, 0x28

    const/16 v1, 0x82

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 198
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 200
    :cond_5
    const/high16 v0, 0x42740000    # 61.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6

    .line 201
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 202
    const/high16 v0, 0x41a00000    # 20.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 203
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 204
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 205
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 206
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 207
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 208
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 209
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 211
    const/16 v0, 0x28

    const/16 v1, 0x78

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 212
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 213
    :cond_6
    const/high16 v0, 0x42860000    # 67.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_7

    .line 214
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 215
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 216
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 217
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 218
    invoke-virtual {p0, p1, v3, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 219
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 220
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 221
    const/high16 v0, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 222
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 224
    const/16 v0, 0x28

    const/16 v1, 0x6e

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 225
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 226
    :cond_7
    const/high16 v0, 0x42920000    # 73.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_8

    .line 227
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 228
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 229
    invoke-virtual {p0, p1, v6, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 230
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 231
    invoke-virtual {p0, p1, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 232
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 233
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 234
    const/high16 v0, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 235
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 237
    const/16 v0, 0x28

    const/16 v1, 0x64

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 238
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 239
    :cond_8
    const/high16 v0, 0x429e0000    # 79.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_9

    .line 240
    const/4 v0, 0x0

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 241
    const/high16 v0, 0x41a00000    # 20.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 242
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 243
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 244
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 245
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 246
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 247
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 248
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 250
    const/16 v0, 0x28

    const/16 v1, 0x50

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 251
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 252
    :cond_9
    const/high16 v0, 0x429e0000    # 79.0f

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 253
    const/16 v0, 0x28

    const/16 v1, 0x28

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 254
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method public static drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V
    .locals 7
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "cornerBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "squareImage"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p3, "mRms"    # F

    .prologue
    const/high16 v6, 0x42700000    # 60.0f

    const/high16 v5, 0x42f00000    # 120.0f

    const/high16 v4, 0x42c80000    # 100.0f

    const/high16 v3, 0x430c0000    # 140.0f

    const/4 v2, 0x0

    .line 10
    const/high16 v0, 0x41f80000    # 31.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 11
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 12
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 13
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 14
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 15
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 16
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 17
    const/high16 v0, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 19
    const/16 v0, 0x28

    const/16 v1, 0xaa

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 20
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    const/high16 v0, 0x42140000    # 37.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 23
    const/4 v0, 0x0

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 24
    const/high16 v0, 0x41a00000    # 20.0f

    const/high16 v1, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 25
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 26
    const/high16 v0, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 27
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 28
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 29
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 30
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 31
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 33
    const/16 v0, 0x28

    const/16 v1, 0xa0

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 34
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 35
    :cond_2
    const/high16 v0, 0x422c0000    # 43.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_3

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 37
    const/high16 v0, 0x41a00000    # 20.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 38
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 39
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 40
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 41
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 42
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 43
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 44
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 46
    const/16 v0, 0x28

    const/16 v1, 0x96

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 47
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 48
    :cond_3
    const/high16 v0, 0x42440000    # 49.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 50
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 51
    invoke-virtual {p0, p1, v6, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 52
    invoke-virtual {p0, p1, v4, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 53
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 54
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 55
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 56
    const/high16 v0, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 57
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 58
    const/16 v0, 0x28

    const/16 v1, 0x8c

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 59
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 60
    :cond_4
    const/high16 v0, 0x425c0000    # 55.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_5

    .line 61
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 62
    const/high16 v0, 0x41a00000    # 20.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 63
    invoke-virtual {p0, p1, v6, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 64
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 65
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 66
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 67
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 68
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 69
    const/high16 v0, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 70
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 72
    const/16 v0, 0x28

    const/16 v1, 0x82

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 73
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 75
    :cond_5
    const/high16 v0, 0x42740000    # 61.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6

    .line 76
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 77
    const/high16 v0, 0x41a00000    # 20.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 78
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 79
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 80
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 81
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 82
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 83
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 84
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 86
    const/16 v0, 0x28

    const/16 v1, 0x78

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 87
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 88
    :cond_6
    const/high16 v0, 0x42860000    # 67.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_7

    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 90
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 91
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 92
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 93
    invoke-virtual {p0, p1, v3, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 94
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 95
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 96
    const/high16 v0, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 97
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 99
    const/16 v0, 0x28

    const/16 v1, 0x6e

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 100
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 101
    :cond_7
    const/high16 v0, 0x42920000    # 73.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_8

    .line 102
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 103
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 104
    invoke-virtual {p0, p1, v6, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 105
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 106
    invoke-virtual {p0, p1, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 107
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 108
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 109
    const/high16 v0, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 110
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 112
    const/16 v0, 0x28

    const/16 v1, 0x64

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 113
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 114
    :cond_8
    const/high16 v0, 0x429e0000    # 79.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_9

    .line 115
    const/4 v0, 0x0

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 116
    const/high16 v0, 0x41a00000    # 20.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 117
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 118
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 119
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 120
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 121
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 122
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 123
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 125
    const/16 v0, 0x28

    const/16 v1, 0x50

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 126
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 127
    :cond_9
    const/high16 v0, 0x429e0000    # 79.0f

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 128
    const/16 v0, 0x28

    const/16 v1, 0x28

    const/16 v2, 0x186

    const/16 v3, 0xc8

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 129
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method
