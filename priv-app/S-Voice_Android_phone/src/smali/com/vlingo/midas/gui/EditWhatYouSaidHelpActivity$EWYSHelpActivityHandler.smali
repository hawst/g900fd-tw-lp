.class Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;
.super Landroid/os/Handler;
.source "EditWhatYouSaidHelpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EWYSHelpActivityHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .prologue
    .line 257
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 258
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 259
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 262
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .line 263
    .local v0, "o":Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    if-eqz v0, :cond_0

    .line 264
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 291
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 266
    :pswitch_1
    # getter for: Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
    invoke-static {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->access$000(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    if-ne v1, v2, :cond_0

    .line 268
    # getter for: Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
    invoke-static {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->access$000(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->scheduleStartSpotter(J)V

    goto :goto_0

    .line 275
    :pswitch_2
    # getter for: Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z
    invoke-static {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->access$100(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 276
    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z
    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->access$102(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;Z)Z

    .line 279
    :cond_1
    # getter for: Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
    invoke-static {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->access$000(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    goto :goto_0

    .line 283
    :pswitch_3
    # invokes: Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->clearDismissKeyguardFlag()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->access$200(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    goto :goto_0

    .line 287
    :pswitch_4
    # getter for: Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;
    invoke-static {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->access$300(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->removeMessages(I)V

    goto :goto_0

    .line 264
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
