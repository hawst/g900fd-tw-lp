.class public Lcom/vlingo/midas/gui/WelcomePage;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "WelcomePage.java"

# interfaces
.implements Lcom/vlingo/midas/gui/WelcomeFragment$onNext;


# static fields
.field public static final IS_FIRST_LAUNCH:Ljava/lang/String; = "IS_FIRST_LAUNCH"

.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"

.field public static final LOAD_DISCLAIMER_FOR_PHONE:I = 0x1

.field public static final LOAD_DISCLAIMER_FOR_TABLET:I = 0x2


# instance fields
.field private mCurrentFragment:Landroid/support/v4/app/Fragment;

.field private mDefaultSharedPrefs1:Landroid/content/SharedPreferences;

.field private mSaveInstanceState:Landroid/os/Bundle;

.field private mSvoiceTitle:Landroid/widget/TextView;

.field private mWelcomeVideo:Landroid/widget/VideoView;

.field prefForWishesEventAlert:Landroid/content/SharedPreferences;

.field private welcomeDone:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->welcomeDone:Z

    .line 48
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomePage;->mSvoiceTitle:Landroid/widget/TextView;

    .line 49
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    .line 54
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomePage;->mSaveInstanceState:Landroid/os/Bundle;

    return-void
.end method

.method private LaunchFirstTimeScreen()V
    .locals 3

    .prologue
    .line 255
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 256
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->afterTOSAccepted()V

    .line 259
    :cond_0
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/WelcomePage;->startActivity(Landroid/content/Intent;)V

    .line 260
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->finish()V

    .line 261
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/WelcomePage;)Landroid/widget/VideoView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/WelcomePage;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/WelcomePage;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/WelcomePage;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->setBackgroundForKitkat()V

    return-void
.end method

.method private afterTOSAccepted()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 266
    invoke-static {v4}, Lcom/vlingo/midas/settings/MidasSettings;->setLocationEnabled(Z)V

    .line 267
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.vlingo.LANGUAGE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/WelcomePage;->sendBroadcast(Landroid/content/Intent;)V

    .line 271
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/ServiceManager;->startLocalServices()Z

    .line 272
    sget-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v1, :cond_0

    .line 273
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->startOtherAppAfterAccept()V

    .line 276
    :cond_0
    invoke-static {v4}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 277
    invoke-static {v4}, Lcom/vlingo/midas/settings/MidasSettings;->setTOSAccepted(Z)V

    .line 278
    const-string/jumbo v1, "all_notifications_accepted"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 280
    const-string/jumbo v1, "tos_accepted_version"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 281
    const-string/jumbo v1, "prefForWishesEventAlert"

    invoke-virtual {p0, v1, v3}, Lcom/vlingo/midas/gui/WelcomePage;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomePage;->prefForWishesEventAlert:Landroid/content/SharedPreferences;

    .line 283
    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomePage;->prefForWishesEventAlert:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 284
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "IS_FIRST_LAUNCH"

    const-string/jumbo v2, "0000-00-00"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 285
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 288
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    sget-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v1, :cond_2

    .line 289
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->finishAffinity()V

    .line 290
    sput-boolean v3, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    .line 295
    :goto_0
    return-void

    .line 293
    :cond_2
    invoke-static {v3}, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->setLaunchedForTosAcceptOtherApp(Z)V

    .line 294
    const-string/jumbo v1, "former_tos_acceptance_state"

    const-string/jumbo v2, "reminder_not_needed"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private hideSystemUI()V
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 58
    .local v0, "decorView":Landroid/view/View;
    const/16 v1, 0xf06

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 64
    return-void
.end method

.method private init()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 161
    const-string/jumbo v2, "WelcomePage"

    const-string/jumbo v3, "init"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "android.resource://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$raw;->svoice_welcomepage:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 163
    .local v1, "video":Landroid/net/Uri;
    sget v2, Lcom/vlingo/midas/R$id;->welcome_video:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/WelcomePage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/VideoView;

    iput-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    .line 164
    sget v2, Lcom/vlingo/midas/R$id;->placeHolder:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/WelcomePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 165
    .local v0, "placeholder":Landroid/widget/FrameLayout;
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/WelcomePage;->setWFDTcpDisable(Landroid/widget/VideoView;)V

    .line 166
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v2, v1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 167
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v2, v4}, Landroid/widget/VideoView;->setFocusable(Z)V

    .line 168
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v2, v4}, Landroid/widget/VideoView;->setFocusableInTouchMode(Z)V

    .line 174
    sget v2, Lcom/vlingo/midas/R$id;->svoice_title_welcomepage:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/WelcomePage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mSvoiceTitle:Landroid/widget/TextView;

    .line 175
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mSvoiceTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->requestFocus()Z

    .line 178
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->welcomeDone:Z

    if-nez v2, :cond_1

    .line 179
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    new-instance v3, Lcom/vlingo/midas/gui/WelcomePage$2;

    invoke-direct {v3, p0, v0}, Lcom/vlingo/midas/gui/WelcomePage$2;-><init>(Lcom/vlingo/midas/gui/WelcomePage;Landroid/widget/FrameLayout;)V

    invoke-virtual {v2, v3}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 199
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    new-instance v3, Lcom/vlingo/midas/gui/WelcomePage$3;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/WelcomePage$3;-><init>(Lcom/vlingo/midas/gui/WelcomePage;)V

    invoke-virtual {v2, v3}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 208
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->loadWelcomeFragment()V

    .line 212
    invoke-static {p0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    if-eqz v2, :cond_0

    .line 214
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/VideoView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->setBackgroundForKitkat()V

    .line 220
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->getAlpha()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 221
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/VideoView;->setAlpha(F)V

    .line 224
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->loadDislaimerFragment()V

    goto :goto_0
.end method

.method private loadDislaimerFragment()V
    .locals 4

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 300
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->welcomeDone:Z

    if-nez v2, :cond_0

    .line 301
    sget v2, Lcom/vlingo/midas/R$animator;->slide_in_right:I

    sget v3, Lcom/vlingo/midas/R$animator;->slide_out_left:I

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 302
    new-instance v2, Lcom/vlingo/midas/gui/DisclaimerFragment;

    invoke-direct {v2}, Lcom/vlingo/midas/gui/DisclaimerFragment;-><init>()V

    iput-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 303
    sget v2, Lcom/vlingo/midas/R$id;->welcome_page_container:I

    iget-object v3, p0, Lcom/vlingo/midas/gui/WelcomePage;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 305
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->welcomeDone:Z

    .line 306
    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mDefaultSharedPrefs1:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 307
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "welcome from conversation"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 308
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 309
    return-void
.end method

.method private loadWelcomeFragment()V
    .locals 3

    .prologue
    .line 230
    new-instance v0, Lcom/vlingo/midas/gui/WelcomeFragment;

    invoke-direct {v0}, Lcom/vlingo/midas/gui/WelcomeFragment;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 231
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->welcome_page_container:I

    iget-object v2, p0, Lcom/vlingo/midas/gui/WelcomePage;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 232
    return-void
.end method

.method private setBackgroundForKitkat()V
    .locals 3

    .prologue
    .line 142
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 143
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->welcome_page_img_v:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->welcome_page_img:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;
    .locals 1

    .prologue
    .line 152
    new-instance v0, Lcom/vlingo/midas/gui/WelcomePage$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/WelcomePage$1;-><init>(Lcom/vlingo/midas/gui/WelcomePage;)V

    return-object v0
.end method

.method private setWFDTcpDisable(Landroid/widget/VideoView;)V
    .locals 8
    .param p1, "videoView"    # Landroid/widget/VideoView;

    .prologue
    .line 354
    :try_start_0
    const-string/jumbo v4, "android.widget.VideoView"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 355
    .local v3, "videoViewClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v4, "setWFDTcpDisable"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 356
    .local v2, "setWFDTcpDisable":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, p1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_0

    .line 359
    const-string/jumbo v4, "setStopMusic"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 360
    .local v1, "setMusicStop":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, p1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 379
    .end local v1    # "setMusicStop":Ljava/lang/reflect/Method;
    .end local v2    # "setWFDTcpDisable":Ljava/lang/reflect/Method;
    .end local v3    # "videoViewClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    :goto_0
    return-void

    .line 363
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 366
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 368
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 369
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v0

    .line 371
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 372
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 374
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 375
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v0

    .line 377
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private startOtherAppAfterAccept()V
    .locals 2

    .prologue
    .line 332
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 333
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.midas.action.TOS_ACCEPTED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 335
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 337
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->finishAffinity()V

    .line 238
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x1

    .line 70
    iput-object p1, p0, Lcom/vlingo/midas/gui/WelcomePage;->mSaveInstanceState:Landroid/os/Bundle;

    .line 71
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v9

    iput-object v9, p0, Lcom/vlingo/midas/gui/WelcomePage;->mDefaultSharedPrefs1:Landroid/content/SharedPreferences;

    .line 72
    iget-object v9, p0, Lcom/vlingo/midas/gui/WelcomePage;->mDefaultSharedPrefs1:Landroid/content/SharedPreferences;

    const-string/jumbo v10, "welcome from conversation"

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 73
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/vlingo/midas/gui/WelcomePage;->welcomeDone:Z

    .line 77
    :goto_0
    invoke-virtual {p0, v11}, Lcom/vlingo/midas/gui/WelcomePage;->requestWindowFeature(I)Z

    .line 78
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->hideSystemUI()V

    .line 79
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 81
    invoke-virtual {p0, v11}, Lcom/vlingo/midas/gui/WelcomePage;->setRequestedOrientation(I)V

    .line 83
    :cond_0
    sget v9, Lcom/vlingo/midas/R$layout;->welcome_page_for_svoice:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/gui/WelcomePage;->setContentView(I)V

    .line 84
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->init()V

    .line 85
    const-string/jumbo v9, "system/fonts/SamsungSans-Bold.ttf"

    invoke-static {v9}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v5

    .line 86
    .local v5, "samsungSans":Landroid/graphics/Typeface;
    iget-object v9, p0, Lcom/vlingo/midas/gui/WelcomePage;->mSvoiceTitle:Landroid/widget/TextView;

    if-eqz v9, :cond_1

    .line 87
    iget-object v9, p0, Lcom/vlingo/midas/gui/WelcomePage;->mSvoiceTitle:Landroid/widget/TextView;

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 91
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 92
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "privateFlags"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 93
    .local v4, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 94
    .local v6, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 96
    .local v1, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v0, 0x0

    .local v0, "currentPrivateFlags":I
    const/4 v8, 0x0

    .local v8, "valueofFlagsEnableStatusBar":I
    const/4 v7, 0x0

    .line 97
    .local v7, "valueofFlagsDisableTray":I
    invoke-virtual {v4, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 98
    invoke-virtual {v6, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 99
    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v7

    .line 101
    or-int/2addr v0, v8

    .line 102
    or-int/2addr v0, v7

    .line 104
    invoke-virtual {v4, v3, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 105
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    .end local v0    # "currentPrivateFlags":I
    .end local v1    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v3    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v4    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v6    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v7    # "valueofFlagsDisableTray":I
    .end local v8    # "valueofFlagsEnableStatusBar":I
    :goto_1
    return-void

    .line 75
    .end local v5    # "samsungSans":Landroid/graphics/Typeface;
    :cond_2
    iput-boolean v11, p0, Lcom/vlingo/midas/gui/WelcomePage;->welcomeDone:Z

    goto :goto_0

    .line 109
    .restart local v5    # "samsungSans":Landroid/graphics/Typeface;
    :catch_0
    move-exception v2

    .line 112
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 106
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v9

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 243
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v0, v3}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 245
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v0, v3}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 246
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v0, v3}, Landroid/widget/VideoView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 247
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$color;->solid_black:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setBackgroundColor(I)V

    .line 248
    iput-object v3, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    .line 251
    :cond_0
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onDestroy()V

    .line 252
    return-void
.end method

.method public onNextClicked(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 313
    packed-switch p1, :pswitch_data_0

    .line 328
    :goto_0
    return-void

    .line 315
    :pswitch_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->LaunchFirstTimeScreen()V

    goto :goto_0

    .line 318
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->loadDislaimerFragment()V

    goto :goto_0

    .line 322
    :pswitch_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->LaunchFirstTimeScreen()V

    goto :goto_0

    .line 313
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 127
    const-string/jumbo v0, "WelcomePage"

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onPause()V

    .line 129
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->suspend()V

    .line 139
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 344
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 345
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomePage;->hideSystemUI()V

    .line 346
    const-string/jumbo v0, "WelcomePage"

    const-string/jumbo v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->resume()V

    .line 350
    :cond_0
    return-void
.end method
