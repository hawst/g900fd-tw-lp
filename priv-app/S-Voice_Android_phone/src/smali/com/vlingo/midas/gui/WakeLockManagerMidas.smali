.class public Lcom/vlingo/midas/gui/WakeLockManagerMidas;
.super Lcom/vlingo/core/internal/display/WakeLockManagerNoop;
.source "WakeLockManagerMidas.java"


# static fields
.field private static final S_VOICE_PKG:Ljava/lang/String; = "com.vlingo.midas"

.field private static final WAKE_LOCK_NAME:Ljava/lang/String; = "VlingoMain"

.field private static final WAKE_LOCK_PAUSE:J = 0xbb8L

.field private static instance:Lcom/vlingo/core/internal/display/WakeLockManager;

.field private static screenTimeoutVal:I


# instance fields
.field private powerManager:Landroid/os/PowerManager;

.field private wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/core/internal/display/WakeLockManagerNoop;-><init>()V

    .line 40
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->powerManager:Landroid/os/PowerManager;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/WakeLockManagerMidas;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/WakeLockManagerMidas;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/gui/WakeLockManagerMidas;Landroid/os/PowerManager$WakeLock;)Landroid/os/PowerManager$WakeLock;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/WakeLockManagerMidas;
    .param p1, "x1"    # Landroid/os/PowerManager$WakeLock;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;

    return-object p1
.end method

.method public static declared-synchronized getInstance()Lcom/vlingo/core/internal/display/WakeLockManager;
    .locals 2

    .prologue
    .line 32
    const-class v1, Lcom/vlingo/midas/gui/WakeLockManagerMidas;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->instance:Lcom/vlingo/core/internal/display/WakeLockManager;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;

    invoke-direct {v0}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;-><init>()V

    sput-object v0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->instance:Lcom/vlingo/core/internal/display/WakeLockManager;

    .line 35
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->instance:Lcom/vlingo/core/internal/display/WakeLockManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static setScreenTimeoutVal(I)V
    .locals 0
    .param p0, "value"    # I

    .prologue
    .line 45
    sput p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->screenTimeoutVal:I

    .line 46
    return-void
.end method


# virtual methods
.method public declared-synchronized acquireWakeLock()V
    .locals 3

    .prologue
    .line 59
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->powerManager:Landroid/os/PowerManager;

    const v1, 0x3000001a

    const-string/jumbo v2, "VlingoMain"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 67
    const-string/jumbo v0, "JDEBUG"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " wakeLock acquired wakeLock : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 71
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public isMidasPkg()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 113
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v6, "activity"

    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 115
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 116
    .local v2, "runningList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v2, :cond_1

    .line 117
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 118
    .local v1, "cmpName":Landroid/content/ComponentName;
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, "com.vlingo.midas"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    .line 120
    .end local v1    # "cmpName":Landroid/content/ComponentName;
    :goto_0
    return v3

    .restart local v1    # "cmpName":Landroid/content/ComponentName;
    :cond_0
    move v3, v5

    .line 118
    goto :goto_0

    .end local v1    # "cmpName":Landroid/content/ComponentName;
    :cond_1
    move v3, v5

    .line 120
    goto :goto_0
.end method

.method public declared-synchronized releaseWakeLock()V
    .locals 1

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/vlingo/midas/gui/WakeLockManagerMidas$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/WakeLockManagerMidas$1;-><init>(Lcom/vlingo/midas/gui/WakeLockManagerMidas;)V

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/WakeLockManagerMidas$1;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    monitor-exit p0

    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
