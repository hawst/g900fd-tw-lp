.class public Lcom/vlingo/midas/gui/widgets/TimerWidget$TimerIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TimerWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/TimerWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimerIntentReceiver"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 561
    const-class v0, Lcom/vlingo/midas/gui/widgets/TimerWidget$TimerIntentReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget$TimerIntentReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 565
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v9

    sget-object v10, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v9, v10}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getWidgetSpecificProperties(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)Ljava/util/Map;

    move-result-object v9

    if-nez v9, :cond_1

    .line 605
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    sget-object v9, Lcom/vlingo/midas/gui/widgets/TimerWidget$TimerIntentReceiver;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "onReceive() action="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 571
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v9, "com.sec.android.app.clockpackage.timer.NOTIFY_TIMER_CMD_RESULT"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 572
    const-string/jumbo v9, "Result"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 573
    .local v6, "resultSuccess":Ljava/lang/String;
    const-string/jumbo v9, "Success"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 574
    const-string/jumbo v9, "Input command"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 575
    .local v3, "inputCmd":Ljava/lang/String;
    const-string/jumbo v9, "Remain Millis"

    const-wide/16 v10, -0x1

    invoke-virtual {p2, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    sput-wide v9, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    .line 576
    # invokes: Lcom/vlingo/midas/gui/widgets/TimerWidget;->initTimeView()V
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->access$200()V

    .line 578
    const-string/jumbo v9, "com.sec.android.app.clockpackage.timer.TIMER_START"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string/jumbo v9, "com.sec.android.app.clockpackage.timer.TIMER_RESTART"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 579
    :cond_2
    const/4 v9, 0x1

    invoke-static {v9}, Lcom/vlingo/midas/gui/timer/TimerManager;->setState(I)V

    .line 580
    sget-wide v9, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    # invokes: Lcom/vlingo/midas/gui/widgets/TimerWidget;->setTimer(J)V
    invoke-static {v9, v10}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->access$300(J)V

    .line 581
    sget-object v9, Lcom/vlingo/midas/gui/timer/TimerManager;->timer:Landroid/os/CountDownTimer;

    invoke-virtual {v9}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0

    .line 583
    :cond_3
    const-string/jumbo v9, "com.sec.android.app.clockpackage.timer.TIMER_STOP"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 584
    const/4 v9, 0x2

    invoke-static {v9}, Lcom/vlingo/midas/gui/timer/TimerManager;->setState(I)V

    goto :goto_0

    .line 589
    .end local v3    # "inputCmd":Ljava/lang/String;
    .end local v6    # "resultSuccess":Ljava/lang/String;
    :cond_4
    const-string/jumbo v9, "timer_send_time"

    const-wide/16 v10, -0x1

    invoke-virtual {p2, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    .line 590
    .local v7, "sendTime":J
    const-string/jumbo v9, "timer_remain_time"

    const-wide/16 v10, -0x1

    invoke-virtual {p2, v9, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 591
    .local v4, "remainTime":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 592
    .local v1, "currentTime":J
    sub-long v9, v1, v7

    sub-long v9, v4, v9

    sput-wide v9, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    .line 593
    # invokes: Lcom/vlingo/midas/gui/widgets/TimerWidget;->initTimeView()V
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->access$200()V

    .line 594
    const-string/jumbo v9, "com.sec.android.app.clockpackage.timer.ticker.TIMER_START"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 595
    const/4 v9, 0x1

    invoke-static {v9}, Lcom/vlingo/midas/gui/timer/TimerManager;->setState(I)V

    .line 596
    sget-wide v9, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    # invokes: Lcom/vlingo/midas/gui/widgets/TimerWidget;->setTimer(J)V
    invoke-static {v9, v10}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->access$300(J)V

    .line 597
    sget-object v9, Lcom/vlingo/midas/gui/timer/TimerManager;->timer:Landroid/os/CountDownTimer;

    invoke-virtual {v9}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto/16 :goto_0

    .line 598
    :cond_5
    const-string/jumbo v9, "com.sec.android.app.clockpackage.timer.ticker.TIMER_RESET"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 599
    const/4 v9, 0x3

    invoke-static {v9}, Lcom/vlingo/midas/gui/timer/TimerManager;->setState(I)V

    .line 600
    # invokes: Lcom/vlingo/midas/gui/widgets/TimerWidget;->initTimeView()V
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->access$200()V

    goto/16 :goto_0

    .line 601
    :cond_6
    const-string/jumbo v9, "com.sec.android.app.clockpackage.timer.ticker.TIMER_STOP"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 602
    const/4 v9, 0x2

    invoke-static {v9}, Lcom/vlingo/midas/gui/timer/TimerManager;->setState(I)V

    goto/16 :goto_0
.end method
