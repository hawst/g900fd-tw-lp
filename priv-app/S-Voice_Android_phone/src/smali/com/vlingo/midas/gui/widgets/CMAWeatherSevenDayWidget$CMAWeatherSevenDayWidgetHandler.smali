.class Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$CMAWeatherSevenDayWidgetHandler;
.super Landroid/os/Handler;
.source "CMAWeatherSevenDayWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CMAWeatherSevenDayWidgetHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;

    .prologue
    .line 386
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 387
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$CMAWeatherSevenDayWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 388
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 391
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget$CMAWeatherSevenDayWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;

    .line 392
    .local v0, "o":Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;
    if-eqz v0, :cond_0

    .line 393
    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->setValuesForecastWeatherOneDay()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->access$600(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;)V

    .line 394
    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->setValuesForecastWeather()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->access$700(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;)V

    .line 395
    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->setTalkbackString()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;->access$800(Lcom/vlingo/midas/gui/widgets/CMAWeatherSevenDayWidget;)V

    .line 397
    :cond_0
    return-void
.end method
