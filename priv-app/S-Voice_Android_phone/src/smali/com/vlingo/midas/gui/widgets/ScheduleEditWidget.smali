.class public Lcom/vlingo/midas/gui/widgets/ScheduleEditWidget;
.super Lcom/vlingo/midas/gui/widgets/ScheduleWidget;
.source "ScheduleEditWidget.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method


# virtual methods
.method public launchSchedule()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 35
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 37
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->schedule_confirm:I

    if-ne v0, v1, :cond_1

    .line 39
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEditWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    if-eqz v1, :cond_0

    .line 40
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEditWidget;->isClicked:Z

    .line 42
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleEditWidget;->retire()V

    .line 44
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEditWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    sget v1, Lcom/vlingo/midas/R$id;->btn_cancel_schedule:I

    if-ne v0, v1, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleEditWidget;->cancel()V

    goto :goto_0

    .line 51
    :catch_0
    move-exception v1

    goto :goto_0

    .line 48
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 27
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;->onFinishInflate()V

    .line 29
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEditWidget;->mConfirm:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleEditWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->save_btn:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/Button;->setText(Ljava/lang/CharSequence;)V

    .line 31
    return-void
.end method
