.class public Lcom/vlingo/midas/gui/customviews/ListeningView;
.super Landroid/view/View;
.source "ListeningView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/customviews/ListeningView$MyTimerTask;
    }
.end annotation


# static fields
.field private static final BASE_DENSITY:F = 3.0f


# instance fields
.field frameMultiplier:I

.field mContext:Landroid/content/Context;

.field private mCount:I

.field mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

.field mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

.field mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

.field mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

.field mCueRect4:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

.field private mDensityRatio:F

.field mHeight:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field mRect0Anchors1:[[F

.field mRect0Anchors2:[[F

.field mRect0Points:[[F

.field mRect1Anchors1:[[F

.field mRect1Anchors2:[[F

.field mRect1Points:[[F

.field mRect2Anchors1:[[F

.field mRect2Anchors2:[[F

.field mRect2Points:[[F

.field mRect3Anchors1:[[F

.field mRect3Anchors2:[[F

.field mRect3Points:[[F

.field mRect4Anchors1:[[F

.field mRect4Anchors2:[[F

.field mRect4Points:[[F

.field private mRmsHandler:Landroid/os/Handler;

.field mWidth:I

.field private myTimerTask:Lcom/vlingo/midas/gui/customviews/ListeningView$MyTimerTask;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/gui/customviews/ListeningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/midas/gui/customviews/ListeningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 101
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    iput v3, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->frameMultiplier:I

    .line 44
    const/4 v0, 0x7

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_4

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect4Points:[[F

    .line 47
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_7

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_8

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_9

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_a

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_b

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect4Anchors1:[[F

    .line 50
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_d

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_e

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_f

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_10

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_11

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect4Anchors2:[[F

    .line 55
    const/4 v0, 0x7

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_13

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_14

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_15

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_16

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_17

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect3Points:[[F

    .line 58
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_1a

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_1b

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_1c

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_1d

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_1e

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_1f

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect3Anchors1:[[F

    .line 61
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_20

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_21

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_22

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_23

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_24

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_25

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect3Anchors2:[[F

    .line 66
    const/4 v0, 0x7

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_26

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_27

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_28

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_29

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_2a

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_2b

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_2c

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect2Points:[[F

    .line 69
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_2d

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_2e

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_2f

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_30

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_31

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_32

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect2Anchors1:[[F

    .line 72
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_33

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_34

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_35

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_36

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_37

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_38

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect2Anchors2:[[F

    .line 78
    const/4 v0, 0x7

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_39

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_3a

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_3b

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_3c

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_3d

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_3e

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_3f

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect1Points:[[F

    .line 81
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_40

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_41

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_42

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_43

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_44

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_45

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect1Anchors1:[[F

    .line 84
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_46

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_47

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_48

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_49

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_4a

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_4b

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect1Anchors2:[[F

    .line 88
    const/4 v0, 0x7

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_4c

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_4d

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_4e

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_4f

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_50

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_51

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_52

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect0Points:[[F

    .line 91
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_53

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_54

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_55

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_56

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_57

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_58

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect0Anchors1:[[F

    .line 94
    const/4 v0, 0x6

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_59

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_5a

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_5b

    aput-object v1, v0, v3

    new-array v1, v3, [F

    fill-array-data v1, :array_5c

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_5d

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_5e

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect0Anchors2:[[F

    .line 267
    iput v4, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCount:I

    .line 290
    new-instance v0, Lcom/vlingo/midas/gui/customviews/ListeningView$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/customviews/ListeningView$1;-><init>(Lcom/vlingo/midas/gui/customviews/ListeningView;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRmsHandler:Landroid/os/Handler;

    .line 102
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->init(Landroid/content/Context;)V

    .line 103
    return-void

    .line 44
    nop

    :array_0
    .array-data 4
        0x43538000    # 211.5f
        0x42c10000    # 96.5f
    .end array-data

    :array_1
    .array-data 4
        0x43d3cccd    # 423.6f
        0x42c10000    # 96.5f
    .end array-data

    :array_2
    .array-data 4
        0x4401e666    # 519.6f
        0x43408000    # 192.5f
    .end array-data

    :array_3
    .array-data 4
        0x43d3cccd    # 423.6f
        0x43904000    # 288.5f
    .end array-data

    :array_4
    .array-data 4
        0x43538000    # 211.5f
        0x43904000    # 288.5f
    .end array-data

    :array_5
    .array-data 4
        0x42e6cccd    # 115.4f
        0x43408000    # 192.5f
    .end array-data

    :array_6
    .array-data 4
        0x43538000    # 211.5f
        0x42c10000    # 96.5f
    .end array-data

    .line 47
    :array_7
    .array-data 4
        0x43d3cccd    # 423.6f
        0x42c10000    # 96.5f
    .end array-data

    :array_8
    .array-data 4
        0x43ee4ccd    # 476.6f
        0x42c10000    # 96.5f
    .end array-data

    :array_9
    .array-data 4
        0x4401e666    # 519.6f
        0x43758000    # 245.5f
    .end array-data

    :array_a
    .array-data 4
        0x43538000    # 211.5f
        0x43904000    # 288.5f
    .end array-data

    :array_b
    .array-data 4
        0x431e6666    # 158.4f
        0x43904000    # 288.5f
    .end array-data

    :array_c
    .array-data 4
        0x42e6cccd    # 115.4f
        0x430b8000    # 139.5f
    .end array-data

    .line 50
    :array_d
    .array-data 4
        0x43d3cccd    # 423.6f
        0x42c10000    # 96.5f
    .end array-data

    :array_e
    .array-data 4
        0x4401e666    # 519.6f
        0x430b8000    # 139.5f
    .end array-data

    :array_f
    .array-data 4
        0x43ee4ccd    # 476.6f
        0x43904000    # 288.5f
    .end array-data

    :array_10
    .array-data 4
        0x43538000    # 211.5f
        0x43904000    # 288.5f
    .end array-data

    :array_11
    .array-data 4
        0x42e6cccd    # 115.4f
        0x43758000    # 245.5f
    .end array-data

    :array_12
    .array-data 4
        0x431e6666    # 158.4f
        0x42c10000    # 96.5f
    .end array-data

    .line 55
    :array_13
    .array-data 4
        0x43d5c000    # 427.5f
        0x439c0000    # 312.0f
    .end array-data

    :array_14
    .array-data 4
        0x434f8000    # 207.5f
        0x439c0000    # 312.0f
    .end array-data

    :array_15
    .array-data 4
        0x42b00000    # 88.0f
        0x43408000    # 192.5f
    .end array-data

    :array_16
    .array-data 4
        0x434f8000    # 207.5f
        0x42920000    # 73.0f
    .end array-data

    :array_17
    .array-data 4
        0x43d5c000    # 427.5f
        0x42920000    # 73.0f
    .end array-data

    :array_18
    .array-data 4
        0x4408c000    # 547.0f
        0x43408000    # 192.5f
    .end array-data

    :array_19
    .array-data 4
        0x43d5c000    # 427.5f
        0x439c0000    # 312.0f
    .end array-data

    .line 58
    :array_1a
    .array-data 4
        0x434f8000    # 207.5f
        0x439c0000    # 312.0f
    .end array-data

    :array_1b
    .array-data 4
        0x430d8000    # 141.5f
        0x439c0000    # 312.0f
    .end array-data

    :array_1c
    .array-data 4
        0x42b00000    # 88.0f
        0x42fd0000    # 126.5f
    .end array-data

    :array_1d
    .array-data 4
        0x43d5c000    # 427.5f
        0x42920000    # 73.0f
    .end array-data

    :array_1e
    .array-data 4
        0x43f6c000    # 493.5f
        0x42920000    # 73.0f
    .end array-data

    :array_1f
    .array-data 4
        0x4408c000    # 547.0f
        0x4383f333    # 263.9f
    .end array-data

    .line 61
    :array_20
    .array-data 4
        0x434f8000    # 207.5f
        0x439c0000    # 312.0f
    .end array-data

    :array_21
    .array-data 4
        0x42b00000    # 88.0f
        0x4381b333    # 259.4f
    .end array-data

    :array_22
    .array-data 4
        0x430d8000    # 141.5f
        0x42920000    # 73.0f
    .end array-data

    :array_23
    .array-data 4
        0x43d5c000    # 427.5f
        0x42920000    # 73.0f
    .end array-data

    :array_24
    .array-data 4
        0x4408c000    # 547.0f
        0x42fd0000    # 126.5f
    .end array-data

    :array_25
    .array-data 4
        0x43f6c000    # 493.5f
        0x439c0000    # 312.0f
    .end array-data

    .line 66
    :array_26
    .array-data 4
        0x44106000    # 577.5f
        0x43408000    # 192.5f
    .end array-data

    :array_27
    .array-data 4
        0x43dac000    # 437.5f
        0x423e0000    # 47.5f
    .end array-data

    :array_28
    .array-data 4
        0x43458000    # 197.5f
        0x423e0000    # 47.5f
    .end array-data

    :array_29
    .array-data 4
        0x42660000    # 57.5f
        0x43408000    # 192.5f
    .end array-data

    :array_2a
    .array-data 4
        0x43458000    # 197.5f
        0x43a8c000    # 337.5f
    .end array-data

    :array_2b
    .array-data 4
        0x43dac000    # 437.5f
        0x43a8c000    # 337.5f
    .end array-data

    :array_2c
    .array-data 4
        0x44106000    # 577.5f
        0x43408000    # 192.5f
    .end array-data

    .line 69
    :array_2d
    .array-data 4
        0x44106000    # 577.5f
        0x42e66666    # 115.2f
    .end array-data

    :array_2e
    .array-data 4
        0x43458000    # 197.5f
        0x423e0000    # 47.5f
    .end array-data

    :array_2f
    .array-data 4
        0x42f06666    # 120.2f
        0x423e0000    # 47.5f
    .end array-data

    :array_30
    .array-data 4
        0x42660000    # 57.5f
        0x43874000    # 270.5f
    .end array-data

    :array_31
    .array-data 4
        0x43458000    # 197.5f
        0x43a8c000    # 337.5f
    .end array-data

    :array_32
    .array-data 4
        0x44014ccd    # 517.2f
        0x43a8c000    # 337.5f
    .end array-data

    .line 72
    :array_33
    .array-data 4
        0x4400b99a    # 514.9f
        0x423e0000    # 47.5f
    .end array-data

    :array_34
    .array-data 4
        0x43458000    # 197.5f
        0x423e0000    # 47.5f
    .end array-data

    :array_35
    .array-data 4
        0x42660000    # 57.5f
        0x42ed0000    # 118.5f
    .end array-data

    :array_36
    .array-data 4
        0x42f06666    # 120.2f
        0x43a8c000    # 337.5f
    .end array-data

    :array_37
    .array-data 4
        0x43bcc000    # 377.5f
        0x43a8c000    # 337.5f
    .end array-data

    :array_38
    .array-data 4
        0x44106000    # 577.5f
        0x4386c000    # 269.5f
    .end array-data

    .line 78
    :array_39
    .array-data 4
        0x43dc0000    # 440.0f
        0x43b40000    # 360.0f
    .end array-data

    :array_3a
    .array-data 4
        0x43430000    # 195.0f
        0x43b40000    # 360.0f
    .end array-data

    :array_3b
    .array-data 4
        0x41dc0000    # 27.5f
        0x43408000    # 192.5f
    .end array-data

    :array_3c
    .array-data 4
        0x43430000    # 195.0f
        0x41c80000    # 25.0f
    .end array-data

    :array_3d
    .array-data 4
        0x43dc0000    # 440.0f
        0x41c80000    # 25.0f
    .end array-data

    :array_3e
    .array-data 4
        0x4417e000    # 607.5f
        0x43408000    # 192.5f
    .end array-data

    :array_3f
    .array-data 4
        0x43dc0000    # 440.0f
        0x43b40000    # 360.0f
    .end array-data

    .line 81
    :array_40
    .array-data 4
        0x43430000    # 195.0f
        0x43b40000    # 360.0f
    .end array-data

    :array_41
    .array-data 4
        0x42cbcccd    # 101.9f
        0x43b40000    # 360.0f
    .end array-data

    :array_42
    .array-data 4
        0x41dc0000    # 27.5f
        0x42c80000    # 100.0f
    .end array-data

    :array_43
    .array-data 4
        0x43dc0000    # 440.0f
        0x41c80000    # 25.0f
    .end array-data

    :array_44
    .array-data 4
        0x44052000    # 532.5f
        0x41c80000    # 25.0f
    .end array-data

    :array_45
    .array-data 4
        0x4417e000    # 607.5f
        0x43904000    # 288.5f
    .end array-data

    .line 84
    :array_46
    .array-data 4
        0x43430000    # 195.0f
        0x43b40000    # 360.0f
    .end array-data

    :array_47
    .array-data 4
        0x41dc0000    # 27.5f
        0x43940ccd    # 296.1f
    .end array-data

    :array_48
    .array-data 4
        0x42cd0000    # 102.5f
        0x41c80000    # 25.0f
    .end array-data

    :array_49
    .array-data 4
        0x43dc0000    # 440.0f
        0x41c80000    # 25.0f
    .end array-data

    :array_4a
    .array-data 4
        0x4417e000    # 607.5f
        0x42c80000    # 100.0f
    .end array-data

    :array_4b
    .array-data 4
        0x4405999a    # 534.4f
        0x43b40000    # 360.0f
    .end array-data

    .line 88
    :array_4c
    .array-data 4
        0x43df3333    # 446.4f
        0x43c08000    # 385.0f
    .end array-data

    :array_4d
    .array-data 4
        0x433c999a    # 188.6f
        0x43c08000    # 385.0f
    .end array-data

    :array_4e
    .array-data 4
        0x0
        0x43408000    # 192.5f
    .end array-data

    :array_4f
    .array-data 4
        0x433c999a    # 188.6f
        0x0
    .end array-data

    :array_50
    .array-data 4
        0x43df3333    # 446.4f
        0x0
    .end array-data

    :array_51
    .array-data 4
        0x441ec666    # 635.1f
        0x43408000    # 192.5f
    .end array-data

    :array_52
    .array-data 4
        0x43df3333    # 446.4f
        0x43c08000    # 385.0f
    .end array-data

    .line 91
    :array_53
    .array-data 4
        0x433c999a    # 188.6f
        0x43c08000    # 385.0f
    .end array-data

    :array_54
    .array-data 4
        0x42a90000    # 84.5f
        0x43c08000    # 385.0f
    .end array-data

    :array_55
    .array-data 4
        0x0
        0x42b70000    # 91.5f
    .end array-data

    :array_56
    .array-data 4
        0x43df3333    # 446.4f
        0x0
    .end array-data

    :array_57
    .array-data 4
        0x4409a666    # 550.6f
        0x0
    .end array-data

    :array_58
    .array-data 4
        0x441ec666    # 635.1f
        0x43944000    # 296.5f
    .end array-data

    .line 94
    :array_59
    .array-data 4
        0x433c999a    # 188.6f
        0x43c08000    # 385.0f
    .end array-data

    :array_5a
    .array-data 4
        0x0
        0x43974000    # 302.5f
    .end array-data

    :array_5b
    .array-data 4
        0x42a90000    # 84.5f
        0x0
    .end array-data

    :array_5c
    .array-data 4
        0x43df3333    # 446.4f
        0x0
    .end array-data

    :array_5d
    .array-data 4
        0x441ec666    # 635.1f
        0x42b30000    # 89.5f
    .end array-data

    :array_5e
    .array-data 4
        0x4409a666    # 550.6f
        0x43c08000    # 385.0f
    .end array-data
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/customviews/ListeningView;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/ListeningView;

    .prologue
    .line 21
    iget v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCount:I

    return v0
.end method

.method static synthetic access$008(Lcom/vlingo/midas/gui/customviews/ListeningView;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/ListeningView;

    .prologue
    .line 21
    iget v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/customviews/ListeningView;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/ListeningView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/customviews/ListeningView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/ListeningView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRmsHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 55
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 114
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/customviews/ListeningView;->mContext:Landroid/content/Context;

    .line 115
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPaint:Landroid/graphics/Paint;

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 117
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 121
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPath:Landroid/graphics/Path;

    .line 123
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/customviews/ListeningView;->getRotation()F

    move-result v15

    .line 126
    .local v15, "rot":F
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/customviews/ListeningView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mDensityRatio:F

    .line 127
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mDensityRatio:F

    const v3, 0x3f95ceb2

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mDensityRatio:F

    .line 131
    :cond_0
    const/4 v2, 0x3

    new-array v8, v2, [F

    fill-array-data v8, :array_0

    .line 132
    .local v8, "xscales":[F
    const/4 v2, 0x3

    new-array v9, v2, [F

    fill-array-data v9, :array_1

    .line 133
    .local v9, "yscales":[F
    const/4 v2, 0x2

    new-array v13, v2, [I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0x10

    aput v3, v13, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xe

    aput v3, v13, v2

    .line 134
    .local v13, "frames":[I
    new-instance v2, Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect0Points:[[F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect0Anchors1:[[F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect0Anchors2:[[F

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v10, 0x33

    const/16 v11, 0xff

    const/16 v12, 0xff

    const/16 v14, 0xff

    invoke-static {v10, v11, v12, v14}, Landroid/graphics/Color;->argb(IIII)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPaint:Landroid/graphics/Paint;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mDensityRatio:F

    move/from16 v16, v0

    invoke-direct/range {v2 .. v16}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;-><init>([[F[[F[[FFF[F[FILandroid/graphics/Path;Landroid/graphics/Paint;[IIFF)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    .line 143
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v22, v0

    fill-array-data v22, :array_2

    .line 144
    .local v22, "xscales2":[F
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v23, v0

    fill-array-data v23, :array_3

    .line 145
    .local v23, "yscales2":[F
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 v27, v0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xf

    aput v3, v27, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xe

    aput v3, v27, v2

    .line 146
    .local v27, "frames2":[I
    new-instance v16, Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect1Points:[[F

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect1Anchors1:[[F

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect1Anchors2:[[F

    move-object/from16 v19, v0

    const/high16 v20, 0x41dc0000    # 27.5f

    const/high16 v21, 0x41c80000    # 25.0f

    const/16 v2, 0x4c

    const/16 v3, 0xff

    const/16 v4, 0xff

    const/16 v5, 0xff

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPath:Landroid/graphics/Path;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v26, v0

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mDensityRatio:F

    move/from16 v30, v0

    move/from16 v29, v15

    invoke-direct/range {v16 .. v30}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;-><init>([[F[[F[[FFF[F[FILandroid/graphics/Path;Landroid/graphics/Paint;[IIFF)V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    .line 155
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v34, v0

    fill-array-data v34, :array_4

    .line 156
    .local v34, "xscales3":[F
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v35, v0

    fill-array-data v35, :array_5

    .line 157
    .local v35, "yscales3":[F
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 v39, v0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xe

    aput v3, v39, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xe

    aput v3, v39, v2

    .line 158
    .local v39, "frames3":[I
    new-instance v28, Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect2Points:[[F

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect2Anchors1:[[F

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect2Anchors2:[[F

    move-object/from16 v31, v0

    const/high16 v32, 0x42660000    # 57.5f

    const/high16 v33, 0x423e0000    # 47.5f

    const/16 v2, 0x66

    const/16 v3, 0xff

    const/16 v4, 0xff

    const/16 v5, 0xff

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPath:Landroid/graphics/Path;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v38, v0

    const/16 v40, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mDensityRatio:F

    move/from16 v42, v0

    move/from16 v41, v15

    invoke-direct/range {v28 .. v42}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;-><init>([[F[[F[[FFF[F[FILandroid/graphics/Path;Landroid/graphics/Paint;[IIFF)V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    .line 168
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v46, v0

    fill-array-data v46, :array_6

    .line 169
    .local v46, "xscales4":[F
    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v47, v0

    fill-array-data v47, :array_7

    .line 170
    .local v47, "yscales4":[F
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 v51, v0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xd

    aput v3, v51, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->frameMultiplier:I

    mul-int/lit8 v3, v3, 0xd

    aput v3, v51, v2

    .line 171
    .local v51, "frames4":[I
    new-instance v40, Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect3Points:[[F

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect3Anchors1:[[F

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mRect3Anchors2:[[F

    move-object/from16 v43, v0

    const/high16 v44, 0x42b00000    # 88.0f

    const/high16 v45, 0x42920000    # 73.0f

    const/16 v2, 0x99

    const/16 v3, 0xff

    const/16 v4, 0xff

    const/16 v5, 0xff

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v48

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPath:Landroid/graphics/Path;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v50, v0

    const/16 v52, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mDensityRatio:F

    move/from16 v54, v0

    move/from16 v53, v15

    invoke-direct/range {v40 .. v54}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;-><init>([[F[[F[[FFF[F[FILandroid/graphics/Path;Landroid/graphics/Paint;[IIFF)V

    move-object/from16 v0, v40

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    .line 180
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    const/16 v3, 0xff

    const/16 v4, 0x1a

    const/16 v5, 0x36

    const/16 v6, 0x3f

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->setColor(I)V

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    const/16 v3, 0xff

    const/16 v4, 0x31

    const/16 v5, 0x4a

    const/16 v6, 0x53

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->setColor(I)V

    .line 183
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    const/16 v3, 0xff

    const/16 v4, 0x5a

    const/16 v5, 0x6e

    const/16 v6, 0x75

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->setColor(I)V

    .line 184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    const/16 v3, 0xff

    const/16 v4, 0x7b

    const/16 v5, 0x8b

    const/16 v6, 0x91

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->setColor(I)V

    .line 187
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setDimension()V

    .line 189
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->setTag(Ljava/lang/Object;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->setTag(Ljava/lang/Object;)V

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->setTag(Ljava/lang/Object;)V

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->setTag(Ljava/lang/Object;)V

    .line 194
    return-void

    .line 131
    nop

    :array_0
    .array-data 4
        0x3ed1eb85    # 0.41f
        0x3f800000    # 1.0f
        0x3e9eb852    # 0.31f
    .end array-data

    .line 132
    :array_1
    .array-data 4
        0x3ebd70a4    # 0.37f
        0x3f800000    # 1.0f
        0x3e9eb852    # 0.31f
    .end array-data

    .line 143
    :array_2
    .array-data 4
        0x3f11eb85    # 0.57f
        0x3f800000    # 1.0f
        0x3eb33333    # 0.35f
    .end array-data

    .line 144
    :array_3
    .array-data 4
        0x3f051eb8    # 0.52f
        0x3f800000    # 1.0f
        0x3eb33333    # 0.35f
    .end array-data

    .line 155
    :array_4
    .array-data 4
        0x3f2b851f    # 0.67f
        0x3f800000    # 1.0f
        0x3ed70a3d    # 0.42f
    .end array-data

    .line 156
    :array_5
    .array-data 4
        0x3f1eb852    # 0.62f
        0x3f800000    # 1.0f
        0x3ed70a3d    # 0.42f
    .end array-data

    .line 168
    :array_6
    .array-data 4
        0x3f400000    # 0.75f
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data

    .line 169
    :array_7
    .array-data 4
        0x3f333333    # 0.7f
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data
.end method

.method private restart()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->restart()V

    .line 208
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->restart()V

    .line 209
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->restart()V

    .line 210
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->restart()V

    .line 213
    :cond_0
    return-void
.end method

.method private setDimension()V
    .locals 3

    .prologue
    .line 241
    const/4 v1, 0x0

    .local v1, "maxWidth":F
    const/4 v0, 0x0

    .line 243
    .local v0, "maxHeight":F
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getWidth()F

    move-result v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getWidth()F

    move-result v1

    .line 244
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getWidth()F

    move-result v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getWidth()F

    move-result v1

    .line 245
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getWidth()F

    move-result v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getWidth()F

    move-result v1

    .line 246
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getWidth()F

    move-result v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getWidth()F

    move-result v1

    .line 249
    :cond_3
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getHeight()F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_4

    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getHeight()F

    move-result v0

    .line 250
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getHeight()F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_5

    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getHeight()F

    move-result v0

    .line 251
    :cond_5
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getHeight()F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_6

    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getHeight()F

    move-result v0

    .line 252
    :cond_6
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getHeight()F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_7

    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->getHeight()F

    move-result v0

    .line 255
    :cond_7
    float-to-int v2, v1

    iput v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mWidth:I

    .line 256
    float-to-int v2, v0

    iput v2, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mHeight:I

    .line 257
    return-void
.end method


# virtual methods
.method public cancelTimer()V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 314
    :cond_0
    return-void
.end method

.method public initListeningAnimForTutorial()V
    .locals 6

    .prologue
    .line 284
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCount:I

    .line 285
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->timer:Ljava/util/Timer;

    .line 286
    new-instance v0, Lcom/vlingo/midas/gui/customviews/ListeningView$MyTimerTask;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/customviews/ListeningView$MyTimerTask;-><init>(Lcom/vlingo/midas/gui/customviews/ListeningView;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->myTimerTask:Lcom/vlingo/midas/gui/customviews/ListeningView$MyTimerTask;

    .line 288
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->myTimerTask:Lcom/vlingo/midas/gui/customviews/ListeningView$MyTimerTask;

    const-wide/16 v2, 0x190

    const-wide/16 v4, 0x82

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 289
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 219
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mContext:Landroid/content/Context;

    .line 221
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->draw(Landroid/graphics/Canvas;)V

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->draw(Landroid/graphics/Canvas;)V

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->draw(Landroid/graphics/Canvas;)V

    .line 229
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->draw(Landroid/graphics/Canvas;)V

    .line 232
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/ListeningView;->invalidate()V

    .line 233
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 237
    iget v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mWidth:I

    iget v1, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setMeasuredDimension(II)V

    .line 238
    return-void
.end method

.method public setItsPosition()V
    .locals 5

    .prologue
    .line 261
    const/4 v0, 0x0

    .line 262
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/ListeningView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 263
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    const-wide v1, -0x3feeae147ae147aeL    # -4.33

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/ListeningView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    float-to-double v3, v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 264
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 265
    return-void
.end method

.method public update(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect0:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->update(I)V

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect1:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->update(I)V

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect2:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->update(I)V

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ListeningView;->mCueRect3:Lcom/vlingo/midas/gui/customviews/CueRoundRect;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/CueRoundRect;->update(I)V

    .line 203
    :cond_3
    return-void
.end method
