.class public Lcom/vlingo/midas/gui/WidgetBuilder;
.super Ljava/lang/Object;
.source "WidgetBuilder.java"

# interfaces
.implements Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;


# static fields
.field protected static final widgets:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected context:Landroid/content/Context;

.field protected parentActivity:Lcom/vlingo/midas/gui/ConversationActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/vlingo/midas/gui/WidgetBuilder;->widgets:Ljava/util/EnumMap;

    .line 30
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AnswerQuestion:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_answer_question:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 31
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowButton:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_button:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 32
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ContactDetail:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_contact_detail:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 33
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeMessage:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_message:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 34
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadback:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_messagereadback:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 35
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MessageReadbackBodyHidden:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_messagereadbackbodyhidden:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 36
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageDisplay:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_multiple_message:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 37
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageShowWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_multiple_message_showunread:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 38
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_schedule_delete:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 39
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_schedule_edit:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 40
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_contact_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 41
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowContactTypeChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_phone_type_select:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 42
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEventChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_schedule_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 43
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_schedule_single:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 44
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ScheduleEvent:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_schedule:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 45
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_task:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 46
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_task:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 47
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditTask:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_task:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 48
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowTaskChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_task_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 49
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowAlarmChoices:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_alarm_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 56
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->AddressBook:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_address_book:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 57
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Map:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_map:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 58
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->Memo:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_memo:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 59
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MemoList:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_memo_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 60
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->OpenApp:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_application_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 61
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_alarm_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 62
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DeleteAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_alarm_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 63
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->EditAlarm:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_alarm_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 64
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_timer:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 67
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowChatbotWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_chatbot:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 68
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->DriveNewsWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_normal_news:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 69
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWCISWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_help_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 70
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowClock:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_clock:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 71
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowLocalSearchWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_local_search:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 72
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowNaverWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_naver:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 75
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_weather_todays_plus_weekly:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 77
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_weather_single_citi:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 78
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCMAWeatherWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->cma_widget_weather_todays_plus_weekly:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 79
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowCMAWeatherDailyWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->cma_widget_weather_single_citi:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 81
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SocialNetworkChoice:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_social_network_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 82
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ComposeSocialStatus:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_compose_social_status:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 83
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowWolframWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_wolfram_alpha:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 84
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->ShowTrueKnowledgeWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_true_knowledge:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 86
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->PlayMusic:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_music_choice:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 87
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_music:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 90
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_multiple_sender_message_readout:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 91
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_multiple_message_readout:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 92
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->WebBrowser:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    sget v1, Lcom/vlingo/midas/R$layout;->widget_web_browser:I

    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WidgetBuilder;->registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V

    .line 93
    return-void
.end method

.method protected constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 1
    .param p1, "parentActivity"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    const/4 v0, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object v0, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->parentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

    .line 104
    iput-object v0, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->context:Landroid/content/Context;

    .line 112
    iput-object p1, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->parentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

    .line 113
    iput-object p1, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->context:Landroid/content/Context;

    .line 114
    return-void
.end method

.method protected constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;Landroid/content/Context;)V
    .locals 1
    .param p1, "parentActivity"    # Lcom/vlingo/midas/gui/ConversationActivity;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object v0, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->parentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

    .line 104
    iput-object v0, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->context:Landroid/content/Context;

    .line 117
    iput-object p1, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->parentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

    .line 118
    iput-object p2, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->context:Landroid/content/Context;

    .line 119
    return-void
.end method

.method private isDrivingModeOffered()Z
    .locals 1

    .prologue
    .line 140
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v0

    return v0
.end method

.method protected static registerWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;I)V
    .locals 2
    .param p0, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p1, "resId"    # I

    .prologue
    .line 27
    sget-object v0, Lcom/vlingo/midas/gui/WidgetBuilder;->widgets:Ljava/util/EnumMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    return-void
.end method


# virtual methods
.method protected buildWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)Lcom/vlingo/midas/gui/Widget;
    .locals 7
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p2, "decorators"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "TT;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")",
            "Lcom/vlingo/midas/gui/Widget",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p3, "object":Ljava/lang/Object;, "TT;"
    const/4 v6, 0x0

    .line 144
    const/4 v2, 0x0

    .line 146
    .local v2, "widget":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TT;>;"
    sget-object v3, Lcom/vlingo/midas/gui/WidgetBuilder;->widgets:Ljava/util/EnumMap;

    invoke-virtual {v3, p1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 147
    sget-object v3, Lcom/vlingo/midas/gui/WidgetBuilder;->widgets:Ljava/util/EnumMap;

    invoke-virtual {v3, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 151
    .local v0, "resId":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->context:Landroid/content/Context;

    invoke-static {v3, v0, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/Widget;

    .line 152
    .local v1, "w":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TT;>;"
    move-object v2, v1

    .line 168
    :goto_0
    instance-of v3, v2, Lcom/vlingo/midas/gui/Widget;

    if-eqz v3, :cond_0

    .line 169
    invoke-virtual {v2, p3, p2, p1, p4}, Lcom/vlingo/midas/gui/Widget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 170
    :cond_0
    return-object v2

    .line 154
    .end local v0    # "resId":I
    .end local v1    # "w":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TT;>;"
    :cond_1
    iget-object v3, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->context:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Widget \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\' is missing"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 155
    sget v0, Lcom/vlingo/midas/R$layout;->widget_missing:I

    .line 157
    .restart local v0    # "resId":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->context:Landroid/content/Context;

    invoke-static {v3, v0, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/Widget;

    .line 158
    .restart local v1    # "w":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TT;>;"
    move-object v2, v1

    goto :goto_0
.end method

.method public setEavesdropper(Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionEavesdropper;)V
    .locals 0
    .param p1, "eavesdropper"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionEavesdropper;

    .prologue
    .line 181
    return-void
.end method

.method public showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 4
    .param p1, "key"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p2, "decorators"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "TT;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 124
    .local p3, "object":Ljava/lang/Object;, "TT;"
    const-string/jumbo v1, "getFieldID"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "drivingfragment-showWidget  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/WidgetBuilder;->buildWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)Lcom/vlingo/midas/gui/Widget;

    move-result-object v0

    .line 127
    .local v0, "widget":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TT;>;"
    iget-object v1, p0, Lcom/vlingo/midas/gui/WidgetBuilder;->parentActivity:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->addWidget(Lcom/vlingo/midas/gui/Widget;)V

    .line 137
    return-void
.end method
