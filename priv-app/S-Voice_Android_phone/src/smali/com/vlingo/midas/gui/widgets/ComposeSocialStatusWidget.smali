.class public Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "ComposeSocialStatusWidget.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/midas/dialogmanager/types/SocialType;",
        ">;"
    }
.end annotation


# instance fields
.field private cancelBtn:Landroid/widget/Button;

.field compose_social_body_container:Landroid/widget/LinearLayout;

.field compose_social_button_container:Landroid/widget/LinearLayout;

.field contactImage:Landroid/widget/ImageView;

.field private contactName:Landroid/widget/TextView;

.field private context:Landroid/content/Context;

.field facebookIcon:Landroid/widget/ImageView;

.field private isClicked:Z

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field messagButtonDivider:Landroid/widget/ImageView;

.field private msgBody:Landroid/widget/EditText;

.field thumbImg:Landroid/graphics/Bitmap;

.field twitterIcon:Landroid/widget/ImageView;

.field private updateBtn:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->isClicked:Z

    .line 47
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->context:Landroid/content/Context;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->msgBody:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->isClicked:Z

    return p1
.end method


# virtual methods
.method public initialize(Lcom/vlingo/midas/dialogmanager/types/SocialType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 8
    .param p1, "socialType"    # Lcom/vlingo/midas/dialogmanager/types/SocialType;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 120
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 121
    const-string/jumbo v1, ""

    .line 122
    .local v1, "loginName":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 123
    .local v2, "status":Ljava/lang/String;
    const/4 v3, 0x0

    .line 124
    .local v3, "type":Lcom/vlingo/midas/social/api/SocialNetworkType;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 127
    invoke-virtual {p1}, Lcom/vlingo/midas/dialogmanager/types/SocialType;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 128
    invoke-virtual {p1}, Lcom/vlingo/midas/dialogmanager/types/SocialType;->getSocialNetwork()Lcom/vlingo/midas/social/api/SocialNetworkType;

    move-result-object v3

    .line 130
    sget-object v4, Lcom/vlingo/midas/social/api/SocialNetworkType;->FACEBOOK:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne v3, v4, :cond_6

    .line 132
    const-string/jumbo v4, "facebook_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->thumbImg:Landroid/graphics/Bitmap;

    .line 133
    const-string/jumbo v4, "facebook_account_name"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->facebookIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->twitterIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 176
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->thumbImg:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_1

    .line 177
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->contactImage:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->thumbImg:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 180
    :cond_1
    if-eqz v1, :cond_2

    .line 181
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->contactName:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    :cond_2
    if-eqz v2, :cond_3

    .line 186
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->msgBody:Landroid/widget/EditText;

    invoke-virtual {v4, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 188
    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string/jumbo v4, "en-US"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 193
    :cond_3
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->updateBtn:Landroid/widget/Button;

    if-eqz v4, :cond_5

    .line 194
    :cond_4
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->updateBtn:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 196
    :cond_5
    return-void

    .line 137
    :cond_6
    sget-object v4, Lcom/vlingo/midas/social/api/SocialNetworkType;->TWITTER:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne v3, v4, :cond_7

    .line 138
    const-string/jumbo v4, "twitter_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->thumbImg:Landroid/graphics/Bitmap;

    .line 139
    const-string/jumbo v4, "twitter_account_name"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 140
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->twitterIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 141
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->facebookIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 143
    :cond_7
    sget-object v4, Lcom/vlingo/midas/social/api/SocialNetworkType;->WEIBO:Lcom/vlingo/midas/social/api/SocialNetworkType;

    if-ne v3, v4, :cond_8

    .line 144
    const-string/jumbo v4, "weibo_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->thumbImg:Landroid/graphics/Bitmap;

    .line 145
    const-string/jumbo v4, "weibo_account_name"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 146
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->facebookIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->twitterIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 150
    :cond_8
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 151
    const-string/jumbo v4, "weibo_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 152
    const-string/jumbo v4, "weibo_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->thumbImg:Landroid/graphics/Bitmap;

    .line 153
    const-string/jumbo v4, "weibo_account_name"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 154
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->facebookIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->twitterIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 159
    :cond_9
    const-string/jumbo v4, "facebook_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 160
    const-string/jumbo v4, "facebook_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->thumbImg:Landroid/graphics/Bitmap;

    .line 164
    :cond_a
    :goto_1
    const-string/jumbo v4, "facebook_account_name"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_d

    .line 165
    const-string/jumbo v4, "facebook_account_name"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 170
    :cond_b
    :goto_2
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->twitterIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 171
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->facebookIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 161
    :cond_c
    const-string/jumbo v4, "twitter_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 162
    const-string/jumbo v4, "twitter_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->thumbImg:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 167
    :cond_d
    const-string/jumbo v4, "twitter_account_name"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 168
    const-string/jumbo v4, "twitter_account_name"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 31
    check-cast p1, Lcom/vlingo/midas/dialogmanager/types/SocialType;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->initialize(Lcom/vlingo/midas/dialogmanager/types/SocialType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->isClicked:Z

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x1

    return v0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 53
    sget v0, Lcom/vlingo/midas/R$id;->contact_name:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->contactName:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/vlingo/midas/R$id;->msg_body:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->msgBody:Landroid/widget/EditText;

    .line 55
    sget v0, Lcom/vlingo/midas/R$id;->senderImage:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->contactImage:Landroid/widget/ImageView;

    .line 56
    sget v0, Lcom/vlingo/midas/R$id;->facebookIcon:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->facebookIcon:Landroid/widget/ImageView;

    .line 57
    sget v0, Lcom/vlingo/midas/R$id;->twitterIcon:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->twitterIcon:Landroid/widget/ImageView;

    .line 59
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->facebookIcon:Landroid/widget/ImageView;

    sget v1, Lcom/vlingo/midas/R$drawable;->voice_talk_social_update_weibo:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 62
    :cond_0
    sget v0, Lcom/vlingo/midas/R$id;->btn_divider:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->messagButtonDivider:Landroid/widget/ImageView;

    .line 63
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->messagButtonDivider:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 67
    :cond_1
    sget v0, Lcom/vlingo/midas/R$id;->compose_social_button_container:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->compose_social_button_container:Landroid/widget/LinearLayout;

    .line 68
    sget v0, Lcom/vlingo/midas/R$id;->widget_compose_social_status_body_container:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->compose_social_body_container:Landroid/widget/LinearLayout;

    .line 70
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->msgBody:Landroid/widget/EditText;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 81
    sget v0, Lcom/vlingo/midas/R$id;->btn_cancel_social:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->cancelBtn:Landroid/widget/Button;

    .line 82
    sget v0, Lcom/vlingo/midas/R$id;->updateButton:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->updateBtn:Landroid/widget/Button;

    .line 83
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->updateBtn:Landroid/widget/Button;

    if-eqz v0, :cond_2

    .line 84
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->updateBtn:Landroid/widget/Button;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$2;-><init>(Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->cancelBtn:Landroid/widget/Button;

    if-eqz v0, :cond_3

    .line 101
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->cancelBtn:Landroid/widget/Button;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$3;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$3;-><init>(Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->msgBody:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 116
    return-void
.end method

.method public retire()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 213
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->retire()V

    .line 215
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->updateBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->updateBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->cancelBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->compose_social_button_container:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 222
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->compose_social_button_container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 224
    :cond_2
    return-void
.end method
