.class public Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "NaverMovieWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/midas/naver/NaverAdaptor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private btnBook:Landroid/widget/Button;

.field private btnBooking:Landroid/widget/Button;

.field private btnDetaillink:Landroid/widget/Button;

.field private btnMovieInfo:Landroid/widget/Button;

.field private btnSameNameMovies:Landroid/widget/Button;

.field item:Lcom/vlingo/midas/naver/NaverMovieItem;

.field private mActors:Landroid/widget/TextView;

.field private final mContext:Landroid/content/Context;

.field private mDirectors:Landroid/widget/TextView;

.field private mGenre:Landroid/widget/TextView;

.field private mGrade:Landroid/widget/TextView;

.field private mHandler:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;

.field mMoviePoster:Landroid/widget/ImageView;

.field mMovieTitle:Landroid/widget/TextView;

.field private mOpenDate:Landroid/widget/TextView;

.field private mPosterImage:Landroid/graphics/Bitmap;

.field private mRatings:Landroid/widget/TextView;

.field mRatingsView:Landroid/widget/RatingBar;

.field private mRunningTime:Landroid/widget/TextView;

.field private mhandler:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field private wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 130
    new-instance v0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;-><init>(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;

    .line 58
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mContext:Landroid/content/Context;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mPosterImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mPosterImage:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;)Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;

    return-object v0
.end method


# virtual methods
.method public initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 6
    .param p1, "object"    # Lcom/vlingo/midas/naver/NaverAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const/16 v5, 0x8

    .line 137
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    .line 138
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getVVSActionHandlerListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mhandler:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 139
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v0

    .line 140
    .local v0, "result":Lcom/vlingo/midas/naver/NaverXMLParser;
    new-instance v2, Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->itemList:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Hashtable;

    invoke-direct {v2, v3, v1}, Lcom/vlingo/midas/naver/NaverMovieItem;-><init>(Landroid/content/Context;Ljava/util/Hashtable;)V

    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    .line 142
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mMovieTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    sget v1, Lcom/vlingo/midas/R$id;->image_Movie_Playing:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->isOpenSoon()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 146
    sget v1, Lcom/vlingo/midas/R$id;->image_Movie_Playing:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mContext:Landroid/content/Context;

    sget v3, Lcom/vlingo/midas/R$string;->movie_soon:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mRatingsView:Landroid/widget/RatingBar;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getRating()Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/high16 v3, 0x41200000    # 10.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mRatingsView:Landroid/widget/RatingBar;

    invoke-virtual {v3}, Landroid/widget/RatingBar;->getNumStars()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/RatingBar;->setRating(F)V

    .line 153
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mRatings:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getRating()Ljava/lang/Float;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getActors()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 155
    sget v1, Lcom/vlingo/midas/R$id;->container_Actors:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 158
    :goto_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getDirectors()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 159
    sget v1, Lcom/vlingo/midas/R$id;->container_Director:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 163
    :goto_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getOpenDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_5

    .line 164
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mOpenDate:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 165
    sget v1, Lcom/vlingo/midas/R$id;->view_Movie_div2:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 168
    :goto_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getGrade()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 169
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mGrade:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    sget v1, Lcom/vlingo/midas/R$id;->container_Grade:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 173
    :goto_4
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getGenre()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 174
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mGenre:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 175
    sget v1, Lcom/vlingo/midas/R$id;->view_Movie_div1:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 179
    :goto_5
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getRunningTime()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 180
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mRunningTime:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    sget v1, Lcom/vlingo/midas/R$id;->view_Movie_div1:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 185
    :goto_6
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 211
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 212
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getDetailUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 219
    :goto_7
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getBookingUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 226
    :goto_8
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getSameNameUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 247
    :cond_1
    :goto_9
    return-void

    .line 148
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->isNowShowing()Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 149
    sget v1, Lcom/vlingo/midas/R$id;->image_Movie_Playing:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 157
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mActors:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getActors()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 161
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mDirectors:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getDirectors()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 167
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mOpenDate:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getOpenDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 172
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mGrade:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getGrade()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 177
    :cond_7
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mGenre:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getGenre()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 183
    :cond_8
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mRunningTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverMovieItem;->getRunningTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 215
    :cond_9
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnDetaillink:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 216
    sget v1, Lcom/vlingo/midas/R$id;->movie_button_divider1:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 217
    sget v1, Lcom/vlingo/midas/R$id;->movie_button_divider2:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    .line 222
    :cond_a
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnBooking:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 224
    sget v1, Lcom/vlingo/midas/R$id;->movie_button_divider2:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_8

    .line 229
    :cond_b
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnSameNameMovies:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 230
    sget v1, Lcom/vlingo/midas/R$id;->movie_button_divider2:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 231
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getBookingUrl()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_c

    .line 232
    sget v1, Lcom/vlingo/midas/R$id;->movie_button_divider1:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_9

    .line 234
    :cond_c
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getDetailUrl()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 235
    sget v1, Lcom/vlingo/midas/R$id;->movie_button_divider1:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_9

    .line 239
    :cond_d
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getDetailUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 241
    :goto_a
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getBookingUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 243
    :goto_b
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getSameNameUrl()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 244
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnSameNameMovies:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_9

    .line 240
    :cond_e
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnDetaillink:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_a

    .line 242
    :cond_f
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnBooking:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_b
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 31
    check-cast p1, Lcom/vlingo/midas/naver/NaverAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 64
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 65
    .local v0, "sIntent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnDetaillink:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getDetailUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 67
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getDetailUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 77
    :goto_0
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 81
    :cond_0
    return-void

    .line 68
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnBooking:Landroid/widget/Button;

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getBookingUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 69
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getBookingUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    .line 73
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnSameNameMovies:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getSameNameUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->item:Lcom/vlingo/midas/naver/NaverMovieItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem;->getSameNameUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 89
    sget v0, Lcom/vlingo/midas/R$id;->text_Movie_Title:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mMovieTitle:Landroid/widget/TextView;

    .line 90
    sget v0, Lcom/vlingo/midas/R$id;->image_Movie_Poster:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mMoviePoster:Landroid/widget/ImageView;

    .line 91
    sget v0, Lcom/vlingo/midas/R$id;->ratings:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mRatingsView:Landroid/widget/RatingBar;

    .line 92
    sget v0, Lcom/vlingo/midas/R$id;->text_Movie_Rating:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mRatings:Landroid/widget/TextView;

    .line 93
    sget v0, Lcom/vlingo/midas/R$id;->text_Movie_Genre:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mGenre:Landroid/widget/TextView;

    .line 94
    sget v0, Lcom/vlingo/midas/R$id;->text_Movie_OpenDate:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mOpenDate:Landroid/widget/TextView;

    .line 95
    sget v0, Lcom/vlingo/midas/R$id;->text_Movie_Actors:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mActors:Landroid/widget/TextView;

    .line 96
    sget v0, Lcom/vlingo/midas/R$id;->text_Movie_Director:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mDirectors:Landroid/widget/TextView;

    .line 97
    sget v0, Lcom/vlingo/midas/R$id;->text_Movie_RunningTime:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mRunningTime:Landroid/widget/TextView;

    .line 98
    sget v0, Lcom/vlingo/midas/R$id;->text_Movie_Grade:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mGrade:Landroid/widget/TextView;

    .line 101
    sget v0, Lcom/vlingo/midas/R$id;->btn_Movie_Detaillink:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnDetaillink:Landroid/widget/Button;

    .line 102
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnDetaillink:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    sget v0, Lcom/vlingo/midas/R$id;->btn_Movie_Bookinglink:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnBooking:Landroid/widget/Button;

    .line 104
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnBooking:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    sget v0, Lcom/vlingo/midas/R$id;->btn_Movie_Samenameslink:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnSameNameMovies:Landroid/widget/Button;

    .line 106
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->btnSameNameMovies:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    return-void
.end method
