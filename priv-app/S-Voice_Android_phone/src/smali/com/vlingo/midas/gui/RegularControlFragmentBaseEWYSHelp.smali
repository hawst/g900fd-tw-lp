.class public abstract Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;
.super Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
.source "RegularControlFragmentBaseEWYSHelp.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static LOG_TAG:Ljava/lang/String;


# instance fields
.field private DEBUG_UNLOCK_STEP:I

.field private final DEBUG_UNLOCK_TIMEOUT_MS:I

.field private DEBUG_UNLOCK_TIMESTAMP:J

.field private animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

.field private dialogFragment:Lcom/vlingo/midas/gui/ContentFragment;

.field protected fullContainer:Landroid/widget/RelativeLayout;

.field mCloseActivityHandler:Landroid/os/Handler;

.field protected reco_idleBtn:Landroid/widget/ImageView;

.field protected reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

.field protected reco_thinkingBtn:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string/jumbo v0, "RegularControlFragmentBaseEWYSHelp"

    sput-object v0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->DEBUG_UNLOCK_STEP:I

    .line 29
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->DEBUG_UNLOCK_TIMEOUT_MS:I

    .line 30
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->DEBUG_UNLOCK_TIMESTAMP:J

    .line 108
    new-instance v0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp$1;-><init>(Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->mCloseActivityHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method protected disableHelpWidgetButton()V
    .locals 2

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->isActivityCreated()Z

    move-result v1

    if-nez v1, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 234
    .local v0, "fa":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method protected enableHelpWidgetButton()V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->isActivityCreated()Z

    move-result v1

    if-nez v1, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 222
    .local v0, "fa":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method abstract getThninkingBtnOver()I
.end method

.method abstract getThninkingMicAnimationList()I
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 93
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 94
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking:I

    if-eq v0, v1, :cond_0

    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking_land:I

    if-ne v0, v1, :cond_3

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->resetThinkingState()V

    .line 97
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 98
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    if-eq v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    if-ne v1, v2, :cond_2

    .line 101
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 106
    :cond_2
    :goto_0
    return-void

    .line 103
    :cond_3
    sget v1, Lcom/vlingo/midas/R$id;->btn_idle:I

    if-eq v0, v1, :cond_2

    sget v1, Lcom/vlingo/midas/R$id;->btn_idle_land:I

    if-ne v0, v1, :cond_2

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 143
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 148
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->isActivityCreated()Z

    move-result v1

    if-nez v1, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 152
    .local v0, "fa":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v1, :cond_2

    .line 156
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 157
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 160
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 161
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ContentFragment;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->dialogFragment:Lcom/vlingo/midas/gui/ContentFragment;

    .line 44
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->onCreate(Landroid/os/Bundle;)V

    .line 45
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 245
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 248
    invoke-super {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->onDestroy()V

    .line 249
    return-void
.end method

.method public onLanguageChanged()V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->mic_button:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->mic_button:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 195
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->dialogFragment:Lcom/vlingo/midas/gui/ContentFragment;

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->stopSpotting()V

    .line 66
    :cond_0
    invoke-super {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->onPause()V

    .line 67
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->onResume()V

    .line 51
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    sget v1, Lcom/vlingo/midas/R$drawable;->s_voice_mic:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 53
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    sget v1, Lcom/vlingo/midas/R$drawable;->s_voice_standby_bg:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 57
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 71
    invoke-super {p0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->onStop()V

    .line 72
    return-void
.end method

.method public performClickFromDriveControl()Z
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return v0
.end method

.method public removeControlPanel()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->fullContainer:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 88
    return-void
.end method

.method protected resetThinkingState()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getThninkingBtnOver()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 139
    return-void
.end method

.method public setBlueHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 259
    return-void
.end method

.method protected setButtonIdle(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 167
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_thinkingBtn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 168
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 170
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    .line 171
    return-void
.end method

.method protected setButtonListening()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 175
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 178
    return-void
.end method

.method protected setButtonThinking()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_thinkingBtn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 185
    return-void
.end method

.method public setNormalPanel()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->fullContainer:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    return-void
.end method

.method protected setPrompt()V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method protected setThinkingAnimation()V
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->getThninkingMicAnimationList()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 126
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->reco_thinkingBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 127
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 128
    return-void
.end method

.method public showRMSChange(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->isActivityCreated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    :cond_0
    return-void
.end method

.method public showSpectrum([I)V
    .locals 1
    .param p1, "spectrum"    # [I

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->isActivityCreated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    :cond_0
    return-void
.end method
