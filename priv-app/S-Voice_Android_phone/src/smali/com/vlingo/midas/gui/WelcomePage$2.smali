.class Lcom/vlingo/midas/gui/WelcomePage$2;
.super Ljava/lang/Object;
.source "WelcomePage.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/WelcomePage;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/WelcomePage;

.field final synthetic val$placeholder:Landroid/widget/FrameLayout;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/WelcomePage;Landroid/widget/FrameLayout;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/vlingo/midas/gui/WelcomePage$2;->this$0:Lcom/vlingo/midas/gui/WelcomePage;

    iput-object p2, p0, Lcom/vlingo/midas/gui/WelcomePage$2;->val$placeholder:Landroid/widget/FrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "arg0"    # Landroid/media/MediaPlayer;

    .prologue
    .line 182
    const-string/jumbo v0, "WelcomePage"

    const-string/jumbo v1, "Video Prepared to Play"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage$2;->this$0:Lcom/vlingo/midas/gui/WelcomePage;

    # getter for: Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WelcomePage;->access$000(Lcom/vlingo/midas/gui/WelcomePage;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 184
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomePage$2;->this$0:Lcom/vlingo/midas/gui/WelcomePage;

    # getter for: Lcom/vlingo/midas/gui/WelcomePage;->mWelcomeVideo:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WelcomePage;->access$000(Lcom/vlingo/midas/gui/WelcomePage;)Landroid/widget/VideoView;

    move-result-object v0

    new-instance v1, Lcom/vlingo/midas/gui/WelcomePage$2$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/WelcomePage$2$1;-><init>(Lcom/vlingo/midas/gui/WelcomePage$2;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/VideoView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 196
    :cond_0
    return-void
.end method
