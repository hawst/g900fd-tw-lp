.class public Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;
.super Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;
.source "MultipleMessageShowWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$ListAdapterLineItems;
    }
.end annotation


# instance fields
.field private senderSummaries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/messages/MessageSenderSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 28
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 3
    .param p1, "s"    # Ljava/util/List;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 48
    move-object v0, p1

    .line 49
    .local v0, "summaries":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/MessageSenderSummary;>;"
    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;->senderSummaries:Ljava/util/List;

    .line 50
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 51
    if-eqz v0, :cond_0

    .line 52
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;->mMultipleMessages:Landroid/widget/ListView;

    new-instance v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$1;

    invoke-direct {v2, p0, v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageShowWidget;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 62
    :cond_0
    return-void
.end method
