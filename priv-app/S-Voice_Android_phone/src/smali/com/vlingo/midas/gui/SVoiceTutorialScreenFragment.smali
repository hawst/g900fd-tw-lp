.class public Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;
.super Landroid/support/v4/app/Fragment;
.source "SVoiceTutorialScreenFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final IS_FIRST_LAUNCH:Ljava/lang/String; = "IS_FIRST_LAUNCH"

.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"


# instance fields
.field private isAgreeBtnChecked:Z

.field private isDeclineChecked:Z

.field private mButtonLayout:Landroid/widget/LinearLayout;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mDefaultSharedPrefs:Landroid/content/SharedPreferences;

.field private mDisclaimerContent:Landroid/widget/TextView;

.field private mDisclaimerTOSLayout:Landroid/widget/RelativeLayout;

.field private mTOSContent:Landroid/widget/TextView;

.field private mTOSTitle:Landroid/widget/TextView;

.field private mTheme:I

.field private mTitleBar:Landroid/app/ActionBar;

.field private mWayForSVoiceTutorialLayout:Landroid/widget/RelativeLayout;

.field private main_layout:Landroid/widget/RelativeLayout;

.field private nextBtn:Landroid/widget/LinearLayout;

.field private nextBtnImg:Landroid/widget/ImageView;

.field prefForWishesEventAlert:Landroid/content/SharedPreferences;

.field private previousBtn:Landroid/widget/LinearLayout;

.field private rightBtnText:Landroid/widget/TextView;

.field private root_layout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 33
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 34
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->main_layout:Landroid/widget/RelativeLayout;

    .line 35
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDisclaimerTOSLayout:Landroid/widget/RelativeLayout;

    .line 36
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mCheckBox:Landroid/widget/CheckBox;

    .line 38
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mButtonLayout:Landroid/widget/LinearLayout;

    .line 39
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mWayForSVoiceTutorialLayout:Landroid/widget/RelativeLayout;

    .line 42
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->nextBtn:Landroid/widget/LinearLayout;

    .line 43
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->nextBtnImg:Landroid/widget/ImageView;

    .line 44
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->previousBtn:Landroid/widget/LinearLayout;

    .line 45
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->rightBtnText:Landroid/widget/TextView;

    .line 46
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDisclaimerContent:Landroid/widget/TextView;

    .line 47
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mTOSContent:Landroid/widget/TextView;

    .line 48
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mTOSTitle:Landroid/widget/TextView;

    .line 55
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mTitleBar:Landroid/app/ActionBar;

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->isDeclineChecked:Z

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->isAgreeBtnChecked:Z

    .line 58
    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->root_layout:Landroid/widget/RelativeLayout;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;
    .param p1, "x1"    # Landroid/content/SharedPreferences;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    return-object p1
.end method

.method private findViewById(I)Landroid/view/View;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 172
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->root_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 127
    sget v0, Lcom/vlingo/midas/R$id;->main_layout_tutorial:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->main_layout:Landroid/widget/RelativeLayout;

    .line 128
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->main_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_bg_tile:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 129
    sget v0, Lcom/vlingo/midas/R$id;->svoice_tutorial:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mWayForSVoiceTutorialLayout:Landroid/widget/RelativeLayout;

    .line 130
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->root_layout:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->button_layout:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mButtonLayout:Landroid/widget/LinearLayout;

    .line 131
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->root_layout:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->next_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->nextBtn:Landroid/widget/LinearLayout;

    .line 133
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->nextBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->layoutToDisplayIfTOSAccepted()V

    .line 136
    return-void
.end method

.method private layoutToDisplayIfTOSAccepted()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 141
    sget v1, Lcom/vlingo/midas/R$id;->samsung_disclaimer_tos_layout:I

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDisclaimerTOSLayout:Landroid/widget/RelativeLayout;

    .line 142
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->svoiceTutorialScreen()V

    .line 143
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mTitleBar:Landroid/app/ActionBar;

    if-eqz v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mTitleBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->show()V

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->main_layout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 149
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->main_layout:Landroid/widget/RelativeLayout;

    sget v2, Lcom/vlingo/midas/R$color;->help_about_bg_color:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 152
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 153
    sget v1, Lcom/vlingo/midas/R$id;->main_layout_k:I

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 154
    .local v0, "mTutorialScreenK":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 155
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 158
    .end local v0    # "mTutorialScreenK":Landroid/view/View;
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mWayForSVoiceTutorialLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_3

    .line 159
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mWayForSVoiceTutorialLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 165
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mButtonLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_4

    .line 166
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 169
    :cond_4
    return-void
.end method

.method private setForNointroductionScreenOnNxtLaunch()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 82
    const-string/jumbo v1, "showing_notifications"

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 84
    const-string/jumbo v1, "all_notifications_accepted"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 85
    invoke-static {v3}, Lcom/vlingo/midas/iux/IUXManager;->setIUXComplete(Z)V

    .line 86
    invoke-static {v2}, Lcom/vlingo/midas/iux/IUXManager;->setIUXIntroRequired(Z)V

    .line 87
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 88
    .local v0, "o":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;
    invoke-virtual {v0, v3}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->setTutorialCompleted(Z)V

    .line 89
    return-void
.end method

.method private showPreferences()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 69
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 70
    iget-object v2, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 71
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "is_move_to_svoice"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 72
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 73
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->setForNointroductionScreenOnNxtLaunch()V

    .line 75
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 76
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v2, "is_start_option_menu"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 77
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->startActivity(Landroid/content/Intent;)V

    .line 78
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 79
    return-void
.end method

.method private svoiceTutorialScreen()V
    .locals 2

    .prologue
    .line 192
    sget v1, Lcom/vlingo/midas/R$id;->move_to_tutorial:I

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 193
    .local v0, "moveToTutorial":Landroid/widget/Button;
    new-instance v1, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment$1;-><init>(Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->init()V

    .line 94
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 95
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 64
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->showPreferences()V

    .line 66
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 218
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->checkVoiceWakeupFeature()V

    .line 219
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 220
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 221
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "is_move_to_svoice"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 222
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 223
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->setForNointroductionScreenOnNxtLaunch()V

    .line 224
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/iux/IUXManager;->finishIUX(Landroid/app/Activity;)V

    .line 225
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 100
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 101
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    sget v2, Lcom/vlingo/midas/R$id;->left_container:I

    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 103
    .local v0, "leftContainer":Landroid/widget/RelativeLayout;
    sget v2, Lcom/vlingo/midas/R$id;->right_container:I

    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 104
    .local v1, "rightContainer":Landroid/widget/RelativeLayout;
    if-eqz p1, :cond_0

    .line 105
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 106
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 107
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 114
    .end local v0    # "leftContainer":Landroid/widget/RelativeLayout;
    .end local v1    # "rightContainer":Landroid/widget/RelativeLayout;
    :cond_0
    :goto_0
    return-void

    .line 108
    .restart local v0    # "leftContainer":Landroid/widget/RelativeLayout;
    .restart local v1    # "rightContainer":Landroid/widget/RelativeLayout;
    :cond_1
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 109
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 110
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 118
    sget v0, Lcom/vlingo/midas/R$layout;->tutorial_screen_k:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->root_layout:Landroid/widget/RelativeLayout;

    .line 123
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->root_layout:Landroid/widget/RelativeLayout;

    return-object v0
.end method
