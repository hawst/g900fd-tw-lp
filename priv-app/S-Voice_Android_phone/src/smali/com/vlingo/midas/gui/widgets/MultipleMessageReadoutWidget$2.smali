.class Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;
.super Ljava/lang/Object;
.source "MultipleMessageReadoutWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

.field final synthetic val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field final synthetic val$listControlData:Lcom/vlingo/core/internal/util/ListControlData;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$listControlData:Lcom/vlingo/core/internal/util/ListControlData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 156
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 157
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/DialogDataType;->ACTIVE_CONTROLLER:Lcom/vlingo/core/internal/dialogmanager/DialogDataType;

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getState(Lcom/vlingo/core/internal/dialogmanager/DialogDataType;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/Controller;

    .line 158
    .local v1, "controller":Lcom/vlingo/core/internal/dialogmanager/Controller;
    instance-of v6, v1, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    if-eqz v6, :cond_0

    move-object v0, v1

    .line 159
    check-cast v0, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;

    .line 162
    .local v0, "alertController":Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;
    :try_start_0
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$listControlData:Lcom/vlingo/core/internal/util/ListControlData;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ListControlData;->decCurrentPage()V

    .line 163
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$string;->which_one_for_disambiguation:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    .line 165
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->makeShowMessageBody()Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    move-result-object v2

    .line 166
    .local v2, "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getAlertQueue()Ljava/util/LinkedList;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getAlertQueue()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 167
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v6

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getAlertQueue()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 168
    .local v5, "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0, v5}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getMessagesForWidget(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v6, v7, v2, v8, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 180
    .end local v0    # "alertController":Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;
    .end local v2    # "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .end local v5    # "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/messages/SMSMMSAlert;>;"
    :cond_0
    :goto_0
    return-void

    .line 170
    .restart local v0    # "alertController":Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;
    .restart local v2    # "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    :cond_1
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-static {v6}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v6

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/controllers/AlertReadoutController;->getSenderList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/util/ListControlData;->filterElementsForCurrentPage(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 171
    .local v4, "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    sget-object v7, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MultipleSenderMessageReadoutWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-interface {v6, v7, v2, v4, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->showWidget(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    :try_end_0
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 174
    .end local v2    # "deco":Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .end local v4    # "pageSenderList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;>;"
    :catch_0
    move-exception v3

    .line 175
    .local v3, "e":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;->val$actionHandlerListener:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    invoke-interface {v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->getActivityContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$string;->that_is_all_for_disambiguation:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;->tts(Ljava/lang/String;)V

    goto :goto_0
.end method
