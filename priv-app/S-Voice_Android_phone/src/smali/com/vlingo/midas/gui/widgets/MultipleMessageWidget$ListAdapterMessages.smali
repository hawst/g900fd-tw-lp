.class public Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;
.super Landroid/widget/BaseAdapter;
.source "MultipleMessageWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ListAdapterMessages"
.end annotation


# instance fields
.field private final list:Ljava/util/List;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->list:Ljava/util/List;

    .line 85
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "messages"    # Ljava/util/List;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 88
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->list:Ljava/util/List;

    .line 89
    return-void
.end method

.method private getDisplayMessage(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Z)Ljava/lang/String;
    .locals 5
    .param p1, "message"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .param p2, "showMessageBody"    # Z

    .prologue
    const/16 v4, 0x1e

    .line 109
    if-eqz p2, :cond_1

    .line 110
    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_0

    .line 113
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    :cond_0
    :goto_0
    return-object v0

    .line 116
    .end local v0    # "msg":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->core_car_safereader_hidden_message_body:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "msg":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 93
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 94
    .local v0, "toReturn":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->getLimitedCount(I)I

    move-result v1

    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 104
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    .line 124
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->list:Ljava/util/List;

    .line 125
    .local v4, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageType;>;"
    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .line 128
    .local v3, "message":Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    if-nez p2, :cond_1

    .line 129
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$layout;->item_message_details:I

    invoke-static {v5, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 130
    new-instance v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;

    invoke-direct {v2, v7}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;)V

    .line 131
    .local v2, "holder":Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;
    sget v5, Lcom/vlingo/midas/R$id;->from:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;->from:Landroid/widget/TextView;

    .line 132
    sget v5, Lcom/vlingo/midas/R$id;->body:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;->body:Landroid/widget/TextView;

    .line 133
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 138
    :goto_0
    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 139
    iget-object v5, v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;->from:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    iget-object v7, v7, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    :goto_1
    sget v5, Lcom/vlingo/midas/R$id;->count:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 146
    .local v0, "count":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 147
    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    :cond_0
    const/4 v5, 0x1

    invoke-direct {p0, v3, v5}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->getDisplayMessage(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Z)Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "displayMessage":Ljava/lang/String;
    iget-object v5, v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;->body:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    new-instance v5, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages$1;

    invoke-direct {v5, p0, v3}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages$1;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V

    invoke-virtual {p2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne p1, v5, :cond_3

    .line 166
    sget v5, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 170
    :goto_2
    return-object p2

    .line 135
    .end local v0    # "count":Landroid/widget/TextView;
    .end local v1    # "displayMessage":Ljava/lang/String;
    .end local v2    # "holder":Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;

    .restart local v2    # "holder":Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;
    goto/16 :goto_0

    .line 141
    :cond_2
    iget-object v5, v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ViewHolder;->from:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    iget-object v7, v7, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getContactName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 168
    .restart local v0    # "count":Landroid/widget/TextView;
    .restart local v1    # "displayMessage":Ljava/lang/String;
    :cond_3
    sget v5, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2
.end method
