.class Lcom/vlingo/midas/gui/widgets/NaverLocalWidget$1;
.super Ljava/lang/Object;
.source "NaverLocalWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 140
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->item:Lcom/vlingo/midas/naver/NaverLocalItem;
    invoke-static {v8}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->access$000(Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;)Lcom/vlingo/midas/naver/NaverLocalItem;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/midas/naver/NaverLocalItem;->getMapImageUrl()Ljava/lang/String;

    move-result-object v7

    .line 141
    .local v7, "url":Ljava/lang/String;
    if-nez v7, :cond_0

    .line 197
    :goto_0
    return-void

    .line 143
    :cond_0
    const-string/jumbo v8, "defaul&"

    const-string/jumbo v9, "default&"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 144
    const-string/jumbo v8, "&w=290&h=200"

    invoke-virtual {v7, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 145
    move-object v2, v7

    .line 148
    .local v2, "encryptedUrl":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/naver/api/security/client/MACManager;->initialize()V

    .line 149
    invoke-static {v7}, Lcom/naver/api/security/client/MACManager;->getEncryptUrl(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 157
    :goto_1
    :try_start_1
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 158
    .local v5, "picURL":Ljava/net/URL;
    invoke-virtual {v5}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-static {v8}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 163
    .local v3, "image":Landroid/graphics/Bitmap;
    if-nez v3, :cond_2

    .line 165
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-virtual {v5}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 167
    .local v4, "in":Ljava/io/BufferedReader;
    const/4 v6, 0x0

    .line 168
    .local v6, "str":Ljava/lang/String;
    :goto_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 172
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;)Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v6, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 194
    .end local v3    # "image":Landroid/graphics/Bitmap;
    .end local v4    # "in":Ljava/io/BufferedReader;
    .end local v5    # "picURL":Ljava/net/URL;
    .end local v6    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 150
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 152
    .local v1, "e1":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 175
    .end local v1    # "e1":Ljava/lang/Exception;
    .restart local v3    # "image":Landroid/graphics/Bitmap;
    .restart local v4    # "in":Ljava/io/BufferedReader;
    .restart local v5    # "picURL":Ljava/net/URL;
    .restart local v6    # "str":Ljava/lang/String;
    :cond_1
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 193
    .end local v4    # "in":Ljava/io/BufferedReader;
    .end local v6    # "str":Ljava/lang/String;
    :goto_3
    invoke-virtual {v5}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 178
    :cond_2
    new-instance v8, Landroid/os/Handler;

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v9, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget$1$1;

    invoke-direct {v9, p0, v3}, Lcom/vlingo/midas/gui/widgets/NaverLocalWidget$1$1;-><init>(Lcom/vlingo/midas/gui/widgets/NaverLocalWidget$1;Landroid/graphics/Bitmap;)V

    const-wide/16 v10, 0x1f4

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3
.end method
