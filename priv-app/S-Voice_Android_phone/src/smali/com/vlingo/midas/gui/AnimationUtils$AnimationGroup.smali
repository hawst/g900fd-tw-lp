.class public abstract Lcom/vlingo/midas/gui/AnimationUtils$AnimationGroup;
.super Ljava/util/ArrayList;
.source "AnimationUtils.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/AnimationUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AnimationGroup"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Landroid/view/animation/Animation;",
        ">;",
        "Landroid/view/animation/Animation$AnimationListener;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "a"    # Landroid/view/animation/Animation;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/AnimationUtils$AnimationGroup;->add(Landroid/view/animation/Animation;)Z

    .line 29
    return-void
.end method


# virtual methods
.method public varargs add([Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "anims"    # [Landroid/view/animation/Animation;

    .prologue
    .line 36
    move-object v1, p1

    .local v1, "arr$":[Landroid/view/animation/Animation;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    .line 37
    .local v0, "a":Landroid/view/animation/Animation;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/AnimationUtils$AnimationGroup;->add(Landroid/view/animation/Animation;)Z

    .line 36
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 39
    .end local v0    # "a":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method public add(Landroid/view/animation/Animation;)Z
    .locals 1
    .param p1, "a"    # Landroid/view/animation/Animation;

    .prologue
    .line 32
    invoke-virtual {p1, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 33
    invoke-super {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 21
    check-cast p1, Landroid/view/animation/Animation;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/AnimationUtils$AnimationGroup;->add(Landroid/view/animation/Animation;)Z

    move-result v0

    return v0
.end method

.method public abstract onAnimationEnd()V
.end method

.method public declared-synchronized onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 42
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/AnimationUtils$AnimationGroup;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/AnimationUtils$AnimationGroup;->remove(Ljava/lang/Object;)Z

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AnimationUtils$AnimationGroup;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AnimationUtils$AnimationGroup;->onAnimationEnd()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :cond_1
    monitor-exit p0

    return-void

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 50
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 52
    return-void
.end method
