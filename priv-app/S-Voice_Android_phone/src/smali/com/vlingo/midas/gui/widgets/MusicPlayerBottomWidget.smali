.class public Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;
.super Landroid/widget/LinearLayout;
.source "MusicPlayerBottomWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;
    }
.end annotation


# static fields
.field public static final ACTION_MUSIC_CLOSE:Ljava/lang/String; = "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

.field private static final UPDATE_THUMBNAIL:I


# instance fields
.field private final context:Landroid/content/Context;

.field private dialogScrollContent:Landroid/widget/LinearLayout;

.field private dialogScrollView:Landroid/widget/ScrollView;

.field private fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

.field private isPlaying:Z

.field private mAudioClose:Landroid/widget/ImageView;

.field private mAudioNext:Landroid/widget/ImageView;

.field private mAudioPlay:Landroid/widget/ImageView;

.field private mAudioPrevious:Landroid/widget/ImageView;

.field private mAudioTemplateRLayout:Landroid/widget/LinearLayout;

.field private mHandler:Landroid/os/Handler;

.field private mThumbnail:Landroid/widget/ImageView;

.field private mTitle:Landroid/widget/TextView;

.field private musicBroadcastReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->isPlaying:Z

    .line 99
    new-instance v0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mHandler:Landroid/os/Handler;

    .line 65
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioTemplateRLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->isPlaying:Z

    return v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->isPlaying:Z

    return p1
.end method

.method static synthetic access$202(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Lcom/vlingo/midas/gui/customviews/RelativeLayout;)Lcom/vlingo/midas/gui/customviews/RelativeLayout;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;
    .param p1, "x1"    # Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    return-object p1
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollContent:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$302(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollContent:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Landroid/widget/ScrollView;)Landroid/widget/ScrollView;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;
    .param p1, "x1"    # Landroid/widget/ScrollView;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollView:Landroid/widget/ScrollView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mThumbnail:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->setPlayingButton(Z)V

    return-void
.end method

.method private close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 249
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    sget v2, Lcom/vlingo/midas/R$id;->dialog_full_container:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    .line 251
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    sget v2, Lcom/vlingo/midas/R$id;->dialog_scroll_content:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollContent:Landroid/widget/LinearLayout;

    .line 253
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    sget v2, Lcom/vlingo/midas/R$id;->dialog_scrollview:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollView:Landroid/widget/ScrollView;

    .line 256
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.music.musicservicecommand.pause"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 258
    .local v0, "pauseIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 259
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 260
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/widget/ScrollView;->setPadding(IIII)V

    .line 261
    sget v1, Lcom/vlingo/midas/R$id;->audioTemplateRLayout:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 268
    return-void
.end method

.method private initViews()V
    .locals 1

    .prologue
    .line 76
    sget v0, Lcom/vlingo/midas/R$id;->audioTemplateRLayout:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioTemplateRLayout:Landroid/widget/LinearLayout;

    .line 78
    sget v0, Lcom/vlingo/midas/R$id;->audioImage:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mThumbnail:Landroid/widget/ImageView;

    .line 79
    sget v0, Lcom/vlingo/midas/R$id;->audioTitle:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mTitle:Landroid/widget/TextView;

    .line 80
    sget v0, Lcom/vlingo/midas/R$id;->audioPrevious:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioPrevious:Landroid/widget/ImageView;

    .line 81
    sget v0, Lcom/vlingo/midas/R$id;->audioPlay:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioPlay:Landroid/widget/ImageView;

    .line 82
    sget v0, Lcom/vlingo/midas/R$id;->audioNext:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioNext:Landroid/widget/ImageView;

    .line 83
    sget v0, Lcom/vlingo/midas/R$id;->audioClose:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioClose:Landroid/widget/ImageView;

    .line 85
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->isPlaying:Z

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->setPlayingButton(Z)V

    .line 87
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioPrevious:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioNext:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioClose:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    return-void
.end method

.method private play()V
    .locals 2

    .prologue
    .line 277
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.music.musicservicecommand.play"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 279
    .local v0, "playIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 281
    return-void
.end method

.method private playNext()V
    .locals 2

    .prologue
    .line 271
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.music.musicservicecommand.playnext"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 273
    .local v0, "playIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 274
    return-void
.end method

.method private playPrevious()V
    .locals 2

    .prologue
    .line 284
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.music.musicservicecommand.playprevious"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 286
    .local v0, "previousIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 287
    return-void
.end method

.method private setPlayingButton(Z)V
    .locals 2
    .param p1, "isPlaying"    # Z

    .prologue
    .line 212
    if-nez p1, :cond_0

    .line 213
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioPlay:Landroid/widget/ImageView;

    sget v1, Lcom/vlingo/midas/R$drawable;->music_widget_selector_play_bottom:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 219
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioPlay:Landroid/widget/ImageView;

    sget v1, Lcom/vlingo/midas/R$drawable;->music_widget_selector_pause_bottom:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private stop()V
    .locals 2

    .prologue
    .line 242
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.app.music.musicservicecommand.pause"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 244
    .local v0, "pauseIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 245
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->isPlaying:Z

    invoke-direct {p0, v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->setPlayingButton(Z)V

    .line 246
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 223
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 225
    .local v0, "_id":I
    sget v1, Lcom/vlingo/midas/R$id;->audioPrevious:I

    if-ne v0, v1, :cond_1

    .line 226
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->playPrevious()V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    sget v1, Lcom/vlingo/midas/R$id;->audioPlay:I

    if-ne v0, v1, :cond_3

    .line 228
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->isPlaying:Z

    if-eqz v1, :cond_2

    .line 229
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->stop()V

    goto :goto_0

    .line 232
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->play()V

    goto :goto_0

    .line 234
    :cond_3
    sget v1, Lcom/vlingo/midas/R$id;->audioNext:I

    if-ne v0, v1, :cond_4

    .line 235
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->playNext()V

    goto :goto_0

    .line 236
    :cond_4
    sget v1, Lcom/vlingo/midas/R$id;->audioClose:I

    if-ne v0, v1, :cond_0

    .line 237
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->close()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 291
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 293
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 295
    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->musicBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 300
    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mHandler:Landroid/os/Handler;

    .line 302
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 96
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->initViews()V

    .line 97
    return-void
.end method
