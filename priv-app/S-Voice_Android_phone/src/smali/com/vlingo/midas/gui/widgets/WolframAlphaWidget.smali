.class public Lcom/vlingo/midas/gui/widgets/WolframAlphaWidget;
.super Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;
.source "WolframAlphaWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private wolframAlphaImage:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method private openWolframAlphaWeb()V
    .locals 2

    .prologue
    .line 66
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 67
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "http://www.wolframalpha.com/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 68
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WolframAlphaWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 69
    return-void
.end method


# virtual methods
.method public getLayoutID()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/vlingo/midas/R$id;->wolfram_content_container:I

    return v0
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 60
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->logoImage:I

    if-ne v0, v1, :cond_0

    .line 61
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WolframAlphaWidget;->openWolframAlphaWeb()V

    .line 63
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 45
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    sget v1, Lcom/vlingo/midas/R$id;->wolframeShadow:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WolframAlphaWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 47
    .local v0, "shadow":Landroid/view/View;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    .end local v0    # "shadow":Landroid/view/View;
    :cond_0
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->onFinishInflate()V

    .line 51
    sget v1, Lcom/vlingo/midas/R$id;->logoImage:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WolframAlphaWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WolframAlphaWidget;->wolframAlphaImage:Landroid/widget/ImageView;

    .line 52
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WolframAlphaWidget;->wolframAlphaImage:Landroid/widget/ImageView;

    const-string/jumbo v2, "wolframalpha.com"

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WolframAlphaWidget;->wolframAlphaImage:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method
