.class Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;
.super Landroid/widget/BaseAdapter;
.source "ScheduleChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScheduleAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private mEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/schedule/ScheduleEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p3, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/schedule/ScheduleEvent;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 91
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 92
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mEvents:Ljava/util/List;

    .line 93
    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->context:Landroid/content/Context;

    .line 94
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mEvents:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 98
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mEvents:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 99
    .local v0, "toReturn":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;->getLimitedCount(I)I

    move-result v1

    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mEvents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 109
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 116
    if-nez p2, :cond_2

    .line 117
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v10, Lcom/vlingo/midas/R$layout;->item_schedule_choice:I

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 119
    new-instance v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;

    invoke-direct {v7, p0}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;)V

    .line 120
    .local v7, "holder":Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;
    sget v4, Lcom/vlingo/midas/R$id;->schedule_title:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 121
    sget v4, Lcom/vlingo/midas/R$id;->schedule_date:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->date:Landroid/widget/TextView;

    .line 122
    sget v4, Lcom/vlingo/midas/R$id;->schedule_time:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    .line 123
    sget v4, Lcom/vlingo/midas/R$id;->divider:I

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->divider:Landroid/view/View;

    .line 125
    invoke-virtual {p2, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 130
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->mEvents:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vlingo/core/internal/schedule/ScheduleEvent;

    .line 133
    .local v6, "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 134
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    sget v10, Lcom/vlingo/midas/R$string;->my_event:I

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(I)V

    .line 139
    :goto_1
    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getBegin()J

    move-result-wide v0

    .line 140
    .local v0, "begin":J
    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getEnd()J

    move-result-wide v2

    .line 141
    .local v2, "end":J
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v5

    .line 142
    .local v5, "currentLocale":Ljava/util/Locale;
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v10, "date_format"

    invoke-static {v4, v10}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 143
    .local v8, "phoneDateFormat":Ljava/lang/String;
    const-wide/16 v10, 0x0

    cmp-long v4, v0, v10

    if-eqz v4, :cond_5

    .line 144
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->date:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1, v8, v5}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatScheduleDate(JLjava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getAllDay()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 147
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->all_day:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    :goto_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->getCount()I

    move-result v4

    const/4 v10, 0x1

    if-eq v4, v10, :cond_a

    .line 157
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 159
    if-nez p1, :cond_6

    .line 160
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->getCount()I

    move-result v4

    const/4 v10, 0x1

    if-ne v4, v10, :cond_0

    .line 161
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v10, 0x4

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 166
    :cond_0
    :goto_3
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 184
    :cond_1
    :goto_4
    new-instance v4, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;

    invoke-direct {v4, p0, p1}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;I)V

    invoke-virtual {p2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    return-object p2

    .line 127
    .end local v0    # "begin":J
    .end local v2    # "end":J
    .end local v5    # "currentLocale":Ljava/util/Locale;
    .end local v6    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    .end local v7    # "holder":Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;
    .end local v8    # "phoneDateFormat":Ljava/lang/String;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;

    .restart local v7    # "holder":Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;
    goto/16 :goto_0

    .line 136
    .restart local v6    # "event":Lcom/vlingo/core/internal/schedule/ScheduleEvent;
    :cond_3
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/schedule/ScheduleEvent;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 149
    .restart local v0    # "begin":J
    .restart local v2    # "end":J
    .restart local v5    # "currentLocale":Ljava/util/Locale;
    .restart local v8    # "phoneDateFormat":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    invoke-static/range {v0 .. v5}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatScheduleTime(JJZLjava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    .line 150
    .local v9, "timeString":Ljava/lang/String;
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 153
    .end local v9    # "timeString":Ljava/lang/String;
    :cond_5
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 163
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-lt p1, v4, :cond_0

    .line 164
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v10, 0x4

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 168
    :cond_7
    if-nez p1, :cond_8

    .line 169
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 170
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->getCount()I

    move-result v4

    const/4 v10, 0x1

    if-ne v4, v10, :cond_1

    .line 171
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v10, 0x4

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 173
    :cond_8
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-lt p1, v4, :cond_9

    .line 174
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 175
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v10, 0x4

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 177
    :cond_9
    sget v4, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4

    .line 181
    :cond_a
    iget-object v4, v7, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget$ScheduleAdapter$ViewHolder;->divider:Landroid/view/View;

    const/4 v10, 0x4

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method
