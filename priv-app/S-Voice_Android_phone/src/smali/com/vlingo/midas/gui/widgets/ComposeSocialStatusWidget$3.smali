.class Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$3;
.super Ljava/lang/Object;
.source "ComposeSocialStatusWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 104
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->access$100(Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;

    const/4 v2, 0x1

    # setter for: Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->isClicked:Z
    invoke-static {v1, v2}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->access$202(Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;Z)Z

    .line 107
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->retire()V

    .line 108
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 109
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Cancel"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;->access$100(Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 112
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
