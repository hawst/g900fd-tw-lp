.class public Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;
.super Landroid/widget/BaseAdapter;
.source "ContactDetailWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ContactAdapter"
.end annotation


# instance fields
.field private final contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;

.field private final titleInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 339
    .local p3, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .local p4, "titleInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 340
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;->contacts:Ljava/util/List;

    .line 341
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;->titleInfo:Ljava/util/List;

    .line 342
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 346
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 351
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 361
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 362
    .local v0, "data":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;->titleInfo:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 364
    .local v2, "subjectInfo":Ljava/lang/String;
    if-nez p2, :cond_2

    .line 365
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->item_contact_detail:I

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 366
    new-instance v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;

    invoke-direct {v1, v5}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$1;)V

    .line 367
    .local v1, "holder":Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;
    sget v3, Lcom/vlingo/midas/R$id;->text_detail_first:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;->subject:Landroid/widget/TextView;

    .line 368
    sget v3, Lcom/vlingo/midas/R$id;->text_detail_second:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;->data_view:Landroid/widget/TextView;

    .line 369
    sget v3, Lcom/vlingo/midas/R$id;->list_divider:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;->divider:Landroid/view/View;

    .line 370
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 371
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;->divider:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 373
    :cond_0
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 384
    :goto_0
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;->subject:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;->data_view:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 387
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p1, v3, :cond_1

    .line 388
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;->divider:Landroid/view/View;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 391
    :cond_1
    new-instance v3, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    return-object p2

    .line 375
    .end local v1    # "holder":Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 377
    new-instance v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;

    invoke-direct {v1, v5}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$1;)V

    .line 378
    .restart local v1    # "holder":Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;
    sget v3, Lcom/vlingo/midas/R$id;->list_divider:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;->divider:Landroid/view/View;

    .line 379
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;->divider:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 381
    .end local v1    # "holder":Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;

    .restart local v1    # "holder":Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;
    goto :goto_0
.end method
