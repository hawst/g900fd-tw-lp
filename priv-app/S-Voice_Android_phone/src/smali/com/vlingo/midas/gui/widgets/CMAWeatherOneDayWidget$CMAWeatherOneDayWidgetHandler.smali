.class Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget$CMAWeatherOneDayWidgetHandler;
.super Landroid/os/Handler;
.source "CMAWeatherOneDayWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CMAWeatherOneDayWidgetHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;

    .prologue
    .line 219
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 220
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget$CMAWeatherOneDayWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 221
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 224
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget$CMAWeatherOneDayWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;

    .line 225
    .local v0, "o":Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;
    if-eqz v0, :cond_0

    .line 226
    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->setValuesForcastWeather()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->access$000(Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;)V

    .line 227
    # invokes: Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->setTalkbackString()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->access$100(Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;)V

    .line 229
    :cond_0
    return-void
.end method
