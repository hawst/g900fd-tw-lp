.class Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;
.super Ljava/lang/Object;
.source "AlarmChoiceWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;

.field final synthetic val$alarm:Lcom/vlingo/core/internal/util/Alarm;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;ILcom/vlingo/core/internal/util/Alarm;)V
    .locals 0

    .prologue
    .line 441
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;

    iput p2, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;->val$position:I

    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;->val$alarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 443
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->retire()V

    .line 444
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 445
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 446
    const-string/jumbo v2, "choice"

    iget v3, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;->val$position:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 448
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mAlarmList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$500(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)Landroid/widget/ListView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 449
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mAlarmList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$500(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 451
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_1

    .line 452
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;->val$alarm:Lcom/vlingo/core/internal/util/Alarm;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/Alarm;->getId()I

    move-result v3

    # invokes: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->openAlarmApp(I)V
    invoke-static {v2, v3}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$600(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;I)V

    .line 457
    .end local v0    # "adapter":Landroid/widget/ListAdapter;
    :cond_0
    :goto_0
    return-void

    .line 454
    .restart local v0    # "adapter":Landroid/widget/ListAdapter;
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget$AlarmAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;->access$700(Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    goto :goto_0
.end method
