.class public Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "NaverMovieChoiceWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$2;,
        Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;,
        Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;,
        Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MoreClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/midas/naver/NaverAdaptor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private final mContext:Landroid/content/Context;

.field private mHour:I

.field private mMin:I

.field private mMovieList:Landroid/widget/ListView;

.field private mMovies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTempHour:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mContext:Landroid/content/Context;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovies:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected static setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V
    .locals 4
    .param p0, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 80
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 81
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$dimen;->item_naver_movie_choice_height:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {p0}, Landroid/widget/ListView;->getCount()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 82
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    .line 84
    return-void
.end method


# virtual methods
.method public initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 6
    .param p1, "allMovies"    # Lcom/vlingo/midas/naver/NaverAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 90
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 92
    if-eqz p1, :cond_1

    .line 93
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;

    new-instance v4, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, p0, v5, p1}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;Landroid/content/Context;Lcom/vlingo/midas/naver/NaverAdaptor;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 94
    invoke-virtual {p1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v3

    iget-object v3, v3, Lcom/vlingo/midas/naver/NaverXMLParser;->itemList:Ljava/util/List;

    iput-object v3, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovies:Ljava/util/List;

    .line 96
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;

    invoke-direct {v4, p0}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 158
    sget v3, Lcom/vlingo/midas/R$id;->text_Movie_MainTitle:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 159
    .local v1, "mainTitle":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v0

    .line 160
    .local v0, "Result":Lcom/vlingo/midas/naver/NaverXMLParser;
    iget-object v3, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->svoiceQuery:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->svoiceQuery:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 161
    iget-object v3, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->svoiceQuery:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 168
    sget v3, Lcom/vlingo/midas/R$id;->logo_k:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 169
    sget v3, Lcom/vlingo/midas/R$id;->logo:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 172
    :cond_0
    sget v3, Lcom/vlingo/midas/R$id;->btn_more:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 173
    .local v2, "moreBtn":Landroid/widget/Button;
    new-instance v4, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MoreClickListener;

    iget-object v3, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->propertyList:Ljava/util/Hashtable;

    const-string/jumbo v5, "moreUrl"

    invoke-virtual {v3, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v4, p0, v3}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MoreClickListener;-><init>(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    .end local v0    # "Result":Lcom/vlingo/midas/naver/NaverXMLParser;
    .end local v1    # "mainTitle":Landroid/widget/TextView;
    .end local v2    # "moreBtn":Landroid/widget/Button;
    :cond_1
    return-void

    .line 162
    .restart local v0    # "Result":Lcom/vlingo/midas/naver/NaverXMLParser;
    .restart local v1    # "mainTitle":Landroid/widget/TextView;
    :cond_2
    iget-object v3, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->query:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->query:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 163
    iget-object v3, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->query:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 165
    :cond_3
    const-string/jumbo v3, "--"

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 38
    check-cast p1, Lcom/vlingo/midas/naver/NaverAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 406
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 400
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 58
    sget v0, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;

    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 60
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 61
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 395
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;

    sget v1, Lcom/vlingo/midas/R$dimen;->item_naver_movie_choice_height:I

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->measureListviewHeight(Landroid/widget/ListView;IZ)V

    .line 76
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 77
    return-void
.end method
