.class Lcom/vlingo/midas/gui/WelcomeFragment$2;
.super Ljava/lang/Object;
.source "WelcomeFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/WelcomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/WelcomeFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/WelcomeFragment;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/vlingo/midas/gui/WelcomeFragment$2;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v2, 0x8

    .line 78
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment$2;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    # getter for: Lcom/vlingo/midas/gui/WelcomeFragment;->mWelcomeTextLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WelcomeFragment;->access$100(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 79
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment$2;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    # getter for: Lcom/vlingo/midas/gui/WelcomeFragment;->mWelcomeTextLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WelcomeFragment;->access$100(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment$2;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    # getter for: Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WelcomeFragment;->access$200(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment$2;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    # getter for: Lcom/vlingo/midas/gui/WelcomeFragment;->mSvoiceTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WelcomeFragment;->access$300(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment$2;->this$0:Lcom/vlingo/midas/gui/WelcomeFragment;

    iget-object v0, v0, Lcom/vlingo/midas/gui/WelcomeFragment;->mOnNext:Lcom/vlingo/midas/gui/WelcomeFragment$onNext;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/vlingo/midas/gui/WelcomeFragment$onNext;->onNextClicked(I)V

    .line 83
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 74
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 70
    return-void
.end method
