.class public Lcom/vlingo/midas/gui/NoticePopup;
.super Landroid/app/Activity;
.source "NoticePopup.java"


# instance fields
.field m_SelectedIndex:I

.field private m_checkBox:Landroid/widget/CheckBox;

.field private m_oTextDlg:Landroid/app/AlertDialog;

.field private noticePopupText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_checkBox:Landroid/widget/CheckBox;

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_SelectedIndex:I

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/NoticePopup;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/NoticePopup;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_checkBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private initialize()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 119
    const-string/jumbo v2, "voiceinputcontrol_showNeverAgain"

    invoke-static {v2, v0}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 121
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x400

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 33
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/NoticePopup;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 37
    invoke-virtual {p0, v9}, Lcom/vlingo/midas/gui/NoticePopup;->requestWindowFeature(I)Z

    .line 39
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/NoticePopup;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 40
    .local v3, "layout":Landroid/view/LayoutInflater;
    sget v6, Lcom/vlingo/midas/R$layout;->notice_popup_custom_view:I

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 41
    .local v1, "checked":Landroid/view/View;
    sget v6, Lcom/vlingo/midas/R$id;->noticepopup_checkbox:I

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    iput-object v6, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_checkBox:Landroid/widget/CheckBox;

    .line 42
    sget v6, Lcom/vlingo/midas/R$id;->notice_popup_text:I

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/vlingo/midas/gui/NoticePopup;->noticePopupText:Landroid/widget/TextView;

    .line 44
    invoke-direct {p0}, Lcom/vlingo/midas/gui/NoticePopup;->initialize()Z

    move-result v6

    if-nez v6, :cond_1

    .line 45
    iget-object v6, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 47
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/NoticePopup;->finish()V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->help_wcis_driving_mode:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/NoticePopup;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 56
    sget v6, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_ok:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/gui/NoticePopup;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/midas/gui/NoticePopup$1;

    invoke-direct {v7, p0}, Lcom/vlingo/midas/gui/NoticePopup$1;-><init>(Lcom/vlingo/midas/gui/NoticePopup;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 69
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 71
    iget-object v6, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_checkBox:Landroid/widget/CheckBox;

    iget-object v7, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v7}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v7

    add-int/lit8 v7, v7, 0x1e

    invoke-virtual {v6, v7, v8, v8, v8}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 72
    iget-object v6, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_checkBox:Landroid/widget/CheckBox;

    new-instance v7, Lcom/vlingo/midas/gui/NoticePopup$2;

    invoke-direct {v7, p0}, Lcom/vlingo/midas/gui/NoticePopup$2;-><init>(Lcom/vlingo/midas/gui/NoticePopup;)V

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_oTextDlg:Landroid/app/AlertDialog;

    .line 81
    iget-object v6, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_oTextDlg:Landroid/app/AlertDialog;

    new-instance v7, Lcom/vlingo/midas/gui/NoticePopup$3;

    invoke-direct {v7, p0}, Lcom/vlingo/midas/gui/NoticePopup$3;-><init>(Lcom/vlingo/midas/gui/NoticePopup;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 88
    :try_start_0
    const-string/jumbo v6, "statusbar"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/gui/NoticePopup;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 89
    .local v4, "sbservice":Ljava/lang/Object;
    const-string/jumbo v6, "android.app.StatusBarManager"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    .line 90
    .local v5, "statusbarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v6, "collapsePanels"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 91
    .local v2, "hidesb":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v4, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    .end local v2    # "hidesb":Ljava/lang/reflect/Method;
    .end local v4    # "sbservice":Ljava/lang/Object;
    .end local v5    # "statusbarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v6

    sget-object v7, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v6, v7, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v6

    sget-object v7, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v6, v7, :cond_0

    .line 102
    :cond_2
    iget-object v6, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_oTextDlg:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 92
    :catch_0
    move-exception v6

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 112
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/NoticePopup;->hasWindowFocus()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_oTextDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_oTextDlg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/vlingo/midas/gui/NoticePopup;->m_oTextDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 116
    :cond_1
    return-void
.end method
