.class public Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;
.super Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;
.source "PlayMusicWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$1;,
        Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;,
        Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
        ">;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field private static mHashMap:Lcom/vlingo/core/internal/util/SparseArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vlingo/core/internal/util/SparseArrayMap",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private COUNTER_HASHMAP_ENTRY:I

.field private FIRST_TIME:I

.field private VALUE_COUNTER_HASHMAP:I

.field private final context:Landroid/content/Context;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mListView:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/vlingo/core/internal/util/SparseArrayMap;

    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->getMultiWidgetItemsInitialMax()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/vlingo/core/internal/util/SparseArrayMap;-><init>(I)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mHashMap:Lcom/vlingo/core/internal/util/SparseArrayMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iput v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->COUNTER_HASHMAP_ENTRY:I

    .line 53
    iput v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->VALUE_COUNTER_HASHMAP:I

    .line 54
    const/4 v0, 0x1

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->FIRST_TIME:I

    .line 60
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->context:Landroid/content/Context;

    .line 61
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    .prologue
    .line 43
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->COUNTER_HASHMAP_ENTRY:I

    return v0
.end method

.method static synthetic access$202(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->COUNTER_HASHMAP_ENTRY:I

    return p1
.end method

.method static synthetic access$208(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    .prologue
    .line 43
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->COUNTER_HASHMAP_ENTRY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->COUNTER_HASHMAP_ENTRY:I

    return v0
.end method

.method static synthetic access$300()Lcom/vlingo/core/internal/util/SparseArrayMap;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mHashMap:Lcom/vlingo/core/internal/util/SparseArrayMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    .prologue
    .line 43
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->FIRST_TIME:I

    return v0
.end method

.method static synthetic access$408(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    .prologue
    .line 43
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->FIRST_TIME:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->FIRST_TIME:I

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    .prologue
    .line 43
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->VALUE_COUNTER_HASHMAP:I

    return v0
.end method

.method static synthetic access$502(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->VALUE_COUNTER_HASHMAP:I

    return p1
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 43
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 4
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 73
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 75
    if-nez p1, :cond_0

    .line 85
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->context:Landroid/content/Context;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v1, p0, v2, v3}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 81
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 82
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mListView:Landroid/widget/ListView;

    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 83
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onFinishInflate()V

    .line 66
    sget v0, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mListView:Landroid/widget/ListView;

    .line 67
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 266
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->retire()V

    .line 267
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 268
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.ContactChoice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 270
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 271
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 272
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 277
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 278
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onMeasure(II)V

    .line 279
    return-void
.end method
