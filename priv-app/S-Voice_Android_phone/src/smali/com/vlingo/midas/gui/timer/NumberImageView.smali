.class public Lcom/vlingo/midas/gui/timer/NumberImageView;
.super Landroid/widget/ImageView;
.source "NumberImageView.java"


# instance fields
.field private isPressed:Z

.field private number:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 106
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setMeasuredDimension(II)V

    .line 107
    return-void
.end method

.method public setNumber(I)V
    .locals 1
    .param p1, "number"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/vlingo/midas/gui/timer/NumberImageView;->number:I

    .line 85
    sget-object v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;->timeNumber:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, p1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 87
    return-void
.end method

.method public setNumberEnable(I)V
    .locals 3
    .param p1, "number"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/vlingo/midas/gui/timer/NumberImageView;->number:I

    .line 93
    :try_start_0
    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, p1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "ae":Ljava/lang/ArrayIndexOutOfBoundsException;
    sget-object v1, Lcom/vlingo/midas/gui/widgets/TimerWidget;->sEnabledNumber:[Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/timer/NumberImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 96
    const-string/jumbo v1, "NumberImageView"

    const-string/jumbo v2, "ArrayOutOfBoundException occurred while setting the timer image.So setting default image(0 number)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method
