.class final Lcom/vlingo/midas/gui/widgets/TimerWidget$2;
.super Landroid/os/CountDownTimer;
.source "TimerWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/TimerWidget;->setTimer(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(JJ)V
    .locals 0
    .param p1, "x0"    # J
    .param p3, "x1"    # J

    .prologue
    .line 349
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 357
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    .line 358
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/midas/gui/timer/TimerManager;->state:I

    .line 359
    # invokes: Lcom/vlingo/midas/gui/widgets/TimerWidget;->updateTime()V
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->access$100()V

    .line 360
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->SetTimer:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V

    .line 361
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 362
    return-void
.end method

.method public onTick(J)V
    .locals 1
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 351
    sput-wide p1, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    .line 352
    sget-boolean v0, Lcom/vlingo/midas/gui/timer/TimerManager;->isStarted:Z

    if-eqz v0, :cond_0

    .line 353
    # invokes: Lcom/vlingo/midas/gui/widgets/TimerWidget;->updateTime()V
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->access$100()V

    .line 355
    :cond_0
    return-void
.end method
