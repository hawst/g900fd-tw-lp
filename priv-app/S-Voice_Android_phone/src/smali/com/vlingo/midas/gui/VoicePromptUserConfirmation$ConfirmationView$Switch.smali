.class public final enum Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;
.super Ljava/lang/Enum;
.source "VoicePromptUserConfirmation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Switch"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

.field public static final enum OFF:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

.field public static final enum ON:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 166
    new-instance v0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    const-string/jumbo v1, "OFF"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;->OFF:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    new-instance v0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    const-string/jumbo v1, "ON"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;->ON:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    sget-object v1, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;->OFF:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;->ON:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;->$VALUES:[Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 166
    const-class v0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;->$VALUES:[Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    invoke-virtual {v0}, [Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    return-object v0
.end method
