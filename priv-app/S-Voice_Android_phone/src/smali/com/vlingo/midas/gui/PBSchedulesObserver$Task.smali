.class Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;
.super Ljava/lang/Object;
.source "PBSchedulesObserver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/PBSchedulesObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Task"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/PBSchedulesObserver;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;->this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 29
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;->this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    sget-object v2, Lcom/vlingo/midas/gui/DialogFragment;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->getTodayInstances()Landroid/database/Cursor;

    move-result-object v2

    iput-object v2, v1, Lcom/vlingo/midas/gui/PBSchedulesObserver;->cursor:Landroid/database/Cursor;

    .line 30
    iget-object v1, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;->this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    iget-object v1, v1, Lcom/vlingo/midas/gui/PBSchedulesObserver;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 31
    sget-object v1, Lcom/vlingo/midas/gui/DialogFragment;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    iget-object v2, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;->this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    iget-object v2, v2, Lcom/vlingo/midas/gui/PBSchedulesObserver;->cursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->getEventInfo(Landroid/database/Cursor;)V

    .line 32
    :cond_0
    sget-object v1, Lcom/vlingo/midas/gui/DialogFragment;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->getContacts()Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lcom/vlingo/midas/gui/DialogFragment;->birthdayList:Ljava/util/ArrayList;

    .line 33
    const/4 v1, 0x1

    sput-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->threadVar:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    iget-object v1, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;->this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    iget-object v1, v1, Lcom/vlingo/midas/gui/PBSchedulesObserver;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 39
    iget-object v1, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;->this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    iget-object v1, v1, Lcom/vlingo/midas/gui/PBSchedulesObserver;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 42
    :cond_1
    :goto_0
    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v1, "SmartNotification"

    const-string/jumbo v2, "Exception Occurred in DialogFragment SmartNotification"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    iget-object v1, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;->this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    iget-object v1, v1, Lcom/vlingo/midas/gui/PBSchedulesObserver;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 39
    iget-object v1, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;->this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    iget-object v1, v1, Lcom/vlingo/midas/gui/PBSchedulesObserver;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 38
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;->this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    iget-object v2, v2, Lcom/vlingo/midas/gui/PBSchedulesObserver;->cursor:Landroid/database/Cursor;

    if-eqz v2, :cond_2

    .line 39
    iget-object v2, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;->this$0:Lcom/vlingo/midas/gui/PBSchedulesObserver;

    iget-object v2, v2, Lcom/vlingo/midas/gui/PBSchedulesObserver;->cursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method
