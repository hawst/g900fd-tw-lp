.class Lcom/vlingo/midas/gui/DialogBubble$6;
.super Ljava/lang/Object;
.source "DialogBubble.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/DialogBubble;->setTalkbackString(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogBubble;

.field final synthetic val$editable:Z


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogBubble;Z)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    iput-boolean p2, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->val$editable:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 575
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$500(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/EditText;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setImportantForAccessibility(I)V

    .line 576
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->val$editable:Z

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$500(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/EditText;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->talkback_you_said:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;
    invoke-static {v2}, Lcom/vlingo/midas/gui/DialogBubble;->access$500(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/EditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->talkback_bubble_editable:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 588
    :goto_0
    return-void

    .line 584
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$500(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/EditText;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->talkback_you_said:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogBubble$6;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;
    invoke-static {v2}, Lcom/vlingo/midas/gui/DialogBubble;->access$500(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/EditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
