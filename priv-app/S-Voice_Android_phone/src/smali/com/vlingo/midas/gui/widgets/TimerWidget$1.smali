.class Lcom/vlingo/midas/gui/widgets/TimerWidget$1;
.super Ljava/lang/Object;
.source "TimerWidget.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/TimerWidget;->initialize(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/TimerWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/TimerWidget;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 148
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    .line 150
    sget-wide v1, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    long-to-double v1, v1

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-nez v1, :cond_0

    .line 161
    :goto_0
    return v0

    .line 154
    :cond_0
    sget-boolean v1, Lcom/vlingo/midas/gui/timer/TimerManager;->isStarted:Z

    if-eqz v1, :cond_1

    .line 155
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    # invokes: Lcom/vlingo/midas/gui/widgets/TimerWidget;->stop()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->access$000(Lcom/vlingo/midas/gui/widgets/TimerWidget;)V

    goto :goto_0

    .line 157
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TimerWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->start()V

    goto :goto_0

    .line 161
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
