.class Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;
.super Landroid/os/Handler;
.source "DialogBubble.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogBubble;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DialogBubbleHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/DialogBubble;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogBubble;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 87
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 88
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 91
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble$DialogBubbleHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    .line 92
    .local v0, "db":Lcom/vlingo/midas/gui/DialogBubble;
    if-eqz v0, :cond_0

    .line 93
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 94
    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->mListener:Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$100(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;

    move-result-object v1

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$000(Lcom/vlingo/midas/gui/DialogBubble;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;->onSlideTextStart(Ljava/lang/String;)V

    .line 95
    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$7;->$SwitchMap$com$vlingo$midas$gui$DialogBubble$BubbleType:[I

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->type:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$200(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 105
    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleUserText:Lcom/vlingo/midas/gui/customviews/EditText;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$500(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/EditText;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$color;->user_bubble:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/EditText;->setTextColor(I)V

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 99
    :pswitch_0
    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleVlingoText:Lcom/vlingo/midas/gui/customviews/TextView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$300(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/TextView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$color;->prompt_bubble:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/TextView;->setTextColor(I)V

    goto :goto_0

    .line 102
    :pswitch_1
    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$400(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/TextView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$color;->prompt_bubble_wakeup:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/TextView;->setTextColor(I)V

    goto :goto_0

    .line 110
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 111
    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->mListener:Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$100(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;

    move-result-object v1

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->text:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$000(Lcom/vlingo/midas/gui/DialogBubble;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;->onSlideTextEnd(Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
