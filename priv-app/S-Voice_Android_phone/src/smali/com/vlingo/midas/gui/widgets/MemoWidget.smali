.class public Lcom/vlingo/midas/gui/widgets/MemoWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "MemoWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/vlingo/midas/util/LPActionInterface;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/memo/Memo;",
        ">;",
        "Lcom/vlingo/midas/util/LPActionInterface;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private isClicked:Z

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private final mContext:Landroid/content/Context;

.field private mIntent:Landroid/content/Intent;

.field private memoContainer:Landroid/view/View;

.field protected msgBody:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->isClicked:Z

    .line 47
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method private launchMemo()V
    .locals 4

    .prologue
    .line 152
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MemoWidget;->retire()V

    .line 153
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_2

    .line 154
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->isInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 170
    :goto_0
    return-void

    .line 156
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->isInstalled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 166
    :catch_0
    move-exception v0

    goto :goto_0

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 162
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "memo.findmostrecent"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "memo.body"

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->msgBody:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public handleLPAction(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method public initialize(Lcom/vlingo/core/internal/memo/Memo;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 9
    .param p1, "memo"    # Lcom/vlingo/core/internal/memo/Memo;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 71
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 72
    invoke-virtual {p1}, Lcom/vlingo/core/internal/memo/Memo;->getText()Ljava/lang/String;

    move-result-object v3

    .line 75
    .local v3, "text":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "countryCode":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "en-US"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 78
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->msgBody:Landroid/widget/TextView;

    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 81
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    .line 83
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->msgBody:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/MemoManager;->getMemoUtil()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/internal/memo/IMemoUtil;->getCreateMemoAction()Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "actionName":Ljava/lang/String;
    const-string/jumbo v2, "voicetalk_string"

    .line 86
    .local v2, "extraName":Ljava/lang/String;
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    .line 87
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->msgBody:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 88
    .local v4, "title":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    invoke-virtual {v5, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    .end local v0    # "actionName":Ljava/lang/String;
    .end local v2    # "extraName":Ljava/lang/String;
    .end local v4    # "title":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MemoWidget;->invalidate()V

    .line 117
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MemoWidget;->requestLayout()V

    .line 118
    return-void

    .line 91
    :cond_2
    invoke-virtual {p1}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v5

    const/4 v6, -0x2

    if-ne v5, v6, :cond_3

    .line 92
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->msgBody:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iput-object v8, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    .line 94
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->memoContainer:Landroid/view/View;

    if-eqz v5, :cond_1

    .line 95
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->memoContainer:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 98
    :cond_3
    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 99
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MemoWidget;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$string;->no_text:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 104
    :cond_4
    :goto_1
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->msgBody:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    new-instance v5, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/MemoManager;->getMemoUtil()Lcom/vlingo/core/internal/memo/IMemoUtil;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/core/internal/memo/IMemoUtil;->getViewMemoAction()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    .line 107
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->isInstalled()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 108
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    sget-object v6, Lcom/vlingo/midas/samsungutils/utils/memo/KMemoUtil;->MEMO_BASE_URI:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v7

    int-to-long v7, v7

    invoke-static {v6, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 109
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SMemo2Util;->isInstalled()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/memo/SNote2Util;->isInstalled()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 110
    :cond_6
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    const-string/jumbo v6, "id"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_0

    .line 100
    :cond_7
    if-eqz v3, :cond_4

    const-string/jumbo v5, ".snb"

    invoke-virtual {v3, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 101
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    invoke-virtual {v3, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 112
    :cond_8
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->mIntent:Landroid/content/Intent;

    const-string/jumbo v6, "memoID"

    invoke-virtual {p1}, Lcom/vlingo/core/internal/memo/Memo;->getId()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 34
    check-cast p1, Lcom/vlingo/core/internal/memo/Memo;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MemoWidget;->initialize(Lcom/vlingo/core/internal/memo/Memo;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->isClicked:Z

    if-nez v0, :cond_0

    .line 181
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->isClicked:Z

    .line 182
    const/4 v0, 0x0

    .line 185
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->isClicked:Z

    goto :goto_0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x1

    return v0
.end method

.method public mustBeShown()Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->memoContainer:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 135
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/MemoWidget;->launchMemo()V

    .line 138
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 52
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 53
    sget v0, Lcom/vlingo/midas/R$id;->msg_body:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MemoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->msgBody:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/vlingo/midas/R$id;->memo_container1:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MemoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->memoContainer:Landroid/view/View;

    .line 56
    new-instance v0, Lcom/vlingo/midas/gui/widgets/MemoWidget$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/MemoWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/MemoWidget;)V

    const-wide/16 v1, 0xdac

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 62
    return-void
.end method

.method public retire()V
    .locals 0

    .prologue
    .line 190
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->retire()V

    .line 191
    return-void
.end method

.method public setListener()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->memoContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MemoWidget;->memoContainer:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    :cond_0
    return-void
.end method
