.class public final enum Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;
.super Ljava/lang/Enum;
.source "BargeInWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WordList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

.field public static final enum Alarm:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

.field public static final enum All:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

.field public static final enum Camera:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

.field public static final enum Music:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

.field public static final enum Radio:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

.field public static final enum Video:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

.field public static final enum VoiceTalkAll:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

.field public static final enum VoiceTalkSchedule:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    new-instance v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    const-string/jumbo v1, "VoiceTalkAll"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->VoiceTalkAll:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    new-instance v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    const-string/jumbo v1, "VoiceTalkSchedule"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->VoiceTalkSchedule:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    new-instance v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    const-string/jumbo v1, "All"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->All:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    new-instance v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    const-string/jumbo v1, "Alarm"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->Alarm:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    new-instance v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    const-string/jumbo v1, "Music"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->Music:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    new-instance v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    const-string/jumbo v1, "Radio"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->Radio:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    new-instance v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    const-string/jumbo v1, "Video"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->Video:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    new-instance v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    const-string/jumbo v1, "Camera"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->Camera:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    sget-object v1, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->VoiceTalkAll:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->VoiceTalkSchedule:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->All:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->Alarm:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->Music:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->Radio:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->Video:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->Camera:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->$VALUES:[Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 53
    const-class v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->$VALUES:[Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    invoke-virtual {v0}, [Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    return-object v0
.end method
