.class abstract Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "MessageWidgetBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
        ">;"
    }
.end annotation


# instance fields
.field contactName:Landroid/widget/TextView;

.field context:Landroid/content/Context;

.field listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field messagButtonDivider:Landroid/view/View;

.field messageButtonContainer:Landroid/widget/LinearLayout;

.field messageContainer:Landroid/widget/LinearLayout;

.field messageDate:Landroid/widget/TextView;

.field messageDate_img:Landroid/widget/ImageView;

.field messageReadBackButtonContainer:Landroid/widget/LinearLayout;

.field msgBody:Landroid/widget/TextView;

.field name:Ljava/lang/String;

.field protected receiverAddresses:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field subjRow:Landroid/widget/LinearLayout;

.field subject:Ljava/lang/String;

.field text:Ljava/lang/String;

.field titleFrom:Lcom/vlingo/midas/gui/customviews/TextView;

.field titleTo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->titleTo:Landroid/widget/TextView;

    .line 46
    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->titleFrom:Lcom/vlingo/midas/gui/customviews/TextView;

    .line 61
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->context:Landroid/content/Context;

    .line 62
    return-void
.end method

.method private getRecipientListAsString(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 216
    .local p1, "recipientNames":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;>;"
    const-string/jumbo v2, ""

    .line 217
    .local v2, "recipientString":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 218
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 219
    .local v1, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 220
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->comma:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->space:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 222
    :cond_0
    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 223
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 225
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 229
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_2
    return-object v2
.end method


# virtual methods
.method protected editInNativeSmsApp()V
    .locals 6

    .prologue
    .line 243
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->receiverAddresses:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 244
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->receiverAddresses:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 245
    .local v2, "receiverAddress":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.SENDTO"

    const-string/jumbo v4, "smsto"

    const/4 v5, 0x0

    invoke-static {v4, v2, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 246
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "sms_body"

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->msgBody:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 250
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "receiverAddress":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 15
    .param p1, "messageType"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 132
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 134
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->receiverAddresses:Ljava/util/ArrayList;

    .line 135
    if-nez p1, :cond_0

    .line 213
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->name()Ljava/lang/String;

    move-result-object v11

    .line 138
    .local v11, "widgetName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v8

    .line 139
    .local v8, "mtRecipients":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 141
    .local v9, "mtRecipientsAddress":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v8, :cond_8

    .line 142
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;

    .line 143
    .local v10, "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    invoke-virtual {v10}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 144
    invoke-virtual {v10}, Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;->getAddress()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v9, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 147
    .end local v10    # "recipient":Lcom/vlingo/core/internal/dialogmanager/types/RecipientType;
    :cond_2
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->receiverAddresses:Ljava/util/ArrayList;

    .line 148
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getRecipients()Ljava/util/List;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->getRecipientListAsString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->name:Ljava/lang/String;

    .line 153
    .end local v7    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getSubject()Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->subject:Ljava/lang/String;

    .line 154
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->subject:Ljava/lang/String;

    invoke-static {v12}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 155
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "<"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v14, Lcom/vlingo/midas/R$string;->core_safereader_subject:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->subject:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ">\r\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->text:Ljava/lang/String;

    .line 157
    :cond_3
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->text:Ljava/lang/String;

    invoke-static {v12}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_9

    .line 158
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->text:Ljava/lang/String;

    .line 163
    :goto_3
    sget v12, Lcom/vlingo/midas/R$id;->date_msg_detail:I

    invoke-virtual {p0, v12}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    iput-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate:Landroid/widget/TextView;

    .line 164
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate:Landroid/widget/TextView;

    if-eqz v12, :cond_6

    .line 165
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v12

    if-eqz v12, :cond_c

    .line 166
    const-string/jumbo v12, "ComposeMessage"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 167
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    :cond_4
    new-instance v3, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getTimeStamp()J

    move-result-wide v12

    invoke-direct {v3, v12, v13}, Ljava/util/Date;-><init>(J)V

    .line 170
    .local v3, "date":Ljava/util/Date;
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string/jumbo v12, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v5, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 171
    .local v5, "formatDate":Ljava/text/SimpleDateFormat;
    invoke-virtual {v5, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 172
    .local v1, "InputDate":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/schedule/DateUtil;->isToday(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 173
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate:Landroid/widget/TextView;

    if-eqz v12, :cond_5

    .line 174
    invoke-virtual {v3}, Ljava/util/Date;->getHours()I

    move-result v12

    const/16 v13, 0xa

    if-ge v12, v13, :cond_a

    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->context:Landroid/content/Context;

    invoke-static {v12}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 175
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate:Landroid/widget/TextView;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "0"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v3}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    :cond_5
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getType()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getType()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "MMS"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 188
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate_img:Landroid/widget/ImageView;

    if-eqz v12, :cond_6

    .line 189
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate_img:Landroid/widget/ImageView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 197
    .end local v1    # "InputDate":Ljava/lang/String;
    .end local v3    # "date":Ljava/util/Date;
    .end local v5    # "formatDate":Ljava/text/SimpleDateFormat;
    :cond_6
    :goto_5
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v2

    .line 199
    .local v2, "countryCode":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_7

    const-string/jumbo v12, "en-US"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 210
    :cond_7
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->contactName:Landroid/widget/TextView;

    iget-object v13, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->name:Ljava/lang/String;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->msgBody:Landroid/widget/TextView;

    iget-object v13, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->text:Ljava/lang/String;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 150
    .end local v2    # "countryCode":Ljava/lang/String;
    :cond_8
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    sget v13, Lcom/vlingo/midas/R$string;->message:I

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->name:Ljava/lang/String;

    goto/16 :goto_2

    .line 160
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getMessage()Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->text:Ljava/lang/String;

    goto/16 :goto_3

    .line 177
    .restart local v1    # "InputDate":Ljava/lang/String;
    .restart local v3    # "date":Ljava/util/Date;
    .restart local v5    # "formatDate":Ljava/text/SimpleDateFormat;
    :cond_a
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatDisplayTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 182
    :cond_b
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v12, "dd/MM"

    invoke-direct {v4, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 183
    .local v4, "dateMonthFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v4, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 184
    .local v6, "formatted":Ljava/lang/String;
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate:Landroid/widget/TextView;

    invoke-virtual {v12, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 193
    .end local v1    # "InputDate":Ljava/lang/String;
    .end local v3    # "date":Ljava/util/Date;
    .end local v4    # "dateMonthFormat":Ljava/text/SimpleDateFormat;
    .end local v5    # "formatDate":Ljava/text/SimpleDateFormat;
    .end local v6    # "formatted":Ljava/lang/String;
    :cond_c
    iget-object v12, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 36
    check-cast p1, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 66
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 67
    sget v1, Lcom/vlingo/midas/R$id;->message_container:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageContainer:Landroid/widget/LinearLayout;

    .line 68
    sget v1, Lcom/vlingo/midas/R$id;->message_button_container:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageButtonContainer:Landroid/widget/LinearLayout;

    .line 69
    sget v1, Lcom/vlingo/midas/R$id;->message_readback_button_container:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageReadBackButtonContainer:Landroid/widget/LinearLayout;

    .line 70
    sget v1, Lcom/vlingo/midas/R$id;->btn_divider:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messagButtonDivider:Landroid/view/View;

    .line 72
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 73
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messagButtonDivider:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messagButtonDivider:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 77
    :cond_0
    sget v1, Lcom/vlingo/midas/R$id;->date_msg_image:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate_img:Landroid/widget/ImageView;

    .line 78
    sget v1, Lcom/vlingo/midas/R$id;->date_msg_image:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->messageDate_img:Landroid/widget/ImageView;

    .line 79
    sget v1, Lcom/vlingo/midas/R$id;->message_divider:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 82
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 83
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 84
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 86
    :cond_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 90
    .end local v0    # "view":Landroid/view/View;
    :cond_2
    sget v1, Lcom/vlingo/midas/R$id;->message_title_from_to:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/vlingo/midas/gui/customviews/TextView;

    if-eqz v1, :cond_4

    .line 91
    sget v1, Lcom/vlingo/midas/R$id;->message_title_from_to:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/customviews/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->titleFrom:Lcom/vlingo/midas/gui/customviews/TextView;

    .line 96
    :cond_3
    :goto_0
    sget v1, Lcom/vlingo/midas/R$id;->contact_name:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->contactName:Landroid/widget/TextView;

    .line 116
    sget v1, Lcom/vlingo/midas/R$id;->msg_body:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->msgBody:Landroid/widget/TextView;

    .line 117
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->msgBody:Landroid/widget/TextView;

    new-instance v2, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase$1;-><init>(Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 128
    return-void

    .line 92
    :cond_4
    sget v1, Lcom/vlingo/midas/R$id;->message_title_from_to:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 93
    sget v1, Lcom/vlingo/midas/R$id;->message_title_from_to:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->titleTo:Landroid/widget/TextView;

    goto :goto_0
.end method

.method protected openNativeSmsApp()V
    .locals 6

    .prologue
    .line 233
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->receiverAddresses:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 234
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->receiverAddresses:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 235
    .local v2, "receiverAddress":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.SENDTO"

    const-string/jumbo v4, "smsto"

    const/4 v5, 0x0

    invoke-static {v4, v2, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 236
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 237
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 241
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "receiverAddress":Ljava/lang/String;
    :cond_0
    return-void
.end method
