.class Lcom/vlingo/midas/gui/ConversationActivity$8;
.super Ljava/lang/Object;
.source "ConversationActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/ConversationActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ConversationActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0

    .prologue
    .line 2154
    iput-object p1, p0, Lcom/vlingo/midas/gui/ConversationActivity$8;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 2158
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$8;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2159
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->showingNotifications()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2162
    const-string/jumbo v0, "ConversationActivity"

    const-string/jumbo v1, "[LatencyCheck] setTaskOnGetAudioFocus & mhandler.sendEmptyMessage(MSG_START_RECORDING)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2163
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$8;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessage(I)Z

    .line 2165
    :cond_0
    return-void
.end method
