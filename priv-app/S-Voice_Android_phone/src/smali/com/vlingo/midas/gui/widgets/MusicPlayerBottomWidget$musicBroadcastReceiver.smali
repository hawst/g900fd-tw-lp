.class public Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MusicPlayerBottomWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "musicBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 116
    if-eqz p2, :cond_0

    .line 117
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 118
    .local v7, "action":Ljava/lang/String;
    const-string/jumbo v1, "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # setter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->isPlaying:Z
    invoke-static {v1, v5}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$102(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Z)Z

    .line 121
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    move-object v1, p1

    check-cast v1, Landroid/app/Activity;

    sget v4, Lcom/vlingo/midas/R$id;->dialog_full_container:I

    invoke-virtual {v1, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    # setter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;
    invoke-static {v3, v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$202(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Lcom/vlingo/midas/gui/customviews/RelativeLayout;)Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    .line 123
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    move-object v1, p1

    check-cast v1, Landroid/app/Activity;

    sget v4, Lcom/vlingo/midas/R$id;->dialog_scroll_content:I

    invoke-virtual {v1, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    # setter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v3, v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$302(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 125
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    sget v1, Lcom/vlingo/midas/R$id;->dialog_scrollview:I

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    # setter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v3, v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$402(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Landroid/widget/ScrollView;)Landroid/widget/ScrollView;

    .line 127
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$300(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/LinearLayout;

    move-result-object v1

    const/16 v3, 0xf

    invoke-virtual {v1, v5, v5, v5, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 128
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$400(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/ScrollView;

    move-result-object v1

    const/16 v3, 0x51

    invoke-virtual {v1, v5, v5, v5, v3}, Landroid/widget/ScrollView;->setPadding(IIII)V

    .line 129
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioTemplateRLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 208
    .end local v7    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 132
    .restart local v7    # "action":Ljava/lang/String;
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    const-string/jumbo v1, "com.android.music.metachanged"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 133
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mTitle:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$500(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 134
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mTitle:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$500(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "track"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    :cond_2
    sget-object v1, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->MusicPlusUtil:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v1}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->get(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)Ljava/lang/Class;

    move-result-object v11

    .line 138
    .local v11, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    const/4 v12, 0x0

    .line 139
    .local v12, "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    if-eqz v11, :cond_3

    .line 141
    :try_start_0
    invoke-virtual {v11}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/vlingo/core/internal/util/MusicPlusUtil;

    move-object v12, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 148
    :cond_3
    :goto_1
    const/4 v9, 0x0

    .line 150
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 151
    .local v2, "contentUri":Landroid/net/Uri;
    if-eqz v12, :cond_5

    .line 152
    :try_start_1
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->GENERAL:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v12, v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v2

    .line 155
    :goto_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 157
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 159
    :cond_4
    const-string/jumbo v1, "album"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "album"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 164
    :goto_3
    const/4 v8, 0x0

    .line 165
    .local v8, "albumartUri":Landroid/net/Uri;
    if-eqz v12, :cond_7

    .line 166
    sget-object v1, Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;->ALBUMART:Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;

    invoke-interface {v12, v1}, Lcom/vlingo/core/internal/util/MusicPlusUtil;->getMusicUri(Lcom/vlingo/core/internal/util/MusicPlusUtil$PlayType;)Landroid/net/Uri;

    move-result-object v8

    .line 169
    :goto_4
    const-string/jumbo v1, "album_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v8, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v13

    .line 173
    .local v13, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mThumbnail:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$600(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v13}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V
    :try_end_1
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    if-eqz v9, :cond_0

    .line 178
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 179
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 142
    .end local v2    # "contentUri":Landroid/net/Uri;
    .end local v8    # "albumartUri":Landroid/net/Uri;
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v13    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v10

    .line 143
    .local v10, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v10}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1

    .line 144
    .end local v10    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v10

    .line 145
    .local v10, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v10}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 154
    .end local v10    # "e":Ljava/lang/IllegalAccessException;
    .restart local v2    # "contentUri":Landroid/net/Uri;
    .restart local v9    # "cursor":Landroid/database/Cursor;
    :cond_5
    :try_start_2
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_2

    .line 163
    :cond_6
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_3

    .line 168
    .restart local v8    # "albumartUri":Landroid/net/Uri;
    :cond_7
    const-string/jumbo v1, "content://media/external/audio/albumart/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_2
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    goto :goto_4

    .line 174
    .end local v8    # "albumartUri":Landroid/net/Uri;
    :catch_2
    move-exception v10

    .line 175
    .local v10, "e":Landroid/database/CursorIndexOutOfBoundsException;
    :try_start_3
    invoke-virtual {v10}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 177
    if-eqz v9, :cond_0

    .line 178
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 179
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 177
    .end local v10    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_8

    .line 178
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 179
    const/4 v9, 0x0

    :cond_8
    throw v1

    .line 186
    .end local v2    # "contentUri":Landroid/net/Uri;
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v11    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/core/internal/CoreAdapter;>;"
    .end local v12    # "musicPlusUtil":Lcom/vlingo/core/internal/util/MusicPlusUtil;
    :cond_9
    const-string/jumbo v1, "com.sec.android.music.musicservicecommnad.mediainfo"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v3, "isStopped"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 189
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mAudioTemplateRLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 191
    :cond_a
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v3, "isPlaying"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 192
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    const/4 v3, 0x1

    # setter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->isPlaying:Z
    invoke-static {v1, v3}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$102(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Z)Z

    .line 194
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 197
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$700(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getMusicLaunchedFromSvoice()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 198
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$700(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v3, 0x2bc

    invoke-virtual {v1, v5, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 205
    :cond_b
    :goto_5
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->isPlaying:Z
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;)Z

    move-result v3

    # invokes: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->setPlayingButton(Z)V
    invoke-static {v1, v3}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$800(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Z)V

    goto/16 :goto_0

    .line 203
    :cond_c
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget$musicBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;

    # setter for: Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->isPlaying:Z
    invoke-static {v1, v5}, Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;->access$102(Lcom/vlingo/midas/gui/widgets/MusicPlayerBottomWidget;Z)Z

    goto :goto_5
.end method
