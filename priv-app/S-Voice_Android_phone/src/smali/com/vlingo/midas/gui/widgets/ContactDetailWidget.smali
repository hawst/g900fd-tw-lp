.class public Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "ContactDetailWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/contacts/ContactMatch;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private contactList:Landroid/widget/ListView;

.field private isDetailsToShow:Z

.field protected phoneType:Ljava/lang/String;

.field private uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    const-string/jumbo v0, "call"

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->phoneType:Ljava/lang/String;

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->isDetailsToShow:Z

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->launchNativeContact()V

    return-void
.end method

.method private filterAddress(Lcom/vlingo/core/internal/contacts/ContactData;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Z
    .locals 4
    .param p1, "address"    # Lcom/vlingo/core/internal/contacts/ContactData;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 227
    if-nez p2, :cond_1

    .line 234
    :cond_0
    :goto_0
    return v0

    .line 228
    :cond_1
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomeAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p1, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-eq v2, v0, :cond_2

    move v0, v1

    .line 229
    goto :goto_0

    .line 231
    :cond_2
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p1, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 232
    goto :goto_0
.end method

.method private filterEmail(Lcom/vlingo/core/internal/contacts/ContactData;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Z
    .locals 4
    .param p1, "email"    # Lcom/vlingo/core/internal/contacts/ContactData;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 238
    if-nez p2, :cond_1

    .line 245
    :cond_0
    :goto_0
    return v0

    .line 239
    :cond_1
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomeEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p1, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    if-eq v2, v0, :cond_2

    move v0, v1

    .line 240
    goto :goto_0

    .line 242
    :cond_2
    sget-object v2, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v2}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p1, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 243
    goto :goto_0
.end method

.method private getContactPhoto(Ljava/lang/Long;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "contactID"    # Ljava/lang/Long;

    .prologue
    .line 330
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->uri:Landroid/net/Uri;

    .line 331
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->uri:Landroid/net/Uri;

    invoke-static {v2, v3}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 332
    .local v0, "is":Ljava/io/InputStream;
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 333
    .local v1, "photo":Landroid/graphics/Bitmap;
    return-object v1
.end method

.method private getPhoneType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Ljava/lang/String;
    .locals 1
    .param p1, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;

    .prologue
    .line 213
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->phoneType:Ljava/lang/String;

    .line 223
    :goto_0
    return-object v0

    .line 214
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowHomePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getTypeStringEN(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 217
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowMobilePhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getTypeStringEN(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 220
    :cond_2
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowWorkPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p1, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 221
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ContactUtil;->getTypeStringEN(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 223
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->phoneType:Ljava/lang/String;

    goto :goto_0
.end method

.method private launchNativeContact()V
    .locals 3

    .prologue
    .line 411
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->uri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 412
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 413
    return-void
.end method


# virtual methods
.method public getRoundedShape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v8, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    .line 303
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v9

    if-nez v9, :cond_0

    .line 326
    .end local p1    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object p1

    .line 305
    .restart local p1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_1
    move-object p1, v8

    .line 306
    goto :goto_0

    .line 308
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$dimen;->contact_border_width:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v0, v9

    .line 309
    .local v0, "borderwidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    mul-int/lit8 v10, v0, 0x2

    add-int v7, v9, v10

    .line 310
    .local v7, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    mul-int/lit8 v10, v0, 0x2

    add-int v3, v9, v10

    .line 312
    .local v3, "height":I
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v3, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 313
    .local v2, "canvasBitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/BitmapShader;

    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v10, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v6, p1, v9, v10}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 314
    .local v6, "shader":Landroid/graphics/BitmapShader;
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 315
    .local v4, "paint":Landroid/graphics/Paint;
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 316
    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 318
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 319
    .local v1, "canvas":Landroid/graphics/Canvas;
    if-le v7, v3, :cond_3

    int-to-float v9, v3

    div-float v5, v9, v11

    .line 320
    .local v5, "radius":F
    :goto_1
    div-int/lit8 v9, v7, 0x2

    int-to-float v9, v9

    div-int/lit8 v10, v3, 0x2

    int-to-float v10, v10

    invoke-virtual {v1, v9, v10, v5, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 321
    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 322
    sget-object v8, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 323
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$color;->contact_border_color:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 324
    int-to-float v8, v0

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 325
    div-int/lit8 v8, v7, 0x2

    int-to-float v8, v8

    div-int/lit8 v9, v3, 0x2

    int-to-float v9, v9

    int-to-float v10, v0

    sub-float v10, v5, v10

    invoke-virtual {v1, v8, v9, v10, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    move-object p1, v2

    .line 326
    goto :goto_0

    .line 319
    .end local v5    # "radius":F
    :cond_3
    int-to-float v9, v7

    div-float v5, v9, v11

    goto :goto_1
.end method

.method public initialize(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 29
    .param p1, "contacts"    # Lcom/vlingo/core/internal/contacts/ContactMatch;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 88
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .local v23, "titleInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v8, "allContactDetail":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz p1, :cond_d

    .line 92
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->showHeaderView(Lcom/vlingo/core/internal/contacts/ContactMatch;)V

    .line 93
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v21

    .line 94
    .local v21, "phoneNumber":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getEmailData()Ljava/util/List;

    move-result-object v15

    .line 95
    .local v15, "emailAddress":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getAddressData()Ljava/util/List;

    move-result-object v5

    .line 96
    .local v5, "address":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getBirthdayData()Ljava/util/List;

    move-result-object v9

    .line 97
    .local v9, "birthday":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/4 v11, 0x0

    .line 98
    .local v11, "contactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    const/16 v20, 0x0

    .line 99
    .local v20, "phoneLabels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v19, 0x0

    .line 100
    .local v19, "phoneData":Lcom/vlingo/core/internal/contacts/ContactData;
    const/16 v17, 0x0

    .line 101
    .local v17, "emailData":Lcom/vlingo/core/internal/contacts/ContactData;
    const/4 v6, 0x0

    .line 102
    .local v6, "addressData":Lcom/vlingo/core/internal/contacts/ContactData;
    const/4 v10, 0x0

    .line 103
    .local v10, "birthdayData":Lcom/vlingo/core/internal/contacts/ContactData;
    const/4 v14, 0x0

    .line 105
    .local v14, "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v21, :cond_3

    if-eqz p2, :cond_0

    sget-object v25, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowPhone:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 106
    :cond_0
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_1

    .line 107
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getPhoneType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeMap(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v14

    .line 108
    invoke-interface {v14}, Ljava/util/Map;->isEmpty()Z

    move-result v25

    if-eqz v25, :cond_1

    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getPhoneType(Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/vlingo/core/internal/util/DialUtil;->getPhoneTypeMap(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v14

    .line 112
    :cond_1
    if-eqz v14, :cond_3

    .line 113
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->isDetailsToShow:Z

    .line 114
    const/16 v22, 0x0

    .line 115
    .local v22, "phoneNumberSize":I
    if-eqz v21, :cond_2

    .line 116
    invoke-interface {v14}, Ljava/util/Map;->size()I

    move-result v22

    .line 118
    :cond_2
    new-instance v11, Ljava/util/ArrayList;

    .end local v11    # "contactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    invoke-interface {v14}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 119
    .restart local v11    # "contactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    new-instance v20, Ljava/util/ArrayList;

    .end local v20    # "phoneLabels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v25

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 120
    .restart local v20    # "phoneLabels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v22

    if-ge v0, v1, :cond_3

    .line 121
    move/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 122
    .local v13, "contactInfo":Lcom/vlingo/core/internal/contacts/ContactData;
    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 124
    .local v24, "type":Ljava/lang/String;
    invoke-interface/range {v23 .. v24}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 129
    .end local v13    # "contactInfo":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v18    # "i":I
    .end local v22    # "phoneNumberSize":I
    .end local v24    # "type":Ljava/lang/String;
    :cond_3
    if-eqz v5, :cond_6

    if-eqz p2, :cond_4

    sget-object v25, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowAddress:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 130
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    .line 131
    .local v7, "addressSize":I
    const/16 v18, 0x0

    .restart local v18    # "i":I
    :goto_1
    move/from16 v0, v18

    if-ge v0, v7, :cond_6

    .line 132
    move/from16 v0, v18

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/vlingo/core/internal/contacts/ContactData;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->filterAddress(Lcom/vlingo/core/internal/contacts/ContactData;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 133
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->isDetailsToShow:Z

    .line 134
    move/from16 v0, v18

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    move/from16 v0, v18

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/vlingo/core/internal/contacts/ContactData;

    move-object/from16 v0, v25

    iget v0, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    move/from16 v25, v0

    packed-switch v25, :pswitch_data_0

    .line 131
    :cond_5
    :goto_2
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 137
    :pswitch_0
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->title_address:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string/jumbo v26, " / "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move/from16 v0, v18

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/vlingo/core/internal/contacts/ContactData;

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 140
    :pswitch_1
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->title_address:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string/jumbo v26, " / "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->phone_type_home:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 143
    :pswitch_2
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->title_address:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string/jumbo v26, " / "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->phone_type_work:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 146
    :pswitch_3
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->title_address:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string/jumbo v26, " / "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->phone_type_other:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 155
    .end local v7    # "addressSize":I
    .end local v18    # "i":I
    :cond_6
    if-eqz v15, :cond_9

    if-eqz p2, :cond_7

    sget-object v25, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowEmail:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 156
    :cond_7
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v16

    .line 157
    .local v16, "emailAddressSize":I
    const/16 v18, 0x0

    .restart local v18    # "i":I
    :goto_3
    move/from16 v0, v18

    move/from16 v1, v16

    if-ge v0, v1, :cond_9

    .line 158
    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/vlingo/core/internal/contacts/ContactData;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->filterEmail(Lcom/vlingo/core/internal/contacts/ContactData;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 159
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->isDetailsToShow:Z

    .line 160
    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/vlingo/core/internal/contacts/ContactData;

    move-object/from16 v0, v25

    iget v0, v0, Lcom/vlingo/core/internal/contacts/ContactData;->type:I

    move/from16 v25, v0

    packed-switch v25, :pswitch_data_1

    .line 177
    :goto_4
    const-string/jumbo v25, "custom"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    :cond_8
    :goto_5
    add-int/lit8 v18, v18, 0x1

    goto :goto_3

    .line 163
    :pswitch_4
    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/vlingo/core/internal/contacts/ContactData;

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vlingo/core/internal/contacts/ContactData;->label:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 166
    :pswitch_5
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->title_email:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string/jumbo v26, " / "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->email_type_home:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 169
    :pswitch_6
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->title_email:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string/jumbo v26, " / "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->phone_type_work:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 172
    :pswitch_7
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->title_email:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string/jumbo v26, " / "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->phone_type_other:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 175
    :pswitch_8
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->title_email:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string/jumbo v26, " / "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->phone_type_mobile:I

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 184
    .end local v16    # "emailAddressSize":I
    .end local v18    # "i":I
    :cond_9
    if-eqz v9, :cond_b

    if-eqz p2, :cond_a

    sget-object v25, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ContactShowBirthday:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 185
    :cond_a
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "birthdayData":Lcom/vlingo/core/internal/contacts/ContactData;
    check-cast v10, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 186
    .restart local v10    # "birthdayData":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v10, :cond_b

    .line 187
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->isDetailsToShow:Z

    .line 188
    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    sget v26, Lcom/vlingo/midas/R$string;->title_birthday:I

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    :cond_b
    sget v25, Lcom/vlingo/midas/R$id;->contact_detail_list_container:I

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 194
    .local v12, "contactDetailListContainer":Landroid/widget/LinearLayout;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v25

    if-eqz v25, :cond_c

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->isDetailsToShow:Z

    move/from16 v25, v0

    if-nez v25, :cond_c

    .line 195
    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 196
    :cond_c
    new-instance v4, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getContext()Landroid/content/Context;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-direct {v4, v0, v1, v8, v2}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    .line 197
    .local v4, "adapter":Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->contactList:Landroid/widget/ListView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->contactList:Landroid/widget/ListView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 199
    invoke-virtual {v4}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;->notifyDataSetChanged()V

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->contactList:Landroid/widget/ListView;

    move-object/from16 v25, v0

    new-instance v26, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$1;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;)V

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 210
    .end local v4    # "adapter":Lcom/vlingo/midas/gui/widgets/ContactDetailWidget$ContactAdapter;
    .end local v5    # "address":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v6    # "addressData":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v9    # "birthday":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v10    # "birthdayData":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v11    # "contactData":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v12    # "contactDetailListContainer":Landroid/widget/LinearLayout;
    .end local v14    # "contactMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v15    # "emailAddress":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    .end local v17    # "emailData":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v19    # "phoneData":Lcom/vlingo/core/internal/contacts/ContactData;
    .end local v20    # "phoneLabels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v21    # "phoneNumber":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    :cond_d
    return-void

    .line 135
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 161
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 49
    check-cast p1, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->initialize(Lcom/vlingo/core/internal/contacts/ContactMatch;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 431
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 425
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 417
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 418
    .local v0, "id":I
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->launchNativeContact()V

    .line 419
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->retire()V

    .line 420
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 68
    sget v0, Lcom/vlingo/midas/R$id;->contact_detail_list:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->contactList:Landroid/widget/ListView;

    .line 69
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->contactList:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 77
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 78
    return-void
.end method

.method public showHeaderView(Lcom/vlingo/core/internal/contacts/ContactMatch;)V
    .locals 14
    .param p1, "contacts"    # Lcom/vlingo/core/internal/contacts/ContactMatch;

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v11, 0x0

    .line 250
    sget v8, Lcom/vlingo/midas/R$id;->image_contact_detail:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 251
    .local v6, "qcb":Landroid/widget/ImageView;
    sget v8, Lcom/vlingo/midas/R$id;->text_title_name:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 252
    .local v7, "title_name":Landroid/widget/TextView;
    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    const/4 v3, 0x0

    .line 254
    .local v3, "img_cnt":I
    const/4 v8, 0x5

    new-array v1, v8, [I

    sget v8, Lcom/vlingo/midas/R$drawable;->contacts_default_image_01:I

    aput v8, v1, v11

    sget v8, Lcom/vlingo/midas/R$drawable;->contacts_default_image_02:I

    aput v8, v1, v9

    sget v8, Lcom/vlingo/midas/R$drawable;->contacts_default_image_03:I

    aput v8, v1, v10

    sget v8, Lcom/vlingo/midas/R$drawable;->contacts_default_image_04:I

    aput v8, v1, v12

    sget v8, Lcom/vlingo/midas/R$drawable;->contacts_default_image_05:I

    aput v8, v1, v13

    .line 261
    .local v1, "DEFAULT_IMG":[I
    const/4 v8, 0x5

    new-array v0, v8, [I

    sget v8, Lcom/vlingo/midas/R$color;->contact_img_1_bg_color:I

    aput v8, v0, v11

    sget v8, Lcom/vlingo/midas/R$color;->contact_img_2_bg_color:I

    aput v8, v0, v9

    sget v8, Lcom/vlingo/midas/R$color;->contact_img_3_bg_color:I

    aput v8, v0, v10

    sget v8, Lcom/vlingo/midas/R$color;->contact_img_4_bg_color:I

    aput v8, v0, v12

    sget v8, Lcom/vlingo/midas/R$color;->contact_img_5_bg_color:I

    aput v8, v0, v13

    .line 269
    .local v0, "DEFAULT_COLOR":[I
    iget-wide v8, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getContactPhoto(Ljava/lang/Long;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 270
    .local v4, "photo":Landroid/graphics/Bitmap;
    if-nez v4, :cond_3

    .line 271
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 272
    sget-object v8, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->contactslist:Ljava/util/HashMap;

    if-eqz v8, :cond_1

    sget-object v8, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->contactslist:Ljava/util/HashMap;

    iget-wide v9, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 273
    sget-object v8, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->contactslist:Ljava/util/HashMap;

    iget-wide v9, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryContactID:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 274
    .local v5, "position":I
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    aget v9, v1, v11

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 276
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getRoundedShape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 277
    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 278
    add-int/lit8 v3, v3, 0x1

    .line 298
    .end local v5    # "position":I
    :goto_0
    iget-object v8, p1, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    return-void

    .line 280
    .restart local v5    # "position":I
    :cond_0
    rem-int/lit8 v8, v5, 0x5

    aget v8, v0, v8

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 285
    .end local v5    # "position":I
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    aget v9, v1, v11

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 286
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getRoundedShape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 287
    sget v8, Lcom/vlingo/midas/R$color;->contact_img_1_bg_color:I

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 291
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$drawable;->contacts_default_image_01:I

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 292
    .local v2, "imageBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 295
    .end local v2    # "imageBitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;->getRoundedShape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 296
    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
