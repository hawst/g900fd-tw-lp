.class public Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;
.super Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;
.source "MultipleSenderMessageReadoutWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$ListAdapterLineItems;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private summaries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageReadoutType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->summaries:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 37
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 3
    .param p1, "s"    # Ljava/util/List;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->summaries:Ljava/util/List;

    .line 58
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 59
    if-eqz p2, :cond_0

    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ShowMessageBody:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->showMessageBody:Z

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->summaries:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->summaries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMaxDisplayNumber()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 65
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->summaries:Ljava/util/List;

    const/4 v1, 0x0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMaxDisplayNumber()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->summaries:Ljava/util/List;

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;->mMultipleMessages:Landroid/widget/ListView;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleSenderMessageReadoutWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 78
    :cond_2
    return-void
.end method
