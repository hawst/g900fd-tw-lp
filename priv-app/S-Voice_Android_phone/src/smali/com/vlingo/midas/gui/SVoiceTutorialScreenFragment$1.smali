.class Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment$1;
.super Ljava/lang/Object;
.source "SVoiceTutorialScreenFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->svoiceTutorialScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment$1;->this$0:Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 197
    iget-object v2, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment$1;->this$0:Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;

    iget-object v3, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment$1;->this$0:Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    # setter for: Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v2, v3}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->access$002(Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 198
    iget-object v2, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment$1;->this$0:Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;

    # getter for: Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->access$000(Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 199
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "showing_notifications"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 200
    const-string/jumbo v2, "is_move_to_svoice"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 201
    const-string/jumbo v2, "is_tutorial_completed"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 202
    sput-boolean v4, Lcom/vlingo/midas/gui/ConversationActivity;->doLaunchTutorial:Z

    .line 203
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 204
    const-string/jumbo v2, "all_notifications_accepted"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 205
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment$1;->this$0:Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/VlingoApplication;->getMainActivityClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 206
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v2, "isLaunchFromTutorialIUX"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 207
    sget-object v2, Lcom/vlingo/midas/gui/DataCheckActivity;->DIALOG_PROCESSED:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208
    invoke-static {v5}, Lcom/vlingo/midas/settings/MidasSettings;->setShowAgainBackgroundData(Z)V

    .line 209
    iget-object v2, p0, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment$1;->this$0:Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;

    invoke-virtual {v2, v1}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;->startActivity(Landroid/content/Intent;)V

    .line 211
    return-void
.end method
