.class public Lcom/vlingo/midas/gui/customviews/DrawAnimationTablet;
.super Ljava/lang/Object;
.source "DrawAnimationTablet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V
    .locals 6
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "cornerBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "squareImage"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p3, "mRms"    # F

    .prologue
    const/high16 v5, 0x420c0000    # 35.0f

    const/high16 v4, 0x41700000    # 15.0f

    const/high16 v3, 0x42be0000    # 95.0f

    const/high16 v1, 0x425c0000    # 55.0f

    const/4 v2, 0x0

    .line 144
    const/high16 v0, 0x41f80000    # 31.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 145
    const v0, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 146
    const v0, 0x43938000    # 295.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 147
    const/high16 v0, 0x438d0000    # 282.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 148
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x438a0000    # 276.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 149
    const/high16 v0, 0x438d0000    # 282.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 150
    const/high16 v0, 0x42e60000    # 115.0f

    const v1, 0x43938000    # 295.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 151
    const/high16 v0, 0x43070000    # 135.0f

    const v1, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    const/high16 v0, 0x42140000    # 37.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 155
    const v0, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 156
    const v0, 0x438b8000    # 279.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 157
    const v0, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 158
    const/high16 v0, 0x42960000    # 75.0f

    const v1, 0x43818000    # 259.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 159
    const v0, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 160
    const/high16 v0, 0x42e60000    # 115.0f

    const v1, 0x438b8000    # 279.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 161
    const/high16 v0, 0x43070000    # 135.0f

    const v1, 0x43868000    # 269.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 163
    const/16 v0, 0xf

    const/16 v1, 0x122

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 164
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 166
    :cond_2
    const/high16 v0, 0x422c0000    # 43.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_3

    .line 167
    const/high16 v0, 0x43590000    # 217.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 168
    const/high16 v0, 0x436c0000    # 236.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 169
    const/high16 v0, 0x43730000    # 243.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 170
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x436c0000    # 236.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 171
    const/high16 v0, 0x43590000    # 217.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 172
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x43730000    # 243.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 173
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x436c0000    # 236.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 175
    const/16 v0, 0xf

    const/16 v1, 0xfa

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 176
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 179
    :cond_3
    const/high16 v0, 0x42440000    # 49.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    .line 180
    const/high16 v0, 0x43590000    # 217.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 181
    const/high16 v0, 0x43460000    # 198.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 182
    const/high16 v0, 0x433f0000    # 191.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 183
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x43460000    # 198.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 184
    const/high16 v0, 0x43590000    # 217.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 185
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x43590000    # 217.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 186
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x433f0000    # 191.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 188
    const/16 v0, 0xf

    const/16 v1, 0xdc

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 189
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 192
    :cond_4
    cmpg-float v0, p3, v1

    if-gez v0, :cond_5

    .line 193
    const/high16 v0, 0x432c0000    # 172.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 194
    const/high16 v0, 0x43380000    # 184.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 195
    const/high16 v0, 0x433f0000    # 191.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 196
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x43380000    # 184.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 197
    const/high16 v0, 0x43250000    # 165.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 198
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x433f0000    # 191.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 199
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x43380000    # 184.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 201
    const/16 v0, 0xf

    const/16 v1, 0xc8

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 202
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 204
    :cond_5
    const/high16 v0, 0x42740000    # 61.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6

    .line 205
    const/high16 v0, 0x43110000    # 145.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 206
    const/high16 v0, 0x431e0000    # 158.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 207
    const/high16 v0, 0x43250000    # 165.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 208
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x431e0000    # 158.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 209
    const/high16 v0, 0x430b0000    # 139.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 210
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x43250000    # 165.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 211
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x431e0000    # 158.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 213
    const/16 v0, 0xf

    const/16 v1, 0xaa

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 214
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 216
    :cond_6
    const/high16 v0, 0x42860000    # 67.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_7

    .line 217
    const/high16 v0, 0x42ee0000    # 119.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 218
    const/high16 v0, 0x430b0000    # 139.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 219
    const/high16 v0, 0x42fc0000    # 126.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 220
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x42e20000    # 113.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 221
    const/high16 v0, 0x42fc0000    # 126.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 222
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x43040000    # 132.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 223
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x42fc0000    # 126.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 225
    const/16 v0, 0xf

    const/16 v1, 0x8c

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 226
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 228
    :cond_7
    const/high16 v0, 0x42920000    # 73.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_8

    .line 229
    const/high16 v0, 0x42ae0000    # 87.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 230
    const/high16 v0, 0x42e20000    # 113.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 231
    const/high16 v0, 0x42ee0000    # 119.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 232
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x42fc0000    # 126.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 233
    const/high16 v0, 0x42ee0000    # 119.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 234
    const/high16 v0, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 235
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x42ae0000    # 87.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 237
    const/16 v0, 0xf

    const/16 v1, 0x82

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 238
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 241
    :cond_8
    const/high16 v0, 0x429e0000    # 79.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_9

    .line 242
    const/high16 v0, 0x42740000    # 61.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 243
    const/high16 v0, 0x42ae0000    # 87.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 244
    const/high16 v0, 0x42ae0000    # 87.0f

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 245
    const/high16 v0, 0x42960000    # 75.0f

    const/high16 v1, 0x42940000    # 74.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 246
    const/high16 v0, 0x42ae0000    # 87.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 247
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 248
    const/high16 v0, 0x43070000    # 135.0f

    const/high16 v1, 0x42740000    # 61.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 250
    const/16 v0, 0xf

    const/16 v1, 0x5a

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 251
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 255
    :cond_9
    const/high16 v0, 0x429e0000    # 79.0f

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 256
    const/16 v0, 0xf

    const/16 v1, 0x28

    const/16 v2, 0xb4

    const/16 v3, 0x140

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 257
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method public static drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V
    .locals 7
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "cornerBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "squareImage"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p3, "mRms"    # F

    .prologue
    const/high16 v6, 0x42200000    # 40.0f

    const/high16 v5, 0x42aa0000    # 85.0f

    const/high16 v4, 0x428c0000    # 70.0f

    const/high16 v3, 0x425c0000    # 55.0f

    const/4 v2, 0x0

    .line 9
    const/high16 v0, 0x41f80000    # 31.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 10
    const/4 v0, 0x0

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 11
    const/high16 v0, 0x41c80000    # 25.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 12
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 13
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 14
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 15
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 16
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 17
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 18
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 19
    const/high16 v0, 0x43110000    # 145.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 20
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    const/high16 v0, 0x42140000    # 37.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 23
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 24
    const/high16 v0, 0x41c80000    # 25.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 25
    invoke-virtual {p0, p1, v6, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 26
    invoke-virtual {p0, p1, v3, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 27
    invoke-virtual {p0, p1, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 28
    invoke-virtual {p0, p1, v5, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 29
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 30
    const/high16 v0, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 31
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 32
    const/high16 v0, 0x43110000    # 145.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 33
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 35
    :cond_2
    const/high16 v0, 0x422c0000    # 43.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_3

    .line 36
    const/4 v0, 0x0

    const/high16 v1, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 37
    const/high16 v0, 0x41c80000    # 25.0f

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 38
    const/high16 v0, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 39
    const/high16 v0, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 40
    const/high16 v0, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 41
    const/high16 v0, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 42
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 43
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 44
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 45
    const/high16 v0, 0x43110000    # 145.0f

    const/high16 v1, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 46
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 49
    :cond_3
    const/high16 v0, 0x42440000    # 49.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    .line 50
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 51
    const/high16 v0, 0x41c80000    # 25.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 52
    invoke-virtual {p0, p1, v6, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 53
    invoke-virtual {p0, p1, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 54
    invoke-virtual {p0, p1, v4, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 55
    invoke-virtual {p0, p1, v5, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 56
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 57
    const/high16 v0, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 58
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 59
    const/high16 v0, 0x43110000    # 145.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 60
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 61
    const/4 v0, 0x0

    const/16 v1, 0x46

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 62
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 63
    :cond_4
    cmpg-float v0, p3, v3

    if-gez v0, :cond_5

    .line 64
    const/4 v0, 0x0

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 65
    const/high16 v0, 0x41c80000    # 25.0f

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 66
    const/high16 v0, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 67
    invoke-virtual {p0, p1, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 68
    invoke-virtual {p0, p1, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 69
    const/high16 v0, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 70
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 71
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 72
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 73
    const/high16 v0, 0x43110000    # 145.0f

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 74
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 75
    const/4 v0, 0x0

    const/16 v1, 0x41

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 76
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 77
    :cond_5
    const/high16 v0, 0x42740000    # 61.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6

    .line 78
    const/4 v0, 0x0

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 79
    const/high16 v0, 0x41c80000    # 25.0f

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 80
    invoke-virtual {p0, p1, v6, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 81
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 82
    const/high16 v0, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 83
    const/high16 v0, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 84
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 85
    const/high16 v0, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 86
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 87
    const/high16 v0, 0x43110000    # 145.0f

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 88
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 89
    const/4 v0, 0x0

    const/16 v1, 0x3c

    const/16 v2, 0xa0

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 90
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 91
    :cond_6
    const/high16 v0, 0x42860000    # 67.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_7

    .line 92
    const/4 v0, 0x0

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 93
    const/high16 v0, 0x41c80000    # 25.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 94
    invoke-virtual {p0, p1, v6, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 95
    const/high16 v0, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 96
    const/high16 v0, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 97
    invoke-virtual {p0, p1, v5, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 98
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 99
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 100
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 101
    const/high16 v0, 0x43110000    # 145.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 102
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 104
    const/4 v0, 0x0

    const/16 v1, 0x32

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 105
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 106
    :cond_7
    const/high16 v0, 0x42920000    # 73.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_8

    .line 107
    const/4 v0, 0x0

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 108
    const/high16 v0, 0x41c80000    # 25.0f

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 109
    const/high16 v0, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 110
    const/high16 v0, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 111
    const/high16 v0, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 112
    const/high16 v0, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 113
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 114
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 115
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 116
    const/high16 v0, 0x43110000    # 145.0f

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 117
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 119
    const/4 v0, 0x0

    const/16 v1, 0x2d

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 120
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 121
    :cond_8
    const/high16 v0, 0x429e0000    # 79.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_9

    .line 122
    const/4 v0, 0x0

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 123
    const/high16 v0, 0x41c80000    # 25.0f

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 124
    const/high16 v0, 0x41700000    # 15.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 125
    const/high16 v0, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 126
    const/high16 v0, 0x41700000    # 15.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 127
    const/high16 v0, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 128
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 129
    const/high16 v0, 0x42e60000    # 115.0f

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 130
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 131
    const/high16 v0, 0x43110000    # 145.0f

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 132
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 134
    const/4 v0, 0x0

    const/16 v1, 0x1e

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 135
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 136
    :cond_9
    const/high16 v0, 0x429e0000    # 79.0f

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 138
    const/4 v0, 0x0

    const/16 v1, 0xa

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 139
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method
