.class public Lcom/vlingo/midas/gui/customviews/MicView;
.super Lcom/vlingo/midas/gui/customviews/MicViewBase;
.source "MicView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/customviews/MicView$1;,
        Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;
    }
.end annotation


# instance fields
.field private blueLight:Landroid/graphics/Bitmap;

.field private blueLight_land:Landroid/graphics/Bitmap;

.field private cornerBitmap:Landroid/graphics/Bitmap;

.field private currentConfig:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field private delay:I

.field private glowCueImageBitmap:Landroid/graphics/Bitmap;

.field private glowcueImageBitmap_land:Landroid/graphics/Bitmap;

.field private height:I

.field private idle:Landroid/graphics/Bitmap;

.field private idle_land:Landroid/graphics/Bitmap;

.field private mBGBitmapNormal:Landroid/graphics/Bitmap;

.field private mBGBitmapNormal_land:Landroid/graphics/Bitmap;

.field private mBGBitmap_land:Landroid/graphics/Bitmap;

.field private mBGBitmap_land_softKey:Landroid/graphics/Bitmap;

.field private mMicActiveBitmap:Landroid/graphics/Bitmap;

.field private mMicActive_land:Landroid/graphics/Bitmap;

.field private mRms:F

.field private normalCueImageBitmap:Landroid/graphics/Bitmap;

.field private normalCueImageBitmap_land:Landroid/graphics/Bitmap;

.field private squareImage:Landroid/graphics/drawable/NinePatchDrawable;

.field private width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/customviews/MicViewBase;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 36
    sget-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->QHD:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->currentConfig:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/customviews/MicViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 36
    sget-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->QHD:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->currentConfig:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    .line 44
    return-void
.end method

.method private createScaledBitmapByResource(III)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "resourceId"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    const/4 v4, 0x1

    .line 791
    const/4 v2, 0x0

    .line 793
    .local v2, "src":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 794
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput v3, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 795
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 796
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, p1, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 801
    .end local v1    # "options":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    if-eqz v2, :cond_0

    invoke-static {v2, p2, p3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .end local v2    # "src":Landroid/graphics/Bitmap;
    :cond_0
    return-object v2

    .line 797
    .restart local v2    # "src":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v0

    .line 798
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public init(II)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 58
    .local v1, "screenWidth":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 60
    .local v0, "screenHeight":I
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    sget-object v2, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->KDEVICE:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    iput-object v2, p0, Lcom/vlingo/midas/gui/customviews/MicView;->currentConfig:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    .line 63
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v9, 0x11

    const/16 v8, 0xa

    const/4 v6, 0x7

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 97
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v1, v3, Landroid/content/res/Configuration;->orientation:I

    .line 99
    .local v1, "orientation":I
    sget-object v3, Lcom/vlingo/midas/gui/customviews/MicView$1;->$SwitchMap$com$vlingo$midas$gui$customviews$MicView$DeviceConfiguration:[I

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->currentConfig:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 782
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 104
    :pswitch_1
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 105
    if-ne v1, v5, :cond_6

    .line 106
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 107
    .local v0, "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 108
    .local v2, "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_1

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_1

    .line 109
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 110
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 111
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto :goto_0

    .line 112
    :cond_1
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_2

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_2

    .line 113
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 114
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto :goto_0

    .line 115
    :cond_2
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_3

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_3

    .line 116
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 117
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto :goto_0

    .line 118
    :cond_3
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_4

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_4

    .line 119
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 120
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto :goto_0

    .line 121
    :cond_4
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 122
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 123
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_5

    .line 124
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationSantos10;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 126
    :cond_5
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 127
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 140
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 141
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 142
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_7

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_7

    .line 143
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 144
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 145
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 146
    :cond_7
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_8

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_8

    .line 147
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 148
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 149
    :cond_8
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_9

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_9

    .line 150
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 151
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 152
    :cond_9
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_a

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_a

    .line 153
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 154
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 155
    :cond_a
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 156
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 157
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_b

    .line 158
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationSantos10;->drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 160
    :cond_b
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 161
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 179
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :pswitch_2
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 180
    if-ne v1, v5, :cond_11

    .line 181
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 182
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 183
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_c

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_c

    .line 184
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 185
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 186
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 187
    :cond_c
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_d

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_d

    .line 188
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 189
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 190
    :cond_d
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_e

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_e

    .line 191
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 192
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 193
    :cond_e
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_f

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_f

    .line 194
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 195
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 196
    :cond_f
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 197
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 198
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_10

    .line 199
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationV1;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 201
    :cond_10
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 202
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 215
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :cond_11
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 216
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 217
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_12

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_12

    .line 218
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 219
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 220
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 221
    :cond_12
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_13

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_13

    .line 222
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 223
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 224
    :cond_13
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_14

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_14

    .line 225
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 226
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 227
    :cond_14
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_15

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_15

    .line 228
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 229
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 230
    :cond_15
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 231
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 232
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_16

    .line 233
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationV1;->drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 235
    :cond_16
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 236
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 255
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :pswitch_3
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 256
    if-ne v1, v5, :cond_1c

    .line 257
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 258
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 259
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_17

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_17

    .line 260
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 261
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 262
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 263
    :cond_17
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_18

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_18

    .line 264
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 265
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 266
    :cond_18
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_19

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_19

    .line 267
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 268
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 269
    :cond_19
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_1a

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_1a

    .line 270
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 271
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 272
    :cond_1a
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 273
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 274
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_1b

    .line 275
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 277
    :cond_1b
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 278
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 291
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :cond_1c
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 292
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 293
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_1d

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_1d

    .line 294
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 295
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 296
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 297
    :cond_1d
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_1e

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_1e

    .line 298
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowcueImageBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 299
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 300
    :cond_1e
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_1f

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_1f

    .line 301
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 302
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 303
    :cond_1f
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_20

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_20

    .line 304
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowcueImageBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 305
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 306
    :cond_20
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 307
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 308
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_21

    .line 309
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;->drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 310
    :cond_21
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 311
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 312
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActive_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 330
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :pswitch_4
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->width:I

    iget v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->height:I

    if-le v3, v4, :cond_22

    .line 331
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 332
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 334
    .restart local v2    # "topPadding":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    add-int/lit8 v5, v2, -0x1

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 335
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationTablet;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 337
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    add-int/lit8 v5, v2, -0x1

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 338
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 350
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :cond_22
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 351
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 353
    .restart local v2    # "topPadding":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal_land:Landroid/graphics/Bitmap;

    add-int/lit8 v4, v0, -0x1e

    int-to-float v4, v4

    add-int/lit8 v5, v2, -0x1e

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 354
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationTablet;->drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 355
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 356
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 357
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActive_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 374
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :pswitch_5
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 375
    if-ne v1, v5, :cond_28

    .line 376
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 377
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 378
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_23

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_23

    .line 379
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 380
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 381
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 382
    :cond_23
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_24

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_24

    .line 383
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 384
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 385
    :cond_24
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_25

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_25

    .line 386
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 387
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 388
    :cond_25
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_26

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_26

    .line 389
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 390
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 391
    :cond_26
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 392
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    add-int/lit8 v5, v2, -0x1

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 393
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_27

    .line 394
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 396
    :cond_27
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    add-int/lit8 v5, v2, -0x1

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 397
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 410
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :cond_28
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 411
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 412
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_29

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_29

    .line 413
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 414
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 415
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 416
    :cond_29
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_2a

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_2a

    .line 417
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowcueImageBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 418
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 419
    :cond_2a
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_2b

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_2b

    .line 420
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 421
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 422
    :cond_2b
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_2c

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_2c

    .line 423
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowcueImageBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 424
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 425
    :cond_2c
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 426
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 427
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_2d

    .line 428
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;->drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 429
    :cond_2d
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 430
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 431
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActive_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 450
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :pswitch_6
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 451
    if-ne v1, v5, :cond_33

    .line 452
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 453
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 454
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_2e

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_2e

    .line 455
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 456
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 457
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 458
    :cond_2e
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_2f

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_2f

    .line 459
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 460
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 461
    :cond_2f
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_30

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_30

    .line 462
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 463
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 464
    :cond_30
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_31

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_31

    .line 465
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 466
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 467
    :cond_31
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 468
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 469
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_32

    .line 470
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 472
    :cond_32
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 473
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 486
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :cond_33
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 487
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 488
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_34

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_34

    .line 489
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 490
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 491
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 492
    :cond_34
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_35

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_35

    .line 493
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 494
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 495
    :cond_35
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_36

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_36

    .line 496
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 497
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 498
    :cond_36
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_37

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_37

    .line 499
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 500
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 501
    :cond_37
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 502
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 503
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_38

    .line 504
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 506
    :cond_38
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 507
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 528
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :pswitch_7
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 529
    if-ne v1, v5, :cond_3e

    .line 530
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 531
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 532
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_39

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_39

    .line 533
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 534
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 535
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 536
    :cond_39
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_3a

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_3a

    .line 537
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 538
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 539
    :cond_3a
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_3b

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_3b

    .line 540
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 541
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 542
    :cond_3b
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_3c

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_3c

    .line 543
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 544
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 545
    :cond_3c
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 546
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 547
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_3d

    .line 548
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 550
    :cond_3d
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 551
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 564
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :cond_3e
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 565
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 566
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_3f

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_3f

    .line 567
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 568
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 569
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 570
    :cond_3f
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_40

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_40

    .line 571
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowcueImageBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 572
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 573
    :cond_40
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_41

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_41

    .line 574
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 575
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 576
    :cond_41
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_42

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_42

    .line 577
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowcueImageBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 578
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 579
    :cond_42
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 580
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 581
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_43

    .line 582
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;->drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 583
    :cond_43
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 584
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 585
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActive_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 603
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :pswitch_8
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v5, :cond_44

    .line 604
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 605
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 607
    .restart local v2    # "topPadding":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 608
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationWVGA;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 610
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 611
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 624
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :cond_44
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 625
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 627
    .restart local v2    # "topPadding":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 628
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationWVGA;->drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 629
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 630
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 631
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActive_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 648
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :pswitch_9
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v5, :cond_45

    .line 649
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 650
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 652
    .restart local v2    # "topPadding":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 653
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 655
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 656
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 674
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :cond_45
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 675
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 677
    .restart local v2    # "topPadding":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 678
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationQHD;->drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 679
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmap_land_softKey:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    const v6, 0x3fcccccd    # 1.6f

    add-float/2addr v5, v6

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 680
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    const v6, 0x3fcccccd    # 1.6f

    add-float/2addr v5, v6

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 681
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActive_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    const v6, 0x3fcccccd    # 1.6f

    add-float/2addr v5, v6

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 704
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :pswitch_a
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 705
    if-ne v1, v5, :cond_4b

    .line 706
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 707
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 708
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_46

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_46

    .line 709
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 710
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 711
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 713
    :cond_46
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_47

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_47

    .line 714
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 715
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 716
    :cond_47
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_48

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_48

    .line 717
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 718
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 719
    :cond_48
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_49

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_49

    .line 720
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 721
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 722
    :cond_49
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 723
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    add-int/lit8 v5, v2, -0x1

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 724
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_4a

    .line 725
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationJDevice;->drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 727
    :cond_4a
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    add-int/lit8 v5, v2, -0x1

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 728
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 741
    .end local v0    # "leftPadding":I
    .end local v2    # "topPadding":I
    :cond_4b
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingLeft()I

    move-result v0

    .line 742
    .restart local v0    # "leftPadding":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->getPaddingTop()I

    move-result v2

    .line 743
    .restart local v2    # "topPadding":I
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-ltz v3, :cond_4c

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v5, :cond_4c

    .line 744
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 745
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 746
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 748
    :cond_4c
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v5, :cond_4d

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v6, :cond_4d

    .line 749
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowcueImageBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 750
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 751
    :cond_4d
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v6, :cond_4e

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v8, :cond_4e

    .line 752
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 753
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 755
    :cond_4e
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v8, :cond_4f

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-gt v3, v9, :cond_4f

    .line 756
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowcueImageBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 757
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    goto/16 :goto_0

    .line 758
    :cond_4f
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    if-le v3, v9, :cond_0

    .line 759
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 760
    iget v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_50

    .line 761
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    iget v5, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    invoke-static {p1, v3, v4, v5}, Lcom/vlingo/midas/gui/customviews/DrawAnimationJDevice;->drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V

    .line 762
    :cond_50
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmap_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 763
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 764
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActive_land:Landroid/graphics/Bitmap;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 87
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->onSizeChanged(IIII)V

    .line 88
    iput p1, p0, Lcom/vlingo/midas/gui/customviews/MicView;->width:I

    .line 89
    iput p2, p0, Lcom/vlingo/midas/gui/customviews/MicView;->height:I

    .line 90
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/midas/gui/customviews/MicView;->init(II)V

    .line 91
    return-void
.end method

.method public reCycle()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 67
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->squareImage:Landroid/graphics/drawable/NinePatchDrawable;

    .line 68
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmap_land:Landroid/graphics/Bitmap;

    .line 69
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActive_land:Landroid/graphics/Bitmap;

    .line 70
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal_land:Landroid/graphics/Bitmap;

    .line 71
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmapNormal:Landroid/graphics/Bitmap;

    .line 72
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mMicActiveBitmap:Landroid/graphics/Bitmap;

    .line 74
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->cornerBitmap:Landroid/graphics/Bitmap;

    .line 76
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight:Landroid/graphics/Bitmap;

    .line 77
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->blueLight_land:Landroid/graphics/Bitmap;

    .line 78
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowCueImageBitmap:Landroid/graphics/Bitmap;

    .line 79
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->glowcueImageBitmap_land:Landroid/graphics/Bitmap;

    .line 80
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mBGBitmap_land_softKey:Landroid/graphics/Bitmap;

    .line 82
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle:Landroid/graphics/Bitmap;

    .line 83
    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->idle_land:Landroid/graphics/Bitmap;

    .line 84
    return-void
.end method

.method public resetListeningAnimation()V
    .locals 1

    .prologue
    .line 805
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->delay:I

    .line 806
    return-void
.end method

.method public setRMSValue(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    const/4 v1, 0x0

    .line 47
    iput p1, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    .line 48
    iget v0, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 49
    iput v1, p0, Lcom/vlingo/midas/gui/customviews/MicView;->mRms:F

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/MicView;->invalidate()V

    .line 53
    return-void
.end method
