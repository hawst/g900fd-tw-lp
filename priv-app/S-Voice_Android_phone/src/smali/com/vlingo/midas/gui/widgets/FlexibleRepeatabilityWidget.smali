.class public abstract Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "FlexibleRepeatabilityWidget.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Type:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<TType;>;"
    }
.end annotation


# instance fields
.field private replaceablePreviousSameWidget:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;, "Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget<TType;>;"
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->replaceablePreviousSameWidget:Z

    .line 18
    return-void
.end method


# virtual methods
.method public initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 1
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TType;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;, "Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget<TType;>;"
    .local p1, "object":Ljava/lang/Object;, "TType;"
    if-eqz p2, :cond_0

    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->Replaceable:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v0}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->replaceablePreviousSameWidget:Z

    .line 24
    return-void

    .line 23
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPreviousSameWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 28
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;, "Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget<TType;>;"
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->replaceablePreviousSameWidget:Z

    return v0
.end method
