.class Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;
.super Ljava/lang/Object;
.source "LocalSearchWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallClickListener"
.end annotation


# instance fields
.field private phoneNumber:Ljava/lang/String;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Ljava/lang/String;)V
    .locals 0
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;->phoneNumber:Ljava/lang/String;

    .line 157
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 162
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.AcceptedText"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    .local v0, "in":Landroid/content/Intent;
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$500(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v2

    new-instance v3, Lcom/vlingo/core/internal/recognition/acceptedtext/LocalSearchAcceptedText;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
    invoke-static {v4}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$400(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCurrentSearchQuery()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;->CALL:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/vlingo/core/internal/recognition/acceptedtext/LocalSearchAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 165
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;->phoneNumber:Ljava/lang/String;

    invoke-static {v2}, Lcom/vlingo/core/internal/util/DialUtil;->getDialIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 166
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 167
    return-void
.end method
