.class Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;
.super Ljava/util/TimerTask;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResumeControlPollTask"
.end annotation


# instance fields
.field private mTimesWaited:I

.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogFragment;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;)V
    .locals 1

    .prologue
    .line 2615
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 2616
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->mTimesWaited:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;Lcom/vlingo/midas/gui/DialogFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/DialogFragment$1;

    .prologue
    .line 2615
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2626
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v2, v1, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    monitor-enter v2

    .line 2627
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ConversationActivity;

    .line 2628
    .local v0, "activity":Lcom/vlingo/midas/gui/ConversationActivity;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    if-nez v1, :cond_1

    .line 2629
    :cond_0
    monitor-exit v2

    .line 2650
    :goto_0
    return-void

    .line 2632
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->mTimesWaited:I

    const/4 v3, 0x3

    if-gt v1, v3, :cond_2

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->isDrivingMode()Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    move-result-object v1

    sget-object v3, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    if-ne v1, v3, :cond_4

    .line 2635
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$1700(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2638
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$1700(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/core/internal/dialogmanager/ResumeControl;->resume()V

    .line 2639
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    const/4 v3, 0x0

    # setter for: Lcom/vlingo/midas/gui/DialogFragment;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    invoke-static {v1, v3}, Lcom/vlingo/midas/gui/DialogFragment;->access$1702(Lcom/vlingo/midas/gui/DialogFragment;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .line 2643
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$1800(Lcom/vlingo/midas/gui/DialogFragment;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 2649
    :goto_1
    monitor-exit v2

    goto :goto_0

    .end local v0    # "activity":Lcom/vlingo/midas/gui/ConversationActivity;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 2647
    .restart local v0    # "activity":Lcom/vlingo/midas/gui/ConversationActivity;
    :cond_4
    :try_start_1
    iget v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->mTimesWaited:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;->mTimesWaited:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
