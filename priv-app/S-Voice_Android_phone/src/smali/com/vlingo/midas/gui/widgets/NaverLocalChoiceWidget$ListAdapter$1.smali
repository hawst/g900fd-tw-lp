.class Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter$1;
.super Ljava/lang/Object;
.source "NaverLocalChoiceWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 263
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 264
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;

    .line 266
    .local v0, "holder":Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;
    # getter for: Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->detailLink:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->access$500(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 268
    # getter for: Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->detailLink:Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;->access$500(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 269
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 271
    :cond_0
    return-void
.end method
