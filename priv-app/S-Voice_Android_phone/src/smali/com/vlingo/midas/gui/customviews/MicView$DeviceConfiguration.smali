.class final enum Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;
.super Ljava/lang/Enum;
.source "MicView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/customviews/MicView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "DeviceConfiguration"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum JDEVICE:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum JMINI:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum KDEVICE:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum QHD:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum SANTOS_10:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum SMART_CAMERA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum TABLET:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum V1:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum WSVGA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum WVGA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

.field public static final enum WXVGA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "QHD"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->QHD:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "WVGA"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->WVGA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "WXVGA"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->WXVGA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "WSVGA"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->WSVGA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "SMART_CAMERA"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->SMART_CAMERA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "TABLET"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->TABLET:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "JDEVICE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->JDEVICE:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "JMINI"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->JMINI:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "SANTOS_10"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->SANTOS_10:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "V1"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->V1:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    new-instance v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const-string/jumbo v1, "KDEVICE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->KDEVICE:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    sget-object v1, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->QHD:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->WVGA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->WXVGA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->WSVGA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->SMART_CAMERA:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->TABLET:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->JDEVICE:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->JMINI:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->SANTOS_10:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->V1:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->KDEVICE:Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->$VALUES:[Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-class v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->$VALUES:[Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    invoke-virtual {v0}, [Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/gui/customviews/MicView$DeviceConfiguration;

    return-object v0
.end method
