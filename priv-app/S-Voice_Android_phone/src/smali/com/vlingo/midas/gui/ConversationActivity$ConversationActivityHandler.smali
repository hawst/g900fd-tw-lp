.class public Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;
.super Landroid/os/Handler;
.source "ConversationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ConversationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConversationActivityHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/ConversationActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/ConversationActivity;

    .prologue
    .line 465
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 466
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 467
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v3, 0x10

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 470
    iget-object v2, p0, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/ConversationActivity;

    .line 471
    .local v1, "o":Lcom/vlingo/midas/gui/ConversationActivity;
    if-eqz v1, :cond_0

    .line 472
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 626
    :cond_0
    :goto_0
    return-void

    .line 474
    :sswitch_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 475
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 476
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->removeDelegateFromLastScreen()V

    .line 479
    :cond_1
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_2

    .line 480
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v5}, Lcom/vlingo/midas/gui/ControlFragment;->disableMicInTutorial(Z)V

    .line 481
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v5}, Lcom/vlingo/midas/gui/ControlFragment;->removeMicStatusTextForTutorial(Z)V

    .line 483
    :cond_2
    # invokes: Lcom/vlingo/midas/gui/ConversationActivity;->reRegisterPhraseSpottingAfterTutorial()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$000(Lcom/vlingo/midas/gui/ConversationActivity;)V

    .line 484
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->showControlFragment()V

    .line 485
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 486
    iget-object v2, v1, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v2, v6}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 491
    :sswitch_1
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_0

    .line 492
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v6}, Lcom/vlingo/midas/gui/ControlFragment;->disableMicInTutorial(Z)V

    goto :goto_0

    .line 495
    :sswitch_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 496
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 497
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->removeDelegateFromLastScreen()V

    .line 499
    :cond_3
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_4

    .line 500
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v6, v5}, Lcom/vlingo/midas/gui/ControlFragment;->fadeInMicLayout(ZI)V

    .line 502
    :cond_4
    # invokes: Lcom/vlingo/midas/gui/ConversationActivity;->reRegisterPhraseSpottingAfterTutorial()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$000(Lcom/vlingo/midas/gui/ConversationActivity;)V

    .line 503
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->showControlFragment()V

    goto :goto_0

    .line 506
    :sswitch_3
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_0

    .line 507
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->hideMic()V

    goto :goto_0

    .line 511
    :sswitch_4
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_0

    .line 512
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v2

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v2, v3, :cond_0

    .line 514
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const-wide/16 v3, 0xc8

    invoke-virtual {v2, v3, v4}, Lcom/vlingo/midas/gui/ControlFragment;->scheduleStartSpotter(J)V

    goto/16 :goto_0

    .line 523
    :sswitch_5
    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$100(Lcom/vlingo/midas/gui/ConversationActivity;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 524
    # setter for: Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z
    invoke-static {v1, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->access$102(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z

    .line 526
    :cond_5
    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->mAutoListeningmodeAlways:Z
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$200(Lcom/vlingo/midas/gui/ConversationActivity;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 527
    # setter for: Lcom/vlingo/midas/gui/ConversationActivity;->mAutoListeningmodeAlways:Z
    invoke-static {v1, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->access$202(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z

    .line 529
    :cond_6
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v2, :cond_0

    # invokes: Lcom/vlingo/midas/gui/ConversationActivity;->isAppForeground()Z
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$300(Lcom/vlingo/midas/gui/ConversationActivity;)Z

    move-result v2

    if-nez v2, :cond_7

    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->isHelpLaunched:Z
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$400(Lcom/vlingo/midas/gui/ConversationActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 530
    :cond_7
    # setter for: Lcom/vlingo/midas/gui/ConversationActivity;->isHelpLaunched:Z
    invoke-static {v1, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->access$402(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z

    .line 531
    sget-object v2, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->alwaysMicrophoneStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    if-eqz v2, :cond_8

    .line 533
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v3, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->alwaysMicrophoneStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/ControlFragment;->startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    .line 535
    sput-object v4, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->alwaysMicrophoneStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    goto/16 :goto_0

    .line 537
    :cond_8
    sget-boolean v2, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    if-nez v2, :cond_9

    .line 538
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v2, v4}, Lcom/vlingo/midas/gui/ControlFragment;->startRecognition(Lcom/vlingo/core/internal/audio/MicrophoneStream;)V

    .line 541
    # setter for: Lcom/vlingo/midas/gui/ConversationActivity;->checkForAbnormalonPause:Z
    invoke-static {v1, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->access$502(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z

    .line 544
    :cond_9
    sput-boolean v5, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    goto/16 :goto_0

    .line 550
    :sswitch_6
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->hasWindowFocus()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 551
    # invokes: Lcom/vlingo/midas/gui/ConversationActivity;->clearDismissKeyguardFlag()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$600(Lcom/vlingo/midas/gui/ConversationActivity;)V

    goto/16 :goto_0

    .line 556
    :sswitch_7
    # invokes: Lcom/vlingo/midas/gui/ConversationActivity;->removeLocationListener()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$700(Lcom/vlingo/midas/gui/ConversationActivity;)V

    goto/16 :goto_0

    .line 559
    :sswitch_8
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 560
    invoke-static {v1, v6, v5}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->startFacebookSSO(Landroid/content/Context;ZZ)V

    goto/16 :goto_0

    .line 562
    :cond_a
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getIntentAfterTTS()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 563
    invoke-static {v4}, Lcom/vlingo/midas/util/SocialUtils;->setIntentAfterTTS(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 567
    :sswitch_9
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 568
    invoke-static {v1, v6, v5}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->startTwitterSSO(Landroid/content/Context;ZZ)V

    goto/16 :goto_0

    .line 570
    :cond_b
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getIntentAfterTTS()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 571
    invoke-static {v4}, Lcom/vlingo/midas/util/SocialUtils;->setIntentAfterTTS(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 575
    :sswitch_a
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getIntentAfterTTS()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 576
    invoke-static {v4}, Lcom/vlingo/midas/util/SocialUtils;->setIntentAfterTTS(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 579
    :sswitch_b
    invoke-static {}, Lcom/vlingo/midas/util/SocialUtils;->getIntentAfterTTS()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 580
    invoke-static {v4}, Lcom/vlingo/midas/util/SocialUtils;->setIntentAfterTTS(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 583
    :sswitch_c
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->stopThinkingAnimation()V

    goto/16 :goto_0

    .line 586
    :sswitch_d
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->finish()V

    goto/16 :goto_0

    .line 591
    :sswitch_e
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioSupported()Z

    move-result v2

    if-nez v2, :cond_0

    .line 592
    :cond_c
    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$800(Lcom/vlingo/midas/gui/ConversationActivity;)Lcom/vlingo/midas/gui/ContentFragment;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v2

    if-nez v2, :cond_0

    .line 593
    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$800(Lcom/vlingo/midas/gui/ConversationActivity;)Lcom/vlingo/midas/gui/ContentFragment;

    move-result-object v2

    sget-object v3, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    const-string/jumbo v4, ""

    invoke-virtual {v2, v3, v4, v6, v5}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    goto/16 :goto_0

    .line 598
    :sswitch_f
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v2

    if-nez v2, :cond_0

    .line 599
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/VlingoApplication;->getMainActivityClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 602
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 603
    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 607
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_10
    # invokes: Lcom/vlingo/midas/gui/ConversationActivity;->stopVoiceWakeupParam()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$900(Lcom/vlingo/midas/gui/ConversationActivity;)V

    goto/16 :goto_0

    .line 610
    :sswitch_11
    # invokes: Lcom/vlingo/midas/gui/ConversationActivity;->doDismissKeyguard()V
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->access$1000(Lcom/vlingo/midas/gui/ConversationActivity;)V

    goto/16 :goto_0

    .line 613
    :sswitch_12
    # setter for: Lcom/vlingo/midas/gui/ConversationActivity;->checkForAbnormalonPause:Z
    invoke-static {v1, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->access$502(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z

    .line 616
    const-string/jumbo v2, "keyguard"

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    if-eqz v2, :cond_0

    .line 618
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->stopSeamlessVoiceWakeup()V

    .line 619
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->startVoiceWakeup()V

    .line 621
    sput-boolean v5, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    goto/16 :goto_0

    .line 472
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_e
        0x1 -> :sswitch_4
        0x2 -> :sswitch_5
        0x3 -> :sswitch_6
        0x4 -> :sswitch_7
        0x6 -> :sswitch_8
        0x7 -> :sswitch_9
        0x9 -> :sswitch_a
        0xb -> :sswitch_b
        0xd -> :sswitch_c
        0xe -> :sswitch_0
        0xf -> :sswitch_d
        0x10 -> :sswitch_f
        0x11 -> :sswitch_10
        0x12 -> :sswitch_11
        0x13 -> :sswitch_12
        0x14 -> :sswitch_2
        0x22b -> :sswitch_1
        0x22c -> :sswitch_3
    .end sparse-switch
.end method
