.class public Lcom/vlingo/midas/gui/DialogFragment;
.super Lcom/vlingo/midas/gui/ContentFragment;
.source "DialogFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;
.implements Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;
.implements Lcom/vlingo/midas/gui/OnEditListener;
.implements Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/DialogFragment$12;,
        Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;,
        Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;,
        Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;,
        Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;,
        Lcom/vlingo/midas/gui/DialogFragment$AnimateDialogExpandThread;,
        Lcom/vlingo/midas/gui/DialogFragment$AnimThread;,
        Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;,
        Lcom/vlingo/midas/gui/DialogFragment$TabletType;,
        Lcom/vlingo/midas/gui/DialogFragment$PhoneType;
    }
.end annotation


# static fields
.field private static final MAX_DUMMY_COUNT:I = 0x5

.field private static final MAX_RESUME_CONTROL_POLLS:I = 0x3

.field private static final REMAIN_DUMMY_COUNT:I = 0x2

.field private static final RESUME_CONTOL_POLL_DURATION:I = 0x2ee

.field private static final TALKBACK_ANNOUNCEMENT:I = 0x65

.field private static final THRESHOLD_MAX_HEIGHT:I = 0x72c

.field private static final THRESHOLD_WIDGET_HEIGHT:I = 0x165

.field private static actionBar:Landroid/app/ActionBar;

.field public static birthdayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static bubbleViewForTalkback:Landroid/view/View;

.field private static currentPhone:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

.field public static eventList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/gui/widgets/ScheduleEventData;",
            ">;"
        }
    .end annotation
.end field

.field public static se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

.field private static vlingoFocus:Z


# instance fields
.field private final ANIMATE_FLIP_UP_DOWN_DURATION:I

.field BottomViewbounds:Landroid/graphics/Rect;

.field ShadowView:Landroid/view/View;

.field public ThreadstopShadowBelow:I

.field public ThreadstopShadowTop:I

.field TopViewbounds:Landroid/graphics/Rect;

.field WakelockAcquired:Z

.field private alldaySchedules:I

.field private birthdays:I

.field private buffer:Landroid/view/View;

.field private count:I

.field private currentTablet:Lcom/vlingo/midas/gui/DialogFragment$TabletType;

.field data:Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

.field private dialogScrollContent:Landroid/widget/LinearLayout;

.field private dialogScrollView:Landroid/widget/ScrollView;

.field private dialog_full_shadowbelow:Landroid/widget/ImageView;

.field private fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

.field private hasOverScroll:Z

.field isBtnAndWakeBubble:Z

.field private isEditStarted:Z

.field private isFullScreen:Z

.field private isScrollingDone:Z

.field private isWolfWidgetExist:Z

.field public isYouCanSay:Z

.field private lastBubbleIsGreeting:Z

.field private mActivityCreated:Z

.field private mDialogHidden:Z

.field private mHelpWidgetExist:Z

.field private mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

.field private mTimer:Ljava/util/Timer;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private menuVal:I

.field private menuVar:Z

.field public metrics:Landroid/util/DisplayMetrics;

.field private mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

.field private mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

.field private moveListBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private openCloseThreshold:F

.field prevEditView:Landroid/view/View;

.field private pullTab:Landroid/widget/ImageView;

.field private pxDownAnim:I

.field private pxDownNotAnim:I

.field private pxTop:I

.field private schedules:I

.field scrollBoundsBottom:Landroid/graphics/Rect;

.field scrollBoundsTop:Landroid/graphics/Rect;

.field private scrollView:Landroid/widget/LinearLayout;

.field public setShadowWhileNotAnim:Z

.field public shouldContinueBelow:I

.field public shouldContinueTop:I

.field private timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

.field private topShadowDp:I

.field private trackingTouch:Z

.field public tutorialShadow:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->OTHER_PHONES:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    sput-object v0, Lcom/vlingo/midas/gui/DialogFragment;->currentPhone:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    .line 175
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/DialogFragment;->vlingoFocus:Z

    .line 176
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/gui/DialogFragment;->actionBar:Landroid/app/ActionBar;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ContentFragment;-><init>()V

    .line 108
    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->menuVal:I

    .line 111
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->isYouCanSay:Z

    .line 112
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->setShadowWhileNotAnim:Z

    .line 134
    const/16 v0, 0x12c

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->ANIMATE_FLIP_UP_DOWN_DURATION:I

    .line 140
    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->tutorialShadow:I

    .line 143
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mHelpWidgetExist:Z

    .line 144
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->isWolfWidgetExist:Z

    .line 145
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->isBtnAndWakeBubble:Z

    .line 146
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->WakelockAcquired:Z

    .line 148
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    .line 154
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->isScrollingDone:Z

    .line 155
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->hasOverScroll:Z

    .line 156
    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->count:I

    .line 158
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->isEditStarted:Z

    .line 159
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->menuVar:Z

    .line 160
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mDialogHidden:Z

    .line 169
    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->alldaySchedules:I

    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->schedules:I

    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->birthdays:I

    .line 181
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment$TabletType;->OTHERS:Lcom/vlingo/midas/gui/DialogFragment$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->currentTablet:Lcom/vlingo/midas/gui/DialogFragment$TabletType;

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->prevEditView:Landroid/view/View;

    .line 3086
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    .line 3240
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    return-void
.end method

.method static synthetic access$1000(Landroid/view/View;Landroid/view/View;)Z
    .locals 1
    .param p0, "x0"    # Landroid/view/View;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 98
    invoke-static {p0, p1}, Lcom/vlingo/midas/gui/DialogFragment;->checkEligibilityForWidgetHeight(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/vlingo/midas/gui/DialogFragment;Landroid/view/View;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragment;->isTapWidget(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vlingo/midas/gui/DialogFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p1, "x1"    # I

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragment;->doScrollAnim(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/vlingo/midas/gui/DialogFragment;F)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p1, "x1"    # F

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragment;->setDialogHeight(F)V

    return-void
.end method

.method static synthetic access$1502(Lcom/vlingo/midas/gui/DialogFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/DialogFragment;->isFullScreen:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/vlingo/midas/gui/DialogFragment;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)Lcom/vlingo/core/internal/dialogmanager/ResumeControl;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p1, "x1"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/vlingo/midas/gui/DialogFragment;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/vlingo/midas/gui/DialogFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p1, "x1"    # I

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragment;->doScrollAnimMoveList(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/DialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/vlingo/midas/gui/DialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->helpScrollAnim()V

    return-void
.end method

.method static synthetic access$2100()Z
    .locals 1

    .prologue
    .line 98
    sget-boolean v0, Lcom/vlingo/midas/gui/DialogFragment;->vlingoFocus:Z

    return v0
.end method

.method static synthetic access$2102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 98
    sput-boolean p0, Lcom/vlingo/midas/gui/DialogFragment;->vlingoFocus:Z

    return p0
.end method

.method static synthetic access$2200(Lcom/vlingo/midas/gui/DialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->pxTop:I

    return v0
.end method

.method static synthetic access$2300(Lcom/vlingo/midas/gui/DialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->hideShadowTop()V

    return-void
.end method

.method static synthetic access$2400(Lcom/vlingo/midas/gui/DialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->addShadowTop()V

    return-void
.end method

.method static synthetic access$2500(Lcom/vlingo/midas/gui/DialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->pxDownNotAnim:I

    return v0
.end method

.method static synthetic access$2600(Lcom/vlingo/midas/gui/DialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->pxDownAnim:I

    return v0
.end method

.method static synthetic access$2700(Lcom/vlingo/midas/gui/DialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->addShadowBottom()V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$400()Lcom/vlingo/midas/gui/DialogFragment$PhoneType;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment;->currentPhone:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/DialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->isScrollingDone:Z

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/customviews/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$800()Landroid/view/View;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$802(Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Landroid/view/View;

    .prologue
    .line 98
    sput-object p0, Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    return-object v0
.end method

.method private acquireWakeLock()V
    .locals 3

    .prologue
    .line 3093
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 3094
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string/jumbo v2, "power"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 3096
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    const-string/jumbo v2, "wakelock_readnews"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 3103
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3104
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 3105
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->WakelockAcquired:Z

    .line 3107
    :cond_1
    return-void
.end method

.method private addShadowBottom()V
    .locals 2

    .prologue
    .line 1325
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1335
    :cond_0
    :goto_0
    return-void

    .line 1329
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 1330
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialog_full_shadowbelow:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1331
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialog_full_shadowbelow:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private addShadowTop()V
    .locals 1

    .prologue
    .line 1304
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1309
    :cond_0
    return-void
.end method

.method private animateDialogExpand()V
    .locals 6

    .prologue
    .line 2263
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRefreshRate()F

    move-result v2

    .line 2265
    .local v2, "refreshRate":F
    const v0, 0x3e99999a    # 0.3f

    mul-float v3, v0, v2

    .line 2267
    .local v3, "frames":F
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2268
    .local v4, "initialHeight":I
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->getBottom()I

    move-result v0

    sub-int/2addr v0, v4

    int-to-float v0, v0

    div-float v5, v0, v3

    .line 2270
    .local v5, "delta":F
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragment$AnimateDialogExpandThread;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/midas/gui/DialogFragment$AnimateDialogExpandThread;-><init>(Lcom/vlingo/midas/gui/DialogFragment;FFIF)V

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragment$AnimateDialogExpandThread;->start()V

    .line 2272
    return-void
.end method

.method private static checkEligibilityForWidgetHeight(Landroid/view/View;Landroid/view/View;)Z
    .locals 3
    .param p0, "viewToChange"    # Landroid/view/View;
    .param p1, "buffer"    # Landroid/view/View;

    .prologue
    const/16 v2, 0x165

    const/4 v0, 0x0

    .line 1402
    if-eqz p0, :cond_1

    instance-of v1, p0, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    if-ge v1, v2, :cond_1

    .line 1406
    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p0, :cond_0

    instance-of v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-nez v1, :cond_0

    instance-of v1, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    if-gt v1, v2, :cond_2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lcom/vlingo/midas/R$id;->buffer:I

    if-ne v1, v2, :cond_0

    :cond_2
    instance-of v1, p0, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private doScrollAnim(I)V
    .locals 6
    .param p1, "height"    # I

    .prologue
    .line 1529
    :try_start_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getHandler()Landroid/os/Handler;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1945
    :goto_0
    return-void

    .line 1531
    :cond_0
    const/4 v0, 0x0

    .line 1532
    .local v0, "delay":I
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1533
    const/16 v0, 0x3e8

    .line 1534
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/vlingo/midas/gui/DialogFragment$6;

    invoke-direct {v3, p0, p1}, Lcom/vlingo/midas/gui/DialogFragment$6;-><init>(Lcom/vlingo/midas/gui/DialogFragment;I)V

    int-to-long v4, v0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1942
    .end local v0    # "delay":I
    :catch_0
    move-exception v1

    .line 1943
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private doScrollAnimMoveList(I)V
    .locals 3
    .param p1, "height"    # I

    .prologue
    .line 1951
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1952
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/gui/DialogFragment$7;

    invoke-direct {v2, p0, p1}, Lcom/vlingo/midas/gui/DialogFragment$7;-><init>(Lcom/vlingo/midas/gui/DialogFragment;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1971
    :cond_0
    :goto_0
    return-void

    .line 1968
    :catch_0
    move-exception v0

    .line 1969
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private doaddTutorialView(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1297
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1298
    return-void
.end method

.method private getReservedHeight()I
    .locals 1

    .prologue
    .line 2319
    const/4 v0, 0x0

    return v0
.end method

.method private helpScrollAnim()V
    .locals 1

    .prologue
    .line 1974
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    .line 1975
    .local v0, "anim":Lcom/vlingo/midas/gui/DialogFragment$AnimThread;
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->start()V

    .line 1976
    return-void
.end method

.method private hideShadowTop()V
    .locals 1

    .prologue
    .line 1314
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1318
    :cond_0
    return-void
.end method

.method private isEqual(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "stringID"    # I

    .prologue
    .line 2361
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2362
    const/4 v0, 0x1

    .line 2364
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTapWidget(Landroid/view/View;)Z
    .locals 1
    .param p1, "bubbleView"    # Landroid/view/View;

    .prologue
    .line 1418
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/vlingo/midas/gui/widgets/MemoWidget;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vlingo/midas/gui/widgets/ContactDetailWidget;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vlingo/midas/gui/widgets/ScheduleSingleWidget;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vlingo/midas/gui/widgets/ScheduleWidget;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vlingo/midas/gui/widgets/MessageWidget;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vlingo/midas/gui/widgets/MusicWidget;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private releaseWakeLock()V
    .locals 1

    .prologue
    .line 3110
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3111
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 3112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 3113
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->WakelockAcquired:Z

    .line 3115
    :cond_0
    return-void
.end method

.method private removeOldContent()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 2131
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    .line 2136
    .local v5, "numViews":I
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v8}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v8

    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v9

    if-le v8, v9, :cond_0

    const/4 v3, 0x1

    .line 2139
    .local v3, "minTotalViewHeight":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 2140
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 2141
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2163
    :goto_1
    return-void

    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v3    # "minTotalViewHeight":Z
    :cond_0
    move v3, v7

    .line 2136
    goto :goto_0

    .line 2145
    .restart local v3    # "minTotalViewHeight":Z
    :cond_1
    const/4 v0, 0x0

    .line 2146
    .local v0, "height":I
    add-int/lit8 v1, v5, -0x1

    .local v1, "i":I
    :goto_2
    if-ltz v1, :cond_2

    .line 2147
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 2148
    .local v6, "v":Landroid/view/View;
    invoke-virtual {v6, v7, v7}, Landroid/view/View;->measure(II)V

    .line 2149
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v0, v8

    .line 2146
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 2152
    .end local v6    # "v":Landroid/view/View;
    :cond_2
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 2153
    .restart local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v7}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v7

    sub-int v4, v7, v0

    .line 2155
    .local v4, "newHeight":I
    if-gez v4, :cond_4

    .line 2156
    const/16 v7, 0x28

    iput v7, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2162
    :cond_3
    :goto_3
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 2158
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v7

    if-nez v7, :cond_3

    .line 2159
    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_3
.end method

.method private scrollUpInCaseMoreHeight()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1354
    move-object v2, p0

    .line 1355
    .local v2, "dialogFragment1":Lcom/vlingo/midas/gui/DialogFragment;
    iget-object v7, v2, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    .line 1361
    .local v4, "numViews":I
    add-int/lit8 v3, v4, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 1362
    iget-object v7, v2, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1364
    .local v0, "bubbleView":Landroid/view/View;
    instance-of v7, v0, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v7, :cond_3

    move-object v7, v0

    check-cast v7, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v7}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v7

    sget-object v8, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v7, v8, :cond_3

    .line 1365
    iget-object v7, v2, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1366
    .local v6, "viewToChange":Landroid/view/View;
    iget-object v7, v2, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    add-int/lit8 v8, v3, 0x2

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1367
    .local v1, "buffer":Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1368
    iget-object v7, v2, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1370
    iget-object v7, v2, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1374
    :cond_0
    invoke-static {v6, v1}, Lcom/vlingo/midas/gui/DialogFragment;->checkEligibilityForWidgetHeight(Landroid/view/View;Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1375
    iget-object v7, v2, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    add-int/lit8 v8, v3, -0x1

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1376
    .local v5, "userBubble":Landroid/view/View;
    if-eqz v5, :cond_1

    .line 1377
    iput-boolean v10, p0, Lcom/vlingo/midas/gui/DialogFragment;->hasOverScroll:Z

    .line 1378
    iput-boolean v10, p0, Lcom/vlingo/midas/gui/DialogFragment;->isScrollingDone:Z

    .line 1379
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/vlingo/midas/gui/DialogFragment;->doScrollAnim(I)V

    .line 1380
    iput-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->isScrollingDone:Z

    .line 1383
    .end local v5    # "userBubble":Landroid/view/View;
    :cond_1
    iput-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->isScrollingDone:Z

    .line 1395
    .end local v0    # "bubbleView":Landroid/view/View;
    .end local v1    # "buffer":Landroid/view/View;
    .end local v6    # "viewToChange":Landroid/view/View;
    :cond_2
    :goto_1
    return-void

    .line 1390
    .restart local v0    # "bubbleView":Landroid/view/View;
    :cond_3
    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/DialogFragment;->isTapWidget(Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1391
    iput-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->isScrollingDone:Z

    goto :goto_1

    .line 1361
    :cond_4
    add-int/lit8 v3, v3, -0x1

    goto :goto_0
.end method

.method private setDialogHeight(F)V
    .locals 2
    .param p1, "y"    # F

    .prologue
    .line 2196
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    new-instance v1, Lcom/vlingo/midas/gui/DialogFragment$8;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/midas/gui/DialogFragment$8;-><init>(Lcom/vlingo/midas/gui/DialogFragment;F)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 2204
    return-void
.end method

.method private shadowVarInit()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 792
    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->tutorialShadow:I

    .line 793
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->isYouCanSay:Z

    .line 794
    sput-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorial:Z

    .line 795
    iget v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->pxTop:I

    .line 796
    const/high16 v0, 0x40c00000    # 6.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->pxDownAnim:I

    .line 797
    const/high16 v0, 0x42000000    # 32.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->pxDownNotAnim:I

    .line 798
    return-void
.end method

.method private showYCSW()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3254
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lcom/vlingo/midas/R$id;->buffer:I

    if-ne v1, v2, :cond_0

    .line 3256
    const/4 v0, 0x1

    .line 3257
    :cond_0
    return v0
.end method


# virtual methods
.method public addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;
    .locals 13
    .param p1, "type"    # Lcom/vlingo/midas/gui/DialogBubble$BubbleType;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "fillScreen"    # Z
    .param p4, "isGreetingHack"    # Z

    .prologue
    .line 461
    if-eqz p4, :cond_0

    iget-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->lastBubbleIsGreeting:Z

    if-nez v9, :cond_1

    :cond_0
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-nez v9, :cond_2

    .line 463
    :cond_1
    const/4 v2, 0x0

    .line 636
    :goto_0
    return-object v2

    .line 466
    :cond_2
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_6

    .line 467
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x2

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 471
    .local v5, "myView":Landroid/view/View;
    instance-of v9, v5, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    if-eqz v9, :cond_3

    .line 472
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x3

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 475
    :cond_3
    instance-of v9, v5, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v9, :cond_6

    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne p1, v9, :cond_6

    move-object v9, v5

    .line 477
    check-cast v9, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v9

    sget-object v10, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v9, v10, :cond_6

    move-object v9, v5

    .line 478
    check-cast v9, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v9, p2}, Lcom/vlingo/midas/gui/DialogBubble;->isNeededRefresh(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    move-object v9, v5

    .line 479
    check-cast v9, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v9, p1, p2}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    .line 480
    :cond_4
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 481
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 483
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->removeMessages(I)V

    .line 484
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    const/4 v10, 0x4

    const-wide/16 v11, 0x64

    invoke-virtual {v9, v10, v11, v12}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 486
    :cond_5
    const/4 v2, 0x0

    goto :goto_0

    .line 490
    .end local v5    # "myView":Landroid/view/View;
    :cond_6
    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne p1, v9, :cond_8

    iget-boolean v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    if-eqz v9, :cond_8

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    if-eqz v9, :cond_8

    .line 495
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 497
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->removeMessages(I)V

    .line 498
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    const/4 v10, 0x4

    const-wide/16 v11, 0x64

    invoke-virtual {v9, v10, v11, v12}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 500
    :cond_7
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x2

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    instance-of v9, v9, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    if-nez v9, :cond_8

    .line 502
    new-instance v9, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/gui/DialogFragment;->addDummyView(Landroid/view/View;)V

    .line 506
    :cond_8
    move/from16 v0, p4

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->lastBubbleIsGreeting:Z

    .line 507
    const/4 v2, 0x0

    .line 509
    .local v2, "db":Lcom/vlingo/midas/gui/DialogBubble;
    :try_start_0
    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne p1, v9, :cond_9

    .line 510
    invoke-virtual {p0, p2}, Lcom/vlingo/midas/gui/DialogFragment;->replaceUserEditBubble(Ljava/lang/String;)Lcom/vlingo/midas/gui/DialogBubble;

    move-result-object v2

    .line 511
    if-eqz v2, :cond_9

    .line 512
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Lcom/vlingo/midas/gui/DialogBubble;->setReplaceAvailable(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 619
    :catch_0
    move-exception v3

    .line 620
    .local v3, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 516
    .end local v3    # "e":Ljava/lang/NullPointerException;
    :cond_9
    if-nez v2, :cond_a

    .line 517
    :try_start_1
    sget v6, Lcom/vlingo/midas/R$layout;->dialog_bubble:I

    .line 518
    .local v6, "resId":I
    sget-object v9, Lcom/vlingo/midas/gui/DialogFragment$12;->$SwitchMap$com$vlingo$midas$gui$DialogBubble$BubbleType:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 531
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v6, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v2, v0

    .line 533
    invoke-virtual {v2, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnSlideTextListenr(Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;)V

    .line 538
    .end local v6    # "resId":I
    :cond_a
    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne p1, v9, :cond_f

    .line 539
    const/4 v8, 0x0

    .line 540
    .local v8, "view":Landroid/view/View;
    const/4 v1, 0x0

    .line 541
    .local v1, "countChild":I
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-eqz v9, :cond_b

    .line 542
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 544
    :cond_b
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v1, :cond_d

    .line 545
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    instance-of v9, v8, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v9, :cond_c

    .line 546
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v9

    sget-object v10, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v9, v10, :cond_c

    .line 547
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/vlingo/midas/gui/DialogBubble;->setOnEditListener(Lcom/vlingo/midas/gui/OnEditListener;)V

    .line 548
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/vlingo/midas/gui/DialogBubble;->setEditable(Z)V

    .line 549
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/vlingo/midas/gui/DialogBubble;->setTalkbackString(Z)V

    .line 544
    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 520
    .end local v1    # "countChild":I
    .end local v4    # "i":I
    .end local v8    # "view":Landroid/view/View;
    .restart local v6    # "resId":I
    :pswitch_0
    sget v6, Lcom/vlingo/midas/R$layout;->dialog_bubble_user_s:I

    .line 521
    goto :goto_1

    .line 525
    :pswitch_1
    sget v6, Lcom/vlingo/midas/R$layout;->dialog_bubble_vlingo_s:I

    .line 526
    goto :goto_1

    .line 528
    :pswitch_2
    sget v6, Lcom/vlingo/midas/R$layout;->dialog_bubble_wakeup_s:I

    goto :goto_1

    .line 554
    .end local v6    # "resId":I
    .restart local v1    # "countChild":I
    .restart local v4    # "i":I
    .restart local v8    # "view":Landroid/view/View;
    :cond_d
    invoke-virtual {v2, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnEditListener(Lcom/vlingo/midas/gui/OnEditListener;)V

    .line 555
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isAsrEditingEnabled()Z

    move-result v7

    .line 556
    .local v7, "supportedLang":Z
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v9

    if-eqz v9, :cond_10

    .line 557
    const/4 v9, 0x0

    sput-boolean v9, Lcom/vlingo/midas/gui/customviews/EditText;->Touchable:Z

    .line 560
    :goto_3
    invoke-virtual {v2, v7}, Lcom/vlingo/midas/gui/DialogBubble;->setEditable(Z)V

    .line 561
    invoke-virtual {v2, v7}, Lcom/vlingo/midas/gui/DialogBubble;->setTalkbackString(Z)V

    .line 562
    invoke-virtual {v2, p0}, Lcom/vlingo/midas/gui/DialogBubble;->setOnSlideTextListenr(Lcom/vlingo/midas/gui/DialogBubble$OnSlideTextListener;)V

    .line 567
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x2

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 569
    instance-of v9, v8, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v9, :cond_f

    .line 570
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v9

    sget-object v10, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v9, v10, :cond_e

    .line 575
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x4

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 579
    :cond_e
    instance-of v9, v8, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v9, :cond_f

    .line 580
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v9

    sget-object v10, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v9, v10, :cond_f

    .line 584
    move-object v0, v8

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v9, v0

    invoke-virtual {v9}, Lcom/vlingo/midas/gui/DialogBubble;->isReplaceAvailable()Z

    move-result v9

    if-eqz v9, :cond_f

    .line 585
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v8}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 605
    .end local v1    # "countChild":I
    .end local v4    # "i":I
    .end local v7    # "supportedLang":Z
    .end local v8    # "view":Landroid/view/View;
    :cond_f
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v9}, Ljava/util/Queue;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_11

    .line 607
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v9, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 609
    invoke-virtual {v2, p1, p2}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    .line 610
    move/from16 v0, p3

    invoke-virtual {p0, v2, v0}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V

    goto/16 :goto_0

    .line 559
    .restart local v1    # "countChild":I
    .restart local v4    # "i":I
    .restart local v7    # "supportedLang":Z
    .restart local v8    # "view":Landroid/view/View;
    :cond_10
    const/4 v9, 0x1

    sput-boolean v9, Lcom/vlingo/midas/gui/customviews/EditText;->Touchable:Z

    goto :goto_3

    .line 616
    .end local v1    # "countChild":I
    .end local v4    # "i":I
    .end local v7    # "supportedLang":Z
    .end local v8    # "view":Landroid/view/View;
    :cond_11
    move/from16 v0, p3

    invoke-virtual {v2, p1, p2, v0}, Lcom/vlingo/midas/gui/DialogBubble;->saveParam(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;Z)V

    .line 617
    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v9, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 518
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public addDummyView(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 916
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V

    .line 917
    return-void
.end method

.method public addHelpChoiceWidget()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 646
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    if-nez v2, :cond_0

    .line 668
    :goto_0
    return-void

    .line 650
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 651
    .local v0, "dvfsLockIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.sec.android.intent.action.DVFS_BOOSTER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 652
    const-string/jumbo v2, "PACKAGE"

    const-string/jumbo v3, "com.vlingo.midas"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 653
    const-string/jumbo v2, "DURATION"

    const-string/jumbo v3, "2000"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 654
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 656
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->mHelpWidgetExist:Z

    if-eqz v2, :cond_1

    .line 657
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->removeAlreadyExistingHelpScreen()V

    .line 660
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$layout;->widget_help_choice:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    .line 662
    .local v1, "v":Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;
    iput-boolean v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->mHelpWidgetExist:Z

    .line 663
    invoke-static {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->setLatestWidget(Lcom/vlingo/midas/gui/Widget;)V

    .line 665
    invoke-virtual {p0, v1, v5}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V

    .line 667
    invoke-static {v5}, Lcom/vlingo/midas/settings/MidasSettings;->setHelpVisible(Z)V

    goto :goto_0
.end method

.method public addHelpWidget(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 679
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    if-nez v2, :cond_0

    .line 693
    :goto_0
    return-void

    .line 683
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 684
    .local v0, "dvfsLockIntent":Landroid/content/Intent;
    const-string/jumbo v2, "com.sec.android.intent.action.DVFS_BOOSTER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 685
    const-string/jumbo v2, "PACKAGE"

    const-string/jumbo v3, "com.vlingo.midas"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 686
    const-string/jumbo v2, "DURATION"

    const-string/jumbo v3, "2000"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 687
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 689
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$layout;->widget_help:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/widgets/HelpWidget;

    .line 691
    .local v1, "v":Lcom/vlingo/midas/gui/widgets/HelpWidget;
    invoke-virtual {v1, p1}, Lcom/vlingo/midas/gui/widgets/HelpWidget;->setAddView(Landroid/content/Intent;)V

    .line 692
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public addMainPrompt()V
    .locals 5

    .prologue
    .line 2282
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getHomeMainPrompt()Ljava/lang/String;

    move-result-object v1

    .line 2285
    .local v1, "mainPrompt":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2286
    sget-object v2, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0, v2, v1, v3, v4}, Lcom/vlingo/midas/gui/DialogFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    .line 2288
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ControlFragment;

    .line 2291
    .local v0, "cf":Lcom/vlingo/midas/gui/ControlFragment;
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->currentTablet:Lcom/vlingo/midas/gui/DialogFragment$TabletType;

    sget-object v3, Lcom/vlingo/midas/gui/DialogFragment$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/DialogFragment$TabletType;

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2293
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .end local v0    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    check-cast v0, Lcom/vlingo/midas/gui/ControlFragment;

    .line 2300
    .restart local v0    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    :cond_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->isActivityCreated()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2313
    :cond_2
    :goto_0
    return-void

    .line 2305
    :cond_3
    if-nez v0, :cond_2

    goto :goto_0
.end method

.method public addTimerWidget()V
    .locals 4

    .prologue
    .line 806
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    if-nez v1, :cond_0

    .line 812
    :goto_0
    return-void

    .line 809
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$layout;->widget_timer:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;

    .line 811
    .local v0, "v":Lcom/vlingo/midas/gui/widgets/TimerWidget;
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public addTutorialWidget()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3244
    const/4 v0, 0x0

    .line 3245
    .local v0, "v":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3246
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$layout;->widget_tutorial_activity_k:I

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .end local v0    # "v":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    check-cast v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    .line 3250
    .restart local v0    # "v":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    :goto_0
    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/DialogFragment;->doaddTutorialView(Landroid/view/View;)V

    .line 3251
    return-void

    .line 3248
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$layout;->widget_tutorial_activity_l:I

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .end local v0    # "v":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    check-cast v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .restart local v0    # "v":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
    goto :goto_0
.end method

.method public addYouCanSayWidget()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 753
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 788
    :goto_0
    return-void

    .line 757
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/vlingo/midas/gui/DialogFragment;->actionBar:Landroid/app/ActionBar;

    if-eqz v3, :cond_2

    .line 761
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 763
    .local v1, "settings":Landroid/content/SharedPreferences;
    const-string/jumbo v3, "smartNotification"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/vlingo/midas/gui/DialogFragment;->eventList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    sget-object v3, Lcom/vlingo/midas/gui/DialogFragment;->eventList:Ljava/util/ArrayList;

    sget-object v4, Lcom/vlingo/midas/gui/DialogFragment;->birthdayList:Ljava/util/ArrayList;

    invoke-virtual {p0, v3, v4}, Lcom/vlingo/midas/gui/DialogFragment;->find(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 765
    const-string/jumbo v3, "YCS Inside"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->widget_smart:I

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;

    .line 767
    .local v2, "v":Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;
    const-string/jumbo v3, "YCS outside"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;->setAddView()V

    .line 769
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V

    .line 777
    .end local v2    # "v":Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;
    :goto_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->shadowVarInit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 778
    .end local v1    # "settings":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v0

    .line 779
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "SmartNotification"

    const-string/jumbo v4, "Exception Occurred in DialogFragment SmartNotification"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 772
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "settings":Landroid/content/SharedPreferences;
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->widget_you_can_say:I

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;

    .line 773
    .local v2, "v":Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setAddView()V

    .line 774
    const/16 v3, 0xd7

    iput v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    .line 775
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 784
    .end local v1    # "settings":Landroid/content/SharedPreferences;
    .end local v2    # "v":Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->widget_you_can_say:I

    invoke-static {v3, v4, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;

    .line 785
    .restart local v2    # "v":Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;->setAddView()V

    .line 786
    invoke-virtual {p0, v2, v5}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V

    goto/16 :goto_0
.end method

.method public cancelAsrEditing()V
    .locals 5

    .prologue
    .line 926
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    if-eqz v3, :cond_2

    .line 927
    const/4 v0, 0x0

    .line 928
    .local v0, "child":Landroid/view/View;
    const/4 v1, 0x0

    .line 929
    .local v1, "countChild":I
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_0

    .line 930
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .line 932
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 933
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v3, v0, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v3, :cond_1

    move-object v3, v0

    .line 934
    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v3

    sget-object v4, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v3, v4, :cond_1

    move-object v3, v0

    .line 935
    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->isUserTextReadyToTouch()Z

    move-result v3

    if-nez v3, :cond_1

    move-object v3, v0

    .line 938
    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->setUserTextReadyToTouch()V

    .line 932
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 944
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "countChild":I
    .end local v2    # "i":I
    :cond_2
    return-void
.end method

.method public checkWakeUpView(Landroid/view/View;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1433
    instance-of v0, p1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/vlingo/midas/gui/DialogBubble;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v0, v1, :cond_0

    .line 1435
    const/4 v0, 0x1

    .line 1437
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cleanupPreviousBubbles()V
    .locals 6

    .prologue
    .line 856
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    const/16 v5, 0xa

    if-le v4, v5, :cond_1

    .line 857
    const/4 v2, 0x0

    .line 858
    .local v2, "totalDummyCount":I
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 859
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 860
    .local v3, "v1":Landroid/view/View;
    instance-of v4, v3, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    if-eqz v4, :cond_0

    .line 861
    add-int/lit8 v2, v2, 0x1

    .line 862
    const/4 v4, 0x5

    if-lt v2, v4, :cond_0

    .line 863
    add-int/lit8 v1, v0, -0x2

    .local v1, "i1":I
    :goto_1
    if-ltz v1, :cond_1

    .line 864
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 863
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 858
    .end local v1    # "i1":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 871
    .end local v0    # "i":I
    .end local v2    # "totalDummyCount":I
    .end local v3    # "v1":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public cleanupPreviousBubblesForOnTrimMemory()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 874
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_2

    .line 875
    const/4 v1, 0x0

    .line 877
    .local v1, "totalDummyCount":I
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 878
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 879
    .local v3, "v1":Landroid/view/View;
    instance-of v5, v3, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    if-eqz v5, :cond_0

    .line 880
    add-int/lit8 v1, v1, 0x1

    .line 877
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 884
    .end local v3    # "v1":Landroid/view/View;
    :cond_1
    if-gt v1, v7, :cond_3

    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    instance-of v5, v5, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    instance-of v5, v5, Lcom/vlingo/midas/gui/widgets/SmartNotificationWidget;

    if-eqz v5, :cond_3

    .line 903
    .end local v0    # "i":I
    .end local v1    # "totalDummyCount":I
    :cond_2
    return-void

    .line 890
    .restart local v0    # "i":I
    .restart local v1    # "totalDummyCount":I
    :cond_3
    const/4 v2, 0x0

    .local v2, "v":I
    :goto_1
    const/16 v5, 0xa

    if-gt v2, v5, :cond_2

    .line 891
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 892
    .local v4, "v2":Landroid/view/View;
    if-eq v1, v7, :cond_2

    if-eqz v4, :cond_2

    .line 894
    instance-of v5, v4, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    if-eqz v5, :cond_4

    .line 895
    add-int/lit8 v1, v1, -0x1

    .line 896
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 890
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 898
    :cond_4
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto :goto_2
.end method

.method protected doAddView(Landroid/view/View;Z)V
    .locals 28
    .param p1, "v"    # Landroid/view/View;
    .param p2, "fillScreen"    # Z

    .prologue
    .line 958
    move-object/from16 v5, p1

    .line 959
    .local v5, "AddingView":Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v23

    if-eqz v23, :cond_0

    .line 960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v23 .. v27}, Landroid/widget/ScrollView;->setPadding(IIII)V

    .line 962
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v23

    if-eqz v23, :cond_3

    .line 964
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->setShadowWhileNotAnim:Z

    move/from16 v23, v0

    if-nez v23, :cond_1

    .line 966
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->shouldContinueBelow:I

    .line 967
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->removeMessages(I)V

    .line 968
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    move-object/from16 v23, v0

    const/16 v24, 0x4

    const-wide/16 v25, 0x3e8

    invoke-virtual/range {v23 .. v26}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 970
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->hideShadowBottom()V

    .line 981
    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorialOn()Z

    move-result v23

    if-eqz v23, :cond_3

    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v23

    if-eqz v23, :cond_3

    .line 1292
    :cond_2
    :goto_0
    return-void

    .line 986
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->isAdded()Z

    move-result v23

    if-eqz v23, :cond_2d

    .line 990
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/Widget;

    move/from16 v23, v0

    if-eqz v23, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->currentTablet:Lcom/vlingo/midas/gui/DialogFragment$TabletType;

    move-object/from16 v23, v0

    sget-object v24, Lcom/vlingo/midas/gui/DialogFragment$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/DialogFragment$TabletType;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_4

    .line 992
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/midas/gui/DialogFragment;->startAnimationTranslate(Landroid/view/View;)V

    .line 998
    :cond_4
    sget-object v23, Lcom/vlingo/midas/gui/DialogFragment;->currentPhone:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    sget-object v24, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->K_DEVICE:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_8

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    move/from16 v23, v0

    if-nez v23, :cond_8

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/Widget;

    move/from16 v23, v0

    if-nez v23, :cond_6

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v23, v0

    if-eqz v23, :cond_5

    move-object/from16 v23, p1

    check-cast v23, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual/range {v23 .. v23}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v23

    sget-object v24, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_6

    :cond_5
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v23, v0

    if-eqz v23, :cond_8

    move-object/from16 v23, p1

    check-cast v23, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual/range {v23 .. v23}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v23

    sget-object v24, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_8

    .line 999
    :cond_6
    const-string/jumbo v23, "translationY"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    fill-array-data v24, :array_0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v18

    .line 1000
    .local v18, "translateWidgetAnimation":Landroid/animation/ValueAnimator;
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/Widget;

    move/from16 v23, v0

    if-eqz v23, :cond_1c

    .line 1001
    const-wide/16 v23, 0x362

    move-object/from16 v0, v18

    move-wide/from16 v1, v23

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1007
    :goto_1
    new-instance v23, Lcom/vlingo/midas/gui/DialogFragment$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/gui/DialogFragment$1;-><init>(Lcom/vlingo/midas/gui/DialogFragment;Landroid/view/View;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1037
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v23

    if-eqz v23, :cond_7

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v23, v0

    if-nez v23, :cond_1e

    .line 1038
    :cond_7
    invoke-virtual/range {v18 .. v18}, Landroid/animation/ValueAnimator;->start()V

    .line 1045
    .end local v18    # "translateWidgetAnimation":Landroid/animation/ValueAnimator;
    :cond_8
    :goto_2
    const/4 v8, 0x0

    .line 1046
    .local v8, "btnPadding":I
    const/4 v14, 0x0

    .line 1051
    .local v14, "isWolframFlag":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->isFocusable()Z

    move-result v23

    if-nez v23, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->isFocusableInTouchMode()Z

    move-result v23

    if-eqz v23, :cond_a

    .line 1052
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->setFocusable(Z)V

    .line 1053
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 1057
    :cond_a
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/vlingo/midas/gui/ControlFragmentData;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v23

    sget-object v24, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_b

    .line 1058
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->cancelAsrEditing()V

    .line 1061
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->removeReallyWakeupBubble()V

    .line 1062
    if-eqz p2, :cond_c

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->isFullScreen:Z

    move/from16 v23, v0

    if-nez v23, :cond_c

    .line 1063
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->animateDialogExpand()V

    .line 1064
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->isFullScreen:Z

    .line 1069
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    sget v24, Lcom/vlingo/midas/R$dimen;->widget_padding_top:I

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 1070
    .local v22, "widgetPaddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    sget v24, Lcom/vlingo/midas/R$dimen;->widget_padding_bottom:I

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    .line 1072
    .local v21, "widgetPaddingBottom":I
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/Widget;

    move/from16 v23, v0

    if-eqz v23, :cond_20

    .line 1073
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v23

    add-int/lit8 v13, v23, -0x1

    .local v13, "i":I
    :goto_3
    if-ltz v13, :cond_f

    .line 1074
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    .line 1075
    .local v19, "v1":Landroid/view/View;
    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/vlingo/midas/gui/Widget;

    move/from16 v23, v0

    if-eqz v23, :cond_1f

    .line 1076
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v23

    const-string/jumbo v24, "ButtonWidget"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v23

    const-string/jumbo v24, "WolframAlphaWidget"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 1080
    const/4 v14, 0x1

    .line 1081
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->isWolfWidgetExist:Z

    .line 1082
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getHeight()I

    move-result v20

    .line 1083
    .local v20, "v1Height":I
    div-int/lit8 v23, v20, 0x4

    sub-int v8, v20, v23

    .line 1085
    .end local v20    # "v1Height":I
    :cond_d
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_f

    check-cast v19, Lcom/vlingo/midas/gui/Widget;

    .end local v19    # "v1":Landroid/view/View;
    invoke-virtual/range {v19 .. v19}, Lcom/vlingo/midas/gui/Widget;->isWidgetReplaceable()Z

    move-result v23

    if-nez v23, :cond_e

    move-object/from16 v23, p1

    check-cast v23, Lcom/vlingo/midas/gui/Widget;

    invoke-virtual/range {v23 .. v23}, Lcom/vlingo/midas/gui/Widget;->isPreviousSameWidgetReplaceable()Z

    move-result v23

    if-eqz v23, :cond_f

    .line 1088
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 1095
    :cond_f
    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    move/from16 v3, v24

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 1102
    .end local v13    # "i":I
    :goto_4
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/midas/gui/DialogFragment;->checkWakeUpView(Landroid/view/View;)Z

    move-result v23

    if-eqz v23, :cond_10

    .line 1103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    .line 1104
    .local v9, "cnt":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    add-int/lit8 v24, v9, -0x3

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    .line 1105
    .restart local v19    # "v1":Landroid/view/View;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v23

    const-string/jumbo v24, "ButtonWidget"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_10

    .line 1106
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->isBtnAndWakeBubble:Z

    .line 1110
    .end local v9    # "cnt":I
    .end local v19    # "v1":Landroid/view/View;
    :cond_10
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/NormalNewsWidget;

    move/from16 v23, v0

    if-eqz v23, :cond_11

    .line 1111
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->acquireWakeLock()V

    .line 1117
    :cond_11
    sget-object v23, Lcom/vlingo/midas/gui/DialogFragment;->currentPhone:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    sget-object v24, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->K_DEVICE:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_12

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/MusicWidget;

    move/from16 v23, v0

    if-eqz v23, :cond_12

    .line 1122
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->removeAlreadyExistingMusicWidget()V

    .line 1123
    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    move/from16 v3, v24

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 1124
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v10

    .line 1125
    .local v10, "df":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    sget-object v23, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getWidgetSpecificProperties(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)Ljava/util/Map;

    move-result-object v23

    if-nez v23, :cond_12

    .line 1126
    sget-object v23, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    const-string/jumbo v24, "embeddedMusic"

    const-string/jumbo v25, "true"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v10, v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->addWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    .end local v10    # "df":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    :cond_12
    sget-object v23, Lcom/vlingo/midas/gui/DialogFragment;->currentPhone:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    sget-object v24, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->K_DEVICE:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_13

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    move/from16 v23, v0

    if-eqz v23, :cond_13

    .line 1130
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v23

    if-eqz v23, :cond_22

    .line 1131
    const/16 v23, 0x1

    const/high16 v24, 0x41500000    # 13.0f

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v23

    move/from16 v0, v23

    float-to-int v7, v0

    .line 1132
    .local v7, "bottomMargin":I
    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 1142
    .end local v7    # "bottomMargin":I
    :cond_13
    :goto_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v23

    if-eqz v23, :cond_14

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v23, v0

    if-eqz v23, :cond_14

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->lastBubbleIsGreeting:Z

    move/from16 v23, v0

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_14

    .line 1143
    const/16 v23, 0x0

    const/16 v24, 0x1c

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 1145
    :cond_14
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v23, v0

    if-eqz v23, :cond_15

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v23

    if-eqz v23, :cond_15

    .line 1146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v24

    add-int/lit8 v24, v24, -0x2

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v23

    move-object/from16 v0, v23

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v23, v0

    if-eqz v23, :cond_15

    .line 1147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v24

    add-int/lit8 v24, v24, -0x2

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 1151
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v24

    add-int/lit8 v24, v24, -0x1

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1154
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCoverOpened()Z

    move-result v23

    if-nez v23, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    move-object/from16 v23, v0

    if-eqz v23, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Ljava/util/Queue;->isEmpty()Z

    move-result v23

    if-nez v23, :cond_16

    .line 1155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Ljava/util/Queue;->clear()V

    .line 1159
    :cond_16
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v23

    if-nez v23, :cond_17

    .line 1160
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->requestFocus()Z

    .line 1163
    :cond_17
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v23

    if-eqz v23, :cond_18

    .line 1164
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->removeOldContent()V

    .line 1166
    :cond_18
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    move/from16 v23, v0

    if-nez v23, :cond_19

    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/midas/gui/DialogFragment;->checkWakeUpView(Landroid/view/View;)Z

    move-result v23

    if-nez v23, :cond_19

    .line 1167
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->cleanupPreviousBubbles()V

    .line 1180
    :cond_19
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->hasOverScroll:Z

    .line 1181
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->isScrollingDone:Z

    .line 1183
    sget-object v23, Lcom/vlingo/midas/gui/DialogFragment;->currentPhone:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    sget-object v24, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->K_DEVICE:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_1a

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    move/from16 v23, v0

    if-nez v23, :cond_1a

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/Widget;

    move/from16 v23, v0

    if-eqz v23, :cond_1a

    .line 1184
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->scrollUpInCaseMoreHeight()V

    .line 1187
    :cond_1a
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    move/from16 v23, v0

    if-eqz v23, :cond_1b

    .line 1188
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->hasOverScroll:Z

    .line 1189
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->isScrollingDone:Z

    .line 1192
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->hasOverScroll:Z

    move/from16 v23, v0

    if-nez v23, :cond_28

    .line 1194
    if-eqz v14, :cond_24

    .line 1195
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->isBtnAndWakeBubble:Z

    move/from16 v23, v0

    if-eqz v23, :cond_23

    .line 1196
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->removeWakeupBubble()V

    .line 1197
    new-instance v6, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v8, v14}, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;-><init>(Lcom/vlingo/midas/gui/DialogFragment;IZ)V

    .line 1199
    .local v6, "anim":Lcom/vlingo/midas/gui/DialogFragment$AnimThread;
    invoke-virtual {v6}, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->start()V

    .line 1200
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->isBtnAndWakeBubble:Z

    .line 1205
    :goto_6
    new-instance v17, Ljava/lang/Thread;

    new-instance v23, Lcom/vlingo/midas/gui/DialogFragment$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment$2;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1219
    .local v17, "thread":Ljava/lang/Thread;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 1002
    .end local v6    # "anim":Lcom/vlingo/midas/gui/DialogFragment$AnimThread;
    .end local v8    # "btnPadding":I
    .end local v14    # "isWolframFlag":Z
    .end local v17    # "thread":Ljava/lang/Thread;
    .end local v21    # "widgetPaddingBottom":I
    .end local v22    # "widgetPaddingTop":I
    .restart local v18    # "translateWidgetAnimation":Landroid/animation/ValueAnimator;
    :cond_1c
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v23, v0

    if-eqz v23, :cond_1d

    move-object/from16 v23, p1

    check-cast v23, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual/range {v23 .. v23}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v23

    sget-object v24, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_1d

    .line 1003
    const-wide/16 v23, 0x384

    move-object/from16 v0, v18

    move-wide/from16 v1, v23

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    goto/16 :goto_1

    .line 1005
    :cond_1d
    const-wide/16 v23, 0x3b6

    move-object/from16 v0, v18

    move-wide/from16 v1, v23

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    goto/16 :goto_1

    .line 1039
    :cond_1e
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v23

    if-eqz v23, :cond_8

    .line 1040
    invoke-virtual/range {v18 .. v18}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_2

    .line 1073
    .end local v18    # "translateWidgetAnimation":Landroid/animation/ValueAnimator;
    .restart local v8    # "btnPadding":I
    .restart local v13    # "i":I
    .restart local v14    # "isWolframFlag":Z
    .restart local v19    # "v1":Landroid/view/View;
    .restart local v21    # "widgetPaddingBottom":I
    .restart local v22    # "widgetPaddingTop":I
    :cond_1f
    add-int/lit8 v13, v13, -0x1

    goto/16 :goto_3

    .line 1096
    .end local v13    # "i":I
    .end local v19    # "v1":Landroid/view/View;
    :cond_20
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;

    move/from16 v23, v0

    if-eqz v23, :cond_21

    .line 1097
    const/16 v23, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getPaddingTop()I

    move-result v24

    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_4

    .line 1099
    :cond_21
    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_4

    .line 1135
    :cond_22
    const/16 v23, 0x1

    const/high16 v24, 0x41200000    # 10.0f

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v23

    move/from16 v0, v23

    float-to-int v7, v0

    .line 1136
    .restart local v7    # "bottomMargin":I
    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3, v7}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_5

    .line 1202
    .end local v7    # "bottomMargin":I
    :cond_23
    new-instance v6, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    .line 1203
    .restart local v6    # "anim":Lcom/vlingo/midas/gui/DialogFragment$AnimThread;
    invoke-virtual {v6}, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->start()V

    goto/16 :goto_6

    .line 1221
    .end local v6    # "anim":Lcom/vlingo/midas/gui/DialogFragment$AnimThread;
    :cond_24
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    move/from16 v23, v0

    if-nez v23, :cond_2

    .line 1224
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    move/from16 v23, v0

    if-nez v23, :cond_28

    .line 1225
    const/4 v12, 0x0

    .line 1226
    .local v12, "height":I
    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    .line 1227
    .local v16, "r":Landroid/graphics/Rect;
    invoke-virtual/range {p0 .. p1}, Lcom/vlingo/midas/gui/DialogFragment;->checkWakeUpView(Landroid/view/View;)Z

    move-result v23

    if-eqz v23, :cond_27

    .line 1228
    const/16 v19, 0x0

    .line 1229
    .restart local v19    # "v1":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v23

    add-int/lit8 v13, v23, -0x1

    .restart local v13    # "i":I
    :goto_7
    if-lez v13, :cond_26

    .line 1230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    .line 1231
    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    move/from16 v23, v0

    if-nez v23, :cond_25

    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;

    move/from16 v23, v0

    if-nez v23, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    move-object/from16 v23, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_25

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    if-ne v0, v1, :cond_26

    .line 1229
    :cond_25
    add-int/lit8 v13, v13, -0x1

    goto :goto_7

    .line 1239
    :cond_26
    if-eqz v19, :cond_27

    .line 1240
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getHeight()I

    move-result v12

    .line 1243
    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1248
    .end local v13    # "i":I
    .end local v19    # "v1":Landroid/view/View;
    :cond_27
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    move/from16 v23, v0

    if-eqz v23, :cond_2c

    .line 1249
    move-object/from16 v11, p0

    .line 1250
    .local v11, "dialogFragment":Lcom/vlingo/midas/gui/DialogFragment;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    move-object/from16 v23, v0

    new-instance v24, Lcom/vlingo/midas/gui/DialogFragment$3;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11}, Lcom/vlingo/midas/gui/DialogFragment$3;-><init>(Lcom/vlingo/midas/gui/DialogFragment;Lcom/vlingo/midas/gui/DialogFragment;)V

    const-wide/16 v25, 0x64

    invoke-virtual/range {v23 .. v26}, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1274
    .end local v11    # "dialogFragment":Lcom/vlingo/midas/gui/DialogFragment;
    .end local v12    # "height":I
    .end local v16    # "r":Landroid/graphics/Rect;
    :cond_28
    :goto_8
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->isWolfWidgetExist:Z

    .line 1275
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->hasOverScroll:Z

    .line 1281
    .end local v8    # "btnPadding":I
    .end local v14    # "isWolframFlag":Z
    .end local v21    # "widgetPaddingBottom":I
    .end local v22    # "widgetPaddingTop":I
    :goto_9
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v23, v0

    if-eqz v23, :cond_29

    move-object/from16 v23, p1

    check-cast v23, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual/range {v23 .. v23}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v23

    sget-object v24, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_29

    .line 1282
    sput-object p1, Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;

    .line 1283
    const/16 v23, 0x1

    sput-boolean v23, Lcom/vlingo/midas/gui/DialogFragment;->vlingoFocus:Z

    .line 1285
    :cond_29
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v23

    if-eqz v23, :cond_2b

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/MessageWidget;

    move/from16 v23, v0

    if-nez v23, :cond_2a

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/MessageReadbackWidget;

    move/from16 v23, v0

    if-nez v23, :cond_2a

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/ComposeSocialStatusWidget;

    move/from16 v23, v0

    if-eqz v23, :cond_2b

    .line 1287
    :cond_2a
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment;->doScroll()V

    .line 1289
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    .line 1290
    .local v15, "lp":Landroid/view/ViewGroup$LayoutParams;
    const/16 v23, 0x0

    move/from16 v0, v23

    iput v0, v15, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1260
    .end local v15    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .restart local v8    # "btnPadding":I
    .restart local v12    # "height":I
    .restart local v14    # "isWolframFlag":Z
    .restart local v16    # "r":Landroid/graphics/Rect;
    .restart local v21    # "widgetPaddingBottom":I
    .restart local v22    # "widgetPaddingTop":I
    :cond_2c
    move-object/from16 v11, p0

    .line 1261
    .restart local v11    # "dialogFragment":Lcom/vlingo/midas/gui/DialogFragment;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    move-object/from16 v23, v0

    new-instance v24, Lcom/vlingo/midas/gui/DialogFragment$4;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11}, Lcom/vlingo/midas/gui/DialogFragment$4;-><init>(Lcom/vlingo/midas/gui/DialogFragment;Lcom/vlingo/midas/gui/DialogFragment;)V

    const-wide/16 v25, 0x64

    invoke-virtual/range {v23 .. v26}, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_8

    .line 1278
    .end local v8    # "btnPadding":I
    .end local v11    # "dialogFragment":Lcom/vlingo/midas/gui/DialogFragment;
    .end local v12    # "height":I
    .end local v14    # "isWolframFlag":Z
    .end local v16    # "r":Landroid/graphics/Rect;
    .end local v21    # "widgetPaddingBottom":I
    .end local v22    # "widgetPaddingTop":I
    :cond_2d
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    goto :goto_9

    .line 999
    :array_0
    .array-data 4
        0x44f00000    # 1920.0f
        0x0
    .end array-data
.end method

.method public doScroll()V
    .locals 4

    .prologue
    .line 1447
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 1448
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    new-instance v1, Lcom/vlingo/midas/gui/DialogFragment$5;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/DialogFragment$5;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1455
    :cond_0
    return-void
.end method

.method find(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/midas/gui/widgets/ScheduleEventData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "a":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vlingo/midas/gui/widgets/ScheduleEventData;>;"
    .local p2, "list1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x2

    const/4 v4, 0x0

    .line 697
    const/4 v3, 0x0

    .line 698
    .local v3, "i":I
    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->alldaySchedules:I

    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->schedules:I

    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->birthdays:I

    .line 699
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 701
    .local v0, "curr":J
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 702
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    .line 703
    .local v2, "data":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getType()I

    move-result v5

    if-nez v5, :cond_2

    .line 705
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getAllday()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 707
    iget v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->alldaySchedules:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->alldaySchedules:I

    .line 724
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 709
    :cond_1
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getMill()J

    move-result-wide v5

    cmp-long v5, v5, v0

    if-lez v5, :cond_0

    .line 711
    iget v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->schedules:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->schedules:I

    goto :goto_1

    .line 716
    :cond_2
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getType()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 718
    if-eqz p2, :cond_0

    .line 720
    iget v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->birthdays:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->birthdays:I

    goto :goto_1

    .line 727
    .end local v2    # "data":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :cond_3
    iget v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->schedules:I

    if-gtz v5, :cond_4

    iget v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->alldaySchedules:I

    if-gtz v5, :cond_4

    iget v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->birthdays:I

    if-lez v5, :cond_7

    .line 729
    :cond_4
    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    .line 730
    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->birthdays:I

    if-lez v4, :cond_5

    .line 732
    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->birthdays:I

    if-ge v4, v7, :cond_8

    .line 733
    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    add-int/lit8 v4, v4, 0x2d

    add-int/lit8 v4, v4, 0x2d

    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    .line 737
    :cond_5
    :goto_2
    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->schedules:I

    iget v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->alldaySchedules:I

    add-int/2addr v4, v5

    if-lez v4, :cond_6

    .line 739
    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->schedules:I

    iget v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->alldaySchedules:I

    add-int/2addr v4, v5

    if-ge v4, v7, :cond_9

    .line 740
    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    add-int/lit8 v4, v4, 0x4b

    add-int/lit8 v4, v4, 0x32

    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    .line 744
    :cond_6
    :goto_3
    const/4 v4, 0x1

    .line 747
    :cond_7
    return v4

    .line 735
    :cond_8
    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    add-int/lit8 v4, v4, 0x5a

    add-int/lit8 v4, v4, 0x2d

    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    goto :goto_2

    .line 742
    :cond_9
    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    add-int/lit16 v4, v4, 0x96

    add-int/lit8 v4, v4, 0x32

    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->topShadowDp:I

    goto :goto_3
.end method

.method findLastBubbleByType(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;)Lcom/vlingo/midas/gui/DialogBubble;
    .locals 3
    .param p1, "type"    # Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    .prologue
    .line 2520
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_1

    .line 2521
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 2522
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2523
    .local v1, "v1":Landroid/view/View;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 2525
    check-cast v1, Lcom/vlingo/midas/gui/DialogBubble;

    .line 2528
    .end local v0    # "i":I
    .end local v1    # "v1":Landroid/view/View;
    :goto_1
    return-object v1

    .line 2521
    .restart local v0    # "i":I
    .restart local v1    # "v1":Landroid/view/View;
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2528
    .end local v0    # "i":I
    .end local v1    # "v1":Landroid/view/View;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getdialogScroll()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method public getdialogScrollView()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public hideMusic()V
    .locals 2

    .prologue
    .line 1511
    invoke-super {p0}, Lcom/vlingo/midas/gui/ContentFragment;->hideMusic()V

    .line 1513
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    .line 1514
    .local v0, "df":Lcom/vlingo/core/internal/dialogmanager/DialogFlow;
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getWidgetSpecificProperties(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1515
    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;->MusicPlayingWidget:Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->removeWidgetSpecificProperty(Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;)V

    .line 1517
    :cond_0
    return-void
.end method

.method public hideShadowBottom()V
    .locals 2

    .prologue
    .line 1344
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 1345
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialog_full_shadowbelow:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1346
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialog_full_shadowbelow:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1349
    :cond_0
    return-void
.end method

.method public isActivityCreated()Z
    .locals 1

    .prologue
    .line 3145
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInEditMode()Z
    .locals 1

    .prologue
    .line 3089
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->isEditStarted:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 224
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/ContentFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 225
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    sput-object v4, Lcom/vlingo/midas/gui/DialogFragment;->actionBar:Landroid/app/ActionBar;

    .line 233
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v4

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v4, v5, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v4

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v4, v5, :cond_1

    .line 234
    :cond_0
    sget-object v4, Lcom/vlingo/midas/gui/DialogFragment$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/DialogFragment$TabletType;

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->currentTablet:Lcom/vlingo/midas/gui/DialogFragment$TabletType;

    .line 240
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 241
    sget-object v4, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->K_DEVICE:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    sput-object v4, Lcom/vlingo/midas/gui/DialogFragment;->currentPhone:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    .line 244
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getView()Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->dialog_full_container:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    .line 246
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getView()Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->dialog_scroll_content:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    .line 249
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 251
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getView()Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->dialog_scrollview:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ScrollView;

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    .line 254
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getView()Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->pull_tab:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->pullTab:Landroid/widget/ImageView;

    .line 255
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    sget v5, Lcom/vlingo/midas/R$id;->buffer:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    .line 262
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v3

    .line 263
    .local v3, "languageApplication":Ljava/lang/String;
    iput-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->fragmentLanguage:Ljava/lang/String;

    .line 265
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->trackingTouch:Z

    .line 266
    const/4 v4, 0x0

    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->openCloseThreshold:F

    .line 267
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    invoke-virtual {v4, p0}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 268
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v4

    if-nez v4, :cond_3

    .line 269
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->animateDialogExpand()V

    .line 271
    :cond_3
    iput-boolean v6, p0, Lcom/vlingo/midas/gui/DialogFragment;->isFullScreen:Z

    .line 273
    const/4 v2, 0x0

    .line 274
    .local v2, "isNeedShowTimer":Z
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getView()Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->widget_timer:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/gui/widgets/TimerWidget;

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    .line 277
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    if-eqz v4, :cond_4

    .line 278
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    invoke-virtual {v4, p1}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->onRestoreInstanceState(Landroid/os/Bundle;)Z

    move-result v2

    .line 280
    if-eqz v2, :cond_4

    .line 281
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->addTimerWidget()V

    .line 285
    :cond_4
    new-instance v4, Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/DialogFragment;Lcom/vlingo/midas/gui/DialogFragment$1;)V

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->moveListBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 286
    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v4, "LIST_MOVE_ACTION"

    invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 287
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v4, "LIST_MOVE_ACTION"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 288
    const-string/jumbo v4, "com.vlingo.LANGUAGE_CHANGED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 289
    const-string/jumbo v4, "SHOW_LIST_ACTION"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->moveListBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v1}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 293
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v4

    sget-object v5, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v4, v5, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->registerTaskRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 298
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ConversationActivity;

    .line 299
    .local v0, "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 300
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->isDrivingMode()Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    move-result-object v4

    sget-object v5, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    if-eq v4, v5, :cond_5

    .line 303
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->addYouCanSayWidget()V

    .line 307
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->addMainPrompt()V

    .line 333
    :cond_5
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    if-nez v4, :cond_6

    if-nez v2, :cond_6

    .line 342
    :cond_6
    iput-boolean v6, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    .line 343
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1467
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/ContentFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1469
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1483
    :cond_0
    :goto_0
    return-void

    .line 1472
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->setDialogContent()V

    .line 1474
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->isAsrEditingEnabled()Z

    move-result v0

    .line 1479
    .local v0, "enableASREdting":Z
    invoke-static {}, Lcom/vlingo/midas/gui/ConversationActivity;->getLatestWidget()Lcom/vlingo/midas/gui/Widget;

    move-result-object v1

    instance-of v1, v1, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-nez v1, :cond_0

    .line 1480
    if-eqz v0, :cond_2

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->isEditStarted:Z

    if-nez v1, :cond_0

    .line 1481
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->doScroll()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 197
    invoke-static {p0}, Lcom/vlingo/midas/util/MiniModeUtils;->addListener(Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;)V

    .line 198
    sget v1, Lcom/vlingo/midas/R$layout;->dialog_view:I

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 200
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 201
    sget v1, Lcom/vlingo/midas/R$id;->dialog_scroll_content:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    .line 202
    sget v1, Lcom/vlingo/midas/R$id;->dialog_full_shadowbelow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialog_full_shadowbelow:Landroid/widget/ImageView;

    .line 204
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->metrics:Landroid/util/DisplayMetrics;

    .line 207
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->metrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 210
    :cond_1
    iput-boolean v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->isFullScreen:Z

    .line 212
    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 381
    invoke-super {p0}, Lcom/vlingo/midas/gui/ContentFragment;->onDestroy()V

    .line 382
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    .line 383
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->isEditStarted:Z

    .line 384
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->unbindDrawables(Landroid/view/View;)V

    .line 385
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setHelpVisible(Z)V

    .line 387
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->moveListBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 388
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->moveListBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 390
    :cond_0
    invoke-static {p0}, Lcom/vlingo/midas/util/MiniModeUtils;->removeListener(Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;)V

    .line 391
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v0, v1, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 393
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;->RECOGNITION_START:Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;

    invoke-virtual {v0, v1, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->unregisterTaskFlowRegulator(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator;)V

    .line 396
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 397
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 398
    iput-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    .line 400
    :cond_1
    return-void
.end method

.method public onEditCanceled(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 3005
    const/4 v2, 0x0

    .line 3007
    .local v2, "cf":Lcom/vlingo/midas/gui/ControlFragment;
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    if-eqz v3, :cond_2

    .line 3008
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3009
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/widget/ScrollView;->setPadding(IIII)V

    .line 3010
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v6, :cond_4

    .line 3011
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .end local v2    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    check-cast v2, Lcom/vlingo/midas/gui/ControlFragment;

    .line 3020
    .restart local v2    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->isActivityCreated()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3023
    invoke-virtual {v2, v6}, Lcom/vlingo/midas/gui/ControlFragment;->setToIdleByEditCancel(Z)V

    .line 3024
    sget-object v3, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 3025
    invoke-virtual {v2, v5}, Lcom/vlingo/midas/gui/ControlFragment;->setToIdleByEditCancel(Z)V

    .line 3028
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3029
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->setMicStatusText()V

    .line 3038
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->removeOldContent()V

    .line 3046
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    .line 3047
    .local v0, "anim":Lcom/vlingo/midas/gui/DialogFragment$AnimThread;
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->start()V

    .line 3050
    .end local v0    # "anim":Lcom/vlingo/midas/gui/DialogFragment$AnimThread;
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v3

    if-nez v3, :cond_3

    .line 3052
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/ConversationActivity;

    .line 3053
    .local v1, "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    if-eqz v1, :cond_3

    .line 3054
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->showControlFragment()V

    .line 3057
    .end local v1    # "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    :cond_3
    iput-boolean v5, p0, Lcom/vlingo/midas/gui/DialogFragment;->isEditStarted:Z

    .line 3058
    return-void

    .line 3015
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .end local v2    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    check-cast v2, Lcom/vlingo/midas/gui/ControlFragment;

    .restart local v2    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    goto :goto_0
.end method

.method public onEditFinished(Landroid/view/View;)V
    .locals 13
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x1

    .line 2911
    iget-boolean v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    if-nez v8, :cond_1

    .line 2994
    :cond_0
    :goto_0
    return-void

    .line 2915
    :cond_1
    iput-boolean v10, p0, Lcom/vlingo/midas/gui/DialogFragment;->isEditStarted:Z

    .line 2920
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v8

    add-int/lit8 v4, v8, -0x2

    .local v4, "i":I
    :goto_1
    if-ltz v4, :cond_2

    .line 2921
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2923
    .local v1, "child":Landroid/view/View;
    if-ne p1, v1, :cond_6

    .line 2948
    .end local v1    # "child":Landroid/view/View;
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->removeOldContent()V

    .line 2949
    iput-boolean v10, p0, Lcom/vlingo/midas/gui/DialogFragment;->isEditStarted:Z

    .line 2951
    const/4 v6, 0x0

    .line 2952
    .local v6, "nextViewTotalHeight":I
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 2954
    .local v7, "view":Landroid/view/View;
    instance-of v8, v7, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v8, :cond_3

    move-object v8, v7

    .line 2955
    check-cast v8, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v8

    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v8, v9, :cond_3

    .line 2956
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v9, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x3

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 2960
    :cond_3
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 2961
    .local v0, "b":I
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v8

    sub-int v6, v0, v8

    .line 2963
    const/16 v3, 0x320

    .line 2964
    .local v3, "delay":I
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v8}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v8

    if-le v6, v8, :cond_4

    .line 2967
    const/16 v3, 0x514

    .line 2970
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v8

    if-nez v8, :cond_5

    .line 2971
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    new-instance v9, Lcom/vlingo/midas/gui/DialogFragment$11;

    invoke-direct {v9, p0}, Lcom/vlingo/midas/gui/DialogFragment$11;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    int-to-long v10, v3

    invoke-virtual {v8, v9, v10, v11}, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2989
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-static {v8}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2990
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v8, v12}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 2991
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v8, v12}, Landroid/view/View;->setFocusable(Z)V

    .line 2992
    iget-object v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0

    .line 2928
    .end local v0    # "b":I
    .end local v3    # "delay":I
    .end local v6    # "nextViewTotalHeight":I
    .end local v7    # "view":Landroid/view/View;
    .restart local v1    # "child":Landroid/view/View;
    :cond_6
    instance-of v8, v1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v8, :cond_7

    .line 2929
    check-cast v1, Lcom/vlingo/midas/gui/DialogBubble;

    .end local v1    # "child":Landroid/view/View;
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v8

    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v8, v9, :cond_7

    .line 2920
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_7
    move-object v2, p1

    .line 2937
    check-cast v2, Lcom/vlingo/midas/gui/DialogBubble;

    .line 2939
    .local v2, "db":Lcom/vlingo/midas/gui/DialogBubble;
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v5

    .line 2941
    .local v5, "newTxt":Ljava/lang/String;
    sget-object v8, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {p0, v8, v5, v12, v10}, Lcom/vlingo/midas/gui/DialogFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    move-result-object v8

    invoke-virtual {v8, v12}, Lcom/vlingo/midas/gui/DialogBubble;->setReplaceAvailable(Z)V

    .line 2944
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->replaceOriginalText()V

    goto/16 :goto_2
.end method

.method public onEditStarted(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const/high16 v7, 0x43480000    # 200.0f

    const/4 v6, 0x0

    .line 2783
    iget-boolean v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z

    if-eqz v4, :cond_4

    .line 2784
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2785
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v0, v4, Landroid/util/DisplayMetrics;->density:F

    .line 2786
    .local v0, "a":F
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 2787
    .local v3, "r":Landroid/graphics/Rect;
    invoke-virtual {p1, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2788
    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    const v5, 0x439d8000    # 315.0f

    mul-float/2addr v5, v0

    float-to-int v5, v5

    if-le v4, v5, :cond_0

    .line 2789
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    mul-float v5, v7, v0

    float-to-int v5, v5

    invoke-virtual {v4, v6, v6, v6, v5}, Landroid/widget/ScrollView;->setPadding(IIII)V

    .line 2790
    iget-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    mul-float v5, v7, v0

    float-to-int v5, v5

    invoke-virtual {v4, v6, v5}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 2862
    .end local v0    # "a":F
    .end local v3    # "r":Landroid/graphics/Rect;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/ConversationActivity;

    .line 2863
    .local v1, "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    if-eqz v1, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2865
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->hideControlFragment()V

    .line 2869
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->removeWakeupBubble()V

    .line 2871
    const/4 v2, 0x0

    .line 2873
    .local v2, "cf":Lcom/vlingo/midas/gui/ControlFragment;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v8, :cond_5

    .line 2874
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .end local v2    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    check-cast v2, Lcom/vlingo/midas/gui/ControlFragment;

    .line 2883
    .restart local v2    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    :goto_0
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->isActivityCreated()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2885
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2886
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->removeMessages()V

    .line 2889
    :cond_2
    sget-object v4, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v2, v4}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 2891
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2892
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentData;->getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/ControlFragmentData;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v4

    sget-object v5, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v4, v5, :cond_3

    .line 2893
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragment;->removeMicStatusText()V

    .line 2897
    :cond_3
    iput-boolean v8, p0, Lcom/vlingo/midas/gui/DialogFragment;->isEditStarted:Z

    .line 2898
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragment;->prevEditView:Landroid/view/View;

    .line 2900
    .end local v1    # "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    .end local v2    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    :cond_4
    return-void

    .line 2878
    .restart local v1    # "ca":Lcom/vlingo/midas/gui/ConversationActivity;
    .restart local v2    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .end local v2    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    check-cast v2, Lcom/vlingo/midas/gui/ControlFragment;

    .restart local v2    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    goto :goto_0
.end method

.method protected onIdle()V
    .locals 2

    .prologue
    .line 3139
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->WakelockAcquired:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "dm_main"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3140
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->releaseWakeLock()V

    .line 3142
    :cond_0
    return-void
.end method

.method public onLanguageChanged()V
    .locals 5

    .prologue
    .line 821
    const-string/jumbo v3, "language"

    const-string/jumbo v4, "en-US"

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->fragmentLanguage:Ljava/lang/String;

    .line 824
    sget-object v3, Lcom/vlingo/midas/gui/DialogFragment;->eventList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 826
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v3, Lcom/vlingo/midas/gui/DialogFragment;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 828
    sget-object v3, Lcom/vlingo/midas/gui/DialogFragment;->eventList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    .line 829
    .local v2, "temp":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    invoke-virtual {v2}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->getAllday()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 831
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->all_day:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->setStartDate(Ljava/lang/String;)V

    .line 826
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 836
    .end local v0    # "i":I
    .end local v2    # "temp":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->resetAllContent()V

    .line 840
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 841
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    const/4 v3, 0x0

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 842
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 844
    return-void
.end method

.method public onMiniModeChange(Z)V
    .locals 1
    .param p1, "isMiniMode"    # Z

    .prologue
    .line 3262
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment;->actionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 3263
    if-eqz p1, :cond_1

    .line 3264
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment;->actionBar:Landroid/app/ActionBar;

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->hideActionBar(Landroid/app/ActionBar;)V

    .line 3276
    :cond_0
    :goto_0
    return-void

    .line 3266
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment;->actionBar:Landroid/app/ActionBar;

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->showActionBar(Landroid/app/ActionBar;)V

    .line 3267
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 3268
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    .line 3270
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->setDialogContent()V

    .line 3271
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->showYCSW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3272
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->addYouCanSayWidget()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 355
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->ThreadstopShadowBelow:I

    .line 359
    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->ThreadstopShadowTop:I

    .line 361
    :cond_0
    invoke-super {p0}, Lcom/vlingo/midas/gui/ContentFragment;->onPause()V

    .line 362
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 365
    :cond_1
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->WakelockAcquired:Z

    if-eqz v0, :cond_2

    .line 366
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->releaseWakeLock()V

    .line 368
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 369
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->prevEditView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogFragment;->onEditCanceled(Landroid/view/View;)V

    .line 371
    :cond_3
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2698
    invoke-super {p0}, Lcom/vlingo/midas/gui/ContentFragment;->onResume()V

    .line 2699
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->setDialogContent()V

    .line 2700
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703
    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->ThreadstopShadowBelow:I

    .line 2704
    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->ThreadstopShadowTop:I

    .line 2705
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->setShadowTop()V

    .line 2706
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->setShadowBottom()V

    .line 2708
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 411
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/ContentFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 412
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->widget_timer:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/TimerWidget;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    .line 414
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->timerWidget:Lcom/vlingo/midas/gui/widgets/TimerWidget;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/widgets/TimerWidget;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 417
    :cond_0
    return-void
.end method

.method public onSlideTextEnd(Ljava/lang/String;)V
    .locals 5
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 2378
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2380
    .local v0, "db":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 2383
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 2386
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2388
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 2390
    .local v1, "newDb":Landroid/view/View;
    instance-of v2, v1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v2, :cond_1

    .line 2392
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v2, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-object v2, v1

    .line 2395
    check-cast v2, Lcom/vlingo/midas/gui/DialogBubble;

    move-object v3, v1

    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v4

    move-object v3, v1

    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V

    move-object v2, v1

    .line 2398
    check-cast v2, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->isFillScreen()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V

    .line 2407
    .end local v1    # "newDb":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 2402
    .restart local v1    # "newDb":Landroid/view/View;
    :cond_1
    if-eqz v1, :cond_0

    .line 2403
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public onSlideTextStart(Ljava/lang/String;)V
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 2418
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2420
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/vlingo/midas/gui/Widget;

    if-eqz v1, :cond_0

    .line 2422
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2423
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 2424
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->doAddView(Landroid/view/View;Z)V

    .line 2427
    .end local v0    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onTaskWaitingToStart(Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;Lcom/vlingo/core/internal/dialogmanager/ResumeControl;)V
    .locals 6
    .param p1, "type"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlowTaskRegulator$EventType;
    .param p2, "control"    # Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .prologue
    .line 432
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    monitor-enter v1

    .line 435
    :try_start_0
    iput-object p2, p0, Lcom/vlingo/midas/gui/DialogFragment;->mResumeControl:Lcom/vlingo/core/internal/dialogmanager/ResumeControl;

    .line 436
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mTimer:Ljava/util/Timer;

    .line 438
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/vlingo/midas/gui/DialogFragment$ResumeControlPollTask;-><init>(Lcom/vlingo/midas/gui/DialogFragment;Lcom/vlingo/midas/gui/DialogFragment$1;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x2ee

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 440
    return-void

    .line 436
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 2174
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2175
    .local v0, "action":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 2176
    .local v1, "y":F
    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->pullTab:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTop()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->pullTab:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getBottom()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v1, v2

    if-gez v2, :cond_1

    .line 2178
    iput-boolean v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->trackingTouch:Z

    .line 2179
    iput v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->openCloseThreshold:F

    .line 2180
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/DialogFragment;->setDialogHeight(F)V

    .line 2192
    :cond_0
    :goto_0
    return v3

    .line 2181
    :cond_1
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->trackingTouch:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    if-ne v0, v3, :cond_4

    .line 2183
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->trackingTouch:Z

    .line 2184
    iget v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->openCloseThreshold:F

    cmpl-float v2, v1, v2

    if-lez v2, :cond_3

    .line 2185
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->animateDialogExpand()V

    goto :goto_0

    .line 2186
    :cond_3
    iget v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->openCloseThreshold:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 2187
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 2189
    :cond_4
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->trackingTouch:Z

    if-eqz v2, :cond_0

    .line 2190
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/DialogFragment;->setDialogHeight(F)V

    goto :goto_0
.end method

.method protected onUserCancel()V
    .locals 2

    .prologue
    .line 3132
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->WakelockAcquired:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "dm_main"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3133
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->releaseWakeLock()V

    .line 3135
    :cond_0
    return-void
.end method

.method public removeAlreadyExistingHelpScreen()V
    .locals 3

    .prologue
    .line 2606
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 2607
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2608
    .local v1, "v1":Landroid/view/View;
    if-eqz v1, :cond_1

    instance-of v2, v1, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-eqz v2, :cond_1

    .line 2609
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2613
    .end local v1    # "v1":Landroid/view/View;
    :cond_0
    return-void

    .line 2606
    .restart local v1    # "v1":Landroid/view/View;
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public removeAlreadyExistingMusicWidget()V
    .locals 3

    .prologue
    .line 3121
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 3122
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3123
    .local v1, "v1":Landroid/view/View;
    if-eqz v1, :cond_1

    instance-of v2, v1, Lcom/vlingo/midas/gui/widgets/MusicWidget;

    if-eqz v2, :cond_1

    .line 3124
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 3128
    .end local v1    # "v1":Landroid/view/View;
    :cond_0
    return-void

    .line 3121
    .restart local v1    # "v1":Landroid/view/View;
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public removeMicStatusText()V
    .locals 0

    .prologue
    .line 1522
    return-void
.end method

.method public removeReallyWakeupBubble()V
    .locals 2

    .prologue
    .line 2552
    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->findLastBubbleByType(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;)Lcom/vlingo/midas/gui/DialogBubble;

    move-result-object v0

    .line 2553
    .local v0, "db":Lcom/vlingo/midas/gui/DialogBubble;
    if-eqz v0, :cond_0

    .line 2554
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2556
    :cond_0
    return-void
.end method

.method public removeWakeupBubble()V
    .locals 2

    .prologue
    .line 2538
    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->findLastBubbleByType(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;)Lcom/vlingo/midas/gui/DialogBubble;

    move-result-object v0

    .line 2539
    .local v0, "db":Lcom/vlingo/midas/gui/DialogBubble;
    if-eqz v0, :cond_0

    .line 2540
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogBubble;->setVisibility(I)V

    .line 2542
    :cond_0
    return-void
.end method

.method public replaceUserEditBubble(Ljava/lang/String;)Lcom/vlingo/midas/gui/DialogBubble;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 2582
    const/4 v0, 0x0

    .line 2583
    .local v0, "db":Lcom/vlingo/midas/gui/DialogBubble;
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 2584
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2585
    .local v2, "v1":Landroid/view/View;
    if-eqz v2, :cond_0

    instance-of v3, v2, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v3, :cond_0

    move-object v3, v2

    check-cast v3, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/DialogBubble;->isReplaceAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 2587
    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    .line 2588
    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/DialogBubble;->setText(Ljava/lang/String;)V

    .line 2589
    sget-object v3, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {v0, v3}, Lcom/vlingo/midas/gui/DialogBubble;->setType(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;)V

    move-object v3, v0

    .line 2593
    .end local v2    # "v1":Landroid/view/View;
    :goto_1
    return-object v3

    .line 2583
    .restart local v2    # "v1":Landroid/view/View;
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 2593
    .end local v2    # "v1":Landroid/view/View;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public resetAllContent()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2436
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2437
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 2440
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2441
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 2444
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-nez v1, :cond_2

    .line 2472
    :goto_0
    return-void

    .line 2446
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2449
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2450
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getReservedHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2451
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2452
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2453
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2455
    iput-boolean v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->lastBubbleIsGreeting:Z

    .line 2457
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "LangTest : S Voice Language 1 resetAllContent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "language"

    const-string/jumbo v4, "en-US"

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2459
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 2466
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->addYouCanSayWidget()V

    .line 2471
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->addMainPrompt()V

    goto :goto_0
.end method

.method public resetAllContentForTutorial(Z)V
    .locals 4
    .param p1, "isForTutorial"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2486
    if-eqz p1, :cond_3

    .line 2487
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2488
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mWaitingQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 2491
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2492
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->mRunningQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 2495
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-nez v1, :cond_2

    .line 2516
    :goto_0
    return-void

    .line 2498
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2500
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2501
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getReservedHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2502
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 2503
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2504
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2505
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->buffer:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2508
    iput-boolean v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->lastBubbleIsGreeting:Z

    .line 2509
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    goto :goto_0

    .line 2511
    .end local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2512
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->removeViewAt(I)V

    .line 2514
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->resetAllContent()V

    goto :goto_0
.end method

.method public resetScreen()V
    .locals 1

    .prologue
    .line 2480
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 2483
    :goto_0
    return-void

    .line 2482
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    goto :goto_0
.end method

.method public setDialogContent()V
    .locals 7

    .prologue
    const/4 v3, -0x1

    const/4 v6, 0x0

    .line 2754
    const/4 v1, 0x0

    .line 2755
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 2760
    :cond_0
    sget-boolean v4, Lcom/vlingo/midas/gui/ConversationActivity;->isTutorial:Z

    if-nez v4, :cond_1

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_1
    move v0, v3

    .line 2761
    .local v0, "height":I
    :goto_0
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v1    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {v1, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2762
    .restart local v1    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2769
    .end local v0    # "height":I
    :goto_1
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    if-eqz v3, :cond_2

    if-eqz v1, :cond_2

    .line 2770
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v3, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2772
    :cond_2
    return-void

    .line 2760
    :cond_3
    const/4 v0, -0x2

    goto :goto_0

    .line 2765
    :cond_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$dimen;->land_scrolldialog_marginleft:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2766
    .local v2, "marginLeft":I
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v1    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2767
    .restart local v1    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v2, v6, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_1
.end method

.method public setMicStatusText()V
    .locals 0

    .prologue
    .line 1507
    return-void
.end method

.method public setShadowBottom()V
    .locals 1

    .prologue
    .line 2735
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragment$10;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/DialogFragment$10;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    .line 2751
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2752
    return-void
.end method

.method public setShadowTop()V
    .locals 1

    .prologue
    .line 2713
    new-instance v0, Lcom/vlingo/midas/gui/DialogFragment$9;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/DialogFragment$9;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    .line 2730
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2731
    return-void
.end method

.method public startAnimationFlipDown(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v6, 0x1

    .line 2350
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v3, v1, v2

    .line 2351
    .local v3, "centerX":F
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v4, v1, v2

    .line 2352
    .local v4, "centerY":F
    new-instance v0, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v5, 0x439b0000    # 310.0f

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;-><init>(FFFFFZZ)V

    .line 2354
    .local v0, "rotation":Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;
    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setDuration(J)V

    .line 2355
    invoke-virtual {v0, v6}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setFillAfter(Z)V

    .line 2356
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2357
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2358
    return-void
.end method

.method public startAnimationFlipUp(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    const/high16 v2, 0x40000000    # 2.0f

    .line 2331
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v3, v1, v2

    .line 2332
    .local v3, "centerX":F
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v4, v1, v2

    .line 2333
    .local v4, "centerY":F
    new-instance v0, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;

    const/high16 v1, -0x3d4c0000    # -90.0f

    const/4 v2, 0x0

    const/high16 v5, 0x439b0000    # 310.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;-><init>(FFFFFZZ)V

    .line 2335
    .local v0, "rotation":Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;
    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setDuration(J)V

    .line 2336
    invoke-virtual {v0, v7}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setFillAfter(Z)V

    .line 2337
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/effect/Rotate3dAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2338
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2339
    return-void
.end method

.method public startAnimationTranslate(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 3150
    const-string/jumbo v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 3151
    .local v0, "translateWidgetAnimation":Landroid/animation/ValueAnimator;
    const-wide/16 v1, 0x362

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 3152
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 3153
    return-void

    .line 3150
    :array_0
    .array-data 4
        0x44480000    # 800.0f
        0x0
    .end array-data
.end method
