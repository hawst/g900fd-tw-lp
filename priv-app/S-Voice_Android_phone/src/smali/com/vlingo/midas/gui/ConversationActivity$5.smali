.class Lcom/vlingo/midas/gui/ConversationActivity$5;
.super Ljava/lang/Object;
.source "ConversationActivity.java"

# interfaces
.implements Lcom/vlingo/midas/gui/ControlFragment$ControlFragmentCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ConversationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ConversationActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0

    .prologue
    .line 1805
    iput-object p1, p0, Lcom/vlingo/midas/gui/ConversationActivity$5;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private onBoth(Z)V
    .locals 3
    .param p1, "shouldShow"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1807
    if-eqz p1, :cond_1

    .line 1810
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$5;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;
    invoke-static {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->access$2000(Lcom/vlingo/midas/gui/ConversationActivity;)Lcom/vlingo/midas/gui/DialogFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1811
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$5;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # getter for: Lcom/vlingo/midas/gui/ConversationActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragment;
    invoke-static {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->access$2000(Lcom/vlingo/midas/gui/ConversationActivity;)Lcom/vlingo/midas/gui/DialogFragment;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/vlingo/midas/gui/DialogFragment;->setShadowWhileNotAnim:Z

    .line 1812
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$5;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 1813
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$5;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessage(I)Z

    .line 1815
    :cond_1
    return-void
.end method


# virtual methods
.method public onRecognitionStarted()V
    .locals 1

    .prologue
    .line 1832
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v0, :cond_0

    .line 1833
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/Widget;->onRecognitionStarted()V

    .line 1835
    :cond_0
    return-void
.end method

.method public onStartSpotting()V
    .locals 2

    .prologue
    .line 1819
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isIdle()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v0, v1, :cond_1

    .line 1821
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity$5;->onBoth(Z)V

    .line 1823
    :cond_1
    return-void
.end method

.method public onStopSpotting()V
    .locals 1

    .prologue
    .line 1827
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/ConversationActivity$5;->onBoth(Z)V

    .line 1828
    return-void
.end method
