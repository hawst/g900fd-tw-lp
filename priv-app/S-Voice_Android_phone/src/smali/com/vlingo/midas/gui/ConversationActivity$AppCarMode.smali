.class public final enum Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;
.super Ljava/lang/Enum;
.source "ConversationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ConversationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppCarMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

.field public static final enum Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

.field public static final enum None:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

.field public static final enum Regular:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 269
    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    const-string/jumbo v1, "None"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->None:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    const-string/jumbo v1, "Driving"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    const-string/jumbo v1, "Regular"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Regular:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    .line 268
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->None:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Driving:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->Regular:Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->$VALUES:[Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 268
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 268
    const-class v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;
    .locals 1

    .prologue
    .line 268
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->$VALUES:[Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    invoke-virtual {v0}, [Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/gui/ConversationActivity$AppCarMode;

    return-object v0
.end method
