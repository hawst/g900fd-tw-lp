.class public Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;
.super Lcom/vlingo/midas/gui/Widget;
.source "HelpChoiceWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/Widget",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final ACTION_SEC_HELP:Ljava/lang/String; = "com.samsung.helphub.HELP"

.field public static final EXTRA_LIST_HEIGHT:Ljava/lang/String; = "EXTRA_LIST_HEIGHT"

.field public static final LIST_MOVE_ACTION:Ljava/lang/String; = "LIST_MOVE_ACTION"

.field static view:Landroid/view/View;


# instance fields
.field private final ANIMATE_TRANSLATE_UP_DURATION:I

.field private final WIDGET_LAST_ROW:I

.field private currentTablet:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;

.field private expandedView:Landroid/view/View;

.field private firstHelpView:Z

.field private isKitkatPhoneGUI:Z

.field private lastSelectedView:Landroid/view/View;

.field private lv:Landroid/widget/LinearLayout;

.field private final mContext:Landroid/content/Context;

.field private mLocale:Ljava/util/Locale;

.field mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

.field private miniModeScrollOffSet:I

.field private offsetLt03:I

.field private wcisData:Lcom/vlingo/midas/help/WCISData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/Widget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->WIDGET_LAST_ROW:I

    .line 62
    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    .line 63
    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->lastSelectedView:Landroid/view/View;

    .line 64
    const/16 v0, 0xa0

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->offsetLt03:I

    .line 65
    new-instance v0, Lcom/vlingo/midas/help/WCISData;

    invoke-direct {v0}, Lcom/vlingo/midas/help/WCISData;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->wcisData:Lcom/vlingo/midas/help/WCISData;

    .line 67
    const/16 v0, 0xc8

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->miniModeScrollOffSet:I

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->isKitkatPhoneGUI:Z

    .line 73
    const/16 v0, 0x362

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->ANIMATE_TRANSLATE_UP_DURATION:I

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->firstHelpView:Z

    .line 81
    sget-object v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;->OTHERS:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->currentTablet:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;

    .line 49
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mContext:Landroid/content/Context;

    .line 50
    new-instance v0, Lcom/vlingo/midas/ui/PackageInfoProvider;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 51
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    .prologue
    .line 44
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->offsetLt03:I

    return v0
.end method

.method static synthetic access$202(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->offsetLt03:I

    return p1
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    .prologue
    .line 44
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->miniModeScrollOffSet:I

    return v0
.end method

.method static synthetic access$402(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->miniModeScrollOffSet:I

    return p1
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->isLT03()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->currentTablet:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->isKitkatPhoneGUI:Z

    return v0
.end method

.method private addRects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 3
    .param p1, "a"    # Landroid/graphics/Rect;
    .param p2, "b"    # Landroid/graphics/Rect;

    .prologue
    .line 761
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 762
    .local v0, "r":Landroid/graphics/Rect;
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 763
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 764
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 765
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 766
    return-object v0
.end method

.method private checkIcon(I)V
    .locals 1
    .param p1, "ResId"    # I

    .prologue
    .line 575
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    if-ne p1, v0, :cond_1

    .line 576
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setMemoIcon()V

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 577
    :cond_1
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    if-ne p1, v0, :cond_2

    .line 578
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setMessageIcon()V

    goto :goto_0

    .line 579
    :cond_2
    sget v0, Lcom/vlingo/midas/R$drawable;->help_navi:I

    if-ne p1, v0, :cond_3

    .line 580
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setNavigationIcon()V

    goto :goto_0

    .line 581
    :cond_3
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    if-ne p1, v0, :cond_4

    .line 582
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setVoiceRecorderIcon()V

    goto :goto_0

    .line 583
    :cond_4
    sget v0, Lcom/vlingo/midas/R$drawable;->help_weather:I

    if-ne p1, v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setWeatherIcon()V

    goto :goto_0
.end method

.method private getDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "ResId"    # I

    .prologue
    .line 589
    const/4 v0, 0x0

    .line 591
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    sget v1, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    if-ne p1, v1, :cond_0

    .line 592
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getMemoIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 607
    :goto_0
    return-object v1

    .line 594
    :cond_0
    sget v1, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    if-ne p1, v1, :cond_1

    .line 595
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getMessageIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 596
    goto :goto_0

    .line 597
    :cond_1
    sget v1, Lcom/vlingo/midas/R$drawable;->help_navi:I

    if-ne p1, v1, :cond_2

    .line 598
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 599
    goto :goto_0

    .line 600
    :cond_2
    sget v1, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    if-ne p1, v1, :cond_3

    .line 601
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getVoiceRecorderIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 602
    goto :goto_0

    .line 603
    :cond_3
    sget v1, Lcom/vlingo/midas/R$drawable;->help_weather:I

    if-ne p1, v1, :cond_4

    .line 604
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getWeatherIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 605
    goto :goto_0

    .line 607
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getRelativeRect(Landroid/view/View;Landroid/view/View;)Landroid/graphics/Rect;
    .locals 2
    .param p1, "myView"    # Landroid/view/View;
    .param p2, "getViewRect"    # Landroid/view/View;

    .prologue
    .line 753
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    if-eq v0, v1, :cond_0

    if-ne p1, p2, :cond_1

    .line 754
    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getViewRect(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 756
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getViewRect(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getViewRect(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->addRects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method private getScrollView()Landroid/widget/ScrollView;
    .locals 2

    .prologue
    .line 735
    move-object v0, p0

    .line 737
    .local v0, "view":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    if-eq v0, v1, :cond_0

    instance-of v1, v0, Landroid/widget/ScrollView;

    if-nez v1, :cond_0

    .line 738
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .restart local v0    # "view":Landroid/view/View;
    goto :goto_0

    .line 740
    :cond_0
    instance-of v1, v0, Landroid/widget/ScrollView;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 741
    check-cast v1, Landroid/widget/ScrollView;

    .line 748
    :goto_1
    return-object v1

    .line 743
    :cond_1
    if-eqz v0, :cond_2

    .line 744
    sget v1, Lcom/vlingo/midas/R$id;->help_choice_ScrollView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 745
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 746
    check-cast v1, Landroid/widget/ScrollView;

    goto :goto_1

    .line 748
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getViewRect(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 770
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 771
    .local v0, "r":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 772
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 773
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 774
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 775
    return-object v0
.end method

.method private isLT03()Z
    .locals 3

    .prologue
    const/16 v2, 0xa00

    const/16 v1, 0x640

    .line 781
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v0, v2, :cond_2

    .line 785
    :cond_1
    const/4 v0, 0x1

    .line 787
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getLocaleLanguage()I
    .locals 2

    .prologue
    .line 85
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mLocale:Ljava/util/Locale;

    .line 86
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mLocale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "mCurrentLanguage":Ljava/lang/String;
    const-string/jumbo v1, "en_GB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    const/4 v1, 0x0

    .line 101
    :goto_0
    return v1

    .line 90
    :cond_0
    const-string/jumbo v1, "en_US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    const/4 v1, 0x1

    goto :goto_0

    .line 92
    :cond_1
    const-string/jumbo v1, "ko_KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 93
    const/4 v1, 0x2

    goto :goto_0

    .line 94
    :cond_2
    const-string/jumbo v1, "de_DE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 95
    const/4 v1, 0x3

    goto :goto_0

    .line 96
    :cond_3
    const-string/jumbo v1, "fr_FR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 97
    const/4 v1, 0x4

    goto :goto_0

    .line 98
    :cond_4
    const-string/jumbo v1, "it_IT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 99
    const/4 v1, 0x5

    goto :goto_0

    .line 101
    :cond_5
    const/4 v1, 0x6

    goto :goto_0
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 44
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 1
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 175
    .local p1, "object":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->firstHelpView:Z

    .line 176
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->setAddView()V

    .line 177
    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 619
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 613
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x0

    .line 654
    sget v4, Lcom/vlingo/midas/R$id;->header1_examples:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 656
    .local v1, "header1_examples":Landroid/widget/LinearLayout;
    sget v4, Lcom/vlingo/midas/R$id;->wcis_widget_row_divider1:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 661
    .local v0, "divider1":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v4

    if-nez v4, :cond_7

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 663
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 664
    sget v4, Lcom/vlingo/midas/R$id;->help_expander_icon:I

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 665
    sget v4, Lcom/vlingo/midas/R$id;->help_expander_icon:I

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 666
    .local v3, "imageView":Landroid/widget/ImageView;
    sget v4, Lcom/vlingo/midas/R$drawable;->tw_expander_open_default_holo_light:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 669
    .end local v3    # "imageView":Landroid/widget/ImageView;
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 670
    sget v4, Lcom/vlingo/midas/R$id;->help_expander_icon:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    sget v6, Lcom/vlingo/midas/R$drawable;->tw_expander_close_default_holo_light:I

    invoke-virtual {v4, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 676
    :cond_1
    :goto_1
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v4

    if-ne v4, v7, :cond_9

    .line 677
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 679
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    if-eqz v4, :cond_2

    .line 680
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    sget v6, Lcom/vlingo/midas/R$id;->header1_examples:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 682
    .local v2, "header1_examples_expanded":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 683
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setSelected(Z)V

    .line 685
    .end local v2    # "header1_examples_expanded":Landroid/widget/LinearLayout;
    :cond_2
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    .line 691
    :goto_2
    if-eqz v0, :cond_a

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    if-eqz v4, :cond_a

    .line 692
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 698
    :cond_3
    :goto_3
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->lastSelectedView:Landroid/view/View;

    if-eqz v4, :cond_4

    .line 699
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->lastSelectedView:Landroid/view/View;

    sget v6, Lcom/vlingo/midas/R$id;->wcis_widget_row_divider1:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 702
    :cond_4
    const/16 v4, 0x3e9

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    if-ne v4, v5, :cond_5

    if-eqz v0, :cond_5

    .line 703
    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 705
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 706
    const/16 v4, 0x6e

    iput v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->offsetLt03:I

    .line 708
    :cond_6
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->lastSelectedView:Landroid/view/View;

    .line 715
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$4;

    invoke-direct {v5, p0, p1}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$4;-><init>(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;Landroid/view/View;)V

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 732
    return-void

    :cond_7
    move v4, v5

    .line 661
    goto/16 :goto_0

    .line 672
    :cond_8
    sget v4, Lcom/vlingo/midas/R$id;->help_expander_icon:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    sget v6, Lcom/vlingo/midas/R$drawable;->tw_expander_open_default_holo_light:I

    invoke-virtual {v4, v6}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 687
    :cond_9
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 688
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    goto :goto_2

    .line 694
    :cond_a
    if-eqz v0, :cond_3

    .line 695
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method protected onClickKitkat(Landroid/view/View;Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;)V
    .locals 8
    .param p1, "row"    # Landroid/view/View;
    .param p2, "holder"    # Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x0

    .line 493
    move-object v3, p1

    .line 495
    .local v3, "v":Landroid/view/View;
    sget v4, Lcom/vlingo/midas/R$id;->header1_examples:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 497
    .local v1, "header1_examples":Landroid/widget/LinearLayout;
    sget v4, Lcom/vlingo/midas/R$id;->wcis_widget_row_divider1:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 502
    .local v0, "divider1":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->isSelected()Z

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    .line 504
    invoke-virtual {v3}, Landroid/view/View;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 505
    iget-object v4, p2, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->help_expander_icon:Landroid/widget/ImageView;

    sget v6, Lcom/vlingo/midas/R$drawable;->tw_expander_close_default_holo_light:I

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 510
    :goto_1
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v4

    if-ne v4, v7, :cond_6

    .line 512
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 513
    if-eqz v0, :cond_0

    .line 514
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 516
    :cond_0
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    if-eqz v4, :cond_1

    .line 517
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    sget v6, Lcom/vlingo/midas/R$id;->header1_examples:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 519
    .local v2, "header1_examples_expanded":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 520
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setSelected(Z)V

    .line 522
    .end local v2    # "header1_examples_expanded":Landroid/widget/LinearLayout;
    :cond_1
    iput-object v3, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    .line 528
    :goto_2
    const/16 v4, 0x3e9

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v5

    if-ne v4, v5, :cond_2

    if-eqz v0, :cond_2

    .line 529
    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 531
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 532
    const/16 v4, 0x6e

    iput v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->offsetLt03:I

    .line 539
    :cond_3
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$3;

    invoke-direct {v5, p0, v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$3;-><init>(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;Landroid/view/View;)V

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 558
    return-void

    :cond_4
    move v4, v5

    .line 502
    goto :goto_0

    .line 507
    :cond_5
    iget-object v4, p2, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->help_expander_icon:Landroid/widget/ImageView;

    sget v6, Lcom/vlingo/midas/R$drawable;->tw_expander_open_default_holo_light:I

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 524
    :cond_6
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 525
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;

    goto :goto_2
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 107
    invoke-super {p0}, Lcom/vlingo/midas/gui/Widget;->onFinishInflate()V

    .line 109
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->isKitkatPhoneGUI:Z

    .line 118
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_1

    .line 119
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->currentTablet:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;

    .line 122
    :cond_1
    sget v0, Lcom/vlingo/midas/R$id;->wycs_top_list:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->lv:Landroid/widget/LinearLayout;

    .line 123
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->wcisData:Lcom/vlingo/midas/help/WCISData;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/help/WCISData;->addItems(Landroid/content/Context;)V

    .line 159
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->checkIcon(I)V

    .line 160
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_message:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->checkIcon(I)V

    .line 161
    sget v0, Lcom/vlingo/midas/R$drawable;->help_navi:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->checkIcon(I)V

    .line 162
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->checkIcon(I)V

    .line 163
    sget v0, Lcom/vlingo/midas/R$drawable;->help_weather:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->checkIcon(I)V

    .line 165
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->setAddView()V

    .line 166
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isATTDevice()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->lv:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 169
    :cond_2
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->firstHelpView:Z

    .line 170
    return-void
.end method

.method public setAddView()V
    .locals 21

    .prologue
    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->wcisData:Lcom/vlingo/midas/help/WCISData;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/midas/help/WCISData;->getItems()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 181
    .local v8, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;>;"
    const/4 v6, 0x1

    .line 182
    .local v6, "first":Z
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_a

    .line 183
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/HashMap;

    .line 185
    .local v9, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    sget v19, Lcom/vlingo/midas/R$layout;->wcis_widget_row:I

    const/16 v20, 0x0

    invoke-static/range {v18 .. v20}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v16

    .line 187
    .local v16, "row":Landroid/view/View;
    new-instance v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;

    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-direct {v7, v0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;)V

    .line 188
    .local v7, "holder":Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;
    sget v18, Lcom/vlingo/midas/R$id;->help_choice_image:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageView;

    move-object/from16 v0, v18

    iput-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->image:Landroid/widget/ImageView;

    .line 189
    sget v18, Lcom/vlingo/midas/R$id;->help_choice_text1:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    iput-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->text1:Landroid/widget/TextView;

    .line 190
    sget v18, Lcom/vlingo/midas/R$id;->help_choice_text2:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    iput-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->text2:Landroid/widget/TextView;

    .line 191
    sget v18, Lcom/vlingo/midas/R$id;->text1_examples:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    iput-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->text1_examples:Landroid/widget/TextView;

    .line 193
    sget v18, Lcom/vlingo/midas/R$id;->header1_examples:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    move-object/from16 v0, v18

    iput-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->header1_examples:Landroid/widget/LinearLayout;

    .line 195
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 196
    sget v18, Lcom/vlingo/midas/R$id;->help_expander_icon:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageView;

    move-object/from16 v0, v18

    iput-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->help_expander_icon:Landroid/widget/ImageView;

    .line 204
    new-instance v18, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;Landroid/view/View;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 356
    :cond_0
    :goto_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 358
    const-string/jumbo v18, "EXTRA_LIST_ICON"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 360
    .local v5, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v5, :cond_6

    .line 361
    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->image:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 367
    :goto_2
    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->text1:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const-string/jumbo v18, "EXTRA_LIST_TITLE"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->text2:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const-string/jumbo v18, "EXTRA_LIST_EXAMPLE"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    const-string/jumbo v18, "EXTRA_EXAMPLE_LIST"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->text1_examples:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->setDetailText(ILandroid/widget/TextView;)V

    .line 375
    const-string/jumbo v18, "EXTRA_EXAMPLE_LIST"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    sget v19, Lcom/vlingo/midas/R$array;->wcis_music_examples:I

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasRadio()Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 377
    sget v18, Lcom/vlingo/midas/R$array;->wcis_music_examples_no_radio:I

    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->text1_examples:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->setDetailText(ILandroid/widget/TextView;)V

    .line 380
    :cond_1
    if-eqz v8, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_2

    .line 381
    sget v18, Lcom/vlingo/midas/R$id;->wcis_widget_row_divider1:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 382
    .local v4, "div":Landroid/view/View;
    if-eqz v4, :cond_2

    .line 383
    const/16 v18, 0x4

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 387
    .end local v4    # "div":Landroid/view/View;
    :cond_2
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getPaddingLeft()I

    move-result v10

    .line 388
    .local v10, "l":I
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getPaddingTop()I

    move-result v17

    .line 389
    .local v17, "t":I
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getPaddingRight()I

    move-result v15

    .line 390
    .local v15, "r":I
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    .line 392
    .local v3, "b":I
    if-eqz v6, :cond_7

    .line 393
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->isKitkatPhoneGUI:Z

    move/from16 v18, v0

    if-nez v18, :cond_3

    .line 394
    sget v18, Lcom/vlingo/midas/R$drawable;->wcis_row_top_background:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 396
    :cond_3
    const/4 v6, 0x0

    .line 416
    :cond_4
    :goto_3
    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v10, v1, v15, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 418
    new-instance v18, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;-><init>(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;Landroid/view/View;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 486
    const/16 v18, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 487
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->lv:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 352
    .end local v3    # "b":I
    .end local v5    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v10    # "l":I
    .end local v15    # "r":I
    .end local v17    # "t":I
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 353
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 363
    .restart local v5    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_6
    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->image:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const-string/jumbo v18, "EXTRA_LIST_ICON"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 397
    .restart local v3    # "b":I
    .restart local v10    # "l":I
    .restart local v15    # "r":I
    .restart local v17    # "t":I
    :cond_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_9

    .line 400
    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->header1_examples:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v12

    .line 401
    .local v12, "ll":I
    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->header1_examples:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v14

    .line 402
    .local v14, "lt":I
    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->header1_examples:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v13

    .line 403
    .local v13, "lr":I
    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->header1_examples:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v11

    .line 406
    .local v11, "lb":I
    iget-object v0, v7, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;->header1_examples:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v14, v13, v11}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 407
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->isKitkatPhoneGUI:Z

    move/from16 v18, v0

    if-nez v18, :cond_8

    .line 408
    sget v18, Lcom/vlingo/midas/R$drawable;->wcis_row_bottom_background:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 410
    :cond_8
    const/16 v18, 0x3e9

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    goto/16 :goto_3

    .line 412
    .end local v11    # "lb":I
    .end local v12    # "ll":I
    .end local v13    # "lr":I
    .end local v14    # "lt":I
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->isKitkatPhoneGUI:Z

    move/from16 v18, v0

    if-nez v18, :cond_4

    .line 413
    sget v18, Lcom/vlingo/midas/R$drawable;->wcis_row_middle_background:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 489
    .end local v3    # "b":I
    .end local v5    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v7    # "holder":Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$ViewHolder;
    .end local v9    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v10    # "l":I
    .end local v15    # "r":I
    .end local v16    # "row":Landroid/view/View;
    .end local v17    # "t":I
    :cond_a
    return-void
.end method

.method public setDetailText(ILandroid/widget/TextView;)V
    .locals 6
    .param p1, "textResId"    # I
    .param p2, "textViewResId"    # Landroid/widget/TextView;

    .prologue
    .line 561
    if-eqz p2, :cond_1

    .line 562
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 564
    .local v1, "dyk":Ljava/lang/StringBuffer;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 565
    .local v4, "s":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 564
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 567
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "s":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 570
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 572
    .end local v1    # "dyk":Ljava/lang/StringBuffer;
    :cond_1
    return-void
.end method

.method public startAnimationTranslate(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 624
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->firstHelpView:Z

    if-eqz v1, :cond_0

    .line 625
    const-string/jumbo v1, "translationY"

    new-array v2, v2, [F

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    aput v3, v2, v5

    aput v4, v2, v6

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 627
    .local v0, "translateWidgetAnimation":Landroid/animation/ValueAnimator;
    const-wide/16 v1, 0x362

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 628
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 635
    :goto_0
    return-void

    .line 630
    .end local v0    # "translateWidgetAnimation":Landroid/animation/ValueAnimator;
    :cond_0
    const-string/jumbo v1, "translationY"

    new-array v2, v2, [F

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    aput v3, v2, v5

    aput v4, v2, v6

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 632
    .restart local v0    # "translateWidgetAnimation":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 633
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0
.end method
