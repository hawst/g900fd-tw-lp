.class public Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "WeatherOneDayWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget$WeatherOneDayWidgetHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/weather/WeatherAdaptor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final sdf:Ljava/text/SimpleDateFormat;

.field private static final sdf2:Ljava/text/SimpleDateFormat;


# instance fields
.field private accuWeather_Web:Landroid/widget/ImageView;

.field private citi_name:Landroid/widget/TextView;

.field private climate_name:Landroid/widget/TextView;

.field private contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

.field private currentCondition:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private date_val:Ljava/lang/String;

.field private date_value:Landroid/widget/TextView;

.field private deviderImageView:Landroid/widget/TextView;

.field private forecasts:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private location:Ljava/lang/String;

.field private log:Lcom/vlingo/core/internal/logging/Logger;

.field private mCityDetail:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mCurrentTemp:Landroid/widget/TextView;

.field private mCurrentTemp_degree:Landroid/widget/TextView;

.field private mHandler:Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget$WeatherOneDayWidgetHandler;

.field private mTemparature:Landroid/widget/LinearLayout;

.field private mUnit_meter_C:Landroid/view/View;

.field private mUnit_meter_F:Landroid/view/View;

.field private mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

.field private max:Ljava/lang/String;

.field private maxTemperature1:Landroid/widget/TextView;

.field private min:Ljava/lang/String;

.field private minTemperature1:Landroid/widget/TextView;

.field private normalBackground:I

.field private sunRise:Ljava/lang/String;

.field private sunSet:Ljava/lang/String;

.field private tabletPortBackground:I

.field private temp_unit:Ljava/lang/String;

.field private wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

.field private weatherBackgroundImage:Landroid/widget/FrameLayout;

.field private weatherBox:Landroid/widget/ImageView;

.field private weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

.field private weatherImage1:Landroid/widget/ImageView;

.field private weathernews:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEE, d MMM"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->sdf:Ljava/text/SimpleDateFormat;

    .line 51
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->sdf2:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const-class v0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->log:Lcom/vlingo/core/internal/logging/Logger;

    .line 96
    iput v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->normalBackground:I

    .line 97
    iput v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->tabletPortBackground:I

    .line 242
    new-instance v0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget$WeatherOneDayWidgetHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget$WeatherOneDayWidgetHandler;-><init>(Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget$WeatherOneDayWidgetHandler;

    .line 101
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mContext:Landroid/content/Context;

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->setValuesForcastWeather()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->setTalkbackString()V

    return-void
.end method

.method private getTempUnit()Z
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->temp_unit:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->unit_temperature_farenheit:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->temp_unit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getWeatherDate()Ljava/lang/String;
    .locals 8

    .prologue
    .line 192
    const/4 v0, 0x0

    .line 193
    .local v0, "dateFormat":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    .line 195
    .local v1, "language":Ljava/lang/String;
    const-string/jumbo v5, "ko-KR"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "ja-JP"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 196
    :cond_0
    const-string/jumbo v5, "ko-KR"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 197
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "M"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->mon1:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " d"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " E"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->cradle_home_date:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 208
    :goto_0
    if-eqz v0, :cond_3

    .line 209
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v2, v0, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 211
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "yyyy-MM-dd"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 215
    .local v3, "sdf2":Ljava/text/SimpleDateFormat;
    :try_start_0
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getWeatherDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 216
    .local v4, "wDate":Ljava/util/Date;
    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 221
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v3    # "sdf2":Ljava/text/SimpleDateFormat;
    .end local v4    # "wDate":Ljava/util/Date;
    :goto_1
    return-object v5

    .line 201
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "M"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->mon1:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " d"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " E"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->cradle_home_date:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 206
    :cond_2
    const-string/jumbo v0, "EE, d MMMM"

    goto :goto_0

    .line 217
    .restart local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v3    # "sdf2":Ljava/text/SimpleDateFormat;
    :catch_0
    move-exception v5

    .line 221
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v3    # "sdf2":Ljava/text/SimpleDateFormat;
    :cond_3
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getDateForOneDayWidget()Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method private isLT03()Z
    .locals 3

    .prologue
    const/16 v2, 0xa00

    const/16 v1, 0x640

    .line 506
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v0, v2, :cond_2

    .line 510
    :cond_1
    const/4 v0, 0x1

    .line 512
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onResponseReceived()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 150
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    .line 151
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getProvider()Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    .line 153
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherElement;->getChildCount()I

    move-result v5

    if-nez v5, :cond_1

    .line 155
    :cond_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 156
    .local v3, "intent":Landroid/content/Intent;
    const-string/jumbo v5, "com.vlingo.core.internal.dialogmanager.NoData"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v6, 0x0

    invoke-interface {v5, v3, v6}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 189
    .end local v3    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 162
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationState()Ljava/lang/String;

    move-result-object v4

    .line 163
    .local v4, "state":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "city":Ljava/lang/String;
    if-nez v4, :cond_2

    const-string/jumbo v4, ""

    .line 165
    :cond_2
    if-nez v1, :cond_3

    const-string/jumbo v1, ""

    .line 166
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$string;->weather_location:I

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v8, 0x1

    aput-object v4, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->location:Ljava/lang/String;

    .line 167
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    const-string/jumbo v6, "Units"

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v5

    const-string/jumbo v6, "Units"

    invoke-virtual {v5, v6}, Lcom/vlingo/core/internal/weather/WeatherElement;->findNamedChild(Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherElement;

    move-result-object v0

    .line 169
    .local v0, "Unit":Lcom/vlingo/core/internal/weather/WeatherElement;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/weather/WeatherElement;->getAttributes()Ljava/util/Map;

    move-result-object v5

    const-string/jumbo v6, "Temperature"

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->temp_unit:Ljava/lang/String;
    :try_end_0
    .catch Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    .end local v0    # "Unit":Lcom/vlingo/core/internal/weather/WeatherElement;
    .end local v1    # "city":Ljava/lang/String;
    .end local v4    # "state":Ljava/lang/String;
    :goto_1
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherDetails:Lcom/vlingo/core/internal/weather/WeatherElement;

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getDate()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getWeatherInfoUtil(Landroid/content/Context;Lcom/vlingo/core/internal/weather/WeatherElement;Ljava/lang/String;)Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    .line 178
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getWeatherDate()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->date_val:Ljava/lang/String;

    .line 179
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getMaximumTempOneDayWeather()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->max:Ljava/lang/String;

    .line 180
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getMinimumTempOneDayWeather()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->min:Ljava/lang/String;

    .line 181
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getSunRise()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->sunRise:Ljava/lang/String;

    .line 182
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v5}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getSunSet()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->sunSet:Ljava/lang/String;

    .line 186
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mContext:Landroid/content/Context;

    const-string/jumbo v6, "com.vlingo.midas"

    const-string/jumbo v7, "WEAT"

    invoke-static {v5, v6, v7}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget$WeatherOneDayWidgetHandler;

    invoke-virtual {v5, v9}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget$WeatherOneDayWidgetHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 170
    :catch_0
    move-exception v2

    .line 172
    .local v2, "e1":Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;
    invoke-virtual {v2}, Lcom/vlingo/core/internal/weather/WeatherElement$WeatherNotFound;->printStackTrace()V

    goto :goto_1
.end method

.method private openAccuWeatherWeb()V
    .locals 2

    .prologue
    .line 411
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 412
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "http://www.accuweather.com/m"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 413
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 414
    return-void
.end method

.method private setTalkbackString()V
    .locals 5

    .prologue
    .line 370
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getTempUnit()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 371
    const-string/jumbo v0, "F"

    .line 376
    .local v0, "tempUnit":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 377
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xb0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 380
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 382
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 384
    return-void

    .line 373
    .end local v0    # "tempUnit":Ljava/lang/String;
    :cond_1
    const-string/jumbo v0, "C"

    .restart local v0    # "tempUnit":Ljava/lang/String;
    goto :goto_0
.end method

.method private setValuesForcastWeather()V
    .locals 15

    .prologue
    const/16 v14, 0xb0

    const/16 v13, 0x65

    const/4 v12, 0x0

    const/16 v11, 0x8

    .line 245
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weathernews:Landroid/widget/ImageView;

    if-eqz v8, :cond_0

    .line 246
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->contentProvider:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    sget-object v9, Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;->WEATHERNEWS:Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    if-ne v8, v9, :cond_9

    .line 248
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 249
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weathernews:Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 256
    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->min:Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 257
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->min:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    double-to-int v1, v8

    .line 258
    .local v1, "minTemp":I
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 259
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    .end local v1    # "minTemp":I
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->max:Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 266
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->max:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    double-to-int v0, v8

    .line 267
    .local v0, "maxTemp":I
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 268
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    .end local v0    # "maxTemp":I
    :cond_2
    :goto_2
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->deviderImageView:Landroid/widget/TextView;

    if-eqz v8, :cond_4

    .line 276
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->min:Ljava/lang/String;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->max:Ljava/lang/String;

    if-nez v8, :cond_c

    .line 277
    :cond_3
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->deviderImageView:Landroid/widget/TextView;

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    :cond_4
    :goto_3
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->citi_name:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->location:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    new-instance v7, Lcom/vlingo/midas/util/WeatherResourceUtil;

    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getCurrentWeatherCode()I

    move-result v8

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->getProvider()Lcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/vlingo/midas/util/WeatherResourceUtil;-><init>(ILcom/vlingo/core/internal/weather/WeatherAdaptor$WeatherContentProvider;)V

    .line 289
    .local v7, "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    invoke-virtual {v7}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherDrawable()I

    move-result v6

    .line 290
    .local v6, "weatherDrawableCode":I
    if-eq v6, v13, :cond_5

    .line 291
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherImage1:Landroid/widget/ImageView;

    invoke-virtual {v8, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 294
    :cond_5
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v8, "yyyyMMdd h:mm a"

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 296
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    const/4 v3, 0x0

    .line 297
    .local v3, "sunRiseTime":Ljava/util/Date;
    const/4 v4, 0x0

    .line 300
    .local v4, "sunSetTime":Ljava/util/Date;
    :try_start_0
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->sunRise:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 301
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->sunSet:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 305
    :goto_4
    invoke-virtual {v7}, Lcom/vlingo/midas/util/WeatherResourceUtil;->getWeatherBackgroundDrawable()I

    move-result v5

    .line 306
    .local v5, "weatherBackgroundDrawableCode":I
    if-eq v5, v13, :cond_6

    .line 313
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isNightMode()Z

    move-result v8

    if-eqz v8, :cond_d

    .line 314
    invoke-virtual {v7, v5}, Lcom/vlingo/midas/util/WeatherResourceUtil;->changeToNightBackground(I)I

    move-result v8

    iput v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->normalBackground:I

    .line 317
    :goto_5
    iget v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->normalBackground:I

    invoke-virtual {v7, v8}, Lcom/vlingo/midas/util/WeatherResourceUtil;->changeToVerticalBackground(I)I

    move-result v8

    iput v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->tabletPortBackground:I

    .line 318
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_e

    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->isLT03()Z

    move-result v8

    if-eqz v8, :cond_e

    .line 319
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherBackgroundImage:Landroid/widget/FrameLayout;

    iget v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->tabletPortBackground:I

    invoke-virtual {v8, v9}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 326
    :cond_6
    :goto_6
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v9}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getWeatherString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->date_value:Landroid/widget/TextView;

    if-eqz v8, :cond_7

    .line 328
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->date_value:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->date_val:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v8

    if-eqz v8, :cond_10

    .line 332
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v10}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getCurrentTemp()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getTempUnit()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 334
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_C:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    .line 365
    :cond_8
    :goto_7
    return-void

    .line 251
    .end local v2    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    .end local v3    # "sunRiseTime":Ljava/util/Date;
    .end local v4    # "sunSetTime":Ljava/util/Date;
    .end local v5    # "weatherBackgroundDrawableCode":I
    .end local v6    # "weatherDrawableCode":I
    .end local v7    # "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    :cond_9
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 252
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weathernews:Landroid/widget/ImageView;

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 261
    .restart local v1    # "minTemp":I
    :cond_a
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 270
    .end local v1    # "minTemp":I
    .restart local v0    # "maxTemp":I
    :cond_b
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 279
    .end local v0    # "maxTemp":I
    :cond_c
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v8

    if-nez v8, :cond_4

    .line 280
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->deviderImageView:Landroid/widget/TextView;

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 316
    .restart local v2    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    .restart local v3    # "sunRiseTime":Ljava/util/Date;
    .restart local v4    # "sunSetTime":Ljava/util/Date;
    .restart local v5    # "weatherBackgroundDrawableCode":I
    .restart local v6    # "weatherDrawableCode":I
    .restart local v7    # "wru":Lcom/vlingo/midas/util/WeatherResourceUtil;
    :cond_d
    iput v5, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->normalBackground:I

    goto/16 :goto_5

    .line 321
    :cond_e
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v8

    if-nez v8, :cond_6

    .line 322
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherBackgroundImage:Landroid/widget/FrameLayout;

    iget v9, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->normalBackground:I

    invoke-virtual {v8, v9}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    goto/16 :goto_6

    .line 336
    :cond_f
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_F:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7

    .line 338
    :cond_10
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    invoke-virtual {v8}, Lcom/vlingo/core/internal/weather/WeatherAdaptor;->isToday()Z

    move-result v8

    if-eqz v8, :cond_12

    .line 339
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mWeatherInfoUtil:Lcom/vlingo/core/internal/weather/WeatherInfoUtil;

    invoke-virtual {v10}, Lcom/vlingo/core/internal/weather/WeatherInfoUtil;->getCurrentTemp()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getTempUnit()Z

    move-result v8

    if-eqz v8, :cond_11

    .line 342
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_C:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    .line 344
    :cond_11
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_F:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    .line 347
    :cond_12
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    if-eqz v8, :cond_13

    .line 348
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 349
    :cond_13
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCurrentTemp_degree:Landroid/widget/TextView;

    if-eqz v8, :cond_14

    .line 350
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCurrentTemp_degree:Landroid/widget/TextView;

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 351
    :cond_14
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_C:Landroid/view/View;

    if-eqz v8, :cond_15

    .line 352
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_C:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    .line 354
    :cond_15
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_F:Landroid/view/View;

    if-eqz v8, :cond_8

    .line 355
    iget-object v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_F:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    .line 302
    .end local v5    # "weatherBackgroundDrawableCode":I
    :catch_0
    move-exception v8

    goto/16 :goto_4
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/weather/WeatherAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "adaptor"    # Lcom/vlingo/core/internal/weather/WeatherAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 143
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 144
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    .line 145
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->onResponseReceived()V

    .line 147
    return-void
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 46
    check-cast p1, Lcom/vlingo/core/internal/weather/WeatherAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->initialize(Lcom/vlingo/core/internal/weather/WeatherAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 399
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 404
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 405
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->accu_weather:I

    if-ne v0, v1, :cond_0

    .line 406
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->openAccuWeatherWeb()V

    .line 408
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 10
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v9, 0x0

    .line 457
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 459
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->isLT03()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 461
    iget v7, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_4

    .line 462
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherBackgroundImage:Landroid/widget/FrameLayout;

    iget v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->tabletPortBackground:I

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 465
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_left_right_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 468
    .local v2, "citiRightMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCityDetail:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_0

    .line 469
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_citi_top_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 471
    .local v3, "citiTopMargin":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_citi_max_width:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 473
    .local v1, "citiMaxWidth":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCityDetail:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9, v3, v2, v9}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 474
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCityDetail:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v1, v7, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 477
    .end local v1    # "citiMaxWidth":I
    .end local v3    # "citiTopMargin":I
    :cond_0
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    if-eqz v7, :cond_1

    .line 478
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_accu_bottom_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 480
    .local v0, "accuBottomMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v0, v7, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 481
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 484
    .end local v0    # "accuBottomMargin":I
    :cond_1
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_2

    .line 485
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_temprature_bottom_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 487
    .local v6, "tempBottomMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v6, v7, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 488
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 491
    .end local v6    # "tempBottomMargin":I
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_climate_bottom_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 493
    .local v4, "climateBottomMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v4, v7, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 494
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 497
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_icon_top_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 499
    .local v5, "iconTopMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherImage1:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v5, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 500
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherImage1:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 502
    .end local v2    # "citiRightMargin":I
    .end local v4    # "climateBottomMargin":I
    .end local v5    # "iconTopMargin":I
    :cond_3
    return-void

    .line 464
    :cond_4
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherBackgroundImage:Landroid/widget/FrameLayout;

    iget v8, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->normalBackground:I

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 107
    :try_start_0
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 108
    sget v1, Lcom/vlingo/midas/R$id;->citi_name:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->citi_name:Landroid/widget/TextView;

    .line 109
    sget v1, Lcom/vlingo/midas/R$id;->imageView_singledayweather:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherImage1:Landroid/widget/ImageView;

    .line 110
    sget v1, Lcom/vlingo/midas/R$id;->climate_name:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    .line 111
    sget v1, Lcom/vlingo/midas/R$id;->present_temp:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    .line 112
    sget v1, Lcom/vlingo/midas/R$id;->widget_citi:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherBackgroundImage:Landroid/widget/FrameLayout;

    .line 113
    sget v1, Lcom/vlingo/midas/R$id;->date_view:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->date_value:Landroid/widget/TextView;

    .line 114
    sget v1, Lcom/vlingo/midas/R$id;->unit_meter_F:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_F:Landroid/view/View;

    .line 115
    sget v1, Lcom/vlingo/midas/R$id;->unit_meter_C:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_C:Landroid/view/View;

    .line 116
    sget v1, Lcom/vlingo/midas/R$id;->devider_small:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->deviderImageView:Landroid/widget/TextView;

    .line 117
    sget v1, Lcom/vlingo/midas/R$id;->min_temp:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    .line 118
    sget v1, Lcom/vlingo/midas/R$id;->max_temp:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    .line 119
    sget v1, Lcom/vlingo/midas/R$id;->accu_weather:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    .line 120
    sget v1, Lcom/vlingo/midas/R$id;->citi_details:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCityDetail:Landroid/widget/LinearLayout;

    .line 121
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    sget v1, Lcom/vlingo/midas/R$id;->present_temp_degree:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCurrentTemp_degree:Landroid/widget/TextView;

    .line 124
    sget v1, Lcom/vlingo/midas/R$id;->temprature:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    .line 125
    sget v1, Lcom/vlingo/midas/R$id;->weathernews:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weathernews:Landroid/widget/ImageView;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 137
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 138
    return-void
.end method

.method public setMinimizeWindow()V
    .locals 12

    .prologue
    const/16 v11, 0x33

    const/high16 v10, 0x41c80000    # 25.0f

    const/high16 v9, 0x41c00000    # 24.0f

    const/4 v8, 0x0

    const/4 v7, -0x2

    .line 417
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherBackgroundImage:Landroid/widget/FrameLayout;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/16 v5, 0x30c

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 419
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->weatherImage1:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v4, 0x2af

    const/16 v5, 0x285

    const/4 v6, 0x5

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 420
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->date_value:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 421
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->date_value:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setTextSize(F)V

    .line 423
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->citi_name:Landroid/widget/TextView;

    const/high16 v3, 0x420c0000    # 35.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 425
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 426
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x50

    invoke-direct {v0, v7, v7, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 428
    .local v0, "cn":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v2, 0x30

    invoke-virtual {v0, v11, v8, v8, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 429
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 431
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    const/high16 v3, 0x42200000    # 40.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 432
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_F:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 435
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mUnit_meter_C:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 438
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 439
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextSize(F)V

    .line 441
    :cond_3
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    .line 442
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextSize(F)V

    .line 444
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->deviderImageView:Landroid/widget/TextView;

    if-eqz v2, :cond_5

    .line 445
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->deviderImageView:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setTextSize(F)V

    .line 447
    :cond_5
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_6

    .line 448
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x50

    invoke-direct {v1, v7, v7, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 450
    .local v1, "tp":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v2, 0x63

    invoke-virtual {v1, v11, v8, v8, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 451
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 453
    .end local v1    # "tp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_6
    return-void
.end method
