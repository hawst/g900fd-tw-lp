.class Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$7;
.super Ljava/lang/Object;
.source "EditWhatYouSaidHelpActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->showTOS()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V
    .locals 0

    .prologue
    .line 1257
    iput-object p1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$7;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1268
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->setLocationEnabled(Z)V

    .line 1269
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/ServiceManager;->startLocalServices()Z

    .line 1271
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->requiresIUX()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1272
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$7;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-static {v0}, Lcom/vlingo/midas/iux/IUXManagerHelp;->processIUX(Landroid/app/Activity;)V

    .line 1277
    :goto_0
    return-void

    .line 1275
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$7;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    # invokes: Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startSettingsActivity()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->access$600(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    goto :goto_0
.end method
