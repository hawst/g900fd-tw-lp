.class Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;
.super Ljava/lang/Thread;
.source "DialogFragmentEWYSHelp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimThread"
.end annotation


# static fields
.field private static final ANIM_FRAME_DELAY:I = 0x5

.field private static final ANIM_FRAME_MAX:I = 0x14

.field private static final ANIM_FRAME_MIN:I = 0x1

.field private static final ANIM_FRAME_MIN_START_POINT:I = 0x14


# instance fields
.field private final dialogFragment:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;",
            ">;"
        }
    .end annotation
.end field

.field private isWolframFlag:Z

.field private mFromHeight:I

.field private final mToHeight:I

.field private padding:I


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)V
    .locals 1
    .param p1, "dialogFragment"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .prologue
    const/4 v0, 0x0

    .line 1093
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1090
    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->padding:I

    .line 1091
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->isWolframFlag:Z

    .line 1094
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    .line 1096
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$300(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mToHeight:I

    .line 1097
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$200(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mFromHeight:I

    .line 1098
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;I)V
    .locals 1
    .param p1, "dialogFragment"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .param p2, "BottomPadding"    # I

    .prologue
    const/4 v0, 0x0

    .line 1100
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1090
    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->padding:I

    .line 1091
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->isWolframFlag:Z

    .line 1101
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    .line 1102
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$300(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    sub-int/2addr v0, p2

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mToHeight:I

    .line 1104
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$200(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mFromHeight:I

    .line 1105
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;IZ)V
    .locals 1
    .param p1, "dialogFragment"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .param p2, "padding"    # I
    .param p3, "isWolframFlag"    # Z

    .prologue
    const/4 v0, 0x0

    .line 1107
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1090
    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->padding:I

    .line 1091
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->isWolframFlag:Z

    .line 1108
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    .line 1109
    iput p2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->padding:I

    .line 1110
    iput-boolean p3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->isWolframFlag:Z

    .line 1111
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$300(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mToHeight:I

    .line 1112
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$200(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mFromHeight:I

    .line 1113
    return-void
.end method

.method private continueLoop()Z
    .locals 2

    .prologue
    .line 1116
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .line 1117
    .local v0, "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    if-nez v0, :cond_0

    .line 1118
    const/4 v1, 0x0

    .line 1120
    :goto_0
    return v1

    :cond_0
    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$400(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Z

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1125
    const/16 v2, 0x14

    .line 1126
    .local v2, "value":I
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->continueLoop()Z

    move-result v0

    .line 1127
    .local v0, "continueLoop":Z
    :cond_0
    :goto_0
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mToHeight:I

    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mFromHeight:I

    if-le v3, v4, :cond_3

    if-eqz v0, :cond_3

    .line 1128
    iget-object v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .line 1129
    .local v1, "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    if-nez v1, :cond_1

    .line 1152
    .end local v1    # "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    :goto_1
    return-void

    .line 1133
    .restart local v1    # "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    :cond_1
    iget-boolean v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->isWolframFlag:Z

    if-eqz v3, :cond_2

    .line 1134
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mFromHeight:I

    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->padding:I

    sub-int/2addr v3, v4

    # invokes: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doScrollAnim(I)V
    invoke-static {v1, v3}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$500(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;I)V

    .line 1138
    :goto_2
    const/4 v1, 0x0

    .line 1140
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mFromHeight:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mFromHeight:I

    .line 1142
    const-wide/16 v3, 0x5

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1147
    :goto_3
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mToHeight:I

    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mFromHeight:I

    sub-int/2addr v3, v4

    const/16 v4, 0x14

    if-ge v3, v4, :cond_0

    .line 1148
    const/4 v2, 0x1

    goto :goto_0

    .line 1136
    :cond_2
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->mFromHeight:I

    # invokes: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->doScrollAnim(I)V
    invoke-static {v1, v3}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$500(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;I)V

    goto :goto_2

    .line 1151
    .end local v1    # "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    :cond_3
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimThread;->isWolframFlag:Z

    goto :goto_1

    .line 1143
    .restart local v1    # "df":Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    :catch_0
    move-exception v3

    goto :goto_3
.end method
