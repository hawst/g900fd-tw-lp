.class public Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;
.super Ljava/lang/Object;
.source "VoicePromptUserConfirmation.java"

# interfaces
.implements Lcom/vlingo/core/internal/settings/VoicePrompt$ConfirmationDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;
    }
.end annotation


# static fields
.field private static final KEY_VOICE_PROMPT_PREFERENCE:Ljava/lang/String; = "voice_prompt_preference_value"

.field private static alertDialogIsShowing:Z


# instance fields
.field private final MENU_VOICE_PROMPT_OFF:I

.field private final MENU_VOICE_PROMPT_ON:I

.field private NO_ICON:I

.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->alertDialogIsShowing:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->NO_ICON:I

    .line 42
    const/16 v0, 0x14

    iput v0, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->MENU_VOICE_PROMPT_OFF:I

    .line 43
    const/16 v0, 0x15

    iput v0, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->MENU_VOICE_PROMPT_ON:I

    .line 51
    iput-object p1, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->context:Landroid/content/Context;

    .line 52
    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 35
    sput-boolean p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->alertDialogIsShowing:Z

    return p0
.end method

.method private context()Landroid/content/Context;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->context:Landroid/content/Context;

    return-object v0
.end method

.method private getConfirmationDialog(Lcom/vlingo/core/internal/settings/VoicePrompt;Ljava/lang/Runnable;)V
    .locals 15
    .param p1, "_voicePrompt"    # Lcom/vlingo/core/internal/settings/VoicePrompt;
    .param p2, "_onOK"    # Ljava/lang/Runnable;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->context()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 77
    .local v5, "r":Landroid/content/res/Resources;
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v12

    if-eqz v12, :cond_1

    sget v12, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_disable:I

    :goto_0
    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 78
    .local v7, "title_dialog":Ljava/lang/String;
    sget v12, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_ok:I

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 79
    .local v8, "title_ok":Ljava/lang/String;
    sget v12, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_cancel:I

    invoke-virtual {v5, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 81
    .local v6, "title_cancel":Ljava/lang/String;
    new-instance v9, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;

    invoke-direct {p0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->context()Landroid/content/Context;

    move-result-object v13

    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v12

    if-eqz v12, :cond_2

    sget-object v12, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;->OFF:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    :goto_1
    invoke-direct {v9, v13, v12}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;-><init>(Landroid/content/Context;Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;)V

    .line 83
    .local v9, "view":Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;
    new-instance v4, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$1;

    move-object/from16 v0, p2

    invoke-direct {v4, p0, v9, v0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$1;-><init>(Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;Ljava/lang/Runnable;)V

    .line 100
    .local v4, "onOK":Landroid/content/DialogInterface$OnClickListener;
    new-instance v2, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$2;

    invoke-direct {v2, p0, v9}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$2;-><init>(Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;)V

    .line 110
    .local v2, "onDecline":Landroid/content/DialogInterface$OnClickListener;
    new-instance v1, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$3;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$3;-><init>(Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;)V

    .line 117
    .local v1, "onCancel":Landroid/content/DialogInterface$OnCancelListener;
    new-instance v3, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$4;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$4;-><init>(Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;)V

    .line 129
    .local v3, "onKey":Landroid/content/DialogInterface$OnKeyListener;
    const/4 v11, 0x0

    .line 131
    .local v11, "voicePromptUserConfirmationBuilder":Landroid/app/AlertDialog$Builder;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 132
    new-instance v11, Landroid/app/AlertDialog$Builder;

    .end local v11    # "voicePromptUserConfirmationBuilder":Landroid/app/AlertDialog$Builder;
    new-instance v12, Landroid/view/ContextThemeWrapper;

    invoke-direct {p0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->context()Landroid/content/Context;

    move-result-object v13

    const v14, 0x103012b

    invoke-direct {v12, v13, v14}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v11, v12}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 136
    .restart local v11    # "voicePromptUserConfirmationBuilder":Landroid/app/AlertDialog$Builder;
    :goto_2
    iget v12, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->NO_ICON:I

    invoke-virtual {v11, v12}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 137
    invoke-virtual {v11, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 138
    invoke-virtual {v11, v9}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 139
    invoke-virtual {v11, v8, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 140
    invoke-virtual {v11, v6, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 141
    invoke-virtual {v11, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 142
    invoke-virtual {v11, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 144
    sget-boolean v12, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->alertDialogIsShowing:Z

    if-nez v12, :cond_0

    .line 145
    invoke-virtual {v11}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    .line 146
    .local v10, "voicePromptUserConfirmationAlertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v10}, Landroid/app/AlertDialog;->show()V

    .line 148
    .end local v10    # "voicePromptUserConfirmationAlertDialog":Landroid/app/AlertDialog;
    :cond_0
    return-void

    .line 77
    .end local v1    # "onCancel":Landroid/content/DialogInterface$OnCancelListener;
    .end local v2    # "onDecline":Landroid/content/DialogInterface$OnClickListener;
    .end local v3    # "onKey":Landroid/content/DialogInterface$OnKeyListener;
    .end local v4    # "onOK":Landroid/content/DialogInterface$OnClickListener;
    .end local v6    # "title_cancel":Ljava/lang/String;
    .end local v7    # "title_dialog":Ljava/lang/String;
    .end local v8    # "title_ok":Ljava/lang/String;
    .end local v9    # "view":Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;
    .end local v11    # "voicePromptUserConfirmationBuilder":Landroid/app/AlertDialog$Builder;
    :cond_1
    sget v12, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_enable:I

    goto :goto_0

    .line 81
    .restart local v6    # "title_cancel":Ljava/lang/String;
    .restart local v7    # "title_dialog":Ljava/lang/String;
    .restart local v8    # "title_ok":Ljava/lang/String;
    :cond_2
    sget-object v12, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;->ON:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView$Switch;

    goto :goto_1

    .line 134
    .restart local v1    # "onCancel":Landroid/content/DialogInterface$OnCancelListener;
    .restart local v2    # "onDecline":Landroid/content/DialogInterface$OnClickListener;
    .restart local v3    # "onKey":Landroid/content/DialogInterface$OnKeyListener;
    .restart local v4    # "onOK":Landroid/content/DialogInterface$OnClickListener;
    .restart local v9    # "view":Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;
    .restart local v11    # "voicePromptUserConfirmationBuilder":Landroid/app/AlertDialog$Builder;
    :cond_3
    new-instance v11, Landroid/app/AlertDialog$Builder;

    .end local v11    # "voicePromptUserConfirmationBuilder":Landroid/app/AlertDialog$Builder;
    invoke-direct {p0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->context()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v11, v12}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .restart local v11    # "voicePromptUserConfirmationBuilder":Landroid/app/AlertDialog$Builder;
    goto :goto_2
.end method

.method public static getShouldAskValue()Z
    .locals 2

    .prologue
    .line 155
    const-string/jumbo v0, "use_voice_prompt_confirm_with_user"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isVoicePromptShowing(Z)V
    .locals 0
    .param p0, "isShowing"    # Z

    .prologue
    .line 151
    sput-boolean p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->alertDialogIsShowing:Z

    .line 152
    return-void
.end method


# virtual methods
.method public confirm(Lcom/vlingo/core/internal/settings/VoicePrompt;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "_voicePrompt"    # Lcom/vlingo/core/internal/settings/VoicePrompt;
    .param p2, "_onConfirm"    # Ljava/lang/Runnable;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->getConfirmationDialog(Lcom/vlingo/core/internal/settings/VoicePrompt;Ljava/lang/Runnable;)V

    .line 58
    return-void
.end method
