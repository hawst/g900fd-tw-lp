.class public Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;
.super Landroid/support/v4/app/Fragment;
.source "SVoiceTutorialFragmentT.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final IS_FIRST_LAUNCH:Ljava/lang/String; = "IS_FIRST_LAUNCH"

.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"


# instance fields
.field private mButtonLayout:Landroid/widget/LinearLayout;

.field private mDefaultSharedPrefs:Landroid/content/SharedPreferences;

.field private mTitleBar:Landroid/app/ActionBar;

.field private mWayForSVoiceTutorialLayout:Landroid/widget/RelativeLayout;

.field private main_layout:Landroid/widget/RelativeLayout;

.field private nextBtn:Landroid/widget/LinearLayout;

.field prefForWishesEventAlert:Landroid/content/SharedPreferences;

.field private root_layout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 35
    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->main_layout:Landroid/widget/RelativeLayout;

    .line 36
    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mButtonLayout:Landroid/widget/LinearLayout;

    .line 37
    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mWayForSVoiceTutorialLayout:Landroid/widget/RelativeLayout;

    .line 38
    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->nextBtn:Landroid/widget/LinearLayout;

    .line 42
    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mTitleBar:Landroid/app/ActionBar;

    .line 43
    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->root_layout:Landroid/widget/RelativeLayout;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;
    .param p1, "x1"    # Landroid/content/SharedPreferences;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    return-object p1
.end method

.method private findViewById(I)Landroid/view/View;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->root_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 92
    sget v0, Lcom/vlingo/midas/R$id;->main_layout_tutorial:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->main_layout:Landroid/widget/RelativeLayout;

    .line 93
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->main_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_bg_tile:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 94
    sget v0, Lcom/vlingo/midas/R$id;->svoice_tutorial:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mWayForSVoiceTutorialLayout:Landroid/widget/RelativeLayout;

    .line 95
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->root_layout:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->button_layout:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mButtonLayout:Landroid/widget/LinearLayout;

    .line 96
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->root_layout:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->next_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->nextBtn:Landroid/widget/LinearLayout;

    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->nextBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->layoutToDisplayIfTOSAccepted()V

    .line 100
    return-void
.end method

.method private layoutToDisplayIfTOSAccepted()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 104
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->svoiceTutorialScreen()V

    .line 105
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mTitleBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mTitleBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->main_layout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->main_layout:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$color;->solid_white:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mWayForSVoiceTutorialLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mWayForSVoiceTutorialLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mButtonLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    .line 118
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 121
    :cond_3
    return-void
.end method

.method private setForNointroductionScreenOnNxtLaunch()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 66
    const-string/jumbo v1, "showing_notifications"

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 67
    const-string/jumbo v1, "all_notifications_accepted"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 68
    invoke-static {v3}, Lcom/vlingo/midas/iux/IUXManager;->setIUXComplete(Z)V

    .line 69
    invoke-static {v2}, Lcom/vlingo/midas/iux/IUXManager;->setIUXIntroRequired(Z)V

    .line 70
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    .local v0, "o":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;
    invoke-virtual {v0, v3}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->setTutorialCompleted(Z)V

    .line 72
    return-void
.end method

.method private showPreferences()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 53
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 54
    iget-object v2, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 55
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "is_move_to_svoice"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 56
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 57
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->setForNointroductionScreenOnNxtLaunch()V

    .line 59
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v2, "is_start_option_menu"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 61
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->startActivity(Landroid/content/Intent;)V

    .line 62
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 63
    return-void
.end method

.method private svoiceTutorialScreen()V
    .locals 3

    .prologue
    .line 128
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->root_layout:Landroid/widget/RelativeLayout;

    sget v2, Lcom/vlingo/midas/R$id;->move_to_tutorial:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 129
    .local v0, "moveToTutorial":Landroid/widget/Button;
    new-instance v1, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT$1;-><init>(Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->init()V

    .line 77
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 78
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 48
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->showPreferences()V

    .line 50
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 152
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->checkVoiceWakeupFeature()V

    .line 153
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 154
    iget-object v1, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 155
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "is_move_to_svoice"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 156
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 157
    invoke-direct {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->setForNointroductionScreenOnNxtLaunch()V

    .line 158
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/iux/IUXManager;->finishIUX(Landroid/app/Activity;)V

    .line 159
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 83
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    sget v0, Lcom/vlingo/midas/R$layout;->tutorial_screen_t:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->root_layout:Landroid/widget/RelativeLayout;

    .line 88
    iget-object v0, p0, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;->root_layout:Landroid/widget/RelativeLayout;

    return-object v0
.end method
