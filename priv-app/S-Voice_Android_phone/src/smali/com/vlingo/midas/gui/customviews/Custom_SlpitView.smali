.class public Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;
.super Landroid/view/ViewGroup;
.source "Custom_SlpitView.java"


# instance fields
.field c1:F

.field c2:F

.field isDragging:Z

.field leftWeight:F

.field pointer_index:I

.field rightWeight:F

.field private splitterBackground:Landroid/graphics/drawable/Drawable;

.field private splitterPos:I

.field private splitterRect:Landroid/graphics/Rect;

.field private splitterSize:I

.field private temp_splitter:Landroid/graphics/drawable/Drawable;

.field private width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 23
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterRect:Landroid/graphics/Rect;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->isDragging:Z

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterRect:Landroid/graphics/Rect;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->isDragging:Z

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->extractAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method private check()V
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 61
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "SplitPaneLayout must have exactly two child views."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    return-void
.end method

.method private extractAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 66
    sget-object v2, Lcom/vlingo/midas/R$styleable;->SplitView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 67
    .local v0, "attributes":Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    const/16 v3, 0xa

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterSize:I

    .line 68
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->leftWeight:F

    .line 69
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->rightWeight:F

    .line 71
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v1

    .line 72
    .local v1, "value":Landroid/util/TypedValue;
    if-eqz v1, :cond_1

    .line 73
    iget v2, v1, Landroid/util/TypedValue;->type:I

    if-eq v2, v4, :cond_0

    iget v2, v1, Landroid/util/TypedValue;->type:I

    if-ne v2, v5, :cond_1

    .line 75
    :cond_0
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterBackground:Landroid/graphics/drawable/Drawable;

    .line 76
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterBackground:Landroid/graphics/drawable/Drawable;

    iput-object v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->temp_splitter:Landroid/graphics/drawable/Drawable;

    .line 79
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 81
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 88
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->temp_splitter:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->temp_splitter:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 90
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->temp_splitter:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 92
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v4, 0x0

    .line 38
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterPos:I

    iget v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterSize:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterPos:I

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterSize:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v4, v2, p5}, Landroid/graphics/Rect;->set(IIII)V

    .line 39
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterPos:I

    iget v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterSize:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v4, v4, v1, p5}, Landroid/view/View;->layout(IIII)V

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterPos:I

    iget v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterSize:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1, v4, p4, p5}, Landroid/view/View;->layout(IIII)V

    .line 41
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 47
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 48
    invoke-direct {p0}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->check()V

    .line 49
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->width:I

    .line 50
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 51
    .local v0, "height":I
    iget v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->leftWeight:F

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->rightWeight:F

    add-float v1, v2, v3

    .line 52
    .local v1, "total":F
    iget v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->width:I

    int-to-float v2, v2

    div-float/2addr v2, v1

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->leftWeight:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterPos:I

    .line 53
    iget v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->width:I

    int-to-float v2, v2

    div-float/2addr v2, v1

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->leftWeight:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterSize:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->c1:F

    .line 54
    iget v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->width:I

    int-to-float v2, v2

    div-float/2addr v2, v1

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->rightWeight:F

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->c2:F

    .line 55
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->c1:F

    float-to-int v3, v3

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 56
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget v3, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->c2:F

    float-to-int v3, v3

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 57
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 98
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 99
    .local v5, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    .line 100
    .local v6, "y":F
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "X ::"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Y ::"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 102
    .local v1, "enlargedRectForTouch":Landroid/graphics/Rect;
    iget-object v9, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    iput v9, v1, Landroid/graphics/Rect;->top:I

    .line 103
    iget-object v9, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    iput v9, v1, Landroid/graphics/Rect;->bottom:I

    .line 104
    iget-object v9, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    add-int/lit8 v9, v9, -0xf

    iput v9, v1, Landroid/graphics/Rect;->left:I

    .line 105
    iget-object v9, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    add-int/lit8 v9, v9, 0xf

    iput v9, v1, Landroid/graphics/Rect;->right:I

    .line 107
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v9

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v10

    invoke-virtual {v1, v9, v10}, Landroid/graphics/Rect;->contains(II)Z

    move-result v9

    if-eq v9, v8, :cond_0

    iget-boolean v9, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->isDragging:Z

    if-eqz v9, :cond_2

    .line 108
    :cond_0
    const-string/jumbo v9, "onTouch"

    const-string/jumbo v10, "True"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 116
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    :pswitch_0
    move v7, v8

    .line 178
    .end local v0    # "action":I
    :cond_2
    return v7

    .line 121
    .restart local v0    # "action":I
    :pswitch_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v9, Lcom/vlingo/midas/R$drawable;->splitter_ondown:I

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    iput-object v7, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->temp_splitter:Landroid/graphics/drawable/Drawable;

    .line 122
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v4

    .line 123
    .local v4, "ip":I
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    iput v7, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->pointer_index:I

    .line 125
    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->performHapticFeedback(I)Z

    .line 126
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    .line 127
    iput-boolean v8, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->isDragging:Z

    .line 128
    const-string/jumbo v7, "Pooja"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Down "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->pointer_index:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 145
    .end local v4    # "ip":I
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v7

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    iget v9, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->pointer_index:I

    if-ne v7, v9, :cond_3

    .line 147
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 149
    iget v7, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->width:I

    int-to-float v7, v7

    sub-float/2addr v7, v5

    div-float/2addr v7, v5

    const v9, 0x3edb6db7

    cmpl-float v7, v7, v9

    if-lez v7, :cond_3

    .line 150
    iput v5, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->leftWeight:F

    .line 151
    iget v7, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->width:I

    int-to-float v7, v7

    sub-float/2addr v7, v5

    iput v7, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->rightWeight:F

    .line 152
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->requestLayout()V

    .line 153
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->invalidate()V

    .line 157
    :cond_3
    const-string/jumbo v7, "Pooja"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "ACTION_MOVE "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->pointer_index:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 162
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v2

    .line 163
    .local v2, "i":I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 164
    .local v3, "id":I
    iget v9, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->pointer_index:I

    if-ne v3, v9, :cond_1

    .line 166
    iget-object v9, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterBackground:Landroid/graphics/drawable/Drawable;

    iput-object v9, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->temp_splitter:Landroid/graphics/drawable/Drawable;

    .line 167
    iput-boolean v7, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->isDragging:Z

    .line 168
    iget-object v7, p0, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->splitterRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/customviews/Custom_SlpitView;->invalidate(Landroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
