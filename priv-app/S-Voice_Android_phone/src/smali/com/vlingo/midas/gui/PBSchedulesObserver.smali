.class public Lcom/vlingo/midas/gui/PBSchedulesObserver;
.super Landroid/database/ContentObserver;
.source "PBSchedulesObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field context:Landroid/content/Context;

.field cursor:Landroid/database/Cursor;

.field youcansayThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/content/Context;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "cont"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 14
    const-class v0, Lcom/vlingo/midas/gui/PBSchedulesObserver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver;->TAG:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver;->context:Landroid/content/Context;

    .line 19
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 48
    iget-object v0, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onChange() : selfChange="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->threadVar:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 50
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->threadVar:Z

    .line 51
    new-instance v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    iget-object v1, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/vlingo/midas/gui/DialogFragment;->se:Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;

    .line 52
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/PBSchedulesObserver$Task;-><init>(Lcom/vlingo/midas/gui/PBSchedulesObserver;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver;->youcansayThread:Ljava/lang/Thread;

    .line 53
    iget-object v0, p0, Lcom/vlingo/midas/gui/PBSchedulesObserver;->youcansayThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 55
    :cond_0
    return-void
.end method
