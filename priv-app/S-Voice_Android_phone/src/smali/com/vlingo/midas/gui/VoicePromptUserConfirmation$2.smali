.class Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$2;
.super Ljava/lang/Object;
.source "VoicePromptUserConfirmation.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->getConfirmationDialog(Lcom/vlingo/core/internal/settings/VoicePrompt;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;

.field final synthetic val$view:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$2;->this$0:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;

    iput-object p2, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$2;->val$view:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 104
    const/4 v0, 0x0

    # setter for: Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->alertDialogIsShowing:Z
    invoke-static {v0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->access$002(Z)Z

    .line 106
    iget-object v0, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$2;->val$view:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->DontAsk()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/settings/VoicePrompt;->shouldAsk(Z)V

    .line 108
    :cond_0
    return-void
.end method
