.class public Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "LocalSearchWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;,
        Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;,
        Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$CallClickListener;,
        Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTION_NAVIGATE_CHINESE:Ljava/lang/String; = "com.autonavi.xmgd.action.NAVIGATE"

.field private static final ACTION_SHOWMAP_CHINESE:Ljava/lang/String; = "com.autonavi.xmgd.action.SHOWMAP"


# instance fields
.field china_localsearch:Landroid/widget/ImageView;

.field private context:Landroid/content/Context;

.field private dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private localSearchListings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vlingo/core/internal/localsearch/LocalSearchListing;",
            ">;"
        }
    .end annotation
.end field

.field private lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

.field private mLocalSearchList:Landroid/widget/ListView;

.field private mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 103
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;

    .line 104
    new-instance v0, Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;)Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;
    .param p1, "x1"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->localSearchListings:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mLocalSearchList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/midas/ui/PackageInfoProvider;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    return-object v0
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 4
    .param p1, "adaptor"    # Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 134
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 135
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    .line 137
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCount()I

    move-result v0

    .line 138
    .local v0, "count":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->localSearchListings:Ljava/util/ArrayList;

    .line 139
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 140
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->localSearchListings:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    invoke-virtual {v3, v1}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getItem(I)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mLocalSearchList:Landroid/widget/ListView;

    new-instance v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 150
    return-void
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 58
    check-cast p1, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->initialize(Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 689
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 683
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 109
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 110
    sget v0, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mLocalSearchList:Landroid/widget/ListView;

    .line 112
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mLocalSearchList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 117
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mLocalSearchList:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 123
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 124
    return-void
.end method

.method public onRecognitionStarted()V
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    if-eqz v0, :cond_0

    .line 696
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;->dismiss()V

    .line 697
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->dialog:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$LocalSearchDialog;

    .line 699
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 129
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onSizeChanged(IIII)V

    .line 130
    return-void
.end method
