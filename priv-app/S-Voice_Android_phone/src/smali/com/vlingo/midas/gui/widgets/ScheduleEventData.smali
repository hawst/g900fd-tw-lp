.class public Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
.super Ljava/lang/Object;
.source "ScheduleEventData.java"


# static fields
.field public static final BIRTHDAY:I = 0x3

.field public static final SCHEDULE:I


# instance fields
.field private inMilli:J

.field private inMilliEnd:J

.field private isAllday:Z

.field private startDate:Ljava/lang/String;

.field private subTitle:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private type:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->isAllday:Z

    return-void
.end method


# virtual methods
.method public getAllday()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->isAllday:Z

    return v0
.end method

.method public getMill()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->inMilli:J

    return-wide v0
.end method

.method public getMillEnd()J
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->inMilliEnd:J

    return-wide v0
.end method

.method public getStartDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->startDate:Ljava/lang/String;

    return-object v0
.end method

.method public getSubTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->subTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->type:I

    return v0
.end method

.method public setAllday()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->isAllday:Z

    .line 41
    return-void
.end method

.method public setMilli(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->inMilli:J

    .line 53
    return-void
.end method

.method public setMilliEnd(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->inMilliEnd:J

    .line 62
    return-void
.end method

.method public setStartDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "startDate"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->startDate:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setSubTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "subTitle"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->subTitle:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->title:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->type:I

    .line 86
    return-void
.end method
