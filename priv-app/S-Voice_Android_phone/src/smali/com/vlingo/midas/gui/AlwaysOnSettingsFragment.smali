.class public Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;
.super Landroid/support/v4/app/Fragment;
.source "AlwaysOnSettingsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$AlwaysOnSettingListener;
    }
.end annotation


# static fields
.field public static final ALWAYS_ON_SETTINGS:Ljava/lang/String; = "always_on_settings"

.field public static final IS_DONE:Ljava/lang/String; = "is_done"

.field public static final IS_FIRST_LAUNCH:Ljava/lang/String; = "IS_FIRST_LAUNCH"

.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"


# instance fields
.field private BubbleText:Landroid/widget/TextView;

.field private alwaysOnContent:Landroid/widget/TextView;

.field private alwaysOnSettingListener:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$AlwaysOnSettingListener;

.field private alwaysOnSettingsLayout:Landroid/widget/LinearLayout;

.field private mContent:Landroid/widget/TextView;

.field private mUrl:Landroid/widget/TextView;

.field private nextBtn:Landroid/widget/LinearLayout;

.field private nextBtnImg:Landroid/widget/ImageView;

.field prefForWishesEventAlert:Landroid/content/SharedPreferences;

.field private previousBtn:Landroid/widget/LinearLayout;

.field private radioGroup:Landroid/widget/RadioGroup;

.field root_container:Landroid/widget/RelativeLayout;

.field sharedpreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnSettingListener:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$AlwaysOnSettingListener;

    .line 43
    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnSettingsLayout:Landroid/widget/LinearLayout;

    .line 44
    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtn:Landroid/widget/LinearLayout;

    .line 45
    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtnImg:Landroid/widget/ImageView;

    .line 46
    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->previousBtn:Landroid/widget/LinearLayout;

    .line 47
    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnContent:Landroid/widget/TextView;

    .line 48
    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mContent:Landroid/widget/TextView;

    .line 49
    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->BubbleText:Landroid/widget/TextView;

    .line 50
    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mUrl:Landroid/widget/TextView;

    .line 267
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->screenWhenDisagree()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mUrl:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->screenWhenConfirmed()V

    return-void
.end method

.method private afterAlwaysOnAccepted()V
    .locals 3

    .prologue
    .line 68
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->sharedpreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 69
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "is_done"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 70
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 72
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->radioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    sget v2, Lcom/vlingo/midas/R$id;->always_accept_radio_btn:I

    if-ne v1, v2, :cond_0

    .line 73
    const-string/jumbo v1, "wake_up_time"

    sget v2, Lcom/vlingo/midas/R$string;->always_including_off:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnSettingListener:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$AlwaysOnSettingListener;

    invoke-interface {v1}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$AlwaysOnSettingListener;->AlwaysOnSetting()V

    .line 78
    return-void

    .line 75
    :cond_0
    const-string/jumbo v1, "wake_up_time"

    sget v2, Lcom/vlingo/midas/R$string;->only_is_s_voice:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private displayAlwaysOnScreen()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 107
    const-string/jumbo v1, "system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v1}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 108
    .local v0, "secRobotoLight":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnSettingsLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnSettingsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnContent:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 113
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasCarModeApp()Z

    move-result v1

    if-nez v1, :cond_6

    .line 114
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnContent:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_samsung_without_carmode:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnContent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    if-ne v1, v2, :cond_1

    .line 119
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnContent:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mContent:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 123
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasCarModeApp()Z

    move-result v1

    if-nez v1, :cond_7

    .line 124
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mContent:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_old_without_carmode:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    :goto_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mContent:Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/midas/gui/CustomLinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 129
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mContent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    if-ne v1, v2, :cond_2

    .line 130
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mContent:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 134
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtnImg:Landroid/widget/ImageView;

    if-eqz v1, :cond_3

    .line 135
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtnImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 138
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtn:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_4

    .line 139
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasLSIChipset()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 140
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtn:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 146
    :cond_4
    :goto_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->previousBtn:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_5

    .line 147
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->previousBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 149
    :cond_5
    return-void

    .line 116
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnContent:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_samsung:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 126
    :cond_7
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mContent:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_old:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 142
    :cond_8
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_2
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 160
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v2, Lcom/vlingo/midas/R$id;->samsung_always_on_layout:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnSettingsLayout:Landroid/widget/LinearLayout;

    .line 162
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v2, Lcom/vlingo/midas/R$id;->always_content_url:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mUrl:Landroid/widget/TextView;

    .line 163
    new-instance v0, Landroid/text/SpannableString;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->always_listening_link:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 164
    .local v0, "content":Landroid/text/SpannableString;
    new-instance v1, Landroid/text/style/UnderlineSpan;

    invoke-direct {v1}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 165
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mUrl:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mUrl:Landroid/widget/TextView;

    new-instance v2, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$1;-><init>(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v2, Lcom/vlingo/midas/R$id;->radio_group:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    iput-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->radioGroup:Landroid/widget/RadioGroup;

    .line 192
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v2, Lcom/vlingo/midas/R$id;->hi_galaxy_send_a_message:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->BubbleText:Landroid/widget/TextView;

    .line 193
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKDDIPhone()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->BubbleText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->always_listening_bubble_for_japanese:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v2, Lcom/vlingo/midas/R$id;->prev_btn:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->previousBtn:Landroid/widget/LinearLayout;

    .line 196
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v2, Lcom/vlingo/midas/R$id;->next_btn_img:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtnImg:Landroid/widget/ImageView;

    .line 197
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v2, Lcom/vlingo/midas/R$id;->next_btn:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtn:Landroid/widget/LinearLayout;

    .line 198
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasLSIChipset()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 201
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->radioGroup:Landroid/widget/RadioGroup;

    sget v2, Lcom/vlingo/midas/R$id;->always_decline_radio_btn:I

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 204
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->radioGroup:Landroid/widget/RadioGroup;

    new-instance v2, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;-><init>(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 228
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->radioGroup:Landroid/widget/RadioGroup;

    sget v2, Lcom/vlingo/midas/R$id;->always_accept_radio_btn:I

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$3;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$3;-><init>(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    iget-object v1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->radioGroup:Landroid/widget/RadioGroup;

    sget v2, Lcom/vlingo/midas/R$id;->always_decline_radio_btn:I

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$4;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$4;-><init>(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    return-void
.end method

.method private screenWhenConfirmed()V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtn:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 256
    return-void
.end method

.method private screenWhenDisagree()V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->nextBtn:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 260
    return-void
.end method

.method private showAlwaysOnScreen()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->displayAlwaysOnScreen()V

    .line 104
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 99
    invoke-direct {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->showAlwaysOnScreen()V

    .line 100
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 246
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 247
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 248
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string/jumbo v2, "WakeupCommandPreference1"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 249
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "isRecorded"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 250
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 252
    .end local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 84
    const-string/jumbo v0, "always_on_settings"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->sharedpreferences:Landroid/content/SharedPreferences;

    .line 85
    instance-of v0, p1, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;

    if-eqz v0, :cond_0

    .line 86
    check-cast p1, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$AlwaysOnSettingListener;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnSettingListener:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$AlwaysOnSettingListener;

    .line 89
    :goto_0
    return-void

    .line 88
    .restart local p1    # "activity":Landroid/app/Activity;
    :cond_0
    new-instance v0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;

    invoke-direct {v0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->alwaysOnSettingListener:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$AlwaysOnSettingListener;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 273
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/midas/iux/IUXManager;->setIUXComplete(Z)V

    .line 274
    invoke-direct {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->afterAlwaysOnAccepted()V

    .line 275
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    sget v0, Lcom/vlingo/midas/R$layout;->always_on_screen:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->root_container:Landroid/widget/RelativeLayout;

    .line 63
    invoke-direct {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->init()V

    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->root_container:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 264
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 265
    return-void
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 153
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 155
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v1

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/vlingo/midas/uifocus/UiFocusActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/uifocus/UiFocusUtil;->onForegrounded(Landroid/content/Context;ZLandroid/content/Intent;)Z

    .line 156
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 93
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 94
    return-void
.end method
