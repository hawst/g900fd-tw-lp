.class public Lcom/vlingo/midas/gui/LandRegularControlFragment;
.super Lcom/vlingo/midas/gui/RegularControlFragmentBase;
.source "LandRegularControlFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/LandRegularControlFragment$1;,
        Lcom/vlingo/midas/gui/LandRegularControlFragment$OnChangeOffLandListener;,
        Lcom/vlingo/midas/gui/LandRegularControlFragment$OnChangeLandListener;
    }
.end annotation


# instance fields
.field LOG_TAG:Ljava/lang/String;

.field private dialogFragment:Lcom/vlingo/midas/gui/ContentFragment;

.field private onChangeListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

.field private onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;-><init>()V

    .line 31
    const-string/jumbo v0, "LandRegularControlFragment"

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->LOG_TAG:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 36
    iput-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 180
    return-void
.end method


# virtual methods
.method getThninkingBtnOver()I
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method getThninkingMicAnimationList()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 82
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->onActivityCreated(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->control_full_container_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->fullContainer:Landroid/widget/RelativeLayout;

    .line 85
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->menu_layout_kitkat_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->layoutMenuBtn:Landroid/widget/LinearLayout;

    .line 87
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 88
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->mic_btn_layout_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->layoutMicBtn:Landroid/widget/LinearLayout;

    .line 89
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->mic_btn_layout_kitkat_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    .line 90
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->mic_status_txt:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->micStatusText:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->layoutMicBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->layoutMenuBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 98
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletHighGUI()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletLowGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->layoutMenuBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 103
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 104
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_toggle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->toggle_btn:Landroid/widget/ImageView;

    .line 105
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_kitkat_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/MicViewKK;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 106
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_mic_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->reco_thinkingBtn:Landroid/widget/ImageView;

    .line 107
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_idle_kitkat_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->reco_idleBtn:Landroid/widget/ImageView;

    .line 118
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_menu_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->menuBtn_Kitkat:Landroid/widget/ImageView;

    .line 120
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->setmActivityCreated(Z)V

    .line 122
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->setNormalPanel()V

    .line 125
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    if-nez v0, :cond_3

    .line 126
    new-instance v0, Lcom/vlingo/midas/gui/LandRegularControlFragment$OnChangeLandListener;

    invoke-direct {v0, p0, v4}, Lcom/vlingo/midas/gui/LandRegularControlFragment$OnChangeLandListener;-><init>(Lcom/vlingo/midas/gui/LandRegularControlFragment;Lcom/vlingo/midas/gui/LandRegularControlFragment$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 127
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    if-nez v0, :cond_4

    .line 128
    new-instance v0, Lcom/vlingo/midas/gui/LandRegularControlFragment$OnChangeOffLandListener;

    invoke-direct {v0, p0, v4}, Lcom/vlingo/midas/gui/LandRegularControlFragment$OnChangeOffLandListener;-><init>(Lcom/vlingo/midas/gui/LandRegularControlFragment;Lcom/vlingo/midas/gui/LandRegularControlFragment$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 130
    :cond_4
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    if-eqz v0, :cond_5

    .line 131
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 132
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 136
    :cond_5
    :goto_2
    return-void

    .line 95
    :cond_6
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->layoutMenuBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 108
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 109
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_kitkat_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/MicViewKK;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 110
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_listening_mic_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->reco_listeningMic:Landroid/widget/ImageView;

    .line 111
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->reco_thinkingBtn:Landroid/widget/ImageView;

    .line 112
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_idle_kitkat_land:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->reco_idleBtn:Landroid/widget/ImageView;

    .line 113
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->mic_idle_kitkat:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->reco_idleMic:Landroid/widget/ImageView;

    .line 114
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->btn_thinking_kitkat_bg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->reco_thinkingBtnBg:Landroid/widget/ImageButton;

    .line 115
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->hour_glass_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->mHourGlassImg:Landroid/widget/ImageView;

    goto/16 :goto_1

    .line 134
    :cond_8
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    goto :goto_2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 150
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 151
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    if-eqz v0, :cond_0

    .line 152
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->onCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ContentFragment;

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->dialogFragment:Lcom/vlingo/midas/gui/ContentFragment;

    .line 68
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    sget v0, Lcom/vlingo/midas/R$layout;->land_control_view:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 162
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    if-eqz v0, :cond_0

    .line 163
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->removeListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 164
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->removeListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 166
    :cond_0
    iput-object v2, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 167
    iput-object v2, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 168
    invoke-super {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->onDestroy()V

    .line 169
    return-void
.end method

.method public onPromptOffClick(Z)V
    .locals 2
    .param p1, "manually"    # Z

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getVoicePrompt()Lcom/vlingo/core/internal/settings/VoicePrompt;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 53
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v0

    if-nez v0, :cond_2

    .line 54
    if-eqz p1, :cond_0

    .line 55
    const-string/jumbo v0, "manually_prompt_on_in_talkback"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 57
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->setManually(Z)V

    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    if-nez v0, :cond_1

    .line 59
    new-instance v0, Lcom/vlingo/midas/gui/LandRegularControlFragment$OnChangeOffLandListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/gui/LandRegularControlFragment$OnChangeOffLandListener;-><init>(Lcom/vlingo/midas/gui/LandRegularControlFragment;Lcom/vlingo/midas/gui/LandRegularControlFragment$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 60
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeOffListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/settings/VoicePrompt;->on()V

    .line 62
    :cond_2
    return-void
.end method

.method public onPromptOnClick(Z)V
    .locals 2
    .param p1, "manually"    # Z

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/LandRegularControlFragment;->getVoicePrompt()Lcom/vlingo/core/internal/settings/VoicePrompt;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    .line 41
    invoke-static {}, Lcom/vlingo/core/internal/settings/VoicePrompt;->isOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    const-string/jumbo v0, "manually_prompt_on_in_talkback"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 43
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    invoke-virtual {v0, p1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->setManually(Z)V

    .line 44
    iget-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/vlingo/midas/gui/LandRegularControlFragment$OnChangeLandListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/gui/LandRegularControlFragment$OnChangeLandListener;-><init>(Lcom/vlingo/midas/gui/LandRegularControlFragment;Lcom/vlingo/midas/gui/LandRegularControlFragment$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    .line 46
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->prompt:Lcom/vlingo/core/internal/settings/VoicePrompt;

    iget-object v1, p0, Lcom/vlingo/midas/gui/LandRegularControlFragment;->onChangeListenerLand:Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/settings/VoicePrompt;->addListener(Lcom/vlingo/core/internal/settings/VoicePrompt$Listener;)Lcom/vlingo/core/internal/settings/VoicePrompt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/settings/VoicePrompt;->off()V

    .line 48
    :cond_1
    return-void
.end method
