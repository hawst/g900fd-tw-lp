.class Lcom/vlingo/midas/gui/customviews/EditText$1;
.super Landroid/content/BroadcastReceiver;
.source "EditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/customviews/EditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/customviews/EditText;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/customviews/EditText;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 308
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "ResponseAxT9Info"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 311
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    const-string/jumbo v2, "AxT9IME.isVisibleWindow"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    # setter for: Lcom/vlingo/midas/gui/customviews/EditText;->mIsKeypadShow:Z
    invoke-static {v1, v2}, Lcom/vlingo/midas/gui/customviews/EditText;->access$002(Lcom/vlingo/midas/gui/customviews/EditText;Z)Z

    .line 313
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    # invokes: Lcom/vlingo/midas/gui/customviews/EditText;->isPermitted()Z
    invoke-static {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->access$400(Lcom/vlingo/midas/gui/customviews/EditText;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    # getter for: Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z
    invoke-static {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->access$500(Lcom/vlingo/midas/gui/customviews/EditText;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 314
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    # getter for: Lcom/vlingo/midas/gui/customviews/EditText;->mIsKeypadShow:Z
    invoke-static {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->access$000(Lcom/vlingo/midas/gui/customviews/EditText;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 315
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    # getter for: Lcom/vlingo/midas/gui/customviews/EditText;->mHandler:Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;
    invoke-static {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->access$600(Lcom/vlingo/midas/gui/customviews/EditText;)Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;->removeMessages(I)V

    .line 316
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/customviews/EditText;->setCursorVisible(Z)V

    .line 318
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->isFocused()Z

    move-result v1

    if-nez v1, :cond_0

    .line 319
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->requestFocus()Z

    .line 321
    :cond_0
    # setter for: Lcom/vlingo/midas/gui/customviews/EditText;->msEditDone:Z
    invoke-static {v3}, Lcom/vlingo/midas/gui/customviews/EditText;->access$702(Z)Z

    .line 330
    :cond_1
    :goto_0
    return-void

    .line 323
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    # setter for: Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z
    invoke-static {v1, v4}, Lcom/vlingo/midas/gui/customviews/EditText;->access$502(Lcom/vlingo/midas/gui/customviews/EditText;Z)Z

    .line 325
    # getter for: Lcom/vlingo/midas/gui/customviews/EditText;->msEditDone:Z
    invoke-static {}, Lcom/vlingo/midas/gui/customviews/EditText;->access$700()Z

    move-result v1

    if-nez v1, :cond_1

    .line 326
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText$1;->this$0:Lcom/vlingo/midas/gui/customviews/EditText;

    # getter for: Lcom/vlingo/midas/gui/customviews/EditText;->mHandler:Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;
    invoke-static {v1}, Lcom/vlingo/midas/gui/customviews/EditText;->access$600(Lcom/vlingo/midas/gui/customviews/EditText;)Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v5, v2, v3}, Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
