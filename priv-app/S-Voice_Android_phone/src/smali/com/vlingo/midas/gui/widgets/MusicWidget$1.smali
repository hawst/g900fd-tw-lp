.class Lcom/vlingo/midas/gui/widgets/MusicWidget$1;
.super Landroid/os/Handler;
.source "MusicWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/MusicWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MusicWidget;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v2, 0x12c

    .line 109
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 114
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$drawable;->voice_music_volume_01:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 115
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 120
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$drawable;->voice_music_volume_02:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 121
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 126
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$drawable;->voice_music_volume_03:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 127
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MusicWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MusicWidget;->mVolumnHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/MusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/MusicWidget;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
