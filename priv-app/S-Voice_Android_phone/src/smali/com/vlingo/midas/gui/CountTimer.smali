.class Lcom/vlingo/midas/gui/CountTimer;
.super Landroid/os/CountDownTimer;
.source "ControlFragment.java"


# instance fields
.field private END_nonSPEECH:I


# direct methods
.method public constructor <init>(JJ)V
    .locals 1
    .param p1, "millisInFuture"    # J
    .param p3, "countDownInterval"    # J

    .prologue
    .line 1291
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 1288
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/midas/gui/CountTimer;->END_nonSPEECH:I

    .line 1293
    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 1298
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->getResult()I

    move-result v0

    iget v1, p0, Lcom/vlingo/midas/gui/CountTimer;->END_nonSPEECH:I

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1300
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->setToIdle()V

    .line 1301
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/vlingo/midas/gui/ControlFragment;->standardFlag:Z

    .line 1302
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1303
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->announceTTSMiniMode()V

    .line 1307
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 1313
    return-void
.end method
