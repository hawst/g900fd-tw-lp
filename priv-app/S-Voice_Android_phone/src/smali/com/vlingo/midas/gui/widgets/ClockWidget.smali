.class public Lcom/vlingo/midas/gui/widgets/ClockWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "ClockWidget.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Ljava/util/Date;",
        ">;"
    }
.end annotation


# static fields
.field private static final M12:Ljava/lang/String; = "hh:mm"

.field private static final M24:Ljava/lang/String; = "kk:mm"


# instance fields
.field private mAmPmText:Landroid/widget/TextView;

.field private mAmPmTextKorean:Landroid/widget/TextView;

.field private mAttached:Z

.field private mBigNumbers_night:[I

.field private mCalendar:Ljava/util/Calendar;

.field private mContext:Landroid/content/Context;

.field private mCountyText:Landroid/widget/TextView;

.field private mDate:Ljava/util/Date;

.field private mDayView:Landroid/widget/TextView;

.field private mFormat:Ljava/lang/CharSequence;

.field private mHourPostView:Landroid/widget/ImageView;

.field private mHourPreView:Landroid/widget/ImageView;

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mLive:Z

.field private mMinPostView:Landroid/widget/ImageView;

.field private mMinPreView:Landroid/widget/ImageView;

.field private mMonthText:Landroid/widget/TextView;

.field private mStrDate:Ljava/lang/String;

.field private mTimeText:Landroid/widget/TextView;

.field private month:Ljava/lang/String;

.field private timeDifference:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->month:Ljava/lang/String;

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->timeDifference:J

    .line 55
    iput-boolean v3, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mLive:Z

    .line 56
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAttached:Z

    .line 65
    const/16 v0, 0xa

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$drawable;->voice_talk_clock_num_0:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$drawable;->voice_talk_clock_num_1:I

    aput v1, v0, v3

    const/4 v1, 0x2

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_clock_num_2:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_clock_num_3:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_clock_num_4:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_clock_num_5:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_clock_num_6:I

    aput v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_clock_num_7:I

    aput v2, v0, v1

    const/16 v1, 0x8

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_clock_num_8:I

    aput v2, v0, v1

    const/16 v1, 0x9

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_clock_num_9:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mBigNumbers_night:[I

    .line 313
    new-instance v0, Lcom/vlingo/midas/gui/widgets/ClockWidget$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/ClockWidget;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 76
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mContext:Landroid/content/Context;

    .line 77
    return-void
.end method

.method static get24HourMode(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 279
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private setDataText(Ljava/util/Date;)V
    .locals 2
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 259
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->formatWidgetDate1(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mStrDate:Ljava/lang/String;

    .line 260
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mStrDate:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mDayView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mStrDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    :cond_0
    return-void
.end method

.method private setDateFormat()V
    .locals 2

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->get24HourMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "kk:mm"

    :goto_0
    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mFormat:Ljava/lang/CharSequence;

    .line 275
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mFormat:Ljava/lang/CharSequence;

    const-string/jumbo v1, "hh:mm"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ru_RU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->setShowAmPm(Z)V

    .line 276
    return-void

    .line 274
    :cond_0
    const-string/jumbo v0, "hh:mm"

    goto :goto_0

    .line 275
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private updateTime()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 181
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->timeDifference:J

    add-long/2addr v5, v7

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    iput-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mDate:Ljava/util/Date;

    .line 182
    iget-boolean v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mLive:Z

    if-eqz v4, :cond_0

    .line 183
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mCalendar:Ljava/util/Calendar;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mDate:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 186
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->setDateFormat()V

    .line 187
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mDate:Ljava/util/Date;

    invoke-direct {p0, v4}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->setDataText(Ljava/util/Date;)V

    .line 189
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mFormat:Ljava/lang/CharSequence;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mCalendar:Ljava/util/Calendar;

    invoke-static {v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 191
    .local v0, "newTime":Ljava/lang/CharSequence;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 196
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->get24HourMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz v0, :cond_2

    .line 197
    const/4 v1, 0x0

    .line 198
    .local v1, "timeValue":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 199
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 200
    const-string/jumbo v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string/jumbo v4, "0"

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 201
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 203
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mTimeText:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    .end local v1    # "timeValue":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mCalendar:Ljava/util/Calendar;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-nez v4, :cond_4

    :goto_1
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->setIsMorning(Z)V

    .line 218
    return-void

    .line 205
    :cond_2
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mTimeText:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 209
    :cond_3
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mHourPreView:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mBigNumbers_night:[I

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 210
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mHourPostView:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mBigNumbers_night:[I

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 213
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mMinPreView:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mBigNumbers_night:[I

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 214
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mMinPostView:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mBigNumbers_night:[I

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x5

    invoke-virtual {v6, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_4
    move v2, v3

    .line 217
    goto :goto_1
.end method


# virtual methods
.method public formatWidgetDate1(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;
    .locals 2
    .param p1, "dateValue"    # Ljava/util/Date;
    .param p2, "currentLocale"    # Ljava/util/Locale;

    .prologue
    .line 238
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEEE"

    invoke-direct {v0, v1, p2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 34
    check-cast p1, Ljava/util/Date;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->initialize(Ljava/util/Date;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/Date;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 5
    .param p1, "dateObj"    # Ljava/util/Date;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 110
    .local v0, "systemLocale":Ljava/util/Locale;
    if-nez p1, :cond_2

    .line 111
    new-instance p1, Ljava/util/Date;

    .end local p1    # "dateObj":Ljava/util/Date;
    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    .line 112
    .restart local p1    # "dateObj":Ljava/util/Date;
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->timeDifference:J

    .line 131
    :goto_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy.MM.dd"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/Date;

    .end local p1    # "dateObj":Ljava/util/Date;
    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    :cond_0
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->month:Ljava/lang/String;

    .line 133
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->month:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 134
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mMonthText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->month:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->onTimeChanged()V

    .line 137
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mCalendar:Ljava/util/Calendar;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->setIsMorning(Z)V

    .line 139
    return-void

    .line 114
    .restart local p1    # "dateObj":Ljava/util/Date;
    :cond_2
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->timeDifference:J

    goto :goto_0

    .line 137
    .end local p1    # "dateObj":Ljava/util/Date;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onDetachedFromWindow()V

    .line 170
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAttached:Z

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAttached:Z

    .line 174
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 83
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    sget v1, Lcom/vlingo/midas/R$id;->clock_time_text:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mTimeText:Landroid/widget/TextView;

    .line 85
    const-string/jumbo v1, "system/fonts/SamsungSans-Num3T.ttf"

    invoke-static {v1}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 86
    .local v0, "samsungSans":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mTimeText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 87
    sget v1, Lcom/vlingo/midas/R$id;->clock_time_ampm_text_korean:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmTextKorean:Landroid/widget/TextView;

    .line 96
    .end local v0    # "samsungSans":Landroid/graphics/Typeface;
    :goto_0
    sget v1, Lcom/vlingo/midas/R$id;->clock_time_ampm_text:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmText:Landroid/widget/TextView;

    .line 97
    sget v1, Lcom/vlingo/midas/R$id;->clock_time_day_text:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mDayView:Landroid/widget/TextView;

    .line 98
    sget v1, Lcom/vlingo/midas/R$id;->clock_month_years_text:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mMonthText:Landroid/widget/TextView;

    .line 100
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mCalendar:Ljava/util/Calendar;

    .line 101
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->setDateFormat()V

    .line 103
    return-void

    .line 91
    :cond_0
    sget v1, Lcom/vlingo/midas/R$id;->clock_hour_prefix:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mHourPreView:Landroid/widget/ImageView;

    .line 92
    sget v1, Lcom/vlingo/midas/R$id;->clock_hour_postfix:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mHourPostView:Landroid/widget/ImageView;

    .line 93
    sget v1, Lcom/vlingo/midas/R$id;->clock_min_prefix:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mMinPreView:Landroid/widget/ImageView;

    .line 94
    sget v1, Lcom/vlingo/midas/R$id;->clock_min_postfix:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mMinPostView:Landroid/widget/ImageView;

    goto :goto_0
.end method

.method public onTimeChanged()V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->updateTime()V

    .line 178
    return-void
.end method

.method public processSystemMessage(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 156
    const-string/jumbo v1, "android.intent.action.TIME_TICK"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 158
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 163
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->onTimeChanged()V

    .line 164
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->invalidate()V

    .line 165
    return-void
.end method

.method setIsMorning(Z)V
    .locals 3
    .param p1, "isMorning"    # Z

    .prologue
    const/16 v2, 0x8

    .line 290
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->setDateFormat()V

    .line 291
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 292
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ko-KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 295
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmTextKorean:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 297
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmTextKorean:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    sget v0, Lcom/vlingo/midas/R$string;->am:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 298
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmTextKorean:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311
    :cond_0
    :goto_1
    return-void

    .line 297
    :cond_1
    sget v0, Lcom/vlingo/midas/R$string;->pm:I

    goto :goto_0

    .line 302
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmText:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    sget v0, Lcom/vlingo/midas/R$string;->am:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 303
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmTextKorean:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmTextKorean:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 302
    :cond_3
    sget v0, Lcom/vlingo/midas/R$string;->pm:I

    goto :goto_2

    .line 307
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmTextKorean:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmTextKorean:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method setShowAmPm(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 283
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ClockWidget;->mAmPmText:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    return-void

    .line 283
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setTextViewScale(Landroid/widget/TextView;I)V
    .locals 7
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "textViewWidth"    # I

    .prologue
    .line 242
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextScaleX()F

    move-result v0

    .line 243
    .local v0, "originScaleX":F
    move v3, v0

    .line 244
    .local v3, "scaleX":F
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 246
    .local v4, "text":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    .line 247
    .local v1, "paint":Landroid/graphics/Paint;
    :goto_0
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    int-to-float v6, p2

    cmpl-float v5, v5, v6

    if-lez v5, :cond_0

    .line 248
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 249
    .end local v1    # "paint":Landroid/graphics/Paint;
    .local v2, "paint":Landroid/graphics/Paint;
    const v5, 0x3dcccccd    # 0.1f

    sub-float/2addr v3, v5

    .line 250
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextScaleX(F)V

    move-object v1, v2

    .end local v2    # "paint":Landroid/graphics/Paint;
    .restart local v1    # "paint":Landroid/graphics/Paint;
    goto :goto_0

    .line 253
    :cond_0
    sub-float v5, v0, v3

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_1

    .line 254
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 256
    :cond_1
    return-void
.end method
