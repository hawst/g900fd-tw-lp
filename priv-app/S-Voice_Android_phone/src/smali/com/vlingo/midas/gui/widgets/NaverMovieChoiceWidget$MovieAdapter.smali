.class Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;
.super Landroid/widget/BaseAdapter;
.source "NaverMovieChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MovieAdapter"
.end annotation


# instance fields
.field private final mAllMovies:Lcom/vlingo/midas/naver/NaverAdaptor;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mMovies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;Landroid/content/Context;Lcom/vlingo/midas/naver/NaverAdaptor;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "naveradopter"    # Lcom/vlingo/midas/naver/NaverAdaptor;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 227
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 228
    invoke-virtual {p3}, Lcom/vlingo/midas/naver/NaverAdaptor;->getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v0

    iget-object v0, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->itemList:Ljava/util/List;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->mMovies:Ljava/util/List;

    .line 229
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->mAllMovies:Lcom/vlingo/midas/naver/NaverAdaptor;

    .line 230
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    .line 234
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->mMovies:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 235
    .local v0, "naturalSize":I
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    invoke-virtual {v2, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->getLimitedCount(I)I

    move-result v1

    .line 236
    .local v1, "toReturn":I
    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 241
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->mMovies:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 246
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x8

    .line 252
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$200(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int/lit16 v0, v5, 0xa0

    .line 254
    .local v0, "density":I
    if-nez p2, :cond_0

    .line 255
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v6, Lcom/vlingo/midas/R$layout;->item_movie_choice:I

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 257
    new-instance v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;

    invoke-direct {v3}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;-><init>()V

    .line 260
    .local v3, "holder":Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;
    sget v5, Lcom/vlingo/midas/R$id;->text_Movie_Title:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieTitle:Landroid/widget/TextView;

    .line 261
    sget v5, Lcom/vlingo/midas/R$id;->image_Movie_Grade:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieGrade:Landroid/widget/TextView;

    .line 262
    sget v5, Lcom/vlingo/midas/R$id;->text_Movie_Director:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieDirector:Landroid/widget/TextView;

    .line 263
    sget v5, Lcom/vlingo/midas/R$id;->text_Movie_Actors:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieActor:Landroid/widget/TextView;

    .line 264
    sget v5, Lcom/vlingo/midas/R$id;->text_Movie_Rating:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieRatingNumber:Landroid/widget/TextView;

    .line 265
    sget v5, Lcom/vlingo/midas/R$id;->ratings:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RatingBar;

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieRating:Landroid/widget/RatingBar;

    .line 266
    sget v5, Lcom/vlingo/midas/R$id;->image_Movie_Poster:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMoviePoster:Landroid/widget/ImageView;

    .line 267
    sget v5, Lcom/vlingo/midas/R$id;->movie_divider:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieDivider:Landroid/view/View;

    .line 268
    sget v5, Lcom/vlingo/midas/R$id;->text_Movie_Rank:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieRank:Landroid/widget/TextView;

    .line 269
    sget v5, Lcom/vlingo/midas/R$id;->text_Movie_OpenDate:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieOpenDate:Landroid/widget/TextView;

    .line 271
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 277
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 278
    sget v5, Lcom/vlingo/midas/R$id;->image_Moive_Rank:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 279
    sget v5, Lcom/vlingo/midas/R$id;->text_Movie_Rank:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 287
    :goto_1
    new-instance v4, Lcom/vlingo/midas/naver/NaverMovieItem;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->mMovies:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Hashtable;

    invoke-direct {v4, v6, v5}, Lcom/vlingo/midas/naver/NaverMovieItem;-><init>(Landroid/content/Context;Ljava/util/Hashtable;)V

    .line 290
    .local v4, "item":Lcom/vlingo/midas/naver/NaverMovieItem;
    iget-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieTitle:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverMovieItem;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    iget-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieRating:Landroid/widget/RatingBar;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverMovieItem;->getRating()Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    const/high16 v7, 0x41200000    # 10.0f

    div-float/2addr v6, v7

    iget-object v7, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieRating:Landroid/widget/RatingBar;

    invoke-virtual {v7}, Landroid/widget/RatingBar;->getNumStars()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/RatingBar;->setRating(F)V

    .line 292
    iget-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieRatingNumber:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverMovieItem;->getRating()Ljava/lang/Float;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    iget-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieRank:Landroid/widget/TextView;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    iget-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieActor:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverMovieItem;->getActors()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    iget-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieDirector:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverMovieItem;->getDirectors()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieOpenDate:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverMovieItem;->getOpenDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverMovieItem;->getServiceUrl()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mServicelink:Ljava/lang/String;

    .line 300
    iget-object v2, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieGrade:Landroid/widget/TextView;

    .line 301
    .local v2, "gradeView":Landroid/widget/TextView;
    invoke-virtual {v4}, Lcom/vlingo/midas/naver/NaverMovieItem;->getGrade()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/naver/NaverMovieItem;->getGradeToInt(Ljava/lang/String;)Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;

    move-result-object v1

    .line 302
    .local v1, "grade":Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;
    sget-object v5, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$2;->$SwitchMap$com$vlingo$midas$naver$NaverMovieItem$GradeInt:[I

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 317
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 319
    :goto_2
    sget v5, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 321
    if-nez p1, :cond_2

    .line 375
    :goto_3
    new-instance v5, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter$1;

    invoke-direct {v5, p0}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;)V

    invoke-virtual {p2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    return-object p2

    .line 274
    .end local v1    # "grade":Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;
    .end local v2    # "gradeView":Landroid/widget/TextView;
    .end local v3    # "holder":Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;
    .end local v4    # "item":Lcom/vlingo/midas/naver/NaverMovieItem;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;

    .restart local v3    # "holder":Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;
    goto/16 :goto_0

    .line 283
    :cond_1
    sget v5, Lcom/vlingo/midas/R$id;->image_Moive_Rank:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 284
    sget v5, Lcom/vlingo/midas/R$id;->text_Movie_Rank:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 305
    .restart local v1    # "grade":Lcom/vlingo/midas/naver/NaverMovieItem$GradeInt;
    .restart local v2    # "gradeView":Landroid/widget/TextView;
    .restart local v4    # "item":Lcom/vlingo/midas/naver/NaverMovieItem;
    :pswitch_0
    sget v5, Lcom/vlingo/midas/R$drawable;->movie_age_12_bg:I

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 306
    const-string/jumbo v5, "12"

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 308
    :pswitch_1
    sget v5, Lcom/vlingo/midas/R$drawable;->movie_age_15_bg:I

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 309
    const-string/jumbo v5, "15"

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 311
    :pswitch_2
    sget v5, Lcom/vlingo/midas/R$drawable;->movie_age_18_bg:I

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 312
    const-string/jumbo v5, "18"

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 314
    :pswitch_3
    sget v5, Lcom/vlingo/midas/R$drawable;->movie_age_all_bg:I

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 315
    const-string/jumbo v5, "A"

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 354
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieAdapter;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-lt p1, v5, :cond_3

    .line 356
    iget-object v5, v3, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$MovieHolder;->mMovieDivider:Landroid/view/View;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 371
    :cond_3
    sget v5, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3

    .line 302
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
