.class public Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;
.super Landroid/widget/ImageView;
.source "AnswerQuestionWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "RenderedImage"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 311
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 312
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 313
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->init()V

    return-void
.end method

.method private init()V
    .locals 0

    .prologue
    .line 315
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 15
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 322
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 324
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 326
    .local v7, "d":Landroid/graphics/drawable/Drawable;
    instance-of v1, v7, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_2

    .line 329
    const/4 v11, 0x0

    .line 330
    .local v11, "resizeRequired":Z
    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 332
    .local v0, "bitmapOrg":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    .line 333
    .local v9, "newHeight":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    .line 335
    .local v10, "newWidth":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getMaximumBitmapHeight()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 336
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getMaximumBitmapHeight()I

    move-result v9

    .line 337
    const/4 v11, 0x1

    .line 342
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getMaximumBitmapWidth()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 343
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getMaximumBitmapWidth()I

    move-result v10

    .line 344
    const/4 v11, 0x1

    .line 349
    :cond_1
    if-eqz v11, :cond_2

    .line 351
    int-to-float v1, v10

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v14, v1, v2

    .line 352
    .local v14, "scaleWidth":F
    int-to-float v1, v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v13, v1, v2

    .line 354
    .local v13, "scaleHeight":F
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 356
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v5, v14, v13}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 359
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 361
    .local v12, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v8, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v8, v12}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 362
    .local v8, "dTmp":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 367
    .end local v0    # "bitmapOrg":Landroid/graphics/Bitmap;
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v8    # "dTmp":Landroid/graphics/drawable/Drawable;
    .end local v9    # "newHeight":I
    .end local v10    # "newWidth":I
    .end local v11    # "resizeRequired":Z
    .end local v12    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v13    # "scaleHeight":F
    .end local v14    # "scaleWidth":F
    :cond_2
    invoke-super/range {p0 .. p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 368
    return-void
.end method
