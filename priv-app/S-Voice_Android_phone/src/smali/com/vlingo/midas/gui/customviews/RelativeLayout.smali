.class public Lcom/vlingo/midas/gui/customviews/RelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "RelativeLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;
    }
.end annotation


# static fields
.field private static layoutInflatedListener:Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public static removeLayoutInflateListener()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->layoutInflatedListener:Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;

    .line 23
    return-void
.end method

.method public static setLayoutInflateListener(Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;)V
    .locals 0
    .param p0, "layoutlistener"    # Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;

    .prologue
    .line 18
    sput-object p0, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->layoutInflatedListener:Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;

    .line 19
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 36
    sget-object v0, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->layoutInflatedListener:Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;

    if-eqz v0, :cond_0

    .line 37
    sget-object v0, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->layoutInflatedListener:Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;

    invoke-interface {v0}, Lcom/vlingo/midas/gui/customviews/RelativeLayout$OnLayoutInflateListener;->onLayoutInflated()V

    .line 39
    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 40
    return-void
.end method
