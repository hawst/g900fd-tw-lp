.class public Lcom/vlingo/midas/gui/ControlFragmentData;
.super Ljava/lang/Object;
.source "ControlFragmentData.java"


# static fields
.field private static singletonObject:Lcom/vlingo/midas/gui/ControlFragmentData;


# instance fields
.field private currentState:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

.field private isPaused:Z

.field private mTTSOnIdleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private startRecoOnSpotterStop:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->startRecoOnSpotterStop:Z

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->isPaused:Z

    .line 21
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->currentState:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->mTTSOnIdleList:Ljava/util/ArrayList;

    .line 11
    return-void
.end method

.method public static getIntance()Lcom/vlingo/midas/gui/ControlFragmentData;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentData;->singletonObject:Lcom/vlingo/midas/gui/ControlFragmentData;

    if-nez v0, :cond_0

    .line 14
    new-instance v0, Lcom/vlingo/midas/gui/ControlFragmentData;

    invoke-direct {v0}, Lcom/vlingo/midas/gui/ControlFragmentData;-><init>()V

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentData;->singletonObject:Lcom/vlingo/midas/gui/ControlFragmentData;

    .line 16
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentData;->singletonObject:Lcom/vlingo/midas/gui/ControlFragmentData;

    return-object v0
.end method


# virtual methods
.method public getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->currentState:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    return-object v0
.end method

.method public getTTSOnIdleList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->mTTSOnIdleList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->isPaused:Z

    return v0
.end method

.method public isStartRecoOnSpotterStop()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->startRecoOnSpotterStop:Z

    return v0
.end method

.method public setCurrentState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V
    .locals 0
    .param p1, "currentState"    # Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->currentState:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    .line 46
    return-void
.end method

.method public setPaused(Z)V
    .locals 0
    .param p1, "isPaused"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->isPaused:Z

    .line 39
    return-void
.end method

.method public setStartRecoOnSpotterStop(Z)V
    .locals 0
    .param p1, "startRecoOnSpotterStop"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->startRecoOnSpotterStop:Z

    .line 30
    return-void
.end method

.method public setTTSOnIdleList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "mTTSOnIdleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragmentData;->mTTSOnIdleList:Ljava/util/ArrayList;

    .line 54
    return-void
.end method
