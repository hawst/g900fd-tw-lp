.class Lcom/vlingo/midas/gui/DialogFragment$AnimThread;
.super Ljava/lang/Thread;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimThread"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;
    }
.end annotation


# static fields
.field private static final ANIM_FRAME_DELAY:I = 0x5

.field private static final ANIM_FRAME_MAX:I = 0x14

.field private static final ANIM_FRAME_MIN:I = 0x1

.field private static final ANIM_FRAME_MIN_START_POINT:I = 0x14


# instance fields
.field private final dialogFragment:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/DialogFragment;",
            ">;"
        }
    .end annotation
.end field

.field private isWolframFlag:Z

.field private mDirection:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

.field private mFromHeight:I

.field private final mToHeight:I

.field private padding:I

.field private skipAnimThread:Z


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;)V
    .locals 1
    .param p1, "dialogFragment"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    const/4 v0, 0x0

    .line 2000
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1992
    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->padding:I

    .line 1993
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->isWolframFlag:Z

    .line 1994
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->skipAnimThread:Z

    .line 1996
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;->FORWARD:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mDirection:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    .line 2001
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    .line 2004
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mToHeight:I

    .line 2005
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mFromHeight:I

    .line 2007
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;I)V
    .locals 1
    .param p1, "dialogFragment"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p2, "BottomPadding"    # I

    .prologue
    const/4 v0, 0x0

    .line 2031
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1992
    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->padding:I

    .line 1993
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->isWolframFlag:Z

    .line 1994
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->skipAnimThread:Z

    .line 1996
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;->FORWARD:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mDirection:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    .line 2032
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    .line 2034
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    sub-int/2addr v0, p2

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mToHeight:I

    .line 2036
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mFromHeight:I

    .line 2037
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;IZ)V
    .locals 1
    .param p1, "dialogFragment"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p2, "padding"    # I
    .param p3, "isWolframFlag"    # Z

    .prologue
    const/4 v0, 0x0

    .line 2040
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1992
    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->padding:I

    .line 1993
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->isWolframFlag:Z

    .line 1994
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->skipAnimThread:Z

    .line 1996
    sget-object v0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;->FORWARD:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mDirection:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    .line 2041
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    .line 2043
    iput p2, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->padding:I

    .line 2044
    iput-boolean p3, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->isWolframFlag:Z

    .line 2045
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mToHeight:I

    .line 2046
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mFromHeight:I

    .line 2047
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;Z)V
    .locals 6
    .param p1, "dialogFragment"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p2, "helpFlag"    # Z

    .prologue
    const/4 v4, 0x0

    .line 2009
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1992
    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->padding:I

    .line 1993
    iput-boolean v4, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->isWolframFlag:Z

    .line 1994
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->skipAnimThread:Z

    .line 1996
    sget-object v4, Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;->FORWARD:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mDirection:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    .line 2010
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    .line 2012
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 2013
    .local v2, "numViews":I
    const/4 v0, 0x0

    .line 2014
    .local v0, "height":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 2015
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2016
    .local v3, "v":Landroid/view/View;
    instance-of v4, v3, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-eqz v4, :cond_1

    .line 2021
    .end local v3    # "v":Landroid/view/View;
    :cond_0
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {p1}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v4

    iput v4, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mFromHeight:I

    .line 2022
    iput v0, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mToHeight:I

    .line 2024
    iget v4, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mFromHeight:I

    iget v5, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mToHeight:I

    if-ge v4, v5, :cond_2

    .line 2025
    sget-object v4, Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;->FORWARD:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mDirection:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    .line 2029
    :goto_1
    return-void

    .line 2018
    .restart local v3    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    .line 2014
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2027
    .end local v3    # "v":Landroid/view/View;
    :cond_2
    sget-object v4, Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;->REVERSE:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    iput-object v4, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mDirection:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    goto :goto_1
.end method

.method private continueLoop()Z
    .locals 2

    .prologue
    .line 2050
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/DialogFragment;

    .line 2051
    .local v0, "df":Lcom/vlingo/midas/gui/DialogFragment;
    if-nez v0, :cond_0

    .line 2052
    const/4 v1, 0x0

    .line 2054
    :goto_0
    return v1

    :cond_0
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mActivityCreated:Z
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragment;->access$200(Lcom/vlingo/midas/gui/DialogFragment;)Z

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/16 v8, 0x14

    .line 2059
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mDirection:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    sget-object v9, Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;->FORWARD:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    if-ne v7, v9, :cond_2

    move v3, v8

    .line 2060
    .local v3, "increment":I
    :goto_0
    iget v1, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mFromHeight:I

    .line 2062
    .local v1, "height":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v7}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    .line 2063
    .local v4, "numViews":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_0

    .line 2065
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v7}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v7

    add-int/lit8 v9, v2, -0x2

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2066
    .local v5, "v":Landroid/view/View;
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v7}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 2067
    .local v6, "v1":Landroid/view/View;
    if-eqz v5, :cond_3

    if-eqz v6, :cond_3

    .line 2068
    instance-of v7, v6, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v7, :cond_3

    check-cast v6, Lcom/vlingo/midas/gui/DialogBubble;

    .end local v6    # "v1":Landroid/view/View;
    invoke-virtual {v6}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v7

    sget-object v9, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v7, v9, :cond_3

    .line 2069
    instance-of v7, v5, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-eqz v7, :cond_3

    .line 2070
    iput-boolean v11, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->skipAnimThread:Z

    .line 2076
    .end local v5    # "v":Landroid/view/View;
    :cond_0
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mDirection:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    sget-object v9, Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;->FORWARD:Lcom/vlingo/midas/gui/DialogFragment$AnimThread$HelpAnimDir;

    if-ne v7, v9, :cond_4

    .line 2078
    iget-boolean v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->skipAnimThread:Z

    if-eqz v7, :cond_5

    .line 2079
    :cond_1
    :goto_2
    iget v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mToHeight:I

    if-ge v1, v7, :cond_5

    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->continueLoop()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2080
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/DialogFragment;

    .line 2085
    .local v0, "df":Lcom/vlingo/midas/gui/DialogFragment;
    # invokes: Lcom/vlingo/midas/gui/DialogFragment;->doScrollAnim(I)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$1300(Lcom/vlingo/midas/gui/DialogFragment;I)V

    .line 2087
    const/4 v0, 0x0

    .line 2089
    add-int/2addr v1, v3

    .line 2091
    const-wide/16 v9, 0x5

    :try_start_0
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2096
    :goto_3
    iget v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mToHeight:I

    iget v9, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mFromHeight:I

    sub-int/2addr v7, v9

    if-ge v7, v8, :cond_1

    .line 2097
    const/4 v3, 0x1

    goto :goto_2

    .line 2059
    .end local v0    # "df":Lcom/vlingo/midas/gui/DialogFragment;
    .end local v1    # "height":I
    .end local v2    # "i":I
    .end local v3    # "increment":I
    .end local v4    # "numViews":I
    :cond_2
    const/16 v3, -0x14

    goto :goto_0

    .line 2063
    .restart local v1    # "height":I
    .restart local v2    # "i":I
    .restart local v3    # "increment":I
    .restart local v4    # "numViews":I
    .restart local v5    # "v":Landroid/view/View;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2103
    .end local v5    # "v":Landroid/view/View;
    :cond_4
    :goto_4
    iget v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mToHeight:I

    if-le v1, v7, :cond_5

    invoke-direct {p0}, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->continueLoop()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2104
    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/DialogFragment;

    .line 2109
    .restart local v0    # "df":Lcom/vlingo/midas/gui/DialogFragment;
    # invokes: Lcom/vlingo/midas/gui/DialogFragment;->doScrollAnim(I)V
    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$1300(Lcom/vlingo/midas/gui/DialogFragment;I)V

    .line 2111
    const/4 v0, 0x0

    .line 2113
    add-int/2addr v1, v3

    .line 2115
    const-wide/16 v9, 0x5

    :try_start_1
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2120
    :goto_5
    iget v7, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mFromHeight:I

    iget v9, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->mToHeight:I

    sub-int/2addr v7, v9

    if-ge v7, v8, :cond_4

    .line 2121
    const/4 v3, -0x1

    goto :goto_4

    .line 2126
    .end local v0    # "df":Lcom/vlingo/midas/gui/DialogFragment;
    :cond_5
    iput-boolean v11, p0, Lcom/vlingo/midas/gui/DialogFragment$AnimThread;->isWolframFlag:Z

    .line 2127
    return-void

    .line 2092
    .restart local v0    # "df":Lcom/vlingo/midas/gui/DialogFragment;
    :catch_0
    move-exception v7

    goto :goto_3

    .line 2116
    :catch_1
    move-exception v7

    goto :goto_5
.end method
