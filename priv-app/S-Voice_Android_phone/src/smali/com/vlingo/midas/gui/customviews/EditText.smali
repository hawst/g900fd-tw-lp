.class public Lcom/vlingo/midas/gui/customviews/EditText;
.super Landroid/widget/EditText;
.source "EditText.java"

# interfaces
.implements Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;
    }
.end annotation


# static fields
.field private static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field private static final MSG_EDIT_CANCELED:I = 0x2

.field private static final MSG_LOSS_WINDOW_FOCUS:I = 0x1

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"

.field public static Touchable:Z

.field private static msEditDone:Z


# instance fields
.field private firstTouch:Z

.field private final mContext:Landroid/content/Context;

.field private mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

.field private mHandler:Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;

.field private mIsKeypadShow:Z

.field private mIsReceiverRegistered:Z

.field private final mMainReceiver:Landroid/content/BroadcastReceiver;

.field private mPermitted:Z

.field private final mSoftkeyReceiver:Landroid/os/ResultReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/midas/gui/customviews/EditText;->Touchable:Z

    .line 48
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/customviews/EditText;->msEditDone:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 91
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z

    .line 86
    new-instance v0, Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;-><init>(Lcom/vlingo/midas/gui/customviews/EditText;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mHandler:Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;

    .line 88
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mPermitted:Z

    .line 305
    new-instance v0, Lcom/vlingo/midas/gui/customviews/EditText$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/customviews/EditText$1;-><init>(Lcom/vlingo/midas/gui/customviews/EditText;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mMainReceiver:Landroid/content/BroadcastReceiver;

    .line 334
    new-instance v0, Lcom/vlingo/midas/gui/customviews/EditText$2;

    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mHandler:Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/gui/customviews/EditText$2;-><init>(Lcom/vlingo/midas/gui/customviews/EditText;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mSoftkeyReceiver:Landroid/os/ResultReceiver;

    .line 92
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/customviews/EditText;->setLongClickable(Z)V

    .line 93
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/customviews/EditText;->setEnabled(Z)V

    .line 94
    iput-object p1, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mContext:Landroid/content/Context;

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/customviews/EditText;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/EditText;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mIsKeypadShow:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/gui/customviews/EditText;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/EditText;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mIsKeypadShow:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/customviews/EditText;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/EditText;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/customviews/EditText;->setEditUI(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/customviews/EditText;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/EditText;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->editCanceled()V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/customviews/EditText;)Lcom/vlingo/midas/gui/OnEditListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/EditText;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/customviews/EditText;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/EditText;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->isPermitted()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/vlingo/midas/gui/customviews/EditText;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/EditText;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z

    return v0
.end method

.method static synthetic access$502(Lcom/vlingo/midas/gui/customviews/EditText;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/EditText;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z

    return p1
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/customviews/EditText;)Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/customviews/EditText;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mHandler:Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;

    return-object v0
.end method

.method static synthetic access$700()Z
    .locals 1

    .prologue
    .line 36
    sget-boolean v0, Lcom/vlingo/midas/gui/customviews/EditText;->msEditDone:Z

    return v0
.end method

.method static synthetic access$702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 36
    sput-boolean p0, Lcom/vlingo/midas/gui/customviews/EditText;->msEditDone:Z

    return p0
.end method

.method private editCanceled()V
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 281
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->hideKeypad()V

    .line 283
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    invoke-interface {v0, p0}, Lcom/vlingo/midas/gui/OnEditListener;->onEditCanceled(Landroid/view/View;)V

    .line 286
    :cond_0
    return-void
.end method

.method private isPermitted()Z
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mPermitted:Z

    return v0
.end method

.method private setEditUI(Z)V
    .locals 9
    .param p1, "edit"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0xff

    const/16 v5, 0xdc

    .line 245
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->getPaddingTop()I

    move-result v3

    .line 246
    .local v3, "t":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->getPaddingBottom()I

    move-result v0

    .line 247
    .local v0, "b":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->getPaddingRight()I

    move-result v2

    .line 248
    .local v2, "r":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->getPaddingLeft()I

    move-result v1

    .line 250
    .local v1, "l":I
    invoke-super {p0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 252
    if-eqz p1, :cond_2

    .line 253
    sget v4, Lcom/vlingo/midas/R$drawable;->voice_talk_message_typed:I

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/customviews/EditText;->setBackgroundResource(I)V

    .line 254
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 255
    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/customviews/EditText;->setTextColor(I)V

    .line 259
    :goto_0
    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/customviews/EditText;->setCursorVisible(Z)V

    .line 261
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->isFocused()Z

    move-result v4

    if-nez v4, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->requestFocus()Z

    .line 264
    :cond_0
    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/customviews/EditText;->setLongClickable(Z)V

    .line 265
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->showKeyapd()V

    .line 276
    :goto_1
    invoke-virtual {p0, v1, v3, v2, v0}, Lcom/vlingo/midas/gui/customviews/EditText;->setPadding(IIII)V

    .line 277
    return-void

    .line 257
    :cond_1
    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/customviews/EditText;->setTextColor(I)V

    goto :goto_0

    .line 267
    :cond_2
    sget v4, Lcom/vlingo/midas/R$drawable;->voice_talk_message_02:I

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/customviews/EditText;->setBackgroundResource(I)V

    .line 268
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 269
    const/4 v4, -0x1

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/customviews/EditText;->setTextColor(I)V

    .line 273
    :goto_2
    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/customviews/EditText;->setCursorVisible(Z)V

    .line 274
    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/customviews/EditText;->setLongClickable(Z)V

    goto :goto_1

    .line 271
    :cond_3
    const/high16 v4, -0x1000000

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/customviews/EditText;->setTextColor(I)V

    goto :goto_2
.end method


# virtual methods
.method public hideKeypad()V
    .locals 4

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 299
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mSoftkeyReceiver:Landroid/os/ResultReceiver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    .line 302
    :cond_0
    return-void
.end method

.method public isReadyToTouch()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 381
    invoke-super {p0}, Landroid/widget/EditText;->onAttachedToWindow()V

    .line 382
    invoke-static {p0}, Lcom/vlingo/midas/util/MiniModeUtils;->addListener(Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;)V

    .line 383
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 4
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    .line 106
    .local v0, "connection":Landroid/view/inputmethod/InputConnection;
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    and-int/lit16 v1, v2, 0xff

    .line 108
    .local v1, "imeActions":I
    and-int/lit8 v2, v1, 0x6

    if-eqz v2, :cond_0

    .line 110
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    xor-int/2addr v2, v1

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 112
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/lit8 v2, v2, 0x6

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 114
    :cond_0
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    .line 115
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v3, -0x40000001    # -1.9999999f

    and-int/2addr v2, v3

    iput v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 118
    :cond_1
    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 388
    invoke-super {p0}, Landroid/widget/EditText;->onDetachedFromWindow()V

    .line 389
    invoke-static {p0}, Lcom/vlingo/midas/util/MiniModeUtils;->removeListener(Lcom/vlingo/midas/util/MiniModeUtils$OnMiniModeChangeListener;)V

    .line 390
    return-void
.end method

.method public onEditorAction(I)V
    .locals 2
    .param p1, "actionCode"    # I

    .prologue
    const/4 v1, 0x1

    .line 212
    invoke-super {p0, p1}, Landroid/widget/EditText;->onEditorAction(I)V

    .line 214
    const/4 v0, 0x6

    if-ne p1, v0, :cond_1

    .line 215
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z

    .line 216
    sput-boolean v1, Lcom/vlingo/midas/gui/customviews/EditText;->msEditDone:Z

    .line 218
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->hideKeypad()V

    .line 220
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/customviews/EditText;->setEditUI(Z)V

    .line 222
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    invoke-interface {v0, p0}, Lcom/vlingo/midas/gui/OnEditListener;->onEditFinished(Landroid/view/View;)V

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dismissKeypad()V

    .line 230
    :cond_1
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 364
    const/16 v1, 0x42

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 365
    iput-boolean v3, p0, Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z

    .line 366
    sput-boolean v3, Lcom/vlingo/midas/gui/customviews/EditText;->msEditDone:Z

    .line 367
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->hideKeypad()V

    .line 368
    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/customviews/EditText;->setEditUI(Z)V

    .line 370
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eqz v1, :cond_0

    .line 371
    iget-object v1, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    invoke-interface {v1, p0}, Lcom/vlingo/midas/gui/OnEditListener;->onEditFinished(Landroid/view/View;)V

    .line 375
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 352
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dismissKeypad()V

    .line 355
    const/4 v0, 0x0

    .line 358
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMiniModeChange(Z)V
    .locals 1
    .param p1, "isMiniMode"    # Z

    .prologue
    .line 395
    if-eqz p1, :cond_0

    .line 396
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/customviews/EditText;->Touchable:Z

    .line 399
    :goto_0
    return-void

    .line 398
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vlingo/midas/gui/customviews/EditText;->Touchable:Z

    goto :goto_0
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "lengthBefore"    # I
    .param p4, "lengthAfter"    # I

    .prologue
    .line 127
    if-ne p3, p4, :cond_1

    .line 130
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->isPermitted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    invoke-interface {v0, p0}, Lcom/vlingo/midas/gui/OnEditListener;->onEditCanceled(Landroid/view/View;)V

    .line 138
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 139
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->isPermitted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    invoke-interface {v0, p0}, Lcom/vlingo/midas/gui/OnEditListener;->onEditFinished(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 144
    sget-boolean v3, Lcom/vlingo/midas/gui/customviews/EditText;->Touchable:Z

    if-nez v3, :cond_1

    move v1, v2

    .line 163
    :cond_0
    :goto_0
    return v1

    .line 146
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 148
    .local v1, "result":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 150
    .local v0, "action":I
    if-nez v0, :cond_2

    .line 151
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mHandler:Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/EditText$EditTextHandler;->removeMessages(I)V

    goto :goto_0

    .line 152
    :cond_2
    if-ne v0, v2, :cond_0

    .line 153
    invoke-direct {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->isPermitted()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z

    if-eqz v3, :cond_0

    .line 154
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/vlingo/midas/gui/customviews/EditText;->firstTouch:Z

    .line 156
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    if-eqz v3, :cond_3

    .line 157
    iget-object v3, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    invoke-interface {v3, p0}, Lcom/vlingo/midas/gui/OnEditListener;->onEditStarted(Landroid/view/View;)V

    .line 159
    :cond_3
    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/customviews/EditText;->setEditUI(Z)V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 188
    invoke-super {p0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 190
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mPermitted:Z

    .line 191
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 193
    .local v0, "context":Landroid/content/Context;
    if-eqz p1, :cond_1

    .line 194
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mIsReceiverRegistered:Z

    if-nez v2, :cond_0

    .line 195
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 196
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "ResponseAxT9Info"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 197
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mMainReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 199
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mIsReceiverRegistered:Z

    .line 208
    .end local v1    # "filter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mIsReceiverRegistered:Z

    if-eqz v2, :cond_0

    .line 203
    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mMainReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 205
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mIsReceiverRegistered:Z

    goto :goto_0
.end method

.method public setOnEditListener(Lcom/vlingo/midas/gui/OnEditListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/midas/gui/OnEditListener;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mEditListener:Lcom/vlingo/midas/gui/OnEditListener;

    .line 99
    return-void
.end method

.method public setReadyToTouch()V
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/customviews/EditText;->setEditUI(Z)V

    .line 239
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->hideKeypad()V

    .line 240
    return-void
.end method

.method public showKeyapd()V
    .locals 3

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/customviews/EditText;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 292
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vlingo/midas/gui/customviews/EditText;->mSoftkeyReceiver:Landroid/os/ResultReceiver;

    invoke-virtual {v0, p0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    .line 293
    return-void
.end method
