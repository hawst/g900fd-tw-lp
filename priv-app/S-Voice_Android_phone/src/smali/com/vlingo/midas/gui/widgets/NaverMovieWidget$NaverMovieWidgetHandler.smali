.class Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;
.super Landroid/os/Handler;
.source "NaverMovieWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NaverMovieWidgetHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;

    .prologue
    .line 113
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 114
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 115
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 118
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget$NaverMovieWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;

    .line 119
    .local v0, "o":Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;
    if-eqz v0, :cond_1

    .line 120
    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mPosterImage:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->access$000(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, v0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mMoviePoster:Landroid/widget/ImageView;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mPosterImage:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->access$000(Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 122
    :cond_0
    iget-object v1, v0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mMoviePoster:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 123
    sget v1, Lcom/vlingo/midas/R$id;->text_Movie_Poster_Cover:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 125
    iget-object v1, v0, Lcom/vlingo/midas/gui/widgets/NaverMovieWidget;->mMoviePoster:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 127
    :cond_1
    return-void
.end method
