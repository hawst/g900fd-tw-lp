.class Lcom/vlingo/midas/gui/DialogBubble$3;
.super Landroid/view/View$AccessibilityDelegate;
.source "DialogBubble.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogBubble;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogBubble;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogBubble$3;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4
    .param p1, "host"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 226
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 227
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v2

    const v3, 0x8000

    if-ne v2, v3, :cond_1

    .line 229
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogBubble$3;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/DialogBubble;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/ConversationActivity;

    .line 230
    .local v1, "parent":Lcom/vlingo/midas/gui/ConversationActivity;
    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/ControlFragment;

    .line 234
    .local v0, "cf":Lcom/vlingo/midas/gui/ControlFragment;
    if-eqz v0, :cond_1

    .line 235
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->avoidWakeUpCommandTTS()V

    .line 237
    .end local v0    # "cf":Lcom/vlingo/midas/gui/ControlFragment;
    .end local v1    # "parent":Lcom/vlingo/midas/gui/ConversationActivity;
    :cond_1
    return-void
.end method
