.class Lcom/vlingo/midas/gui/DialogBubble$2;
.super Ljava/lang/Object;
.source "DialogBubble.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/DialogBubble;->initialize(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogBubble;

.field final synthetic val$text:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogBubble;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogBubble$2;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    iput-object p2, p0, Lcom/vlingo/midas/gui/DialogBubble$2;->val$text:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 209
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_0

    .line 210
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$2;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$400(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble$2;->val$text:Ljava/lang/String;

    const-string/jumbo v2, "\n "

    const-string/jumbo v3, "\n"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    :goto_0
    return-void

    .line 212
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_1

    .line 214
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$2;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$400(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble$2;->val$text:Ljava/lang/String;

    const-string/jumbo v2, "\n "

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$2;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$400(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogBubble$2;->val$text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogBubble$2;->this$0:Lcom/vlingo/midas/gui/DialogBubble;

    # getter for: Lcom/vlingo/midas/gui/DialogBubble;->bubbleWakeupText:Lcom/vlingo/midas/gui/customviews/TextView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogBubble;->access$400(Lcom/vlingo/midas/gui/DialogBubble;)Lcom/vlingo/midas/gui/customviews/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/TextView;->setVisibility(I)V

    goto :goto_0
.end method
