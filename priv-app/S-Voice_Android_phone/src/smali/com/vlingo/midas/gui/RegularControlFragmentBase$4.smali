.class Lcom/vlingo/midas/gui/RegularControlFragmentBase$4;
.super Ljava/lang/Object;
.source "RegularControlFragmentBase.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/RegularControlFragmentBase;->fadeInMicLayout(ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/RegularControlFragmentBase;)V
    .locals 0

    .prologue
    .line 942
    iput-object p1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$4;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 949
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$4;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->removeMicStatusTextForTutorial(Z)V

    .line 950
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$4;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 952
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$4;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 953
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$4;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 954
    iget-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$4;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    iget-object v0, v0, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->reco_idleBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentBase$4;->this$0:Lcom/vlingo/midas/gui/RegularControlFragmentBase;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/RegularControlFragmentBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->talkback_mic_button_idle:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 955
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 946
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 944
    return-void
.end method
