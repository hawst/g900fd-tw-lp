.class Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ControlFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ControlFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FocusChangeBroadcastReceiver"
.end annotation


# instance fields
.field private audioFocusLossTask:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/vlingo/midas/gui/ControlFragment;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/ControlFragment;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/ControlFragment;Lcom/vlingo/midas/gui/ControlFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/ControlFragment;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/ControlFragment$1;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/ControlFragment;)V

    return-void
.end method

.method private resetTask()V
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->audioFocusLossTask:Ljava/lang/Runnable;

    .line 206
    return-void
.end method

.method private runAudioFocusLossTask()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->audioFocusLossTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->audioFocusLossTask:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 211
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->resetTask()V

    .line 213
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 147
    const-string/jumbo v3, "com.vlingo.client.app.extra.FOCUS_CHANGE"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 148
    .local v0, "focusChange":I
    const-string/jumbo v3, "com.vlingo.client.app.extra.FOCUS_CHANGE_FROM"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, "focusChangeFrom":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 182
    :pswitch_1
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v3

    sget-object v4, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-ne v3, v4, :cond_2

    .line 183
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 185
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->runAudioFocusLossTask()V

    .line 186
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragment;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    move-result-object v3

    sget-object v4, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    if-eq v3, v4, :cond_3

    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    iget-boolean v3, v3, Lcom/vlingo/midas/gui/ControlFragment;->standardFlag:Z

    if-eq v3, v5, :cond_3

    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    # getter for: Lcom/vlingo/midas/gui/ControlFragment;->shouldFinish:Z
    invoke-static {v3}, Lcom/vlingo/midas/gui/ControlFragment;->access$300(Lcom/vlingo/midas/gui/ControlFragment;)Z

    move-result v3

    if-eq v3, v5, :cond_3

    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    # getter for: Lcom/vlingo/midas/gui/ControlFragment;->timesTTSPlayed:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/ControlFragment;->access$000(Lcom/vlingo/midas/gui/ControlFragment;)I

    move-result v3

    # getter for: Lcom/vlingo/midas/gui/ControlFragment;->maxTTSPlayCount:I
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragment;->access$100()I

    move-result v4

    if-lt v3, v4, :cond_5

    :cond_3
    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {}, Lcom/vlingo/midas/util/SViewCoverUtils;->isCoverGUISet()Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    instance-of v3, v3, Lcom/vlingo/midas/gui/ConversationActivity;

    if-eqz v3, :cond_5

    .line 188
    const-string/jumbo v3, "SVoice::ControlFragment"

    const-string/jumbo v4, "Resetting Finish SVoice timer in mini mode"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragment;->resetFinishMessage()V

    .line 191
    :cond_5
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    # getter for: Lcom/vlingo/midas/gui/ControlFragment;->toListening:Z
    invoke-static {v3}, Lcom/vlingo/midas/gui/ControlFragment;->access$400(Lcom/vlingo/midas/gui/ControlFragment;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 192
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    const/4 v4, 0x0

    # setter for: Lcom/vlingo/midas/gui/ControlFragment;->toListening:Z
    invoke-static {v3, v4}, Lcom/vlingo/midas/gui/ControlFragment;->access$402(Lcom/vlingo/midas/gui/ControlFragment;Z)Z

    goto :goto_0

    .line 158
    :pswitch_2
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isIUXComplete()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 159
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/ControlFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 160
    .local v2, "frActivity":Landroid/support/v4/app/FragmentActivity;
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_6

    .line 173
    .end local v2    # "frActivity":Landroid/support/v4/app/FragmentActivity;
    :cond_6
    sget-boolean v3, Lcom/vlingo/midas/gui/ControlFragment;->startTimer:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    # getter for: Lcom/vlingo/midas/gui/ControlFragment;->timesTTSPlayed:I
    invoke-static {v3}, Lcom/vlingo/midas/gui/ControlFragment;->access$000(Lcom/vlingo/midas/gui/ControlFragment;)I

    move-result v3

    # getter for: Lcom/vlingo/midas/gui/ControlFragment;->maxTTSPlayCount:I
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragment;->access$100()I

    move-result v4

    if-ge v3, v4, :cond_0

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-static {}, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen()Z

    move-result v3

    if-nez v3, :cond_0

    .line 174
    :cond_7
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    # getter for: Lcom/vlingo/midas/gui/ControlFragment;->countTimer:Lcom/vlingo/midas/gui/CountTimer;
    invoke-static {v3}, Lcom/vlingo/midas/gui/ControlFragment;->access$200(Lcom/vlingo/midas/gui/ControlFragment;)Lcom/vlingo/midas/gui/CountTimer;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 175
    iget-object v3, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/ControlFragment;

    # getter for: Lcom/vlingo/midas/gui/ControlFragment;->countTimer:Lcom/vlingo/midas/gui/CountTimer;
    invoke-static {v3}, Lcom/vlingo/midas/gui/ControlFragment;->access$200(Lcom/vlingo/midas/gui/ControlFragment;)Lcom/vlingo/midas/gui/CountTimer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/CountTimer;->start()Landroid/os/CountDownTimer;

    .line 176
    :cond_8
    sput-boolean v5, Lcom/vlingo/midas/gui/ControlFragment;->startTimer:Z

    goto/16 :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public setOnAudioFocusLossTask(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/vlingo/midas/gui/ControlFragment$FocusChangeBroadcastReceiver;->audioFocusLossTask:Ljava/lang/Runnable;

    .line 202
    return-void
.end method
