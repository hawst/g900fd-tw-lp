.class Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget$WeatherOneDayWidgetHandler;
.super Landroid/os/Handler;
.source "WeatherOneDayWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WeatherOneDayWidgetHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;

    .prologue
    .line 227
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 228
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget$WeatherOneDayWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 229
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 232
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget$WeatherOneDayWidgetHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;

    .line 233
    .local v0, "o":Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;
    if-eqz v0, :cond_0

    .line 236
    # invokes: Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->setValuesForcastWeather()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->access$000(Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;)V

    .line 237
    # invokes: Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->setTalkbackString()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;->access$100(Lcom/vlingo/midas/gui/widgets/WeatherOneDayWidget;)V

    .line 239
    :cond_0
    return-void
.end method
