.class Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;
.super Landroid/os/Handler;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DialogFragmentHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/DialogFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/gui/DialogFragment;

    .prologue
    .line 3063
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 3064
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 3065
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 3068
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 3083
    :cond_0
    :goto_0
    return-void

    .line 3074
    :pswitch_0
    const/16 v0, 0x65

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;->removeMessages(I)V

    .line 3075
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->vlingoFocus:Z
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$2100()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3076
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 3077
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 3078
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocusFromTouch()Z

    .line 3079
    const/4 v0, 0x0

    # setter for: Lcom/vlingo/midas/gui/DialogFragment;->vlingoFocus:Z
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragment;->access$2102(Z)Z

    goto :goto_0

    .line 3068
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
    .end packed-switch
.end method
