.class Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;
.super Ljava/lang/Object;
.source "AlwaysOnSettingsFragment.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    .line 207
    sget v0, Lcom/vlingo/midas/R$id;->always_accept_radio_btn:I

    if-ne p2, v0, :cond_1

    .line 208
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSoundDetectorSettingOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->toast_while_running_sound_detector_for_svoice:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 212
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    # invokes: Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->screenWhenDisagree()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->access$000(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V

    .line 224
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    # getter for: Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mUrl:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->access$100(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    # invokes: Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->screenWhenConfirmed()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->access$200(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V

    goto :goto_0

    .line 217
    :cond_1
    sget v0, Lcom/vlingo/midas/R$id;->always_decline_radio_btn:I

    if-ne p2, v0, :cond_2

    .line 218
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    # getter for: Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->mUrl:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->access$100(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    # invokes: Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->screenWhenConfirmed()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->access$200(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V

    goto :goto_0

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$2;->this$0:Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    # invokes: Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->screenWhenDisagree()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;->access$000(Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;)V

    goto :goto_0
.end method
