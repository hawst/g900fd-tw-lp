.class public Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;
.super Landroid/widget/RelativeLayout;
.source "ItemReviewDetailView.java"


# instance fields
.field private ratingView:Landroid/widget/RatingBar;

.field private textAuthorView:Landroid/widget/TextView;

.field private textDateView:Landroid/widget/TextView;

.field private textReviewBodyView:Landroid/widget/TextView;

.field private textTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public static create(Landroid/content/Context;Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;)Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "review"    # Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;

    .prologue
    .line 31
    sget v1, Lcom/vlingo/midas/R$layout;->item_local_search_detail_review:I

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;

    .line 32
    .local v0, "v":Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;
    iget-object v1, v0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->ratingView:Landroid/widget/RatingBar;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->getRating()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RatingBar;->setRating(F)V

    .line 33
    iget-object v1, v0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textTitleView:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    iget-object v1, v0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textAuthorView:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->author:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    iget-object v1, v0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textDateView:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->date:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    iget-object v1, v0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textReviewBodyView:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/vlingo/core/internal/localsearch/LocalSearchListing$Review;->body:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    return-object v0
.end method


# virtual methods
.method public getRatingView()Landroid/widget/RatingBar;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->ratingView:Landroid/widget/RatingBar;

    return-object v0
.end method

.method public getTextAuthorView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textAuthorView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTextDateView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textDateView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTextReviewBodyView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textReviewBodyView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTextTitleView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textTitleView:Landroid/widget/TextView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 42
    sget v0, Lcom/vlingo/midas/R$id;->rating:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->ratingView:Landroid/widget/RatingBar;

    .line 43
    sget v0, Lcom/vlingo/midas/R$id;->text_title:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textTitleView:Landroid/widget/TextView;

    .line 44
    sget v0, Lcom/vlingo/midas/R$id;->text_author:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textAuthorView:Landroid/widget/TextView;

    .line 45
    sget v0, Lcom/vlingo/midas/R$id;->text_date:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textDateView:Landroid/widget/TextView;

    .line 46
    sget v0, Lcom/vlingo/midas/R$id;->text_review_body:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/customviews/ItemReviewDetailView;->textReviewBodyView:Landroid/widget/TextView;

    .line 47
    return-void
.end method
