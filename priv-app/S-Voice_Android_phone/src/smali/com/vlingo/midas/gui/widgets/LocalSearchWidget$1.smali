.class Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;
.super Ljava/lang/Object;
.source "LocalSearchWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->initialize(Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 144
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->localSearchListings:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$100(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 145
    new-instance v0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;)V

    .line 146
    .local v0, "adapter":Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->mLocalSearchList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$300(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 148
    .end local v0    # "adapter":Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$ListAdapter;
    :cond_0
    return-void
.end method
