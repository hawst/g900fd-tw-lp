.class Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages$1;
.super Ljava/lang/Object;
.source "MultipleMessageReadoutWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;

.field final synthetic val$message:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;ILcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages$1;->this$1:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;

    iput p2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages$1;->val$position:I

    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages$1;->val$message:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 386
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 387
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 388
    const-string/jumbo v1, "id"

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages$1;->val$position:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 389
    const-string/jumbo v1, "message_type"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages$1;->val$message:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 390
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages$1;->this$1:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 391
    return-void
.end method
