.class Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$3;
.super Ljava/lang/Object;
.source "LocalSearchWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->getDetailLocalSearchView()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)V
    .locals 0

    .prologue
    .line 404
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$3;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 406
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "com.vlingo.core.internal.dialogmanager.AcceptedText"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 407
    .local v1, "in":Landroid/content/Intent;
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$3;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v3, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$500(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v3

    new-instance v4, Lcom/vlingo/core/internal/recognition/acceptedtext/LocalSearchAcceptedText;

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$3;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v5, v5, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->lsAdaptor:Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;
    invoke-static {v5}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$400(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/localsearch/LocalSearchAdaptor;->getCurrentSearchQuery()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;->RESERVE:Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;

    invoke-virtual {v6}, Lcom/vlingo/core/internal/recognition/acceptedtext/AcceptedText$ActionType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/vlingo/core/internal/recognition/acceptedtext/LocalSearchAcceptedText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v1, v4}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 410
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 411
    .local v0, "action":Landroid/content/Intent;
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$3;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->listing:Lcom/vlingo/core/internal/localsearch/LocalSearchListing;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->access$800(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;)Lcom/vlingo/core/internal/localsearch/LocalSearchListing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/localsearch/LocalSearchListing;->getReserveUrl()Ljava/lang/String;

    move-result-object v2

    .line 412
    .local v2, "url":Ljava/lang/String;
    invoke-static {v2}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "http://"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 413
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "http://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 415
    :cond_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 416
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener$3;->this$1:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;

    iget-object v3, v3, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget$DetailsClickListener;->this$0:Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;->access$600(Lcom/vlingo/midas/gui/widgets/LocalSearchWidget;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 417
    return-void
.end method
