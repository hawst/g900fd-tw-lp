.class Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$3;
.super Ljava/lang/Object;
.source "EditWhatYouSaidHelpActivity.java"

# interfaces
.implements Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V
    .locals 0

    .prologue
    .line 726
    iput-object p1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$3;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private onBoth(Z)V
    .locals 0
    .param p1, "shouldShow"    # Z

    .prologue
    .line 728
    if-eqz p1, :cond_0

    .line 729
    :cond_0
    return-void
.end method


# virtual methods
.method public onRecognitionStarted()V
    .locals 1

    .prologue
    .line 744
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v0, :cond_0

    .line 745
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/Widget;->onRecognitionStarted()V

    .line 747
    :cond_0
    return-void
.end method

.method public onStartSpotting()V
    .locals 1

    .prologue
    .line 733
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$3;->onBoth(Z)V

    .line 734
    return-void
.end method

.method public onStopSpotting()V
    .locals 1

    .prologue
    .line 738
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$3;->onBoth(Z)V

    .line 739
    return-void
.end method
