.class public Lcom/vlingo/midas/gui/DataCheckActivity;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "DataCheckActivity.java"


# static fields
.field public static DIALOG_PROCESSED:Ljava/lang/String; = null

.field private static final STEP_BACKGROUND_DATA_CHECK:I = 0x0

.field private static final STEP_CONVERSATION:I = 0x3

.field private static final STEP_MOBILE_DATA_CHECK:I = 0x1

.field private static final STEP_WLAN_DATA_CHECK:I = 0x2

.field private static leftPadding:I

.field private static mNetworkAlertDlg:Landroid/app/AlertDialog;


# instance fields
.field private STEP_NEXT:I

.field builder:Landroid/app/AlertDialog$Builder;

.field dont_ask:Landroid/widget/CheckBox;

.field private mDensity:F

.field private oldIntent:Landroid/content/Intent;

.field final ssu:Lcom/vlingo/core/internal/util/SystemServicesUtil;

.field textView:Landroid/widget/TextView;

.field view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/gui/DataCheckActivity;->mNetworkAlertDlg:Landroid/app/AlertDialog;

    .line 30
    const/4 v0, 0x0

    sput v0, Lcom/vlingo/midas/gui/DataCheckActivity;->leftPadding:I

    .line 32
    const-string/jumbo v0, "dialogProcessed"

    sput-object v0, Lcom/vlingo/midas/gui/DataCheckActivity;->DIALOG_PROCESSED:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->oldIntent:Landroid/content/Intent;

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->STEP_NEXT:I

    .line 39
    new-instance v0, Lcom/vlingo/core/internal/util/SystemServicesUtil;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/util/SystemServicesUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->ssu:Lcom/vlingo/core/internal/util/SystemServicesUtil;

    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/DataCheckActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DataCheckActivity;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->dismissDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/DataCheckActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DataCheckActivity;

    .prologue
    .line 20
    iget v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->STEP_NEXT:I

    return v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/DataCheckActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DataCheckActivity;
    .param p1, "x1"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DataCheckActivity;->recreateView(I)V

    return-void
.end method

.method private createDialog()V
    .locals 3

    .prologue
    .line 104
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    .line 105
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x103012b

    invoke-direct {v1, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    .line 110
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/vlingo/midas/gui/DataCheckActivity$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/DataCheckActivity$1;-><init>(Lcom/vlingo/midas/gui/DataCheckActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 122
    return-void

    .line 108
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private createMobileDataCheckDialog()V
    .locals 5

    .prologue
    .line 180
    sget v0, Lcom/vlingo/midas/R$layout;->dialog_voice_prompt_confirmation:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->view:Landroid/view/View;

    .line 181
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->view:Landroid/view/View;

    sget v1, Lcom/vlingo/midas/R$id;->text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->textView:Landroid/widget/TextView;

    .line 182
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->view:Landroid/view/View;

    sget v1, Lcom/vlingo/midas/R$id;->dont_ask:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    .line 183
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->popup_connect_network_body:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v0

    const/high16 v1, 0x41000000    # 8.0f

    iget v2, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->mDensity:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    sput v0, Lcom/vlingo/midas/gui/DataCheckActivity;->leftPadding:I

    .line 185
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    sget v1, Lcom/vlingo/midas/gui/DataCheckActivity;->leftPadding:I

    iget-object v2, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 187
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 188
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->popup_connect_network_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 190
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_cancel:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/gui/DataCheckActivity$4;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/DataCheckActivity$4;-><init>(Lcom/vlingo/midas/gui/DataCheckActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 200
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_ok:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/gui/DataCheckActivity$5;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/DataCheckActivity$5;-><init>(Lcom/vlingo/midas/gui/DataCheckActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 217
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->view:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 218
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/gui/DataCheckActivity;->mNetworkAlertDlg:Landroid/app/AlertDialog;

    .line 219
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    sget-object v0, Lcom/vlingo/midas/gui/DataCheckActivity;->mNetworkAlertDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 221
    :cond_0
    return-void
.end method

.method private createWlanDataCheckDialog()V
    .locals 5

    .prologue
    .line 133
    sget v0, Lcom/vlingo/midas/R$layout;->dialog_voice_prompt_confirmation:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->view:Landroid/view/View;

    .line 135
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->view:Landroid/view/View;

    sget v1, Lcom/vlingo/midas/R$id;->text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->textView:Landroid/widget/TextView;

    .line 136
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->view:Landroid/view/View;

    sget v1, Lcom/vlingo/midas/R$id;->dont_ask:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    .line 137
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->popup_connect_network_body_wifionly:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 140
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v0

    const/high16 v1, 0x41000000    # 8.0f

    iget v2, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->mDensity:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    sput v0, Lcom/vlingo/midas/gui/DataCheckActivity;->leftPadding:I

    .line 141
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    sget v1, Lcom/vlingo/midas/gui/DataCheckActivity;->leftPadding:I

    iget-object v2, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 143
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->dont_ask:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->popup_connect_network_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 146
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_cancel:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/gui/DataCheckActivity$2;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/DataCheckActivity$2;-><init>(Lcom/vlingo/midas/gui/DataCheckActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 156
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->voice_prompt_confirmation_ok:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/vlingo/midas/gui/DataCheckActivity$3;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/DataCheckActivity$3;-><init>(Lcom/vlingo/midas/gui/DataCheckActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 173
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->view:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 174
    iget-object v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/gui/DataCheckActivity;->mNetworkAlertDlg:Landroid/app/AlertDialog;

    .line 175
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    sget-object v0, Lcom/vlingo/midas/gui/DataCheckActivity;->mNetworkAlertDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 177
    :cond_0
    return-void
.end method

.method private dismissDialog()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125
    sget-object v0, Lcom/vlingo/midas/gui/DataCheckActivity;->mNetworkAlertDlg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 126
    iput-object v1, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->builder:Landroid/app/AlertDialog$Builder;

    .line 127
    sget-object v0, Lcom/vlingo/midas/gui/DataCheckActivity;->mNetworkAlertDlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 128
    sput-object v1, Lcom/vlingo/midas/gui/DataCheckActivity;->mNetworkAlertDlg:Landroid/app/AlertDialog;

    .line 130
    :cond_0
    return-void
.end method

.method private recreateView(I)V
    .locals 1
    .param p1, "step"    # I

    .prologue
    const/4 v0, 0x3

    .line 72
    packed-switch p1, :pswitch_data_0

    .line 101
    :goto_0
    return-void

    .line 86
    :pswitch_0
    iput v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->STEP_NEXT:I

    .line 87
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->createDialog()V

    .line 88
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->createWlanDataCheckDialog()V

    goto :goto_0

    .line 92
    :pswitch_1
    iput v0, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->STEP_NEXT:I

    .line 93
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->createDialog()V

    .line 94
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->createMobileDataCheckDialog()V

    goto :goto_0

    .line 98
    :pswitch_2
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->returnConversationActivity()V

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private returnConversationActivity()V
    .locals 3

    .prologue
    .line 242
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 243
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "intent"

    iget-object v2, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->oldIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 244
    sget-object v1, Lcom/vlingo/midas/gui/DataCheckActivity;->DIALOG_PROCESSED:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 245
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->dismissDialog()V

    .line 246
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DataCheckActivity;->startActivity(Landroid/content/Intent;)V

    .line 247
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->finish()V

    .line 248
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 45
    .local v1, "titleBar":Landroid/app/ActionBar;
    if-eqz v1, :cond_0

    .line 46
    sget v2, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/DataCheckActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    iput v2, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->mDensity:F

    .line 49
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 50
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "intent"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    iput-object v2, p0, Lcom/vlingo/midas/gui/DataCheckActivity;->oldIntent:Landroid/content/Intent;

    .line 51
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 234
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 235
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->dismissDialog()V

    .line 236
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->finish()V

    .line 238
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/ui/VLActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 227
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onPause()V

    .line 228
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->dismissDialog()V

    .line 229
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 56
    sget v0, Lcom/vlingo/midas/R$layout;->main:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DataCheckActivity;->setContentView(I)V

    .line 61
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DataCheckActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isWifiOnly(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/DataCheckActivity;->recreateView(I)V

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/DataCheckActivity;->recreateView(I)V

    goto :goto_0
.end method
