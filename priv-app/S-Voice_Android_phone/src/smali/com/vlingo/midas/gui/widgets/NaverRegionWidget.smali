.class public Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "NaverRegionWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/midas/naver/NaverAdaptor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private btnFindRoute:Landroid/widget/Button;

.field private btnMore:Landroid/widget/Button;

.field private item:Lcom/vlingo/midas/naver/NaverRegionItem;

.field private final mContext:Landroid/content/Context;

.field private mMainAddress:Landroid/widget/TextView;

.field private mMap:Landroid/widget/ImageView;

.field private mNewAddress:Landroid/widget/TextView;

.field private mZipCode:Landroid/widget/TextView;

.field private mhandler:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

.field private wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mContext:Landroid/content/Context;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;)Lcom/vlingo/midas/naver/NaverRegionItem;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mMap:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 5
    .param p1, "object"    # Lcom/vlingo/midas/naver/NaverAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const/16 v4, 0x8

    .line 106
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    .line 107
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getVVSActionHandlerListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mhandler:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    .line 108
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->wAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v0

    .line 109
    .local v0, "result":Lcom/vlingo/midas/naver/NaverXMLParser;
    new-instance v2, Lcom/vlingo/midas/naver/NaverRegionItem;

    iget-object v1, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->itemList:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Hashtable;

    invoke-direct {v2, v1}, Lcom/vlingo/midas/naver/NaverRegionItem;-><init>(Ljava/util/Hashtable;)V

    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    .line 111
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mMainAddress:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverRegionItem;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverRegionItem;->getMatchAddress()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    sget v1, Lcom/vlingo/midas/R$id;->region_container_newaddress:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 116
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverRegionItem;->getZipCode()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    sget v1, Lcom/vlingo/midas/R$id;->region_container_zipcode:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 121
    :goto_1
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 181
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 182
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverRegionItem;->getRouteUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 187
    :goto_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverRegionItem;->getMoreUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 202
    :cond_0
    :goto_3
    return-void

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mNewAddress:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverRegionItem;->getMatchAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 119
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mZipCode:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v2}, Lcom/vlingo/midas/naver/NaverRegionItem;->getZipCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 184
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnFindRoute:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 185
    sget v1, Lcom/vlingo/midas/R$id;->divider:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 189
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnMore:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 190
    sget v1, Lcom/vlingo/midas/R$id;->divider:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 195
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverRegionItem;->getRouteUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 197
    :goto_4
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverRegionItem;->getMoreUrl()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 198
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnMore:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3

    .line 196
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnFindRoute:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_4
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 34
    check-cast p1, Lcom/vlingo/midas/naver/NaverAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 58
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 59
    .local v0, "sIntent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnFindRoute:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverRegionItem;->getRouteUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 61
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverRegionItem;->getRouteUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 70
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 72
    :cond_0
    return-void

    .line 62
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnMore:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverRegionItem;->getMoreUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->item:Lcom/vlingo/midas/naver/NaverRegionItem;

    invoke-virtual {v1}, Lcom/vlingo/midas/naver/NaverRegionItem;->getMoreUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 89
    sget v0, Lcom/vlingo/midas/R$id;->text_Region_Main_Address:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mMainAddress:Landroid/widget/TextView;

    .line 90
    sget v0, Lcom/vlingo/midas/R$id;->image_Region_Map:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mMap:Landroid/widget/ImageView;

    .line 91
    sget v0, Lcom/vlingo/midas/R$id;->text_Region_New_Address:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mNewAddress:Landroid/widget/TextView;

    .line 92
    sget v0, Lcom/vlingo/midas/R$id;->text_Region_Zipcode:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->mZipCode:Landroid/widget/TextView;

    .line 93
    sget v0, Lcom/vlingo/midas/R$id;->btn_Region_Route:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnFindRoute:Landroid/widget/Button;

    .line 94
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnFindRoute:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    sget v0, Lcom/vlingo/midas/R$id;->btn_Region_More:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnMore:Landroid/widget/Button;

    .line 96
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnMore:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    return-void
.end method

.method public retire()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 77
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->retire()V

    .line 79
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnMore:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverRegionWidget;->btnFindRoute:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 81
    return-void
.end method
