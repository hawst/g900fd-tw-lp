.class Lcom/vlingo/midas/gui/DialogFragment$6;
.super Ljava/lang/Object;
.source "DialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/DialogFragment;->doScrollAnim(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogFragment;

.field final synthetic val$height:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;I)V
    .locals 0

    .prologue
    .line 1534
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iput p2, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->val$height:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private avoidScrollForBuffer(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)Z
    .locals 2
    .param p1, "bubbleView"    # Landroid/view/View;
    .param p2, "checkForBuffer"    # Landroid/view/View;
    .param p3, "dummyView"    # Landroid/view/View;
    .param p4, "youCanSayWidget"    # Landroid/view/View;

    .prologue
    .line 1935
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-ne v0, v1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/vlingo/midas/R$id;->buffer:I

    if-ne v0, v1, :cond_0

    if-eqz p3, :cond_0

    instance-of v0, p3, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    instance-of v0, p4, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;

    if-nez v0, :cond_1

    instance-of v0, p4, Lcom/vlingo/midas/gui/Widget;

    if-nez v0, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragment$6;->checkForWakeUpBubble(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/vlingo/midas/R$id;->buffer:I

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkForUserBubble(Landroid/view/View;)Z
    .locals 2
    .param p1, "suspectedVlingo3"    # Landroid/view/View;

    .prologue
    .line 1914
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-eq v0, v1, :cond_0

    check-cast p1, Lcom/vlingo/midas/gui/DialogBubble;

    .end local p1    # "suspectedVlingo3":Landroid/view/View;
    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getId()I

    move-result v0

    sget v1, Lcom/vlingo/midas/R$id;->dialog_bubble_user_s:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkForVlingo(Landroid/view/View;)Z
    .locals 2
    .param p1, "suspectedVlingo"    # Landroid/view/View;

    .prologue
    .line 1926
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-eq v0, v1, :cond_0

    check-cast p1, Lcom/vlingo/midas/gui/DialogBubble;

    .end local p1    # "suspectedVlingo":Landroid/view/View;
    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getId()I

    move-result v0

    sget v1, Lcom/vlingo/midas/R$id;->dialog_bubble_vlingo_s:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkForVlingoError(Lcom/vlingo/midas/gui/DialogBubble;)Z
    .locals 2
    .param p1, "dialogBubble"    # Lcom/vlingo/midas/gui/DialogBubble;

    .prologue
    .line 1901
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    sget v1, Lcom/vlingo/midas/R$string;->reco_error_0:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    sget v1, Lcom/vlingo/midas/R$string;->reco_error_1:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    sget v1, Lcom/vlingo/midas/R$string;->reco_error_2:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    sget v1, Lcom/vlingo/midas/R$string;->reco_error_3:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    sget v1, Lcom/vlingo/midas/R$string;->reco_error_4:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    sget v1, Lcom/vlingo/midas/R$string;->reco_error_5:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    sget v1, Lcom/vlingo/midas/R$string;->reco_error_6:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    sget v1, Lcom/vlingo/midas/R$string;->reco_error_7:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    sget v1, Lcom/vlingo/midas/R$string;->reco_error_8:I

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkForWakeUpBubble(Landroid/view/View;)Z
    .locals 2
    .param p1, "suspectedVlingo3"    # Landroid/view/View;

    .prologue
    .line 1920
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/vlingo/midas/gui/DialogBubble;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    if-eq v0, v1, :cond_0

    check-cast p1, Lcom/vlingo/midas/gui/DialogBubble;

    .end local p1    # "suspectedVlingo3":Landroid/view/View;
    invoke-virtual {p1}, Lcom/vlingo/midas/gui/DialogBubble;->getId()I

    move-result v0

    sget v1, Lcom/vlingo/midas/R$id;->dialog_bubble_wakeup_s:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private doScrollAnimForKPhone(I)V
    .locals 45
    .param p1, "height"    # I

    .prologue
    .line 1551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->isScrollingDone:Z
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$500(Lcom/vlingo/midas/gui/DialogFragment;)Z

    move-result v41

    if-nez v41, :cond_7

    .line 1552
    const/16 v21, 0x1

    .line 1553
    .local v21, "overScrollWidget":Z
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    .line 1554
    .local v9, "dialogFragment1":Lcom/vlingo/midas/gui/DialogFragment;
    const/16 v20, 0x0

    .line 1555
    .local v20, "numViews":I
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    if-eqz v41, :cond_0

    .line 1556
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v20

    .line 1557
    :cond_0
    const/16 v30, 0x0

    .line 1558
    .local v30, "userBubble":Landroid/view/View;
    const/16 v36, 0x0

    .line 1559
    .local v36, "vlingoBubble":Landroid/view/View;
    const/16 v34, 0x0

    .line 1560
    .local v34, "viewToChange1":Landroid/view/View;
    const/4 v3, 0x0

    .line 1561
    .local v3, "bubbleView":Landroid/view/View;
    const/4 v10, 0x0

    .line 1562
    .local v10, "doNothing":Z
    const/16 v18, 0x0

    .line 1563
    .local v18, "netWorkProblem":Z
    const/16 v28, 0x0

    .line 1564
    .local v28, "tapScreen":Z
    const/16 v16, 0x0

    .line 1565
    .local v16, "moreButton":Z
    const/16 v22, 0x0

    .line 1566
    .local v22, "scroll":Z
    const/16 v19, 0x0

    .line 1567
    .local v19, "noScroll":Z
    const/16 v32, 0x0

    .line 1568
    .local v32, "userScroll":Z
    const/16 v39, 0x0

    .line 1569
    .local v39, "vlingoScroll":Z
    const/16 v38, 0x0

    .line 1570
    .local v38, "vlingoNewBubble":Landroid/view/View;
    const/16 v31, -0x1

    .line 1571
    .local v31, "userBubbleIndex":I
    const/16 v37, -0x1

    .line 1576
    .local v37, "vlingoBubbleIndex":I
    add-int/lit8 v13, v20, -0x1

    .local v13, "i":I
    :goto_0
    if-ltz v13, :cond_2

    .line 1577
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1578
    add-int/lit8 v41, v20, -0x1

    move/from16 v0, v41

    if-ne v13, v0, :cond_1

    .line 1580
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v41

    if-eqz v41, :cond_1

    .line 1582
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->setShadowWhileNotAnim:Z

    move/from16 v41, v0

    if-nez v41, :cond_1

    .line 1583
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iput-object v3, v0, Lcom/vlingo/midas/gui/DialogFragment;->ShadowView:Landroid/view/View;

    .line 1591
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v41

    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v41

    if-eqz v41, :cond_b

    .line 1592
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, 0x2

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1593
    .local v5, "buffer":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v23

    .line 1594
    .local v23, "suspectedVlingo":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x2

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v24

    .line 1595
    .local v24, "suspectedVlingo1":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, 0x3

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v25

    .line 1596
    .local v25, "suspectedVlingo3":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, 0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v26

    .line 1597
    .local v26, "suspectedVlingo4":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, 0x4

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v27

    .line 1598
    .local v27, "suspectedVlingo5":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$700(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v41 .. v42}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->setFocusable(Z)V

    .line 1599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1600
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v41 .. v42}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 1601
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/vlingo/midas/gui/DialogFragment$6;->checkForVlingo(Landroid/view/View;)Z

    move-result v41

    if-eqz v41, :cond_b

    .line 1602
    # setter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {v3}, Lcom/vlingo/midas/gui/DialogFragment;->access$802(Landroid/view/View;)Landroid/view/View;

    move-object v8, v3

    .line 1603
    check-cast v8, Lcom/vlingo/midas/gui/DialogBubble;

    .line 1604
    .local v8, "dialogBubble":Lcom/vlingo/midas/gui/DialogBubble;
    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v41

    if-eqz v41, :cond_9

    .line 1605
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/vlingo/midas/gui/DialogFragment$6;->checkForVlingoError(Lcom/vlingo/midas/gui/DialogBubble;)Z

    move-result v41

    if-eqz v41, :cond_9

    .line 1606
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    const/16 v42, 0x1

    invoke-virtual/range {v41 .. v42}, Landroid/view/View;->setFocusable(Z)V

    .line 1607
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->requestFocus()Z

    .line 1608
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->requestFocusFromTouch()Z

    .line 1744
    .end local v5    # "buffer":Landroid/view/View;
    .end local v8    # "dialogBubble":Lcom/vlingo/midas/gui/DialogBubble;
    .end local v23    # "suspectedVlingo":Landroid/view/View;
    .end local v24    # "suspectedVlingo1":Landroid/view/View;
    .end local v25    # "suspectedVlingo3":Landroid/view/View;
    .end local v26    # "suspectedVlingo4":Landroid/view/View;
    .end local v27    # "suspectedVlingo5":Landroid/view/View;
    :cond_2
    :goto_1
    add-int/lit8 v13, v20, -0x1

    :goto_2
    if-ltz v13, :cond_5

    .line 1745
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1746
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v23

    .line 1747
    .restart local v23    # "suspectedVlingo":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v41

    const-string/jumbo v42, "audio"

    invoke-virtual/range {v41 .. v42}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    .line 1749
    .local v2, "audioManager":Landroid/media/AudioManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v41

    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v41

    if-eqz v41, :cond_23

    if-eqz v3, :cond_3

    instance-of v0, v3, Lcom/vlingo/midas/gui/widgets/MusicWidget;

    move/from16 v41, v0

    if-nez v41, :cond_4

    :cond_3
    invoke-virtual {v2}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v41

    if-eqz v41, :cond_23

    .line 1751
    :cond_4
    add-int/lit8 v41, v20, -0x4

    move/from16 v0, v41

    if-lt v13, v0, :cond_22

    .line 1752
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment$6;->checkForVlingo(Landroid/view/View;)Z

    move-result v41

    if-eqz v41, :cond_23

    .line 1753
    # setter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static/range {v23 .. v23}, Lcom/vlingo/midas/gui/DialogFragment;->access$802(Landroid/view/View;)Landroid/view/View;

    move-object/from16 v8, v23

    .line 1754
    check-cast v8, Lcom/vlingo/midas/gui/DialogBubble;

    .line 1755
    .restart local v8    # "dialogBubble":Lcom/vlingo/midas/gui/DialogBubble;
    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v41

    if-eqz v41, :cond_20

    .line 1756
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/vlingo/midas/gui/DialogFragment$6;->checkForVlingoError(Lcom/vlingo/midas/gui/DialogBubble;)Z

    move-result v41

    if-eqz v41, :cond_20

    .line 1757
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    const/16 v42, 0x1

    invoke-virtual/range {v41 .. v42}, Landroid/view/View;->setFocusable(Z)V

    .line 1758
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->requestFocus()Z

    .line 1759
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->requestFocusFromTouch()Z

    .line 1788
    .end local v2    # "audioManager":Landroid/media/AudioManager;
    .end local v8    # "dialogBubble":Lcom/vlingo/midas/gui/DialogBubble;
    .end local v23    # "suspectedVlingo":Landroid/view/View;
    :cond_5
    :goto_3
    const/4 v12, 0x0

    .line 1789
    .local v12, "helpChoiceWidget":Landroid/view/View;
    add-int/lit8 v13, v20, -0x1

    :goto_4
    if-ltz v13, :cond_6

    .line 1790
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 1791
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x3

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1792
    .local v6, "bufferView":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x2

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1793
    .local v8, "dialogBubble":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v17

    .line 1794
    .local v17, "myDummyView":Landroid/view/View;
    if-eqz v12, :cond_24

    invoke-virtual {v12}, Landroid/view/View;->getId()I

    move-result v41

    sget v42, Lcom/vlingo/midas/R$id;->buffer:I

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_24

    if-eqz v6, :cond_24

    instance-of v0, v6, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    move/from16 v41, v0

    if-eqz v41, :cond_24

    if-eqz v8, :cond_24

    instance-of v0, v8, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    move/from16 v41, v0

    if-eqz v41, :cond_24

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/DialogFragment$6;->checkForWakeUpBubble(Landroid/view/View;)Z

    move-result v41

    if-eqz v41, :cond_24

    .line 1798
    move-object v12, v6

    .line 1799
    const/16 v21, 0x0

    .line 1812
    .end local v6    # "bufferView":Landroid/view/View;
    .end local v8    # "dialogBubble":Landroid/view/View;
    .end local v17    # "myDummyView":Landroid/view/View;
    :cond_6
    :goto_5
    const/16 v41, 0x1

    move/from16 v0, v16

    move/from16 v1, v41

    if-ne v0, v1, :cond_26

    if-eqz v30, :cond_26

    .line 1813
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getTop()I

    move-result v43

    add-int/lit8 v43, v43, -0xa

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    .line 1815
    const/16 v16, 0x0

    .line 1872
    .end local v3    # "bubbleView":Landroid/view/View;
    .end local v9    # "dialogFragment1":Lcom/vlingo/midas/gui/DialogFragment;
    .end local v10    # "doNothing":Z
    .end local v12    # "helpChoiceWidget":Landroid/view/View;
    .end local v13    # "i":I
    .end local v16    # "moreButton":Z
    .end local v18    # "netWorkProblem":Z
    .end local v19    # "noScroll":Z
    .end local v20    # "numViews":I
    .end local v21    # "overScrollWidget":Z
    .end local v22    # "scroll":Z
    .end local v28    # "tapScreen":Z
    .end local v30    # "userBubble":Landroid/view/View;
    .end local v31    # "userBubbleIndex":I
    .end local v32    # "userScroll":Z
    .end local v34    # "viewToChange1":Landroid/view/View;
    .end local v36    # "vlingoBubble":Landroid/view/View;
    .end local v37    # "vlingoBubbleIndex":I
    .end local v38    # "vlingoNewBubble":Landroid/view/View;
    .end local v39    # "vlingoScroll":Z
    :cond_7
    :goto_6
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v41

    if-eqz v41, :cond_8

    .line 1875
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/DialogFragment;->setShadowWhileNotAnim:Z

    move/from16 v41, v0

    if-nez v41, :cond_8

    .line 1878
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    const/16 v42, 0x1

    move/from16 v0, v42

    move-object/from16 v1, v41

    iput v0, v1, Lcom/vlingo/midas/gui/DialogFragment;->shouldContinueBelow:I

    .line 1879
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->hideShadowBottom()V

    .line 1880
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$1200(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    move-result-object v41

    const/16 v42, 0x4

    invoke-virtual/range {v41 .. v42}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->removeMessages(I)V

    .line 1881
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$1200(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    move-result-object v41

    const/16 v42, 0x4

    const-wide/16 v43, 0x3e8

    invoke-virtual/range {v41 .. v44}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1884
    :cond_8
    return-void

    .line 1613
    .restart local v3    # "bubbleView":Landroid/view/View;
    .restart local v5    # "buffer":Landroid/view/View;
    .local v8, "dialogBubble":Lcom/vlingo/midas/gui/DialogBubble;
    .restart local v9    # "dialogFragment1":Lcom/vlingo/midas/gui/DialogFragment;
    .restart local v10    # "doNothing":Z
    .restart local v13    # "i":I
    .restart local v16    # "moreButton":Z
    .restart local v18    # "netWorkProblem":Z
    .restart local v19    # "noScroll":Z
    .restart local v20    # "numViews":I
    .restart local v21    # "overScrollWidget":Z
    .restart local v22    # "scroll":Z
    .restart local v23    # "suspectedVlingo":Landroid/view/View;
    .restart local v24    # "suspectedVlingo1":Landroid/view/View;
    .restart local v25    # "suspectedVlingo3":Landroid/view/View;
    .restart local v26    # "suspectedVlingo4":Landroid/view/View;
    .restart local v27    # "suspectedVlingo5":Landroid/view/View;
    .restart local v28    # "tapScreen":Z
    .restart local v30    # "userBubble":Landroid/view/View;
    .restart local v31    # "userBubbleIndex":I
    .restart local v32    # "userScroll":Z
    .restart local v34    # "viewToChange1":Landroid/view/View;
    .restart local v36    # "vlingoBubble":Landroid/view/View;
    .restart local v37    # "vlingoBubbleIndex":I
    .restart local v38    # "vlingoNewBubble":Landroid/view/View;
    .restart local v39    # "vlingoScroll":Z
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$900(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    move-result-object v41

    if-eqz v41, :cond_a

    .line 1614
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$900(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    move-result-object v41

    const/16 v42, 0x65

    invoke-virtual/range {v41 .. v42}, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;->removeMessages(I)V

    .line 1615
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$900(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    move-result-object v41

    const/16 v42, 0x65

    const-wide/16 v43, 0x1f4

    invoke-virtual/range {v41 .. v44}, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 1618
    :cond_a
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    const/16 v42, 0x1

    invoke-virtual/range {v41 .. v42}, Landroid/view/View;->setFocusable(Z)V

    .line 1619
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->requestFocus()Z

    .line 1620
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->requestFocusFromTouch()Z

    goto/16 :goto_1

    .line 1626
    .end local v5    # "buffer":Landroid/view/View;
    .end local v8    # "dialogBubble":Lcom/vlingo/midas/gui/DialogBubble;
    .end local v23    # "suspectedVlingo":Landroid/view/View;
    .end local v24    # "suspectedVlingo1":Landroid/view/View;
    .end local v25    # "suspectedVlingo3":Landroid/view/View;
    .end local v26    # "suspectedVlingo4":Landroid/view/View;
    .end local v27    # "suspectedVlingo5":Landroid/view/View;
    :cond_b
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, 0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 1627
    .local v7, "checkForBuffer":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 1628
    .local v11, "dummyView":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x2

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v40

    .line 1629
    .local v40, "youCanSayWidget":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-direct {v0, v3, v7, v11, v1}, Lcom/vlingo/midas/gui/DialogFragment$6;->avoidScrollForBuffer(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)Z

    move-result v41

    if-eqz v41, :cond_c

    .line 1630
    const/16 v19, 0x1

    .line 1631
    goto/16 :goto_1

    .line 1634
    :cond_c
    if-eqz v3, :cond_d

    instance-of v0, v3, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v41, v0

    if-eqz v41, :cond_d

    move-object/from16 v41, v3

    check-cast v41, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v41

    sget-object v42, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_d

    .line 1635
    const/16 v32, 0x1

    .line 1636
    move-object/from16 v30, v3

    .line 1637
    move/from16 v31, v13

    .line 1638
    goto/16 :goto_1

    .line 1641
    :cond_d
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/vlingo/midas/gui/DialogFragment$6;->checkForVlingo(Landroid/view/View;)Z

    move-result v41

    if-eqz v41, :cond_e

    move-object v8, v3

    .line 1642
    check-cast v8, Lcom/vlingo/midas/gui/DialogBubble;

    .line 1643
    .restart local v8    # "dialogBubble":Lcom/vlingo/midas/gui/DialogBubble;
    invoke-virtual {v8}, Lcom/vlingo/midas/gui/DialogBubble;->getText()Ljava/lang/String;

    move-result-object v41

    if-eqz v41, :cond_e

    .line 1644
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/vlingo/midas/gui/DialogFragment$6;->checkForVlingoError(Lcom/vlingo/midas/gui/DialogBubble;)Z

    move-result v41

    if-eqz v41, :cond_e

    .line 1645
    const/16 v39, 0x1

    .line 1646
    move-object/from16 v38, v3

    .line 1647
    goto/16 :goto_1

    .line 1652
    .end local v8    # "dialogBubble":Lcom/vlingo/midas/gui/DialogBubble;
    :cond_e
    instance-of v0, v3, Lcom/vlingo/midas/gui/widgets/ButtonWidget;

    move/from16 v41, v0

    if-eqz v41, :cond_17

    .line 1653
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, 0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v33

    .line 1654
    .local v33, "viewToChange":Landroid/view/View;
    move-object/from16 v34, v33

    .line 1655
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v35

    .line 1656
    .local v35, "vlingo":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x2

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v29

    .line 1657
    .local v29, "user":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, 0x2

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1658
    .restart local v5    # "buffer":Landroid/view/View;
    add-int/lit8 v14, v20, -0x1

    .local v14, "j":I
    :goto_7
    if-lt v14, v13, :cond_11

    .line 1659
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v14}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1660
    .local v4, "bubble_wake_up":Landroid/view/View;
    instance-of v0, v4, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v41, v0

    if-eqz v41, :cond_f

    check-cast v4, Lcom/vlingo/midas/gui/DialogBubble;

    .end local v4    # "bubble_wake_up":Landroid/view/View;
    invoke-virtual {v4}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v41

    sget-object v42, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_10

    :cond_f
    if-eqz v33, :cond_12

    invoke-virtual/range {v33 .. v33}, Landroid/view/View;->getMeasuredHeight()I

    move-result v41

    const/16 v42, 0x165

    move/from16 v0, v41

    move/from16 v1, v42

    if-le v0, v1, :cond_12

    .line 1661
    :cond_10
    const/16 v22, 0x1

    .line 1666
    :cond_11
    if-eqz v5, :cond_13

    instance-of v0, v5, Lcom/vlingo/midas/gui/widgets/WolframAlphaWidget;

    move/from16 v41, v0

    if-eqz v41, :cond_13

    .line 1667
    const/16 v16, 0x1

    .line 1669
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x2

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v30

    .line 1670
    goto/16 :goto_1

    .line 1658
    :cond_12
    add-int/lit8 v14, v14, -0x1

    goto :goto_7

    .line 1673
    :cond_13
    if-nez v22, :cond_15

    .line 1674
    if-eqz v33, :cond_14

    invoke-virtual/range {v33 .. v33}, Landroid/view/View;->getId()I

    move-result v41

    sget v42, Lcom/vlingo/midas/R$id;->buffer:I

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_14

    if-eqz v35, :cond_14

    move-object/from16 v0, v35

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v41, v0

    if-eqz v41, :cond_14

    check-cast v35, Lcom/vlingo/midas/gui/DialogBubble;

    .end local v35    # "vlingo":Landroid/view/View;
    invoke-virtual/range {v35 .. v35}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v41

    sget-object v42, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_14

    if-eqz v29, :cond_14

    move-object/from16 v0, v29

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v41, v0

    if-eqz v41, :cond_14

    check-cast v29, Lcom/vlingo/midas/gui/DialogBubble;

    .end local v29    # "user":Landroid/view/View;
    invoke-virtual/range {v29 .. v29}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v41

    sget-object v42, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_2

    .line 1677
    :cond_14
    const/4 v10, 0x1

    .line 1678
    goto/16 :goto_1

    .line 1680
    .restart local v29    # "user":Landroid/view/View;
    .restart local v35    # "vlingo":Landroid/view/View;
    :cond_15
    instance-of v0, v3, Lcom/vlingo/midas/gui/widgets/ButtonWidget;

    move/from16 v41, v0

    if-eqz v41, :cond_16

    .line 1681
    if-eqz v5, :cond_16

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v41

    const/16 v42, 0x165

    move/from16 v0, v41

    move/from16 v1, v42

    if-le v0, v1, :cond_16

    .line 1682
    const/16 v16, 0x1

    .line 1683
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x2

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v30

    .line 1684
    goto/16 :goto_1

    .line 1688
    :cond_16
    const/16 v22, 0x0

    .line 1689
    goto/16 :goto_1

    .line 1690
    .end local v5    # "buffer":Landroid/view/View;
    .end local v14    # "j":I
    .end local v29    # "user":Landroid/view/View;
    .end local v33    # "viewToChange":Landroid/view/View;
    .end local v35    # "vlingo":Landroid/view/View;
    :cond_17
    instance-of v0, v3, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v41, v0

    if-eqz v41, :cond_1e

    move-object/from16 v41, v3

    check-cast v41, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v41

    sget-object v42, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_1e

    .line 1691
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, 0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v33

    .line 1692
    .restart local v33    # "viewToChange":Landroid/view/View;
    move-object/from16 v34, v33

    .line 1693
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, 0x2

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1694
    .restart local v5    # "buffer":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v23

    .line 1695
    .restart local v23    # "suspectedVlingo":Landroid/view/View;
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x2

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v24

    .line 1697
    .restart local v24    # "suspectedVlingo1":Landroid/view/View;
    if-eqz v23, :cond_19

    move-object/from16 v0, v23

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v41, v0

    if-eqz v41, :cond_19

    move-object/from16 v41, v23

    check-cast v41, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v41

    sget-object v42, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_19

    if-eqz v24, :cond_19

    move-object/from16 v0, v24

    instance-of v0, v0, Lcom/vlingo/midas/gui/Widget;

    move/from16 v41, v0

    if-nez v41, :cond_18

    move-object/from16 v0, v24

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;

    move/from16 v41, v0

    if-eqz v41, :cond_19

    :cond_18
    move-object/from16 v0, v24

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;

    move/from16 v41, v0

    if-nez v41, :cond_19

    .line 1700
    const/16 v18, 0x1

    .line 1701
    goto/16 :goto_1

    .line 1704
    :cond_19
    if-eqz v5, :cond_1a

    instance-of v0, v5, Lcom/vlingo/midas/gui/DialogBubble;

    move/from16 v41, v0

    if-eqz v41, :cond_1a

    move-object/from16 v41, v5

    check-cast v41, Lcom/vlingo/midas/gui/DialogBubble;

    invoke-virtual/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogBubble;->getType()Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-result-object v41

    sget-object v42, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_1a

    .line 1705
    const/4 v10, 0x1

    .line 1706
    goto/16 :goto_1

    .line 1709
    :cond_1a
    if-eqz v23, :cond_1b

    move-object/from16 v0, v23

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/YouCanSayWidget;

    move/from16 v41, v0

    if-eqz v41, :cond_1b

    if-nez v24, :cond_1b

    .line 1710
    const/16 v18, 0x1

    .line 1711
    goto/16 :goto_1

    .line 1714
    :cond_1b
    if-eqz v5, :cond_1c

    instance-of v0, v5, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    move/from16 v41, v0

    if-eqz v41, :cond_1c

    .line 1715
    const/4 v10, 0x1

    .line 1716
    goto/16 :goto_1

    .line 1719
    :cond_1c
    move-object/from16 v0, v33

    # invokes: Lcom/vlingo/midas/gui/DialogFragment;->checkEligibilityForWidgetHeight(Landroid/view/View;Landroid/view/View;)Z
    invoke-static {v0, v5}, Lcom/vlingo/midas/gui/DialogFragment;->access$1000(Landroid/view/View;Landroid/view/View;)Z

    move-result v41

    if-eqz v41, :cond_2

    .line 1720
    move-object/from16 v0, v33

    instance-of v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    move/from16 v41, v0

    if-eqz v41, :cond_1d

    .line 1721
    const/16 v21, 0x0

    .line 1725
    :goto_8
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    add-int/lit8 v42, v13, -0x1

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v30

    .line 1726
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v36

    .line 1727
    add-int/lit8 v31, v13, -0x1

    .line 1728
    move/from16 v37, v13

    goto/16 :goto_1

    .line 1723
    :cond_1d
    const/16 v21, 0x1

    goto :goto_8

    .line 1737
    .end local v5    # "buffer":Landroid/view/View;
    .end local v23    # "suspectedVlingo":Landroid/view/View;
    .end local v24    # "suspectedVlingo1":Landroid/view/View;
    .end local v33    # "viewToChange":Landroid/view/View;
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    # invokes: Lcom/vlingo/midas/gui/DialogFragment;->isTapWidget(Landroid/view/View;)Z
    invoke-static {v0, v3}, Lcom/vlingo/midas/gui/DialogFragment;->access$1100(Lcom/vlingo/midas/gui/DialogFragment;Landroid/view/View;)Z

    move-result v41

    if-eqz v41, :cond_1f

    .line 1738
    const/16 v28, 0x1

    .line 1739
    goto/16 :goto_1

    .line 1576
    :cond_1f
    add-int/lit8 v13, v13, -0x1

    goto/16 :goto_0

    .line 1764
    .end local v7    # "checkForBuffer":Landroid/view/View;
    .end local v11    # "dummyView":Landroid/view/View;
    .end local v40    # "youCanSayWidget":Landroid/view/View;
    .restart local v2    # "audioManager":Landroid/media/AudioManager;
    .restart local v8    # "dialogBubble":Lcom/vlingo/midas/gui/DialogBubble;
    .restart local v23    # "suspectedVlingo":Landroid/view/View;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$900(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    move-result-object v41

    if-eqz v41, :cond_21

    .line 1765
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$900(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    move-result-object v41

    const/16 v42, 0x65

    invoke-virtual/range {v41 .. v42}, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;->removeMessages(I)V

    .line 1766
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mhandler:Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$900(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;

    move-result-object v41

    const/16 v42, 0x65

    const-wide/16 v43, 0x1f4

    invoke-virtual/range {v41 .. v44}, Lcom/vlingo/midas/gui/DialogFragment$DialogFragmentHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_3

    .line 1769
    :cond_21
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    const/16 v42, 0x1

    invoke-virtual/range {v41 .. v42}, Landroid/view/View;->setFocusable(Z)V

    .line 1770
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->requestFocus()Z

    .line 1771
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->bubbleViewForTalkback:Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$800()Landroid/view/View;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->requestFocusFromTouch()Z

    goto/16 :goto_3

    .line 1777
    .end local v8    # "dialogBubble":Lcom/vlingo/midas/gui/DialogBubble;
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->fullContainer:Lcom/vlingo/midas/gui/customviews/RelativeLayout;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$700(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/customviews/RelativeLayout;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v41 .. v42}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->setFocusable(Z)V

    .line 1778
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v41 .. v42}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1779
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v41 .. v42}, Landroid/widget/ScrollView;->setFocusable(Z)V

    goto/16 :goto_3

    .line 1744
    :cond_23
    add-int/lit8 v13, v13, -0x1

    goto/16 :goto_2

    .line 1803
    .end local v2    # "audioManager":Landroid/media/AudioManager;
    .end local v23    # "suspectedVlingo":Landroid/view/View;
    .restart local v6    # "bufferView":Landroid/view/View;
    .local v8, "dialogBubble":Landroid/view/View;
    .restart local v12    # "helpChoiceWidget":Landroid/view/View;
    .restart local v17    # "myDummyView":Landroid/view/View;
    :cond_24
    if-eqz v12, :cond_25

    instance-of v0, v12, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    move/from16 v41, v0

    if-eqz v41, :cond_25

    if-eqz v6, :cond_25

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v41

    sget v42, Lcom/vlingo/midas/R$id;->buffer:I

    move/from16 v0, v41

    move/from16 v1, v42

    if-ne v0, v1, :cond_25

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/vlingo/midas/gui/DialogFragment$6;->checkForWakeUpBubble(Landroid/view/View;)Z

    move-result v41

    if-eqz v41, :cond_25

    if-eqz v17, :cond_25

    move-object/from16 v0, v17

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    move/from16 v41, v0

    if-eqz v41, :cond_25

    .line 1807
    const/16 v21, 0x0

    .line 1808
    goto/16 :goto_5

    .line 1789
    :cond_25
    add-int/lit8 v13, v13, -0x1

    goto/16 :goto_4

    .line 1816
    .end local v6    # "bufferView":Landroid/view/View;
    .end local v8    # "dialogBubble":Landroid/view/View;
    .end local v17    # "myDummyView":Landroid/view/View;
    :cond_26
    const/16 v41, 0x1

    move/from16 v0, v21

    move/from16 v1, v41

    if-ne v0, v1, :cond_29

    if-eqz v30, :cond_29

    if-eqz v36, :cond_29

    .line 1817
    if-eqz v28, :cond_27

    if-eqz v3, :cond_27

    .line 1818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v43

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    .line 1821
    const/16 v28, 0x0

    goto/16 :goto_6

    .line 1823
    :cond_27
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment$6;->lastIndexOfChoiceWidget()I

    move-result v15

    .line 1824
    .local v15, "lastIndex":I
    add-int/lit8 v41, v37, 0x1

    move/from16 v0, v41

    if-le v15, v0, :cond_28

    .line 1825
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Landroid/view/View;->getTop()I

    move-result v43

    add-int/lit8 v43, v43, -0xa

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    .line 1829
    :goto_9
    if-eqz v34, :cond_7

    invoke-virtual/range {v34 .. v34}, Landroid/view/View;->getMeasuredHeight()I

    move-result v41

    const/16 v42, 0x165

    move/from16 v0, v41

    move/from16 v1, v42

    if-lt v0, v1, :cond_7

    goto/16 :goto_6

    .line 1827
    :cond_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getTop()I

    move-result v43

    add-int/lit8 v43, v43, -0xa

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_9

    .line 1836
    .end local v15    # "lastIndex":I
    :cond_29
    if-nez v21, :cond_2a

    if-eqz v12, :cond_2a

    .line 1837
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual {v12}, Landroid/view/View;->getTop()I

    move-result v43

    add-int/lit8 v43, v43, -0xa

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto/16 :goto_6

    .line 1839
    :cond_2a
    if-eqz v18, :cond_2b

    .line 1843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    const/16 v43, 0xef

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    goto/16 :goto_6

    .line 1845
    :cond_2b
    if-eqz v10, :cond_2c

    .line 1848
    if-eqz v34, :cond_7

    move-object/from16 v0, v34

    instance-of v0, v0, Lcom/vlingo/midas/gui/DialogFragment$MyDummyView;

    move/from16 v41, v0

    if-eqz v41, :cond_7

    .line 1849
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    const/16 v43, 0x5

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    goto/16 :goto_6

    .line 1853
    :cond_2c
    if-eqz v32, :cond_2e

    if-eqz v30, :cond_2e

    .line 1854
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/DialogFragment$6;->lastIndexOfChoiceWidget()I

    move-result v15

    .line 1855
    .restart local v15    # "lastIndex":I
    add-int/lit8 v41, v31, 0x1

    move/from16 v0, v41

    if-ne v15, v0, :cond_2d

    .line 1856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getTop()I

    move-result v43

    add-int/lit8 v43, v43, -0xa

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    .line 1860
    :goto_a
    const/16 v30, 0x0

    .line 1861
    const/16 v32, 0x0

    .line 1862
    goto/16 :goto_6

    .line 1858
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getMeasuredHeight()I

    move-result v43

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    goto :goto_a

    .line 1862
    .end local v15    # "lastIndex":I
    :cond_2e
    if-eqz v39, :cond_2f

    if-eqz v38, :cond_2f

    .line 1863
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v38 .. v38}, Landroid/view/View;->getMeasuredHeight()I

    move-result v43

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    .line 1864
    const/16 v39, 0x0

    .line 1865
    const/16 v38, 0x0

    goto/16 :goto_6

    .line 1866
    :cond_2f
    if-nez v19, :cond_7

    .line 1867
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    move-object/from16 v41, v0

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static/range {v41 .. v41}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v41

    const/16 v42, 0x0

    add-int/lit8 v43, p1, -0xa

    invoke-virtual/range {v41 .. v43}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto/16 :goto_6
.end method

.method private lastIndexOfChoiceWidget()I
    .locals 3

    .prologue
    .line 1887
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_2

    .line 1888
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1889
    .local v1, "v":Landroid/view/View;
    instance-of v2, v1, Lcom/vlingo/midas/gui/widgets/AlarmChoiceWidget;

    if-nez v2, :cond_0

    instance-of v2, v1, Lcom/vlingo/midas/gui/widgets/ContactChoiceWidget;

    if-nez v2, :cond_0

    instance-of v2, v1, Lcom/vlingo/midas/gui/widgets/ScheduleChoiceWidget;

    if-nez v2, :cond_0

    instance-of v2, v1, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;

    if-nez v2, :cond_0

    instance-of v2, v1, Lcom/vlingo/midas/gui/widgets/NaverWidget;

    if-eqz v2, :cond_1

    .line 1897
    .end local v0    # "i":I
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    :goto_1
    return v0

    .line 1887
    .restart local v0    # "i":I
    .restart local v1    # "v":Landroid/view/View;
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1897
    .end local v1    # "v":Landroid/view/View;
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1537
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1538
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->currentPhone:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$400()Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->K_DEVICE:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    if-ne v0, v1, :cond_1

    .line 1539
    iget v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->val$height:I

    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/DialogFragment$6;->doScrollAnimForKPhone(I)V

    .line 1545
    :cond_0
    :goto_0
    return-void

    .line 1541
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, 0x0

    iget v2, p0, Lcom/vlingo/midas/gui/DialogFragment$6;->val$height:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_0
.end method
