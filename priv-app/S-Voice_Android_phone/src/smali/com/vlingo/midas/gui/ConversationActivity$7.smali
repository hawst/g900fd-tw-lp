.class Lcom/vlingo/midas/gui/ConversationActivity$7;
.super Ljava/lang/Object;
.source "ConversationActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/ConversationActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ConversationActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0

    .prologue
    .line 2046
    iput-object p1, p0, Lcom/vlingo/midas/gui/ConversationActivity$7;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 2054
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragment;->isPaused()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2055
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2056
    const-string/jumbo v0, "ConversationActivity"

    const-string/jumbo v1, "[LatencyCheck] setTaskOnGetAudioFocus & BluetoothManager.onAppStateChanged(true)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2057
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->onAppStateChanged(Z)V

    .line 2069
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$7;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # setter for: Lcom/vlingo/midas/gui/ConversationActivity;->startRecoBySCO:Z
    invoke-static {v0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->access$2302(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z

    .line 2070
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$7;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # setter for: Lcom/vlingo/midas/gui/ConversationActivity;->autoStartOnResume:Z
    invoke-static {v0, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->access$102(Lcom/vlingo/midas/gui/ConversationActivity;Z)Z

    .line 2072
    :cond_1
    return-void

    .line 2060
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$7;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    invoke-virtual {v0, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->removeMessages(I)V

    .line 2061
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/notification/NotificationPopUpManagerValues;->showingNotifications()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2064
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$7;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v4, v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 2065
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0, v3}, Lcom/vlingo/midas/gui/ControlFragment;->setIsRecognitionPostBtConnect(Z)V

    goto :goto_0
.end method
