.class public Lcom/vlingo/midas/gui/widgets/ButtonWidget;
.super Lcom/vlingo/midas/gui/Widget;
.source "ButtonWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/Widget",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field button:Landroid/widget/Button;

.field customButton:Landroid/view/View;

.field listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/Widget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method private isLT03()Z
    .locals 3

    .prologue
    const/16 v2, 0xa00

    const/16 v1, 0x640

    .line 71
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v0, v2, :cond_2

    .line 75
    :cond_1
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setLandscapeWidget()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 82
    const/high16 v4, 0x438d0000    # 282.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v3, v4

    .line 83
    .local v3, "width":I
    const/high16 v4, 0x42540000    # 53.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v0, v4

    .line 84
    .local v0, "height":I
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 86
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v4, 0x43960000    # 300.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v1, v4

    .line 87
    .local v1, "margin":I
    invoke-virtual {v2, v1, v6, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 88
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    if-eqz v4, :cond_0

    .line 89
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    :cond_0
    return-void
.end method

.method private setPotraitWidget()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 95
    const/high16 v4, 0x438d0000    # 282.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v3, v4

    .line 96
    .local v3, "width":I
    const/high16 v4, 0x42540000    # 53.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v0, v4

    .line 97
    .local v0, "height":I
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 99
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v4, 0x42d00000    # 104.0f

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v1, v4

    .line 100
    .local v1, "margin":I
    invoke-virtual {v2, v1, v6, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 101
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    if-eqz v4, :cond_0

    .line 102
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 30
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->initialize(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/lang/String;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 161
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 170
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 171
    sget v1, Lcom/vlingo/midas/R$id;->widget_button_text:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 172
    .local v0, "tv":Landroid/widget/TextView;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->SECRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 173
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    .end local v0    # "tv":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->retire()V

    .line 136
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v4

    if-nez v4, :cond_1

    .line 137
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    if-eqz v4, :cond_2

    .line 138
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$string;->more_btn:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 139
    const/4 v1, 0x0

    .line 140
    .local v1, "countChild":I
    if-eqz p0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getChildCount()I

    move-result v1

    .line 142
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 143
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 144
    .local v0, "child":Landroid/view/View;
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 142
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 149
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "countChild":I
    .end local v2    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->customButton:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 152
    :cond_2
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 153
    .local v3, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v5, 0x0

    invoke-interface {v4, v3, v5}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 155
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/Widget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 127
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 42
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    sget v0, Lcom/vlingo/midas/R$id;->widget_button:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->customButton:Landroid/view/View;

    .line 44
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->customButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    sget v0, Lcom/vlingo/midas/R$id;->button:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    .line 47
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ButtonWidget;->button:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
