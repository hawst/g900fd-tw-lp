.class Lcom/vlingo/midas/gui/ConversationActivity$14;
.super Ljava/lang/Object;
.source "ConversationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/ConversationActivity;->handsfreeModeSecurelockNoti(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ConversationActivity;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;

.field final synthetic val$isLocksecured:Z


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;Landroid/widget/CheckBox;Z)V
    .locals 0

    .prologue
    .line 4101
    iput-object p1, p0, Lcom/vlingo/midas/gui/ConversationActivity$14;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iput-object p2, p0, Lcom/vlingo/midas/gui/ConversationActivity$14;->val$checkBox:Landroid/widget/CheckBox;

    iput-boolean p3, p0, Lcom/vlingo/midas/gui/ConversationActivity$14;->val$isLocksecured:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    .line 4104
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity$14;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4105
    const/4 v0, 0x0

    .line 4106
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/vlingo/midas/gui/ConversationActivity$14;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    const-string/jumbo v2, "preferences_voice_wake_up"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/midas/gui/ConversationActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4107
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity$14;->val$isLocksecured:Z

    if-eqz v1, :cond_2

    .line 4108
    const-string/jumbo v1, "handsfree_secured_lock_enable_pop"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 4111
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4113
    .end local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    iget-boolean v1, p0, Lcom/vlingo/midas/gui/ConversationActivity$14;->val$isLocksecured:Z

    if-eqz v1, :cond_1

    .line 4114
    const-string/jumbo v1, "secure_voice_wake_up"

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 4116
    :cond_1
    return-void

    .line 4110
    .restart local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    const-string/jumbo v1, "handsfree_secured_lock_pop"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method
