.class Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3$1;
.super Ljava/lang/Object;
.source "AnswerQuestionWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->updateUI(Lcom/vlingo/core/internal/questions/DownloadableImage;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;

.field final synthetic val$downloaded:Z

.field final synthetic val$image:Lcom/vlingo/core/internal/questions/DownloadableImage;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;ZLcom/vlingo/core/internal/questions/DownloadableImage;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3$1;->this$1:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;

    iput-boolean p2, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3$1;->val$downloaded:Z

    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3$1;->val$image:Lcom/vlingo/core/internal/questions/DownloadableImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3$1;->this$1:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->val$indicator:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3$1;->this$1:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;

    iget-object v1, v0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->val$ri:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3$1;->val$downloaded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3$1;->val$image:Lcom/vlingo/core/internal/questions/DownloadableImage;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/questions/DownloadableImage;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 281
    return-void

    .line 280
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
