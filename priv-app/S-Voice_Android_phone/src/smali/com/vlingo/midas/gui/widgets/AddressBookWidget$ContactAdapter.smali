.class public Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;
.super Landroid/widget/BaseAdapter;
.source "AddressBookWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/AddressBookWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ContactAdapter"
.end annotation


# instance fields
.field DEFAULT_COLOR:[I

.field DEFAULT_IMG:[I

.field private final avatars:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;"
        }
    .end annotation
.end field

.field private img_cnt:I

.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/AddressBookWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/AddressBookWidget;Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactMatch;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactMatch;>;"
    .local p4, "avatars":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 163
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AddressBookWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 146
    iput v2, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->img_cnt:I

    .line 147
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_01:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_02:I

    aput v1, v0, v3

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_03:I

    aput v1, v0, v4

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_04:I

    aput v1, v0, v5

    sget v1, Lcom/vlingo/midas/R$drawable;->contacts_default_image_05:I

    aput v1, v0, v6

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->DEFAULT_IMG:[I

    .line 155
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_1_bg_color:I

    aput v1, v0, v2

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_2_bg_color:I

    aput v1, v0, v3

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_3_bg_color:I

    aput v1, v0, v4

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_4_bg_color:I

    aput v1, v0, v5

    sget v1, Lcom/vlingo/midas/R$color;->contact_img_5_bg_color:I

    aput v1, v0, v6

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->DEFAULT_COLOR:[I

    .line 164
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->contacts:Ljava/util/List;

    .line 165
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->avatars:Ljava/util/List;

    .line 166
    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->mContext:Landroid/content/Context;

    .line 167
    return-void
.end method

.method private getContactPhoto(Ljava/lang/Long;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "contactID"    # Ljava/lang/Long;

    .prologue
    .line 279
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 280
    .local v2, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 281
    .local v0, "is":Ljava/io/InputStream;
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 282
    .local v1, "photo":Landroid/graphics/Bitmap;
    return-object v1
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 179
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 180
    .local v0, "toReturn":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AddressBookWidget;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getLimitedCount(I)I

    move-result v1

    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 174
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v7, 0x1

    .line 185
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactMatch;

    .line 187
    .local v0, "contact":Lcom/vlingo/core/internal/contacts/ContactMatch;
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getPhoneData()Ljava/util/List;

    move-result-object v3

    .line 188
    .local v3, "phoneNumber":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-gtz v5, :cond_1

    .line 189
    :cond_0
    invoke-virtual {v0}, Lcom/vlingo/core/internal/contacts/ContactMatch;->getEmailData()Ljava/util/List;

    move-result-object v3

    .line 191
    :cond_1
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->avatars:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    .line 192
    .local v4, "photo":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 194
    .local v2, "phoneData":Lcom/vlingo/core/internal/contacts/ContactData;
    if-eqz v3, :cond_2

    .line 195
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "phoneData":Lcom/vlingo/core/internal/contacts/ContactData;
    check-cast v2, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 199
    .restart local v2    # "phoneData":Lcom/vlingo/core/internal/contacts/ContactData;
    :cond_2
    if-nez p2, :cond_5

    .line 200
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AddressBookWidget;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$layout;->item_contact_choice:I

    invoke-static {v5, v6, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 201
    new-instance v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;

    invoke-direct {v1, v10}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/AddressBookWidget$1;)V

    .line 202
    .local v1, "holder":Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;
    sget v5, Lcom/vlingo/midas/R$id;->item_choice_container:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->choice_container:Landroid/widget/LinearLayout;

    .line 203
    sget v5, Lcom/vlingo/midas/R$id;->text_title:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->title:Landroid/widget/TextView;

    .line 204
    sget v5, Lcom/vlingo/midas/R$id;->text_number:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->phone:Landroid/widget/TextView;

    .line 205
    sget v5, Lcom/vlingo/midas/R$id;->star_img:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->star_img:Landroid/view/View;

    .line 206
    sget v5, Lcom/vlingo/midas/R$id;->divider:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->divider:Landroid/view/View;

    .line 207
    sget v5, Lcom/vlingo/midas/R$id;->image_contact:I

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->qcb:Landroid/widget/ImageView;

    .line 209
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->title:Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->SECRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 210
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->phone:Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->SECRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 211
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 215
    :goto_0
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v6, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->primaryDisplayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData;->isPhoneNumber()Z

    move-result v5

    if-ne v5, v7, :cond_6

    .line 218
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->phone:Landroid/widget/TextView;

    iget-object v6, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    :cond_3
    :goto_1
    iget-boolean v5, v0, Lcom/vlingo/core/internal/contacts/ContactMatch;->starred:Z

    if-eqz v5, :cond_7

    .line 224
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->star_img:Landroid/view/View;

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 229
    :goto_2
    if-nez v4, :cond_a

    .line 230
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 231
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 232
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AddressBookWidget;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->DEFAULT_IMG:[I

    iget v7, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->img_cnt:I

    rem-int/lit8 v7, v7, 0x5

    aget v6, v6, v7

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 233
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AddressBookWidget;

    invoke-virtual {v5, v4}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getRoundedShape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 234
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->qcb:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 235
    iget v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->img_cnt:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->img_cnt:I

    .line 244
    :goto_3
    iget v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->img_cnt:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->img_cnt:I

    .line 249
    :goto_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 250
    sget v5, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 251
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne p1, v5, :cond_4

    .line 252
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->divider:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 265
    :cond_4
    :goto_5
    new-instance v5, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter$1;

    invoke-direct {v5, p0, p1}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;I)V

    invoke-virtual {p2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 275
    return-object p2

    .line 213
    .end local v1    # "holder":Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;
    :cond_5
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;

    .restart local v1    # "holder":Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;
    goto :goto_0

    .line 219
    :cond_6
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/vlingo/core/internal/contacts/ContactData;->isEmail()Z

    move-result v5

    if-ne v5, v7, :cond_3

    .line 220
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->phone:Landroid/widget/TextView;

    iget-object v6, v2, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 226
    :cond_7
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->star_img:Landroid/view/View;

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 237
    :cond_8
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->qcb:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->DEFAULT_COLOR:[I

    rem-int/lit8 v7, p1, 0x5

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_3

    .line 240
    :cond_9
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AddressBookWidget;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->DEFAULT_IMG:[I

    iget v7, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->img_cnt:I

    rem-int/lit8 v7, v7, 0x5

    aget v6, v6, v7

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 241
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/AddressBookWidget;

    invoke-virtual {v5, v4}, Lcom/vlingo/midas/gui/widgets/AddressBookWidget;->getRoundedShape(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 242
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->qcb:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3

    .line 246
    :cond_a
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->qcb:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_4

    .line 255
    :cond_b
    if-nez p1, :cond_c

    .line 256
    sget v5, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    invoke-virtual {p2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_5

    .line 257
    :cond_c
    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ContactAdapter;->contacts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne p1, v5, :cond_d

    .line 258
    iget-object v5, v1, Lcom/vlingo/midas/gui/widgets/AddressBookWidget$ViewHolder;->divider:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 259
    sget v5, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_5

    .line 261
    :cond_d
    sget v5, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_5
.end method
