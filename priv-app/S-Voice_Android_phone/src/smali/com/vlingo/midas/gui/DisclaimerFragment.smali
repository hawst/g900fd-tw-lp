.class public Lcom/vlingo/midas/gui/DisclaimerFragment;
.super Landroid/support/v4/app/Fragment;
.source "DisclaimerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/DisclaimerFragment$TOSAcceptListener;
    }
.end annotation


# static fields
.field public static final IS_FIRST_LAUNCH:Ljava/lang/String; = "IS_FIRST_LAUNCH"

.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"


# instance fields
.field private isAgreeBtnChecked:Z

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mDisclaimerContent:Landroid/widget/TextView;

.field private mDisclaimerTOSLayout:Landroid/widget/RelativeLayout;

.field private mLink1:Landroid/widget/TextView;

.field private mLink2:Landroid/widget/TextView;

.field private mTOSAcceptListener:Lcom/vlingo/midas/gui/DisclaimerFragment$TOSAcceptListener;

.field private mTOSContent:Landroid/widget/TextView;

.field private nextBtn:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

.field prefForWishesEventAlert:Landroid/content/SharedPreferences;

.field private previousBtn:Landroid/widget/LinearLayout;

.field private rightBtnText:Landroid/widget/TextView;

.field root_container:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSAcceptListener:Lcom/vlingo/midas/gui/DisclaimerFragment$TOSAcceptListener;

    .line 52
    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerTOSLayout:Landroid/widget/RelativeLayout;

    .line 53
    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mCheckBox:Landroid/widget/CheckBox;

    .line 54
    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->nextBtn:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    .line 55
    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->previousBtn:Landroid/widget/LinearLayout;

    .line 56
    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->rightBtnText:Landroid/widget/TextView;

    .line 57
    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerContent:Landroid/widget/TextView;

    .line 58
    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSContent:Landroid/widget/TextView;

    .line 59
    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink1:Landroid/widget/TextView;

    .line 60
    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink2:Landroid/widget/TextView;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->isAgreeBtnChecked:Z

    .line 320
    return-void
.end method

.method private LaunchFirstTimeScreen()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 337
    invoke-static {}, Lcom/vlingo/core/internal/util/SayHello;->sendHello()Z

    .line 339
    invoke-static {v6}, Lcom/vlingo/midas/settings/MidasSettings;->setLocationEnabled(Z)V

    .line 340
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "com.vlingo.LANGUAGE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 344
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/ServiceManager;->startLocalServices()Z

    .line 346
    sget-boolean v2, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v2, :cond_0

    .line 347
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->startOtherAppAfterAccept()V

    .line 350
    :cond_0
    invoke-static {v6}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 351
    invoke-static {v6}, Lcom/vlingo/midas/settings/MidasSettings;->setTOSAccepted(Z)V

    .line 352
    const-string/jumbo v2, "all_notifications_accepted"

    invoke-static {v2, v5}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 354
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onTosAccepted()V

    .line 357
    const-string/jumbo v2, "tos_accepted_version"

    invoke-static {v2, v5}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_1

    .line 358
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string/jumbo v3, "prefForWishesEventAlert"

    invoke-virtual {v2, v3, v5}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->prefForWishesEventAlert:Landroid/content/SharedPreferences;

    .line 359
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->prefForWishesEventAlert:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 360
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "IS_FIRST_LAUNCH"

    const-string/jumbo v3, "0000-00-00"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 361
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 365
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    sget-boolean v2, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v2, :cond_2

    .line 366
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finishAffinity()V

    .line 367
    sput-boolean v5, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    .line 375
    :goto_0
    return-void

    .line 370
    :cond_2
    invoke-static {v5}, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->setLaunchedForTosAcceptOtherApp(Z)V

    .line 371
    const-string/jumbo v2, "former_tos_acceptance_state"

    const-string/jumbo v3, "reminder_not_needed"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 373
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/DisclaimerFragment;->startActivity(Landroid/content/Intent;)V

    .line 374
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/DisclaimerFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DisclaimerFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/gui/DisclaimerFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DisclaimerFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->isAgreeBtnChecked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/DisclaimerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DisclaimerFragment;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->screenWhenConfirmed()V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/DisclaimerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DisclaimerFragment;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->screenWhenDisagree()V

    return-void
.end method

.method private afterTOSAccepted()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 76
    invoke-static {}, Lcom/vlingo/core/internal/util/SayHello;->sendHello()Z

    .line 78
    invoke-static {v9}, Lcom/vlingo/midas/settings/MidasSettings;->setLocationEnabled(Z)V

    .line 79
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v7, "com.vlingo.LANGUAGE_CHANGED"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 83
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/ServiceManager;->startLocalServices()Z

    .line 85
    sget-boolean v5, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v5, :cond_0

    .line 86
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->startOtherAppAfterAccept()V

    .line 89
    :cond_0
    invoke-static {v9}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 90
    invoke-static {v9}, Lcom/vlingo/midas/settings/MidasSettings;->setTOSAccepted(Z)V

    .line 91
    const-string/jumbo v5, "all_notifications_accepted"

    invoke-static {v5, v8}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 93
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onTosAccepted()V

    .line 96
    const-string/jumbo v5, "tos_accepted_version"

    invoke-static {v5, v8}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-nez v5, :cond_1

    .line 97
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-string/jumbo v6, "prefForWishesEventAlert"

    invoke-virtual {v5, v6, v8}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->prefForWishesEventAlert:Landroid/content/SharedPreferences;

    .line 98
    iget-object v5, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->prefForWishesEventAlert:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 99
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v5, "IS_FIRST_LAUNCH"

    const-string/jumbo v6, "0000-00-00"

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 100
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 104
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    sget-boolean v5, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v5, :cond_2

    .line 105
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->finishAffinity()V

    .line 106
    sput-boolean v8, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    .line 140
    :goto_0
    return-void

    .line 109
    :cond_2
    invoke-static {v8}, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->setLaunchedForTosAcceptOtherApp(Z)V

    .line 110
    const-string/jumbo v5, "former_tos_acceptance_state"

    const-string/jumbo v6, "reminder_not_needed"

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :try_start_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 114
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->checkVoiceWakeupFeature()V

    .line 115
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 116
    .local v3, "mDefaultSharedPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 117
    .restart local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v5, "is_move_to_svoice"

    const/4 v6, 0x1

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 118
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 120
    const-string/jumbo v5, "showing_notifications"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 121
    const-string/jumbo v5, "all_notifications_accepted"

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 122
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/vlingo/midas/iux/IUXManager;->setIUXComplete(Z)V

    .line 123
    const/4 v5, 0x0

    invoke-static {v5}, Lcom/vlingo/midas/iux/IUXManager;->setIUXIntroRequired(Z)V

    .line 124
    new-instance v4, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 125
    .local v4, "o":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->setTutorialCompleted(Z)V

    .line 127
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/midas/iux/IUXManager;->finishIUX(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 131
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "mDefaultSharedPrefs":Landroid/content/SharedPreferences;
    .end local v4    # "o":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/IllegalStateException;
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/DisclaimerFragment;->startActivity(Landroid/content/Intent;)V

    .line 134
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 129
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    :try_start_1
    iget-object v5, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSAcceptListener:Lcom/vlingo/midas/gui/DisclaimerFragment$TOSAcceptListener;

    invoke-interface {v5}, Lcom/vlingo/midas/gui/DisclaimerFragment$TOSAcceptListener;->TOSAccepted()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private displayDisclaimerTOSScreen()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 191
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerTOSLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerTOSLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 196
    :cond_0
    const-string/jumbo v1, "system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v1}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 197
    .local v0, "secRobotoLight":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerContent:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 198
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasCarModeApp()Z

    move-result v1

    if-nez v1, :cond_6

    .line 199
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerContent:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_samsung_without_carmode:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerContent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    if-ne v1, v2, :cond_1

    .line 204
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerContent:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 207
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSContent:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 208
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasCarModeApp()Z

    move-result v1

    if-nez v1, :cond_7

    .line 209
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSContent:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_old_without_carmode:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    :goto_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSContent:Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/midas/gui/CustomLinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 214
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSContent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    if-ne v1, v2, :cond_2

    .line 215
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSContent:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 219
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->rightBtnText:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 220
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->rightBtnText:Landroid/widget/TextView;

    const-string/jumbo v2, "#ffffff"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 221
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->rightBtnText:Landroid/widget/TextView;

    const v2, 0x3ecccccd    # 0.4f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 224
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->nextBtn:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    if-eqz v1, :cond_4

    .line 225
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->nextBtn:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->setEnabled(Z)V

    .line 228
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->previousBtn:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_5

    .line 229
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->previousBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 231
    :cond_5
    return-void

    .line 201
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerContent:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_samsung:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 211
    :cond_7
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSContent:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tos_dialog_view_terms_old:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private init()V
    .locals 5

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v0, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 248
    .local v0, "locale":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 249
    .local v1, "suffix":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v3, Lcom/vlingo/midas/R$id;->samsung_disclaimer_tos_layout:I

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerTOSLayout:Landroid/widget/RelativeLayout;

    .line 250
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v3, Lcom/vlingo/midas/R$id;->disclaimer_content:I

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mDisclaimerContent:Landroid/widget/TextView;

    .line 251
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v3, Lcom/vlingo/midas/R$id;->link1:I

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink1:Landroid/widget/TextView;

    .line 252
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v3, Lcom/vlingo/midas/R$id;->link2:I

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink2:Landroid/widget/TextView;

    .line 253
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink1:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->tos_terms_of_service_link:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink1:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 255
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink1:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v3

    or-int/lit8 v3, v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 256
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink1:Landroid/widget/TextView;

    new-instance v3, Lcom/vlingo/midas/gui/DisclaimerFragment$1;

    invoke-direct {v3, p0, v1}, Lcom/vlingo/midas/gui/DisclaimerFragment$1;-><init>(Lcom/vlingo/midas/gui/DisclaimerFragment;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink2:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->tos_nuance_privacy_policy_link:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink2:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 266
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink2:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink2:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v3

    or-int/lit8 v3, v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 267
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mLink2:Landroid/widget/TextView;

    new-instance v3, Lcom/vlingo/midas/gui/DisclaimerFragment$2;

    invoke-direct {v3, p0, v1}, Lcom/vlingo/midas/gui/DisclaimerFragment$2;-><init>(Lcom/vlingo/midas/gui/DisclaimerFragment;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 273
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v3, Lcom/vlingo/midas/R$id;->disclaimer_checkbox:I

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mCheckBox:Landroid/widget/CheckBox;

    .line 274
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v3, Lcom/vlingo/midas/R$id;->tos_content:I

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSContent:Landroid/widget/TextView;

    .line 275
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v3, Lcom/vlingo/midas/R$id;->prev_btn:I

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->previousBtn:Landroid/widget/LinearLayout;

    .line 276
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->root_container:Landroid/widget/RelativeLayout;

    sget v3, Lcom/vlingo/midas/R$id;->next_btn:I

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    iput-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->nextBtn:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    .line 278
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 279
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->nextBtn:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    sget v3, Lcom/vlingo/midas/R$string;->start:I

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->setButtonText(Ljava/lang/String;)V

    .line 281
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->nextBtn:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    invoke-virtual {v2, p0}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v3, Lcom/vlingo/midas/gui/DisclaimerFragment$3;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/DisclaimerFragment$3;-><init>(Lcom/vlingo/midas/gui/DisclaimerFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    iget-object v2, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v3, Lcom/vlingo/midas/gui/DisclaimerFragment$4;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/DisclaimerFragment$4;-><init>(Lcom/vlingo/midas/gui/DisclaimerFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 304
    return-void
.end method

.method private screenWhenConfirmed()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->nextBtn:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->setEnabled(Z)V

    .line 308
    return-void
.end method

.method private screenWhenDisagree()V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->nextBtn:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->setEnabled(Z)V

    .line 312
    return-void
.end method

.method private showTOSDisclaimerScreen()V
    .locals 2

    .prologue
    .line 186
    const-string/jumbo v0, "showing_notifications"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 187
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->displayDisclaimerTOSScreen()V

    .line 188
    return-void
.end method

.method private startOtherAppAfterAccept()V
    .locals 2

    .prologue
    .line 157
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 158
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.midas.action.TOS_ACCEPTED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 160
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 161
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 177
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 178
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->showTOSDisclaimerScreen()V

    .line 179
    if-eqz p1, :cond_0

    .line 180
    const-string/jumbo v1, "isCheckboxChecked"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 181
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 183
    .end local v0    # "isChecked":Z
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 145
    instance-of v0, p1, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;

    if-eqz v0, :cond_0

    .line 146
    check-cast p1, Lcom/vlingo/midas/gui/DisclaimerFragment$TOSAcceptListener;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSAcceptListener:Lcom/vlingo/midas/gui/DisclaimerFragment$TOSAcceptListener;

    .line 153
    :goto_0
    return-void

    .line 148
    .restart local p1    # "activity":Landroid/app/Activity;
    :cond_0
    new-instance v0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;

    invoke-direct {v0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mTOSAcceptListener:Lcom/vlingo/midas/gui/DisclaimerFragment$TOSAcceptListener;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 326
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vlingo/midas/iux/IUXManager;->setIUXComplete(Z)V

    .line 329
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->LaunchFirstTimeScreen()V

    .line 334
    :goto_0
    return-void

    .line 332
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->afterTOSAccepted()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 69
    sget v0, Lcom/vlingo/midas/R$layout;->disclaimer_screen_k:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->root_container:Landroid/widget/RelativeLayout;

    .line 70
    invoke-direct {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->init()V

    .line 71
    iget-object v0, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->root_container:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 316
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 317
    const-string/jumbo v0, "isCheckboxChecked"

    iget-object v1, p0, Lcom/vlingo/midas/gui/DisclaimerFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 318
    return-void
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 235
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 237
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v1

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/vlingo/midas/uifocus/UiFocusActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v0, v1, v2}, Lcom/vlingo/midas/uifocus/UiFocusUtil;->onForegrounded(Landroid/content/Context;ZLandroid/content/Intent;)Z

    .line 238
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 168
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 169
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 171
    const-string/jumbo v0, "all_notifications_accepted"

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 173
    :cond_0
    return-void
.end method

.method public openWebURL(Ljava/lang/String;)V
    .locals 3
    .param p1, "inURL"    # Ljava/lang/String;

    .prologue
    .line 241
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 243
    .local v0, "browse":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/DisclaimerFragment;->startActivity(Landroid/content/Intent;)V

    .line 244
    return-void
.end method
