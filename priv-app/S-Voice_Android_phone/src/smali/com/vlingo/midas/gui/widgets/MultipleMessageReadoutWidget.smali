.class public Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "MultipleMessageReadoutWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;,
        Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
        ">;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field protected static final TRIMMED_MAX_MESSAGE_KITKAT_LENGTH:I = 0x19

.field protected static final TRIMMED_MAX_MESSAGE_LENGTH:I = 0x1e


# instance fields
.field protected context:Landroid/content/Context;

.field protected listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field ll_message_button_container:Landroid/widget/LinearLayout;

.field protected mMultipleMessages:Landroid/widget/ListView;

.field private messages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/dialogmanager/types/MessageType;",
            ">;"
        }
    .end annotation
.end field

.field protected showMessageBody:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->context:Landroid/content/Context;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->showMessageBody:Z

    .line 73
    return-void
.end method


# virtual methods
.method public fetchContactIdFromPhoneNumber(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;
    .locals 9
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "classContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 430
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 431
    .local v7, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;>;"
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 432
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "display_name"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "_id"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 433
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 435
    :cond_0
    new-instance v6, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;

    invoke-direct {v6}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;-><init>()V

    .line 436
    .local v6, "contact":Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;
    const-string/jumbo v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;->contactId:Ljava/lang/String;

    .line 438
    const-string/jumbo v0, "display_name"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;->contactName:Ljava/lang/String;

    .line 440
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 441
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 444
    .end local v6    # "contact":Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ContactDetail;
    :cond_1
    if-eqz v8, :cond_2

    .line 445
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 448
    :cond_2
    return-object v7
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 53
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 4
    .param p1, "m"    # Ljava/util/List;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 195
    move-object v0, p1

    .line 196
    .local v0, "msgs":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/dialogmanager/types/MessageType;>;"
    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->messages:Ljava/util/List;

    .line 197
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 199
    if-eqz p2, :cond_0

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DecoratorType;->ShowMessageBody:Lcom/vlingo/core/internal/dialogmanager/DecoratorType;

    invoke-virtual {p2, v1}, Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;->has(Lcom/vlingo/core/internal/dialogmanager/DecoratorType;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->showMessageBody:Z

    .line 202
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->messages:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 204
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->messages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMaxDisplayNumber()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 205
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->messages:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getMaxDisplayNumber()I

    move-result v3

    invoke-interface {v1, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->messages:Ljava/util/List;

    .line 208
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->mMultipleMessages:Landroid/widget/ListView;

    new-instance v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;

    invoke-direct {v2, p0, v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 218
    :cond_2
    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    .line 86
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 87
    sget v7, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->mMultipleMessages:Landroid/widget/ListView;

    .line 88
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->mMultipleMessages:Landroid/widget/ListView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 89
    invoke-static {}, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;->getInstance()Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;

    move-result-object v5

    .line 91
    .local v5, "multipleMessageReadoutUtil":Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;
    invoke-virtual {v5}, Lcom/vlingo/midas/util/MultipleMessageReadoutUtil;->getMessagehandlerListener()Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;

    move-result-object v0

    .line 92
    .local v0, "actionHandlerListener":Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;
    if-eqz v0, :cond_0

    .line 93
    invoke-static {v0}, Lcom/vlingo/core/internal/util/OrdinalUtil;->getListControlData(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;)Lcom/vlingo/core/internal/util/ListControlData;

    move-result-object v4

    .line 95
    .local v4, "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    sget v7, Lcom/vlingo/midas/R$id;->message_button_container:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->ll_message_button_container:Landroid/widget/LinearLayout;

    .line 96
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->ll_message_button_container:Landroid/widget/LinearLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 98
    sget v7, Lcom/vlingo/midas/R$id;->btn_next:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 99
    .local v1, "btnNext":Landroid/widget/Button;
    sget v7, Lcom/vlingo/midas/R$id;->btn_prev:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 100
    .local v2, "btnPrev":Landroid/widget/Button;
    sget v7, Lcom/vlingo/midas/R$id;->btn_divider:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 103
    .local v6, "view_btn_divider":Landroid/view/View;
    :try_start_0
    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ListControlData;->incCurrentPage()V

    .line 104
    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ListControlData;->decCurrentPage()V
    :try_end_0
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    :try_start_1
    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ListControlData;->decCurrentPage()V

    .line 113
    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ListControlData;->incCurrentPage()V
    :try_end_1
    .catch Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached; {:try_start_1 .. :try_end_1} :catch_1

    .line 121
    :goto_1
    new-instance v7, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$1;

    invoke-direct {v7, p0, v0, v4}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    new-instance v7, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;

    invoke-direct {v7, p0, v0, v4}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$2;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionHandlerListener;Lcom/vlingo/core/internal/util/ListControlData;)V

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    .end local v1    # "btnNext":Landroid/widget/Button;
    .end local v2    # "btnPrev":Landroid/widget/Button;
    .end local v4    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    .end local v6    # "view_btn_divider":Landroid/view/View;
    :cond_0
    return-void

    .line 105
    .restart local v1    # "btnNext":Landroid/widget/Button;
    .restart local v2    # "btnPrev":Landroid/widget/Button;
    .restart local v4    # "listControlData":Lcom/vlingo/core/internal/util/ListControlData;
    .restart local v6    # "view_btn_divider":Landroid/view/View;
    :catch_0
    move-exception v3

    .line 106
    .local v3, "e1":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    invoke-virtual {v1, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 107
    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 108
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;->printStackTrace()V

    goto :goto_0

    .line 114
    .end local v3    # "e1":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    :catch_1
    move-exception v3

    .line 116
    .restart local v3    # "e1":Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;
    invoke-virtual {v2, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 117
    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 118
    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ListControlData$PageExceptionBoundReached;->printStackTrace()V

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 416
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 189
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->mMultipleMessages:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 190
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 191
    return-void
.end method

.method public retire()V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->retire()V

    .line 79
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->ll_message_button_container:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->ll_message_button_container:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 81
    :cond_0
    return-void
.end method
