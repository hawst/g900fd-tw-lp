.class Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$1;
.super Ljava/lang/Object;
.source "TaskChoiceWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;I)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;

    iput p2, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 174
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;->retire()V

    .line 176
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 177
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    const-string/jumbo v1, "choice"

    iget v2, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$1;->val$position:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 179
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter$1;->this$1:Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget$TaskAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/TaskChoiceWidget;)Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 186
    return-void
.end method
