.class Lcom/vlingo/midas/gui/DialogFragment$10;
.super Ljava/lang/Thread;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/DialogFragment;->setShadowBottom()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;)V
    .locals 0

    .prologue
    .line 2736
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragment$10;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2740
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$10;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    const/4 v2, 0x0

    iput v2, v1, Lcom/vlingo/midas/gui/DialogFragment;->shouldContinueBelow:I

    .line 2741
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$10;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->shouldContinueBelow:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$10;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget v1, v1, Lcom/vlingo/midas/gui/DialogFragment;->ThreadstopShadowBelow:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2742
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$10;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$1200(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->removeMessages(I)V

    .line 2743
    iget-object v1, p0, Lcom/vlingo/midas/gui/DialogFragment$10;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->mhandler1:Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;
    invoke-static {v1}, Lcom/vlingo/midas/gui/DialogFragment;->access$1200(Lcom/vlingo/midas/gui/DialogFragment;)Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/DialogFragment$ShadowThreadHandler;->sendEmptyMessage(I)Z

    .line 2744
    const-wide/16 v1, 0x64

    invoke-static {v1, v2}, Lcom/vlingo/midas/gui/DialogFragment$10;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2746
    :catch_0
    move-exception v0

    .line 2747
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 2749
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    return-void
.end method
