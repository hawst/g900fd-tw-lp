.class Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;
.super Ljava/lang/Object;
.source "AnswerQuestionWidget.java"

# interfaces
.implements Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->createImageContentWithOverlays(Lcom/vlingo/core/internal/questions/Answer$Subsection;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;

.field final synthetic val$downloadTimer:Ljava/util/Timer;

.field final synthetic val$indicator:Landroid/widget/ProgressBar;

.field final synthetic val$ri:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;Ljava/util/Timer;Landroid/widget/ProgressBar;Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->val$downloadTimer:Ljava/util/Timer;

    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->val$indicator:Landroid/widget/ProgressBar;

    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->val$ri:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$RenderedImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private updateUI(Lcom/vlingo/core/internal/questions/DownloadableImage;Z)V
    .locals 2
    .param p1, "image"    # Lcom/vlingo/core/internal/questions/DownloadableImage;
    .param p2, "downloaded"    # Z

    .prologue
    .line 275
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->val$downloadTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 276
    invoke-virtual {p1, p0}, Lcom/vlingo/core/internal/questions/DownloadableImage;->removeListener(Lcom/vlingo/core/internal/questions/DownloadableImage$Listener;)Lcom/vlingo/core/internal/questions/DownloadableImage;

    .line 277
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3$1;-><init>(Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;ZLcom/vlingo/core/internal/questions/DownloadableImage;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 282
    return-void
.end method


# virtual methods
.method public onDownloaded(Lcom/vlingo/core/internal/questions/DownloadableImage;)V
    .locals 1
    .param p1, "image"    # Lcom/vlingo/core/internal/questions/DownloadableImage;

    .prologue
    .line 265
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->updateUI(Lcom/vlingo/core/internal/questions/DownloadableImage;Z)V

    .line 266
    return-void
.end method

.method public onTimeout(Lcom/vlingo/core/internal/questions/DownloadableImage;)V
    .locals 1
    .param p1, "image"    # Lcom/vlingo/core/internal/questions/DownloadableImage;

    .prologue
    .line 271
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/midas/gui/widgets/AnswerQuestionWidget$3;->updateUI(Lcom/vlingo/core/internal/questions/DownloadableImage;Z)V

    .line 272
    return-void
.end method
