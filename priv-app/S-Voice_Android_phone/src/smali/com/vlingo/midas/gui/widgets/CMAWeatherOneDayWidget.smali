.class public Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "CMAWeatherOneDayWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget$CMAWeatherOneDayWidgetHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private accuWeather_Web:Landroid/widget/ImageView;

.field private citi_name:Landroid/widget/TextView;

.field private climate_name:Landroid/widget/TextView;

.field private currentTemp:Ljava/lang/String;

.field private dateString:Ljava/lang/String;

.field private date_value:Landroid/widget/TextView;

.field private deviderImageView:Landroid/widget/TextView;

.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private location:Ljava/lang/String;

.field private mCityDetail:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mCurrentTemp:Landroid/widget/TextView;

.field private mHandler:Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget$CMAWeatherOneDayWidgetHandler;

.field private mTemparature:Landroid/widget/LinearLayout;

.field private mUnit_meter_C:Landroid/widget/ImageView;

.field private mUnit_meter_F:Landroid/widget/ImageView;

.field private maxTemperature1:Landroid/widget/TextView;

.field private minTemperature1:Landroid/widget/TextView;

.field private tempMax:Ljava/lang/String;

.field private tempMin:Ljava/lang/String;

.field private wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

.field private weatherBackgroundImage:Landroid/widget/FrameLayout;

.field private weatherCode:Ljava/lang/String;

.field private weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

.field private weatherImage1:Landroid/widget/ImageView;

.field private weatherPhenomenon:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 232
    new-instance v0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget$CMAWeatherOneDayWidgetHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget$CMAWeatherOneDayWidgetHandler;-><init>(Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget$CMAWeatherOneDayWidgetHandler;

    .line 82
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mContext:Landroid/content/Context;

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->setValuesForcastWeather()V

    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->setTalkbackString()V

    return-void
.end method

.method private onResponseReceived()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 127
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getWeatherElement()Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    move-result-object v7

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    .line 128
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    if-nez v7, :cond_1

    .line 130
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 131
    .local v3, "intent":Landroid/content/Intent;
    const-string/jumbo v7, "com.vlingo.core.internal.dialogmanager.NoData"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v8, 0x0

    invoke-interface {v7, v3, v8}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 197
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getLocationCity()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->location:Ljava/lang/String;

    .line 138
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    .line 140
    .local v0, "currentLocale":Ljava/util/Locale;
    const/4 v6, 0x0

    .line 141
    .local v6, "weatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isToday()Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isDatePlusSeven()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 142
    :cond_2
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-static {v7, v0}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetDate(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->dateString:Ljava/lang/String;

    .line 143
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "weatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    check-cast v6, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    .line 144
    .restart local v6    # "weatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    if-eqz v7, :cond_4

    .line 145
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->temperature:Ljava/lang/String;

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->currentTemp:Ljava/lang/String;

    .line 146
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->weatherPhenomenon:Ljava/lang/String;

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherPhenomenon:Ljava/lang/String;

    .line 147
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherCurrent:Lcom/vlingo/core/internal/weather/china/WeatherCurrent;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/WeatherCurrent;->weatherCode:Ljava/lang/String;

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherCode:Ljava/lang/String;

    .line 181
    :cond_3
    :goto_1
    if-eqz v6, :cond_0

    .line 182
    iget-object v7, v6, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->tempMax:Ljava/lang/String;

    .line 183
    iget-object v7, v6, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->temperature:Ljava/lang/String;

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->tempMin:Ljava/lang/String;

    .line 184
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mContext:Landroid/content/Context;

    const-string/jumbo v8, "com.vlingo.midas"

    const-string/jumbo v9, "WEAT"

    invoke-static {v7, v8, v9}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mHandler:Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget$CMAWeatherOneDayWidgetHandler;

    invoke-virtual {v7, v10}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget$CMAWeatherOneDayWidgetHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 149
    :cond_4
    invoke-direct {p0, v6}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->setPhenomenonWeatherCode(Lcom/vlingo/core/internal/weather/china/WeatherItem;)V

    goto :goto_1

    .line 153
    :cond_5
    const/4 v1, 0x0

    .line 155
    .local v1, "date":Ljava/util/Date;
    :try_start_0
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v7, "yyyy-MM-dd"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 157
    .local v4, "sdfDate":Ljava/text/SimpleDateFormat;
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v7}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->getDate()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 163
    .end local v4    # "sdfDate":Ljava/text/SimpleDateFormat;
    :goto_2
    const/4 v5, 0x0

    .line 164
    .local v5, "weatherElementSize":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    if-eqz v7, :cond_6

    .line 165
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    .line 167
    :cond_6
    const/4 v2, 0x1

    .local v2, "index":I
    :goto_3
    if-ge v2, v5, :cond_8

    .line 168
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/util/Date;->getDate()I

    move-result v8

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/WeatherItem;->date:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getDate()I

    move-result v7

    if-ne v8, v7, :cond_7

    .line 171
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherElement:Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;

    iget-object v7, v7, Lcom/vlingo/core/internal/weather/china/CMAWeatherElement;->weatherItems:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "weatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    check-cast v6, Lcom/vlingo/core/internal/weather/china/WeatherItem;

    .line 167
    .restart local v6    # "weatherItem":Lcom/vlingo/core/internal/weather/china/WeatherItem;
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 175
    :cond_8
    if-eqz v6, :cond_3

    .line 176
    invoke-static {v1, v0}, Lcom/vlingo/core/internal/schedule/DateUtil;->formatWidgetDate(Ljava/util/Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->dateString:Ljava/lang/String;

    .line 177
    invoke-direct {p0, v6}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->setPhenomenonWeatherCode(Lcom/vlingo/core/internal/weather/china/WeatherItem;)V

    goto :goto_1

    .line 158
    .end local v2    # "index":I
    .end local v5    # "weatherElementSize":I
    :catch_0
    move-exception v7

    goto :goto_2
.end method

.method private openChinaWeatherWeb()V
    .locals 2

    .prologue
    .line 334
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 335
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "http://samsung.weather.com.cn/mobile/index.html"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 337
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 338
    return-void
.end method

.method private setPhenomenonWeatherCode(Lcom/vlingo/core/internal/weather/china/WeatherItem;)V
    .locals 1
    .param p1, "weatherItem"    # Lcom/vlingo/core/internal/weather/china/WeatherItem;

    .prologue
    .line 200
    iget-object v0, p1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherPhenomenon:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherPhenomenon:Ljava/lang/String;

    .line 201
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherPhenomenon:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherPhenomenon:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherPhenomenon:Ljava/lang/String;

    .line 203
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherPhenomenon:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherPhenomenon:Ljava/lang/String;

    .line 207
    :cond_0
    iget-object v0, p1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->day:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherCode:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherCode:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p1, Lcom/vlingo/core/internal/weather/china/WeatherItem;->night:Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;

    iget-object v0, v0, Lcom/vlingo/core/internal/weather/china/WeatherTimeItem;->weatherCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherCode:Ljava/lang/String;

    .line 210
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherCode:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    const-string/jumbo v0, "99"

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherCode:Ljava/lang/String;

    .line 214
    :cond_1
    return-void
.end method

.method private setTalkbackString()V
    .locals 5

    .prologue
    .line 302
    const-string/jumbo v0, "C"

    .line 303
    .local v0, "tempUnit":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xb0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 305
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->lowest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 308
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->highest:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 311
    return-void
.end method

.method private setValuesForcastWeather()V
    .locals 10

    .prologue
    const/16 v9, 0x63

    const/16 v8, 0x8

    const/4 v7, 0x0

    const/16 v6, 0xb0

    .line 237
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->tempMin:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 238
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 239
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->tempMin:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->tempMax:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 245
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 246
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->tempMax:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    :cond_1
    :goto_1
    new-instance v2, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherCode:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v2, v3}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;-><init>(I)V

    .line 259
    .local v2, "wru":Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
    invoke-virtual {v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherDrawable()I

    move-result v1

    .line 260
    .local v1, "weatherDrawableCode":I
    if-eq v1, v9, :cond_2

    .line 261
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherImage1:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 264
    :cond_2
    invoke-virtual {v2}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->getWeatherBackgroundDrawable()I

    move-result v0

    .line 265
    .local v0, "weatherBackgroundDrawableCode":I
    if-eq v0, v9, :cond_3

    .line 266
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isNightMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 267
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherBackgroundImage:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Lcom/vlingo/midas/util/CMAWeatherResourceUtil;->changeToNightBackground(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 275
    :cond_3
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->citi_name:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->location:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherPhenomenon:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->date_value:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->dateString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->currentTemp:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    invoke-virtual {v3}, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;->isToday()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 289
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->currentTemp:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mUnit_meter_C:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 291
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mUnit_meter_F:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 297
    :goto_2
    return-void

    .line 241
    .end local v0    # "weatherBackgroundDrawableCode":I
    .end local v1    # "weatherDrawableCode":I
    .end local v2    # "wru":Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
    :cond_4
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->tempMin:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 248
    :cond_5
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->tempMax:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 249
    :cond_6
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->currentTemp:Ljava/lang/String;

    invoke-static {v3}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 250
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 251
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->currentTemp:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 253
    :cond_7
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->currentTemp:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 293
    .restart local v0    # "weatherBackgroundDrawableCode":I
    .restart local v1    # "weatherDrawableCode":I
    .restart local v2    # "wru":Lcom/vlingo/midas/util/CMAWeatherResourceUtil;
    :cond_8
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->tempMax:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mUnit_meter_C:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 295
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mUnit_meter_F:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "adaptor"    # Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 121
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 122
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->wAdaptor:Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    .line 123
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->onResponseReceived()V

    .line 124
    return-void
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 41
    check-cast p1, Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->initialize(Lcom/vlingo/core/internal/weather/china/CMAWeatherAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 327
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 328
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->child_accuweather:I

    if-ne v0, v1, :cond_0

    .line 329
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->openChinaWeatherWeb()V

    .line 331
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 10
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v9, 0x0

    .line 369
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 371
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 372
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_left_right_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 375
    .local v2, "citiRightMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mCityDetail:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_0

    .line 376
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_citi_top_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 378
    .local v3, "citiTopMargin":I
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_citi_max_width:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 380
    .local v1, "citiMaxWidth":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mCityDetail:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9, v3, v2, v9}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 381
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mCityDetail:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v1, v7, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 384
    .end local v1    # "citiMaxWidth":I
    .end local v3    # "citiTopMargin":I
    :cond_0
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    if-eqz v7, :cond_1

    .line 385
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_accu_bottom_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 387
    .local v0, "accuBottomMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v0, v7, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 388
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 391
    .end local v0    # "accuBottomMargin":I
    :cond_1
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_2

    .line 392
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_temprature_bottom_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 394
    .local v6, "tempBottomMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v6, v7, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 395
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 398
    .end local v6    # "tempBottomMargin":I
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_climate_bottom_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 400
    .local v4, "climateBottomMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v4, v7, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 401
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 404
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->widget_weather_icon_top_margin:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 406
    .local v5, "iconTopMargin":I
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherImage1:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v5, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 407
    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherImage1:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 409
    .end local v2    # "citiRightMargin":I
    .end local v4    # "climateBottomMargin":I
    .end local v5    # "iconTopMargin":I
    :cond_3
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 89
    :try_start_0
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 90
    sget v0, Lcom/vlingo/midas/R$id;->citi_name:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->citi_name:Landroid/widget/TextView;

    .line 91
    sget v0, Lcom/vlingo/midas/R$id;->imageView_singledayweather:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherImage1:Landroid/widget/ImageView;

    .line 92
    sget v0, Lcom/vlingo/midas/R$id;->date_view:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->date_value:Landroid/widget/TextView;

    .line 93
    sget v0, Lcom/vlingo/midas/R$id;->climate_name:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    .line 94
    sget v0, Lcom/vlingo/midas/R$id;->min_temp:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    .line 95
    sget v0, Lcom/vlingo/midas/R$id;->max_temp:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    .line 96
    sget v0, Lcom/vlingo/midas/R$id;->present_temp:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    .line 97
    sget v0, Lcom/vlingo/midas/R$id;->unit_meter_F:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mUnit_meter_F:Landroid/widget/ImageView;

    .line 98
    sget v0, Lcom/vlingo/midas/R$id;->unit_meter_C:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mUnit_meter_C:Landroid/widget/ImageView;

    .line 99
    sget v0, Lcom/vlingo/midas/R$id;->divider_small:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->deviderImageView:Landroid/widget/TextView;

    .line 100
    sget v0, Lcom/vlingo/midas/R$id;->widget_citi:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherBackgroundImage:Landroid/widget/FrameLayout;

    .line 101
    sget v0, Lcom/vlingo/midas/R$id;->citi_details:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mCityDetail:Landroid/widget/LinearLayout;

    .line 102
    sget v0, Lcom/vlingo/midas/R$id;->temprature:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    .line 103
    sget v0, Lcom/vlingo/midas/R$id;->child_accuweather:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    .line 104
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->accuWeather_Web:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    return-void

    .line 105
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 115
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 116
    return-void
.end method

.method public setMinimizeWindow()V
    .locals 12

    .prologue
    const/16 v11, 0x33

    const/high16 v10, 0x41c80000    # 25.0f

    const/high16 v9, 0x41c00000    # 24.0f

    const/4 v8, 0x0

    const/4 v7, -0x2

    .line 341
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherBackgroundImage:Landroid/widget/FrameLayout;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/16 v5, 0x30c

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 343
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->weatherImage1:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v4, 0x2af

    const/16 v5, 0x285

    const/4 v6, 0x5

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 345
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->date_value:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setTextSize(F)V

    .line 346
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->citi_name:Landroid/widget/TextView;

    const/high16 v3, 0x420c0000    # 35.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 348
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 349
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x50

    invoke-direct {v0, v7, v7, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 351
    .local v0, "cn":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v2, 0x30

    invoke-virtual {v0, v11, v8, v8, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 352
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->climate_name:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 354
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mCurrentTemp:Landroid/widget/TextView;

    const/high16 v3, 0x42200000    # 40.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 357
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->minTemperature1:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextSize(F)V

    .line 358
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->maxTemperature1:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTextSize(F)V

    .line 359
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->deviderImageView:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setTextSize(F)V

    .line 361
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x50

    invoke-direct {v1, v7, v7, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 363
    .local v1, "tp":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v2, 0x63

    invoke-virtual {v1, v11, v8, v8, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 364
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/CMAWeatherOneDayWidget;->mTemparature:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 365
    return-void
.end method
