.class Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;
.super Ljava/lang/Thread;
.source "DialogFragmentEWYSHelp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimateDialogExpandThread"
.end annotation


# instance fields
.field final delta:F

.field private final dialogFragment:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;",
            ">;"
        }
    .end annotation
.end field

.field final frames:F

.field final initialHeight:I

.field final refreshRate:F


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;FFIF)V
    .locals 1
    .param p1, "dialogFragment"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .param p2, "refreshRate"    # F
    .param p3, "frames"    # F
    .param p4, "initialHeight"    # I
    .param p5, "delta"    # F

    .prologue
    .line 1241
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1242
    iput p2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->refreshRate:F

    .line 1243
    iput p3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->frames:F

    .line 1244
    iput p4, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->initialHeight:I

    .line 1245
    iput p5, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->delta:F

    .line 1247
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    .line 1249
    return-void
.end method

.method static synthetic access$800(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;

    .prologue
    .line 1232
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1254
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->mActivityCreated:Z
    invoke-static {v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$400(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Z

    move-result v1

    .line 1257
    .local v1, "mActivityCreated":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    int-to-float v2, v0

    :try_start_0
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->frames:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    if-eqz v1, :cond_0

    .line 1258
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->initialHeight:I

    int-to-float v3, v3

    int-to-float v4, v0

    iget v5, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->delta:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    # invokes: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->setDialogHeight(F)V
    invoke-static {v2, v3}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$600(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;F)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1261
    const/high16 v2, 0x447a0000    # 1000.0f

    :try_start_1
    iget v3, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->refreshRate:F

    div-float/2addr v2, v3

    float-to-long v2, v2

    invoke-static {v2, v3}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1257
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1269
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # setter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isFullScreen:Z
    invoke-static {v2, v6}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$702(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;Z)Z

    .line 1270
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$200(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/ScrollView;

    move-result-object v2

    new-instance v3, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread$1;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;)V

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 1279
    :goto_2
    return-void

    .line 1266
    :catch_0
    move-exception v2

    .line 1269
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # setter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isFullScreen:Z
    invoke-static {v2, v6}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$702(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;Z)Z

    .line 1270
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$200(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/ScrollView;

    move-result-object v2

    new-instance v3, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread$1;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;)V

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 1269
    :catchall_0
    move-exception v2

    move-object v3, v2

    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # setter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->isFullScreen:Z
    invoke-static {v2, v6}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$702(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;Z)Z

    .line 1270
    iget-object v2, p0, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;->dialogFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    # getter for: Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v2}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->access$200(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;)Landroid/widget/ScrollView;

    move-result-object v2

    new-instance v4, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread$1;

    invoke-direct {v4, p0}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread$1;-><init>(Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp$AnimateDialogExpandThread;)V

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    throw v3

    .line 1262
    :catch_1
    move-exception v2

    goto :goto_1
.end method
