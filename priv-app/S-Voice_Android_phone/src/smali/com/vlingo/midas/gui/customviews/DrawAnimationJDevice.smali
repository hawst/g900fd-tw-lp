.class public Lcom/vlingo/midas/gui/customviews/DrawAnimationJDevice;
.super Ljava/lang/Object;
.source "DrawAnimationJDevice.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V
    .locals 7
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "cornerBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "squareImage"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p3, "mRms"    # F

    .prologue
    const/high16 v6, 0x43160000    # 150.0f

    const/high16 v5, 0x43020000    # 130.0f

    const/high16 v4, 0x42f00000    # 120.0f

    const/high16 v3, 0x433e0000    # 190.0f

    const/4 v2, 0x0

    .line 416
    const/high16 v0, 0x42100000    # 36.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 418
    const/high16 v0, 0x42200000    # 40.0f

    const/high16 v1, 0x43c30000    # 390.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 419
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x43c80000    # 400.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 420
    const/high16 v0, 0x42700000    # 60.0f

    const/high16 v1, 0x43cd0000    # 410.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 421
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x43d70000    # 430.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 423
    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x43dc0000    # 440.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 424
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x43d70000    # 430.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 425
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x43d20000    # 420.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 426
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43cd0000    # 410.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 427
    const/high16 v0, 0x43c80000    # 400.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 428
    const/high16 v0, 0x43c30000    # 390.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 429
    const/high16 v0, 0x430c0000    # 140.0f

    const/high16 v1, 0x43c80000    # 400.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 430
    const/high16 v0, 0x43cd0000    # 410.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 431
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x43d20000    # 420.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 432
    const/high16 v0, 0x432a0000    # 170.0f

    const/high16 v1, 0x43d70000    # 430.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 433
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43dc0000    # 440.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 435
    const/high16 v0, 0x43d70000    # 430.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 436
    const/high16 v0, 0x43480000    # 200.0f

    const/high16 v1, 0x43cd0000    # 410.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 437
    const/high16 v0, 0x43520000    # 210.0f

    const/high16 v1, 0x43c30000    # 390.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 439
    const/16 v0, 0x3c

    const/16 v1, 0x1b8

    const/16 v2, 0x104

    const/16 v3, 0x1ea

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 440
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 662
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    const/high16 v0, 0x422c0000    # 43.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 446
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 447
    const/high16 v0, 0x42700000    # 60.0f

    const/high16 v1, 0x43af0000    # 350.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 448
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x43b40000    # 360.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 450
    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x43b90000    # 370.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 451
    const/high16 v0, 0x42b40000    # 90.0f

    const v1, 0x43b18000    # 355.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 452
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 453
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43a50000    # 330.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 454
    const/high16 v0, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 455
    const v0, 0x43b18000    # 355.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 456
    const/high16 v0, 0x430c0000    # 140.0f

    const/high16 v1, 0x43b90000    # 370.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 457
    const/high16 v0, 0x43b40000    # 360.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 458
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x43af0000    # 350.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 459
    const/high16 v0, 0x432a0000    # 170.0f

    const/high16 v1, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 460
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43a50000    # 330.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 462
    const/high16 v0, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 463
    const/high16 v0, 0x43480000    # 200.0f

    const/high16 v1, 0x43af0000    # 350.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 464
    const/high16 v0, 0x43520000    # 210.0f

    const/high16 v1, 0x43b40000    # 360.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 465
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43b90000    # 370.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 467
    const/16 v0, 0x3c

    const/16 v1, 0x172

    const/16 v2, 0x104

    const/16 v3, 0x1ea

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 468
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 470
    :cond_2
    const/high16 v0, 0x42440000    # 49.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_3

    .line 471
    const/high16 v0, 0x42200000    # 40.0f

    const/high16 v1, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 472
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x43a00000    # 320.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 473
    const/high16 v0, 0x42700000    # 60.0f

    const/high16 v1, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 474
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x43a00000    # 320.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 476
    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 477
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x43a50000    # 330.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 478
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x43a00000    # 320.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 479
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x439b0000    # 310.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 480
    const/high16 v0, 0x43a00000    # 320.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 481
    const/high16 v0, 0x43a50000    # 330.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 482
    const/high16 v0, 0x430c0000    # 140.0f

    const/high16 v1, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 485
    const/high16 v0, 0x43a50000    # 330.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 486
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x43a00000    # 320.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 487
    const/high16 v0, 0x432a0000    # 170.0f

    const/high16 v1, 0x439b0000    # 310.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 488
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 490
    const/high16 v0, 0x439b0000    # 310.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 491
    const/high16 v0, 0x43480000    # 200.0f

    const/high16 v1, 0x43a00000    # 320.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 492
    const/high16 v0, 0x43520000    # 210.0f

    const/high16 v1, 0x43a50000    # 330.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 493
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 495
    const/16 v0, 0x3c

    const/16 v1, 0x154

    const/16 v2, 0x104

    const/16 v3, 0x1ea

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 496
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 499
    :cond_3
    const/high16 v0, 0x42600000    # 56.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    .line 500
    const/high16 v0, 0x42200000    # 40.0f

    const/high16 v1, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 501
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 502
    const/high16 v0, 0x42700000    # 60.0f

    const/high16 v1, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 503
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 505
    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 506
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 507
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 508
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 509
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 510
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 511
    const/high16 v0, 0x430c0000    # 140.0f

    const/high16 v1, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 512
    const/high16 v0, 0x43910000    # 290.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 513
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 514
    const/high16 v0, 0x432a0000    # 170.0f

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 515
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 517
    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 518
    const/high16 v0, 0x43480000    # 200.0f

    const/high16 v1, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 519
    const/high16 v0, 0x43520000    # 210.0f

    const/high16 v1, 0x43910000    # 290.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 520
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 522
    const/16 v0, 0x3c

    const/16 v1, 0x12c

    const/16 v2, 0x104

    const/16 v3, 0x1ea

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 523
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 526
    :cond_4
    const/high16 v0, 0x42780000    # 62.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_5

    .line 527
    const/high16 v0, 0x42200000    # 40.0f

    const/high16 v1, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 528
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 529
    const/high16 v0, 0x42700000    # 60.0f

    const/high16 v1, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 530
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 532
    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 533
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 534
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 536
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x437a0000    # 250.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 537
    const/high16 v0, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 538
    const/high16 v0, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 539
    const/high16 v0, 0x430c0000    # 140.0f

    const/high16 v1, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 540
    const/high16 v0, 0x437a0000    # 250.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 541
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 542
    const/high16 v0, 0x432a0000    # 170.0f

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 543
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 545
    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 546
    const/high16 v0, 0x43480000    # 200.0f

    const/high16 v1, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 547
    const/high16 v0, 0x43520000    # 210.0f

    const/high16 v1, 0x437a0000    # 250.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 548
    const/high16 v0, 0x43520000    # 210.0f

    const/high16 v1, 0x43820000    # 260.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 550
    const/16 v0, 0x3c

    const/16 v1, 0x118

    const/16 v2, 0x104

    const/16 v3, 0x1ea

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 551
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 552
    :cond_5
    const/high16 v0, 0x42880000    # 68.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6

    .line 553
    const/high16 v0, 0x42200000    # 40.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 554
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 555
    const/high16 v0, 0x42700000    # 60.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 556
    const/high16 v0, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 558
    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 559
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x43390000    # 185.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 560
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 561
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 562
    const/high16 v0, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 563
    const/high16 v0, 0x43390000    # 185.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 564
    const/high16 v0, 0x430c0000    # 140.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 565
    const/high16 v0, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 566
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 567
    const/high16 v0, 0x432a0000    # 170.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 568
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 570
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 571
    const/high16 v0, 0x43480000    # 200.0f

    const/high16 v1, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 572
    const/high16 v0, 0x43520000    # 210.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 573
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 575
    const/16 v0, 0x3c

    const/16 v1, 0xc8

    const/16 v2, 0x104

    const/16 v3, 0x1ea

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 576
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 577
    :cond_6
    const/high16 v0, 0x42940000    # 74.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_7

    .line 578
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 579
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x43250000    # 165.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 580
    const/high16 v0, 0x42700000    # 60.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 581
    const/high16 v0, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 583
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 584
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 585
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 586
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 587
    const/high16 v0, 0x43250000    # 165.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 588
    invoke-virtual {p0, p1, v5, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 589
    const/high16 v0, 0x430c0000    # 140.0f

    const/high16 v1, 0x43250000    # 165.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 590
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 591
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 592
    const/high16 v0, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 593
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 595
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 596
    const/high16 v0, 0x43480000    # 200.0f

    const/high16 v1, 0x43250000    # 165.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 597
    const/high16 v0, 0x43520000    # 210.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 598
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 600
    const/16 v0, 0x3c

    const/16 v1, 0xbe

    const/16 v2, 0x104

    const/16 v3, 0x1ea

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 601
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 602
    :cond_7
    const/high16 v0, 0x42a20000    # 81.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_8

    .line 603
    const/high16 v0, 0x42200000    # 40.0f

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 604
    const/high16 v0, 0x42480000    # 50.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 605
    const/high16 v0, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 606
    const/high16 v0, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 608
    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 609
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 610
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 611
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 612
    invoke-virtual {p0, p1, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 613
    invoke-virtual {p0, p1, v5, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 614
    const/high16 v0, 0x430c0000    # 140.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 615
    invoke-virtual {p0, p1, v6, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 616
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 617
    const/high16 v0, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 618
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 620
    invoke-virtual {p0, p1, v3, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 621
    const/high16 v0, 0x43480000    # 200.0f

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 622
    const/high16 v0, 0x43520000    # 210.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 623
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 625
    const/16 v0, 0x3c

    const/16 v1, 0x96

    const/16 v2, 0x104

    const/16 v3, 0x1ea

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 626
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 629
    :cond_8
    const/high16 v0, 0x42b00000    # 88.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_9

    .line 630
    const/high16 v0, 0x42200000    # 40.0f

    const/high16 v1, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 631
    const/high16 v0, 0x42480000    # 50.0f

    const/high16 v1, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 632
    const/high16 v0, 0x42700000    # 60.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 633
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 635
    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 636
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42be0000    # 95.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 637
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 638
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x42d20000    # 105.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 639
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 640
    const/high16 v0, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 641
    const/high16 v0, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 642
    invoke-virtual {p0, p1, v6, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 643
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 644
    const/high16 v0, 0x432a0000    # 170.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 645
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 647
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 648
    const/high16 v0, 0x43480000    # 200.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 649
    const/high16 v0, 0x43520000    # 210.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 650
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 652
    const/16 v0, 0x3c

    const/16 v1, 0x78

    const/16 v2, 0x104

    const/16 v3, 0x1ea

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 653
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 657
    :cond_9
    const/high16 v0, 0x42b00000    # 88.0f

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 658
    const/16 v0, 0x3c

    const/16 v1, 0x3c

    const/16 v2, 0x104

    const/16 v3, 0x1ea

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 659
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method public static drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V
    .locals 7
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "cornerBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "squareImage"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p3, "mRms"    # F

    .prologue
    const/high16 v6, 0x433e0000    # 190.0f

    const/high16 v5, 0x43520000    # 210.0f

    const/high16 v4, 0x42f00000    # 120.0f

    const/high16 v3, 0x432a0000    # 170.0f

    const/4 v2, 0x0

    .line 10
    const/high16 v0, 0x42100000    # 36.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 12
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 13
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 14
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 15
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 16
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 17
    invoke-virtual {p0, p1, v3, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 18
    const/high16 v0, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 19
    const/high16 v0, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 20
    const/high16 v0, 0x43660000    # 230.0f

    const/high16 v1, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 23
    const/high16 v0, 0x430c0000    # 140.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 24
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 25
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 28
    const/high16 v0, 0x436b0000    # 235.0f

    const/high16 v1, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 29
    const/high16 v0, 0x437f0000    # 255.0f

    const/high16 v1, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 30
    const v0, 0x43898000    # 275.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 31
    const v0, 0x43938000    # 295.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 32
    const v0, 0x439d8000    # 315.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 33
    const v0, 0x43a78000    # 335.0f

    const/high16 v1, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 34
    const v0, 0x43b18000    # 355.0f

    const/high16 v1, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 36
    const/high16 v0, 0x43b40000    # 360.0f

    const/high16 v1, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 37
    const/high16 v0, 0x43be0000    # 380.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 38
    const/high16 v0, 0x43cd0000    # 410.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 39
    const/high16 v0, 0x43d20000    # 420.0f

    const/high16 v1, 0x43570000    # 215.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 41
    const/16 v0, 0x46

    const/16 v1, 0xf0

    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 42
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    const/high16 v0, 0x422c0000    # 43.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 45
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 46
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 47
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 48
    invoke-virtual {p0, p1, v4, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 52
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 53
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 54
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 58
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 59
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 60
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 61
    const/high16 v0, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 63
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 64
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 65
    const/high16 v0, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 66
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 67
    const/high16 v0, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 70
    const/high16 v0, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 71
    const/high16 v0, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 72
    const/high16 v0, 0x437a0000    # 250.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 74
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 75
    const/high16 v0, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 76
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 77
    const/high16 v0, 0x439b0000    # 310.0f

    const/high16 v1, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 80
    const/high16 v0, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 81
    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 82
    const/high16 v0, 0x43a00000    # 320.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 85
    const/high16 v0, 0x439b0000    # 310.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 86
    const/high16 v0, 0x43a00000    # 320.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 87
    const/high16 v0, 0x43a50000    # 330.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 89
    const/high16 v0, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 90
    const/high16 v0, 0x43b40000    # 360.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 91
    const/high16 v0, 0x43be0000    # 380.0f

    const/high16 v1, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 93
    const/high16 v0, 0x43b90000    # 370.0f

    const/high16 v1, 0x435c0000    # 220.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 94
    const/high16 v0, 0x43be0000    # 380.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 95
    const/high16 v0, 0x43c30000    # 390.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 96
    const/high16 v0, 0x43c80000    # 400.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 98
    const/16 v0, 0x46

    const/16 v1, 0xe6

    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 99
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 101
    :cond_2
    const/high16 v0, 0x42440000    # 49.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_3

    .line 102
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x432f0000    # 175.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 103
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 104
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 105
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 106
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x432f0000    # 175.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 107
    invoke-virtual {p0, p1, v3, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 111
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 112
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 113
    invoke-virtual {p0, p1, v4, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 115
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 116
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 117
    const/high16 v0, 0x43480000    # 200.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 118
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 119
    const/high16 v0, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 120
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 121
    const/high16 v0, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 124
    const/high16 v0, 0x432f0000    # 175.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 125
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x432f0000    # 175.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 126
    const/high16 v0, 0x43660000    # 230.0f

    const/high16 v1, 0x432f0000    # 175.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 128
    const/high16 v0, 0x43700000    # 240.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 129
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 130
    const/high16 v0, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 131
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 132
    const/high16 v0, 0x43a00000    # 320.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 133
    const/high16 v0, 0x43aa0000    # 340.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 134
    const/high16 v0, 0x43b40000    # 360.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 137
    const/high16 v0, 0x43910000    # 290.0f

    const/high16 v1, 0x432f0000    # 175.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 138
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x432f0000    # 175.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 139
    const/high16 v0, 0x439b0000    # 310.0f

    const/high16 v1, 0x432f0000    # 175.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 142
    const/high16 v0, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 143
    const/high16 v0, 0x43b40000    # 360.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 144
    const/high16 v0, 0x43be0000    # 380.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 146
    const/high16 v0, 0x43c30000    # 390.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 147
    const/high16 v0, 0x43cd0000    # 410.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 148
    const/high16 v0, 0x43d20000    # 420.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 150
    const/16 v0, 0x46

    const/16 v1, 0xbe

    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 151
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 152
    :cond_3
    const/high16 v0, 0x42600000    # 56.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    .line 153
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 154
    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 155
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 156
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 157
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 160
    const/high16 v0, 0x430c0000    # 140.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 161
    const/high16 v0, 0x43200000    # 160.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 162
    const/high16 v0, 0x43340000    # 180.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 163
    const/high16 v0, 0x43480000    # 200.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 164
    const/high16 v0, 0x435c0000    # 220.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 168
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 169
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 170
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 173
    const/high16 v0, 0x43700000    # 240.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 174
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 175
    const/high16 v0, 0x438c0000    # 280.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 176
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 177
    const/high16 v0, 0x43a00000    # 320.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 180
    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 181
    const/high16 v0, 0x438c0000    # 280.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 182
    const/high16 v0, 0x43910000    # 290.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 185
    const/high16 v0, 0x43b40000    # 360.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 186
    const/high16 v0, 0x43be0000    # 380.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 188
    const/high16 v0, 0x43c80000    # 400.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 189
    const/high16 v0, 0x43d20000    # 420.0f

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 192
    const/16 v0, 0x46

    const/16 v1, 0xb4

    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 193
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 194
    :cond_4
    const/high16 v0, 0x42780000    # 62.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_5

    .line 195
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 196
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 197
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 198
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 199
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 200
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 204
    const/high16 v0, 0x42c80000    # 100.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 205
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 206
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 209
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 210
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 211
    const/high16 v0, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 212
    const/high16 v0, 0x43700000    # 240.0f

    const/high16 v1, 0x431e0000    # 158.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 214
    const/high16 v0, 0x437a0000    # 250.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 216
    const/high16 v0, 0x43820000    # 260.0f

    const/high16 v1, 0x43040000    # 132.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 219
    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 221
    const/high16 v0, 0x438c0000    # 280.0f

    const/high16 v1, 0x43040000    # 132.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 224
    const/high16 v0, 0x43910000    # 290.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 225
    const/high16 v0, 0x43960000    # 300.0f

    const/high16 v1, 0x431e0000    # 158.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 227
    const/high16 v0, 0x439b0000    # 310.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 228
    const/high16 v0, 0x43a50000    # 330.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 229
    const/high16 v0, 0x43aa0000    # 340.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 231
    const/high16 v0, 0x43af0000    # 350.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 233
    const/high16 v0, 0x43b90000    # 370.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 235
    const/high16 v0, 0x43c30000    # 390.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 236
    const/high16 v0, 0x43cd0000    # 410.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 237
    const/high16 v0, 0x43d20000    # 420.0f

    const/high16 v1, 0x431b0000    # 155.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 238
    const/high16 v0, 0x43d70000    # 430.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 241
    const/16 v0, 0x46

    const/16 v1, 0xaa

    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 242
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 244
    :cond_5
    const/high16 v0, 0x42880000    # 68.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6

    .line 245
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 246
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x43110000    # 145.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 247
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 248
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 249
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 250
    const/high16 v0, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 251
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 253
    const/high16 v0, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 254
    const/high16 v0, 0x43660000    # 230.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 255
    const/high16 v0, 0x437a0000    # 250.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 256
    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 257
    const/high16 v0, 0x43910000    # 290.0f

    const/high16 v1, 0x430c0000    # 140.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 258
    const/high16 v0, 0x439b0000    # 310.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 260
    const/high16 v0, 0x43910000    # 290.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 261
    const/high16 v0, 0x439b0000    # 310.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 264
    const/high16 v0, 0x43a00000    # 320.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 265
    const/high16 v0, 0x43a50000    # 330.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 266
    const/high16 v0, 0x43aa0000    # 340.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 269
    const/high16 v0, 0x43a50000    # 330.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 270
    const/high16 v0, 0x43af0000    # 350.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 271
    const/high16 v0, 0x43b90000    # 370.0f

    const/high16 v1, 0x43110000    # 145.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 273
    const/high16 v0, 0x43c30000    # 390.0f

    const/high16 v1, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 275
    const/high16 v0, 0x43cd0000    # 410.0f

    const/high16 v1, 0x43110000    # 145.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 276
    const/high16 v0, 0x43d70000    # 430.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 278
    const/16 v0, 0x46

    const/16 v1, 0xa0

    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 279
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 280
    :cond_6
    const/high16 v0, 0x42940000    # 74.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_7

    .line 281
    const/high16 v0, 0x42700000    # 60.0f

    const/high16 v1, 0x42fa0000    # 125.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 282
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 283
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42fa0000    # 125.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 284
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 287
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 288
    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 289
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 292
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x43070000    # 135.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 293
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 294
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 297
    const/high16 v0, 0x43200000    # 160.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 298
    invoke-virtual {p0, p1, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 299
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 302
    invoke-virtual {p0, p1, v6, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 303
    const/high16 v0, 0x43070000    # 135.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 304
    const/high16 v0, 0x43660000    # 230.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 305
    const/high16 v0, 0x437a0000    # 250.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 307
    const/high16 v0, 0x43870000    # 270.0f

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 308
    const/high16 v0, 0x43910000    # 290.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 309
    const/high16 v0, 0x439b0000    # 310.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 310
    const/high16 v0, 0x43a50000    # 330.0f

    const/high16 v1, 0x43070000    # 135.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 311
    const/high16 v0, 0x43af0000    # 350.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 313
    const/high16 v0, 0x43b90000    # 370.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 315
    const/high16 v0, 0x43b40000    # 360.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 316
    const/high16 v0, 0x43b90000    # 370.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 317
    const/high16 v0, 0x43be0000    # 380.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 320
    const/high16 v0, 0x43c30000    # 390.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 321
    const/high16 v0, 0x43cd0000    # 410.0f

    const/high16 v1, 0x43070000    # 135.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 322
    const/high16 v0, 0x43d70000    # 430.0f

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 325
    const/16 v0, 0x46

    const/16 v1, 0x96

    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 326
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 327
    :cond_7
    const/high16 v0, 0x42a20000    # 81.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_8

    .line 328
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 329
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 330
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 332
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 334
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 335
    const/high16 v0, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 337
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 339
    invoke-virtual {p0, p1, v5, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 340
    const/high16 v0, 0x43660000    # 230.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 342
    const/high16 v0, 0x437a0000    # 250.0f

    const/high16 v1, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 344
    const/high16 v0, 0x43870000    # 270.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 347
    const/high16 v0, 0x43910000    # 290.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 350
    const/high16 v0, 0x43870000    # 270.0f

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 352
    const/high16 v0, 0x439b0000    # 310.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 354
    const/high16 v0, 0x43a50000    # 330.0f

    const/high16 v1, 0x42e60000    # 115.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 355
    const/high16 v0, 0x43af0000    # 350.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 357
    const/high16 v0, 0x43b90000    # 370.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 359
    const/high16 v0, 0x43c30000    # 390.0f

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 360
    const/high16 v0, 0x43cd0000    # 410.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 361
    const/high16 v0, 0x43d70000    # 430.0f

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 362
    const/high16 v0, 0x43e10000    # 450.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 363
    const/high16 v0, 0x43eb0000    # 470.0f

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 366
    const/16 v0, 0x46

    const/16 v1, 0x82

    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 367
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 368
    :cond_8
    const/high16 v0, 0x42b00000    # 88.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_9

    .line 369
    const/high16 v0, 0x428c0000    # 70.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 370
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42be0000    # 95.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 371
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 373
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 375
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 376
    const/high16 v0, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 378
    const/high16 v0, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 380
    const/high16 v0, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v5, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 381
    const/high16 v0, 0x43660000    # 230.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 383
    const/high16 v0, 0x437a0000    # 250.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 385
    const/high16 v0, 0x43870000    # 270.0f

    const/high16 v1, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 388
    const/high16 v0, 0x43910000    # 290.0f

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 389
    const/high16 v0, 0x439b0000    # 310.0f

    const/high16 v1, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 391
    const/high16 v0, 0x43a50000    # 330.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 393
    const/high16 v0, 0x43af0000    # 350.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 395
    const/high16 v0, 0x43b90000    # 370.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 396
    const/high16 v0, 0x43c30000    # 390.0f

    const/high16 v1, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 398
    const/high16 v0, 0x43cd0000    # 410.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 400
    const/high16 v0, 0x43d70000    # 430.0f

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 403
    const/high16 v0, 0x43e10000    # 450.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 405
    const/high16 v0, 0x43eb0000    # 470.0f

    const/high16 v1, 0x428c0000    # 70.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 406
    const/16 v0, 0x46

    const/16 v1, 0x64

    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 407
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 408
    :cond_9
    const/high16 v0, 0x42b00000    # 88.0f

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 409
    const/16 v0, 0x46

    const/16 v1, 0x3c

    const/16 v2, 0x1e0

    const/16 v3, 0x104

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 410
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method
