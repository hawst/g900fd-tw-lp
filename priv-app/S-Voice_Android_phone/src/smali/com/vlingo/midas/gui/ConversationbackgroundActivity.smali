.class public Lcom/vlingo/midas/gui/ConversationbackgroundActivity;
.super Landroid/app/Activity;
.source "ConversationbackgroundActivity.java"


# instance fields
.field private hasPaused:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationbackgroundActivity;->hasPaused:Z

    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationbackgroundActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 38
    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 15
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 16
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/ConversationbackgroundActivity;->startActivity(Landroid/content/Intent;)V

    .line 17
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationbackgroundActivity;->hasPaused:Z

    .line 32
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 22
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 23
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/ConversationbackgroundActivity;->hasPaused:Z

    if-eqz v0, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/ConversationbackgroundActivity;->finish()V

    .line 25
    :cond_0
    return-void
.end method
