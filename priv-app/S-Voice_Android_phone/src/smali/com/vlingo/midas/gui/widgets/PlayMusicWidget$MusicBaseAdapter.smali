.class Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;
.super Landroid/widget/BaseAdapter;
.source "PlayMusicWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MusicBaseAdapter"
.end annotation


# instance fields
.field private arrayMusic:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;"
        }
    .end annotation
.end field

.field private layoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p3, "arrayMusic":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 92
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 93
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->arrayMusic:Ljava/util/List;

    .line 94
    return-void
.end method

.method private imageBitmapProcessing(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;ILandroid/view/View;Landroid/view/ViewGroup;)V
    .locals 16
    .param p1, "audioHolder"    # Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;
    .param p2, "position"    # I
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 165
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->COUNTER_HASHMAP_ENTRY:I
    invoke-static {v12}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$200(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I

    move-result v12

    const/4 v13, 0x5

    if-le v12, v13, :cond_0

    .line 166
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    const/4 v13, 0x0

    # setter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->COUNTER_HASHMAP_ENTRY:I
    invoke-static {v12, v13}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$202(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;I)I

    .line 170
    :cond_0
    const/4 v5, 0x0

    .line 171
    .local v5, "inWidth":I
    const/4 v3, 0x0

    .line 173
    .local v3, "inHeight":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->context:Landroid/content/Context;
    invoke-static {v12}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->arrayMusic:Ljava/util/List;

    move/from16 v0, p2

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    invoke-virtual {v12}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getMusicThumbnail()Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v13, v12}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 177
    .local v2, "in":Ljava/io/InputStream;
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 178
    .local v7, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v12, 0x1

    iput-boolean v12, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 179
    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v12, v7, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 180
    const/4 v12, 0x0

    invoke-static {v2, v12, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 181
    if-eqz v2, :cond_1

    .line 182
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 184
    :cond_1
    const/4 v2, 0x0

    .line 187
    iget v5, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 188
    iget v3, v7, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 191
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->context:Landroid/content/Context;
    invoke-static {v12}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->arrayMusic:Ljava/util/List;

    move/from16 v0, p2

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    invoke-virtual {v12}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getMusicThumbnail()Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v13, v12}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 192
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 194
    .restart local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    div-int/lit8 v12, v5, 0x64

    div-int/lit8 v13, v3, 0x64

    invoke-static {v12, v13}, Ljava/lang/Math;->max(II)I

    move-result v12

    iput v12, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 196
    const/4 v12, 0x0

    invoke-static {v2, v12, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 198
    .local v10, "roughBitmap":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_5

    .line 200
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 201
    .local v6, "m":Landroid/graphics/Matrix;
    new-instance v4, Landroid/graphics/RectF;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    int-to-float v14, v14

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    int-to-float v15, v15

    invoke-direct {v4, v12, v13, v14, v15}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 202
    .local v4, "inRect":Landroid/graphics/RectF;
    new-instance v8, Landroid/graphics/RectF;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/high16 v14, 0x42dc0000    # 110.0f

    const/high16 v15, 0x42dc0000    # 110.0f

    invoke-direct {v8, v12, v13, v14, v15}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 203
    .local v8, "outRect":Landroid/graphics/RectF;
    sget-object v12, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v6, v4, v8, v12}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 204
    const/16 v12, 0x9

    new-array v11, v12, [F

    .line 205
    .local v11, "values":[F
    invoke-virtual {v6, v11}, Landroid/graphics/Matrix;->getValues([F)V

    .line 207
    const/4 v9, 0x0

    .line 209
    .local v9, "resizedBitmap":Landroid/graphics/Bitmap;
    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mHashMap:Lcom/vlingo/core/internal/util/SparseArrayMap;
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$300()Lcom/vlingo/core/internal/util/SparseArrayMap;

    move-result-object v12

    if-eqz v12, :cond_4

    .line 210
    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mHashMap:Lcom/vlingo/core/internal/util/SparseArrayMap;
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$300()Lcom/vlingo/core/internal/util/SparseArrayMap;

    move-result-object v12

    invoke-virtual {v12, v9}, Lcom/vlingo/core/internal/util/SparseArrayMap;->containsValue(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->FIRST_TIME:I
    invoke-static {v12}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$400(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_4

    .line 211
    :cond_2
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    const/4 v13, 0x0

    aget v13, v11, v13

    mul-float/2addr v12, v13

    float-to-int v12, v12

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    int-to-float v13, v13

    const/4 v14, 0x4

    aget v14, v11, v14

    mul-float/2addr v13, v14

    float-to-int v13, v13

    const/4 v14, 0x1

    invoke-static {v10, v12, v13, v14}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 214
    if-eqz v9, :cond_3

    .line 215
    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mHashMap:Lcom/vlingo/core/internal/util/SparseArrayMap;
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$300()Lcom/vlingo/core/internal/util/SparseArrayMap;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->COUNTER_HASHMAP_ENTRY:I
    invoke-static {v13}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$200(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I

    move-result v13

    invoke-virtual {v12, v13, v9}, Lcom/vlingo/core/internal/util/SparseArrayMap;->put(ILjava/lang/Object;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->COUNTER_HASHMAP_ENTRY:I
    invoke-static {v13}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$200(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I

    move-result v13

    # setter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->VALUE_COUNTER_HASHMAP:I
    invoke-static {v12, v13}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$502(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;I)I

    .line 217
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    # operator++ for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->COUNTER_HASHMAP_ENTRY:I
    invoke-static {v12}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$208(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I

    .line 219
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    # operator++ for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->FIRST_TIME:I
    invoke-static {v12}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$408(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I

    .line 223
    :cond_4
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    const/4 v10, 0x0

    .line 241
    :try_start_1
    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mHashMap:Lcom/vlingo/core/internal/util/SparseArrayMap;
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$300()Lcom/vlingo/core/internal/util/SparseArrayMap;

    move-result-object v12

    if-eqz v12, :cond_5

    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mHashMap:Lcom/vlingo/core/internal/util/SparseArrayMap;
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$300()Lcom/vlingo/core/internal/util/SparseArrayMap;

    move-result-object v12

    invoke-virtual {v12, v9}, Lcom/vlingo/core/internal/util/SparseArrayMap;->containsValue(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 242
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioImage:Landroid/widget/ImageView;

    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->mHashMap:Lcom/vlingo/core/internal/util/SparseArrayMap;
    invoke-static {}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$300()Lcom/vlingo/core/internal/util/SparseArrayMap;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->VALUE_COUNTER_HASHMAP:I
    invoke-static {v14}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$500(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)I

    move-result v14

    invoke-virtual {v12, v14}, Lcom/vlingo/core/internal/util/SparseArrayMap;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/graphics/Bitmap;

    invoke-virtual {v13, v12}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 253
    .end local v2    # "in":Ljava/io/InputStream;
    .end local v4    # "inRect":Landroid/graphics/RectF;
    .end local v6    # "m":Landroid/graphics/Matrix;
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v8    # "outRect":Landroid/graphics/RectF;
    .end local v9    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v10    # "roughBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "values":[F
    :cond_5
    :goto_0
    return-void

    .line 250
    :catch_0
    move-exception v1

    .line 251
    .local v1, "e":Ljava/io/IOException;
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioImage:Landroid/widget/ImageView;

    sget v13, Lcom/vlingo/midas/R$drawable;->music_player_thumb_01:I

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 247
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "in":Ljava/io/InputStream;
    .restart local v4    # "inRect":Landroid/graphics/RectF;
    .restart local v6    # "m":Landroid/graphics/Matrix;
    .restart local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v8    # "outRect":Landroid/graphics/RectF;
    .restart local v9    # "resizedBitmap":Landroid/graphics/Bitmap;
    .restart local v10    # "roughBitmap":Landroid/graphics/Bitmap;
    .restart local v11    # "values":[F
    :catch_1
    move-exception v12

    goto :goto_0

    .line 245
    :catch_2
    move-exception v12

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 97
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->arrayMusic:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 98
    .local v0, "toReturn":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->getLimitedCount(I)I

    move-result v1

    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->arrayMusic:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 106
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x4

    .line 112
    if-nez p2, :cond_1

    .line 113
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    sget v4, Lcom/vlingo/midas/R$layout;->music:I

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 114
    new-instance v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;

    invoke-direct {v0, v6}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;-><init>(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$1;)V

    .line 115
    .local v0, "audioHolder":Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;
    sget v3, Lcom/vlingo/midas/R$id;->audioImage:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioImage:Landroid/widget/ImageView;

    .line 116
    sget v3, Lcom/vlingo/midas/R$id;->audioTitle:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioTitle:Landroid/widget/TextView;

    .line 117
    sget v3, Lcom/vlingo/midas/R$id;->audioArtist:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioArtist:Landroid/widget/TextView;

    .line 118
    sget v3, Lcom/vlingo/midas/R$id;->audioDivider:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioDivider:Landroid/view/View;

    .line 119
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 132
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;->access$100(Lcom/vlingo/midas/gui/widgets/PlayMusicWidget;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->arrayMusic:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    invoke-virtual {v3}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getMusicThumbnail()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 135
    .local v1, "bit":Landroid/graphics/Bitmap;
    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    .end local v1    # "bit":Landroid/graphics/Bitmap;
    :goto_1
    :try_start_1
    iget-object v4, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioTitle:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->arrayMusic:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    invoke-virtual {v3}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v4, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioArtist:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->arrayMusic:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;

    invoke-virtual {v3}, Lcom/vlingo/midas/samsungutils/utils/music/MusicDetails;->getArtist()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 146
    :goto_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 147
    sget v3, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 148
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->arrayMusic:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p1, v3, :cond_0

    .line 149
    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioDivider:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 161
    :cond_0
    :goto_3
    return-object p2

    .line 123
    .end local v0    # "audioHolder":Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;

    .restart local v0    # "audioHolder":Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;
    goto :goto_0

    .line 136
    :catch_0
    move-exception v2

    .line 137
    .local v2, "e":Ljava/lang/Exception;
    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioImage:Landroid/widget/ImageView;

    sget v4, Lcom/vlingo/midas/R$drawable;->voice_music_thumbnail_01:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 143
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 144
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 152
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    if-nez p1, :cond_3

    .line 153
    sget v3, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3

    .line 154
    :cond_3
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$MusicBaseAdapter;->arrayMusic:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p1, v3, :cond_4

    .line 155
    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/PlayMusicWidget$AudioHolder;->audioDivider:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 156
    sget v3, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3

    .line 158
    :cond_4
    sget v3, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3
.end method
