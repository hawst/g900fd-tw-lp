.class Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;
.super Ljava/lang/Object;
.source "HelpChoiceWidget.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->setAddView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

.field final synthetic val$row:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const/16 v6, 0x3e9

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 423
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    sget v2, Lcom/vlingo/midas/R$id;->wcis_widget_row_divider1:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 424
    .local v0, "div":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 425
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    sget v2, Lcom/vlingo/midas/R$id;->header1_examples:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 426
    if-eqz v0, :cond_0

    .line 427
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 436
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->currentTablet:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$700(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$TabletType;

    if-ne v1, v2, :cond_1

    .line 437
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 438
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    sget v2, Lcom/vlingo/midas/R$id;->btn_idle_land:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 450
    :cond_1
    :goto_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v1, v2, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v1, v2, :cond_7

    .line 451
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_detail_focused:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 452
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 484
    :cond_3
    :goto_2
    return-void

    .line 429
    :cond_4
    if-eqz v0, :cond_5

    .line 430
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 432
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v6, v1, :cond_0

    if-eqz v0, :cond_0

    .line 433
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 439
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 440
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    sget v2, Lcom/vlingo/midas/R$id;->btn_idle:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusLeftId(I)V

    goto :goto_1

    .line 455
    :cond_7
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->isKitkatPhoneGUI:Z
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$800(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 456
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_list_focused:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    .line 461
    :cond_8
    if-eqz v0, :cond_9

    .line 462
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 464
    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v6, v1, :cond_a

    if-eqz v0, :cond_a

    .line 465
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 473
    :cond_a
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v1, v2, :cond_b

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v1, v2, :cond_c

    .line 475
    :cond_b
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_detail_focused:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 476
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_2

    .line 479
    :cond_c
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$2;->val$row:Landroid/view/View;

    sget v2, Lcom/vlingo/midas/R$drawable;->wcis_row_middle_background:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2
.end method
