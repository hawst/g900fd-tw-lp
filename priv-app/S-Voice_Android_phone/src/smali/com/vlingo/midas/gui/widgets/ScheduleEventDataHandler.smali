.class public Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;
.super Ljava/lang/Object;
.source "ScheduleEventDataHandler.java"


# static fields
.field private static final EVENT_PROJECTION:[Ljava/lang/String;

.field public static final INSTANCES_CONTENT_URI:Landroid/net/Uri;

.field private static final INSTANCES_PROJECTIONS:[Ljava/lang/String;

.field private static final NO_OF_ITEMS:I = 0x2

.field public static final SCHEDULE_CONTENT_URI:Landroid/net/Uri;

.field private static final SCHEDULE_WHERE_CLAUSE:Ljava/lang/String; = "_id=?"


# instance fields
.field Hours:I

.field Hours1:I

.field alldayCount:I

.field private hoursMode24:Z

.field private mContext:Landroid/content/Context;

.field mStartMillis:J

.field normalScheduleCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 35
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->INSTANCES_CONTENT_URI:Landroid/net/Uri;

    .line 36
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->SCHEDULE_CONTENT_URI:Landroid/net/Uri;

    .line 38
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "event_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "begin"

    aput-object v1, v0, v4

    const-string/jumbo v1, "end"

    aput-object v1, v0, v5

    const-string/jumbo v1, "title"

    aput-object v1, v0, v6

    const-string/jumbo v1, "accessLevel"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "allDay"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->INSTANCES_PROJECTIONS:[Ljava/lang/String;

    .line 84
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "title"

    aput-object v1, v0, v3

    const-string/jumbo v1, "accessLevel"

    aput-object v1, v0, v4

    const-string/jumbo v1, "contact_data_id"

    aput-object v1, v0, v5

    const-string/jumbo v1, "contact_id"

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->EVENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method private getContactsBirthdays()Landroid/database/Cursor;
    .locals 15

    .prologue
    const/4 v14, 0x5

    const/4 v11, 0x2

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 263
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 265
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "display_name"

    aput-object v0, v2, v12

    const-string/jumbo v0, "contact_id"

    aput-object v0, v2, v13

    const-string/jumbo v0, "_id"

    aput-object v0, v2, v11

    const/4 v0, 0x3

    const-string/jumbo v10, "starred"

    aput-object v10, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v10, "times_contacted"

    aput-object v10, v2, v0

    const-string/jumbo v0, "data1"

    aput-object v0, v2, v14

    .line 273
    .local v2, "projection":[Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 274
    .local v6, "cal":Ljava/util/Calendar;
    const-string/jumbo v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 275
    invoke-virtual {v6, v11}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v9, v0, 0x1

    .line 277
    .local v9, "month":I
    invoke-virtual {v6, v14}, Ljava/util/Calendar;->get(I)I

    move-result v8

    .line 278
    .local v8, "day":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "\'%-"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v0, 0x8

    if-le v9, v0, :cond_0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v10, "-"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v0, 0x9

    if-le v8, v0, :cond_1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v10, "\'"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 281
    .local v7, "date":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "mimetype= ? AND data2 IN ( 3 )  AND data1 LIKE "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 288
    .local v3, "where":Ljava/lang/String;
    new-array v4, v13, [Ljava/lang/String;

    const-string/jumbo v0, "vnd.android.cursor.item/contact_event"

    aput-object v0, v4, v12

    .line 289
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v5, "display_name"

    .line 290
    .local v5, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 278
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    .end local v5    # "sortOrder":Ljava/lang/String;
    .end local v7    # "date":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "0"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v11, v9, 0x1

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "0"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public getContacts()Ljava/util/ArrayList;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 300
    .local v4, "contactList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 301
    .local v10, "hs1":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 303
    .local v11, "hs2":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->getContactsBirthdays()Landroid/database/Cursor;

    move-result-object v3

    .line 306
    .local v3, "c":Landroid/database/Cursor;
    if-nez v3, :cond_1

    .line 307
    const/4 v2, 0x0

    .line 510
    if-eqz v3, :cond_0

    .line 511
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 512
    const/4 v3, 0x0

    .line 530
    :cond_0
    :goto_0
    return-object v2

    .line 309
    :cond_1
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_2

    .line 310
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    const/4 v2, 0x0

    .line 510
    if-eqz v3, :cond_0

    .line 511
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 512
    const/4 v3, 0x0

    goto :goto_0

    .line 314
    :cond_2
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v5

    .line 315
    .local v5, "count":I
    const-string/jumbo v22, "display_name"

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 356
    .local v15, "nameColumn":I
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 358
    .local v19, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v22, 0x2

    move/from16 v0, v22

    if-gt v5, v0, :cond_8

    .line 359
    :goto_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v22

    if-eqz v22, :cond_6

    .line 361
    const-string/jumbo v22, "starred"

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_4

    .line 364
    invoke-interface {v3, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 365
    .local v14, "name":Ljava/lang/String;
    invoke-virtual {v4, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 505
    .end local v5    # "count":I
    .end local v14    # "name":Ljava/lang/String;
    .end local v15    # "nameColumn":I
    .end local v19    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v22

    .line 510
    if-eqz v3, :cond_3

    .line 511
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 512
    const/4 v3, 0x0

    .line 520
    :cond_3
    :goto_2
    const-string/jumbo v22, "Ankit"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "Value of contact list final="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v22

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_10

    .line 524
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 525
    .local v2, "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 526
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 369
    .end local v2    # "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "count":I
    .restart local v15    # "nameColumn":I
    .restart local v19    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    :try_start_2
    invoke-interface {v3, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 370
    .restart local v14    # "name":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 510
    .end local v5    # "count":I
    .end local v14    # "name":Ljava/lang/String;
    .end local v15    # "nameColumn":I
    .end local v19    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v22

    if-eqz v3, :cond_5

    .line 511
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 512
    const/4 v3, 0x0

    :cond_5
    throw v22

    .line 374
    .restart local v5    # "count":I
    .restart local v15    # "nameColumn":I
    .restart local v19    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    :try_start_3
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_7

    .line 375
    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 378
    :try_start_4
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 510
    :cond_7
    :goto_3
    if-eqz v3, :cond_3

    .line 511
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 512
    const/4 v3, 0x0

    goto :goto_2

    .line 385
    :cond_8
    const/16 v17, 0x0

    .local v17, "sum1":I
    const/16 v20, 0x0

    .local v20, "temp1":I
    const/16 v18, 0x0

    .local v18, "sum2":I
    const/16 v21, 0x0

    .local v21, "temp2":I
    const/4 v8, 0x0

    .local v8, "flag1":I
    const/4 v9, 0x0

    .line 386
    .local v9, "flag2":I
    :goto_4
    :try_start_5
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v22

    if-eqz v22, :cond_a

    .line 388
    const-string/jumbo v22, "starred"

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_9

    .line 391
    invoke-interface {v3, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 392
    .restart local v14    # "name":Ljava/lang/String;
    const-string/jumbo v22, "times_contacted"

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 395
    .local v6, "count1":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v14, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    add-int v17, v17, v6

    .line 398
    move/from16 v20, v6

    .line 399
    goto :goto_4

    .line 402
    .end local v6    # "count1":I
    .end local v14    # "name":Ljava/lang/String;
    :cond_9
    invoke-interface {v3, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 403
    .restart local v14    # "name":Ljava/lang/String;
    const-string/jumbo v22, "times_contacted"

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 405
    .restart local v6    # "count1":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v11, v14, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    add-int v18, v18, v6

    .line 408
    move/from16 v21, v6

    .line 420
    goto :goto_4

    .line 424
    .end local v6    # "count1":I
    .end local v14    # "name":Ljava/lang/String;
    :cond_a
    invoke-virtual {v10}, Ljava/util/HashMap;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_b

    .line 425
    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v22

    div-int v22, v17, v22

    move/from16 v0, v22

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    .line 426
    const/4 v8, 0x1

    .line 427
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 429
    .restart local v2    # "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v10}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 432
    :try_start_6
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 436
    :goto_5
    :try_start_7
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 437
    const-string/jumbo v22, "Ankit"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "value of arr="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    .end local v2    # "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_b
    invoke-virtual {v11}, Ljava/util/HashMap;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_c

    .line 441
    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v22

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_c

    .line 442
    invoke-virtual {v11}, Ljava/util/HashMap;->size()I

    move-result v22

    div-int v22, v18, v22

    move/from16 v0, v22

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    .line 443
    const/4 v9, 0x1

    .line 444
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 446
    .restart local v2    # "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v11}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 449
    :try_start_8
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 453
    :goto_6
    :try_start_9
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 454
    const-string/jumbo v22, "Ankit"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "value of arr="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    .end local v2    # "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_c
    invoke-virtual {v10}, Ljava/util/HashMap;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_e

    if-nez v8, :cond_e

    .line 459
    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v16

    .line 460
    .local v16, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    new-instance v13, Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 463
    .local v13, "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :try_start_a
    new-instance v22, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler$1;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler$1;-><init>(Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;)V

    move-object/from16 v0, v22

    invoke-static {v13, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 473
    :goto_7
    :try_start_b
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_d

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 474
    .local v7, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 477
    .end local v7    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_d
    const-string/jumbo v22, "Ankit"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "Value of contact list for hs1="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .end local v16    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :cond_e
    invoke-virtual {v11}, Ljava/util/HashMap;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_7

    if-nez v9, :cond_7

    .line 482
    invoke-virtual {v11}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v16

    .line 483
    .restart local v16    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    new-instance v13, Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 486
    .restart local v13    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :try_start_c
    new-instance v22, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler$2;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler$2;-><init>(Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;)V

    move-object/from16 v0, v22

    invoke-static {v13, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 496
    :goto_9
    :try_start_d
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .restart local v12    # "i$":Ljava/util/Iterator;
    :goto_a
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_f

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 497
    .restart local v7    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 500
    .end local v7    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_f
    const-string/jumbo v22, "Ankit"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "Value of contact list for hs2="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_3

    .end local v5    # "count":I
    .end local v8    # "flag1":I
    .end local v9    # "flag2":I
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .end local v15    # "nameColumn":I
    .end local v16    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .end local v17    # "sum1":I
    .end local v18    # "sum2":I
    .end local v19    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v20    # "temp1":I
    .end local v21    # "temp2":I
    :cond_10
    move-object v2, v4

    .line 530
    goto/16 :goto_0

    .line 380
    .restart local v5    # "count":I
    .restart local v15    # "nameColumn":I
    .restart local v19    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_1
    move-exception v22

    goto/16 :goto_3

    .line 434
    .restart local v2    # "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v8    # "flag1":I
    .restart local v9    # "flag2":I
    .restart local v17    # "sum1":I
    .restart local v18    # "sum2":I
    .restart local v20    # "temp1":I
    .restart local v21    # "temp2":I
    :catch_2
    move-exception v22

    goto/16 :goto_5

    .line 451
    :catch_3
    move-exception v22

    goto/16 :goto_6

    .line 494
    .end local v2    # "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v13    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .restart local v16    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :catch_4
    move-exception v22

    goto :goto_9

    .line 471
    :catch_5
    move-exception v22

    goto/16 :goto_7
.end method

.method public getEventInfo(Landroid/database/Cursor;)V
    .locals 21
    .param p1, "cursorInstance"    # Landroid/database/Cursor;

    .prologue
    .line 93
    const/4 v11, 0x0

    .line 94
    .local v11, "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    const/16 v17, 0x0

    sput-object v17, Lcom/vlingo/midas/gui/DialogFragment;->eventList:Ljava/util/ArrayList;

    .line 95
    if-eqz p1, :cond_e

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v17

    if-lez v17, :cond_e

    .line 96
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    sput-object v17, Lcom/vlingo/midas/gui/DialogFragment;->eventList:Ljava/util/ArrayList;

    .line 97
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->alldayCount:I

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->normalScheduleCount:I

    move-object v12, v11

    .line 100
    .end local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .local v12, "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v17

    if-eqz v17, :cond_18

    .line 102
    sget-object v17, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->INSTANCES_PROJECTIONS:[Ljava/lang/String;

    const/16 v18, 0x5

    aget-object v17, v17, v18

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 104
    .local v8, "isAllday":I
    sget-object v17, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->INSTANCES_PROJECTIONS:[Ljava/lang/String;

    const/16 v18, 0x4

    aget-object v17, v17, v18

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 106
    .local v2, "accessLevel":I
    const-string/jumbo v17, "begin"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 107
    .local v13, "startTime":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 108
    .local v5, "cal":Ljava/util/Calendar;
    const-string/jumbo v17, "UTC"

    invoke-static/range {v17 .. v17}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 109
    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v17

    move-wide/from16 v0, v17

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 110
    const/16 v17, 0x3

    move/from16 v0, v17

    if-eq v2, v0, :cond_3

    .line 112
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v8, v0, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->alldayCount:I

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    .line 116
    :cond_1
    if-nez v8, :cond_2

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mStartMillis:J

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-lez v17, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->normalScheduleCount:I

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    .line 120
    :cond_2
    if-nez v8, :cond_3

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mStartMillis:J

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-ltz v17, :cond_0

    .line 126
    :cond_3
    new-instance v11, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;

    invoke-direct {v11}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 127
    .end local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :try_start_1
    const-string/jumbo v3, ""

    .line 128
    .local v3, "ampm":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->hoursMode24:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 130
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    const/16 v18, 0xb

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_c

    .line 131
    const-string/jumbo v3, "PM"

    .line 137
    :cond_4
    :goto_1
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getMinutes()I

    move-result v17

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    .line 138
    .local v9, "minutes":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getMinutes()I

    move-result v17

    const/16 v18, 0x9

    move/from16 v0, v17

    move/from16 v1, v18

    if-gt v0, v1, :cond_5

    .line 139
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v18, 0x30

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 141
    :cond_5
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    .line 142
    .local v15, "start_hour":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->hoursMode24:Z

    move/from16 v17, v0

    if-nez v17, :cond_f

    .line 144
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    const/16 v18, 0xc

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_d

    .line 145
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    rem-int/lit8 v17, v17, 0xc

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours:I

    .line 148
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours:I

    move/from16 v17, v0

    if-nez v17, :cond_6

    .line 149
    const/16 v17, 0xc

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours:I

    .line 150
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    .line 161
    :goto_3
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getTime()J

    move-result-wide v17

    move-wide/from16 v0, v17

    invoke-virtual {v11, v0, v1}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->setMilli(J)V

    .line 162
    const-string/jumbo v17, "end"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 163
    .local v14, "startTime1":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 164
    .local v6, "cal1":Ljava/util/Calendar;
    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v17

    move-wide/from16 v0, v17

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 165
    const-string/jumbo v4, ""

    .line 166
    .local v4, "ampm1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->hoursMode24:Z

    move/from16 v17, v0

    if-nez v17, :cond_7

    .line 168
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    const/16 v18, 0xb

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_11

    .line 169
    const-string/jumbo v4, "PM"

    .line 175
    :cond_7
    :goto_4
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getMinutes()I

    move-result v17

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    .line 176
    .local v10, "minutes1":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getMinutes()I

    move-result v17

    const/16 v18, 0x9

    move/from16 v0, v17

    move/from16 v1, v18

    if-gt v0, v1, :cond_8

    .line 177
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v18, 0x30

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 179
    :cond_8
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    .line 180
    .local v7, "end_hour":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->hoursMode24:Z

    move/from16 v17, v0

    if-nez v17, :cond_14

    .line 182
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    const/16 v18, 0xc

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_12

    .line 183
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    rem-int/lit8 v17, v17, 0xc

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours1:I

    .line 186
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours1:I

    move/from16 v17, v0

    if-nez v17, :cond_9

    .line 187
    const/16 v17, 0xc

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours1:I

    .line 188
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours1:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    .line 200
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours1:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_16

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_16

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v17

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v19

    cmp-long v17, v17, v19

    if-eqz v17, :cond_16

    .line 202
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v17

    const/16 v18, 0x7

    invoke-virtual/range {v17 .. v18}, Ljava/util/Calendar;->get(I)I

    move-result v17

    const/16 v18, 0x7

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    sget v18, Lcom/vlingo/midas/R$string;->all_day:I

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->setStartDate(Ljava/lang/String;)V

    .line 205
    invoke-virtual {v11}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->setAllday()V

    .line 212
    :cond_a
    :goto_7
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getTime()J

    move-result-wide v17

    move-wide/from16 v0, v17

    invoke-virtual {v11, v0, v1}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->setMilliEnd(J)V

    .line 213
    sget-object v17, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->INSTANCES_PROJECTIONS:[Ljava/lang/String;

    const/16 v18, 0x3

    aget-object v17, v17, v18

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 215
    .local v16, "title":Ljava/lang/String;
    invoke-virtual {v11, v2}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->setType(I)V

    .line 216
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->setTitle(Ljava/lang/String;)V

    .line 233
    sget-object v17, Lcom/vlingo/midas/gui/DialogFragment;->eventList:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    const/16 v17, 0x3

    move/from16 v0, v17

    if-eq v2, v0, :cond_b

    .line 236
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v8, v0, :cond_17

    .line 238
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->alldayCount:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->alldayCount:I

    :cond_b
    :goto_8
    move-object v12, v11

    .line 247
    .end local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    goto/16 :goto_0

    .line 133
    .end local v4    # "ampm1":Ljava/lang/String;
    .end local v6    # "cal1":Ljava/util/Calendar;
    .end local v7    # "end_hour":Ljava/lang/String;
    .end local v9    # "minutes":Ljava/lang/String;
    .end local v10    # "minutes1":Ljava/lang/String;
    .end local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .end local v14    # "startTime1":Ljava/lang/String;
    .end local v15    # "start_hour":Ljava/lang/String;
    .end local v16    # "title":Ljava/lang/String;
    .restart local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :cond_c
    const-string/jumbo v3, "AM"

    goto/16 :goto_1

    .line 147
    .restart local v9    # "minutes":Ljava/lang/String;
    .restart local v15    # "start_hour":Ljava/lang/String;
    :cond_d
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 249
    .end local v3    # "ampm":Ljava/lang/String;
    .end local v9    # "minutes":Ljava/lang/String;
    .end local v15    # "start_hour":Ljava/lang/String;
    :catch_0
    move-exception v17

    .line 252
    .end local v2    # "accessLevel":I
    .end local v5    # "cal":Ljava/util/Calendar;
    .end local v8    # "isAllday":I
    .end local v13    # "startTime":Ljava/lang/String;
    :goto_9
    if-eqz p1, :cond_e

    .line 253
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 254
    const/16 p1, 0x0

    .line 259
    :cond_e
    :goto_a
    return-void

    .line 154
    .restart local v2    # "accessLevel":I
    .restart local v3    # "ampm":Ljava/lang/String;
    .restart local v5    # "cal":Ljava/util/Calendar;
    .restart local v8    # "isAllday":I
    .restart local v9    # "minutes":Ljava/lang/String;
    .restart local v13    # "startTime":Ljava/lang/String;
    .restart local v15    # "start_hour":Ljava/lang/String;
    :cond_f
    :try_start_2
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours:I

    .line 155
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours:I

    move/from16 v17, v0

    const/16 v18, 0x9

    move/from16 v0, v17

    move/from16 v1, v18

    if-gt v0, v1, :cond_10

    .line 156
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v18, 0x30

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_3

    .line 159
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_3

    .line 171
    .restart local v4    # "ampm1":Ljava/lang/String;
    .restart local v6    # "cal1":Ljava/util/Calendar;
    .restart local v14    # "startTime1":Ljava/lang/String;
    :cond_11
    const-string/jumbo v4, "AM"

    goto/16 :goto_4

    .line 185
    .restart local v7    # "end_hour":Ljava/lang/String;
    .restart local v10    # "minutes1":Ljava/lang/String;
    :cond_12
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours1:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_5

    .line 252
    .end local v3    # "ampm":Ljava/lang/String;
    .end local v4    # "ampm1":Ljava/lang/String;
    .end local v6    # "cal1":Ljava/util/Calendar;
    .end local v7    # "end_hour":Ljava/lang/String;
    .end local v9    # "minutes":Ljava/lang/String;
    .end local v10    # "minutes1":Ljava/lang/String;
    .end local v14    # "startTime1":Ljava/lang/String;
    .end local v15    # "start_hour":Ljava/lang/String;
    :catchall_0
    move-exception v17

    .end local v2    # "accessLevel":I
    .end local v5    # "cal":Ljava/util/Calendar;
    .end local v8    # "isAllday":I
    .end local v13    # "startTime":Ljava/lang/String;
    :goto_b
    if-eqz p1, :cond_13

    .line 253
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 254
    const/16 p1, 0x0

    :cond_13
    throw v17

    .line 192
    .restart local v2    # "accessLevel":I
    .restart local v3    # "ampm":Ljava/lang/String;
    .restart local v4    # "ampm1":Ljava/lang/String;
    .restart local v5    # "cal":Ljava/util/Calendar;
    .restart local v6    # "cal1":Ljava/util/Calendar;
    .restart local v7    # "end_hour":Ljava/lang/String;
    .restart local v8    # "isAllday":I
    .restart local v9    # "minutes":Ljava/lang/String;
    .restart local v10    # "minutes1":Ljava/lang/String;
    .restart local v13    # "startTime":Ljava/lang/String;
    .restart local v14    # "startTime1":Ljava/lang/String;
    .restart local v15    # "start_hour":Ljava/lang/String;
    :cond_14
    :try_start_3
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getHours()I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours1:I

    .line 193
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours1:I

    move/from16 v17, v0

    const/16 v18, 0x9

    move/from16 v0, v17

    move/from16 v1, v18

    if-gt v0, v1, :cond_15

    .line 194
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v18, 0x30

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_6

    .line 197
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->Hours1:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_6

    .line 210
    :cond_16
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, ":"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " - "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, ":"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lcom/vlingo/midas/gui/widgets/ScheduleEventData;->setStartDate(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 240
    .restart local v16    # "title":Ljava/lang/String;
    :cond_17
    if-nez v8, :cond_b

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mStartMillis:J

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-lez v17, :cond_b

    .line 242
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->normalScheduleCount:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->normalScheduleCount:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_8

    .line 252
    .end local v2    # "accessLevel":I
    .end local v3    # "ampm":Ljava/lang/String;
    .end local v4    # "ampm1":Ljava/lang/String;
    .end local v5    # "cal":Ljava/util/Calendar;
    .end local v6    # "cal1":Ljava/util/Calendar;
    .end local v7    # "end_hour":Ljava/lang/String;
    .end local v8    # "isAllday":I
    .end local v9    # "minutes":Ljava/lang/String;
    .end local v10    # "minutes1":Ljava/lang/String;
    .end local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .end local v13    # "startTime":Ljava/lang/String;
    .end local v14    # "startTime1":Ljava/lang/String;
    .end local v15    # "start_hour":Ljava/lang/String;
    .end local v16    # "title":Ljava/lang/String;
    .restart local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :cond_18
    if-eqz p1, :cond_19

    .line 253
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 254
    const/16 p1, 0x0

    move-object v11, v12

    .end local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    goto/16 :goto_a

    .line 252
    .end local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :catchall_1
    move-exception v17

    move-object v11, v12

    .end local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    goto/16 :goto_b

    .line 249
    .end local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :catch_1
    move-exception v17

    move-object v11, v12

    .end local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    goto/16 :goto_9

    .end local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    :cond_19
    move-object v11, v12

    .end local v12    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    .restart local v11    # "seData":Lcom/vlingo/midas/gui/widgets/ScheduleEventData;
    goto/16 :goto_a
.end method

.method public getTodayInstances()Landroid/database/Cursor;
    .locals 15

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 45
    const/4 v7, 0x0

    .line 48
    .local v7, "cursor":Landroid/database/Cursor;
    new-instance v10, Landroid/text/format/Time;

    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    .line 49
    .local v10, "mStartTime":Landroid/text/format/Time;
    invoke-virtual {v10}, Landroid/text/format/Time;->setToNow()V

    .line 50
    iput v2, v10, Landroid/text/format/Time;->hour:I

    .line 51
    iput v2, v10, Landroid/text/format/Time;->minute:I

    .line 52
    iput v2, v10, Landroid/text/format/Time;->second:I

    .line 53
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 54
    invoke-virtual {v10, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v11

    iput-wide v11, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mStartMillis:J

    .line 55
    iget-wide v11, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mStartMillis:J

    const-wide/32 v13, 0x5265c00

    add-long v8, v11, v13

    .line 56
    .local v8, "mEndMillis":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    iput-wide v11, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mStartMillis:J

    .line 57
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/ClockWidget;->get24HourMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->hoursMode24:Z

    .line 58
    sget-object v0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->INSTANCES_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 59
    .local v6, "builder":Landroid/net/Uri$Builder;
    iget-wide v11, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mStartMillis:J

    invoke-static {v6, v11, v12}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 60
    invoke-static {v6, v8, v9}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 61
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 63
    .local v1, "uri":Landroid/net/Uri;
    const-string/jumbo v3, "( (begin >= ? AND begin < ? ) OR ( end > ? AND end <= ? ) OR ( begin <= ? AND end >= ? ) )  AND visible= 1"

    .line 70
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    iget-wide v11, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mStartMillis:J

    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x2

    iget-wide v11, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mStartMillis:J

    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x3

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x4

    iget-wide v11, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mStartMillis:J

    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x5

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    .line 75
    .local v4, "selectionArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/vlingo/midas/gui/widgets/ScheduleEventDataHandler;->INSTANCES_PROJECTIONS:[Ljava/lang/String;

    const-string/jumbo v5, "begin ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 80
    return-object v7
.end method
