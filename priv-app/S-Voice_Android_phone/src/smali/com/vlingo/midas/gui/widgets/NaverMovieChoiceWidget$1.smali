.class Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;
.super Ljava/lang/Object;
.source "NaverMovieChoiceWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 100
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovies:Ljava/util/List;
    invoke-static {v9}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Ljava/util/List;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;
    invoke-static {v9}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Landroid/widget/ListView;

    move-result-object v9

    if-nez v9, :cond_1

    .line 155
    :cond_0
    return-void

    .line 103
    :cond_1
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovies:Ljava/util/List;
    invoke-static {v9}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v7

    .line 104
    .local v7, "mMoviesSize":I
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovieList:Landroid/widget/ListView;
    invoke-static {v9}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Landroid/widget/ListView;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/ListView;->getCount()I

    move-result v6

    .line 106
    .local v6, "mMovieListSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_0

    if-ge v2, v6, :cond_0

    .line 108
    const/4 v3, 0x0

    .line 109
    .local v3, "image":Landroid/graphics/Bitmap;
    iget-object v9, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->mMovies:Ljava/util/List;
    invoke-static {v9}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->access$000(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Hashtable;

    .line 111
    .local v5, "item":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    new-instance v8, Ljava/net/URL;

    const-string/jumbo v9, "poster"

    invoke-virtual {v5, v9}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 112
    .local v8, "picURL":Ljava/net/URL;
    invoke-virtual {v8}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-static {v9}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 113
    invoke-virtual {v8}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    .end local v8    # "picURL":Ljava/net/URL;
    :goto_1
    if-eqz v3, :cond_2

    .line 125
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 128
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    :goto_2
    move v4, v2

    .line 129
    .local v4, "ind":I
    new-instance v9, Landroid/os/Handler;

    iget-object v10, p0, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;

    invoke-virtual {v10}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v10, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;

    invoke-direct {v10, p0, v4, v0}, Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1$1;-><init>(Lcom/vlingo/midas/gui/widgets/NaverMovieChoiceWidget$1;ILandroid/graphics/drawable/Drawable;)V

    const-wide/16 v11, 0x1f4

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 106
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 114
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    .end local v4    # "ind":I
    :catch_0
    move-exception v1

    .line 115
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 126
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "d":Landroid/graphics/drawable/Drawable;
    goto :goto_2
.end method
