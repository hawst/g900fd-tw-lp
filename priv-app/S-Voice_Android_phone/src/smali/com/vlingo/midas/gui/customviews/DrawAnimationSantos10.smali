.class public Lcom/vlingo/midas/gui/customviews/DrawAnimationSantos10;
.super Ljava/lang/Object;
.source "DrawAnimationSantos10.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static drawLandscapeAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V
    .locals 7
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "cornerBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "squareImage"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p3, "mRms"    # F

    .prologue
    const/high16 v6, 0x41f00000    # 30.0f

    const/high16 v5, 0x42700000    # 60.0f

    const/high16 v4, 0x42480000    # 50.0f

    const/high16 v3, 0x428c0000    # 70.0f

    const/4 v2, 0x0

    .line 129
    const/high16 v0, 0x41f80000    # 31.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 130
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 131
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 132
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 133
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 134
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 135
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 136
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    const/high16 v0, 0x42140000    # 37.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 139
    const/4 v0, 0x0

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 140
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 141
    const/high16 v0, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 142
    const/high16 v0, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 143
    const/high16 v0, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 144
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 145
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 146
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 147
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 148
    :cond_2
    const/high16 v0, 0x422c0000    # 43.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_3

    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 150
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 151
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 152
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 153
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 154
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 155
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 156
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 157
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 159
    const/16 v0, 0x14

    const/16 v1, 0x50

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 160
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 161
    :cond_3
    const/high16 v0, 0x42440000    # 49.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    .line 162
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 163
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 164
    const/high16 v0, 0x425c0000    # 55.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 165
    invoke-virtual {p0, p1, v4, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 166
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 167
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x425c0000    # 55.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 168
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 169
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 170
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x425c0000    # 55.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 171
    const/16 v0, 0x14

    const/16 v1, 0x50

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 172
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 173
    :cond_4
    const/high16 v0, 0x425c0000    # 55.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_5

    .line 174
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 175
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 176
    invoke-virtual {p0, p1, v6, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 177
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 178
    const/high16 v0, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 179
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 180
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 181
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 182
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 183
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 185
    const/16 v0, 0x14

    const/16 v1, 0x5a

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 186
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 189
    :cond_5
    const/high16 v0, 0x42740000    # 61.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6

    .line 190
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 191
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 192
    const/high16 v0, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 193
    const/high16 v0, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 194
    const/high16 v0, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 195
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 196
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 197
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 198
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 200
    const/16 v0, 0x14

    const/16 v1, 0x4b

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 201
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 202
    :cond_6
    const/high16 v0, 0x42860000    # 67.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_7

    .line 203
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 204
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 205
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 206
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 207
    invoke-virtual {p0, p1, v3, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 208
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 209
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 210
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 211
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 213
    const/16 v0, 0x14

    const/16 v1, 0x46

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 214
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 215
    :cond_7
    const/high16 v0, 0x42920000    # 73.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_8

    .line 216
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 217
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 218
    invoke-virtual {p0, p1, v6, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 219
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 220
    invoke-virtual {p0, p1, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 221
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 222
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 223
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 224
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 226
    const/16 v0, 0x14

    const/16 v1, 0x3c

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 227
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 228
    :cond_8
    const/high16 v0, 0x429e0000    # 79.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_9

    .line 229
    const/4 v0, 0x0

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 230
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 231
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 232
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 233
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 234
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 235
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 236
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 237
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 239
    const/16 v0, 0x14

    const/16 v1, 0x32

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 240
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 241
    :cond_9
    const/high16 v0, 0x429e0000    # 79.0f

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 242
    const/16 v0, 0x14

    const/16 v1, 0x14

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 243
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method public static drawPotraitAnimation(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;F)V
    .locals 7
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "cornerBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "squareImage"    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p3, "mRms"    # F

    .prologue
    const/high16 v6, 0x41f00000    # 30.0f

    const/high16 v5, 0x42700000    # 60.0f

    const/high16 v4, 0x42480000    # 50.0f

    const/high16 v3, 0x428c0000    # 70.0f

    const/4 v2, 0x0

    .line 10
    const/high16 v0, 0x41f80000    # 31.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    .line 11
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 12
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 13
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 14
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 15
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 16
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 17
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 19
    :cond_1
    const/high16 v0, 0x42140000    # 37.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 20
    const/4 v0, 0x0

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 21
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 22
    const/high16 v0, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 23
    const/high16 v0, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 24
    const/high16 v0, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 25
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 26
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 27
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42aa0000    # 85.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 28
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 29
    :cond_2
    const/high16 v0, 0x422c0000    # 43.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_3

    .line 30
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 31
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 32
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 33
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 34
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 35
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 36
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 37
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 38
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 40
    const/16 v0, 0x14

    const/16 v1, 0x50

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 41
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 42
    :cond_3
    const/high16 v0, 0x42440000    # 49.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    .line 43
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 44
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 45
    invoke-virtual {p0, p1, v6, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 46
    invoke-virtual {p0, p1, v4, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 47
    invoke-virtual {p0, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 48
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 49
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 50
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 51
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 52
    const/16 v0, 0x14

    const/16 v1, 0x50

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 53
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 54
    :cond_4
    const/high16 v0, 0x425c0000    # 55.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_5

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 56
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 57
    invoke-virtual {p0, p1, v6, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 58
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 59
    const/high16 v0, 0x42960000    # 75.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 60
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 61
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 62
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 63
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 64
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 66
    const/16 v0, 0x14

    const/16 v1, 0x5a

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 67
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 69
    :cond_5
    const/high16 v0, 0x42740000    # 61.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6

    .line 70
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 71
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 72
    const/high16 v0, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 73
    const/high16 v0, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 74
    const/high16 v0, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 75
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 76
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 77
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x42340000    # 45.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 78
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x42820000    # 65.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 80
    const/16 v0, 0x14

    const/16 v1, 0x4b

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 81
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 82
    :cond_6
    const/high16 v0, 0x42860000    # 67.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_7

    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 84
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 85
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 86
    invoke-virtual {p0, p1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 87
    invoke-virtual {p0, p1, v3, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 88
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 89
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 90
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 91
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 93
    const/16 v0, 0x14

    const/16 v1, 0x46

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 94
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 95
    :cond_7
    const/high16 v0, 0x42920000    # 73.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_8

    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 97
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 98
    invoke-virtual {p0, p1, v6, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 99
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 100
    invoke-virtual {p0, p1, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 101
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 102
    const/high16 v0, 0x42dc0000    # 110.0f

    invoke-virtual {p0, p1, v0, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 103
    const/high16 v0, 0x43020000    # 130.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 104
    const/high16 v0, 0x43160000    # 150.0f

    invoke-virtual {p0, p1, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 106
    const/16 v0, 0x14

    const/16 v1, 0x3c

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 107
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 108
    :cond_8
    const/high16 v0, 0x429e0000    # 79.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_9

    .line 109
    const/4 v0, 0x0

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 110
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 111
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 112
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 113
    const/high16 v0, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 114
    const/high16 v0, 0x42b40000    # 90.0f

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 115
    const/high16 v0, 0x42dc0000    # 110.0f

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 116
    const/high16 v0, 0x43020000    # 130.0f

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 117
    const/high16 v0, 0x43160000    # 150.0f

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 119
    const/16 v0, 0x14

    const/16 v1, 0x32

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 120
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 121
    :cond_9
    const/high16 v0, 0x429e0000    # 79.0f

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 122
    const/16 v0, 0x14

    const/16 v1, 0x14

    const/16 v2, 0xb4

    const/16 v3, 0x64

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 123
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method
