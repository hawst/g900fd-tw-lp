.class synthetic Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;
.super Ljava/lang/Object;
.source "EditWhatYouSaidHelpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

.field static final synthetic $SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

.field static final synthetic $SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2104
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->values()[Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

    :try_start_0
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->IDLE:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_0
    :try_start_1
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

    sget-object v1, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->BUSY:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    .line 1991
    :goto_1
    invoke-static {}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->values()[Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    :try_start_2
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->GETTING_READY:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_2
    :try_start_3
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->LISTENING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_3
    :try_start_4
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->CONNECTING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_4
    :try_start_5
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->THINKING:Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    invoke-virtual {v1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    .line 976
    :goto_5
    invoke-static {}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->values()[Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    :try_start_6
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->READY:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_6
    :try_start_7
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_7
    :try_start_8
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->DONE:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_8
    :try_start_9
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->FINISH:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_9
    :try_start_a
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->NONE:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_a
    return-void

    :catch_0
    move-exception v0

    goto :goto_a

    :catch_1
    move-exception v0

    goto :goto_9

    :catch_2
    move-exception v0

    goto :goto_8

    :catch_3
    move-exception v0

    goto :goto_7

    :catch_4
    move-exception v0

    goto :goto_6

    .line 1991
    :catch_5
    move-exception v0

    goto :goto_5

    :catch_6
    move-exception v0

    goto :goto_4

    :catch_7
    move-exception v0

    goto :goto_3

    :catch_8
    move-exception v0

    goto :goto_2

    .line 2104
    :catch_9
    move-exception v0

    goto/16 :goto_1

    :catch_a
    move-exception v0

    goto/16 :goto_0
.end method
