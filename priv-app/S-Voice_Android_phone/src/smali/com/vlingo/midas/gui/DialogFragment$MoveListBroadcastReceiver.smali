.class Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoveListBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogFragment;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;)V
    .locals 0

    .prologue
    .line 2653
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;Lcom/vlingo/midas/gui/DialogFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/DialogFragment;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/DialogFragment$1;

    .prologue
    .line 2653
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;-><init>(Lcom/vlingo/midas/gui/DialogFragment;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2656
    const/4 v0, 0x0

    .line 2657
    .local v0, "action":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 2658
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2660
    :cond_0
    const-string/jumbo v5, "LIST_MOVE_ACTION"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2661
    const-string/jumbo v5, "EXTRA_LIST_HEIGHT"

    const/4 v6, 0x0

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2662
    .local v1, "extra":I
    const/4 v2, 0x0

    .line 2664
    .local v2, "helpHeight":I
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    add-int/lit8 v3, v5, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 2665
    const/4 v4, 0x0

    .line 2666
    .local v4, "v1":Landroid/view/View;
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2667
    if-eqz v4, :cond_1

    .line 2668
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v2, v5

    .line 2669
    instance-of v5, v4, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    if-eqz v5, :cond_1

    .line 2670
    const/4 v3, 0x0

    .line 2671
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v2, v5

    .line 2664
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 2676
    .end local v4    # "v1":Landroid/view/View;
    :cond_2
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iget-object v6, p0, Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollContent:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/vlingo/midas/gui/DialogFragment;->access$600(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v6

    iget-object v7, p0, Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v7}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ScrollView;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int/2addr v6, v1

    sub-int/2addr v6, v2

    # invokes: Lcom/vlingo/midas/gui/DialogFragment;->doScrollAnimMoveList(I)V
    invoke-static {v5, v6}, Lcom/vlingo/midas/gui/DialogFragment;->access$1900(Lcom/vlingo/midas/gui/DialogFragment;I)V

    .line 2688
    .end local v1    # "extra":I
    .end local v2    # "helpHeight":I
    .end local v3    # "i":I
    :cond_3
    :goto_1
    return-void

    .line 2679
    :cond_4
    const-string/jumbo v5, "com.vlingo.LANGUAGE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2681
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getHelpVisible()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2682
    iget-object v5, p0, Lcom/vlingo/midas/gui/DialogFragment$MoveListBroadcastReceiver;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/DialogFragment;->addHelpChoiceWidget()V

    goto :goto_1
.end method
