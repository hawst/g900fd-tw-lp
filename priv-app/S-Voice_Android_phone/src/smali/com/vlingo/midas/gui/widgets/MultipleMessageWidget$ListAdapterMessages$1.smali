.class Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages$1;
.super Ljava/lang/Object;
.source "MultipleMessageWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;

.field final synthetic val$message:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;Lcom/vlingo/core/internal/dialogmanager/types/MessageType;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages$1;->this$1:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages$1;->val$message:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 157
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 158
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Choice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string/jumbo v1, "id"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages$1;->val$message:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 160
    const-string/jumbo v1, "message_type"

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages$1;->val$message:Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages$1;->this$1:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 162
    return-void
.end method
