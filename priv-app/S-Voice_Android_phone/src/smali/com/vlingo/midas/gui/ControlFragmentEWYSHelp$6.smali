.class synthetic Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;
.super Ljava/lang/Object;
.source "ControlFragmentEWYSHelp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$ControlState:[I

.field static final synthetic $SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$GuideState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 402
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->values()[Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$GuideState:[I

    :try_start_0
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$GuideState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->NONE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_b

    :goto_0
    :try_start_1
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$GuideState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->READY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_a

    :goto_1
    :try_start_2
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$GuideState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->EDIT:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_9

    :goto_2
    :try_start_3
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$GuideState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->DONE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_8

    :goto_3
    :try_start_4
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$GuideState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->FINISH:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$GuideState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_7

    .line 323
    :goto_4
    invoke-static {}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->values()[Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$ControlState:[I

    :try_start_5
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$ControlState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_6

    :goto_5
    :try_start_6
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$ControlState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_GETTING_READY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_5

    :goto_6
    :try_start_7
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$ControlState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_4

    :goto_7
    :try_start_8
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$ControlState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_3

    :goto_8
    :try_start_9
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$ControlState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_2

    :goto_9
    :try_start_a
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$ControlState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_EDITING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1

    :goto_a
    :try_start_b
    sget-object v0, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$6;->$SwitchMap$com$vlingo$midas$gui$ControlFragmentEWYSHelp$ControlState:[I

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->DIALOG_BUSY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_0

    :goto_b
    return-void

    :catch_0
    move-exception v0

    goto :goto_b

    :catch_1
    move-exception v0

    goto :goto_a

    :catch_2
    move-exception v0

    goto :goto_9

    :catch_3
    move-exception v0

    goto :goto_8

    :catch_4
    move-exception v0

    goto :goto_7

    :catch_5
    move-exception v0

    goto :goto_6

    :catch_6
    move-exception v0

    goto :goto_5

    .line 402
    :catch_7
    move-exception v0

    goto :goto_4

    :catch_8
    move-exception v0

    goto :goto_3

    :catch_9
    move-exception v0

    goto :goto_2

    :catch_a
    move-exception v0

    goto/16 :goto_1

    :catch_b
    move-exception v0

    goto/16 :goto_0
.end method
