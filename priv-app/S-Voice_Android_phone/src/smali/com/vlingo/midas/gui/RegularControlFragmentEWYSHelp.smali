.class public Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;
.super Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;
.source "RegularControlFragmentEWYSHelp.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field invalidToastListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;-><init>()V

    .line 31
    new-instance v0, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp$1;-><init>(Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->invalidToastListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method getThninkingBtnOver()I
    .locals 1

    .prologue
    .line 75
    sget v0, Lcom/vlingo/midas/R$drawable;->btn_overlay_thinking:I

    return v0
.end method

.method getThninkingMicAnimationList()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x12

    .line 43
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/RegularControlFragmentBaseEWYSHelp;->onActivityCreated(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->control_full_container:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->fullContainer:Landroid/widget/RelativeLayout;

    .line 46
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->fullContainer:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->invalidToastListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->prompt_btn_layout:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 50
    .local v0, "layoutPromptBtn":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->btn_listening_kitkat:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/customviews/MicViewKK;

    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 51
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->btn_thinking:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->reco_thinkingBtn:Landroid/widget/ImageView;

    .line 52
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->btn_idle:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    .line 53
    iget-object v1, p0, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->reco_idleBtn:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->invalidToastListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v1, v3, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v1, v2, :cond_1

    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v1, v3, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v1

    sget-object v2, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v1, v2, :cond_1

    .line 62
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->setmActivityCreated(Z)V

    .line 63
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->setNormalPanel()V

    .line 66
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    sget v0, Lcom/vlingo/midas/R$layout;->control_view_ewys_help:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
