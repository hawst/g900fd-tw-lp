.class Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;
.super Ljava/lang/Object;
.source "MultipleMessageReadoutWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

.field final synthetic val$msgs:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;->val$msgs:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 210
    new-instance v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;->val$msgs:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;Landroid/content/Context;Ljava/util/List;)V

    .line 211
    .local v0, "adapter":Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->mMultipleMessages:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 212
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;->mMultipleMessages:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$3;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 213
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageReadoutWidget$ListAdapterMessages;->notifyDataSetChanged()V

    .line 214
    return-void
.end method
