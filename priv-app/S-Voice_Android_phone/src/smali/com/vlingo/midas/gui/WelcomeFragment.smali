.class public Lcom/vlingo/midas/gui/WelcomeFragment;
.super Landroid/support/v4/app/Fragment;
.source "WelcomeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/WelcomeFragment$onNext;
    }
.end annotation


# instance fields
.field content:Landroid/view/View;

.field private fadeIn:Landroid/view/animation/Animation;

.field private fadeOut:Landroid/view/animation/Animation;

.field private mBottomRelLayout:Landroid/widget/RelativeLayout;

.field private mFadeOutListner:Landroid/view/animation/Animation$AnimationListener;

.field private mLogoLayout:Landroid/widget/LinearLayout;

.field private mNextButtonLayout:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

.field mOnNext:Lcom/vlingo/midas/gui/WelcomeFragment$onNext;

.field private mStretchLayout:Landroid/widget/LinearLayout;

.field private mStretchListener:Landroid/view/animation/Animation$AnimationListener;

.field private mSvoiceTitle:Landroid/widget/TextView;

.field private mWelcomeTextLayout:Landroid/widget/LinearLayout;

.field private stretch:Landroid/view/animation/Animation;

.field private translateUp:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeIn:Landroid/view/animation/Animation;

    .line 26
    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->translateUp:Landroid/view/animation/Animation;

    .line 27
    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->stretch:Landroid/view/animation/Animation;

    .line 28
    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;

    .line 29
    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;

    .line 30
    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mStretchLayout:Landroid/widget/LinearLayout;

    .line 31
    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mWelcomeTextLayout:Landroid/widget/LinearLayout;

    .line 32
    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mBottomRelLayout:Landroid/widget/RelativeLayout;

    .line 33
    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mNextButtonLayout:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    .line 34
    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mSvoiceTitle:Landroid/widget/TextView;

    .line 45
    new-instance v0, Lcom/vlingo/midas/gui/WelcomeFragment$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/WelcomeFragment$1;-><init>(Lcom/vlingo/midas/gui/WelcomeFragment;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mStretchListener:Landroid/view/animation/Animation$AnimationListener;

    .line 67
    new-instance v0, Lcom/vlingo/midas/gui/WelcomeFragment$2;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/WelcomeFragment$2;-><init>(Lcom/vlingo/midas/gui/WelcomeFragment;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mFadeOutListner:Landroid/view/animation/Animation$AnimationListener;

    .line 181
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/WelcomeFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/WelcomeFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mWelcomeTextLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/WelcomeFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/WelcomeFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/WelcomeFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mSvoiceTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->svoice_title_welcomepage:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mSvoiceTitle:Landroid/widget/TextView;

    .line 96
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->content:Landroid/view/View;

    sget v1, Lcom/vlingo/midas/R$id;->logo_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;

    .line 97
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->content:Landroid/view/View;

    sget v1, Lcom/vlingo/midas/R$id;->bottom_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mBottomRelLayout:Landroid/widget/RelativeLayout;

    .line 98
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->content:Landroid/view/View;

    sget v1, Lcom/vlingo/midas/R$id;->stretch_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mStretchLayout:Landroid/widget/LinearLayout;

    .line 99
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->content:Landroid/view/View;

    sget v1, Lcom/vlingo/midas/R$id;->welcome_text_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mWelcomeTextLayout:Landroid/widget/LinearLayout;

    .line 100
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->content:Landroid/view/View;

    sget v1, Lcom/vlingo/midas/R$id;->next_button_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mNextButtonLayout:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    .line 101
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mNextButtonLayout:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$anim;->fade_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeIn:Landroid/view/animation/Animation;

    .line 105
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeIn:Landroid/view/animation/Animation;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 106
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeIn:Landroid/view/animation/Animation;

    const-wide/16 v1, 0x7d0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 108
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$anim;->stretch_anim:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->stretch:Landroid/view/animation/Animation;

    .line 109
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->stretch:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mStretchListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 110
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$anim;->fade_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;

    .line 111
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mFadeOutListner:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 112
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;

    const-wide/16 v1, 0x384

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 113
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mSvoiceTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 114
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mSvoiceTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 118
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomeFragment;->setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 130
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$anim;->y_translation:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->translateUp:Landroid/view/animation/Animation;

    .line 131
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->translateUp:Landroid/view/animation/Animation;

    const-wide/16 v1, 0x320

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 132
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mBottomRelLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->translateUp:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 133
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mBottomRelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 134
    return-void
.end method

.method private setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/vlingo/midas/gui/WelcomeFragment$3;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/WelcomeFragment$3;-><init>(Lcom/vlingo/midas/gui/WelcomeFragment;)V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 41
    check-cast p1, Lcom/vlingo/midas/gui/WelcomeFragment$onNext;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mOnNext:Lcom/vlingo/midas/gui/WelcomeFragment$onNext;

    .line 42
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 187
    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mNextButtonLayout:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->setButtonClicked()V

    .line 188
    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->stretch:Landroid/view/animation/Animation;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mStretchLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 189
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mOnNext:Lcom/vlingo/midas/gui/WelcomeFragment$onNext;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lcom/vlingo/midas/gui/WelcomeFragment$onNext;->onNextClicked(I)V

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/WelcomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->stretch_layout_1:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 193
    .local v0, "v":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 194
    iget-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->stretch:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    sget v0, Lcom/vlingo/midas/R$layout;->welcome_fragment_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->content:Landroid/view/View;

    .line 89
    invoke-direct {p0}, Lcom/vlingo/midas/gui/WelcomeFragment;->init()V

    .line 90
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->content:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 147
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mFadeOutListner:Landroid/view/animation/Animation$AnimationListener;

    .line 148
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mStretchListener:Landroid/view/animation/Animation$AnimationListener;

    .line 149
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->translateUp:Landroid/view/animation/Animation;

    .line 151
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mNextButtonLayout:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mNextButtonLayout:Lcom/vlingo/midas/gui/customviews/CustomIUXButton;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/CustomIUXButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->stretch:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->stretch:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 157
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->stretch:Landroid/view/animation/Animation;

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;

    if-eqz v0, :cond_2

    .line 161
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 162
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->fadeOut:Landroid/view/animation/Animation;

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    .line 165
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mSvoiceTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 166
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 167
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mLogoLayout:Landroid/widget/LinearLayout;

    .line 168
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mSvoiceTitle:Landroid/widget/TextView;

    .line 170
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mStretchLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    .line 171
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mStretchLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 172
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mStretchLayout:Landroid/widget/LinearLayout;

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mBottomRelLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_5

    .line 175
    iget-object v0, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mBottomRelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 176
    iput-object v1, p0, Lcom/vlingo/midas/gui/WelcomeFragment;->mBottomRelLayout:Landroid/widget/RelativeLayout;

    .line 178
    :cond_5
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 179
    return-void
.end method
