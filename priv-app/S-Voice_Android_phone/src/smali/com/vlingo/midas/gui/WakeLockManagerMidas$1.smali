.class Lcom/vlingo/midas/gui/WakeLockManagerMidas$1;
.super Ljava/lang/Thread;
.source "WakeLockManagerMidas.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/WakeLockManagerMidas;->releaseWakeLock()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/WakeLockManagerMidas;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/WakeLockManagerMidas;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas$1;->this$0:Lcom/vlingo/midas/gui/WakeLockManagerMidas;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 93
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isQueuedAudioTask()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas$1;->this$0:Lcom/vlingo/midas/gui/WakeLockManagerMidas;

    # getter for: Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->access$000(Lcom/vlingo/midas/gui/WakeLockManagerMidas;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas$1;->this$0:Lcom/vlingo/midas/gui/WakeLockManagerMidas;

    # getter for: Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->access$000(Lcom/vlingo/midas/gui/WakeLockManagerMidas;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas$1;->this$0:Lcom/vlingo/midas/gui/WakeLockManagerMidas;

    # getter for: Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->access$000(Lcom/vlingo/midas/gui/WakeLockManagerMidas;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 98
    const-string/jumbo v0, "JDEBUG"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " wakeLock release wakeLock : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas$1;->this$0:Lcom/vlingo/midas/gui/WakeLockManagerMidas;

    # getter for: Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v2}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->access$000(Lcom/vlingo/midas/gui/WakeLockManagerMidas;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/vlingo/midas/gui/WakeLockManagerMidas$1;->this$0:Lcom/vlingo/midas/gui/WakeLockManagerMidas;

    const/4 v1, 0x0

    # setter for: Lcom/vlingo/midas/gui/WakeLockManagerMidas;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0, v1}, Lcom/vlingo/midas/gui/WakeLockManagerMidas;->access$002(Lcom/vlingo/midas/gui/WakeLockManagerMidas;Landroid/os/PowerManager$WakeLock;)Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    goto :goto_0
.end method
