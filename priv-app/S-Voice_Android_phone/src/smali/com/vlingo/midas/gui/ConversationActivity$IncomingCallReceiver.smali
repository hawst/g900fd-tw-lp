.class Lcom/vlingo/midas/gui/ConversationActivity$IncomingCallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ConversationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ConversationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IncomingCallReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ConversationActivity;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0

    .prologue
    .line 5902
    iput-object p1, p0, Lcom/vlingo/midas/gui/ConversationActivity$IncomingCallReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;Lcom/vlingo/midas/gui/ConversationActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity;
    .param p2, "x1"    # Lcom/vlingo/midas/gui/ConversationActivity$1;

    .prologue
    .line 5902
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/ConversationActivity$IncomingCallReceiver;-><init>(Lcom/vlingo/midas/gui/ConversationActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    .line 5905
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5906
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$IncomingCallReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ConversationActivity;->finishTutorial(Landroid/content/Context;)V

    .line 5907
    sget-object v0, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 5911
    :goto_0
    return-void

    .line 5909
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$IncomingCallReceiver;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->finish()V

    goto :goto_0
.end method
