.class public Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;
.super Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;
.source "MessageReadbackBodyHiddenWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final DETAIL:I

.field private final RETIRED:I

.field private callbackBtn:Landroid/widget/Button;

.field private readoutBtn:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->DETAIL:I

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->RETIRED:I

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 4
    .param p1, "mt"    # Lcom/vlingo/core/internal/dialogmanager/types/MessageType;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const/16 v2, 0x8

    .line 84
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 86
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->msgBody:Landroid/widget/TextView;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_safereader_hidden_message_body:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 87
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_2

    .line 88
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->text:Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->messageReadBackButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 93
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 98
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 99
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 24
    check-cast p1, Lcom/vlingo/core/internal/dialogmanager/types/MessageType;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->initialize(Lcom/vlingo/core/internal/dialogmanager/types/MessageType;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->messageContainer:Landroid/widget/LinearLayout;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->openNativeSmsApp()V

    .line 142
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->onFinishInflate()V

    .line 38
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->messageContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    sget v0, Lcom/vlingo/midas/R$id;->btn_readout:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    .line 41
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    :cond_0
    sget v0, Lcom/vlingo/midas/R$id;->btn_callback:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    .line 61
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget$2;-><init>(Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    :cond_1
    return-void
.end method

.method public retire()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 120
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/MessageWidgetBase;->retire()V

    .line 122
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->readoutBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->callbackBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->messageReadBackButtonContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 131
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->messageReadBackButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->messageContainer:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->msg_from:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/vlingo/midas/gui/widgets/MessageReadbackBodyHiddenWidget;->getAccessibilityString(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 135
    return-void
.end method
