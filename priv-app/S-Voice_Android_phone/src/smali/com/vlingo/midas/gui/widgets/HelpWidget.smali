.class public Lcom/vlingo/midas/gui/widgets/HelpWidget;
.super Lcom/vlingo/midas/gui/Widget;
.source "HelpWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/Widget",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/Widget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/HelpWidget;->mContext:Landroid/content/Context;

    .line 32
    new-instance v0, Lcom/vlingo/midas/ui/PackageInfoProvider;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/HelpWidget;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 33
    return-void
.end method

.method private checkIcon(I)V
    .locals 1
    .param p1, "ResId"    # I

    .prologue
    .line 63
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    if-ne p1, v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setMemoIcon()V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    if-ne p1, v0, :cond_2

    .line 66
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setVoiceRecorderIcon()V

    goto :goto_0

    .line 67
    :cond_2
    sget v0, Lcom/vlingo/midas/R$drawable;->help_weather:I

    if-ne p1, v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->setWeatherIcon()V

    goto :goto_0
.end method

.method private getDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "ResId"    # I

    .prologue
    .line 73
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_s_memo_pen:I

    if-ne p1, v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getMemoIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    .line 75
    :cond_0
    sget v0, Lcom/vlingo/midas/R$drawable;->mainmenu_icon_voice_recorder:I

    if-ne p1, v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getVoiceRecorderIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_1
    sget v0, Lcom/vlingo/midas/R$drawable;->help_weather:I

    if-ne p1, v0, :cond_2

    .line 78
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/HelpWidget;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/PackageInfoProvider;->getWeatherIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 80
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setTextFromHtml(IIZ)V
    .locals 7
    .param p1, "textResId"    # I
    .param p2, "textViewResId"    # I
    .param p3, "array"    # Z

    .prologue
    .line 84
    invoke-virtual {p0, p2}, Lcom/vlingo/midas/gui/widgets/HelpWidget;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 86
    .local v5, "tv":Landroid/widget/TextView;
    if-eqz v5, :cond_0

    if-eqz p2, :cond_0

    .line 87
    if-nez p3, :cond_1

    .line 88
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/HelpWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 92
    .local v1, "dyk":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/HelpWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 93
    .local v4, "s":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 92
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 96
    .end local v4    # "s":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 24
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/HelpWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 0

    .prologue
    .line 37
    invoke-super {p0}, Lcom/vlingo/midas/gui/Widget;->onFinishInflate()V

    .line 38
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public setAddView(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 42
    const-string/jumbo v5, "EXTRA_SUBTITLE_ICON"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 43
    .local v2, "iconResId":I
    const-string/jumbo v5, "EXTRA_TITLE_BAR"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 44
    .local v4, "subtitleResId":I
    const-string/jumbo v5, "EXTRA_EXAMPLE_LIST"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 46
    .local v1, "exArrResId":I
    if-eqz v2, :cond_0

    .line 47
    sget v5, Lcom/vlingo/midas/R$id;->wycs_detail_image:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/widgets/HelpWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 49
    .local v3, "iv":Landroid/widget/ImageView;
    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/widgets/HelpWidget;->checkIcon(I)V

    .line 50
    invoke-direct {p0, v2}, Lcom/vlingo/midas/gui/widgets/HelpWidget;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 51
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_1

    .line 52
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 58
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "iv":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    sget v5, Lcom/vlingo/midas/R$id;->wycs_title:I

    invoke-direct {p0, v4, v5, v6}, Lcom/vlingo/midas/gui/widgets/HelpWidget;->setTextFromHtml(IIZ)V

    .line 59
    sget v5, Lcom/vlingo/midas/R$id;->text_examples:I

    const/4 v6, 0x1

    invoke-direct {p0, v1, v5, v6}, Lcom/vlingo/midas/gui/widgets/HelpWidget;->setTextFromHtml(IIZ)V

    .line 60
    return-void

    .line 54
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "iv":Landroid/widget/ImageView;
    :cond_1
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
