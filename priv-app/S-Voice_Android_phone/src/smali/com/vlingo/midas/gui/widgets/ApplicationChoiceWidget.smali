.class public Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;
.super Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;
.source "ApplicationChoiceWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$1;,
        Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
        ">;>;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private mContext:Landroid/content/Context;

.field private mListView:Landroid/widget/ListView;

.field private screenDensity:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mContext:Landroid/content/Context;

    .line 54
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->getIconFromActivityName(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private getIconFromActivityName(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p1, "activityName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 88
    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 89
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    .line 92
    .local v2, "icon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, p2, p1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    .local v1, "cn":Landroid/content/ComponentName;
    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 94
    .local v0, "ai":Landroid/content/pm/ActivityInfo;
    invoke-virtual {v0, v3}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 98
    .end local v0    # "ai":Landroid/content/pm/ActivityInfo;
    .end local v1    # "cn":Landroid/content/ComponentName;
    :goto_0
    return-object v2

    .line 95
    :catch_0
    move-exception v4

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 39
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 5
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;",
            ">;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    .line 78
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 79
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 80
    iget v2, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int/lit16 v2, v2, 0xa0

    iput v2, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->screenDensity:I

    .line 81
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 82
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mListView:Landroid/widget/ListView;

    new-instance v3, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;

    iget-object v4, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mContext:Landroid/content/Context;

    invoke-direct {v3, p0, v4, p1}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget$ApplicationsAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 83
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.vlingo.core.internal.dialogmanager.DataTransfered"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    .local v1, "transferIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->getDisplayCount(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 85
    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onFinishInflate()V

    .line 59
    sget v0, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mListView:Landroid/widget/ListView;

    .line 63
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 65
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->retire()V

    .line 108
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 109
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v2

    invoke-interface {v2, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;

    .line 110
    .local v0, "appinfo":Lcom/vlingo/core/internal/util/OpenAppUtil$AppInfo;
    if-eqz v0, :cond_0

    .line 111
    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mActionListener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    invoke-interface {v2, v1, v0}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 112
    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->mListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/ApplicationChoiceWidget;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 71
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/FlexibleRepeatabilityWidget;->onMeasure(II)V

    .line 72
    return-void
.end method
