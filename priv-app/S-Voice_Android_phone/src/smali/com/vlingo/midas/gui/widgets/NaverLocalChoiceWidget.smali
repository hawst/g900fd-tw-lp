.class public Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;
.super Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.source "NaverLocalChoiceWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$1;,
        Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ViewHolder;,
        Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;,
        Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$CallClickListener;,
        Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$MoreClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/midas/gui/widgets/BargeInWidget",
        "<",
        "Lcom/vlingo/midas/naver/NaverAdaptor;",
        ">;"
    }
.end annotation


# static fields
.field private static mDensity:I


# instance fields
.field private listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

.field private localSearchListings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private lsAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

.field private final mContext:Landroid/content/Context;

.field private mLocalSearchList:Landroid/widget/ListView;

.field private mMainLocation:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x2

    sput v0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mDensity:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mContext:Landroid/content/Context;

    .line 54
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int/lit16 v0, v0, 0xa0

    sput v0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mDensity:I

    .line 55
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->localSearchListings:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mMainLocation:Ljava/lang/String;

    return-object v0
.end method

.method protected static setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V
    .locals 4
    .param p0, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 78
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 79
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$dimen;->item_naver_local_search_height:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Landroid/widget/ListView;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sget v3, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mDensity:I

    mul-int/2addr v2, v3

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 82
    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    .line 84
    return-void
.end method


# virtual methods
.method public initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 10
    .param p1, "adaptor"    # Lcom/vlingo/midas/naver/NaverAdaptor;
    .param p2, "decor"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "widgetKey"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "listener"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    const/16 v9, 0x8

    .line 94
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .line 95
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->lsAdaptor:Lcom/vlingo/midas/naver/NaverAdaptor;

    .line 96
    invoke-virtual {p1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v6

    iget-object v6, v6, Lcom/vlingo/midas/naver/NaverXMLParser;->itemList:Ljava/util/List;

    iput-object v6, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->localSearchListings:Ljava/util/List;

    .line 103
    new-instance v1, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;

    const/4 v6, 0x0

    invoke-direct {v1, p0, v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;-><init>(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$1;)V

    .line 104
    .local v1, "adapter":Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$ListAdapter;
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mLocalSearchList:Landroid/widget/ListView;

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 116
    sget v6, Lcom/vlingo/midas/R$id;->text_Local_MainTitle:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 117
    .local v4, "mainTitle":Landroid/widget/TextView;
    sget v6, Lcom/vlingo/midas/R$id;->text_Local_address:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 118
    .local v2, "address":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/vlingo/midas/naver/NaverAdaptor;->getNaverXML()Lcom/vlingo/midas/naver/NaverXMLParser;

    move-result-object v0

    .line 119
    .local v0, "Result":Lcom/vlingo/midas/naver/NaverXMLParser;
    iget-object v6, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->propertyList:Ljava/util/Hashtable;

    const-string/jumbo v7, "biz"

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 120
    .local v3, "biz":Ljava/lang/String;
    iget-object v6, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->propertyList:Ljava/util/Hashtable;

    const-string/jumbo v7, "location"

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mMainLocation:Ljava/lang/String;

    .line 121
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 122
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    :goto_0
    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mMainLocation:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mMainLocation:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 125
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mMainLocation:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mContext:Landroid/content/Context;

    sget v8, Lcom/vlingo/midas/R$string;->naver_local_near_suffix:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    :goto_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 130
    sget v6, Lcom/vlingo/midas/R$id;->logo:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 132
    :cond_0
    sget v6, Lcom/vlingo/midas/R$id;->btn_more:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/gui/customviews/Button;

    .line 133
    .local v5, "moreBtn":Lcom/vlingo/midas/gui/customviews/Button;
    new-instance v7, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$MoreClickListener;

    iget-object v6, v0, Lcom/vlingo/midas/naver/NaverXMLParser;->propertyList:Ljava/util/Hashtable;

    const-string/jumbo v8, "moreUrl"

    invoke-virtual {v6, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v7, p0, v6}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget$MoreClickListener;-><init>(Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Lcom/vlingo/midas/gui/customviews/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    return-void

    .line 123
    .end local v5    # "moreBtn":Lcom/vlingo/midas/gui/customviews/Button;
    :cond_1
    const-string/jumbo v6, "--"

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 127
    :cond_2
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public bridge synthetic initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;
    .param p3, "x2"    # Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;
    .param p4, "x3"    # Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    .prologue
    .line 37
    check-cast p1, Lcom/vlingo/midas/naver/NaverAdaptor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->initialize(Lcom/vlingo/midas/naver/NaverAdaptor;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V

    return-void
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x0

    return v0
.end method

.method public isWidgetReplaceable()Z
    .locals 1

    .prologue
    .line 363
    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onFinishInflate()V

    .line 60
    sget v0, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mLocalSearchList:Landroid/widget/ListView;

    .line 61
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mLocalSearchList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 67
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->mLocalSearchList:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/vlingo/midas/gui/widgets/NaverLocalChoiceWidget;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    .line 73
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onMeasure(II)V

    .line 75
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 89
    invoke-super {p0, p1, p2, p3, p4}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->onSizeChanged(IIII)V

    .line 90
    return-void
.end method
