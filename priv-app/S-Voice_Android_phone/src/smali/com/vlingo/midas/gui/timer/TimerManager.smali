.class public Lcom/vlingo/midas/gui/timer/TimerManager;
.super Ljava/lang/Object;
.source "TimerManager.java"


# static fields
.field public static final RESET:I = 0x3

.field public static final STARTED:I = 0x1

.field public static final STOPPED:I = 0x2

.field public static final TIMER_NOTI_ID:I = 0x14a9d

.field public static hour:I

.field public static inputMillis:J

.field public static isStarted:Z

.field public static minute:I

.field public static remainMillis:J

.field public static second:I

.field public static state:I

.field public static timer:Landroid/os/CountDownTimer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/timer/TimerManager;->isStarted:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static initTime()V
    .locals 11

    .prologue
    const-wide/32 v9, 0x36ee80

    const-wide/32 v7, 0xea60

    const-wide/16 v5, 0x3e8

    .line 22
    sget-wide v1, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    rem-long/2addr v1, v5

    long-to-int v0, v1

    .line 23
    .local v0, "mili_second":I
    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    .line 24
    sget-wide v1, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    rsub-int v3, v0, 0x3e8

    int-to-long v3, v3

    add-long/2addr v1, v3

    sput-wide v1, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    .line 27
    :cond_0
    sget-wide v1, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    div-long/2addr v1, v9

    long-to-int v1, v1

    sput v1, Lcom/vlingo/midas/gui/timer/TimerManager;->hour:I

    .line 28
    sget-wide v1, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    rem-long/2addr v1, v9

    div-long/2addr v1, v7

    long-to-int v1, v1

    sput v1, Lcom/vlingo/midas/gui/timer/TimerManager;->minute:I

    .line 29
    sget-wide v1, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    rem-long/2addr v1, v7

    div-long/2addr v1, v5

    long-to-int v1, v1

    sput v1, Lcom/vlingo/midas/gui/timer/TimerManager;->second:I

    .line 30
    return-void
.end method

.method public static setState(I)V
    .locals 2
    .param p0, "newState"    # I

    .prologue
    const/4 v0, 0x1

    .line 33
    packed-switch p0, :pswitch_data_0

    .line 55
    :goto_0
    return-void

    .line 35
    :pswitch_0
    sput-boolean v0, Lcom/vlingo/midas/gui/timer/TimerManager;->isStarted:Z

    .line 36
    sput v0, Lcom/vlingo/midas/gui/timer/TimerManager;->state:I

    goto :goto_0

    .line 40
    :pswitch_1
    sget-object v0, Lcom/vlingo/midas/gui/timer/TimerManager;->timer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 41
    sget-object v0, Lcom/vlingo/midas/gui/timer/TimerManager;->timer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 43
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/gui/timer/TimerManager;->isStarted:Z

    .line 44
    const/4 v0, 0x2

    sput v0, Lcom/vlingo/midas/gui/timer/TimerManager;->state:I

    goto :goto_0

    .line 48
    :pswitch_2
    sget-object v0, Lcom/vlingo/midas/gui/timer/TimerManager;->timer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_1

    .line 49
    sget-object v0, Lcom/vlingo/midas/gui/timer/TimerManager;->timer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 51
    :cond_1
    sget-wide v0, Lcom/vlingo/midas/gui/timer/TimerManager;->inputMillis:J

    sput-wide v0, Lcom/vlingo/midas/gui/timer/TimerManager;->remainMillis:J

    .line 52
    const/4 v0, 0x3

    sput v0, Lcom/vlingo/midas/gui/timer/TimerManager;->state:I

    goto :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
