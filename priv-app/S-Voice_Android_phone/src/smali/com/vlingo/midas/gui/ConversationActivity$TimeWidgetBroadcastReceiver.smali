.class Lcom/vlingo/midas/gui/ConversationActivity$TimeWidgetBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ConversationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/ConversationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TimeWidgetBroadcastReceiver"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 4780
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/gui/ConversationActivity$1;

    .prologue
    .line 4780
    invoke-direct {p0}, Lcom/vlingo/midas/gui/ConversationActivity$TimeWidgetBroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4783
    if-eqz p2, :cond_3

    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v2, :cond_3

    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    instance-of v2, v2, Lcom/vlingo/midas/gui/widgets/ClockWidget;

    if-eqz v2, :cond_3

    .line 4784
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 4786
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v2, "android.intent.action.TIME_TICK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4787
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 4788
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 4793
    .end local v1    # "extras":Landroid/os/Bundle;
    :cond_0
    const-string/jumbo v2, "android.intent.action.TIME_SET"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4797
    :cond_1
    const-string/jumbo v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4801
    :cond_2
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v2, :cond_3

    .line 4802
    sget-object v2, Lcom/vlingo/midas/gui/ConversationActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v2, p2}, Lcom/vlingo/midas/gui/Widget;->processSystemMessage(Landroid/content/Intent;)V

    .line 4804
    .end local v0    # "action":Ljava/lang/String;
    :cond_3
    return-void
.end method
