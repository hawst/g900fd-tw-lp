.class public abstract Lcom/vlingo/midas/gui/widgets/BargeInWidget;
.super Lcom/vlingo/midas/gui/Widget;
.source "BargeInWidget.java"

# interfaces
.implements Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Type:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/vlingo/midas/gui/Widget",
        "<TType;>;",
        "Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;"
    }
.end annotation


# static fields
.field public static final MAX_WIDGET_ITEMS:I = 0x6


# instance fields
.field private capSize:I

.field private mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

.field protected mWordList:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

.field protected moreBtn:Landroid/widget/Button;

.field protected msgCounts:Landroid/widget/TextView;

.field private final showMoreButton:Z

.field private final showMsgcounts:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/Widget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    sget-object v0, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->VoiceTalkAll:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mWordList:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    .line 58
    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    .line 59
    iput-object v2, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    .line 61
    const-string/jumbo v0, "multi.widget.client.showmorebutton"

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->showMoreButton:Z

    .line 62
    const-string/jumbo v0, "multi.widget.client.showcounts"

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->showMsgcounts:Z

    .line 76
    invoke-static {}, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->getMultiWidgetItemsInitialMax()I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->capSize:I

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/widgets/BargeInWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/BargeInWidget;

    .prologue
    .line 48
    iget v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->capSize:I

    return v0
.end method

.method static synthetic access$002(Lcom/vlingo/midas/gui/widgets/BargeInWidget;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/widgets/BargeInWidget;
    .param p1, "x1"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->capSize:I

    return p1
.end method

.method private getBargeInRecognizer()Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    .locals 1

    .prologue
    .line 65
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-direct {v0}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 67
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->InitBargeInRecognizer(Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;)V

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    return-object v0
.end method

.method protected static setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V
    .locals 9
    .param p0, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 262
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 263
    .local v3, "listAdapter":Landroid/widget/ListAdapter;
    if-nez v3, :cond_1

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-gtz v7, :cond_0

    .line 269
    const/4 v6, 0x0

    .line 270
    .local v6, "totalHeight":I
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    .line 271
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_2

    .line 272
    const/4 v7, 0x0

    invoke-interface {v3, v2, v7, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 274
    .local v4, "listItem":Landroid/view/View;
    const/4 v7, 0x0

    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v4, v7, v8}, Landroid/view/View;->measure(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    :goto_2
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    .line 271
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 275
    :catch_0
    move-exception v1

    .line 276
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 281
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "listItem":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 282
    .local v5, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v7

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    mul-int/2addr v7, v8

    add-int/2addr v7, v6

    iput v7, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 283
    invoke-virtual {p0, v5}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 284
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    goto :goto_0
.end method

.method protected static setListViewHeightBasedOnChildrens(Landroid/widget/ListView;Z)V
    .locals 9
    .param p0, "listView"    # Landroid/widget/ListView;
    .param p1, "force"    # Z

    .prologue
    .line 288
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 289
    .local v3, "listAdapter":Landroid/widget/ListAdapter;
    if-nez v3, :cond_1

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v7, :cond_2

    if-eqz p1, :cond_0

    .line 295
    :cond_2
    const/4 v6, 0x0

    .line 296
    .local v6, "totalHeight":I
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    .line 297
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_3

    .line 298
    const/4 v7, 0x0

    invoke-interface {v3, v2, v7, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 300
    .local v4, "listItem":Landroid/view/View;
    const/4 v7, 0x0

    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v4, v7, v8}, Landroid/view/View;->measure(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :goto_2
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    .line 297
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 301
    :catch_0
    move-exception v1

    .line 302
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 307
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "listItem":Landroid/view/View;
    :cond_3
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 308
    .local v5, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v7

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    mul-int/2addr v7, v8

    add-int/2addr v7, v6

    iput v7, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 309
    invoke-virtual {p0, v5}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 310
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    goto :goto_0
.end method


# virtual methods
.method public getLimitedCount(I)I
    .locals 6
    .param p1, "naturalSize"    # I

    .prologue
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 80
    invoke-static {p1}, Lcom/vlingo/midas/samsungutils/utils/WidgetUtils;->getDisplayCount(I)I

    move-result v0

    .line 81
    .local v0, "toReturn":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 82
    :cond_0
    iget v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->capSize:I

    if-le p1, v1, :cond_4

    .line 83
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->capSize:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->capSize:I

    invoke-static {}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->getMultiWidgetItemsUltimateMax()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 87
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    if-eqz v1, :cond_2

    .line 91
    iget v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->capSize:I

    invoke-static {}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->getMultiWidgetItemsUltimateMax()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 92
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 93
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    sget v2, Lcom/vlingo/midas/R$string;->core_qa_more:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 108
    :cond_2
    :goto_0
    return v0

    .line 95
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 96
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    sget v2, Lcom/vlingo/midas/R$string;->core_qa_more:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 100
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    if-eqz v1, :cond_5

    .line 101
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 103
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 104
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public handleCancel()V
    .locals 1

    .prologue
    .line 129
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelDialog()V

    .line 130
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->stopRecognition()V

    .line 131
    return-void
.end method

.method public handleNext()V
    .locals 0

    .prologue
    .line 114
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    return-void
.end method

.method public handlePrevious()V
    .locals 0

    .prologue
    .line 118
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    return-void
.end method

.method public handleStop()V
    .locals 1

    .prologue
    .line 122
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 123
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->deleteQueuedTtsTasks()V

    .line 124
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->stopRecognition()V

    .line 125
    return-void
.end method

.method public abstract initialize(Ljava/lang/Object;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TType;",
            "Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;",
            "Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;",
            "Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;",
            ")V"
        }
    .end annotation
.end method

.method public isRecognizing()Z
    .locals 2

    .prologue
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    const/4 v0, 0x1

    .line 196
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->getBargeInRecognizer()Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getState()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 199
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    const/4 v1, 0x0

    .line 226
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->showMoreButton:Z

    if-eqz v0, :cond_1

    .line 227
    sget v0, Lcom/vlingo/midas/R$id;->btn_more:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    .line 231
    :goto_0
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->showMsgcounts:Z

    if-eqz v0, :cond_2

    .line 232
    sget v0, Lcom/vlingo/midas/R$id;->counts:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    .line 236
    :goto_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    new-instance v1, Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget$1;-><init>(Lcom/vlingo/midas/gui/widgets/BargeInWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mWordList:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->startRecognition(Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;)V

    .line 259
    return-void

    .line 229
    :cond_1
    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->moreBtn:Landroid/widget/Button;

    goto :goto_0

    .line 234
    :cond_2
    iput-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->msgCounts:Landroid/widget/TextView;

    goto :goto_1
.end method

.method public onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;

    .prologue
    .line 160
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isQueuedTtsTask()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->stopRecognition()V

    .line 165
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/Widget;->onRequestCancelled(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonCanceled;)V

    .line 166
    return-void
.end method

.method public onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 140
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isQueuedTtsTask()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->stopRecognition()V

    .line 145
    :cond_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/Widget;->onRequestDidPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 146
    return-void
.end method

.method public onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V
    .locals 1
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;
    .param p2, "reason"    # Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;

    .prologue
    .line 150
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->isQueuedTtsTask()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->stopRecognition()V

    .line 155
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/gui/Widget;->onRequestIgnored(Lcom/vlingo/core/internal/audio/AudioRequest;Lcom/vlingo/core/internal/audio/IAudioPlaybackService$AudioPlaybackListener$ReasonIgnored;)V

    .line 156
    return-void
.end method

.method public onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/vlingo/core/internal/audio/AudioRequest;

    .prologue
    .line 135
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/Widget;->onRequestWillPlay(Lcom/vlingo/core/internal/audio/AudioRequest;)V

    .line 136
    return-void
.end method

.method public onResults([Ljava/lang/String;)V
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    const/4 v3, 0x1

    .line 327
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    if-nez v1, :cond_1

    .line 349
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v1}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getIntBargeInResult()I

    move-result v0

    .line 333
    .local v0, "result":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 335
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mWordList:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    sget-object v2, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->VoiceTalkAll:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    if-ne v1, v2, :cond_2

    .line 336
    if-ne v0, v3, :cond_0

    .line 337
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->handleStop()V

    goto :goto_0

    .line 339
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mWordList:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    sget-object v2, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->VoiceTalkSchedule:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    if-ne v1, v2, :cond_0

    .line 340
    if-ne v0, v3, :cond_3

    .line 341
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->handleStop()V

    goto :goto_0

    .line 342
    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 343
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->handleNext()V

    goto :goto_0

    .line 344
    :cond_4
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 345
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->handlePrevious()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 315
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->stopRecognition()V

    .line 316
    return-void
.end method

.method public setWordList(Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;)V
    .locals 0
    .param p1, "list"    # Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    .prologue
    .line 204
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->mWordList:Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    .line 205
    return-void
.end method

.method public startRecognition(Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;)V
    .locals 5
    .param p1, "listId"    # Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;

    .prologue
    .line 170
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    const/4 v1, 0x0

    .line 173
    .local v1, "defaultSettingValue":Z
    const-string/jumbo v3, "barge_in_enabled"

    invoke-static {v3, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 176
    .local v0, "bargeinEnabled":Z
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->isRecognizing()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_0

    .line 178
    :try_start_0
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->getBargeInRecognizer()Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/widgets/BargeInWidget$WordList;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->startBargeIn(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 181
    :catch_0
    move-exception v2

    .line 182
    .local v2, "ex":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public stopRecognition()V
    .locals 1

    .prologue
    .line 188
    .local p0, "this":Lcom/vlingo/midas/gui/widgets/BargeInWidget;, "Lcom/vlingo/midas/gui/widgets/BargeInWidget<TType;>;"
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->isRecognizing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/vlingo/midas/gui/widgets/BargeInWidget;->getBargeInRecognizer()Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->stopBargeIn()V

    .line 193
    :cond_0
    return-void
.end method
