.class Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;
.super Ljava/lang/Object;
.source "MultipleMessageWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->initialize(Ljava/util/List;Lcom/vlingo/core/internal/dialogmanager/WidgetDecorator;Lcom/vlingo/core/internal/dialogmanager/util/WidgetUtil$WidgetKey;Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

.field final synthetic val$msgs:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;->val$msgs:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 70
    new-instance v0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;

    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    iget-object v2, v2, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;->val$msgs:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;-><init>(Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;Landroid/content/Context;Ljava/util/List;)V

    .line 71
    .local v0, "adapter":Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->mMultipleMessages:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 72
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;->mMultipleMessages:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 73
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/MultipleMessageWidget$ListAdapterMessages;->notifyDataSetChanged()V

    .line 74
    return-void
.end method
