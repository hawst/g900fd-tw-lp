.class Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;
.super Ljava/lang/Object;
.source "EditWhatYouSaidHelpActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

.field final synthetic val$root:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 521
    iput-object p1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iput-object p2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->val$root:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 534
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->keyboardHidden:I

    if-ne v2, v5, :cond_1

    .line 535
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 536
    .local v1, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->val$root:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 541
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->val$root:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v0, v2, v3

    .line 542
    .local v0, "keypadHeight":I
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v5, :cond_3

    .line 543
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iget v2, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->KEYPAD_DETECT_VALUE_PORT:I

    if-le v0, v2, :cond_2

    .line 545
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getCurrGuideState()Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    move-result-object v2

    sget-object v3, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iget v2, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mKeypadHeight:I

    if-eq v2, v0, :cond_0

    .line 546
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iget-object v2, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    invoke-virtual {v2, v4}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->removeMessages(I)V

    .line 547
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iget-object v2, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    invoke-virtual {v2, v4}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessage(I)Z

    .line 549
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iput v0, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mKeypadHeight:I

    .line 567
    .end local v0    # "keypadHeight":I
    .end local v1    # "rect":Landroid/graphics/Rect;
    :cond_1
    :goto_0
    return-void

    .line 552
    .restart local v0    # "keypadHeight":I
    .restart local v1    # "rect":Landroid/graphics/Rect;
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 553
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iput v0, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mKeypadHeight:I

    goto :goto_0

    .line 557
    :cond_3
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iget v2, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->KEYPAD_DETECT_VALUE_LAND:I

    if-le v0, v2, :cond_1

    .line 559
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getCurrGuideState()Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    move-result-object v2

    sget-object v3, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iget v2, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mKeypadHeight:I

    if-eq v2, v0, :cond_4

    .line 560
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iget-object v2, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    invoke-virtual {v2, v4}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->removeMessages(I)V

    .line 561
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iget-object v2, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    invoke-virtual {v2, v4}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessage(I)Z

    .line 563
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;->this$0:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    iput v0, v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mKeypadHeight:I

    goto :goto_0
.end method
