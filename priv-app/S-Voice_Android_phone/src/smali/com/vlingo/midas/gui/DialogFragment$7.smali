.class Lcom/vlingo/midas/gui/DialogFragment$7;
.super Ljava/lang/Object;
.source "DialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/DialogFragment;->doScrollAnimMoveList(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/DialogFragment;

.field final synthetic val$height:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/DialogFragment;I)V
    .locals 0

    .prologue
    .line 1952
    iput-object p1, p0, Lcom/vlingo/midas/gui/DialogFragment$7;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    iput p2, p0, Lcom/vlingo/midas/gui/DialogFragment$7;->val$height:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1956
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$7;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1957
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$7;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->isScrollingDone:Z
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragment;->access$500(Lcom/vlingo/midas/gui/DialogFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1958
    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->currentPhone:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;
    invoke-static {}, Lcom/vlingo/midas/gui/DialogFragment;->access$400()Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/DialogFragment$PhoneType;->K_DEVICE:Lcom/vlingo/midas/gui/DialogFragment$PhoneType;

    if-ne v0, v1, :cond_1

    .line 1959
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$7;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/midas/gui/DialogFragment$7;->val$height:I

    add-int/lit8 v1, v1, -0xa

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    .line 1965
    :cond_0
    :goto_0
    return-void

    .line 1961
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/DialogFragment$7;->this$0:Lcom/vlingo/midas/gui/DialogFragment;

    # getter for: Lcom/vlingo/midas/gui/DialogFragment;->dialogScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/vlingo/midas/gui/DialogFragment;->access$300(Lcom/vlingo/midas/gui/DialogFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/midas/gui/DialogFragment$7;->val$height:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_0
.end method
