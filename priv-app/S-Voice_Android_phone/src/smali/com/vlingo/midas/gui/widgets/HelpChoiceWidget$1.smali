.class Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;
.super Ljava/lang/Object;
.source "HelpChoiceWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->setAddView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

.field final synthetic val$row:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    iput-object p2, p0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->val$row:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 20
    .param p1, "paramView"    # Landroid/view/View;

    .prologue
    .line 214
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-nez v3, :cond_a

    const/4 v3, 0x1

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 222
    sget v3, Lcom/vlingo/midas/R$id;->help_expander_icon:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 223
    .local v12, "foldIcon":Landroid/widget/ImageView;
    if-eqz v12, :cond_0

    .line 224
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 225
    new-instance v2, Landroid/view/animation/RotateAnimation;

    const/4 v3, 0x0

    const/high16 v4, 0x43340000    # 180.0f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 228
    .local v2, "anim":Landroid/view/animation/RotateAnimation;
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 229
    const-wide/16 v3, 0x2bc

    invoke-virtual {v2, v3, v4}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 230
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 231
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 232
    invoke-virtual {v12, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 233
    const-string/jumbo v3, "."

    invoke-virtual {v12, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 248
    .end local v2    # "anim":Landroid/view/animation/RotateAnimation;
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->val$row:Landroid/view/View;

    move-object/from16 v19, v0

    .line 250
    .local v19, "v":Landroid/view/View;
    sget v3, Lcom/vlingo/midas/R$id;->header1_examples:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    .line 251
    .local v13, "header1_examples":Landroid/widget/LinearLayout;
    sget v3, Lcom/vlingo/midas/R$id;->wcis_widget_row_divider1:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 252
    .local v10, "divider1":Landroid/view/View;
    sget v3, Lcom/vlingo/midas/R$id;->wcis_widget_row_divider2:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 257
    .local v11, "divider2":Landroid/view/View;
    invoke-virtual {v13}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_d

    .line 259
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 260
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 261
    if-eqz v10, :cond_1

    .line 262
    const/4 v3, 0x4

    invoke-virtual {v10, v3}, Landroid/view/View;->setVisibility(I)V

    .line 263
    :cond_1
    if-eqz v11, :cond_2

    .line 264
    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Landroid/view/View;->setVisibility(I)V

    .line 269
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 270
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$id;->header1_examples:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/LinearLayout;

    .line 272
    .local v14, "header1_examples_expanded":Landroid/widget/LinearLayout;
    const/16 v3, 0x8

    invoke-virtual {v14, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 273
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$id;->help_expander_icon:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    .line 274
    .local v16, "imageView":Landroid/widget/ImageView;
    invoke-virtual/range {v16 .. v16}, Landroid/widget/ImageView;->clearAnimation()V

    .line 275
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->talkback_expand_collapse_lists:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 276
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$100(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setSelected(Z)V

    .line 278
    .end local v14    # "header1_examples_expanded":Landroid/widget/LinearLayout;
    .end local v16    # "imageView":Landroid/widget/ImageView;
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    move-object/from16 v0, v19

    # setter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;
    invoke-static {v3, v0}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$102(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;Landroid/view/View;)Landroid/view/View;

    .line 290
    :cond_4
    :goto_3
    const/16 v3, 0x3e9

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getId()I

    move-result v4

    if-ne v3, v4, :cond_5

    if-eqz v10, :cond_5

    .line 291
    const/4 v3, 0x4

    invoke-virtual {v10, v3}, Landroid/view/View;->setVisibility(I)V

    .line 292
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 293
    if-eqz v11, :cond_5

    .line 294
    const/16 v3, 0x8

    invoke-virtual {v11, v3}, Landroid/view/View;->setVisibility(I)V

    .line 297
    :cond_5
    sget-object v3, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->view:Landroid/view/View;

    if-eqz v3, :cond_6

    sget-object v3, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->view:Landroid/view/View;

    move-object/from16 v0, p1

    if-eq v3, v0, :cond_6

    sget-object v3, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->view:Landroid/view/View;

    sget v4, Lcom/vlingo/midas/R$id;->wcis_widget_row_divider1:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 298
    sget-object v3, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->view:Landroid/view/View;

    sget v4, Lcom/vlingo/midas/R$id;->wcis_widget_row_divider1:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 301
    :cond_6
    sput-object p1, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->view:Landroid/view/View;

    .line 303
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 304
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    const/16 v4, 0x9b

    # setter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->offsetLt03:I
    invoke-static {v3, v4}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$202(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;I)I

    .line 306
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneLowGUI()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 307
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    const/16 v4, 0x4b

    # setter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->offsetLt03:I
    invoke-static {v3, v4}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$202(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;I)I

    .line 310
    :cond_8
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static {}, Lcom/vlingo/midas/util/MiniModeUtils;->isSVoiceMiniMode()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 312
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$300(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/view/WindowManager;

    .line 313
    .local v17, "mWinMgr":Landroid/view/WindowManager;
    invoke-interface/range {v17 .. v17}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v9

    .line 314
    .local v9, "displayHeight":I
    new-instance v18, Landroid/util/DisplayMetrics;

    invoke-direct/range {v18 .. v18}, Landroid/util/DisplayMetrics;-><init>()V

    .line 315
    .local v18, "metrics":Landroid/util/DisplayMetrics;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$300(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 316
    int-to-double v3, v9

    const-wide v5, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v3, v5

    double-to-int v15, v3

    .line 317
    .local v15, "height":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    int-to-float v4, v15

    move-object/from16 v0, v18

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v6, 0x42180000    # 38.0f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    # setter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->miniModeScrollOffSet:I
    invoke-static {v3, v4}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$402(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;I)I

    .line 325
    .end local v9    # "displayHeight":I
    .end local v15    # "height":I
    .end local v17    # "mWinMgr":Landroid/view/WindowManager;
    .end local v18    # "metrics":Landroid/util/DisplayMetrics;
    :cond_9
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v4, v0, v1}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1$1;-><init>(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;Landroid/view/View;)V

    const-wide/16 v5, 0x64

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 349
    return-void

    .line 214
    .end local v10    # "divider1":Landroid/view/View;
    .end local v11    # "divider2":Landroid/view/View;
    .end local v12    # "foldIcon":Landroid/widget/ImageView;
    .end local v13    # "header1_examples":Landroid/widget/LinearLayout;
    .end local v19    # "v":Landroid/view/View;
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 235
    .restart local v12    # "foldIcon":Landroid/widget/ImageView;
    :cond_b
    new-instance v2, Landroid/view/animation/RotateAnimation;

    const/high16 v3, 0x43340000    # 180.0f

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 238
    .restart local v2    # "anim":Landroid/view/animation/RotateAnimation;
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 239
    const-wide/16 v3, 0x2bc

    invoke-virtual {v2, v3, v4}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 240
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 241
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 242
    invoke-virtual {v12, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 243
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->talkback_expand_collapse_lists:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 266
    .end local v2    # "anim":Landroid/view/animation/RotateAnimation;
    .restart local v10    # "divider1":Landroid/view/View;
    .restart local v11    # "divider2":Landroid/view/View;
    .restart local v13    # "header1_examples":Landroid/widget/LinearLayout;
    .restart local v19    # "v":Landroid/view/View;
    :cond_c
    if-eqz v10, :cond_2

    .line 267
    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 280
    :cond_d
    const/16 v3, 0x8

    invoke-virtual {v13, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 281
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;

    const/4 v4, 0x0

    # setter for: Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->expandedView:Landroid/view/View;
    invoke-static {v3, v4}, Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;->access$102(Lcom/vlingo/midas/gui/widgets/HelpChoiceWidget;Landroid/view/View;)Landroid/view/View;

    .line 282
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 283
    if-eqz v10, :cond_e

    .line 284
    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Landroid/view/View;->setVisibility(I)V

    .line 285
    :cond_e
    if-eqz v11, :cond_4

    .line 286
    const/16 v3, 0x8

    invoke-virtual {v11, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3
.end method
