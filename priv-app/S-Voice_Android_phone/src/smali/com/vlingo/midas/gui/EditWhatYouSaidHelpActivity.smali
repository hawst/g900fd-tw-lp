.class public Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "EditWhatYouSaidHelpActivity.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;
.implements Lcom/vlingo/core/internal/audio/MicAnimationListener;
.implements Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;
.implements Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;,
        Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;,
        Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$FilePickerResultListener;,
        Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;,
        Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;,
        Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;,
        Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$AppCarMode;
    }
.end annotation


# static fields
.field public static DBG:Z = false

.field public static final DEFAULT_KEYPAD_HEIGHT:I = 0x34b

.field public static final DEFAULT_PREDICTION_HEIGHT:I = 0x69

.field public static final DISPLAY_FROM_BACKGROUND:Ljava/lang/String; = "displayFromBackground"

.field private static final FILE_PICKER_RESULT:I = 0x1

.field public static final HELP_MSG_DONE_S:I = 0x3

.field public static final HELP_MSG_EDIT_S:I = 0x2

.field public static final HELP_MSG_FINISH_S:I = 0x4

.field public static final HELP_MSG_READY_S:I = 0x1

.field public static final IS_FIRST_LAUNCH:Ljava/lang/String; = "IS_FIRST_LAUNCH"

.field public static final KEYPAD_DETECT_VALUE_LAND_H:I = 0x1f4

.field public static final KEYPAD_DETECT_VALUE_LAND_J:I = 0x1f4

.field public static final KEYPAD_DETECT_VALUE_PORT_H:I = 0x2bc

.field public static final KEYPAD_DETECT_VALUE_PORT_J:I = 0x2bc

.field public static LOG_TAG:Ljava/lang/String; = null

.field private static final MSG_CLEAR_DISMISS_KEYGUARD_FLAG:I = 0x3

.field private static final MSG_NAVI_OFF_FULLSCREEN:I = 0x7

.field private static final MSG_REMOVE_LOCATION_LISTENER:I = 0x4

.field private static final MSG_START_RECORDING:I = 0x2

.field private static final MSG_START_SPOTTING:I = 0x1

.field public static final PRESENT_EDITOR:Ljava/lang/String; = "PRESENT_EDITOR"

.field public static final RESUME_FROM_BACKGROUND:Ljava/lang/String; = "resumeFromBackground"

.field public static SPACE:Ljava/lang/String;

.field public static dayType:I

.field static latestWidget:Lcom/vlingo/midas/gui/Widget;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vlingo/midas/gui/Widget",
            "<*>;"
        }
    .end annotation
.end field

.field public static mBlinkAnimation:Landroid/view/animation/Animation;

.field public static mHelpEditTapIv:Landroid/widget/ImageView;

.field public static mHelpEditTapIvLand:Landroid/widget/ImageView;

.field public static mHelpKeypadTapIv:Landroid/widget/ImageView;

.field public static mHelpKeypadTapIvLand:Landroid/widget/ImageView;

.field private static mIsStartedCustomWakeUpSetting:Z

.field private static mTheme:I

.field private static smCurrentActivity:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

.field private static tts:Landroid/speech/tts/TextToSpeech;


# instance fields
.field private FEATURE_DO_NOT_SHOW_DISCLAIMER_POPUP:Z

.field public KEYPAD_DETECT_VALUE_LAND:I

.field public KEYPAD_DETECT_VALUE_PORT:I

.field private final TAG:Ljava/lang/String;

.field private asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

.field private autoStartOnResume:Z

.field private contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

.field private controlContainer:Landroid/view/View;

.field private controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

.field controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

.field private controlViewContainer:Landroid/view/View;

.field private currentTablet:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;

.field private customTitleSupported:Z

.field private dialogContainer:Landroid/view/View;

.field private dialogFragment:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

.field private displayFromBackground:Z

.field private doneTosAndIux:Z

.field private fullContainer:Landroid/widget/LinearLayout;

.field private gestures:Landroid/view/GestureDetector;

.field private handleTextReqeust:Z

.field private helpContainer:Landroid/view/View;

.field private helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

.field private helpWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private help_layout:Landroid/widget/RelativeLayout;

.field private homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

.field private isWakeLockChanged:Z

.field private landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

.field private mFilePickerResultListener:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$FilePickerResultListener;

.field public mGuideState:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

.field private mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

.field private mHelpDoneBubbleTv:Landroid/widget/TextView;

.field private mHelpDoneBubbleTvLand:Landroid/widget/TextView;

.field public mHelpDoneLayout:Landroid/widget/RelativeLayout;

.field public mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

.field public mHelpEditBubbleLayout:Landroid/widget/RelativeLayout;

.field public mHelpEditBubbleLayoutLand:Landroid/widget/RelativeLayout;

.field private mHelpEditBubbleTv:Landroid/widget/TextView;

.field private mHelpEditBubbleTvLand:Landroid/widget/TextView;

.field public mHelpEditLayout:Landroid/widget/RelativeLayout;

.field public mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

.field private mHelpKeypadBubbleTv:Landroid/widget/TextView;

.field private mHelpKeypadBubbleTvLand:Landroid/widget/TextView;

.field public mHelpKeypadLayout:Landroid/widget/RelativeLayout;

.field public mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

.field private mHelpKeypadTipIv:Landroid/widget/ImageView;

.field private mHelpKeypadTipIvLand:Landroid/widget/ImageView;

.field public mInputMethodService:Landroid/view/inputmethod/InputMethodManager;

.field public mKeypadHeight:I

.field private mOrientation:I

.field private mPhraseSpottedDone:I

.field private mScaleAnimation:Landroid/view/animation/Animation;

.field public mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

.field public mWB:Lcom/vlingo/midas/gui/WidgetBuilder;

.field public mWindowHeight:I

.field private nluEventLogUtil:Lcom/vlingo/midas/util/log/NLUEventLogUtil;

.field private normal_layout:Landroid/widget/RelativeLayout;

.field private present_editor:Z

.field private regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

.field private releaseKeyguardByMsg:Z

.field private screenDensityDPI:F

.field private startRecoBySCO:Z

.field private volatile startedWithCarDock:Z

.field private textRequest:Ljava/lang/String;

.field private tosDialog:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    sput-boolean v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->DBG:Z

    .line 112
    const-string/jumbo v0, "EditWhatYouSaidHelpActivity"

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->LOG_TAG:Ljava/lang/String;

    .line 114
    const-string/jumbo v0, " "

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->SPACE:Ljava/lang/String;

    .line 139
    sget v0, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTheme:I

    .line 165
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    .line 170
    sput-boolean v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mIsStartedCustomWakeUpSetting:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 135
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;->OTHERS:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->currentTablet:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;

    .line 140
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->handleTextReqeust:Z

    .line 141
    iput-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->textRequest:Ljava/lang/String;

    .line 142
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->present_editor:Z

    .line 159
    iput-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tosDialog:Landroid/app/AlertDialog;

    .line 161
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z

    .line 162
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->displayFromBackground:Z

    .line 163
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->isWakeLockChanged:Z

    .line 164
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startedWithCarDock:Z

    .line 174
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->doneTosAndIux:Z

    .line 177
    iput v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mPhraseSpottedDone:I

    .line 181
    const-string/jumbo v0, "EditWhatYouSaidHelpActivity"

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->TAG:Ljava/lang/String;

    .line 182
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->customTitleSupported:Z

    .line 195
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startRecoBySCO:Z

    .line 196
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->releaseKeyguardByMsg:Z

    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->FEATURE_DO_NOT_SHOW_DISCLAIMER_POPUP:Z

    .line 216
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->NONE:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mGuideState:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    .line 219
    new-instance v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    invoke-direct {v0, p0, p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    .line 239
    const/16 v0, 0x34b

    iput v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mKeypadHeight:I

    .line 294
    new-instance v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    .line 726
    new-instance v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$3;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$3;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    .line 2052
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z

    return v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z

    return p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->clearDismissKeyguardFlag()V

    return-void
.end method

.method static synthetic access$300(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tosDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$502(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startRecoBySCO:Z

    return p1
.end method

.method static synthetic access$600(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startSettingsActivity()V

    return-void
.end method

.method static synthetic access$700(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    return-object v0
.end method

.method private acquireWakeLock(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2229
    const-string/jumbo v4, "power"

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 2231
    .local v1, "pm":Landroid/os/PowerManager;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v4

    if-eqz v4, :cond_3

    const-wide/16 v2, 0xfa0

    .line 2234
    .local v2, "timeout":J
    :goto_0
    const-string/jumbo v4, "isThisComeFromHomeKeyDoubleClickConcept"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2237
    .local v0, "isFromDoubleClick":Z
    if-eqz v0, :cond_0

    iget-boolean v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->isWakeLockChanged:Z

    if-eqz v4, :cond_1

    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v4

    if-nez v4, :cond_1

    const-string/jumbo v4, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2242
    :cond_1
    const v4, 0x3000001a

    const-string/jumbo v5, "VoiceTalkTemp"

    invoke-virtual {v1, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 2247
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->isWakeLockChanged:Z

    .line 2249
    :cond_2
    return-void

    .line 2231
    .end local v0    # "isFromDoubleClick":Z
    .end local v2    # "timeout":J
    :cond_3
    const-wide/16 v2, 0x258

    goto :goto_0
.end method

.method private checkFragmentLanguage()V
    .locals 4

    .prologue
    .line 948
    const-string/jumbo v2, "language"

    const-string/jumbo v3, "en-US"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 951
    .local v1, "newLanguageApplication":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ContentFragment;->getFragmentLanguage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ContentFragment;->getFragmentLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 953
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ContentFragment;->onLanguageChanged()V

    .line 954
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->onLanguageChanged()V

    .line 955
    sget v2, Lcom/vlingo/midas/R$id;->myTitle:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 956
    .local v0, "actionBarTitle":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 957
    sget v2, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 967
    .end local v0    # "actionBarTitle":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method private clearDismissKeyguardFlag()V
    .locals 2

    .prologue
    .line 2222
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2223
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x400000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 2225
    :cond_0
    return-void
.end method

.method private configureAutoStart(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 633
    if-nez p1, :cond_0

    .line 656
    :goto_0
    return-void

    .line 642
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isIUXComplete()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 643
    const-string/jumbo v2, "home_auto_listen"

    invoke-static {v2, v0}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 650
    :cond_1
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z

    .line 652
    const-string/jumbo v2, "resumeFromBackground"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "displayFromBackground"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-boolean v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z

    if-nez v2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->displayFromBackground:Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private doDismissKeyguard()V
    .locals 10

    .prologue
    .line 588
    const/4 v5, 0x0

    .line 590
    .local v5, "serviceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    const-string/jumbo v6, "android.os.ServiceManager"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 595
    :goto_0
    if-eqz v5, :cond_0

    .line 596
    const/4 v3, 0x0

    .line 598
    .local v3, "getService":Ljava/lang/reflect/Method;
    :try_start_1
    const-string/jumbo v6, "getService"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 604
    :goto_1
    if-eqz v3, :cond_0

    .line 605
    const/4 v1, 0x0

    .line 607
    .local v1, "binder":Landroid/os/IBinder;
    const/4 v6, 0x0

    const/4 v7, 0x1

    :try_start_2
    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v9, "window"

    aput-object v9, v7, v8

    invoke-virtual {v3, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Landroid/os/IBinder;

    move-object v1, v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4

    .line 616
    :goto_2
    if-eqz v1, :cond_0

    .line 617
    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v4

    .line 620
    .local v4, "iwm":Landroid/view/IWindowManager;
    if-eqz v4, :cond_0

    .line 622
    :try_start_3
    invoke-interface {v4}, Landroid/view/IWindowManager;->dismissKeyguard()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_5

    .line 630
    .end local v1    # "binder":Landroid/os/IBinder;
    .end local v3    # "getService":Ljava/lang/reflect/Method;
    .end local v4    # "iwm":Landroid/view/IWindowManager;
    :cond_0
    :goto_3
    return-void

    .line 591
    :catch_0
    move-exception v2

    .line 592
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 600
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v3    # "getService":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v2

    .line 601
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 608
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v1    # "binder":Landroid/os/IBinder;
    :catch_2
    move-exception v2

    .line 609
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 610
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v2

    .line 611
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 612
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v2

    .line 613
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_2

    .line 623
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v4    # "iwm":Landroid/view/IWindowManager;
    :catch_5
    move-exception v2

    .line 624
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3
.end method

.method public static getInstance()Landroid/speech/tts/TextToSpeech;
    .locals 1

    .prologue
    .line 2356
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 2357
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tts:Landroid/speech/tts/TextToSpeech;

    .line 2358
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getLatestWidget()Lcom/vlingo/midas/gui/Widget;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vlingo/midas/gui/Widget",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2191
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    return-object v0
.end method

.method private initFragments(Z)V
    .locals 11
    .param p1, "driveModeEnable"    # Z

    .prologue
    .line 1607
    const/4 p1, 0x0

    .line 1609
    iget v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mOrientation:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_c

    const/4 v5, 0x1

    .line 1610
    .local v5, "isPortrait":Z
    :goto_0
    iget v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mOrientation:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_d

    const/4 v4, 0x1

    .line 1612
    .local v4, "isLandscape":Z
    :goto_1
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1613
    :cond_0
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    if-nez v8, :cond_1

    .line 1614
    sget v9, Lcom/vlingo/midas/R$layout;->main_normal_ewys_help:I

    sget v8, Lcom/vlingo/midas/R$id;->full_container:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    invoke-static {p0, v9, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1615
    sget v9, Lcom/vlingo/midas/R$layout;->guide_ewys_step_1_port:I

    sget v8, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    invoke-static {p0, v9, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1616
    sget v9, Lcom/vlingo/midas/R$layout;->guide_ewys_step_2_port:I

    sget v8, Lcom/vlingo/midas/R$id;->help_layout_parent:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    invoke-static {p0, v9, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1617
    sget v9, Lcom/vlingo/midas/R$layout;->guide_ewys_step_3_port:I

    sget v8, Lcom/vlingo/midas/R$id;->help_layout_parent:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    invoke-static {p0, v9, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1618
    sget v9, Lcom/vlingo/midas/R$layout;->guide_ewys_step_1_land:I

    sget v8, Lcom/vlingo/midas/R$id;->help_layout_parent:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    invoke-static {p0, v9, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1619
    sget v9, Lcom/vlingo/midas/R$layout;->guide_ewys_step_2_land:I

    sget v8, Lcom/vlingo/midas/R$id;->help_layout_parent:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    invoke-static {p0, v9, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1620
    sget v9, Lcom/vlingo/midas/R$layout;->guide_ewys_step_3_land:I

    sget v8, Lcom/vlingo/midas/R$id;->help_layout_parent:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    invoke-static {p0, v9, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1622
    sget v8, Lcom/vlingo/midas/R$id;->normal_layout:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    .line 1638
    :cond_1
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    if-nez v8, :cond_2

    .line 1639
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    sget v9, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    .line 1641
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v8, v9}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v8

    check-cast v8, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    .line 1643
    if-nez v5, :cond_3

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v8

    if-eqz v8, :cond_11

    .line 1644
    :cond_3
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->currentTablet:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;

    sget-object v9, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;

    if-ne v8, v9, :cond_10

    .line 1645
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    if-nez v8, :cond_4

    .line 1646
    sget v8, Lcom/vlingo/midas/R$layout;->main_normal_control_view_ewys_help:I

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1647
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v8, v9}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v8

    check-cast v8, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    .line 1648
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    invoke-virtual {v8, v9}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->setCallback(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;)V

    .line 1651
    :cond_4
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->help_layout:Landroid/widget/RelativeLayout;

    if-nez v8, :cond_5

    .line 1652
    sget v8, Lcom/vlingo/midas/R$layout;->help_list_view:I

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1653
    .local v2, "helpLayout":Landroid/view/View;
    sget v8, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->help_layout:Landroid/widget/RelativeLayout;

    .line 1656
    .end local v2    # "helpLayout":Landroid/view/View;
    :cond_5
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpContainer:Landroid/view/View;

    if-nez v8, :cond_6

    .line 1657
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->help_layout:Landroid/widget/RelativeLayout;

    sget v9, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpContainer:Landroid/view/View;

    .line 1664
    :cond_6
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    if-nez v8, :cond_7

    .line 1666
    new-instance v8, Lcom/vlingo/midas/gui/HelpFragment;

    invoke-direct {v8}, Lcom/vlingo/midas/gui/HelpFragment;-><init>()V

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    .line 1667
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v6

    .line 1668
    .local v6, "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    sget v8, Lcom/vlingo/midas/R$id;->help_layout:I

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v6, v8, v9}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1669
    invoke-virtual {v6}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1671
    .end local v6    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v8

    sget-object v9, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v8, v9, :cond_f

    .line 1672
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$bool;->help_fragment_supported:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 1673
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v8, 0x439d0000    # 314.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x1

    invoke-direct {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1675
    .local v3, "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xb

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1676
    const/4 v8, 0x1

    sget v9, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1678
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpContainer:Landroid/view/View;

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1680
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    sget v9, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlViewContainer:Landroid/view/View;

    .line 1682
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v8, 0x43f30000    # 486.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x2

    invoke-direct {v0, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1684
    .local v0, "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xc

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1685
    const/16 v8, 0x9

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1686
    const/16 v8, 0xe

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1689
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlViewContainer:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1691
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v8, 0x43f30000    # 486.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x1

    invoke-direct {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1693
    .local v7, "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x2

    sget v9, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1694
    const/16 v8, 0x9

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1695
    const/16 v8, 0xa

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1696
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v8, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1758
    .end local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_8
    :goto_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->initHelpResources()V

    .line 1759
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    .line 1845
    :cond_9
    :goto_3
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getCurrGuideState()Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->AdjustHelpVisibility(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 1847
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlContainer:Landroid/view/View;

    if-nez v8, :cond_a

    .line 1848
    sget v8, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlContainer:Landroid/view/View;

    .line 1850
    :cond_a
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    if-nez v8, :cond_b

    .line 1851
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    .line 1863
    :cond_b
    return-void

    .line 1609
    .end local v4    # "isLandscape":Z
    .end local v5    # "isPortrait":Z
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 1610
    .restart local v5    # "isPortrait":Z
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1699
    .restart local v4    # "isLandscape":Z
    :cond_e
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v6

    .line 1700
    .restart local v6    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v6, v8}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1701
    invoke-virtual {v6}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1702
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    .line 1706
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    sget v9, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlViewContainer:Landroid/view/View;

    .line 1707
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, -0x2

    invoke-direct {v0, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1709
    .restart local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xc

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1711
    const/16 v8, 0xe

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1712
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlViewContainer:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1713
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, -0x1

    invoke-direct {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1715
    .restart local v7    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x2

    sget v9, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1717
    const/16 v8, 0xa

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1718
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v8, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 1721
    .end local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v6    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    .end local v7    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_f
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v8

    sget-object v9, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v8, v9, :cond_8

    .line 1723
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v8, 0x0

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x1

    invoke-direct {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1725
    .restart local v3    # "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xb

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1726
    const/4 v8, 0x1

    sget v9, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1727
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpContainer:Landroid/view/View;

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1729
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    sget v9, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlViewContainer:Landroid/view/View;

    .line 1731
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v8, 0x44160000    # 600.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x2

    invoke-direct {v0, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1733
    .restart local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xc

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1734
    const/16 v8, 0x9

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1735
    const/16 v8, 0xe

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1736
    const/4 v8, 0x0

    sget v9, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v0, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1737
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlViewContainer:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1739
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v8, 0x44160000    # 600.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x1

    invoke-direct {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1741
    .restart local v7    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x2

    sget v9, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1742
    const/16 v8, 0x9

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1743
    const/16 v8, 0xa

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1744
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v8, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 1746
    .end local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_10
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    if-nez v8, :cond_8

    .line 1747
    sget v8, Lcom/vlingo/midas/R$layout;->main_normal_control_view_ewys_help:I

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1748
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v8, v9}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v8

    check-cast v8, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    .line 1750
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    invoke-virtual {v8, v9}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->setCallback(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;)V

    .line 1752
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, -0x1

    invoke-direct {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1754
    .restart local v7    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x2

    sget v9, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1755
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v8, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 1761
    .end local v7    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_11
    if-eqz v4, :cond_9

    .line 1763
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->currentTablet:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;

    sget-object v9, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;

    if-ne v8, v9, :cond_18

    .line 1764
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    if-nez v8, :cond_12

    .line 1765
    sget v8, Lcom/vlingo/midas/R$layout;->main_land_control_view_ewys_help:I

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1766
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v8, v9}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v8

    check-cast v8, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    .line 1767
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    invoke-virtual {v8, v9}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->setCallback(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;)V

    .line 1770
    :cond_12
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->help_layout:Landroid/widget/RelativeLayout;

    if-nez v8, :cond_13

    .line 1771
    sget v8, Lcom/vlingo/midas/R$layout;->help_list_view:I

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1772
    sget v8, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->help_layout:Landroid/widget/RelativeLayout;

    .line 1774
    :cond_13
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpContainer:Landroid/view/View;

    if-nez v8, :cond_14

    .line 1775
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->help_layout:Landroid/widget/RelativeLayout;

    sget v9, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpContainer:Landroid/view/View;

    .line 1777
    :cond_14
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    if-nez v8, :cond_15

    .line 1779
    new-instance v8, Lcom/vlingo/midas/gui/HelpFragment;

    invoke-direct {v8}, Lcom/vlingo/midas/gui/HelpFragment;-><init>()V

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    .line 1780
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v6

    .line 1781
    .restart local v6    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    sget v8, Lcom/vlingo/midas/R$id;->help_layout:I

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v6, v8, v9}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1782
    invoke-virtual {v6}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1784
    .end local v6    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_15
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v8

    sget-object v9, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v8, v9, :cond_17

    .line 1786
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v8, 0x43b20000    # 356.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x1

    invoke-direct {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1788
    .restart local v3    # "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xb

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1789
    const/4 v8, 0x1

    sget v9, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1791
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpContainer:Landroid/view/View;

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1793
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    sget v9, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlViewContainer:Landroid/view/View;

    .line 1795
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v8, 0x44670000    # 924.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x2

    invoke-direct {v0, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1797
    .restart local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xc

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1798
    const/16 v8, 0xe

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1799
    const/16 v8, 0x9

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1802
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlViewContainer:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1804
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v8, 0x44670000    # 924.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x1

    invoke-direct {v1, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1806
    .local v1, "dialogParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x2

    sget v9, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v1, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1807
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v8, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1841
    .end local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "dialogParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_16
    :goto_4
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->initHelpResources()V

    .line 1842
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    goto/16 :goto_3

    .line 1809
    :cond_17
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v8

    sget-object v9, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v8, v9, :cond_16

    .line 1811
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const v8, 0x43ac8000    # 345.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x1

    invoke-direct {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1813
    .restart local v3    # "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xb

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1814
    const/4 v8, 0x1

    sget v9, Lcom/vlingo/midas/R$id;->dialog_view:I

    invoke-virtual {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1815
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpContainer:Landroid/view/View;

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1817
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    sget v9, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlViewContainer:Landroid/view/View;

    .line 1819
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const v8, 0x4419c000    # 615.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x2

    invoke-direct {v0, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1821
    .restart local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v8, 0xc

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1822
    const/16 v8, 0xe

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1823
    const/16 v8, 0x9

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1824
    const/4 v8, 0x0

    sget v9, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v0, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1825
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlViewContainer:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1827
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const v8, 0x4419c000    # 615.0f

    iget v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    const/high16 v10, 0x43200000    # 160.0f

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/4 v9, -0x1

    invoke-direct {v1, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1829
    .restart local v1    # "dialogParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x2

    sget v9, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v1, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1830
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    invoke-virtual {v8, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_4

    .line 1833
    .end local v0    # "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "dialogParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_18
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    if-nez v8, :cond_16

    .line 1834
    sget v8, Lcom/vlingo/midas/R$layout;->main_land_control_view_ewys_help:I

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1835
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v8, v9}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v8

    check-cast v8, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    iput-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    .line 1837
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    iget-object v9, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    invoke-virtual {v8, v9}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->setCallback(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;)V

    goto/16 :goto_4
.end method

.method public static setLatestWidget(Lcom/vlingo/midas/gui/Widget;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/midas/gui/Widget",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2187
    .local p0, "latestWidget":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<*>;"
    sput-object p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    .line 2188
    return-void
.end method

.method public static shouldReportError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;)Z
    .locals 2
    .param p0, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    .prologue
    const/4 v0, 0x0

    .line 2125
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_SPEECH_TIMEOUT:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    if-ne v1, p0, :cond_1

    .line 2131
    :cond_0
    :goto_0
    return v0

    .line 2128
    :cond_1
    sget-object v1, Lcom/vlingo/sdk/recognition/VLRecognitionErrors;->ERROR_NO_MATCH:Lcom/vlingo/sdk/recognition/VLRecognitionErrors;

    if-eq v1, p0, :cond_0

    .line 2131
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startSettingsActivity()V
    .locals 2

    .prologue
    .line 2344
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2345
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startActivity(Landroid/content/Intent;)V

    .line 2346
    return-void
.end method

.method private unpackSavedInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 699
    if-eqz p1, :cond_0

    .line 700
    const-string/jumbo v0, "startedWithCarDock"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startedWithCarDock:Z

    .line 702
    const-string/jumbo v0, "autoStartOnResume"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z

    .line 705
    :cond_0
    return-void
.end method


# virtual methods
.method public AdjustHelpPosition(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V
    .locals 12
    .param p1, "guideState"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    .prologue
    const/4 v1, 0x1

    .line 1387
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    if-ne v8, v1, :cond_0

    .line 1388
    .local v1, "isPortrait":Z
    :goto_0
    if-eqz v1, :cond_3

    .line 1389
    sget-object v8, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 1476
    :goto_1
    :pswitch_0
    return-void

    .line 1387
    .end local v1    # "isPortrait":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1392
    .restart local v1    # "isPortrait":Z
    :pswitch_1
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    sget v9, Lcom/vlingo/midas/R$id;->dialog_bubble_vlingo_s:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1393
    .local v7, "v":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1394
    .local v0, "bottom":I
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    sget v9, Lcom/vlingo/midas/R$id;->prompt_user_textview:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1396
    new-instance v8, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$10;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$10;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1406
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1407
    .local v5, "r":Landroid/graphics/Rect;
    invoke-virtual {v7, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1408
    invoke-virtual {v7}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    .line 1409
    .local v4, "paddingTop":I
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    sget v9, Lcom/vlingo/midas/R$id;->dialog_bubble_user_s:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1411
    sget-object v8, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIv:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1414
    .local v2, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1415
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    sget-object v9, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIv:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getHeight()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v6, v8, 0x4

    .line 1416
    .local v6, "topMargin":I
    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int v9, v0, v6

    iget v10, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v11, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1422
    :goto_2
    sget-object v8, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIv:Landroid/widget/ImageView;

    invoke-virtual {v8, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 1419
    .end local v6    # "topMargin":I
    :cond_1
    iget v6, v5, Landroid/graphics/Rect;->top:I

    .line 1420
    .restart local v6    # "topMargin":I
    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    div-int/lit8 v9, v4, 0x2

    sub-int v9, v6, v9

    iget v10, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v11, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_2

    .line 1425
    .end local v0    # "bottom":I
    .end local v2    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "paddingTop":I
    .end local v5    # "r":Landroid/graphics/Rect;
    .end local v6    # "topMargin":I
    .end local v7    # "v":Landroid/view/View;
    :pswitch_2
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1431
    .local v3, "lp2":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    sget v9, Lcom/vlingo/midas/R$id;->prompt_user_textview:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1432
    .restart local v7    # "v":Landroid/view/View;
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1433
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1434
    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v11, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mKeypadHeight:I

    invoke-virtual {v3, v8, v9, v10, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1437
    :goto_3
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 1436
    :cond_2
    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v11, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mKeypadHeight:I

    add-int/lit8 v11, v11, 0x69

    invoke-virtual {v3, v8, v9, v10, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_3

    .line 1444
    .end local v3    # "lp2":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v7    # "v":Landroid/view/View;
    :cond_3
    sget-object v8, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    goto/16 :goto_1

    .line 1446
    :pswitch_3
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    sget v9, Lcom/vlingo/midas/R$id;->dialog_bubble_vlingo_s:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1447
    .restart local v7    # "v":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1448
    .restart local v0    # "bottom":I
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    sget v9, Lcom/vlingo/midas/R$id;->dialog_bubble_user_s:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1451
    sget-object v8, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIvLand:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1454
    .restart local v2    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1455
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    sget-object v9, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIvLand:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getHeight()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v6, v8, 0x4

    .line 1458
    .restart local v6    # "topMargin":I
    :goto_4
    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int v9, v0, v6

    iget v10, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v11, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1459
    sget-object v8, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIvLand:Landroid/widget/ImageView;

    invoke-virtual {v8, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 1457
    .end local v6    # "topMargin":I
    :cond_4
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    sget-object v9, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIvLand:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getHeight()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v6, v8, 0x2

    .restart local v6    # "topMargin":I
    goto :goto_4

    .line 1462
    .end local v0    # "bottom":I
    .end local v2    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v6    # "topMargin":I
    .end local v7    # "v":Landroid/view/View;
    :pswitch_4
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1468
    .restart local v3    # "lp2":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v11, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mKeypadHeight:I

    invoke-virtual {v3, v8, v9, v10, v11}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1469
    iget-object v8, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 1389
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    .line 1444
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public AdjustHelpVisibility(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V
    .locals 5
    .param p1, "guideState"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x4

    .line 1479
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_0

    .line 1481
    .local v0, "isPortrait":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 1482
    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1485
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1486
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1487
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1489
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1490
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1491
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1566
    :goto_1
    return-void

    .end local v0    # "isPortrait":Z
    :cond_0
    move v0, v1

    .line 1479
    goto :goto_0

    .line 1494
    .restart local v0    # "isPortrait":Z
    :pswitch_0
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1495
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1496
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1498
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1499
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1500
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    .line 1503
    :pswitch_1
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1504
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1505
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1507
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1508
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1509
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    .line 1512
    :pswitch_2
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1513
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1514
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1516
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1517
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1518
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    .line 1524
    :cond_1
    sget-object v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 1527
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1528
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1529
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1531
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1532
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1533
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1536
    :pswitch_3
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1537
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1538
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1540
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1541
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1542
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1545
    :pswitch_4
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1546
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1547
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1549
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1550
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1551
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1554
    :pswitch_5
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1555
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1556
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1558
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1559
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1560
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 1482
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1524
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public StartHelpAnimation(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V
    .locals 5
    .param p1, "guideState"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    .prologue
    const/4 v2, 0x1

    .line 1313
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v2, :cond_0

    .line 1314
    .local v2, "isPortrait":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 1315
    sget-object v3, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1375
    :goto_1
    return-void

    .line 1313
    .end local v2    # "isPortrait":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1317
    .restart local v2    # "isPortrait":Z
    :pswitch_0
    sget-object v3, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIv:Landroid/widget/ImageView;

    sget-object v4, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mBlinkAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1319
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v0, v3

    .line 1320
    .local v0, "centerX":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v1, v3

    .line 1321
    .local v1, "centerY":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setPivotX(F)V

    .line 1322
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setPivotY(F)V

    .line 1324
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mScaleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 1327
    .end local v0    # "centerX":F
    .end local v1    # "centerY":F
    :pswitch_1
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v0, v3

    .line 1328
    .restart local v0    # "centerX":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v1, v3

    .line 1329
    .restart local v1    # "centerY":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setPivotX(F)V

    .line 1330
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setPivotY(F)V

    .line 1332
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mScaleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 1335
    .end local v0    # "centerX":F
    .end local v1    # "centerY":F
    :pswitch_2
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v0, v3

    .line 1336
    .restart local v0    # "centerX":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v1, v3

    .line 1337
    .restart local v1    # "centerY":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setPivotX(F)V

    .line 1338
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setPivotY(F)V

    .line 1340
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mScaleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_1

    .line 1345
    .end local v0    # "centerX":F
    .end local v1    # "centerY":F
    :cond_1
    sget-object v3, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    goto/16 :goto_1

    .line 1347
    :pswitch_3
    sget-object v3, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIvLand:Landroid/widget/ImageView;

    sget-object v4, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mBlinkAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1349
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v0, v3

    .line 1350
    .restart local v0    # "centerX":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v1, v3

    .line 1351
    .restart local v1    # "centerY":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setPivotX(F)V

    .line 1352
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setPivotY(F)V

    .line 1354
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayoutLand:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mScaleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_1

    .line 1357
    .end local v0    # "centerX":F
    .end local v1    # "centerY":F
    :pswitch_4
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v0, v3

    .line 1358
    .restart local v0    # "centerX":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v1, v3

    .line 1359
    .restart local v1    # "centerY":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setPivotX(F)V

    .line 1360
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setPivotY(F)V

    .line 1362
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mScaleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_1

    .line 1365
    .end local v0    # "centerX":F
    .end local v1    # "centerY":F
    :pswitch_5
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v0, v3

    .line 1366
    .restart local v0    # "centerX":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    int-to-float v1, v3

    .line 1367
    .restart local v1    # "centerY":F
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setPivotX(F)V

    .line 1368
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setPivotY(F)V

    .line 1370
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mScaleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_1

    .line 1315
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1345
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public addBlueBubble()V
    .locals 3

    .prologue
    .line 1922
    const-string/jumbo v1, "car_word_spotter_enabled"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1925
    .local v0, "spottingEnabled":Z
    if-nez v0, :cond_0

    .line 1928
    :cond_0
    return-void
.end method

.method public addHelpChoiceWidget()V
    .locals 5

    .prologue
    .line 1905
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 1906
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->addHelpChoiceWidget()V

    .line 1908
    iget v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mPhraseSpottedDone:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1909
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->recognized_wakeup:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    .line 1913
    :cond_0
    return-void
.end method

.method public addHelpWidget(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1916
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 1917
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ContentFragment;->addHelpWidget(Landroid/content/Intent;)V

    .line 1918
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->addBlueBubble()V

    .line 1919
    return-void
.end method

.method public addWidget(Lcom/vlingo/midas/gui/Widget;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/midas/gui/Widget",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2175
    .local p1, "widget":Lcom/vlingo/midas/gui/Widget;, "Lcom/vlingo/midas/gui/Widget<TT;>;"
    invoke-static {p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setLatestWidget(Lcom/vlingo/midas/gui/Widget;)V

    .line 2176
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ContentFragment;->addWidget(Landroid/view/View;)V

    .line 2177
    return-void
.end method

.method afterViews()V
    .locals 2

    .prologue
    .line 659
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogContainer:Landroid/view/View;

    invoke-static {}, Lcom/vlingo/midas/gui/AnimationUtils;->inFromTopAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 662
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlContainer:Landroid/view/View;

    invoke-static {}, Lcom/vlingo/midas/gui/AnimationUtils;->inFromBottomAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 665
    :cond_0
    return-void
.end method

.method public changeToDriveMode(Z)V
    .locals 3
    .param p1, "driveModeEnable"    # Z

    .prologue
    .line 1867
    const/4 p1, 0x0

    .line 1868
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->initFragments(Z)V

    .line 1869
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 1871
    .local v0, "fragmentTransition":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->invalidateOptionsMenu()V

    .line 1872
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1874
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    iput-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    .line 1875
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->normal_layout:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1877
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1879
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    if-eqz v1, :cond_2

    .line 1880
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1882
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1883
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    iput-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    .line 1891
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 1892
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1900
    :cond_4
    invoke-direct {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->checkFragmentLanguage()V

    .line 1902
    return-void

    .line 1884
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 1885
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    if-eqz v1, :cond_6

    .line 1886
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1888
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1889
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    iput-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    goto :goto_0
.end method

.method public dismissKeypad()V
    .locals 2

    .prologue
    .line 2397
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    if-eqz v0, :cond_0

    .line 2398
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->removeMessages(I)V

    .line 2400
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1242
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->gestures:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    .line 1243
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->gestures:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1244
    const/4 v0, 0x1

    .line 1247
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public enteredDomain(Lcom/vlingo/core/internal/domain/DomainName;)V
    .locals 0
    .param p1, "domain"    # Lcom/vlingo/core/internal/domain/DomainName;

    .prologue
    .line 2418
    return-void
.end method

.method public getCurrGuideState()Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;
    .locals 1

    .prologue
    .line 2330
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mGuideState:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    return-object v0
.end method

.method public getGrammarContext()Lcom/vlingo/core/internal/dialogmanager/grammar/GrammarContext;
    .locals 1

    .prologue
    .line 2388
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleTextRequestIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 2207
    iput-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->handleTextReqeust:Z

    .line 2208
    if-eqz p1, :cond_0

    .line 2209
    const-string/jumbo v0, "PRESENT_EDITOR"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->present_editor:Z

    .line 2210
    const-string/jumbo v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->textRequest:Ljava/lang/String;

    .line 2212
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->textRequest:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->present_editor:Z

    if-nez v0, :cond_1

    .line 2213
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isIUXComplete()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2215
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getFieldID()Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFieldID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "dm_main"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2216
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->textRequest:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->startUserFlow(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2219
    :cond_1
    return-void
.end method

.method public initFlow()V
    .locals 1

    .prologue
    .line 752
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 753
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->releaseFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;)V

    .line 755
    return-void
.end method

.method public initHelpResources()V
    .locals 2

    .prologue
    .line 1570
    sget v0, Lcom/vlingo/midas/R$id;->help_edit_tap_iv_port:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIv:Landroid/widget/ImageView;

    .line 1572
    sget v0, Lcom/vlingo/midas/R$id;->help_edit_layout_port:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayout:Landroid/widget/RelativeLayout;

    .line 1573
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayout:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->help_edit_bubble_tv_port:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleTv:Landroid/widget/TextView;

    .line 1574
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleTv:Landroid/widget/TextView;

    sget v1, Lcom/vlingo/midas/R$string;->help_tooltip_edit:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1575
    sget v0, Lcom/vlingo/midas/R$id;->help_edit_bubble_layout_port:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayout:Landroid/widget/RelativeLayout;

    .line 1577
    sget v0, Lcom/vlingo/midas/R$id;->help_keypad_layout_port:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    .line 1578
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->help_keypad_bubble_tv_port:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadBubbleTv:Landroid/widget/TextView;

    .line 1579
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadBubbleTv:Landroid/widget/TextView;

    sget v1, Lcom/vlingo/midas/R$string;->help_tooltip_keypad:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1580
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayout:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->help_keypad_tip_iv_port:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadTipIv:Landroid/widget/ImageView;

    .line 1581
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadTipIv:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->bringToFront()V

    .line 1583
    sget v0, Lcom/vlingo/midas/R$id;->help_done_layout_port:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    .line 1584
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayout:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->help_done_bubble_tv_port:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneBubbleTv:Landroid/widget/TextView;

    .line 1585
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneBubbleTv:Landroid/widget/TextView;

    sget v1, Lcom/vlingo/midas/R$string;->recognized_wakeup:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1588
    sget v0, Lcom/vlingo/midas/R$id;->help_edit_tap_iv_land:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditTapIvLand:Landroid/widget/ImageView;

    .line 1589
    sget v0, Lcom/vlingo/midas/R$id;->help_edit_layout_land:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

    .line 1590
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditLayoutLand:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->help_edit_bubble_tv_land:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleTvLand:Landroid/widget/TextView;

    .line 1591
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleTvLand:Landroid/widget/TextView;

    sget v1, Lcom/vlingo/midas/R$string;->help_tooltip_edit:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1592
    sget v0, Lcom/vlingo/midas/R$id;->help_edit_bubble_layout_land:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpEditBubbleLayoutLand:Landroid/widget/RelativeLayout;

    .line 1594
    sget v0, Lcom/vlingo/midas/R$id;->help_keypad_layout_land:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    .line 1595
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->help_keypad_bubble_tv_land:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadBubbleTvLand:Landroid/widget/TextView;

    .line 1596
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadBubbleTvLand:Landroid/widget/TextView;

    sget v1, Lcom/vlingo/midas/R$string;->help_tooltip_keypad:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1597
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadLayoutLand:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->help_keypad_tip_iv_land:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadTipIvLand:Landroid/widget/ImageView;

    .line 1598
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpKeypadTipIvLand:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->bringToFront()V

    .line 1600
    sget v0, Lcom/vlingo/midas/R$id;->help_done_layout_land:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    .line 1601
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneLayoutLand:Landroid/widget/RelativeLayout;

    sget v1, Lcom/vlingo/midas/R$id;->help_done_bubble_tv_land:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneBubbleTvLand:Landroid/widget/TextView;

    .line 1602
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHelpDoneBubbleTvLand:Landroid/widget/TextView;

    sget v1, Lcom/vlingo/midas/R$string;->recognized_wakeup:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1603
    return-void
.end method

.method public isDialogMode()Z
    .locals 2

    .prologue
    .line 692
    sget v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTheme:I

    sget v1, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    if-eq v0, v1, :cond_0

    .line 693
    const/4 v0, 0x1

    .line 695
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPassedAllInitSteps()Z
    .locals 1

    .prologue
    .line 2276
    const/4 v0, 0x1

    return v0
.end method

.method public isStartedForCustomWakeUpSetting()Z
    .locals 1

    .prologue
    .line 2199
    sget-boolean v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mIsStartedCustomWakeUpSetting:Z

    return v0
.end method

.method public is_Device()Z
    .locals 3

    .prologue
    const/16 v2, 0x500

    const/16 v1, 0x2d0

    .line 1378
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v0, v2, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v1, 0x140

    if-ne v0, v1, :cond_2

    .line 1381
    const/4 v0, 0x1

    .line 1383
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onASRRecorderClosed()V
    .locals 0

    .prologue
    .line 2383
    return-void
.end method

.method public onASRRecorderOpened()V
    .locals 0

    .prologue
    .line 2377
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 0
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 252
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 1225
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/midas/ui/VLActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1227
    if-ne p1, v1, :cond_1

    .line 1228
    const/4 v3, -0x1

    if-ne p2, v3, :cond_2

    .line 1229
    .local v1, "ok":Z
    :goto_0
    const/4 v2, 0x0

    .line 1230
    .local v2, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1231
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 1232
    .local v0, "dataUri":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1235
    .end local v0    # "dataUri":Landroid/net/Uri;
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mFilePickerResultListener:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$FilePickerResultListener;

    invoke-interface {v3, v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$FilePickerResultListener;->onResult(ZLjava/lang/String;)V

    .line 1237
    .end local v1    # "ok":Z
    .end local v2    # "path":Ljava/lang/String;
    :cond_1
    return-void

    .line 1228
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1156
    sget-object v0, Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;->NORMAL:Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;

    invoke-static {v0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->appCloseType(Lcom/vlingo/core/internal/bluetooth/BluetoothManager$CloseType;)V

    .line 1157
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onBackPressed()V

    .line 1159
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 1164
    return-void
.end method

.method public onBluetoothServiceConnected()V
    .locals 0

    .prologue
    .line 2341
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 714
    if-eqz p1, :cond_0

    .line 715
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 718
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mOrientation:I

    .line 720
    sget v1, Lcom/vlingo/midas/R$id;->help_layout_parent:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 721
    .local v0, "root":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mWindowHeight:I

    .line 723
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setOrientationDrivingMode()V

    .line 724
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 29
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 299
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->screenDensityDPI:F

    .line 300
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCoverOpened()Z

    move-result v25

    if-nez v25, :cond_0

    .line 301
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->finish()V

    .line 303
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    .line 305
    .local v8, "editor":Landroid/content/SharedPreferences$Editor;
    const/16 v20, 0x0

    .line 306
    .local v20, "tablet":Z
    new-instance v15, Landroid/util/DisplayMetrics;

    invoke-direct {v15}, Landroid/util/DisplayMetrics;-><init>()V

    .line 307
    .local v15, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 308
    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/audio/TTSCache;->purgeCache(Landroid/content/Context;Z)I

    .line 311
    iget v0, v15, Landroid/util/DisplayMetrics;->density:F

    move/from16 v25, v0

    move/from16 v0, v25

    float-to-double v0, v0

    move-wide/from16 v25, v0

    const-wide/high16 v27, 0x3ff0000000000000L    # 1.0

    cmpl-double v25, v25, v27

    if-nez v25, :cond_1

    iget v0, v15, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v25, v0

    iget v0, v15, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v26, v0

    add-int v25, v25, v26

    const/16 v26, 0x7f0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1

    iget v0, v15, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v25, v0

    const v26, 0x4315d32c

    sub-float v25, v25, v26

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-nez v25, :cond_1

    iget v0, v15, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v25, v0

    const v26, 0x431684be

    sub-float v25, v25, v26

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-eqz v25, :cond_2

    :cond_1
    iget v0, v15, Landroid/util/DisplayMetrics;->density:F

    move/from16 v25, v0

    move/from16 v0, v25

    float-to-double v0, v0

    move-wide/from16 v25, v0

    const-wide/high16 v27, 0x3ff0000000000000L    # 1.0

    cmpl-double v25, v25, v27

    if-nez v25, :cond_10

    iget v0, v15, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v25, v0

    iget v0, v15, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v26, v0

    add-int v25, v25, v26

    const/16 v26, 0x820

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_10

    iget v0, v15, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v25, v0

    const v26, 0x4315d2f2

    sub-float v25, v25, v26

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-nez v25, :cond_10

    iget v0, v15, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v25, v0

    const v26, 0x4316849c

    sub-float v25, v25, v26

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-nez v25, :cond_10

    .line 315
    :cond_2
    sget v25, Lcom/vlingo/midas/R$style;->DialogSlideAnim:I

    sput v25, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTheme:I

    .line 316
    const/16 v20, 0x1

    .line 328
    :goto_0
    invoke-super/range {p0 .. p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 330
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mOrientation:I

    .line 334
    new-instance v25, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$1;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$1;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    invoke-static/range {v25 .. v25}, Lcom/vlingo/midas/ui/HomeKeyListener;->onHomePressed(Ljava/lang/Runnable;)Lcom/vlingo/midas/ui/HomeKeyListener;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    .line 341
    const/16 v25, 0x0

    sput-boolean v25, Lcom/vlingo/midas/settings/debug/DebugSettings;->SHOW_DEBUG:Z

    .line 345
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    .line 347
    .local v10, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-super {v0, v10}, Lcom/vlingo/midas/ui/VLActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 349
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->handleTextReqeust:Z

    .line 352
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerInit()Z

    .line 353
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->addListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 356
    const/16 v25, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->requestWindowFeature(I)Z

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->customTitleSupported:Z

    .line 359
    sget v25, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTheme:I

    sget v26, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 360
    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->requestWindowFeature(I)Z

    .line 361
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getWindow()Landroid/view/Window;

    move-result-object v25

    const v26, 0x1000400

    invoke-virtual/range {v25 .. v26}, Landroid/view/Window;->addFlags(I)V

    .line 364
    :cond_3
    sget v25, Lcom/vlingo/midas/R$layout;->main:I

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setContentView(I)V

    .line 366
    sget v25, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTheme:I

    sget v26, Lcom/vlingo/midas/R$style;->actionBarStyle:I

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_4

    .line 367
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v21

    .line 368
    .local v21, "titlebar":Landroid/app/ActionBar;
    sget v25, Lcom/vlingo/midas/R$string;->wcis_edit_what_you_said:I

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 369
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v25

    if-eqz v25, :cond_13

    .line 370
    if-eqz v21, :cond_4

    .line 371
    const/16 v25, 0x8

    move-object/from16 v0, v21

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 381
    .end local v21    # "titlebar":Landroid/app/ActionBar;
    :cond_4
    :goto_1
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getWindow()Landroid/view/Window;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v12

    .line 382
    .local v12, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v25

    const-string/jumbo v26, "privateFlags"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v16

    .line 383
    .local v16, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v25

    const-string/jumbo v26, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v19

    .line 385
    .local v19, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v25

    const-string/jumbo v26, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 388
    .local v6, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v5, 0x0

    .local v5, "currentPrivateFlags":I
    const/16 v24, 0x0

    .local v24, "valueofFlagsEnableStatusBar":I
    const/16 v23, 0x0

    .line 389
    .local v23, "valueofFlagsDisableTray":I
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    .line 390
    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v24

    .line 391
    invoke-virtual {v6, v12}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v23

    .line 393
    or-int v5, v5, v24

    .line 394
    or-int v5, v5, v23

    .line 396
    move-object/from16 v0, v16

    invoke-virtual {v0, v12, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 397
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getWindow()Landroid/view/Window;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    .end local v5    # "currentPrivateFlags":I
    .end local v6    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v12    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v16    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v19    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v23    # "valueofFlagsDisableTray":I
    .end local v24    # "valueofFlagsEnableStatusBar":I
    :goto_2
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v25

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->initFragments(Z)V

    .line 405
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/view/Display;->getRotation()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/view/Display;->getRotation()I

    move-result v25

    const/16 v26, 0x3

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_6

    .line 409
    :cond_5
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 412
    :cond_6
    const-string/jumbo v25, "key_popup_window_opened"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-interface {v8, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 414
    new-instance v25, Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-direct/range {v25 .. v25}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    .line 415
    new-instance v25, Lcom/vlingo/midas/util/log/NLUEventLogUtil;

    invoke-direct/range {v25 .. v25}, Lcom/vlingo/midas/util/log/NLUEventLogUtil;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->nluEventLogUtil:Lcom/vlingo/midas/util/log/NLUEventLogUtil;

    .line 416
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startRecoBySCO:Z

    .line 417
    if-eqz v10, :cond_9

    .line 418
    const-string/jumbo v25, "isSecure"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    .line 423
    .local v11, "isSecure":Z
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isIUXComplete()Z

    move-result v25

    if-eqz v25, :cond_8

    .line 424
    invoke-virtual {v10}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v25

    if-eqz v25, :cond_8

    const-string/jumbo v25, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {v10}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    const-string/jumbo v25, "isThisComeFromHomeKeyDoubleClickConcept"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v25

    if-eqz v25, :cond_8

    :cond_7
    const-string/jumbo v25, "com.sec.action.SVOICE"

    invoke-virtual {v10}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_8

    const-string/jumbo v25, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {v10}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 434
    :cond_8
    if-nez v11, :cond_9

    .line 438
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->doDismissKeyguard()V

    .line 443
    .end local v11    # "isSecure":Z
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->enabled(Landroid/content/SharedPreferences$Editor;)V

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->nluEventLogUtil:Lcom/vlingo/midas/util/log/NLUEventLogUtil;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Lcom/vlingo/midas/util/log/NLUEventLogUtil;->enabled(Landroid/content/SharedPreferences$Editor;)V

    .line 446
    new-instance v18, Landroid/content/IntentFilter;

    const-string/jumbo v25, "android.intent.action.SCREEN_OFF"

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 447
    .local v18, "screenoff":Landroid/content/IntentFilter;
    const-string/jumbo v25, "com.samsung.cover.OPEN"

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v25, v0

    new-instance v26, Landroid/content/IntentFilter;

    const-string/jumbo v27, "com.vlingo.midas.gui.widgets.HelpChoiceWidget"

    invoke-direct/range {v26 .. v27}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 451
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 453
    .local v9, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v25, "android.intent.action.TIME_TICK"

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 454
    const-string/jumbo v25, "android.intent.action.TIME_SET"

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 455
    const-string/jumbo v25, "android.intent.action.TIMEZONE_CHANGED"

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 458
    const-string/jumbo v25, "barge_in_enabled"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-interface {v8, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 461
    const/4 v14, 0x0

    .line 462
    .local v14, "mLanguage":Ljava/lang/Process;
    const/4 v13, 0x0

    .line 465
    .local v13, "mCountry":Ljava/lang/Process;
    :try_start_1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v25

    const-string/jumbo v26, "getprop persist.sys.language"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v14

    .line 467
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v25

    const-string/jumbo v26, "getprop persist.sys.country"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v13

    .line 468
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v25, Ljava/io/InputStreamReader;

    invoke-virtual {v14}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v25

    invoke-direct {v4, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 470
    .local v4, "bLangueage":Ljava/io/BufferedReader;
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v25, Ljava/io/InputStreamReader;

    invoke-virtual {v13}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 473
    .local v3, "bCountry":Ljava/io/BufferedReader;
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 474
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 479
    .end local v3    # "bCountry":Ljava/io/BufferedReader;
    .end local v4    # "bLangueage":Ljava/io/BufferedReader;
    :goto_3
    sget-boolean v25, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mIsStartedCustomWakeUpSetting:Z

    if-nez v25, :cond_b

    .line 480
    invoke-static {}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->isTOSRequired()Z

    move-result v25

    if-nez v25, :cond_b

    .line 481
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/vlingo/midas/ServiceManager;->startLocalServices()Z

    .line 482
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->requiresIUX()Z

    move-result v25

    if-nez v25, :cond_a

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isIUXComplete()Z

    move-result v25

    if-nez v25, :cond_b

    .line 485
    :cond_a
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->doneTosAndIux:Z

    .line 491
    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->configureAutoStart(Landroid/content/Intent;)V

    .line 493
    const/16 v25, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setVolumeControlStream(I)V

    .line 495
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->addListener(Lcom/vlingo/core/internal/audio/MicAnimationListener;)V

    .line 497
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isAppCarModeEnabled()Z

    move-result v25

    if-nez v25, :cond_d

    .line 498
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v25

    if-eqz v25, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    const-string/jumbo v26, "supported_bvra"

    const/16 v27, 0x0

    invoke-static/range {v25 .. v27}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v25

    if-nez v25, :cond_c

    .line 501
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v25

    sget v26, Lcom/vlingo/midas/R$string;->bvra_unsupported_toast:I

    const/16 v27, 0x1

    invoke-static/range {v25 .. v27}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v22

    .line 503
    .local v22, "toast":Landroid/widget/Toast;
    invoke-virtual/range {v22 .. v22}, Landroid/widget/Toast;->show()V

    .line 505
    .end local v22    # "toast":Landroid/widget/Toast;
    :cond_c
    const-string/jumbo v25, "widget_display_max"

    const/16 v26, 0x5

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-interface {v8, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 508
    :cond_d
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v25

    if-nez v25, :cond_e

    .line 509
    sget-object v25, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;->LISTEN:Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;

    const-class v26, Lcom/vlingo/core/internal/dialogmanager/vvs/handlers/ListenHandler;

    invoke-static/range {v25 .. v26}, Lcom/vlingo/core/internal/dialogmanager/vvs/VVSDispatcher;->registerHandler(Lcom/vlingo/core/internal/dialogmanager/vvs/VVSActionKey;Ljava/lang/Class;)V

    .line 512
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    move-object/from16 v25, v0

    sget-object v26, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual/range {v25 .. v26}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    const-wide/16 v27, 0x5dc

    invoke-virtual/range {v25 .. v28}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 515
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    sget v26, Lcom/vlingo/midas/R$dimen;->keypad_detect_value_port:I

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->KEYPAD_DETECT_VALUE_PORT:I

    .line 516
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    sget v26, Lcom/vlingo/midas/R$dimen;->keypad_detect_value_land:I

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->KEYPAD_DETECT_VALUE_LAND:I

    .line 518
    sget v25, Lcom/vlingo/midas/R$id;->help_layout_parent:I

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    .line 519
    .local v17, "root":Landroid/view/View;
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getHeight()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mWindowHeight:I

    .line 520
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v25

    new-instance v26, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$2;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;Landroid/view/View;)V

    invoke-virtual/range {v25 .. v26}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 570
    const-string/jumbo v25, "input_method"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/view/inputmethod/InputMethodManager;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mInputMethodService:Landroid/view/inputmethod/InputMethodManager;

    .line 573
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v25

    if-eqz v25, :cond_f

    .line 574
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setRequestedOrientation(I)V

    .line 577
    :cond_f
    invoke-static {v8}, Lcom/vlingo/midas/settings/MidasSettings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 578
    return-void

    .line 321
    .end local v9    # "filter":Landroid/content/IntentFilter;
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v13    # "mCountry":Ljava/lang/Process;
    .end local v14    # "mLanguage":Ljava/lang/Process;
    .end local v17    # "root":Landroid/view/View;
    .end local v18    # "screenoff":Landroid/content/IntentFilter;
    :cond_10
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v25

    sget-object v26, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_11

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v25

    sget-object v26, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_12

    .line 322
    :cond_11
    sget-object v25, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;->SANTOS_10:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->currentTablet:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TabletType;

    .line 323
    sget v25, Lcom/vlingo/midas/R$style;->actionBarStyle:I

    sput v25, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTheme:I

    .line 324
    sget v25, Lcom/vlingo/midas/R$style;->actionBarStyle:I

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setTheme(I)V

    goto/16 :goto_0

    .line 326
    :cond_12
    sget v25, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v25, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTheme:I

    goto/16 :goto_0

    .line 374
    .restart local v10    # "intent":Landroid/content/Intent;
    .restart local v21    # "titlebar":Landroid/app/ActionBar;
    :cond_13
    if-eqz v21, :cond_4

    .line 375
    const/16 v25, 0xe

    move-object/from16 v0, v21

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_1

    .line 399
    .end local v21    # "titlebar":Landroid/app/ActionBar;
    :catch_0
    move-exception v7

    .line 400
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 476
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v9    # "filter":Landroid/content/IntentFilter;
    .restart local v13    # "mCountry":Ljava/lang/Process;
    .restart local v14    # "mLanguage":Ljava/lang/Process;
    .restart local v18    # "screenoff":Landroid/content/IntentFilter;
    :catch_1
    move-exception v25

    goto/16 :goto_3

    .line 398
    .end local v9    # "filter":Landroid/content/IntentFilter;
    .end local v13    # "mCountry":Ljava/lang/Process;
    .end local v14    # "mLanguage":Ljava/lang/Process;
    .end local v18    # "screenoff":Landroid/content/IntentFilter;
    :catch_2
    move-exception v25

    goto/16 :goto_2
.end method

.method public onCreateDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1144
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->onCreateDescription()V

    .line 1145
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onCreateDescription()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1096
    invoke-static {p0}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->removeListener(Lcom/vlingo/core/internal/bluetooth/BluetoothManagerListener;)V

    .line 1097
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->smCurrentActivity:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    if-ne v0, p0, :cond_0

    .line 1098
    sput-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->smCurrentActivity:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .line 1100
    :cond_0
    sput-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    .line 1102
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onDestroy()V

    .line 1106
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->removeMessages(I)V

    .line 1108
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->removeMessages(I)V

    .line 1110
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->releaseFlow(Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;)V

    .line 1116
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 1117
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->helpWidgetBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1120
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->fullContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_3

    .line 1125
    :goto_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1127
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->removeListener(Lcom/vlingo/core/internal/audio/MicAnimationListener;)V

    .line 1128
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isTOSAccepted()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1129
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_2

    .line 1130
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 1131
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 1134
    :cond_2
    return-void

    .line 1122
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->fullContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$color;->solid_black:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public onEditCanceled()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2316
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getCurrGuideState()Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    if-eq v0, v1, :cond_1

    .line 2326
    :cond_0
    :goto_0
    return v3

    .line 2319
    :cond_1
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->NONE:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->AdjustHelpVisibility(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2322
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getCurrGuideState()Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    if-ne v0, v1, :cond_0

    .line 2323
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    invoke-virtual {v0, v4}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->removeMessages(I)V

    .line 2324
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v4, v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public onEditFinished()Z
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 2301
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getCurrGuideState()Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->EDIT:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    if-eq v0, v1, :cond_0

    .line 2311
    :goto_0
    return v3

    .line 2304
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->NONE:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->AdjustHelpVisibility(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2307
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->DONE:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setGuideState(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V

    .line 2309
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    invoke-virtual {v0, v4}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->removeMessages(I)V

    .line 2310
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    const-wide/16 v1, 0xbb8

    invoke-virtual {v0, v4, v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public onEditStarted()Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2294
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    invoke-virtual {v0, v3}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->removeMessages(I)V

    .line 2295
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v3, v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 2296
    const/4 v0, 0x0

    return v0
.end method

.method public onHeadsetConnected()V
    .locals 0

    .prologue
    .line 2406
    return-void
.end method

.method public onHeadsetDisconnected()V
    .locals 0

    .prologue
    .line 2412
    return-void
.end method

.method public onHomePressed()V
    .locals 1

    .prologue
    .line 1167
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 1172
    return-void
.end method

.method public onInit(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 2350
    if-nez p1, :cond_0

    .line 2351
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 2353
    :cond_0
    return-void
.end method

.method public onInterceptStartReco()Z
    .locals 1

    .prologue
    .line 2121
    const/4 v0, 0x0

    return v0
.end method

.method public onMicAnimationData([I)V
    .locals 0
    .param p1, "v"    # [I

    .prologue
    .line 2273
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1005
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->onPause()V

    .line 1006
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setTaskOnFinishGreetingTTS(Ljava/lang/Runnable;)V

    .line 1007
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onPause()V

    .line 1008
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->removeMessages(I)V

    .line 1009
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    invoke-virtual {v0, v3}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->removeMessages(I)V

    .line 1011
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->displayFromBackground:Z

    .line 1013
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1016
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z

    .line 1020
    :cond_0
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->smCurrentActivity:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    if-ne v0, p0, :cond_1

    iget-boolean v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->releaseKeyguardByMsg:Z

    if-nez v0, :cond_1

    .line 1021
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/VlingoApplication;

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->isSocialLogin()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn(Z)V

    .line 1025
    :cond_1
    iput-boolean v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->releaseKeyguardByMsg:Z

    .line 1026
    sget-boolean v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mIsStartedCustomWakeUpSetting:Z

    if-eqz v0, :cond_2

    .line 1027
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->finish()V

    .line 1030
    :cond_2
    iget-boolean v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->doneTosAndIux:Z

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mIsStartedCustomWakeUpSetting:Z

    if-eqz v0, :cond_3

    .line 1031
    sput-boolean v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mIsStartedCustomWakeUpSetting:Z

    .line 1034
    :cond_3
    invoke-virtual {p0, v3}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setControlFragmentActivityPaused(Z)V

    .line 1036
    return-void
.end method

.method public onRecoCancelled()V
    .locals 2

    .prologue
    .line 2181
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getControlFragmentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    if-eq v0, v1, :cond_0

    .line 2182
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->BUSY:Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V

    .line 2184
    :cond_0
    return-void
.end method

.method public onRecoToneStarting(Z)J
    .locals 2
    .param p1, "startTone"    # Z

    .prologue
    .line 2364
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onRecoToneStopped(Z)V
    .locals 0
    .param p1, "startTone"    # Z

    .prologue
    .line 2371
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 244
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCoverOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->finish()V

    .line 247
    :cond_0
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onRestart()V

    .line 248
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 680
    invoke-direct {p0, p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->unpackSavedInstanceState(Landroid/os/Bundle;)V

    .line 681
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 682
    return-void
.end method

.method public onResultsNoAction()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2167
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->onRecognitionResult(Ljava/lang/String;)V

    .line 2168
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->nluEventLogUtil:Lcom/vlingo/midas/util/log/NLUEventLogUtil;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/util/log/NLUEventLogUtil;->onRecognitionResult(Ljava/lang/String;)V

    .line 2171
    return-void
.end method

.method protected onResume()V
    .locals 13

    .prologue
    const/4 v12, 0x6

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 759
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 761
    iget-object v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    sget-object v6, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v5, v6}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 762
    iget-object v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->dialogFragment:Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/DialogFragmentEWYSHelp;->removeAlreadyExistingHelpScreen()V

    .line 765
    iput v10, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mPhraseSpottedDone:I

    .line 767
    invoke-virtual {p0, v10}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setControlFragmentActivityPaused(Z)V

    .line 768
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 770
    iget-boolean v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->displayFromBackground:Z

    if-nez v5, :cond_9

    .line 771
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v5

    invoke-virtual {v5, p0, p0, v8, v8}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->initFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    .line 776
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDisclaimerAccepted()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v5

    if-nez v5, :cond_a

    :cond_0
    iget-boolean v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->FEATURE_DO_NOT_SHOW_DISCLAIMER_POPUP:Z

    if-nez v5, :cond_a

    .line 778
    new-instance v5, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$4;

    invoke-direct {v5, p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$4;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    const-wide/16 v6, 0x78

    invoke-static {v5, v6, v7}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 787
    iput-boolean v11, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->doneTosAndIux:Z

    .line 793
    :goto_1
    sput-object p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->smCurrentActivity:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;

    .line 795
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 798
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v2

    .line 800
    .local v2, "isDrivingMode":Z
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isIUXComplete()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 801
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_4

    .line 809
    :try_start_0
    iget-boolean v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z

    if-eqz v5, :cond_4

    .line 810
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    .line 811
    .local v1, "isBtHeadsetConnected":Z
    if-eqz v1, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    iget-boolean v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startRecoBySCO:Z

    if-eqz v5, :cond_4

    .line 813
    :cond_2
    new-instance v4, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$5;

    invoke-direct {v4, p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$5;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    .line 832
    .local v4, "r":Ljava/lang/Runnable;
    iget-boolean v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startRecoBySCO:Z

    if-eqz v5, :cond_3

    .line 833
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v5

    const/4 v6, 0x6

    const/4 v7, 0x2

    invoke-virtual {v5, v6, v7, v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->setTaskOnGetAudioFocus(IILjava/lang/Runnable;)V

    .line 840
    :cond_3
    iget-object v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setTaskOnFinishGreetingTTS(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 849
    .end local v1    # "isBtHeadsetConnected":Z
    .end local v4    # "r":Ljava/lang/Runnable;
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 851
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->acquireWakeLock(Landroid/content/Intent;)V

    .line 853
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->sendPendingEvents()V

    .line 854
    iget-object v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v5}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 855
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isAppCarModeEnabled()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 868
    :cond_5
    :goto_3
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isIUXComplete()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 869
    iget-boolean v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->handleTextReqeust:Z

    if-eqz v5, :cond_e

    .line 870
    invoke-virtual {p0, v8}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->handleTextRequestIntent(Landroid/content/Intent;)V

    .line 908
    :cond_6
    :goto_4
    invoke-direct {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->checkFragmentLanguage()V

    .line 909
    sget v5, Lcom/vlingo/midas/R$id;->mainControlLL:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 910
    .local v3, "mainControlLL":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setOrientationDrivingMode()V

    .line 912
    if-eqz v3, :cond_7

    .line 913
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->invalidate()V

    .line 921
    :cond_7
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getApplication()Landroid/app/Application;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/VlingoApplication;

    invoke-virtual {v5, v10}, Lcom/vlingo/midas/VlingoApplication;->setSocialLogin(Z)V

    .line 923
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V

    .line 929
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isTOSAccepted()Z

    move-result v5

    if-nez v5, :cond_8

    .line 930
    new-instance v5, Landroid/speech/tts/TextToSpeech;

    invoke-direct {v5, p0, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    sput-object v5, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tts:Landroid/speech/tts/TextToSpeech;

    .line 932
    :cond_8
    return-void

    .line 773
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "isDrivingMode":Z
    .end local v3    # "mainControlLL":Landroid/widget/LinearLayout;
    :cond_9
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v5

    invoke-virtual {v5, p0, p0, v8, v8}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->stealFlow(Landroid/content/Context;Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener;Ljava/util/Map;Lcom/vlingo/core/internal/dialogmanager/WidgetFactory;)V

    goto/16 :goto_0

    .line 790
    :cond_a
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->languageSupportListCheck()V

    goto/16 :goto_1

    .line 858
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v2    # "isDrivingMode":Z
    :cond_b
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-eq v5, v11, :cond_c

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->isDialogMode()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 860
    :cond_c
    iget-object v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    if-eqz v5, :cond_5

    .line 861
    iget-object v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    iget-object v6, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    invoke-virtual {v5, v6}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->setCallback(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;)V

    goto :goto_3

    .line 862
    :cond_d
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v9, :cond_5

    .line 863
    iget-object v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    if-eqz v5, :cond_5

    .line 864
    iget-object v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    iget-object v6, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragmentCallback:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;

    invoke-virtual {v5, v6}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->setCallback(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlFragmentCallback;)V

    goto :goto_3

    .line 871
    :cond_e
    iget-boolean v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z

    if-eqz v5, :cond_6

    .line 872
    iget-boolean v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startRecoBySCO:Z

    if-nez v5, :cond_6

    .line 873
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    .line 874
    .restart local v1    # "isBtHeadsetConnected":Z
    if-eqz v1, :cond_f

    if-nez v2, :cond_f

    .line 881
    invoke-static {p0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v5

    new-instance v6, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$6;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$6;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    invoke-virtual {v5, v12, v9, v6}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->setTaskOnGetAudioFocus(IILjava/lang/Runnable;)V

    goto/16 :goto_4

    .line 897
    :cond_f
    iget-object v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    invoke-virtual {v5, v9}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->removeMessages(I)V

    .line 898
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v5

    if-nez v5, :cond_6

    .line 899
    iget-object v5, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    invoke-virtual {v5, v9}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_4

    .line 843
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "isBtHeadsetConnected":Z
    :catch_0
    move-exception v5

    goto/16 :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 671
    const-string/jumbo v0, "startedWithCarDock"

    iget-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->startedWithCarDock:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 672
    const-string/jumbo v0, "autoStartOnResume"

    iget-boolean v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->autoStartOnResume:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 673
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 674
    return-void
.end method

.method public onScoConnected()V
    .locals 1

    .prologue
    .line 2253
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setVolumeControlStream(I)V

    .line 2254
    return-void
.end method

.method public onScoDisconnected()V
    .locals 1

    .prologue
    .line 2258
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->setVolumeControlStream(I)V

    .line 2259
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->disconnectScoByIdle()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2260
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->interruptTurn()V

    .line 2261
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelAudio()V

    .line 2262
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 2264
    :cond_0
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 939
    if-nez p2, :cond_1

    .line 945
    :cond_0
    :goto_0
    return-void

    .line 942
    :cond_1
    const-string/jumbo v0, "language"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->resetAllContent()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 1040
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onStart()V

    .line 1042
    sget v0, Lcom/vlingo/midas/R$anim;->blinker:I

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mBlinkAnimation:Landroid/view/animation/Animation;

    .line 1043
    sget v0, Lcom/vlingo/midas/R$anim;->scale_tool_tip:I

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mScaleAnimation:Landroid/view/animation/Animation;

    .line 1045
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/VlingoApplication;->setIsInForeground(Z)V

    .line 1046
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/iux/IUXManagerHelp;->isIUXComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1048
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->getForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 1052
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "seamless_pause"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1054
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->removeMessages(I)V

    .line 1065
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    .line 1069
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "seamless_resume"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1071
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tosDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tosDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1072
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tosDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1075
    :cond_0
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onStop()V

    .line 1078
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/VlingoApplication;->setIsInForeground(Z)V

    .line 1079
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->shouldIncomingMessagesReadout()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1080
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->releaseForegroundFocus(Lcom/vlingo/core/internal/safereader/ISafeReaderAlertHandler;)V

    .line 1081
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->stopSafeReading()V

    .line 1084
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->cancelTTS()V

    .line 1085
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;

    const/4 v1, 0x3

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$EWYSHelpActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1086
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    if-eqz v0, :cond_2

    .line 1088
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->latestWidget:Lcom/vlingo/midas/gui/Widget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/Widget;->onStop()V

    .line 1091
    :cond_2
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 1150
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onTrimMemory(I)V

    .line 1151
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/ui/HomeKeyListener;->onTrimMemory(I)V

    .line 1152
    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    .prologue
    .line 2204
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 1138
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0}, Lcom/vlingo/midas/ui/HomeKeyListener;->onUserLeaveHint()V

    .line 1139
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onUserLeaveHint()V

    .line 1140
    return-void
.end method

.method public promptUser(Ljava/lang/String;)V
    .locals 2
    .param p1, "_msg"    # Ljava/lang/String;

    .prologue
    .line 1938
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 1939
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->announceTTS(Ljava/lang/String;)V

    .line 1940
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->showVlingoText(Ljava/lang/String;)V

    .line 1941
    return-void
.end method

.method public setControlFragmentActivityPaused(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 2284
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    if-eqz v0, :cond_0

    .line 2285
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/LandRegularControlFragmentEWYSHelp;->setActivityPaused(Z)V

    .line 2288
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    if-eqz v0, :cond_1

    .line 2289
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/RegularControlFragmentEWYSHelp;->setActivityPaused(Z)V

    .line 2291
    :cond_1
    return-void
.end method

.method public setGuideState(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;)V
    .locals 0
    .param p1, "guideState"    # Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    .prologue
    .line 2334
    iput-object p1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mGuideState:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    .line 2335
    return-void
.end method

.method protected setOrientationDrivingMode()V
    .locals 5

    .prologue
    .line 971
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v0

    .line 973
    .local v0, "state":Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->changeToDriveMode(Z)V

    .line 974
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 976
    sget-object v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$midas$gui$EditWhatYouSaidHelpActivity$GuideState:[I

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getCurrGuideState()Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$GuideState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 995
    :goto_0
    return-void

    .line 982
    :pswitch_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    const/4 v2, 0x1

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 985
    :pswitch_1
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 988
    :pswitch_2
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 991
    :pswitch_3
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mTutorialHandler:Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$TutorialHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 976
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public showControlFragment()V
    .locals 2

    .prologue
    .line 2267
    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2269
    return-void
.end method

.method public showDialogFlowStateChange(Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;)V
    .locals 2
    .param p1, "newState"    # Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;

    .prologue
    .line 2104
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$core$internal$dialogmanager$DialogFlow$DialogFlowState:[I

    invoke-virtual {p1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow$DialogFlowState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2117
    :goto_0
    return-void

    .line 2106
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 2107
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->onIdle()V

    .line 2109
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mPhraseSpottedDone:I

    goto :goto_0

    .line 2113
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 2114
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->DIALOG_BUSY:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    goto :goto_0

    .line 2104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Ljava/lang/String;)V
    .locals 5
    .param p1, "error"    # Lcom/vlingo/sdk/recognition/VLRecognitionErrors;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 1948
    invoke-static {p1, p0}, Lcom/vlingo/midas/util/ErrorCodeUtils;->getLocalizedMessageForErrorCode(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1955
    .local v0, "localizedMessage":Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->shouldReportError(Lcom/vlingo/sdk/recognition/VLRecognitionErrors;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1956
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    sget-object v2, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->ERROR:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    .line 1958
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->announceTTS(Ljava/lang/String;)V

    .line 1960
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 1961
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-virtual {v1}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->onRecognitionError()V

    .line 1962
    return-void
.end method

.method public showRMSChange(I)V
    .locals 1
    .param p1, "rmsValue"    # I

    .prologue
    .line 1983
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->showRMSChange(I)V

    .line 1984
    return-void
.end method

.method public showReceivedResults(Lcom/vlingo/core/internal/logging/EventLog;)V
    .locals 2
    .param p1, "results"    # Lcom/vlingo/core/internal/logging/EventLog;

    .prologue
    .line 2158
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_POST_RESPONSE:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 2159
    if-eqz p1, :cond_0

    .line 2160
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/logging/EventLog;->getAsrLogMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->onRecognitionResult(Ljava/lang/String;)V

    .line 2161
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->nluEventLogUtil:Lcom/vlingo/midas/util/log/NLUEventLogUtil;

    invoke-virtual {p1}, Lcom/vlingo/core/internal/logging/EventLog;->getNluLogMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/util/log/NLUEventLogUtil;->onRecognitionResult(Ljava/lang/String;)V

    .line 2163
    :cond_0
    return-void
.end method

.method public showRecoStateChange(Lcom/vlingo/sdk/recognition/VLRecognitionStates;)V
    .locals 6
    .param p1, "newState"    # Lcom/vlingo/sdk/recognition/VLRecognitionStates;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1991
    sget-object v0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$11;->$SwitchMap$com$vlingo$sdk$recognition$VLRecognitionStates:[I

    invoke-virtual {p1}, Lcom/vlingo/sdk/recognition/VLRecognitionStates;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2050
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2014
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->removeWakeupBubble()V

    .line 2015
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 2017
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-virtual {v0}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->onRecognitionStart()V

    goto :goto_0

    .line 2027
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getPhraseSpotted()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mPhraseSpottedDone:I

    if-ne v0, v5, :cond_0

    .line 2029
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getPhraseSpotted()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->custom_wakeup_disabled:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2031
    const/4 v0, 0x2

    iput v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mPhraseSpottedDone:I

    .line 2032
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WAKE_UP:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    invoke-virtual {p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->recognized_wakeup:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5, v4}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    goto :goto_0

    .line 2038
    :cond_1
    iput v4, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->mPhraseSpottedDone:I

    goto :goto_0

    .line 2044
    :pswitch_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 2045
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->asrEventLogUtil:Lcom/vlingo/midas/util/log/AsrEventLogUtil;

    invoke-virtual {v0}, Lcom/vlingo/midas/util/log/AsrEventLogUtil;->onRecognitionComplete()V

    goto :goto_0

    .line 1991
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected showSettings()V
    .locals 0

    .prologue
    .line 1301
    return-void
.end method

.method protected showTOS()V
    .locals 4

    .prologue
    .line 1256
    new-instance v1, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$7;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$7;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    new-instance v2, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$8;

    invoke-direct {v2, p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$8;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    new-instance v3, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$9;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity$9;-><init>(Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;)V

    invoke-static {p0, v1, v2, v3}, Lcom/vlingo/midas/tos/TermsOfServiceManager;->getTOSDialog(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog;

    move-result-object v0

    .line 1295
    .local v0, "dialog":Landroid/app/AlertDialog;
    iput-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tosDialog:Landroid/app/AlertDialog;

    .line 1296
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->tosDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1297
    return-void
.end method

.method public showUserText(Ljava/lang/String;Lcom/vlingo/sdk/recognition/NBestData;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "nbest"    # Lcom/vlingo/sdk/recognition/NBestData;

    .prologue
    .line 2136
    const-string/jumbo v1, "last_asr_result"

    invoke-static {v1, p1}, Lcom/vlingo/midas/settings/MidasSettings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2138
    iget-object v1, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    sget-object v2, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->USER:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, p1, v3, v4}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    move-result-object v0

    .line 2140
    .local v0, "db":Lcom/vlingo/midas/gui/DialogBubble;
    if-eqz v0, :cond_0

    .line 2141
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getPreservedState()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/DialogBubble;->setTag(Ljava/lang/Object;)V

    .line 2143
    :cond_0
    return-void
.end method

.method public showVlingoText(Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 2151
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->VLINGO:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    .line 2154
    return-void
.end method

.method public showWarning(Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;Ljava/lang/String;)Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;
    .locals 4
    .param p1, "warning"    # Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 1972
    sget-object v0, Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;->WARNING_NOTHING_RECOGNIZED:Lcom/vlingo/sdk/recognition/VLRecognitionWarnings;

    if-eq p1, v0, :cond_0

    .line 1973
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    sget-object v1, Lcom/vlingo/midas/gui/DialogBubble$BubbleType;->WARNING:Lcom/vlingo/midas/gui/DialogBubble$BubbleType;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p2, v2, v3}, Lcom/vlingo/midas/gui/ContentFragment;->addDialogBubble(Lcom/vlingo/midas/gui/DialogBubble$BubbleType;Ljava/lang/String;ZZ)Lcom/vlingo/midas/gui/DialogBubble;

    .line 1975
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v0, p2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->announceTTS(Ljava/lang/String;)V

    .line 1976
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;->ShowedWarning:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;

    .line 1978
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;->Noop:Lcom/vlingo/core/internal/dialogmanager/DialogFlowListener$ShowWarningResult;

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1176
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/ui/HomeKeyListener;->startActivity(Landroid/content/Intent;)V

    .line 1177
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->startActivity(Landroid/content/Intent;)V

    .line 1178
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 1183
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/midas/ui/HomeKeyListener;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1184
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/ui/VLActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1187
    :goto_0
    return-void

    .line 1185
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;I)V
    .locals 1
    .param p1, "child"    # Landroid/app/Activity;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "requestCode"    # I

    .prologue
    .line 1193
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/midas/ui/HomeKeyListener;->startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;I)V

    .line 1194
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/midas/ui/VLActivity;->startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1197
    :goto_0
    return-void

    .line 1195
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V
    .locals 1
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "requestCode"    # I

    .prologue
    .line 1212
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1, p2, p3}, Lcom/vlingo/midas/ui/HomeKeyListener;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V

    .line 1213
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/midas/ui/VLActivity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1216
    :goto_0
    return-void

    .line 1214
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public startActivityIfNeeded(Landroid/content/Intent;I)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 1202
    :try_start_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->homeKeyDetector:Lcom/vlingo/midas/ui/HomeKeyListener;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/midas/ui/HomeKeyListener;->startActivityIfNeeded(Landroid/content/Intent;I)V

    .line 1203
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/ui/VLActivity;->startActivityIfNeeded(Landroid/content/Intent;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1206
    :goto_0
    return v0

    .line 1204
    :catch_0
    move-exception v0

    .line 1206
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggleDriveMode()V
    .locals 3

    .prologue
    .line 1304
    const/4 v0, 0x0

    .line 1306
    .local v0, "driveFlag":Z
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v2}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->getCurrentState()Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;

    move-result-object v1

    .line 1307
    .local v1, "state":Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->changeToDriveMode(Z)V

    .line 1308
    iget-object v2, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;

    invoke-virtual {v2, v1}, Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp;->setState(Lcom/vlingo/midas/gui/ControlFragmentEWYSHelp$ControlState;)V

    .line 1309
    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 2280
    iget-object v0, p0, Lcom/vlingo/midas/gui/EditWhatYouSaidHelpActivity;->contentFragment:Lcom/vlingo/midas/gui/ContentFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ContentFragment;->onUserCancel()V

    .line 2281
    return-void
.end method
