.class Lcom/vlingo/midas/gui/widgets/MessageWidget$1;
.super Ljava/lang/Object;
.source "MessageWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/widgets/MessageWidget;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/MessageWidget;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/widgets/MessageWidget;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v3, 0x8

    .line 47
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MessageWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    if-eqz v1, :cond_1

    .line 48
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MessageWidget;->sendBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->access$000(Lcom/vlingo/midas/gui/widgets/MessageWidget;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 49
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageWidget;

    const/4 v2, 0x1

    # setter for: Lcom/vlingo/midas/gui/widgets/MessageWidget;->isClicked:Z
    invoke-static {v1, v2}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->access$102(Lcom/vlingo/midas/gui/widgets/MessageWidget;Z)Z

    .line 50
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->access$200(Lcom/vlingo/midas/gui/widgets/MessageWidget;)Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageWidget;

    # getter for: Lcom/vlingo/midas/gui/widgets/MessageWidget;->cancelBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->access$200(Lcom/vlingo/midas/gui/widgets/MessageWidget;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageWidget;

    invoke-virtual {v1}, Lcom/vlingo/midas/gui/widgets/MessageWidget;->retire()V

    .line 54
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 55
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.core.internal.dialogmanager.Default"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/MessageWidget$1;->this$0:Lcom/vlingo/midas/gui/widgets/MessageWidget;

    iget-object v1, v1, Lcom/vlingo/midas/gui/widgets/MessageWidget;->listener:Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/vlingo/core/internal/dialogmanager/vvs/WidgetActionListener;->handleIntent(Landroid/content/Intent;Ljava/lang/Object;)V

    .line 58
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method
