.class Lcom/vlingo/midas/gui/ConversationActivity$3;
.super Landroid/os/Handler;
.source "ConversationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/ConversationActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/ConversationActivity;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/ConversationActivity;)V
    .locals 0

    .prologue
    .line 1326
    iput-object p1, p0, Lcom/vlingo/midas/gui/ConversationActivity$3;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1328
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1365
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1333
    :pswitch_1
    invoke-static {}, Lcom/vlingo/midas/ServiceManager;->getInstance()Lcom/vlingo/midas/ServiceManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/ServiceManager;->startLmttVoconService(Z)Z

    .line 1334
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->requiresIUX()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-nez v0, :cond_1

    .line 1336
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$3;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    sget-boolean v1, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    invoke-static {v0, v1}, Lcom/vlingo/midas/iux/IUXManager;->processIUX(Landroid/app/Activity;Z)V

    .line 1337
    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v0, :cond_0

    .line 1340
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$3;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    # invokes: Lcom/vlingo/midas/gui/ConversationActivity;->startOtherAppAfterAccept()V
    invoke-static {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->access$1800(Lcom/vlingo/midas/gui/ConversationActivity;)V

    goto :goto_0

    .line 1342
    :cond_1
    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v0, :cond_2

    .line 1345
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$3;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->finish()V

    goto :goto_0

    .line 1348
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1349
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$3;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->showPreferences()V

    goto :goto_0

    .line 1351
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$3;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    iget-object v0, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1358
    :pswitch_2
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$3;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.vlingo.midas.SeamlessRecognition"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->stopService(Landroid/content/Intent;)Z

    .line 1359
    sget-boolean v0, Lcom/vlingo/midas/gui/ConversationActivity;->isSvoiceLaunchedFromDriveLink:Z

    if-eqz v0, :cond_4

    .line 1360
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$3;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->setResult(I)V

    .line 1362
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/gui/ConversationActivity$3;->this$0:Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->finish()V

    goto :goto_0

    .line 1328
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
