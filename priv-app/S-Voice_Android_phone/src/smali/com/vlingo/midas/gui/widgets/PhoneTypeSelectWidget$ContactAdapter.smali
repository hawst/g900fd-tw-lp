.class public Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;
.super Landroid/widget/BaseAdapter;
.source "PhoneTypeSelectWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ContactAdapter"
.end annotation


# instance fields
.field private final allContactInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneTypeInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/core/internal/contacts/ContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p3, "phoneTypeInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "allContactInfo":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/core/internal/contacts/ContactData;>;"
    iput-object p1, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 78
    iput-object p4, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->allContactInfo:Ljava/util/List;

    .line 79
    iput-object p3, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->phoneTypeInfo:Ljava/util/List;

    .line 80
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 92
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->allContactInfo:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 93
    .local v0, "toReturn":I
    iget-object v1, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;

    invoke-virtual {v1, v0}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->getLimitedCount(I)I

    move-result v1

    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 83
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->allContactInfo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 87
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedPhoneType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "phoneType"    # Ljava/lang/String;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->phone_type_home:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const-string/jumbo v0, "1"

    .line 146
    :goto_0
    return-object v0

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->phone_type_mobile:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    const-string/jumbo v0, "2"

    goto :goto_0

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->phone_type_work:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    const-string/jumbo v0, "3"

    goto :goto_0

    .line 146
    :cond_2
    const-string/jumbo v0, "7"

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 98
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->allContactInfo:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/contacts/ContactData;

    .line 99
    .local v0, "contactInfo":Lcom/vlingo/core/internal/contacts/ContactData;
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->phoneTypeInfo:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 101
    .local v2, "phoneType":Ljava/lang/String;
    if-nez p2, :cond_0

    .line 102
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->this$0:Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;

    invoke-virtual {v3}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->item_phone_type_choice:I

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 103
    new-instance v1, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;

    invoke-direct {v1, v5}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;-><init>(Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$1;)V

    .line 104
    .local v1, "holder":Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;
    sget v3, Lcom/vlingo/midas/R$id;->phone_number:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;->number:Landroid/widget/TextView;

    .line 105
    sget v3, Lcom/vlingo/midas/R$id;->phone_type:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;->type:Landroid/widget/TextView;

    .line 106
    sget v3, Lcom/vlingo/midas/R$id;->divider:I

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v1, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;->divider:Landroid/view/View;

    .line 107
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 111
    :goto_0
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;->number:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/vlingo/core/internal/contacts/ContactData;->address:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;->type:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    if-nez p1, :cond_1

    .line 115
    sget v3, Lcom/vlingo/midas/R$drawable;->list_selector_top:I

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 123
    :goto_1
    new-instance v3, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter$1;

    invoke-direct {v3, p0, p1}, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter$1;-><init>(Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;I)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    return-object p2

    .line 109
    .end local v1    # "holder":Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;

    .restart local v1    # "holder":Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;
    goto :goto_0

    .line 116
    :cond_1
    iget-object v3, p0, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ContactAdapter;->allContactInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p1, v3, :cond_2

    .line 117
    iget-object v3, v1, Lcom/vlingo/midas/gui/widgets/PhoneTypeSelectWidget$ViewHolder;->divider:Landroid/view/View;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 118
    sget v3, Lcom/vlingo/midas/R$drawable;->list_selector_bottom:I

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 120
    :cond_2
    sget v3, Lcom/vlingo/midas/R$drawable;->list_selector_middle:I

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method
