.class Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$1;
.super Ljava/lang/Object;
.source "VoicePromptUserConfirmation.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->getConfirmationDialog(Lcom/vlingo/core/internal/settings/VoicePrompt;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;

.field final synthetic val$_onOK:Ljava/lang/Runnable;

.field final synthetic val$view:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$1;->this$0:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;

    iput-object p2, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$1;->val$view:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;

    iput-object p3, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$1;->val$_onOK:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x14

    .line 87
    # setter for: Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->alertDialogIsShowing:Z
    invoke-static {v2}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation;->access$002(Z)Z

    .line 89
    iget-object v0, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$1;->val$view:Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$ConfirmationView;->DontAsk()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-static {v2}, Lcom/vlingo/core/internal/settings/VoicePrompt;->shouldAsk(Z)V

    .line 92
    :cond_0
    const-string/jumbo v0, "voice_prompt_preference_value"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 93
    const-string/jumbo v0, "voice_prompt_preference_value"

    const/16 v1, 0x15

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 96
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$1;->val$_onOK:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/vlingo/midas/gui/VoicePromptUserConfirmation$1;->val$_onOK:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 98
    :cond_1
    return-void

    .line 95
    :cond_2
    const-string/jumbo v0, "voice_prompt_preference_value"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_0
.end method
