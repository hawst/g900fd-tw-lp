.class Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;
.super Landroid/os/Handler;
.source "AdvancedFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/twopane/AdvancedFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AdvancedFragmentHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/settings/twopane/AdvancedFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    .prologue
    .line 305
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 306
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 307
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 310
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    .line 311
    .local v0, "o":Lcom/vlingo/midas/settings/twopane/AdvancedFragment;
    if-eqz v0, :cond_0

    .line 313
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 314
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 316
    :pswitch_0
    invoke-virtual {v0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->updateSocialUI()V

    goto :goto_0

    .line 314
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
