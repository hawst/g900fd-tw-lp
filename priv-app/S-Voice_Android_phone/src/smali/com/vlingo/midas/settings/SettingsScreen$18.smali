.class Lcom/vlingo/midas/settings/SettingsScreen$18;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->showWakeupLockScreenDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 1920
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$18;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    iput-object p2, p0, Lcom/vlingo/midas/settings/SettingsScreen$18;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x1

    .line 1925
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$18;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1926
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$18;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1927
    .local v2, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1928
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v3, "VOICE_WAKE_UP_POP_SETTINGSSCREEN"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1929
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1931
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "sharedPref":Landroid/content/SharedPreferences;
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSoundDetectorSettingOn()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1932
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$18;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->toast_while_running_sound_detector_for_svoice:I

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1941
    :goto_0
    return-void

    .line 1937
    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$18;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1938
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "preference_clicked"

    const-string/jumbo v4, "wake_up_lock_screen"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1939
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$18;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v3, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
