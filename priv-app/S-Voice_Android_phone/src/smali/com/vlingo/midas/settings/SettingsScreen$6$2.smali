.class Lcom/vlingo/midas/settings/SettingsScreen$6$2;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen$6;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen$6;)V
    .locals 0

    .prologue
    .line 837
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 841
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v4, v4, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 843
    .local v1, "homeAddress":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 844
    .local v2, "homeAddressCheck":Ljava/lang/String;
    const-string/jumbo v4, "\n"

    const-string/jumbo v5, " "

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 846
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    if-gtz v4, :cond_1

    .line 847
    const-string/jumbo v4, "car_nav_home_address"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v4, v4, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const-string/jumbo v5, "input_method"

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/SettingsScreen;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .line 858
    .local v3, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v4, v4, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 861
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v4, v4, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v4, v4, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 862
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v4, v4, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Dialog;->cancel()V

    .line 865
    :cond_0
    const-wide/16 v4, 0x96

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 870
    :goto_1
    return-void

    .line 849
    .end local v3    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    const-string/jumbo v4, "*#DEBUG#*"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 850
    const-string/jumbo v2, ""

    .line 851
    const/4 v4, 0x1

    sput-boolean v4, Lcom/vlingo/midas/settings/debug/DebugSettings;->SHOW_DEBUG:Z

    .line 854
    :cond_2
    const-string/jumbo v4, "car_nav_home_address"

    invoke-static {v4, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 866
    .restart local v3    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :catch_0
    move-exception v0

    .line 868
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
