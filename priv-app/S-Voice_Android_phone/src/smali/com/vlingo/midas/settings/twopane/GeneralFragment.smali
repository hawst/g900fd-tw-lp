.class public Lcom/vlingo/midas/settings/twopane/GeneralFragment;
.super Landroid/preference/PreferenceFragment;
.source "GeneralFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;


# instance fields
.field private context:Landroid/content/Context;

.field private mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

.field private mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

.field private m_about:Landroid/preference/Preference;

.field private m_languageApplication:Landroid/preference/ListPreference;

.field private scale:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_languageApplication:Landroid/preference/ListPreference;

    .line 23
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_about:Landroid/preference/Preference;

    .line 27
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->context:Landroid/content/Context;

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V
    .locals 1
    .param p1, "onFragmentLoadListener"    # Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_languageApplication:Landroid/preference/ListPreference;

    .line 23
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_about:Landroid/preference/Preference;

    .line 27
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->context:Landroid/content/Context;

    .line 30
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/settings/twopane/GeneralFragment;)Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/twopane/GeneralFragment;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    return-object v0
.end method

.method private initFragments()V
    .locals 2

    .prologue
    .line 82
    const-string/jumbo v0, "language"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_languageApplication:Landroid/preference/ListPreference;

    .line 83
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_languageApplication:Landroid/preference/ListPreference;

    new-instance v1, Lcom/vlingo/midas/settings/twopane/GeneralFragment$1;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment$1;-><init>(Lcom/vlingo/midas/settings/twopane/GeneralFragment;)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 91
    const-string/jumbo v0, "help_about"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_about:Landroid/preference/Preference;

    .line 92
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_about:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_about:Landroid/preference/Preference;

    new-instance v1, Lcom/vlingo/midas/settings/twopane/GeneralFragment$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment$2;-><init>(Lcom/vlingo/midas/settings/twopane/GeneralFragment;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 101
    :cond_0
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 55
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    .line 56
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->finish()V

    .line 117
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->context:Landroid/content/Context;

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;->onHeadlineSelected(I)V

    .line 110
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->context:Landroid/content/Context;

    .line 41
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    iput v2, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->scale:F

    .line 42
    sget v2, Lcom/vlingo/midas/R$xml;->general_settings:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->addPreferencesFromResource(I)V

    .line 43
    invoke-direct {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->initFragments()V

    .line 44
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$layout;->settings_path_view:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    .local v1, "view":Landroid/view/View;
    sget v2, Lcom/vlingo/midas/R$id;->fragment_description:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 46
    .local v0, "textView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->settings_general_lower:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    if-eqz v2, :cond_0

    .line 49
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;->updatePathHeader(Landroid/view/View;I)V

    .line 50
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDetach()V

    .line 61
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->setOnSettingsContentClickListener(Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;)V

    .line 62
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 66
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 68
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->fontScale:F

    .line 69
    .local v0, "new_scale":F
    iget v1, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->scale:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    iput v0, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->scale:F

    .line 71
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 72
    sget v1, Lcom/vlingo/midas/R$xml;->general_settings:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->addPreferencesFromResource(I)V

    .line 73
    invoke-direct {p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->initFragments()V

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_languageApplication:Landroid/preference/ListPreference;

    if-eqz v1, :cond_1

    .line 77
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_languageApplication:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->m_languageApplication:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 79
    :cond_1
    return-void
.end method

.method setOnSettingsContentClickListener(Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;)V
    .locals 0
    .param p1, "mClickListener"    # Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    .line 105
    return-void
.end method
