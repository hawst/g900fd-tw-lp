.class public Lcom/vlingo/midas/settings/twopane/AdvancedFragment;
.super Landroid/preference/PreferenceFragment;
.source "AdvancedFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;
.implements Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;
.implements Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LengthToastFilter;,
        Lcom/vlingo/midas/settings/twopane/AdvancedFragment$InputTextWatcher;,
        Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LoginAccountBroadcastReceiver;,
        Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;
    }
.end annotation


# static fields
.field private static final ERASE_SERVER_DATA:Ljava/lang/String; = "erase_server_data"

.field private static final MAX_LENGHT:I = 0x12c

.field private static final MSG_UPDATE_SOCIAL_UI:I = 0x0

.field private static final SMARTNOTIFICATION:Ljava/lang/String; = "smartNotification"

.field private static alphaPattern:Ljava/util/regex/Pattern;


# instance fields
.field private autoStartSpeakerPreference:Landroid/preference/CheckBoxPreference;

.field private context:Landroid/content/Context;

.field private final greyDisabled:Z

.field private loginAccountBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mEraseServerData:Landroid/preference/Preference;

.field private mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

.field private mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

.field private m_carModeHomeAddress:Landroid/preference/EditTextPreference;

.field private m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

.field private m_headsetMode:Landroid/preference/CheckBoxPreference;

.field private m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

.field private mhandler:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

.field private scale:F

.field private smartNotification:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66
    const-string/jumbo v0, ".*\\p{Alnum}.*"

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->alphaPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->greyDisabled:Z

    .line 76
    new-instance v0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)V

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mhandler:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->context:Landroid/content/Context;

    .line 92
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V
    .locals 1
    .param p1, "onFragmentLoadListener"    # Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->greyDisabled:Z

    .line 76
    new-instance v0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)V

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mhandler:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->context:Landroid/content/Context;

    .line 87
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .line 88
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mhandler:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/twopane/AdvancedFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->containsNoAlphaNumeric(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private checkForSupportedSocialPlatforms()V
    .locals 3

    .prologue
    .line 277
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 279
    .local v0, "preferenceScreen":Landroid/preference/PreferenceScreen;
    const-string/jumbo v1, "facebook_account"

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    .line 280
    const-string/jumbo v1, "twitter_account"

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    .line 281
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "zh_CN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 282
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v1, :cond_0

    .line 283
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 284
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 293
    .end local v0    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    :cond_0
    :goto_0
    return-void

    .line 288
    .restart local v0    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    :cond_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v1, :cond_0

    .line 289
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private containsNoAlphaNumeric(Ljava/lang/String;)Z
    .locals 2
    .param p1, "promptString"    # Ljava/lang/String;

    .prologue
    .line 542
    sget-object v0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->alphaPattern:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initFragments()V
    .locals 3

    .prologue
    .line 127
    const-string/jumbo v0, "car_auto_start_speakerphone"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/TwoLineCheckBoxPreference;

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->autoStartSpeakerPreference:Landroid/preference/CheckBoxPreference;

    .line 128
    sget-object v0, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual {v0}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasDialing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasCallEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "no_receiver_in_call"

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->autoStartSpeakerPreference:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    .line 131
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->autoStartSpeakerPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 134
    :cond_1
    const-string/jumbo v0, "headset_mode"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_headsetMode:Landroid/preference/CheckBoxPreference;

    .line 135
    const-string/jumbo v0, "erase_server_data"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mEraseServerData:Landroid/preference/Preference;

    .line 136
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mEraseServerData:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    .line 137
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSpeakerID()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 140
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mEraseServerData:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 153
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->autoStartSpeakerPreference:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    .line 154
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->autoStartSpeakerPreference:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "car_auto_start_speakerphone"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 156
    :cond_3
    const-string/jumbo v0, "smartNotification"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->smartNotification:Landroid/preference/CheckBoxPreference;

    .line 157
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->smartNotification:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$3;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$3;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 166
    const-string/jumbo v0, "car_nav_home_address"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    .line 167
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    const/16 v1, 0x4001

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 168
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    new-instance v1, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)V

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 238
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->updateSocialUI()V

    .line 239
    invoke-direct {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->checkForSupportedSocialPlatforms()V

    .line 240
    return-void

    .line 143
    :cond_4
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mEraseServerData:Landroid/preference/Preference;

    new-instance v1, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$2;

    invoke-direct {v1, p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$2;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0
.end method

.method private updateUI()V
    .locals 3

    .prologue
    .line 324
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    if-eqz v1, :cond_0

    .line 325
    const-string/jumbo v1, "car_nav_home_address"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "summery":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 327
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 328
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 335
    .end local v0    # "summery":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 331
    .restart local v0    # "summery":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    sget v2, Lcom/vlingo/midas/R$string;->settings_configure_home_address:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 332
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 118
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$layout;->settings_path_view:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 119
    .local v1, "view":Landroid/view/View;
    sget v2, Lcom/vlingo/midas/R$id;->fragment_description:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 120
    .local v0, "textView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->advaced_lower:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    if-eqz v2, :cond_0

    .line 123
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;->updatePathHeader(Landroid/view/View;I)V

    .line 124
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 298
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 299
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    .line 300
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 594
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    if-eqz v0, :cond_0

    .line 595
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->finish()V

    .line 597
    :cond_0
    return-void
.end method

.method public onChangeAccountInfo()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 486
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mhandler:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;->removeMessages(I)V

    .line 487
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mhandler:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;->sendEmptyMessage(I)Z

    .line 488
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 589
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->context:Landroid/content/Context;

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;->onHeadlineSelected(I)V

    .line 590
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->context:Landroid/content/Context;

    .line 98
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->fontScale:F

    iput v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->scale:F

    .line 99
    sget v0, Lcom/vlingo/midas/R$xml;->advanced_settings:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->addPreferencesFromResource(I)V

    .line 100
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->makeFeatureForTablet()V

    .line 101
    new-instance v0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LoginAccountBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LoginAccountBroadcastReceiver;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;Lcom/vlingo/midas/settings/twopane/AdvancedFragment$1;)V

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->loginAccountBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 102
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->loginAccountBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 103
    invoke-direct {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->initFragments()V

    .line 104
    new-instance v0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$1;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$1;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)V

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$1;->start()V

    .line 113
    return-void
.end method

.method protected onDestory()V
    .locals 2

    .prologue
    .line 491
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->unbindFacebookService(Landroid/content/Context;)V

    .line 492
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->unbindTwitterService(Landroid/content/Context;)V

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->loginAccountBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 496
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->loginAccountBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 498
    :cond_1
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 499
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 273
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 274
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 244
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 245
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->fontScale:F

    .line 246
    .local v0, "new_scale":F
    iget v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->scale:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    iput v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->scale:F

    .line 248
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 249
    sget v1, Lcom/vlingo/midas/R$xml;->advanced_settings:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->addPreferencesFromResource(I)V

    .line 250
    invoke-direct {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->initFragments()V

    .line 253
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 254
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 255
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->getFbAccountInfo(Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;)V

    .line 257
    :cond_1
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 258
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->getTwAccountInfo(Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;)V

    .line 260
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->updateUI()V

    .line 261
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v1

    if-nez v1, :cond_3

    .line 262
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mhandler:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;->removeMessages(I)V

    .line 263
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mhandler:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 265
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 266
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->updateSocialUI()V

    .line 268
    :cond_4
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 562
    if-nez p2, :cond_1

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 563
    :cond_1
    const-string/jumbo v1, "car_nav_home_address"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 564
    invoke-direct {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->updateUI()V

    .line 565
    const/4 v0, 0x0

    .line 566
    .local v0, "prompt":Ljava/lang/String;
    const-string/jumbo v1, "car_nav_home_address"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 567
    if-eqz v0, :cond_2

    .line 568
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 569
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 570
    const-string/jumbo v1, "car_nav_home_address"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 576
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0, v0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->containsNoAlphaNumeric(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 577
    :cond_3
    const-string/jumbo v1, "car_nav_home_address"

    const-string/jumbo v2, ""

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 580
    .end local v0    # "prompt":Ljava/lang/String;
    :cond_4
    const-string/jumbo v1, "headset_mode"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 581
    const-string/jumbo v1, "headset_mode"

    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_headsetMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 582
    :cond_5
    const-string/jumbo v1, "car_auto_start_speakerphone"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-nez v1, :cond_0

    .line 583
    const-string/jumbo v1, "car_auto_start_speakerphone"

    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->autoStartSpeakerPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method protected updateSocialUI()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x4

    const/4 v8, 0x0

    .line 338
    const-string/jumbo v4, "AdvancedFragment"

    const-string/jumbo v5, "AdvancedFragment::updateSocialUI()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 340
    const-string/jumbo v4, "key_social_login_attemp_for_resume"

    invoke-static {v4, v8}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 341
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 342
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 343
    invoke-direct {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->checkForSupportedSocialPlatforms()V

    .line 345
    const-string/jumbo v4, "facebook_account"

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    .line 346
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v4, :cond_1

    .line 347
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 348
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "android.intent.action.MAIN"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 349
    const-string/jumbo v4, "android.intent.extra.INTENT"

    const/16 v5, 0x20

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 350
    const-string/jumbo v4, "android.intent.extra.TEXT"

    const-string/jumbo v5, "false"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    const-string/jumbo v4, "weibo_account"

    invoke-static {v4, v8}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 353
    .local v1, "loggedIn":Z
    if-eqz v1, :cond_2

    .line 354
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget v6, Lcom/vlingo/midas/R$string;->settings_logged_in_as:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "weibo_account_name"

    const-string/jumbo v7, ""

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 355
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v5, Lcom/vlingo/midas/R$string;->settings_tap_to_logout_facebook:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 356
    const-string/jumbo v4, "weibo_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 357
    .local v3, "thumbImg":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v8}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    .line 358
    const-string/jumbo v4, "logout_social_network"

    invoke-virtual {v0, v4, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 359
    if-eqz v3, :cond_0

    .line 360
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v3}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewBitmap(Landroid/graphics/Bitmap;)V

    .line 368
    .end local v3    # "thumbImg":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v0}, Lcom/vlingo/midas/settings/ImagePreference;->setIntent(Landroid/content/Intent;)V

    .line 467
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "loggedIn":Z
    :cond_1
    :goto_1
    return-void

    .line 364
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v1    # "loggedIn":Z
    :cond_2
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v5, Lcom/vlingo/midas/R$string;->settings_login_weibo:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 365
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v5, Lcom/vlingo/midas/R$string;->settings_click_to_login_weibo:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v9}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    goto :goto_0

    .line 372
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "loggedIn":Z
    :cond_3
    const-string/jumbo v4, "twitter_account"

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    .line 373
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v4, :cond_5

    .line 374
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v4

    if-nez v4, :cond_9

    .line 375
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 376
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v4, "android.intent.action.MAIN"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 377
    const-string/jumbo v4, "android.intent.extra.INTENT"

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 378
    const-string/jumbo v4, "android.intent.extra.TEXT"

    const-string/jumbo v5, "false"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v0}, Lcom/vlingo/midas/settings/ImagePreference;->setIntent(Landroid/content/Intent;)V

    .line 382
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 383
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterAccountCreated(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 384
    const-string/jumbo v4, "twitter_account"

    invoke-static {v4, v8}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 386
    :cond_4
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterLoggedIn()Z

    move-result v1

    .line 391
    .restart local v1    # "loggedIn":Z
    :goto_2
    if-eqz v1, :cond_8

    .line 392
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget v6, Lcom/vlingo/midas/R$string;->settings_logged_in_as:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "twitter_account_name"

    const-string/jumbo v7, ""

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 393
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    sget v5, Lcom/vlingo/midas/R$string;->settings_tap_to_signout_twitter:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 394
    const-string/jumbo v4, "twitter_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 395
    .restart local v3    # "thumbImg":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v8}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    .line 396
    if-eqz v3, :cond_5

    .line 397
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v3}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewBitmap(Landroid/graphics/Bitmap;)V

    .line 421
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "loggedIn":Z
    .end local v3    # "thumbImg":Landroid/graphics/Bitmap;
    :cond_5
    :goto_3
    const-string/jumbo v4, "facebook_account"

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    .line 422
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v4, :cond_1

    .line 423
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v4

    if-nez v4, :cond_c

    .line 424
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 425
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v4, "android.intent.action.MAIN"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 426
    const-string/jumbo v4, "android.intent.extra.INTENT"

    const/16 v5, 0x8

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 427
    const-string/jumbo v4, "android.intent.extra.TEXT"

    const-string/jumbo v5, "false"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v0}, Lcom/vlingo/midas/settings/ImagePreference;->setIntent(Landroid/content/Intent;)V

    .line 431
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 432
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookAccountCreated(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 433
    const-string/jumbo v4, "facebook_account"

    invoke-static {v4, v8}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 435
    :cond_6
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookLoggedIn()Z

    move-result v1

    .line 439
    .restart local v1    # "loggedIn":Z
    :goto_4
    const-string/jumbo v4, "AdvancedFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "AdvancedFragment::updateSocialUI():loggedIn "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    if-eqz v1, :cond_b

    .line 441
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget v6, Lcom/vlingo/midas/R$string;->settings_logged_in_as:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "facebook_account_name"

    const-string/jumbo v7, ""

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 442
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v5, Lcom/vlingo/midas/R$string;->settings_tap_to_logout_facebook:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 443
    const-string/jumbo v4, "facebook_picture"

    invoke-static {v4}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 444
    .restart local v3    # "thumbImg":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v8}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    .line 445
    if-eqz v3, :cond_1

    .line 446
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v3}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 388
    .end local v1    # "loggedIn":Z
    .end local v3    # "thumbImg":Landroid/graphics/Bitmap;
    :cond_7
    const-string/jumbo v4, "twitter_account"

    invoke-static {v4, v8}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .restart local v1    # "loggedIn":Z
    goto/16 :goto_2

    .line 401
    :cond_8
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4}, Lcom/vlingo/midas/settings/ImagePreference;->isImageViewVisible()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 402
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    sget v5, Lcom/vlingo/midas/R$string;->settings_login_twitter:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 403
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    sget v5, Lcom/vlingo/midas/R$string;->settings_click_to_login_twitter:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 404
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v9}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    goto/16 :goto_3

    .line 408
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "loggedIn":Z
    :cond_9
    const-string/jumbo v4, "twitter_account"

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 413
    .local v2, "pref":Landroid/preference/Preference;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 414
    invoke-virtual {v2, v8}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 415
    invoke-virtual {v2, v10}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    goto/16 :goto_3

    .line 437
    .end local v2    # "pref":Landroid/preference/Preference;
    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_a
    const-string/jumbo v4, "facebook_account"

    invoke-static {v4, v8}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .restart local v1    # "loggedIn":Z
    goto/16 :goto_4

    .line 450
    :cond_b
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4}, Lcom/vlingo/midas/settings/ImagePreference;->isImageViewVisible()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 451
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v5, Lcom/vlingo/midas/R$string;->settings_login_facebook:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 452
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v5, Lcom/vlingo/midas/R$string;->settings_click_to_login_facebook:I

    invoke-virtual {p0, v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 453
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v4, v9}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    goto/16 :goto_1

    .line 457
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "loggedIn":Z
    :cond_c
    const-string/jumbo v4, "facebook_account"

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 458
    .restart local v2    # "pref":Landroid/preference/Preference;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 461
    invoke-virtual {v2, v10}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    goto/16 :goto_1
.end method
