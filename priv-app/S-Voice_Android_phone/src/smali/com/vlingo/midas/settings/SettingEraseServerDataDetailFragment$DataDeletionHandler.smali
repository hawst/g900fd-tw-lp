.class Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;
.super Landroid/os/Handler;
.source "SettingEraseServerDataDetailFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataDeletionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;
    .param p2, "x1"    # Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;-><init>(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 166
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 177
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 179
    :goto_0
    return-void

    .line 168
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->access$100(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 171
    :pswitch_1
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->callback:Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->access$400(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;->onSuccess()V

    goto :goto_0

    .line 174
    :pswitch_2
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->deleteLocalApplicationData()V

    goto :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
