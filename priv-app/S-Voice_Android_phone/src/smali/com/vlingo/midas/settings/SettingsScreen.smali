.class public Lcom/vlingo/midas/settings/SettingsScreen;
.super Lcom/vlingo/midas/settings/VLPreferenceActivity;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;
.implements Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/settings/SettingsScreen$LoginAccountBroadcastReceiver;,
        Lcom/vlingo/midas/settings/SettingsScreen$LengthToastFilter;,
        Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;,
        Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;,
        Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;
    }
.end annotation


# static fields
.field public static final ACTION_SHOW_SOCIAL_SETTING:Ljava/lang/String; = "com.vlingo.client.settings.SOCIAL_SETTING"

.field public static final ACTION_SHOW_VOICE_TALK_SETTING:Ljava/lang/String; = "com.vlingo.client.settings.VOICE_TALK_SETTINGS"

.field public static final DRIVING_MODE_ALARM_NOTIFICATION:Ljava/lang/String; = "driving_mode_alarm_notification"

.field private static final DRIVING_MODE_CHATON_CALL_NOTIFICATION:Ljava/lang/String; = "driving_mode_chaton_call_notification"

.field public static final DRIVING_MODE_INCOMING_CALL_NOTIFICATION:Ljava/lang/String; = "driving_mode_incoming_call_notification"

.field public static final DRIVING_MODE_MESSAGE_NOTIFICATION:Ljava/lang/String; = "driving_mode_message_notification"

.field public static final DRIVING_MODE_SCHEDULE_NOTIFICATION:Ljava/lang/String; = "driving_mode_schedule_notification"

.field private static final ERASE_SERVER_DATA:Ljava/lang/String; = "erase_server_data"

.field public static final EXTRA_SHOW_CHILD_SCREEN:Ljava/lang/String; = "child_screen"

.field private static final KEY_DRIVING_MODE:Ljava/lang/String; = "driving_mode"

.field private static final KEY_TUTORIAL:Ljava/lang/String; = "tutorial_k"

.field private static final KEY_VOICE_INPUT_CONTROL:Ljava/lang/String; = "voice_input_control"

.field private static final KEY_VOICE_INPUT_CONTROL_CATEGORY:Ljava/lang/String; = "voice_input_control_category"

.field private static final KEY_VOICE_WAKEUP_LOCK_SCREEN:Ljava/lang/String; = "wake_up_lock_screen"

.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"

.field public static LOG_TAG:Ljava/lang/String; = null

.field private static final MAX_LENGHT:I = 0x12c

.field private static final MSG_UPDATE_SOCIAL_UI:I = 0x0

.field private static final MY_PLACE:Ljava/lang/String; = "my_place"

.field private static final SET_WAKEUP_DIALOG:I = 0x1

.field private static final SMARTNOTIFICATION:Ljava/lang/String; = "smartNotification"

.field private static final SOFTWARE_UPDATE:Ljava/lang/String; = "software_update"

.field public static final TAG:Ljava/lang/String;

.field private static final VOICEINPUTCONTROL_ALARM:Ljava/lang/String; = "voice_input_control_alarm"

.field private static final VOICEINPUTCONTROL_CAMERA:Ljava/lang/String; = "voice_input_control_camera"

.field private static final VOICEINPUTCONTROL_CHATONV:Ljava/lang/String; = "voice_input_control_chatonv"

.field private static final VOICEINPUTCONTROL_INCOMMING_CALL:Ljava/lang/String; = "voice_input_control_incomming_calls"

.field private static final VOICEINPUTCONTROL_MUSIC:Ljava/lang/String; = "voice_input_control_music"

.field private static final VOICEINPUTCONTROL_RADIO:Ljava/lang/String; = "voice_input_control_radio"

.field public static final VOICE_WAKE_UP_DO_NOT_SHOW_AGAIN_PREF_FILENAME:Ljava/lang/String; = "preferences_voice_wake_up"

.field public static final VOICE_WAKE_UP_IN_SECURED_POP:Ljava/lang/String; = "VOICE_WAKE_UP_IN_SECURED_POP"

.field public static final VOICE_WAKE_UP_POP:Ljava/lang/String; = "VOICE_WAKE_UP_POP"

.field public static final VOICE_WAKE_UP_POP_SETTINGSSCREEN:Ljava/lang/String; = "VOICE_WAKE_UP_POP_SETTINGSSCREEN"

.field public static WakeupSummary:Landroid/preference/Preference;

.field private static addDownKey:Ljava/lang/Boolean;

.field private static alphaPattern:Ljava/util/regex/Pattern;

.field public static isLaunchBySettings:Z

.field private static mTheme:I

.field public static wakeupSecured:Landroid/preference/Preference;


# instance fields
.field private FEATURE_VOICE_UI_6_7_SECURED_LOCK_SCREEN:Z

.field private appLangChanged:Z

.field private bargeInSoFilePath:Ljava/lang/String;

.field drivingModeContentObserver:Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;

.field private final greyDisabled:Z

.field lengthWatcher:Landroid/text/TextWatcher;

.field private loginAccountBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mAutoHapticDialog:Landroid/app/AlertDialog;

.field private mAutoHapticNoPopup:Z

.field private mDensity:F

.field private mDriveMode:Landroid/preference/SwitchPreference;

.field private mEraseServerData:Landroid/preference/Preference;

.field private mMyPlace:Lcom/vlingo/midas/settings/TwoLinePreference;

.field private mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

.field private mSettingClicksForDebug:I

.field private mSettingTimestampForDebug:J

.field private mVoiceInputControl:Landroid/preference/SwitchPreference;

.field private mWakeup:Landroid/preference/CheckBoxPreference;

.field private mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

.field private m_EmailReadback:Landroid/preference/CheckBoxPreference;

.field private m_LocationOn:Landroid/preference/CheckBoxPreference;

.field private m_ProfanityFilter:Landroid/preference/CheckBoxPreference;

.field private m_SMSReadback:Landroid/preference/CheckBoxPreference;

.field private m_SafeReaderReadMessageBody:Landroid/preference/CheckBoxPreference;

.field private m_ShakeToSkip:Landroid/preference/CheckBoxPreference;

.field private m_about:Landroid/preference/Preference;

.field private m_alreadyTTSedWakeupWord:Z

.field private m_autoDial:Landroid/preference/ListPreference;

.field private m_autoPunctuation:Landroid/preference/CheckBoxPreference;

.field private m_auto_start_speaker:Landroid/preference/CheckBoxPreference;

.field private m_auto_start_speakerForZero:Landroid/preference/SwitchPreference;

.field private m_carModeHomeAddress:Landroid/preference/EditTextPreference;

.field private m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

.field private m_carModeStartupTtsPrompt:Landroid/preference/EditTextPreference;

.field private m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

.field private m_headsetMode:Landroid/preference/CheckBoxPreference;

.field private m_languageApplication:Landroid/preference/ListPreference;

.field private m_launchOnCarMode:Landroid/preference/CheckBoxPreference;

.field private m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

.field private m_launchVoicetalkForZero:Landroid/preference/SwitchPreference;

.field private m_listenOverBluetooth:Landroid/preference/CheckBoxPreference;

.field private m_motionSettings:Landroid/preference/CheckBoxPreference;

.field private m_safereaderEmailPollInterval:Landroid/preference/ListPreference;

.field private m_sayWakeUpMode:Landroid/preference/CheckBoxPreference;

.field private m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

.field private m_uuid:Landroid/preference/Preference;

.field private m_voiceCommandHelp:Landroid/preference/Preference;

.field private m_voiceTalkHelp:Landroid/preference/Preference;

.field private m_wakeUpWord:Landroid/preference/CheckBoxPreference;

.field private m_wake_up_engine:Landroid/preference/ListPreference;

.field private m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

.field private m_webSearchEngine:Landroid/preference/ListPreference;

.field private maxLengToast:Landroid/widget/Toast;

.field private mhandler:Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;

.field private scale:F

.field private smartNotification:Landroid/preference/CheckBoxPreference;

.field private software_update:Landroid/preference/Preference;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 118
    const-string/jumbo v0, "SettingsScreen"

    sput-object v0, Lcom/vlingo/midas/settings/SettingsScreen;->LOG_TAG:Ljava/lang/String;

    .line 119
    const-class v0, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/settings/SettingsScreen;->TAG:Ljava/lang/String;

    .line 154
    const-string/jumbo v0, ".*\\p{Alnum}.*"

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/settings/SettingsScreen;->alphaPattern:Ljava/util/regex/Pattern;

    .line 214
    sget v0, Lcom/vlingo/midas/R$style;->CustomBlackNoActionBar:I

    sput v0, Lcom/vlingo/midas/settings/SettingsScreen;->mTheme:I

    .line 227
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/settings/SettingsScreen;->addDownKey:Ljava/lang/Boolean;

    .line 232
    sput-boolean v2, Lcom/vlingo/midas/settings/SettingsScreen;->isLaunchBySettings:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;-><init>()V

    .line 210
    iput-boolean v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->appLangChanged:Z

    .line 211
    iput-boolean v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->greyDisabled:Z

    .line 215
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mDensity:F

    .line 218
    const-string/jumbo v0, "/system/lib/libsasr-jni.so"

    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->bargeInSoFilePath:Ljava/lang/String;

    .line 222
    iput-boolean v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_alreadyTTSedWakeupWord:Z

    .line 230
    iput-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticDialog:Landroid/app/AlertDialog;

    .line 234
    iput-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->drivingModeContentObserver:Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;

    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->FEATURE_VOICE_UI_6_7_SECURED_LOCK_SCREEN:Z

    .line 242
    iput v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mSettingClicksForDebug:I

    .line 243
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mSettingTimestampForDebug:J

    .line 2315
    new-instance v0, Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mhandler:Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;

    .line 2359
    new-instance v0, Lcom/vlingo/midas/settings/SettingsScreen$31;

    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/SettingsScreen$31;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->lengthWatcher:Landroid/text/TextWatcher;

    .line 2397
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/SwitchPreference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/SwitchPreference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mVoiceInputControl:Landroid/preference/SwitchPreference;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->maxLengToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vlingo/midas/settings/SettingsScreen;)Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mhandler:Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeup:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_languageApplication:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/settings/SettingsScreen;)J
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget-wide v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mSettingTimestampForDebug:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/vlingo/midas/settings/SettingsScreen;J)J
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;
    .param p1, "x1"    # J

    .prologue
    .line 116
    iput-wide p1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mSettingTimestampForDebug:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/vlingo/midas/settings/SettingsScreen;)I
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mSettingClicksForDebug:I

    return v0
.end method

.method static synthetic access$502(Lcom/vlingo/midas/settings/SettingsScreen;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;
    .param p1, "x1"    # I

    .prologue
    .line 116
    iput p1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mSettingClicksForDebug:I

    return p1
.end method

.method static synthetic access$508(Lcom/vlingo/midas/settings/SettingsScreen;)I
    .locals 2
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mSettingClicksForDebug:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mSettingClicksForDebug:I

    return v0
.end method

.method static synthetic access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vlingo/midas/settings/SettingsScreen;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/vlingo/midas/settings/SettingsScreen;->containsNoAlphaNumeric(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private checkSystemMotionSettings()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1818
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "motion_engine"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 1820
    .local v1, "system_MotionEnabled":I
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "motion_double_tap"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1823
    .local v0, "system_DoubleTapEnabled":I
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 1824
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_motionSettings:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 1830
    :goto_0
    return-void

    .line 1828
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_motionSettings:Landroid/preference/CheckBoxPreference;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private containsNoAlphaNumeric(Ljava/lang/String;)Z
    .locals 2
    .param p1, "promptString"    # Ljava/lang/String;

    .prologue
    .line 1814
    sget-object v0, Lcom/vlingo/midas/settings/SettingsScreen;->alphaPattern:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initPreference()V
    .locals 29

    .prologue
    .line 445
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getActionBar()Landroid/app/ActionBar;

    move-result-object v25

    .line 446
    .local v25, "titlebar":Landroid/app/ActionBar;
    if-eqz v25, :cond_0

    .line 447
    sget v26, Lcom/vlingo/midas/R$string;->app_settings:I

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 450
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v26

    if-nez v26, :cond_d

    .line 452
    const-string/jumbo v26, "voice_wakeup_in_secured_lock"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Landroid/preference/CheckBoxPreference;

    .line 453
    .local v11, "mWakeupinSecureLock":Landroid/preference/CheckBoxPreference;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v26

    if-nez v26, :cond_1

    .line 454
    const-string/jumbo v26, "car_wakeup"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    .line 455
    .local v7, "generalCategory":Landroid/preference/PreferenceCategory;
    if-eqz v7, :cond_1

    if-eqz v11, :cond_1

    .line 456
    invoke-virtual {v7, v11}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 461
    .end local v7    # "generalCategory":Landroid/preference/PreferenceCategory;
    :cond_1
    const-string/jumbo v26, "voice_input_control"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/SwitchPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->mVoiceInputControl:Landroid/preference/SwitchPreference;

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mVoiceInputControl:Landroid/preference/SwitchPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2

    .line 463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mVoiceInputControl:Landroid/preference/SwitchPreference;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 464
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v26

    const-string/jumbo v27, "voice_input_control_category"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 465
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mVoiceInputControl:Landroid/preference/SwitchPreference;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 468
    :cond_2
    const-string/jumbo v26, "my_place"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->mMyPlace:Lcom/vlingo/midas/settings/TwoLinePreference;

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mMyPlace:Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_3

    .line 470
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v26

    const-string/jumbo v27, "my_place_category"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 471
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mMyPlace:Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 475
    :cond_3
    const-string/jumbo v26, "driving_mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/SwitchPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;

    .line 476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_4

    .line 477
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v26

    const-string/jumbo v27, "driving_mode_category"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 478
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 481
    :cond_4
    sget-object v26, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v26

    if-eqz v26, :cond_5

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasDialing()Z

    move-result v26

    if-eqz v26, :cond_5

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasCallEnabled()Z

    move-result v26

    if-eqz v26, :cond_5

    const-string/jumbo v26, "no_receiver_in_call"

    invoke-static/range {v26 .. v26}, Lcom/vlingo/midas/settings/MidasSettings;->hasFeature(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 485
    :cond_5
    const-string/jumbo v26, "call"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    if-eqz v26, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v26

    if-eqz v26, :cond_6

    .line 486
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v26

    const-string/jumbo v27, "call"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 488
    :cond_6
    const-string/jumbo v26, "social_settings_category"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v20

    check-cast v20, Landroid/preference/PreferenceCategory;

    .line 489
    .local v20, "socialSettingsCategory":Landroid/preference/PreferenceCategory;
    const-string/jumbo v26, "car_auto_start_speakerphone"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/settings/TwoLineCheckBoxPreference;

    .line 490
    .local v4, "autoStartSpeakerPreference":Lcom/vlingo/midas/settings/TwoLineCheckBoxPreference;
    if-eqz v20, :cond_7

    if-eqz v4, :cond_7

    .line 491
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 494
    .end local v4    # "autoStartSpeakerPreference":Lcom/vlingo/midas/settings/TwoLineCheckBoxPreference;
    .end local v20    # "socialSettingsCategory":Landroid/preference/PreferenceCategory;
    :cond_7
    sget-object v26, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MESSAGING:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->isEnabled()Z

    move-result v26

    if-eqz v26, :cond_8

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasMessaging()Z

    move-result v26

    if-nez v26, :cond_9

    .line 495
    :cond_8
    const-string/jumbo v26, "car_safereader_read_message"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    if-eqz v26, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v26

    if-eqz v26, :cond_9

    .line 496
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v26

    const-string/jumbo v27, "car_safereader_read_message"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 500
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getIntent()Landroid/content/Intent;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 501
    .local v3, "action":Ljava/lang/String;
    if-eqz v3, :cond_a

    .line 502
    const-string/jumbo v26, "com.vlingo.client.settings.SOCIAL_SETTING"

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_10

    .line 503
    const-string/jumbo v26, "social_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    check-cast v14, Landroid/preference/PreferenceScreen;

    .line 504
    .local v14, "screen":Landroid/preference/PreferenceScreen;
    invoke-virtual {v14}, Landroid/preference/PreferenceScreen;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 505
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/vlingo/midas/settings/SettingsScreen;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 512
    .end local v14    # "screen":Landroid/preference/PreferenceScreen;
    :cond_a
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getIntent()Landroid/content/Intent;

    move-result-object v26

    const-string/jumbo v27, "child_screen"

    invoke-virtual/range {v26 .. v27}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 514
    .local v21, "subscreen":Ljava/lang/String;
    if-eqz v21, :cond_b

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v26

    if-lez v26, :cond_b

    .line 515
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    check-cast v14, Landroid/preference/PreferenceScreen;

    .line 516
    .restart local v14    # "screen":Landroid/preference/PreferenceScreen;
    invoke-virtual {v14}, Landroid/preference/PreferenceScreen;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 517
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/vlingo/midas/settings/SettingsScreen;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 522
    .end local v14    # "screen":Landroid/preference/PreferenceScreen;
    :cond_b
    const-string/jumbo v6, "call"

    .line 523
    .local v6, "categoryName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceCategory;

    .line 524
    .local v5, "category":Landroid/preference/PreferenceCategory;
    if-eqz v5, :cond_c

    .line 525
    const-string/jumbo v13, "car_auto_start_speakerphone"

    .line 526
    .local v13, "preferenceName":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v26

    if-eqz v26, :cond_11

    .line 527
    invoke-virtual {v5, v13}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/SwitchPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speakerForZero:Landroid/preference/SwitchPreference;

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speakerForZero:Landroid/preference/SwitchPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_c

    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speakerForZero:Landroid/preference/SwitchPreference;

    move-object/from16 v26, v0

    const-string/jumbo v27, "car_auto_start_speakerphone"

    const/16 v28, 0x1

    invoke-static/range {v27 .. v28}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 539
    .end local v13    # "preferenceName":Ljava/lang/String;
    :cond_c
    :goto_1
    new-instance v26, Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 543
    .end local v3    # "action":Ljava/lang/String;
    .end local v5    # "category":Landroid/preference/PreferenceCategory;
    .end local v6    # "categoryName":Ljava/lang/String;
    .end local v11    # "mWakeupinSecureLock":Landroid/preference/CheckBoxPreference;
    .end local v21    # "subscreen":Ljava/lang/String;
    :cond_d
    const-string/jumbo v26, "language"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/ListPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_languageApplication:Landroid/preference/ListPreference;

    .line 545
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->initializeUnsupportedLocales()V

    .line 546
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguages()[Ljava/lang/CharSequence;

    move-result-object v23

    .line 547
    .local v23, "supportedLanguages":[Ljava/lang/CharSequence;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguageDescriptions()[Ljava/lang/CharSequence;

    move-result-object v22

    .line 548
    .local v22, "supportedLanguageDescriptions":[Ljava/lang/CharSequence;
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 549
    .local v18, "settableLanguages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 550
    .local v17, "settableLanguageDescriptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string/jumbo v26, "show_all_languages"

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v19

    .line 551
    .local v19, "showAllLanguages":Z
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v8, v0, :cond_12

    .line 552
    aget-object v26, v23, v8

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 553
    .local v9, "language":Ljava/lang/String;
    invoke-static {v9}, Lcom/vlingo/midas/settings/SettingsScreen;->phoneSupportsLanguage(Ljava/lang/String;)Z

    move-result v26

    if-nez v26, :cond_e

    if-eqz v19, :cond_f

    .line 554
    :cond_e
    aget-object v26, v23, v8

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 555
    aget-object v26, v22, v8

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 551
    :cond_f
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 506
    .end local v8    # "i":I
    .end local v9    # "language":Ljava/lang/String;
    .end local v17    # "settableLanguageDescriptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v18    # "settableLanguages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19    # "showAllLanguages":Z
    .end local v22    # "supportedLanguageDescriptions":[Ljava/lang/CharSequence;
    .end local v23    # "supportedLanguages":[Ljava/lang/CharSequence;
    .restart local v3    # "action":Ljava/lang/String;
    .restart local v11    # "mWakeupinSecureLock":Landroid/preference/CheckBoxPreference;
    :cond_10
    const-string/jumbo v26, "com.vlingo.client.settings.VOICE_TALK_SETTINGS"

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 507
    const-string/jumbo v26, "vlingo_car_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    check-cast v14, Landroid/preference/PreferenceScreen;

    .line 508
    .restart local v14    # "screen":Landroid/preference/PreferenceScreen;
    invoke-virtual {v14}, Landroid/preference/PreferenceScreen;->getTitle()Ljava/lang/CharSequence;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 509
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/vlingo/midas/settings/SettingsScreen;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    goto/16 :goto_0

    .line 532
    .end local v14    # "screen":Landroid/preference/PreferenceScreen;
    .restart local v5    # "category":Landroid/preference/PreferenceCategory;
    .restart local v6    # "categoryName":Ljava/lang/String;
    .restart local v13    # "preferenceName":Ljava/lang/String;
    .restart local v21    # "subscreen":Ljava/lang/String;
    :cond_11
    invoke-virtual {v5, v13}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speaker:Landroid/preference/CheckBoxPreference;

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speaker:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_c

    .line 534
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speaker:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    const-string/jumbo v27, "car_auto_start_speakerphone"

    const/16 v28, 0x1

    invoke-static/range {v27 .. v28}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_1

    .line 561
    .end local v3    # "action":Ljava/lang/String;
    .end local v5    # "category":Landroid/preference/PreferenceCategory;
    .end local v6    # "categoryName":Ljava/lang/String;
    .end local v11    # "mWakeupinSecureLock":Landroid/preference/CheckBoxPreference;
    .end local v13    # "preferenceName":Ljava/lang/String;
    .end local v21    # "subscreen":Ljava/lang/String;
    .restart local v8    # "i":I
    .restart local v17    # "settableLanguageDescriptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v18    # "settableLanguages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v19    # "showAllLanguages":Z
    .restart local v22    # "supportedLanguageDescriptions":[Ljava/lang/CharSequence;
    .restart local v23    # "supportedLanguages":[Ljava/lang/CharSequence;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_languageApplication:Landroid/preference/ListPreference;

    move-object/from16 v27, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/CharSequence;

    move-object/from16 v26, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v26

    check-cast v26, [Ljava/lang/CharSequence;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_languageApplication:Landroid/preference/ListPreference;

    move-object/from16 v27, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/CharSequence;

    move-object/from16 v26, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v26

    check-cast v26, [Ljava/lang/CharSequence;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 600
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_languageApplication:Landroid/preference/ListPreference;

    move-object/from16 v26, v0

    new-instance v27, Lcom/vlingo/midas/settings/SettingsScreen$2;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$2;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/ListPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 633
    new-instance v10, Landroid/content/Intent;

    const-class v26, Lcom/vlingo/midas/settings/SettingsLanguageScreen;

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 634
    .local v10, "languageIntent":Landroid/content/Intent;
    const-string/jumbo v26, "is_from_svoice"

    const/16 v27, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 635
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_languageApplication:Landroid/preference/ListPreference;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Landroid/preference/ListPreference;->setIntent(Landroid/content/Intent;)V

    .line 646
    const-string/jumbo v26, "profanity_filter"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_ProfanityFilter:Landroid/preference/CheckBoxPreference;

    .line 647
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_ProfanityFilter:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_13

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v26

    if-eqz v26, :cond_13

    .line 648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_ProfanityFilter:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    new-instance v27, Lcom/vlingo/midas/settings/SettingsScreen$3;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$3;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 672
    :cond_13
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v26

    if-eqz v26, :cond_14

    .line 673
    const-string/jumbo v26, "software_update"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->software_update:Landroid/preference/Preference;

    .line 674
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->software_update:Landroid/preference/Preference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_14

    .line 675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->software_update:Landroid/preference/Preference;

    move-object/from16 v26, v0

    new-instance v27, Lcom/vlingo/midas/settings/SettingsScreen$4;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$4;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 684
    sget-boolean v26, Lcom/samsung/samsungapps/UpdateManager;->availableUpdate:Z

    if-nez v26, :cond_14

    .line 685
    const-string/jumbo v26, "general_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    .line 686
    .restart local v7    # "generalCategory":Landroid/preference/PreferenceCategory;
    if-eqz v7, :cond_14

    .line 687
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->software_update:Landroid/preference/Preference;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 694
    .end local v7    # "generalCategory":Landroid/preference/PreferenceCategory;
    :cond_14
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v26

    if-eqz v26, :cond_15

    .line 695
    const-string/jumbo v26, "erase_server_data"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->mEraseServerData:Landroid/preference/Preference;

    .line 696
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mEraseServerData:Landroid/preference/Preference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_15

    .line 697
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mEraseServerData:Landroid/preference/Preference;

    move-object/from16 v26, v0

    new-instance v27, Landroid/content/Intent;

    const-class v28, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 702
    :cond_15
    const-string/jumbo v26, "help_about"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_about:Landroid/preference/Preference;

    .line 703
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_about:Landroid/preference/Preference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_16

    .line 704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_about:Landroid/preference/Preference;

    move-object/from16 v26, v0

    new-instance v27, Landroid/content/Intent;

    const-class v28, Lcom/vlingo/midas/help/AboutScreen;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 723
    :cond_16
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v26

    if-nez v26, :cond_26

    .line 728
    const-string/jumbo v13, "launch_voicetalk"

    .line 729
    .restart local v13    # "preferenceName":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v26

    if-eqz v26, :cond_28

    .line 735
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v26

    const-string/jumbo v27, "double_tab_launch"

    const/16 v28, -0x1

    invoke-static/range {v26 .. v28}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v24

    .line 737
    .local v24, "sysConfig":I
    const/16 v26, 0x1

    move/from16 v0, v24

    move/from16 v1, v26

    if-ne v0, v1, :cond_2a

    .line 738
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v26

    if-eqz v26, :cond_29

    .line 750
    :cond_17
    :goto_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isHomeKeyEnabled()Z

    move-result v26

    if-nez v26, :cond_18

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isUCameraModel()Z

    move-result v26

    if-nez v26, :cond_18

    .line 751
    const-string/jumbo v26, "general_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    .line 752
    .restart local v7    # "generalCategory":Landroid/preference/PreferenceCategory;
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    move-result v26

    if-eqz v26, :cond_18

    .line 755
    const/16 v26, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    .line 756
    const/16 v26, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_launchVoicetalkForZero:Landroid/preference/SwitchPreference;

    .line 763
    .end local v7    # "generalCategory":Landroid/preference/PreferenceCategory;
    :cond_18
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v26

    if-eqz v26, :cond_19

    .line 764
    const-string/jumbo v26, "smartNotification"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->smartNotification:Landroid/preference/CheckBoxPreference;

    .line 765
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->smartNotification:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_19

    .line 766
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->smartNotification:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    new-instance v27, Lcom/vlingo/midas/settings/SettingsScreen$5;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$5;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 779
    :cond_19
    const-string/jumbo v26, "web_search_engine"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/ListPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_webSearchEngine:Landroid/preference/ListPreference;

    .line 780
    const-string/jumbo v26, "auto_dial"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/ListPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_autoDial:Landroid/preference/ListPreference;

    .line 781
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speaker:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    if-nez v26, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speakerForZero:Landroid/preference/SwitchPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1b

    .line 782
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_autoDial:Landroid/preference/ListPreference;

    move-object/from16 v26, v0

    sget v27, Lcom/vlingo/midas/R$layout;->round_more_icon:I

    invoke-virtual/range {v26 .. v27}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 784
    :cond_1b
    const-string/jumbo v26, "safereader_email_poll_interval"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/ListPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_safereaderEmailPollInterval:Landroid/preference/ListPreference;

    .line 785
    const-string/jumbo v26, "shake_to_skip"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_ShakeToSkip:Landroid/preference/CheckBoxPreference;

    .line 786
    const-string/jumbo v26, "tts_carmode_startup_prompt"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeStartupTtsPrompt:Landroid/preference/EditTextPreference;

    .line 788
    const-string/jumbo v26, "car_nav_home_address"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    .line 789
    const-string/jumbo v26, "car_nav_office_address"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    .line 791
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v26

    sget v27, Lcom/vlingo/midas/R$string;->character_max_length_reached:I

    const/16 v28, 0x0

    invoke-static/range {v26 .. v28}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->maxLengToast:Landroid/widget/Toast;

    .line 792
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v26

    if-nez v26, :cond_1d

    .line 793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1c

    .line 794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    sget v27, Lcom/vlingo/midas/R$layout;->round_more_icon:I

    invoke-virtual/range {v26 .. v27}, Landroid/preference/EditTextPreference;->setWidgetLayoutResource(I)V

    .line 796
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1d

    .line 797
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    sget v27, Lcom/vlingo/midas/R$layout;->round_more_icon:I

    invoke-virtual/range {v26 .. v27}, Landroid/preference/EditTextPreference;->setWidgetLayoutResource(I)V

    .line 801
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1e

    .line 802
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    new-instance v27, Lcom/vlingo/midas/settings/SettingsScreen$6;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$6;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/EditTextPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 906
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1f

    .line 907
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    new-instance v27, Lcom/vlingo/midas/settings/SettingsScreen$7;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$7;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/EditTextPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 986
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_20

    .line 987
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v26

    const/16 v27, 0x4001

    invoke-virtual/range {v26 .. v27}, Landroid/widget/EditText;->setInputType(I)V

    .line 989
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_21

    .line 990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v26

    const/16 v27, 0x4001

    invoke-virtual/range {v26 .. v27}, Landroid/widget/EditText;->setInputType(I)V

    .line 994
    :cond_21
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->isKoreanNavigationenable()Z

    move-result v26

    if-nez v26, :cond_22

    .line 995
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v26

    if-eqz v26, :cond_2b

    .line 996
    const-string/jumbo v26, "social_settings_category"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v12

    check-cast v12, Landroid/preference/PreferenceCategory;

    .line 997
    .local v12, "naviCategory":Landroid/preference/PreferenceCategory;
    if-eqz v12, :cond_22

    .line 998
    const-string/jumbo v26, "car_nav_office_address"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v12, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 1008
    .end local v12    # "naviCategory":Landroid/preference/PreferenceCategory;
    :cond_22
    :goto_5
    const-string/jumbo v13, "car_word_spotter_enabled"

    .line 1009
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_wakeUpWord:Landroid/preference/CheckBoxPreference;

    .line 1011
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v26

    move-object/from16 v0, v26

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v16, v0

    .line 1012
    .local v16, "screenWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v26

    move-object/from16 v0, v26

    iget v15, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1014
    .local v15, "screenHeight":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->FEATURE_VOICE_UI_6_7_SECURED_LOCK_SCREEN:Z

    move/from16 v26, v0

    if-eqz v26, :cond_2c

    .line 1015
    const-string/jumbo v26, "wake_up_lock_screen"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    .line 1016
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_23

    .line 1017
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v26, v0

    sget v27, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/midas/settings/TwoLinePreference;->setTitle(I)V

    .line 1018
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v26

    if-nez v26, :cond_23

    .line 1019
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v26, v0

    sget v27, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff:I

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    .line 1026
    :cond_23
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isH_Device()Z

    move-result v26

    if-nez v26, :cond_24

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v26

    if-nez v26, :cond_24

    .line 1027
    const-string/jumbo v26, "car_wakeup"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v12

    check-cast v12, Landroid/preference/PreferenceCategory;

    .line 1028
    .restart local v12    # "naviCategory":Landroid/preference/PreferenceCategory;
    const-string/jumbo v26, "wake_up_lock_screen"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v12, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 1090
    .end local v12    # "naviCategory":Landroid/preference/PreferenceCategory;
    :cond_24
    :goto_6
    const-string/jumbo v6, "car_wakeup"

    .line 1091
    .restart local v6    # "categoryName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceCategory;

    .line 1092
    .restart local v5    # "category":Landroid/preference/PreferenceCategory;
    const-string/jumbo v6, "headsetmode"

    .line 1093
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .end local v5    # "category":Landroid/preference/PreferenceCategory;
    check-cast v5, Landroid/preference/PreferenceCategory;

    .line 1094
    .restart local v5    # "category":Landroid/preference/PreferenceCategory;
    const-string/jumbo v6, "social_settings_category"

    .line 1095
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .end local v5    # "category":Landroid/preference/PreferenceCategory;
    check-cast v5, Landroid/preference/PreferenceCategory;

    .line 1096
    .restart local v5    # "category":Landroid/preference/PreferenceCategory;
    const-string/jumbo v6, "car_nav_settings"

    .line 1097
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .end local v5    # "category":Landroid/preference/PreferenceCategory;
    check-cast v5, Landroid/preference/PreferenceCategory;

    .line 1098
    .restart local v5    # "category":Landroid/preference/PreferenceCategory;
    const-string/jumbo v26, "listen_over_bluetooth"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_listenOverBluetooth:Landroid/preference/CheckBoxPreference;

    .line 1099
    const-string/jumbo v26, "auto_punctuation"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_autoPunctuation:Landroid/preference/CheckBoxPreference;

    .line 1100
    const-string/jumbo v26, "headset_mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_headsetMode:Landroid/preference/CheckBoxPreference;

    .line 1101
    const-string/jumbo v26, "say_wake_up_command"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_sayWakeUpMode:Landroid/preference/CheckBoxPreference;

    .line 1102
    const-string/jumbo v26, "car_word_spotter_motion"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_motionSettings:Landroid/preference/CheckBoxPreference;

    .line 1104
    const-string/jumbo v13, "help_voice_talk"

    .line 1105
    const-string/jumbo v26, "general_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    .line 1106
    .restart local v7    # "generalCategory":Landroid/preference/PreferenceCategory;
    invoke-virtual {v7, v13}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_voiceTalkHelp:Landroid/preference/Preference;

    .line 1112
    const-string/jumbo v26, "uuid"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_uuid:Landroid/preference/Preference;

    .line 1114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_uuid:Landroid/preference/Preference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_25

    .line 1115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_uuid:Landroid/preference/Preference;

    move-object/from16 v26, v0

    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getUUIDDeviceID()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_uuid:Landroid/preference/Preference;

    move-object/from16 v26, v0

    new-instance v27, Lcom/vlingo/midas/settings/SettingsScreen$10;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$10;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1129
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_voiceTalkHelp:Landroid/preference/Preference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_26

    .line 1130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_voiceTalkHelp:Landroid/preference/Preference;

    move-object/from16 v26, v0

    new-instance v27, Landroid/content/Intent;

    const-class v28, Lcom/vlingo/midas/help/HelpScreen;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1134
    .end local v5    # "category":Landroid/preference/PreferenceCategory;
    .end local v6    # "categoryName":Ljava/lang/String;
    .end local v7    # "generalCategory":Landroid/preference/PreferenceCategory;
    .end local v13    # "preferenceName":Ljava/lang/String;
    .end local v15    # "screenHeight":I
    .end local v16    # "screenWidth":I
    .end local v24    # "sysConfig":I
    :cond_26
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateSettingUI()V

    .line 1135
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateSocialUI()V

    .line 1137
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v26

    const-string/jumbo v27, "settings"

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->helpPageViewed(Ljava/lang/String;)V

    .line 1139
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->appLangChanged:Z

    .line 1141
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v26

    if-eqz v26, :cond_27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v26, v0

    if-eqz v26, :cond_27

    .line 1142
    const-string/jumbo v26, "preferences_voice_wake_up_time"

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v26

    if-nez v26, :cond_31

    .line 1143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v26, v0

    sget v27, Lcom/vlingo/midas/R$string;->on:I

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    .line 1148
    :cond_27
    :goto_7
    return-void

    .line 732
    .restart local v13    # "preferenceName":Ljava/lang/String;
    :cond_28
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    goto/16 :goto_3

    .line 741
    .restart local v24    # "sysConfig":I
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_4

    .line 744
    :cond_2a
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v26

    if-nez v26, :cond_17

    .line 747
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_4

    .line 1001
    :cond_2b
    const-string/jumbo v26, "car_nav_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v12

    check-cast v12, Landroid/preference/PreferenceCategory;

    .line 1002
    .restart local v12    # "naviCategory":Landroid/preference/PreferenceCategory;
    if-eqz v12, :cond_22

    .line 1003
    const-string/jumbo v26, "car_nav_office_address"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v12, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_5

    .line 1031
    .end local v12    # "naviCategory":Landroid/preference/PreferenceCategory;
    .restart local v15    # "screenHeight":I
    .restart local v16    # "screenWidth":I
    :cond_2c
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isH_Device()Z

    move-result v26

    if-eqz v26, :cond_2d

    .line 1032
    const-string/jumbo v26, "wake_up_lock_screen"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    .line 1033
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    sget v27, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    .line 1034
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    sget v27, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff:I

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    .line 1035
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 1046
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    new-instance v27, Lcom/vlingo/midas/settings/SettingsScreen$8;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$8;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1081
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    new-instance v27, Lcom/vlingo/midas/settings/SettingsScreen$9;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$9;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_6

    .line 1037
    :cond_2d
    const/16 v26, 0x320

    move/from16 v0, v16

    move/from16 v1, v26

    if-ne v0, v1, :cond_2e

    const/16 v26, 0x4d0

    move/from16 v0, v26

    if-eq v15, v0, :cond_2f

    :cond_2e
    const/16 v26, 0x500

    move/from16 v0, v16

    move/from16 v1, v26

    if-ne v0, v1, :cond_30

    const/16 v26, 0x2f0

    move/from16 v0, v26

    if-ne v15, v0, :cond_30

    .line 1038
    :cond_2f
    const-string/jumbo v26, "wake_up_lock_screen"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    .line 1039
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    sget v27, Lcom/vlingo/midas/R$string;->setting_wakeup_unlock_screen_non_motion:I

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    .line 1040
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    goto :goto_8

    .line 1043
    :cond_30
    const-string/jumbo v26, "wake_up_lock_screen"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    goto/16 :goto_8

    .line 1145
    .end local v13    # "preferenceName":Ljava/lang/String;
    .end local v15    # "screenHeight":I
    .end local v16    # "screenWidth":I
    .end local v24    # "sysConfig":I
    :cond_31
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v26, v0

    sget v27, Lcom/vlingo/midas/R$string;->off:I

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    goto/16 :goto_7
.end method

.method private initializeUnsupportedLocales()V
    .locals 0

    .prologue
    .line 2057
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->initializeUnsupportedLocales()V

    .line 2058
    return-void
.end method

.method private isBargeInAvailable()Z
    .locals 2

    .prologue
    .line 2280
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->bargeInSoFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2281
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2282
    const/4 v1, 0x0

    .line 2284
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private launchSetWakeupDialog()V
    .locals 1

    .prologue
    .line 1631
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/SettingsScreen;->showDialog(I)V

    .line 1632
    return-void
.end method

.method private static phoneSupportsLanguage(Ljava/lang/String;)Z
    .locals 1
    .param p0, "vlingoLocale"    # Ljava/lang/String;

    .prologue
    .line 250
    invoke-static {p0}, Lcom/vlingo/midas/settings/MidasSettings;->phoneSupportsLanguage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private saveString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "saveText"    # Ljava/lang/String;

    .prologue
    .line 1635
    const/4 v0, 0x0

    .line 1637
    .local v0, "mFileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    .line 1638
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 1639
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1644
    :goto_0
    return-void

    .line 1640
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private updateDrivingModeControl(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 2275
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "driving_mode_on"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2276
    return-void
.end method

.method private updateState()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2252
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mVoiceInputControl:Landroid/preference/SwitchPreference;

    if-eqz v1, :cond_0

    .line 2253
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mVoiceInputControl:Landroid/preference/SwitchPreference;

    iget-boolean v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticNoPopup:Z

    invoke-virtual {v1, v2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 2255
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;

    if-eqz v1, :cond_2

    .line 2256
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "driving_mode_on"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {v1, v0}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 2259
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateVariableForDialog()V

    .line 2260
    return-void
.end method

.method private updateVariableForDialog()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2265
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2266
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string/jumbo v3, "driving_mode_on"

    invoke-static {v0, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 2267
    .local v1, "keyDM":I
    const-string/jumbo v3, "voice_input_control"

    invoke-static {v0, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 2269
    .local v2, "keyVC":I
    if-ne v1, v5, :cond_0

    if-ne v2, v5, :cond_0

    .line 2270
    iput-boolean v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticNoPopup:Z

    .line 2271
    :cond_0
    return-void
.end method


# virtual methods
.method public isAllDrivingModeOptionsDisabled()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 2213
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "driving_mode_incoming_call_notification"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 2214
    .local v1, "call":I
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "driving_mode_message_notification"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 2215
    .local v3, "message":I
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "driving_mode_alarm_notification"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2216
    .local v0, "alarm":I
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "driving_mode_schedule_notification"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 2217
    .local v4, "schedule":I
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "driving_mode_chaton_call_notification"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 2218
    .local v2, "chatonv":I
    if-nez v1, :cond_0

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    if-nez v4, :cond_0

    if-nez v2, :cond_0

    .line 2219
    const/4 v5, 0x1

    .line 2221
    :cond_0
    return v5
.end method

.method public isAllOptionDisabled()Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 2198
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_incomming_calls"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 2199
    .local v1, "call":I
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_alarm"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2200
    .local v0, "alarm":I
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_camera"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 2201
    .local v2, "camera":I
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_music"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 2202
    .local v4, "music":I
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_radio"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 2203
    .local v5, "radio":I
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_chatonv"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 2204
    .local v3, "chatonv":I
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    if-nez v2, :cond_0

    if-nez v4, :cond_0

    if-nez v5, :cond_0

    if-nez v3, :cond_0

    .line 2205
    const/4 v6, 0x1

    .line 2207
    :cond_0
    return v6
.end method

.method public isKoreanNavigationenable()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2226
    const/4 v1, 0x0

    .line 2227
    .local v1, "versionName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2229
    .local v0, "parts":[Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string/jumbo v5, "com.skt.skaf.l001mtm091"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget-object v1, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 2230
    const-string/jumbo v4, "\\."

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2235
    :goto_0
    const-string/jumbo v4, "language"

    const-string/jumbo v5, "en-US"

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "ko-KR"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string/jumbo v4, "com.skt.skaf.l001mtm091"

    invoke-static {v4, v2}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    aget-object v4, v0, v3

    const-string/jumbo v5, "3"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const-string/jumbo v4, "kt.navi"

    invoke-static {v4, v2}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_1

    const-string/jumbo v4, "com.mnsoft.lgunavi"

    invoke-static {v4, v2}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2241
    :cond_1
    :goto_1
    return v2

    :cond_2
    move v2, v3

    goto :goto_1

    .line 2231
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public onChangeAccountInfo()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2290
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mhandler:Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;->removeMessages(I)V

    .line 2291
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mhandler:Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;->sendEmptyMessage(I)Z

    .line 2292
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1177
    invoke-super {p0, p1}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1178
    sget v2, Lcom/vlingo/midas/R$id;->left_container:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/settings/SettingsScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1179
    .local v0, "leftContainer":Landroid/widget/RelativeLayout;
    sget v2, Lcom/vlingo/midas/R$id;->right_container:I

    invoke-virtual {p0, v2}, Lcom/vlingo/midas/settings/SettingsScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 1180
    .local v1, "rightContainer":Landroid/widget/RelativeLayout;
    if-eqz p1, :cond_1

    .line 1181
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1182
    if-eqz v0, :cond_0

    .line 1183
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1184
    :cond_0
    if-eqz v1, :cond_1

    .line 1185
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1193
    :cond_1
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 1196
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1197
    return-void

    .line 1186
    :cond_2
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1187
    if-eqz v0, :cond_3

    .line 1188
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1189
    :cond_3
    if-eqz v1, :cond_1

    .line 1190
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 278
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v13

    if-nez v13, :cond_0

    .line 279
    new-instance v4, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const-class v14, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v4, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 280
    .local v4, "i":Landroid/content/Intent;
    const/high16 v13, 0x10000000

    invoke-virtual {v4, v13}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 281
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/vlingo/midas/settings/SettingsScreen;->startActivity(Landroid/content/Intent;)V

    .line 282
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->finish()V

    .line 287
    .end local v4    # "i":Landroid/content/Intent;
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 288
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->setRequestedOrientation(I)V

    .line 289
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getWindow()Landroid/view/Window;

    move-result-object v13

    const/16 v14, 0x400

    const/16 v15, 0x400

    invoke-virtual {v13, v14, v15}, Landroid/view/Window;->setFlags(II)V

    .line 292
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget v13, v13, Landroid/content/res/Configuration;->fontScale:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/vlingo/midas/settings/SettingsScreen;->scale:F

    .line 295
    new-instance v7, Landroid/util/DisplayMetrics;

    invoke-direct {v7}, Landroid/util/DisplayMetrics;-><init>()V

    .line 296
    .local v7, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v13

    invoke-interface {v13}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v13

    invoke-virtual {v13, v7}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 297
    iget v13, v7, Landroid/util/DisplayMetrics;->density:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/vlingo/midas/settings/SettingsScreen;->mDensity:F

    .line 299
    iget v13, v7, Landroid/util/DisplayMetrics;->density:F

    float-to-double v13, v13

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    cmpl-double v13, v13, v15

    if-nez v13, :cond_2

    iget v13, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v14, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v13, v14

    const/16 v14, 0x7f0

    if-ne v13, v14, :cond_2

    iget v13, v7, Landroid/util/DisplayMetrics;->xdpi:F

    const v14, 0x4315d32c

    invoke-static {v13, v14}, Ljava/lang/Float;->compare(FF)I

    move-result v13

    if-nez v13, :cond_2

    iget v13, v7, Landroid/util/DisplayMetrics;->ydpi:F

    const v14, 0x431684be

    invoke-static {v13, v14}, Ljava/lang/Float;->compare(FF)I

    move-result v13

    if-eqz v13, :cond_3

    :cond_2
    iget v13, v7, Landroid/util/DisplayMetrics;->density:F

    float-to-double v13, v13

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    cmpl-double v13, v13, v15

    if-nez v13, :cond_7

    iget v13, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v14, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v13, v14

    const/16 v14, 0x820

    if-ne v13, v14, :cond_7

    iget v13, v7, Landroid/util/DisplayMetrics;->xdpi:F

    const v14, 0x4315d2f2

    invoke-static {v13, v14}, Ljava/lang/Float;->compare(FF)I

    move-result v13

    if-nez v13, :cond_7

    iget v13, v7, Landroid/util/DisplayMetrics;->ydpi:F

    const v14, 0x4316849c

    invoke-static {v13, v14}, Ljava/lang/Float;->compare(FF)I

    move-result v13

    if-nez v13, :cond_7

    .line 307
    :cond_3
    sget v13, Lcom/vlingo/midas/R$style;->DialogSlideAnim:I

    sput v13, Lcom/vlingo/midas/settings/SettingsScreen;->mTheme:I

    .line 313
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v13

    if-nez v13, :cond_4

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v13

    if-eqz v13, :cond_8

    .line 314
    :cond_4
    sget v13, Lcom/vlingo/midas/R$style;->MidasTheme:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->setTheme(I)V

    .line 320
    :goto_1
    invoke-super/range {p0 .. p1}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 322
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string/jumbo v14, "is_start_option_menu"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 323
    .local v5, "is_start_option_menu":Z
    const-string/jumbo v13, "is_start_option_menu"

    invoke-static {v13, v5}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 325
    const/4 v13, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->setVolumeControlStream(I)V

    .line 327
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v13

    if-eqz v13, :cond_9

    .line 328
    sget v13, Lcom/vlingo/midas/R$xml;->vlingo_settings_associatedserviceonly:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->addPreferencesFromResource(I)V

    .line 334
    :goto_2
    sget v13, Lcom/vlingo/midas/R$layout;->settings_placeholder:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->setContentView(I)V

    .line 335
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    .line 337
    .local v10, "titlebar":Landroid/app/ActionBar;
    if-eqz v10, :cond_5

    .line 338
    sget v13, Lcom/vlingo/midas/R$mipmap;->svoice:I

    invoke-virtual {v10, v13}, Landroid/app/ActionBar;->setIcon(I)V

    .line 339
    sget v13, Lcom/vlingo/midas/R$string;->app_settings:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 346
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v13

    if-eqz v13, :cond_b

    .line 347
    if-eqz v10, :cond_6

    .line 348
    const/16 v13, 0x8

    invoke-virtual {v10, v13}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 377
    :cond_6
    :goto_3
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getWindow()Landroid/view/Window;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    .line 378
    .local v6, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string/jumbo v14, "privateFlags"

    invoke-virtual {v13, v14}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    .line 379
    .local v8, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string/jumbo v14, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v13, v14}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v9

    .line 380
    .local v9, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string/jumbo v14, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v13, v14}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 382
    .local v2, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v1, 0x0

    .local v1, "currentPrivateFlags":I
    const/4 v12, 0x0

    .local v12, "valueofFlagsEnableStatusBar":I
    const/4 v11, 0x0

    .line 383
    .local v11, "valueofFlagsDisableTray":I
    invoke-virtual {v8, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    .line 384
    invoke-virtual {v9, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v12

    .line 385
    invoke-virtual {v2, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v11

    .line 387
    or-int/2addr v1, v12

    .line 388
    or-int/2addr v1, v11

    .line 390
    invoke-virtual {v8, v6, v1}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 391
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getWindow()Landroid/view/Window;

    move-result-object v13

    invoke-virtual {v13, v6}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    .end local v1    # "currentPrivateFlags":I
    .end local v2    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v6    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v8    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v9    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v11    # "valueofFlagsDisableTray":I
    .end local v12    # "valueofFlagsEnableStatusBar":I
    :goto_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->makeFeatureForTablet()V

    .line 402
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->initPreference()V

    .line 403
    const-string/jumbo v13, "wake_up_lock_screen"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v13

    sput-object v13, Lcom/vlingo/midas/settings/SettingsScreen;->WakeupSummary:Landroid/preference/Preference;

    .line 425
    new-instance v13, Lcom/vlingo/midas/settings/SettingsScreen$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/vlingo/midas/settings/SettingsScreen$1;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v13}, Lcom/vlingo/midas/settings/SettingsScreen$1;->start()V

    .line 435
    new-instance v13, Lcom/vlingo/midas/settings/SettingsScreen$LoginAccountBroadcastReceiver;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v14}, Lcom/vlingo/midas/settings/SettingsScreen$LoginAccountBroadcastReceiver;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;Lcom/vlingo/midas/settings/SettingsScreen$1;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/vlingo/midas/settings/SettingsScreen;->loginAccountBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 437
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vlingo/midas/settings/SettingsScreen;->loginAccountBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v14, Landroid/content/IntentFilter;

    const-string/jumbo v15, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-direct {v14, v15}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/vlingo/midas/settings/SettingsScreen;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 441
    return-void

    .line 310
    .end local v5    # "is_start_option_menu":Z
    .end local v10    # "titlebar":Landroid/app/ActionBar;
    :cond_7
    sget v13, Lcom/vlingo/midas/R$style;->CustomBlackNoActionBar:I

    sput v13, Lcom/vlingo/midas/settings/SettingsScreen;->mTheme:I

    goto/16 :goto_0

    .line 316
    :cond_8
    sget v13, Lcom/vlingo/midas/settings/SettingsScreen;->mTheme:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->setTheme(I)V

    goto/16 :goto_1

    .line 329
    .restart local v5    # "is_start_option_menu":Z
    :cond_9
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v13

    if-eqz v13, :cond_a

    .line 330
    sget v13, Lcom/vlingo/midas/R$xml;->vlingo_settings_for_zero:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->addPreferencesFromResource(I)V

    goto/16 :goto_2

    .line 332
    :cond_a
    sget v13, Lcom/vlingo/midas/R$xml;->vlingo_settings:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vlingo/midas/settings/SettingsScreen;->addPreferencesFromResource(I)V

    goto/16 :goto_2

    .line 351
    .restart local v10    # "titlebar":Landroid/app/ActionBar;
    :cond_b
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v13

    if-eqz v13, :cond_f

    .line 352
    if-eqz v10, :cond_6

    .line 355
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 356
    .restart local v4    # "i":Landroid/content/Intent;
    const-string/jumbo v13, "is_start_option_menu"

    const/4 v14, 0x0

    invoke-virtual {v4, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 357
    const/16 v13, 0xc

    invoke-virtual {v10, v13}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 358
    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    sput-object v13, Lcom/vlingo/midas/settings/SettingsScreen;->addDownKey:Ljava/lang/Boolean;

    goto/16 :goto_3

    .line 360
    :cond_c
    const-string/jumbo v13, "from_settings"

    const/4 v14, 0x0

    invoke-virtual {v4, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_d

    .line 361
    const/16 v13, 0x8

    invoke-virtual {v10, v13}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 362
    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    sput-object v13, Lcom/vlingo/midas/settings/SettingsScreen;->addDownKey:Ljava/lang/Boolean;

    goto/16 :goto_3

    .line 364
    :cond_d
    sget-object v13, Lcom/vlingo/midas/settings/SettingsScreen;->addDownKey:Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-eqz v13, :cond_e

    .line 365
    const/16 v13, 0xc

    invoke-virtual {v10, v13}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_3

    .line 367
    :cond_e
    const/16 v13, 0x8

    invoke-virtual {v10, v13}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_3

    .line 370
    .end local v4    # "i":Landroid/content/Intent;
    :cond_f
    if-eqz v10, :cond_6

    .line 371
    const/16 v13, 0xe

    invoke-virtual {v10, v13}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_3

    .line 395
    :catch_0
    move-exception v3

    .line 398
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 392
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v13

    goto/16 :goto_4
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 10
    .param p1, "id"    # I

    .prologue
    const/4 v7, 0x0

    .line 1835
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$string;->wakeup_setting_dialog_message:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1837
    .local v3, "message":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$string;->alert_yes:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1838
    .local v6, "yes":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$string;->alert_no:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1839
    .local v4, "no":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$string;->setting_wakeup_set_wakeup_command_dialog_title:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1841
    .local v5, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 1842
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget v8, Lcom/vlingo/midas/R$layout;->wakeup_setting_dialog:I

    invoke-virtual {v2, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1844
    .local v1, "checkboxLayout":Landroid/view/View;
    sget v8, Lcom/vlingo/midas/R$id;->checkBox:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1846
    .local v0, "checkBox":Landroid/widget/CheckBox;
    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$13;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$13;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1858
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->isFinishing()Z

    move-result v8

    if-nez v8, :cond_0

    .line 1859
    packed-switch p1, :pswitch_data_0

    .line 1881
    :cond_0
    :goto_0
    return-object v7

    .line 1861
    :pswitch_0
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$15;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$15;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v7, v6, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$14;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$14;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v7, v4, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    goto :goto_0

    .line 1859
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 1620
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->unbindFacebookService(Landroid/content/Context;)V

    .line 1621
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1622
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->unbindTwitterService(Landroid/content/Context;)V

    .line 1624
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->loginAccountBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 1625
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->loginAccountBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/SettingsScreen;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1627
    :cond_1
    invoke-super {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onDestroy()V

    .line 1628
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1164
    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 1165
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1166
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.app.action.CLOSE_APPLICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1167
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/SettingsScreen;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1168
    const/4 v1, 0x1

    .line 1170
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1153
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1157
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 1155
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->finish()V

    goto :goto_0

    .line 1153
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1610
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1611
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;

    if-eqz v0, :cond_0

    .line 1612
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;

    invoke-virtual {v0}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateDrivingModeControl(I)V

    .line 1614
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->drivingModeContentObserver:Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1615
    invoke-super {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onPause()V

    .line 1616
    return-void

    .line 1612
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2062
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;

    if-ne p1, v4, :cond_1

    .line 2063
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2065
    .local v0, "checked":Z
    if-eqz v0, :cond_0

    .line 2066
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "driving_mode_on"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2070
    :goto_0
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "DrivingMode"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/settings/SettingsScreen;->sendBroadcast(Landroid/content/Intent;)V

    .line 2124
    .end local v0    # "checked":Z
    :goto_1
    return v2

    .line 2068
    .restart local v0    # "checked":Z
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "driving_mode_on"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 2073
    .end local v0    # "checked":Z
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    if-ne p1, v4, :cond_3

    :cond_2
    move v2, v3

    .line 2124
    goto :goto_1

    .line 2101
    :cond_3
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeup:Landroid/preference/CheckBoxPreference;

    if-ne p1, v4, :cond_2

    .line 2102
    const-string/jumbo v4, "preferences_voice_wake_up"

    invoke-virtual {p0, v4, v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2103
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string/jumbo v4, "VOICE_WAKE_UP_IN_SECURED_POP"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-nez v4, :cond_4

    .line 2104
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->showWakeupInSecuredLockDialog()Z

    goto :goto_1

    .line 2106
    :cond_4
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2107
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 2111
    :goto_2
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2112
    const-string/jumbo v3, "secure_voice_wake_up"

    invoke-static {v3, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 2109
    :cond_5
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_2

    .line 2114
    :cond_6
    const-string/jumbo v4, "secure_voice_wake_up"

    invoke-static {v4, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1202
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v1

    .line 1203
    .local v1, "res":Z
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "social_settings"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1204
    new-instance v3, Lcom/vlingo/midas/settings/SettingsScreen$11;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/SettingsScreen$11;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-static {v3}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;)V

    .line 1238
    :cond_0
    :goto_0
    return v1

    .line 1210
    :cond_1
    iget-object v5, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1211
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "double_tab_launch"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_2

    :goto_1
    invoke-static {v5, v6, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1

    .line 1214
    :cond_3
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "voice_input_control"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1215
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1216
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/SettingsScreen;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1217
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "driving_mode"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1218
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/vlingo/midas/settings/DrivingModeSettings;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1219
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/SettingsScreen;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1220
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "wake_up_lock_screen"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1222
    invoke-virtual {p0, v4}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1223
    .local v2, "sharedPref":Landroid/content/SharedPreferences;
    const-string/jumbo v5, "VOICE_WAKE_UP_POP_SETTINGSSCREEN"

    invoke-interface {v2, v5, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1224
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->showWakeupLockScreenDialog()V

    goto :goto_0

    .line 1226
    :cond_6
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSoundDetectorSettingOn()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1227
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->toast_while_running_sound_detector_for_svoice:I

    invoke-static {v4, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1232
    :cond_7
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1233
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v3, "preference_clicked"

    const-string/jumbo v4, "wake_up_lock_screen"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1234
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/SettingsScreen;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1490
    invoke-super {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onResume()V

    .line 1492
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->fontScale:F

    .line 1493
    .local v0, "new_scale":F
    iget v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->scale:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_1

    .line 1494
    iput v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->scale:F

    .line 1495
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1496
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->cancel()V

    .line 1498
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 1499
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1500
    sget v1, Lcom/vlingo/midas/R$xml;->vlingo_settings_associatedserviceonly:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->addPreferencesFromResource(I)V

    .line 1504
    :goto_0
    invoke-direct {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->initPreference()V

    .line 1507
    :cond_1
    iput v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mSettingClicksForDebug:I

    .line 1508
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_languageApplication:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1510
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1511
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->getFbAccountInfo(Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI$FacebookSSOCallback;)V

    .line 1513
    :cond_2
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1514
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->getTwAccountInfo(Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI$TwitterSSOCallback;)V

    .line 1517
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateSettingUI()V

    .line 1519
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1520
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mhandler:Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;->removeMessages(I)V

    .line 1521
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mhandler:Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1523
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1524
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateSocialUI()V

    .line 1527
    :cond_5
    iput-boolean v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_alreadyTTSedWakeupWord:Z

    .line 1528
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1529
    invoke-direct {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateState()V

    .line 1530
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateCheckState()V

    .line 1536
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/vlingo/midas/VlingoApplication;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/VlingoApplication;->setSocialLogin(Z)V

    .line 1538
    new-instance v1, Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->drivingModeContentObserver:Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;

    .line 1539
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->getPhoneDrivingModeUri()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen;->drivingModeContentObserver:Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1541
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    if-eqz v1, :cond_6

    .line 1550
    :cond_6
    return-void

    .line 1502
    :cond_7
    sget v1, Lcom/vlingo/midas/R$xml;->vlingo_settings:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->addPreferencesFromResource(I)V

    goto/16 :goto_0

    .line 1533
    :cond_8
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->finish()V

    .line 1534
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 10
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1649
    if-nez p2, :cond_1

    .line 1811
    :cond_0
    :goto_0
    return-void

    .line 1650
    :cond_1
    const-string/jumbo v6, "auto_dial"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1651
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateSettingUI()V

    goto :goto_0

    .line 1653
    :cond_2
    const-string/jumbo v6, "wake_up_default_engine"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1655
    const-string/jumbo v6, "wake_up_default_engine"

    const-string/jumbo v7, ""

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_engine:Landroid/preference/ListPreference;

    invoke-virtual {v7}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1658
    const-string/jumbo v6, "wake_up_default_engine"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_engine:Landroid/preference/ListPreference;

    invoke-virtual {v7}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateSettingUI()V

    goto :goto_0

    .line 1662
    :cond_4
    const-string/jumbo v6, "language"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1663
    iget-boolean v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->appLangChanged:Z

    if-nez v6, :cond_0

    .line 1664
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->isRunning()Z

    move-result v6

    if-nez v6, :cond_5

    .line 1669
    new-instance v6, Ljava/lang/Thread;

    new-instance v7, Lcom/vlingo/midas/settings/SettingsScreen$12;

    invoke-direct {v7, p0}, Lcom/vlingo/midas/settings/SettingsScreen$12;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-direct {v6, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 1675
    :cond_5
    iput-boolean v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->appLangChanged:Z

    .line 1676
    const-string/jumbo v6, "language"

    const-string/jumbo v7, "en-US"

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1681
    .local v3, "language":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1682
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v3, v6, v0}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;Landroid/content/SharedPreferences$Editor;)V

    .line 1683
    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    .line 1691
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v6

    invoke-interface {v6}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onLanguageChanged()V

    .line 1694
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v4

    .line 1695
    .local v4, "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updateParameters(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)V

    .line 1697
    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->convertToBargeinISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1698
    .local v2, "langToChange":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "voicetalk_language"

    invoke-static {v6, v7, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1699
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v7, "com.vlingo.LANGUAGE_CHANGED"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->sendBroadcast(Landroid/content/Intent;)V

    .line 1700
    const-string/jumbo v6, "Always"

    const-string/jumbo v7, "setting screen"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1701
    sput-boolean v8, Lcom/vlingo/midas/settings/SettingsScreen;->isLaunchBySettings:Z

    .line 1702
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 1703
    const/4 v6, 0x0

    sput-boolean v6, Lcom/vlingo/midas/settings/SettingsScreen;->isLaunchBySettings:Z

    .line 1704
    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/SettingsScreen;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1705
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1706
    sget v6, Lcom/vlingo/midas/R$xml;->vlingo_settings_associatedserviceonly:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->addPreferencesFromResource(I)V

    .line 1710
    :goto_1
    invoke-direct {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->initPreference()V

    .line 1711
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishDialog()V

    goto/16 :goto_0

    .line 1708
    :cond_6
    sget v6, Lcom/vlingo/midas/R$xml;->vlingo_settings:I

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->addPreferencesFromResource(I)V

    goto :goto_1

    .line 1714
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "langToChange":Ljava/lang/String;
    .end local v3    # "language":Ljava/lang/String;
    .end local v4    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_7
    const-string/jumbo v6, "web_search_engine"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1715
    iget-boolean v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->appLangChanged:Z

    if-nez v6, :cond_0

    .line 1716
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateSettingUI()V

    goto/16 :goto_0

    .line 1719
    :cond_8
    const-string/jumbo v6, "driving_mode_on"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1722
    invoke-static {}, Lcom/vlingo/midas/drivingmode/DrivingModeUtil;->isPhoneDrivingModeEnabled()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1723
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->startSafeReading()V

    goto/16 :goto_0

    .line 1725
    :cond_9
    invoke-static {}, Lcom/vlingo/core/internal/safereader/SafeReaderProxy;->stopSafeReading()V

    goto/16 :goto_0

    .line 1728
    :cond_a
    const-string/jumbo v6, "car_nav_home_address"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1729
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateSettingUI()V

    .line 1731
    const/4 v5, 0x0

    .line 1732
    .local v5, "prompt":Ljava/lang/String;
    const-string/jumbo v6, "car_nav_home_address"

    invoke-static {v6, v9}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1733
    if-eqz v5, :cond_b

    .line 1734
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_b

    .line 1736
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 1737
    const-string/jumbo v6, "car_nav_home_address"

    invoke-static {v6, v5}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1738
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v6, v5}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1743
    :cond_b
    if-eqz v5, :cond_c

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_c

    invoke-direct {p0, v5}, Lcom/vlingo/midas/settings/SettingsScreen;->containsNoAlphaNumeric(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1746
    :cond_c
    const-string/jumbo v6, "car_nav_home_address"

    const-string/jumbo v7, ""

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1747
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1751
    .end local v5    # "prompt":Ljava/lang/String;
    :cond_d
    const-string/jumbo v6, "car_nav_office_address"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 1752
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateSettingUI()V

    goto/16 :goto_0

    .line 1754
    :cond_e
    const-string/jumbo v6, "shake_to_skip"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1755
    const-string/jumbo v6, "shake_to_skip"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_ShakeToSkip:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1757
    :cond_f
    const-string/jumbo v6, "car_word_spotter_enabled"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1758
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wakeUpWord:Landroid/preference/CheckBoxPreference;

    if-eqz v6, :cond_0

    .line 1759
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wakeUpWord:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1760
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v6

    const-string/jumbo v7, "car-settings-wake-up-word-enabled"

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    .line 1764
    :goto_2
    const-string/jumbo v6, "car_word_spotter_enabled"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wakeUpWord:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1767
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wakeUpWord:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1768
    iget-boolean v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_alreadyTTSedWakeupWord:Z

    if-nez v6, :cond_0

    .line 1769
    iput-boolean v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_alreadyTTSedWakeupWord:Z

    goto/16 :goto_0

    .line 1762
    :cond_10
    invoke-static {}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->getInstance()Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;

    move-result-object v6

    const-string/jumbo v7, "car-settings-wake-up-word-disabled"

    invoke-virtual {v6, v7}, Lcom/vlingo/core/internal/userlogging/UserLoggingEngine;->landingPageViewed(Ljava/lang/String;)V

    goto :goto_2

    .line 1779
    :cond_11
    const-string/jumbo v6, "profanity_filter"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 1780
    const-string/jumbo v6, "profanity_filter"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_ProfanityFilter:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1782
    :cond_12
    const-string/jumbo v6, "auto_punctuation"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 1783
    const-string/jumbo v6, "auto_punctuation"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_autoPunctuation:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1785
    :cond_13
    const-string/jumbo v6, "listen_over_bluetooth"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 1786
    const-string/jumbo v6, "listen_over_bluetooth"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_listenOverBluetooth:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1787
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_listenOverBluetooth:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    invoke-static {v6}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->onListenOverBTSettingChanged(Z)V

    goto/16 :goto_0

    .line 1789
    :cond_14
    const-string/jumbo v6, "launch_car_on_car_dock"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 1790
    const-string/jumbo v6, "launch_car_on_car_dock"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_launchOnCarMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1792
    :cond_15
    const-string/jumbo v6, "car_auto_start_speakerphone"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v6

    if-nez v6, :cond_16

    .line 1793
    const-string/jumbo v6, "car_auto_start_speakerphone"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speaker:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1795
    :cond_16
    const-string/jumbo v6, "car_auto_start_speakerphone"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v6

    if-eqz v6, :cond_17

    .line 1796
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speakerForZero:Landroid/preference/SwitchPreference;

    if-eqz v6, :cond_0

    .line 1797
    const-string/jumbo v6, "car_auto_start_speakerphone"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speakerForZero:Landroid/preference/SwitchPreference;

    invoke-virtual {v7}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1799
    :cond_17
    const-string/jumbo v6, "headset_mode"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 1800
    const-string/jumbo v6, "headset_mode"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_headsetMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1801
    :cond_18
    const-string/jumbo v6, "car_word_spotter_motion"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 1802
    const-string/jumbo v6, "car_word_spotter_motion"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_motionSettings:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1803
    :cond_19
    const-string/jumbo v6, "say_wake_up_command"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 1804
    const-string/jumbo v6, "say_wake_up_command"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_sayWakeUpMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1805
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v6, "android.intent.action.SAY_WAKE_UP"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1806
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v6, "isChecked"

    iget-object v7, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_sayWakeUpMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1807
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1808
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1a
    const-string/jumbo v6, "launch_voicetalk"

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v6

    if-eqz v6, :cond_0

    goto/16 :goto_0
.end method

.method public showGuideDialog()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2128
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticDialog:Landroid/app/AlertDialog;

    if-eqz v4, :cond_0

    .line 2129
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->dismiss()V

    .line 2130
    iput-object v5, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticDialog:Landroid/app/AlertDialog;

    .line 2133
    :cond_0
    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {p0, v4}, Lcom/vlingo/midas/settings/SettingsScreen;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 2134
    .local v1, "inflater":Landroid/view/LayoutInflater;
    sget v4, Lcom/vlingo/midas/R$layout;->custom_alert_dialog:I

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2136
    .local v2, "layout":Landroid/view/View;
    sget v4, Lcom/vlingo/midas/R$id;->custom_dialog_warning_textView:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasDialing()Z

    move-result v5

    if-eqz v5, :cond_3

    sget v5, Lcom/vlingo/midas/R$string;->voice_input_control_message:I

    :goto_0
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 2138
    sget v4, Lcom/vlingo/midas/R$id;->checkBox:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2141
    .local v0, "check":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v4

    const/high16 v5, 0x41000000    # 8.0f

    iget v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mDensity:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x3f000000    # 0.5f

    add-float/2addr v5, v6

    float-to-int v5, v5

    add-int v3, v4, v5

    .line 2142
    .local v3, "leftPadding":I
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v5

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 2145
    new-instance v4, Lcom/vlingo/midas/settings/SettingsScreen$26;

    invoke-direct {v4, p0}, Lcom/vlingo/midas/settings/SettingsScreen$26;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2150
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->isFinishing()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2151
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v5, Lcom/vlingo/midas/R$string;->voice_input_control_title:I

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->voice_input_control_ok:I

    new-instance v6, Lcom/vlingo/midas/settings/SettingsScreen$28;

    invoke-direct {v6, p0, v0}, Lcom/vlingo/midas/settings/SettingsScreen$28;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/widget/CheckBox;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->voice_input_control_cancel:I

    new-instance v6, Lcom/vlingo/midas/settings/SettingsScreen$27;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/settings/SettingsScreen$27;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticDialog:Landroid/app/AlertDialog;

    .line 2172
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticDialog:Landroid/app/AlertDialog;

    new-instance v5, Lcom/vlingo/midas/settings/SettingsScreen$29;

    invoke-direct {v5, p0}, Lcom/vlingo/midas/settings/SettingsScreen$29;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 2187
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticDialog:Landroid/app/AlertDialog;

    if-eqz v4, :cond_2

    .line 2188
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 2189
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mAutoHapticDialog:Landroid/app/AlertDialog;

    new-instance v5, Lcom/vlingo/midas/settings/SettingsScreen$30;

    invoke-direct {v5, p0}, Lcom/vlingo/midas/settings/SettingsScreen$30;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2195
    :cond_2
    return-void

    .line 2136
    .end local v0    # "check":Landroid/widget/CheckBox;
    .end local v3    # "leftPadding":I
    :cond_3
    sget v5, Lcom/vlingo/midas/R$string;->voice_input_control_message_no_call:I

    goto/16 :goto_0
.end method

.method public showWakeupInSecuredLockDialog()Z
    .locals 11

    .prologue
    .line 2412
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 2413
    .local v3, "inflater":Landroid/view/LayoutInflater;
    sget v8, Lcom/vlingo/midas/R$layout;->custom_alert_dialog:I

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2414
    .local v2, "checkboxLayout":Landroid/view/View;
    sget v8, Lcom/vlingo/midas/R$id;->checkBox:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 2415
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const-string/jumbo v8, "system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v8}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    .line 2418
    .local v6, "samsungSans_text":Landroid/graphics/Typeface;
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v8

    const/high16 v9, 0x41000000    # 8.0f

    iget v10, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mDensity:F

    mul-float/2addr v9, v10

    const/high16 v10, 0x3f000000    # 0.5f

    add-float/2addr v9, v10

    float-to-int v9, v9

    add-int v4, v8, v9

    .line 2419
    .local v4, "leftPadding":I
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v8

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v9

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v1, v4, v8, v9, v10}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 2422
    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$32;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$32;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2427
    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$33;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$33;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2433
    const/4 v5, 0x0

    .line 2434
    .local v5, "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->isFinishing()Z

    move-result v8

    if-nez v8, :cond_2

    .line 2435
    new-instance v5, Landroid/app/AlertDialog$Builder;

    .end local v5    # "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2436
    .restart local v5    # "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 2437
    sget v8, Lcom/vlingo/midas/R$id;->custom_dialog_warning_textView:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 2438
    .local v7, "text":Landroid/widget/TextView;
    if-eqz v7, :cond_0

    .line 2439
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2441
    :cond_0
    if-eqz v1, :cond_1

    .line 2442
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2444
    :cond_1
    sget v8, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2445
    sget v8, Lcom/vlingo/midas/R$string;->setting_wakeup_secure_popupbody:I

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 2447
    const v8, 0x104000a

    new-instance v9, Lcom/vlingo/midas/settings/SettingsScreen$34;

    invoke-direct {v9, p0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$34;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2461
    const/high16 v8, 0x1040000

    new-instance v9, Lcom/vlingo/midas/settings/SettingsScreen$35;

    invoke-direct {v9, p0}, Lcom/vlingo/midas/settings/SettingsScreen$35;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2477
    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$36;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$36;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2495
    .end local v7    # "text":Landroid/widget/TextView;
    :cond_2
    if-eqz v5, :cond_3

    .line 2496
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 2497
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2498
    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$37;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$37;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2503
    .end local v0    # "alertDialog":Landroid/app/AlertDialog;
    :cond_3
    const/4 v8, 0x0

    return v8
.end method

.method public showWakeupLockScreenDialog()V
    .locals 11

    .prologue
    .line 1885
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1886
    .local v3, "inflater":Landroid/view/LayoutInflater;
    sget v8, Lcom/vlingo/midas/R$layout;->custom_alert_dialog:I

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1887
    .local v2, "checkboxLayout":Landroid/view/View;
    sget v8, Lcom/vlingo/midas/R$id;->checkBox:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 1888
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const-string/jumbo v8, "system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v8}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    .line 1891
    .local v6, "samsungSans_text":Landroid/graphics/Typeface;
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v4

    .line 1895
    .local v4, "leftPadding":I
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v8

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v9

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v1, v4, v8, v9, v10}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 1898
    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$16;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$16;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1903
    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$17;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$17;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1908
    const/4 v5, 0x0

    .line 1909
    .local v5, "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->isFinishing()Z

    move-result v8

    if-nez v8, :cond_2

    .line 1910
    new-instance v5, Landroid/app/AlertDialog$Builder;

    .end local v5    # "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1911
    .restart local v5    # "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1912
    sget v8, Lcom/vlingo/midas/R$id;->custom_dialog_warning_textView:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1913
    .local v7, "text":Landroid/widget/TextView;
    if-eqz v7, :cond_0

    .line 1914
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1916
    :cond_0
    if-eqz v1, :cond_1

    .line 1917
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1918
    :cond_1
    sget v8, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1919
    sget v8, Lcom/vlingo/midas/R$string;->wakeup_screen_off_dialog_message:I

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 1920
    const v8, 0x104000a

    new-instance v9, Lcom/vlingo/midas/settings/SettingsScreen$18;

    invoke-direct {v9, p0, v1}, Lcom/vlingo/midas/settings/SettingsScreen$18;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1943
    const/high16 v8, 0x1040000

    new-instance v9, Lcom/vlingo/midas/settings/SettingsScreen$19;

    invoke-direct {v9, p0}, Lcom/vlingo/midas/settings/SettingsScreen$19;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1953
    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$20;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$20;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1965
    .end local v7    # "text":Landroid/widget/TextView;
    :cond_2
    if-eqz v5, :cond_3

    .line 1966
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1967
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1968
    new-instance v8, Lcom/vlingo/midas/settings/SettingsScreen$21;

    invoke-direct {v8, p0}, Lcom/vlingo/midas/settings/SettingsScreen$21;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1973
    .end local v0    # "alertDialog":Landroid/app/AlertDialog;
    :cond_3
    return-void
.end method

.method protected updateCheckState()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1556
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_0

    .line 1557
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v3, "launch_voicetalk"

    invoke-static {v3, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1570
    :cond_0
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_ProfanityFilter:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_1

    .line 1571
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_ProfanityFilter:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v3, "profanity_filter"

    invoke-static {v3, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1576
    :cond_1
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speaker:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_2

    .line 1577
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speaker:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v3, "car_auto_start_speakerphone"

    invoke-static {v3, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1580
    :cond_2
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speakerForZero:Landroid/preference/SwitchPreference;

    if-eqz v2, :cond_3

    .line 1581
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_auto_start_speakerForZero:Landroid/preference/SwitchPreference;

    const-string/jumbo v3, "car_auto_start_speakerphone"

    invoke-static {v3, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 1590
    :cond_3
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wakeUpWord:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_4

    .line 1591
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wakeUpWord:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v3, "car_word_spotter_enabled"

    invoke-static {v3, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1594
    :cond_4
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_6

    .line 1595
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isH_Device()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1596
    :cond_5
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v3, "voice_wake_up"

    invoke-static {v3, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1603
    :cond_6
    :goto_0
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_headsetMode:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_7

    .line 1604
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_headsetMode:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v2, "headset_mode"

    invoke-static {v2, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1606
    :cond_7
    return-void

    .line 1598
    :cond_8
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "wake_up_lock_screen"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_9

    move v0, v1

    :cond_9
    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0
.end method

.method protected updateSettingUI()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1395
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 1396
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_webSearchEngine:Landroid/preference/ListPreference;

    if-eqz v8, :cond_0

    .line 1397
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_webSearchEngine:Landroid/preference/ListPreference;

    iget-object v9, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_webSearchEngine:Landroid/preference/ListPreference;

    invoke-virtual {v9}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1400
    :cond_0
    const-string/jumbo v8, "call"

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    .line 1401
    .local v2, "pc_call":Landroid/preference/PreferenceCategory;
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_autoDial:Landroid/preference/ListPreference;

    if-eqz v8, :cond_1

    if-eqz v2, :cond_1

    .line 1402
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_autoDial:Landroid/preference/ListPreference;

    invoke-virtual {v2, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 1405
    :cond_1
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_languageApplication:Landroid/preference/ListPreference;

    if-eqz v8, :cond_2

    .line 1406
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_languageApplication:Landroid/preference/ListPreference;

    iget-object v9, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_languageApplication:Landroid/preference/ListPreference;

    invoke-virtual {v9}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1409
    :cond_2
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_engine:Landroid/preference/ListPreference;

    if-eqz v8, :cond_3

    .line 1410
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_engine:Landroid/preference/ListPreference;

    iget-object v9, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_engine:Landroid/preference/ListPreference;

    invoke-virtual {v9}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1413
    :cond_3
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeStartupTtsPrompt:Landroid/preference/EditTextPreference;

    if-eqz v8, :cond_4

    .line 1414
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeStartupTtsPrompt:Landroid/preference/EditTextPreference;

    const-string/jumbo v9, "tts_carmode_startup_prompt"

    sget v10, Lcom/vlingo/midas/R$string;->midas_greeting:I

    invoke-virtual {p0, v10}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1417
    :cond_4
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_autoPunctuation:Landroid/preference/CheckBoxPreference;

    if-eqz v8, :cond_5

    .line 1418
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_autoPunctuation:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v9, "auto_punctuation"

    invoke-static {v9, v6}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    invoke-virtual {v8, v9}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 1421
    :cond_5
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    if-eqz v8, :cond_6

    .line 1422
    const-string/jumbo v8, "car_nav_home_address"

    invoke-static {v8, v11}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1423
    .local v5, "summery":Ljava/lang/String;
    if-eqz v5, :cond_f

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_f

    .line 1424
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v8, v5}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1425
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v8, v5}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 1434
    .end local v5    # "summery":Ljava/lang/String;
    :cond_6
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->isKoreanNavigationenable()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1435
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    if-eqz v8, :cond_7

    .line 1436
    const-string/jumbo v8, "car_nav_office_address"

    invoke-static {v8, v11}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1437
    .restart local v5    # "summery":Ljava/lang/String;
    if-eqz v5, :cond_10

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_10

    .line 1438
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1439
    .local v1, "officeAddress":Ljava/lang/String;
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v8, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1440
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    invoke-virtual {v8, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 1449
    .end local v1    # "officeAddress":Ljava/lang/String;
    .end local v5    # "summery":Ljava/lang/String;
    :cond_7
    :goto_1
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_safereaderEmailPollInterval:Landroid/preference/ListPreference;

    if-eqz v8, :cond_9

    .line 1450
    const-string/jumbo v8, "safereader_email_poll_interval"

    const-string/jumbo v9, "30000"

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 1451
    .local v3, "pollms":J
    const-wide/32 v8, 0xea60

    div-long/2addr v3, v8

    .line 1452
    sget v8, Lcom/vlingo/midas/R$string;->settings_space_minutes:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1453
    .local v0, "min":Ljava/lang/String;
    const-wide/16 v8, 0x1

    cmp-long v8, v3, v8

    if-nez v8, :cond_8

    sget v8, Lcom/vlingo/midas/R$string;->settings_space_minute:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1454
    :cond_8
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_safereaderEmailPollInterval:Landroid/preference/ListPreference;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1457
    .end local v0    # "min":Ljava/lang/String;
    .end local v3    # "pollms":J
    :cond_9
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_EmailReadback:Landroid/preference/CheckBoxPreference;

    if-eqz v8, :cond_a

    .line 1458
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isEMailReadbackEnabled()Z

    move-result v8

    iget-object v9, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_EmailReadback:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v9}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v9

    if-eq v8, v9, :cond_a

    .line 1459
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_EmailReadback:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isEMailReadbackEnabled()Z

    move-result v9

    invoke-virtual {v8, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1463
    :cond_a
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_motionSettings:Landroid/preference/CheckBoxPreference;

    if-eqz v8, :cond_b

    .line 1464
    invoke-direct {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->checkSystemMotionSettings()V

    .line 1467
    :cond_b
    const-string/jumbo v8, "erase_server_data"

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mEraseServerData:Landroid/preference/Preference;

    .line 1468
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mEraseServerData:Landroid/preference/Preference;

    if-eqz v8, :cond_c

    .line 1469
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSpeakerID()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_c

    .line 1472
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mEraseServerData:Landroid/preference/Preference;

    invoke-virtual {v8, v7}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1479
    :cond_c
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    if-eqz v8, :cond_e

    .line 1480
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isH_Device()Z

    move-result v8

    if-nez v8, :cond_d

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v8

    if-eqz v8, :cond_11

    .line 1481
    :cond_d
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v8, "voice_wake_up"

    invoke-static {v8, v7}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    invoke-virtual {v6, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1486
    :cond_e
    :goto_2
    return-void

    .line 1428
    .restart local v5    # "summery":Ljava/lang/String;
    :cond_f
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    sget v9, Lcom/vlingo/midas/R$string;->settings_configure_home_address:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1429
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;

    const-string/jumbo v9, ""

    invoke-virtual {v8, v9}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1443
    :cond_10
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    sget v9, Lcom/vlingo/midas/R$string;->settings_incar_officeaddress_summary:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1444
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;

    const-string/jumbo v9, ""

    invoke-virtual {v8, v9}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1483
    .end local v5    # "summery":Ljava/lang/String;
    :cond_11
    iget-object v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "wake_up_lock_screen"

    invoke-static {v9, v10, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    if-eqz v9, :cond_12

    :goto_3
    invoke-virtual {v8, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_2

    :cond_12
    move v6, v7

    goto :goto_3
.end method

.method protected updateSocialUI()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x4

    const/4 v10, 0x0

    .line 1243
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 1244
    const-string/jumbo v6, "key_social_login_attemp_for_resume"

    invoke-static {v6, v10}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1246
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1247
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "zh_CN"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1248
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1249
    const-string/jumbo v6, "social_settings_category"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    .line 1250
    .local v2, "myCategory":Landroid/preference/PreferenceCategory;
    if-eqz v2, :cond_0

    .line 1251
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 1392
    .end local v2    # "myCategory":Landroid/preference/PreferenceCategory;
    :cond_0
    :goto_0
    return-void

    .line 1255
    :cond_1
    const-string/jumbo v6, "facebook_account"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    .line 1256
    const-string/jumbo v6, "twitter_account"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    .line 1257
    const-string/jumbo v6, "social_settings_category"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceCategory;

    .line 1258
    .local v3, "myCategory1":Landroid/preference/PreferenceCategory;
    if-eqz v3, :cond_0

    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v6, :cond_0

    .line 1259
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v3, v6}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 1260
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v3, v6}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 1266
    .end local v3    # "myCategory1":Landroid/preference/PreferenceCategory;
    :cond_2
    const-string/jumbo v6, "twitter_account"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    .line 1267
    const-string/jumbo v6, "social_settings_category"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceCategory;

    .line 1268
    .restart local v3    # "myCategory1":Landroid/preference/PreferenceCategory;
    if-eqz v3, :cond_3

    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v6, :cond_3

    .line 1269
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v3, v6}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 1273
    :cond_3
    const-string/jumbo v6, "facebook_account"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    .line 1274
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v6, :cond_0

    .line 1275
    new-instance v0, Landroid/content/Intent;

    const-class v6, Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {v0, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1276
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v6, "android.intent.action.MAIN"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1277
    const-string/jumbo v6, "android.intent.extra.INTENT"

    const/16 v7, 0x20

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1278
    const-string/jumbo v6, "android.intent.extra.TEXT"

    const-string/jumbo v7, "false"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1280
    const-string/jumbo v6, "weibo_account"

    invoke-static {v6, v10}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1281
    .local v1, "loggedIn":Z
    if-eqz v1, :cond_5

    .line 1282
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget v8, Lcom/vlingo/midas/R$string;->settings_logged_in_as:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "weibo_account_name"

    const-string/jumbo v9, ""

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1283
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v7, Lcom/vlingo/midas/R$string;->settings_tap_to_logout_facebook:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1284
    const-string/jumbo v6, "weibo_picture"

    invoke-static {v6}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1285
    .local v5, "thumbImg":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v10}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    .line 1286
    const-string/jumbo v6, "logout_social_network"

    invoke-virtual {v0, v6, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1287
    if-eqz v5, :cond_4

    .line 1288
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewBitmap(Landroid/graphics/Bitmap;)V

    .line 1296
    .end local v5    # "thumbImg":Landroid/graphics/Bitmap;
    :cond_4
    :goto_1
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v0}, Lcom/vlingo/midas/settings/ImagePreference;->setIntent(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1292
    :cond_5
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v7, Lcom/vlingo/midas/R$string;->settings_login_weibo:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1293
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v7, Lcom/vlingo/midas/R$string;->settings_click_to_login_weibo:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1294
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v11}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    goto :goto_1

    .line 1300
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "loggedIn":Z
    .end local v3    # "myCategory1":Landroid/preference/PreferenceCategory;
    :cond_6
    const-string/jumbo v6, "twitter_account"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    .line 1301
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v6, :cond_8

    .line 1302
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v6

    if-nez v6, :cond_c

    .line 1303
    new-instance v0, Landroid/content/Intent;

    const-class v6, Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {v0, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1304
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v6, "android.intent.action.MAIN"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1305
    const-string/jumbo v6, "android.intent.extra.INTENT"

    invoke-virtual {v0, v6, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1306
    const-string/jumbo v6, "android.intent.extra.TEXT"

    const-string/jumbo v7, "false"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1307
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v0}, Lcom/vlingo/midas/settings/ImagePreference;->setIntent(Landroid/content/Intent;)V

    .line 1310
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->twitterSSO()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1311
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterAccountCreated(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 1312
    const-string/jumbo v6, "twitter_account"

    invoke-static {v6, v10}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1314
    :cond_7
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/TwitterSSOAPI;->isTwitterLoggedIn()Z

    move-result v1

    .line 1319
    .restart local v1    # "loggedIn":Z
    :goto_2
    if-eqz v1, :cond_b

    .line 1320
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget v8, Lcom/vlingo/midas/R$string;->settings_logged_in_as:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "twitter_account_name"

    const-string/jumbo v9, ""

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1321
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    sget v7, Lcom/vlingo/midas/R$string;->settings_tap_to_signout_twitter:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1322
    const-string/jumbo v6, "twitter_picture"

    invoke-static {v6}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1323
    .restart local v5    # "thumbImg":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v10}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    .line 1324
    if-eqz v5, :cond_8

    .line 1325
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewBitmap(Landroid/graphics/Bitmap;)V

    .line 1349
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "loggedIn":Z
    .end local v5    # "thumbImg":Landroid/graphics/Bitmap;
    :cond_8
    :goto_3
    const-string/jumbo v6, "facebook_account"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/vlingo/midas/settings/ImagePreference;

    iput-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    .line 1350
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    if-eqz v6, :cond_0

    .line 1351
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v6

    if-nez v6, :cond_f

    .line 1352
    new-instance v0, Landroid/content/Intent;

    const-class v6, Lcom/vlingo/midas/social/SocialAccountActivity;

    invoke-direct {v0, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1353
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v6, "android.intent.action.MAIN"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1354
    const-string/jumbo v6, "android.intent.extra.INTENT"

    const/16 v7, 0x8

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1355
    const-string/jumbo v6, "android.intent.extra.TEXT"

    const-string/jumbo v7, "false"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1356
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v0}, Lcom/vlingo/midas/settings/ImagePreference;->setIntent(Landroid/content/Intent;)V

    .line 1359
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->facebookSSO()Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1360
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookAccountCreated(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 1361
    const-string/jumbo v6, "facebook_account"

    invoke-static {v6, v10}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1363
    :cond_9
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/FacebookSSOAPI;->isFacebookLoggedIn()Z

    move-result v1

    .line 1367
    .restart local v1    # "loggedIn":Z
    :goto_4
    if-eqz v1, :cond_e

    .line 1368
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget v8, Lcom/vlingo/midas/R$string;->settings_logged_in_as:I

    invoke-virtual {p0, v8}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "facebook_account_name"

    const-string/jumbo v9, ""

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1369
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v7, Lcom/vlingo/midas/R$string;->settings_tap_to_logout_facebook:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1370
    const-string/jumbo v6, "facebook_picture"

    invoke-static {v6}, Lcom/vlingo/core/internal/settings/Settings;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1371
    .restart local v5    # "thumbImg":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v10}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    .line 1372
    if-eqz v5, :cond_0

    .line 1373
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v5}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 1316
    .end local v1    # "loggedIn":Z
    .end local v5    # "thumbImg":Landroid/graphics/Bitmap;
    :cond_a
    const-string/jumbo v6, "twitter_account"

    invoke-static {v6, v10}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .restart local v1    # "loggedIn":Z
    goto/16 :goto_2

    .line 1329
    :cond_b
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    sget v7, Lcom/vlingo/midas/R$string;->settings_login_twitter:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1330
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    sget v7, Lcom/vlingo/midas/R$string;->settings_click_to_login_twitter:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1331
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_twitter:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v11}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    goto/16 :goto_3

    .line 1334
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "loggedIn":Z
    :cond_c
    const-string/jumbo v6, "twitter_account"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1339
    .local v4, "pref":Landroid/preference/Preference;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 1342
    invoke-virtual {v4, v10}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1343
    invoke-virtual {v4, v12}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    goto/16 :goto_3

    .line 1365
    .end local v4    # "pref":Landroid/preference/Preference;
    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_d
    const-string/jumbo v6, "facebook_account"

    invoke-static {v6, v10}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .restart local v1    # "loggedIn":Z
    goto/16 :goto_4

    .line 1377
    :cond_e
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v7, Lcom/vlingo/midas/R$string;->settings_login_facebook:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1378
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    sget v7, Lcom/vlingo/midas/R$string;->settings_click_to_login_facebook:I

    invoke-virtual {p0, v7}, Lcom/vlingo/midas/settings/SettingsScreen;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/ImagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1379
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->m_facebook:Lcom/vlingo/midas/settings/ImagePreference;

    invoke-virtual {v6, v11}, Lcom/vlingo/midas/settings/ImagePreference;->setImageViewVisibility(I)V

    goto/16 :goto_0

    .line 1382
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "loggedIn":Z
    :cond_f
    const-string/jumbo v6, "facebook_account"

    invoke-virtual {p0, v6}, Lcom/vlingo/midas/settings/SettingsScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1383
    .restart local v4    # "pref":Landroid/preference/Preference;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1386
    invoke-virtual {v4, v10}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1387
    invoke-virtual {v4, v12}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    goto/16 :goto_0
.end method

.method public wakeup_lockscreen_dialog(Landroid/preference/Preference;)Z
    .locals 9
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 1977
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 1978
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget v6, Lcom/vlingo/midas/R$layout;->custom_alert_dialog:I

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1980
    .local v1, "checkboxLayout":Landroid/view/View;
    sget v6, Lcom/vlingo/midas/R$id;->checkBox:I

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1983
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v6

    const/high16 v7, 0x41000000    # 8.0f

    iget v8, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mDensity:F

    mul-float/2addr v7, v8

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v7, v8

    float-to-int v7, v7

    add-int v3, v6, v7

    .line 1984
    .local v3, "leftPadding":I
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v6

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v7

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v8

    invoke-virtual {v0, v3, v6, v7, v8}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 1987
    new-instance v6, Lcom/vlingo/midas/settings/SettingsScreen$22;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/settings/SettingsScreen$22;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1993
    new-instance v6, Lcom/vlingo/midas/settings/SettingsScreen$23;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/settings/SettingsScreen$23;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2001
    const/4 v4, 0x0

    .line 2002
    .local v4, "mAlertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsScreen;->isFinishing()Z

    move-result v6

    if-nez v6, :cond_1

    .line 2003
    new-instance v4, Landroid/app/AlertDialog$Builder;

    .end local v4    # "mAlertDialog":Landroid/app/AlertDialog$Builder;
    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2004
    .restart local v4    # "mAlertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 2005
    sget v6, Lcom/vlingo/midas/R$id;->custom_dialog_warning_textView:I

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 2006
    .local v5, "text":Landroid/widget/TextView;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isH_Device()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2007
    :cond_0
    sget v6, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    invoke-virtual {v4, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2008
    sget v6, Lcom/vlingo/midas/R$string;->wakeup_screen_off_dialog_message:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 2017
    :goto_0
    const v6, 0x104000a

    new-instance v7, Lcom/vlingo/midas/settings/SettingsScreen$24;

    invoke-direct {v7, p0, v0}, Lcom/vlingo/midas/settings/SettingsScreen$24;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/widget/CheckBox;)V

    invoke-virtual {v4, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2040
    const/high16 v6, 0x1040000

    new-instance v7, Lcom/vlingo/midas/settings/SettingsScreen$25;

    invoke-direct {v7, p0}, Lcom/vlingo/midas/settings/SettingsScreen$25;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    invoke-virtual {v4, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2050
    .end local v5    # "text":Landroid/widget/TextView;
    :cond_1
    if-eqz v4, :cond_2

    .line 2051
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2053
    :cond_2
    const/4 v6, 0x0

    return v6

    .line 2010
    .restart local v5    # "text":Landroid/widget/TextView;
    :cond_3
    sget v6, Lcom/vlingo/midas/R$string;->wakeup_lock_screen_dialog_title:I

    invoke-virtual {v4, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2011
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasRadio()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2012
    sget v6, Lcom/vlingo/midas/R$string;->wakeup_lock_screen_dialog_message:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 2014
    :cond_4
    sget v6, Lcom/vlingo/midas/R$string;->wakeup_lock_screen_dialog_message2:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
