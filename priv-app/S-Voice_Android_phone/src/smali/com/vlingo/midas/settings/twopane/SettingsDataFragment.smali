.class public Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;
.super Landroid/app/Fragment;
.source "SettingsDataFragment.java"

# interfaces
.implements Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;,
        Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;
    }
.end annotation


# instance fields
.field private currentFragment:Landroid/app/Fragment;

.field mFragmentManager:Landroid/app/FragmentManager;

.field mFragmentTransaction:Landroid/app/FragmentTransaction;

.field mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

.field private pathDescriptor:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->mFragmentManager:Landroid/app/FragmentManager;

    .line 25
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->mFragmentTransaction:Landroid/app/FragmentTransaction;

    .line 27
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->pathDescriptor:Landroid/widget/LinearLayout;

    .line 28
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->currentFragment:Landroid/app/Fragment;

    .line 135
    return-void
.end method

.method private addView(Landroid/view/View;I)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "depthLevel"    # I

    .prologue
    .line 111
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    if-ge v2, p2, :cond_1

    .line 112
    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->pathDescriptor:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 113
    .local v0, "childView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 114
    sget v3, Lcom/vlingo/midas/R$id;->fragment_description:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 115
    .local v1, "description":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$color;->path_textview_blue_color_unselected:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 116
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$drawable;->settingheader_background:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 111
    .end local v1    # "description":Landroid/widget/TextView;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 119
    .end local v0    # "childView":Landroid/view/View;
    :cond_1
    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->pathDescriptor:Landroid/widget/LinearLayout;

    invoke-virtual {v3, p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 120
    return-void
.end method

.method private removeViews(I)V
    .locals 3
    .param p1, "depthLevel"    # I

    .prologue
    .line 123
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->pathDescriptor:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .local v1, "index":I
    :goto_0
    if-le v1, p1, :cond_1

    .line 124
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->pathDescriptor:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 125
    .local v0, "childView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 126
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->pathDescriptor:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 123
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 129
    .end local v0    # "childView":Landroid/view/View;
    :cond_1
    return-void
.end method


# virtual methods
.method protected loadFragment(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 67
    const/4 v0, 0x0

    .line 68
    .local v0, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 69
    .local v1, "mFragmentTransaction":Landroid/app/FragmentTransaction;
    packed-switch p1, :pswitch_data_0

    .line 86
    :goto_0
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->currentFragment:Landroid/app/Fragment;

    .line 87
    sget v2, Lcom/vlingo/midas/R$id;->content:I

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 88
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 89
    return-void

    .line 71
    :pswitch_0
    new-instance v0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    .end local v0    # "fragment":Landroid/app/Fragment;
    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;-><init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V

    .line 72
    .restart local v0    # "fragment":Landroid/app/Fragment;
    goto :goto_0

    .line 74
    :pswitch_1
    new-instance v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .end local v0    # "fragment":Landroid/app/Fragment;
    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;-><init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V

    .line 75
    .restart local v0    # "fragment":Landroid/app/Fragment;
    goto :goto_0

    .line 77
    :pswitch_2
    new-instance v0, Lcom/vlingo/midas/help/AboutScreenFragment;

    .end local v0    # "fragment":Landroid/app/Fragment;
    invoke-direct {v0, p0}, Lcom/vlingo/midas/help/AboutScreenFragment;-><init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V

    .line 78
    .restart local v0    # "fragment":Landroid/app/Fragment;
    goto :goto_0

    .line 80
    :pswitch_3
    new-instance v0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    .end local v0    # "fragment":Landroid/app/Fragment;
    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;-><init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V

    .line 81
    .restart local v0    # "fragment":Landroid/app/Fragment;
    goto :goto_0

    .line 83
    :pswitch_4
    new-instance v0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    .end local v0    # "fragment":Landroid/app/Fragment;
    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;-><init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V

    .restart local v0    # "fragment":Landroid/app/Fragment;
    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected loadPreferences(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 45
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->mFragmentTransaction:Landroid/app/FragmentTransaction;

    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, "preferenceFragment":Landroid/preference/PreferenceFragment;
    packed-switch p1, :pswitch_data_0

    .line 60
    :goto_0
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->currentFragment:Landroid/app/Fragment;

    .line 61
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->mFragmentTransaction:Landroid/app/FragmentTransaction;

    sget v2, Lcom/vlingo/midas/R$id;->content:I

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 62
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->mFragmentTransaction:Landroid/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 63
    return-void

    .line 49
    :pswitch_0
    new-instance v0, Lcom/vlingo/midas/settings/twopane/GeneralFragment;

    .end local v0    # "preferenceFragment":Landroid/preference/PreferenceFragment;
    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/twopane/GeneralFragment;-><init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V

    .line 50
    .restart local v0    # "preferenceFragment":Landroid/preference/PreferenceFragment;
    goto :goto_0

    .line 52
    :pswitch_1
    new-instance v0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;

    .end local v0    # "preferenceFragment":Landroid/preference/PreferenceFragment;
    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;-><init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V

    .line 53
    .restart local v0    # "preferenceFragment":Landroid/preference/PreferenceFragment;
    goto :goto_0

    .line 55
    :pswitch_2
    new-instance v0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    .end local v0    # "preferenceFragment":Landroid/preference/PreferenceFragment;
    invoke-direct {v0, p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;-><init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V

    .line 56
    .restart local v0    # "preferenceFragment":Landroid/preference/PreferenceFragment;
    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->currentFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;

    invoke-interface {v0}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;->onBackPressed()V

    .line 133
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->mFragmentManager:Landroid/app/FragmentManager;

    .line 41
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->loadPreferences(I)V

    .line 42
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    sget v1, Lcom/vlingo/midas/R$layout;->activity_main:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 33
    .local v0, "rootView":Landroid/view/View;
    sget v1, Lcom/vlingo/midas/R$id;->pathDescriptor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->pathDescriptor:Landroid/widget/LinearLayout;

    .line 34
    return-object v0
.end method

.method setOnSettingsContentClickListener(Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;)V
    .locals 0
    .param p1, "mClickListener"    # Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    .line 97
    return-void
.end method

.method public updatePathHeader(Landroid/view/View;I)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "depthLevel"    # I

    .prologue
    .line 101
    invoke-direct {p0, p2}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->removeViews(I)V

    .line 102
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->pathDescriptor:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 103
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->addView(Landroid/view/View;I)V

    .line 108
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->pathDescriptor:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 106
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method
