.class Lcom/vlingo/midas/settings/SettingsScreen$24;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->wakeup_lockscreen_dialog(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 2017
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$24;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    iput-object p2, p0, Lcom/vlingo/midas/settings/SettingsScreen$24;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2020
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$24;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2021
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$24;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 2025
    :goto_0
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$24;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2026
    const/4 v0, 0x0

    .line 2027
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$24;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const-string/jumbo v4, "checkBoxPrefForAlert"

    invoke-virtual {v3, v4, v2}, Lcom/vlingo/midas/settings/SettingsScreen;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2028
    const-string/jumbo v3, "isCheckBoxChecked"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2029
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2031
    .end local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isH_Device()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2032
    :cond_1
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->startAlwaysMicOnserviceIfNeeded()V

    .line 2033
    const-string/jumbo v2, "voice_wake_up"

    invoke-static {v2, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 2037
    :goto_1
    return-void

    .line 2023
    :cond_2
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$24;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 2035
    :cond_3
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$24;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "wake_up_lock_screen"

    iget-object v5, p0, Lcom/vlingo/midas/settings/SettingsScreen$24;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;
    invoke-static {v5}, Lcom/vlingo/midas/settings/SettingsScreen;->access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v5

    invoke-virtual {v5}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_4

    :goto_2
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method
