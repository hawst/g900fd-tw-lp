.class Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LoginAccountBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AdvancedFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/twopane/AdvancedFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoginAccountBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LoginAccountBroadcastReceiver;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;Lcom/vlingo/midas/settings/twopane/AdvancedFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/settings/twopane/AdvancedFragment;
    .param p2, "x1"    # Lcom/vlingo/midas/settings/twopane/AdvancedFragment$1;

    .prologue
    .line 469
    invoke-direct {p0, p1}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LoginAccountBroadcastReceiver;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 472
    if-eqz p2, :cond_0

    .line 473
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 476
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 477
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LoginAccountBroadcastReceiver;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mhandler:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;
    invoke-static {v1}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$300(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;->removeMessages(I)V

    .line 478
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LoginAccountBroadcastReceiver;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->mhandler:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;
    invoke-static {v1}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$300(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$AdvancedFragmentHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 481
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    return-void
.end method
