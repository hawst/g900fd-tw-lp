.class Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$1;
.super Ljava/lang/Object;
.source "SettingLanguageScreenFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

.field final synthetic val$selectedItem:I


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;I)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    iput p2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$1;->val$selectedItem:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$1;->val$selectedItem:I

    if-eq v0, p3, :cond_0

    .line 79
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->access$000(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 80
    :cond_0
    const-string/jumbo v1, "language"

    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguages:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->access$100(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/vlingo/midas/settings/MidasSettings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->onLanguageChanged()V

    .line 82
    return-void
.end method
