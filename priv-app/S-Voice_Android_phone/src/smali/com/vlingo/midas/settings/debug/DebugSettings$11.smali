.class Lcom/vlingo/midas/settings/debug/DebugSettings$11;
.super Ljava/lang/Object;
.source "DebugSettings.java"

# interfaces
.implements Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/debug/DebugSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$11;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceUpdated(Landroid/preference/Preference;)V
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 399
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$11;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->enableFakeLatLong(Z)V

    .line 400
    return-void
.end method
