.class public Lcom/vlingo/midas/settings/twopane/WakeupFragment;
.super Landroid/preference/PreferenceFragment;
.source "WakeupFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;


# static fields
.field private static final KEY_VOICE_WAKEUP_LOCK_SCREEN:Ljava/lang/String; = "wake_up_lock_screen"

.field public static mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;


# instance fields
.field private context:Landroid/content/Context;

.field private mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

.field private mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

.field private m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

.field private scale:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->context:Landroid/content/Context;

    .line 42
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V
    .locals 1
    .param p1, "onFragmentLoadListener"    # Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->context:Landroid/content/Context;

    .line 39
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .line 40
    return-void
.end method

.method private initFragment()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 82
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v3

    if-nez v3, :cond_0

    .line 83
    const-string/jumbo v1, "launch_voicetalk"

    .line 84
    .local v1, "preferenceName":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    .line 86
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "double_tab_launch"

    const/4 v5, -0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 87
    .local v2, "sysConfig":I
    if-ne v2, v6, :cond_2

    .line 88
    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 92
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isHomeKeyEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isUCameraModel()Z

    move-result v3

    if-nez v3, :cond_0

    .line 93
    const-string/jumbo v3, "general_settings"

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 94
    .local v0, "generalCategory":Landroid/preference/PreferenceCategory;
    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 97
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    .line 104
    .end local v0    # "generalCategory":Landroid/preference/PreferenceCategory;
    .end local v1    # "preferenceName":Ljava/lang/String;
    .end local v2    # "sysConfig":I
    :cond_0
    const-string/jumbo v3, "wake_up_lock_screen"

    invoke-virtual {p0, v3}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/vlingo/midas/settings/TwoLinePreference;

    sput-object v3, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    .line 105
    sget-object v3, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    if-eqz v3, :cond_1

    .line 106
    const-string/jumbo v3, "voice_wake_up"

    invoke-static {v3, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 107
    sget-object v3, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    sget v4, Lcom/vlingo/midas/R$string;->on:I

    invoke-virtual {v3, v4}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    .line 112
    :cond_1
    :goto_1
    return-void

    .line 90
    .restart local v1    # "preferenceName":Ljava/lang/String;
    .restart local v2    # "sysConfig":I
    :cond_2
    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 109
    .end local v1    # "preferenceName":Ljava/lang/String;
    .end local v2    # "sysConfig":I
    :cond_3
    sget-object v3, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    sget v4, Lcom/vlingo/midas/R$string;->off:I

    invoke-virtual {v3, v4}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    goto :goto_1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$layout;->settings_path_view:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 74
    .local v1, "view":Landroid/view/View;
    sget v2, Lcom/vlingo/midas/R$id;->fragment_description:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 75
    .local v0, "textView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->settings_incar_wakeup_lower:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    if-eqz v2, :cond_0

    .line 78
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;->updatePathHeader(Landroid/view/View;I)V

    .line 79
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 116
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->setOnSettingsContentClickListener(Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;)V

    .line 117
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->finish()V

    .line 150
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->context:Landroid/content/Context;

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;->onHeadlineSelected(I)V

    .line 143
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->context:Landroid/content/Context;

    .line 48
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->fontScale:F

    iput v0, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->scale:F

    .line 49
    sget v0, Lcom/vlingo/midas/R$xml;->wakeup_settings:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->addPreferencesFromResource(I)V

    .line 50
    invoke-direct {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->initFragment()V

    .line 51
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 121
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDetach()V

    .line 122
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->setOnSettingsContentClickListener(Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;)V

    .line 123
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v1, 0x1

    .line 127
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    .line 128
    .local v0, "res":Z
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 129
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "double_tab_launch"

    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 133
    :cond_0
    :goto_1
    return v0

    .line 129
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 130
    :cond_2
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "wake_up_lock_screen"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    iget-object v2, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    invoke-interface {v2, v1}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;->onItemSelected(I)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 56
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 58
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->fontScale:F

    .line 59
    .local v0, "new_scale":F
    iget v1, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->scale:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    iput v0, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->scale:F

    .line 61
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 62
    sget v1, Lcom/vlingo/midas/R$xml;->wakeup_settings:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->addPreferencesFromResource(I)V

    .line 63
    invoke-direct {p0}, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->initFragment()V

    .line 65
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_1

    .line 66
    iget-object v1, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->m_launchVoicetalk:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v2, "launch_voicetalk"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 68
    :cond_1
    return-void
.end method

.method setOnSettingsContentClickListener(Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;)V
    .locals 0
    .param p1, "mClickListener"    # Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    .line 138
    return-void
.end method
