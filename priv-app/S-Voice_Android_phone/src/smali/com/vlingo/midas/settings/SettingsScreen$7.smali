.class Lcom/vlingo/midas/settings/SettingsScreen$7;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->initPreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 907
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 9
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 912
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 914
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    .line 915
    .local v2, "dialog_editbox":Landroid/widget/EditText;
    invoke-virtual {v2}, Landroid/widget/EditText;->selectAll()V

    .line 916
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 917
    new-array v3, v7, [Landroid/text/InputFilter;

    new-instance v4, Lcom/vlingo/midas/settings/SettingsScreen$LengthToastFilter;

    iget-object v5, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const/16 v6, 0x12c

    invoke-direct {v4, v5, v6}, Lcom/vlingo/midas/settings/SettingsScreen$LengthToastFilter;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;I)V

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 921
    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 922
    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 923
    sget v3, Lcom/vlingo/midas/R$string;->settings_incar_officeaddress_summary:I

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(I)V

    .line 924
    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v7, v3}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 926
    new-instance v3, Lcom/vlingo/midas/settings/SettingsScreen$7$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/SettingsScreen$7$1;-><init>(Lcom/vlingo/midas/settings/SettingsScreen$7;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 933
    new-instance v3, Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;

    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    iget-object v5, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v5}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v5

    invoke-virtual {v5}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/app/Dialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 934
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 935
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    const v4, 0x1020019

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 938
    .local v0, "button":Landroid/widget/Button;
    new-instance v3, Lcom/vlingo/midas/settings/SettingsScreen$7$2;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/SettingsScreen$7$2;-><init>(Lcom/vlingo/midas/settings/SettingsScreen$7;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 963
    .end local v0    # "button":Landroid/widget/Button;
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 964
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    const v4, 0x102001a

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 967
    .local v1, "button2":Landroid/widget/Button;
    new-instance v3, Lcom/vlingo/midas/settings/SettingsScreen$7$3;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/SettingsScreen$7$3;-><init>(Lcom/vlingo/midas/settings/SettingsScreen$7;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 981
    .end local v1    # "button2":Landroid/widget/Button;
    .end local v2    # "dialog_editbox":Landroid/widget/EditText;
    :cond_1
    return v8
.end method
