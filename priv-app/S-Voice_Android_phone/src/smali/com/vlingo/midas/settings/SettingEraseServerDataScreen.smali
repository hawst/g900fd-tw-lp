.class public Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;
.super Landroid/support/v4/app/FragmentActivity;
.source "SettingEraseServerDataScreen.java"


# instance fields
.field private fragment:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

.field public ssu:Lcom/vlingo/core/internal/util/SystemServicesUtil;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 16
    new-instance v0, Lcom/vlingo/core/internal/util/SystemServicesUtil;

    invoke-direct {v0, p0}, Lcom/vlingo/core/internal/util/SystemServicesUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->ssu:Lcom/vlingo/core/internal/util/SystemServicesUtil;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0xc

    .line 21
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->setRequestedOrientation(I)V

    .line 25
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 26
    .local v0, "titlebar":Landroid/app/ActionBar;
    if-eqz v0, :cond_1

    .line 27
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 28
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 39
    :cond_1
    :goto_0
    sget v1, Lcom/vlingo/midas/R$layout;->fragment_container:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->setContentView(I)V

    .line 41
    new-instance v1, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    invoke-direct {v1}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;-><init>()V

    iput-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->fragment:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    .line 42
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->fragment:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    if-eqz v1, :cond_2

    .line 43
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->fragment_placeholder:I

    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->fragment:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 45
    :cond_2
    return-void

    .line 30
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 32
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 33
    sget v1, Lcom/vlingo/midas/R$string;->erase_server_data_body:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 35
    :cond_4
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 36
    sget v1, Lcom/vlingo/midas/R$string;->erase_server_data_body:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 49
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 53
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 51
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreen;->finish()V

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
