.class public Lcom/vlingo/midas/settings/ImagePreference;
.super Landroid/preference/Preference;
.source "ImagePreference.java"


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private imageView:Landroid/widget/ImageView;

.field private visibility:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/4 v0, 0x4

    iput v0, p0, Lcom/vlingo/midas/settings/ImagePreference;->visibility:I

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/settings/ImagePreference;->bitmap:Landroid/graphics/Bitmap;

    .line 29
    sget v0, Lcom/vlingo/midas/R$layout;->preference_with_image:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/ImagePreference;->setWidgetLayoutResource(I)V

    .line 30
    return-void
.end method

.method private updateUI()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/midas/settings/ImagePreference;->imageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/vlingo/midas/settings/ImagePreference;->imageView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/vlingo/midas/settings/ImagePreference;->visibility:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 51
    iget-object v0, p0, Lcom/vlingo/midas/settings/ImagePreference;->imageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vlingo/midas/settings/ImagePreference;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method public getImageView()Landroid/widget/ImageView;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vlingo/midas/settings/ImagePreference;->imageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public isImageViewVisible()Z
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/vlingo/midas/settings/ImagePreference;->visibility:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 34
    .local v0, "v":Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 38
    :cond_0
    sget v1, Lcom/vlingo/midas/R$id;->preferenceImage:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vlingo/midas/settings/ImagePreference;->imageView:Landroid/widget/ImageView;

    .line 39
    invoke-direct {p0}, Lcom/vlingo/midas/settings/ImagePreference;->updateUI()V

    .line 40
    return-object v0
.end method

.method public setImageViewBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vlingo/midas/settings/ImagePreference;->bitmap:Landroid/graphics/Bitmap;

    .line 62
    invoke-direct {p0}, Lcom/vlingo/midas/settings/ImagePreference;->updateUI()V

    .line 63
    return-void
.end method

.method public setImageViewVisibility(I)V
    .locals 0
    .param p1, "vis"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/vlingo/midas/settings/ImagePreference;->visibility:I

    .line 57
    invoke-direct {p0}, Lcom/vlingo/midas/settings/ImagePreference;->updateUI()V

    .line 58
    return-void
.end method
