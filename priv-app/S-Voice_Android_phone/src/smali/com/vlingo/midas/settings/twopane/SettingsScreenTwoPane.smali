.class public Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;
.super Landroid/app/Activity;
.source "SettingsScreenTwoPane.java"

# interfaces
.implements Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;
.implements Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;


# static fields
.field public static final ADVANCED_FRAGMENT:I = 0x2

.field public static final GENERAL_FRAGMENT:I = 0x0

.field public static final ITEM_ABOUT:I = 0x2

.field public static final ITEM_ERASE:I = 0x3

.field public static final ITEM_EraseDataDetail:I = 0x4

.field public static final ITEM_LANGUAGE:I = 0x0

.field public static final ITEM_VOICEWAKEUP:I = 0x1

.field public static final WAKEUP_FRAGMENT:I = 0x1


# instance fields
.field contents_param:Landroid/widget/LinearLayout$LayoutParams;

.field header_param:Landroid/widget/LinearLayout$LayoutParams;

.field settingsDataFragment:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;

.field settingsHeaderFragment:Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;

.field settings_contents_ll:Landroid/widget/LinearLayout;

.field settings_header_ll:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsHeaderFragment:Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;

    .line 23
    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsDataFragment:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsDataFragment:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->onBackPressed()V

    .line 112
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 123
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 124
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->header_param:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x40c00000    # 6.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 125
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->contents_param:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x40800000    # 4.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 128
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->header_param:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x40e00000    # 7.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 129
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->contents_param:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x40400000    # 3.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v10, 0x400

    const/4 v12, 0x1

    .line 45
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 46
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    sget v9, Lcom/vlingo/midas/R$layout;->twopanes:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->setContentView(I)V

    .line 48
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    .line 49
    .local v6, "titlebar":Landroid/app/ActionBar;
    const/16 v9, 0x1c

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 50
    sget v9, Lcom/vlingo/midas/R$string;->app_settings:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 51
    invoke-virtual {v6, v12}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 52
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9, v10, v10}, Landroid/view/Window;->setFlags(II)V

    .line 56
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 57
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "privateFlags"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 58
    .local v4, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 59
    .local v5, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 60
    .local v1, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v0, 0x0

    .local v0, "currentPrivateFlags":I
    const/4 v8, 0x0

    .local v8, "valueofFlagsEnableStatusBar":I
    const/4 v7, 0x0

    .line 61
    .local v7, "valueofFlagsDisableTray":I
    invoke-virtual {v4, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 62
    invoke-virtual {v5, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 63
    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v7

    .line 64
    or-int/2addr v0, v8

    .line 65
    or-int/2addr v0, v7

    .line 66
    invoke-virtual {v4, v3, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 67
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 75
    .end local v0    # "currentPrivateFlags":I
    .end local v1    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v3    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v4    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v5    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v7    # "valueofFlagsDisableTray":I
    .end local v8    # "valueofFlagsEnableStatusBar":I
    :goto_0
    sget v9, Lcom/vlingo/midas/R$id;->settings_header_ll:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    iput-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settings_header_ll:Landroid/widget/LinearLayout;

    .line 76
    sget v9, Lcom/vlingo/midas/R$id;->settings_contents_ll:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    iput-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settings_contents_ll:Landroid/widget/LinearLayout;

    .line 77
    iget-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settings_header_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->header_param:Landroid/widget/LinearLayout$LayoutParams;

    .line 78
    iget-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settings_contents_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->contents_param:Landroid/widget/LinearLayout$LayoutParams;

    .line 79
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    if-ne v9, v12, :cond_1

    .line 80
    iget-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->header_param:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v10, 0x40c00000    # 6.0f

    iput v10, v9, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 81
    iget-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->contents_param:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v10, 0x40800000    # 4.0f

    iput v10, v9, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 87
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$id;->settings_header:I

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v9

    check-cast v9, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;

    iput-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsHeaderFragment:Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;

    .line 88
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    sget v10, Lcom/vlingo/midas/R$id;->settings_contents:I

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v9

    check-cast v9, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;

    iput-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsDataFragment:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;

    .line 89
    iget-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsHeaderFragment:Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;

    invoke-virtual {v9, p0}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->setOnHeadlineSelectedListener(Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;)V

    .line 90
    iget-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsDataFragment:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;

    invoke-virtual {v9, p0}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->setOnSettingsContentClickListener(Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;)V

    .line 91
    return-void

    .line 68
    :catch_0
    move-exception v2

    .line 69
    .local v2, "e":Ljava/lang/NoSuchFieldException;
    const-string/jumbo v9, "SettingsLanguageScreen"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "NoSuchFieldException "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/NoSuchFieldException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", continuing"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 70
    .end local v2    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v2

    .line 71
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v9, "SettingsLanguageScreen"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", continuing"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 83
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_0

    .line 84
    iget-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->header_param:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v10, 0x40e00000    # 7.0f

    iput v10, v9, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 85
    iget-object v9, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->contents_param:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v10, 0x40400000    # 3.0f

    iput v10, v9, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto/16 :goto_1
.end method

.method public onHeadlineSelected(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsDataFragment:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->loadPreferences(I)V

    .line 107
    return-void
.end method

.method public onItemSelected(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsDataFragment:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->loadFragment(I)V

    .line 117
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 135
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 139
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 137
    :pswitch_0
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsDataFragment:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment;->onBackPressed()V

    goto :goto_0

    .line 135
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public restartActivity()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->settingsHeaderFragment:Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->loadCategory()V

    .line 100
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->onHeadlineSelected(I)V

    .line 101
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->app_settings:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 102
    return-void
.end method
