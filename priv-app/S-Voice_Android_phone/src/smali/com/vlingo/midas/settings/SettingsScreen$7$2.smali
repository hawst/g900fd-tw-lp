.class Lcom/vlingo/midas/settings/SettingsScreen$7$2;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen$7;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen$7;)V
    .locals 0

    .prologue
    .line 938
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 942
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v3, v3, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 944
    .local v1, "officeAddress":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 946
    .local v2, "officeAddressCheck":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v3, v3

    if-gtz v3, :cond_1

    .line 947
    const-string/jumbo v3, "car_nav_office_address"

    const-string/jumbo v4, ""

    invoke-static {v3, v4}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    :goto_0
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v3, v3, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const-string/jumbo v4, "input_method"

    invoke-virtual {v3, v4}, Lcom/vlingo/midas/settings/SettingsScreen;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 953
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v3, v3, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 956
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v3, v3, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v3, v3, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 957
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$2;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v3, v3, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->cancel()V

    .line 959
    :cond_0
    return-void

    .line 949
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    const-string/jumbo v3, "car_nav_office_address"

    invoke-static {v3, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
