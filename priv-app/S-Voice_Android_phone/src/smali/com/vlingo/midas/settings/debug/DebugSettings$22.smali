.class Lcom/vlingo/midas/settings/debug/DebugSettings$22;
.super Ljava/lang/Object;
.source "DebugSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/debug/DebugSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V
    .locals 0

    .prologue
    .line 553
    iput-object p1, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$22;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v4, 0x1

    .line 556
    iget-object v1, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$22;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    invoke-virtual {v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 557
    .local v0, "pm":Landroid/content/pm/PackageManager;
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->supportsSVoiceAssociatedServiceOnly()Z

    move-result v1

    if-nez v1, :cond_0

    .line 558
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v1

    if-nez v1, :cond_1

    .line 559
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$22;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    invoke-virtual {v2}, Lcom/vlingo/midas/settings/debug/DebugSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.midas.settings.DrivingModeSettings"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 572
    :cond_0
    :goto_0
    return v4

    .line 565
    :cond_1
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$22;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    invoke-virtual {v2}, Lcom/vlingo/midas/settings/debug/DebugSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "com.vlingo.midas.settings.DrivingModeSettings"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v4, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_0
.end method
