.class public Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;
.super Landroid/app/Fragment;
.source "SettingLanguageScreenFragment.java"

# interfaces
.implements Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;
    }
.end annotation


# static fields
.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"

.field public static isLaunchBySettings:Z


# instance fields
.field private arrayAdapter:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;

.field private mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

.field private mListView:Landroid/widget/ListView;

.field private rootView:Landroid/view/View;

.field private settableLanguageDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private settableLanguages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->isLaunchBySettings:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->arrayAdapter:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;

    .line 43
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;

    .line 45
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguages:Ljava/util/ArrayList;

    .line 46
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguageDescriptions:Ljava/util/ArrayList;

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V
    .locals 1
    .param p1, "onFragmentLoadListener"    # Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->arrayAdapter:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;

    .line 43
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;

    .line 45
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguages:Ljava/util/ArrayList;

    .line 46
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguageDescriptions:Ljava/util/ArrayList;

    .line 56
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguages:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguageDescriptions:Ljava/util/ArrayList;

    return-object v0
.end method

.method private initializeUnsupportedLocales()V
    .locals 0

    .prologue
    .line 132
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->initializeUnsupportedLocales()V

    .line 133
    return-void
.end method

.method private static phoneSupportsLanguage(Ljava/lang/String;)Z
    .locals 1
    .param p0, "vlingoLocale"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-static {p0}, Lcom/vlingo/midas/settings/MidasSettings;->phoneSupportsLanguage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method initPreference()V
    .locals 7

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->initializeUnsupportedLocales()V

    .line 104
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguages()[Ljava/lang/CharSequence;

    move-result-object v4

    .line 105
    .local v4, "supportedLanguages":[Ljava/lang/CharSequence;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguageDescriptions()[Ljava/lang/CharSequence;

    move-result-object v3

    .line 106
    .local v3, "supportedLanguageDescriptions":[Ljava/lang/CharSequence;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguages:Ljava/util/ArrayList;

    .line 107
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguageDescriptions:Ljava/util/ArrayList;

    .line 109
    const-string/jumbo v5, "show_all_languages"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 110
    .local v2, "showAllLanguages":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_2

    .line 111
    aget-object v5, v4, v0

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "language":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->phoneSupportsLanguage(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    if-eqz v2, :cond_1

    .line 113
    :cond_0
    iget-object v5, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguages:Ljava/util/ArrayList;

    aget-object v6, v4, v0

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    iget-object v5, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguageDescriptions:Ljava/util/ArrayList;

    aget-object v6, v3, v0

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    .end local v1    # "language":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->onHeadlineSelected(I)V

    .line 218
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 91
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->settings_path_view:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 93
    .local v2, "view":Landroid/view/View;
    sget v3, Lcom/vlingo/midas/R$id;->fragment_description:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 94
    .local v1, "textView":Landroid/widget/TextView;
    sget v3, Lcom/vlingo/midas/R$id;->icon:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 95
    .local v0, "iconView":Landroid/widget/ImageView;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 96
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->settings_language_dialogtitle:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    if-eqz v3, :cond_0

    .line 98
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    const/4 v4, 0x1

    invoke-interface {v3, v2, v4}, Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;->updatePathHeader(Landroid/view/View;I)V

    .line 100
    .end local v0    # "iconView":Landroid/widget/ImageView;
    .end local v1    # "textView":Landroid/widget/TextView;
    .end local v2    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    sget v2, Lcom/vlingo/midas/R$layout;->widget_language_choice:I

    invoke-virtual {p1, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->rootView:Landroid/view/View;

    .line 64
    invoke-super {p0, p3}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->initPreference()V

    .line 66
    new-instance v2, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;-><init>(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;Landroid/app/Activity;)V

    iput-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->arrayAdapter:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;

    .line 67
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->rootView:Landroid/view/View;

    sget v3, Lcom/vlingo/midas/R$id;->listview:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;

    .line 68
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->arrayAdapter:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 69
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 70
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 71
    const-string/jumbo v2, "language"

    const-string/jumbo v3, "en-US"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "language":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguages:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 73
    .local v1, "selectedItem":I
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v1, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 74
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;

    new-instance v3, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$1;

    invoke-direct {v3, p0, v1}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$1;-><init>(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;I)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 85
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->rootView:Landroid/view/View;

    return-object v2
.end method

.method public onLanguageChanged()V
    .locals 8

    .prologue
    .line 141
    invoke-static {}, Lcom/vlingo/core/internal/audio/AudioPlaybackService;->isRunning()Z

    move-result v5

    if-nez v5, :cond_0

    .line 147
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$2;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$2;-><init>(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 153
    :cond_0
    const-string/jumbo v5, "language"

    const-string/jumbo v6, "en-US"

    invoke-static {v5, v6}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 159
    .local v3, "language":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 160
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v3, v5, v1}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;Landroid/content/SharedPreferences$Editor;)V

    .line 161
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :goto_0
    new-instance v5, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$3;

    invoke-direct {v5, p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$3;-><init>(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;)V

    const-wide/16 v6, 0xfa

    invoke-static {v5, v6, v7}, Lcom/vlingo/core/internal/util/ActivityUtil;->scheduleOnMainThread(Ljava/lang/Runnable;J)V

    .line 175
    invoke-static {}, Lcom/vlingo/core/facade/CoreManager;->getLmttClientEventHandler()Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/core/facade/lmtt/ILmttClientEventHandler;->onLanguageChanged()V

    .line 178
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v4

    .line 179
    .local v4, "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->updateParameters(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;)V

    .line 180
    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->convertToBargeinISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 181
    .local v2, "langToChange":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "voicetalk_language"

    invoke-static {v5, v6, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 182
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v7, "com.vlingo.LANGUAGE_CHANGED"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 183
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 184
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    invoke-virtual {v5}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->restartActivity()V

    .line 188
    :goto_1
    const/4 v5, 0x1

    sput-boolean v5, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->isLaunchBySettings:Z

    .line 189
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 190
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/midas/associatedapp/settings/SettingsUtils;->setAppName(Ljava/lang/String;)V

    .line 191
    const/4 v5, 0x0

    sput-boolean v5, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->isLaunchBySettings:Z

    .line 192
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->finishDialog()V

    .line 193
    return-void

    .line 162
    .end local v2    # "langToChange":Ljava/lang/String;
    .end local v4    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :catch_0
    move-exception v0

    .line 163
    .local v0, "ae":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    .line 186
    .end local v0    # "ae":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v2    # "langToChange":Ljava/lang/String;
    .restart local v4    # "phraseSpotterParams":Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto :goto_1
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 126
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 127
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "language":Ljava/lang/String;
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguages:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 129
    return-void
.end method
