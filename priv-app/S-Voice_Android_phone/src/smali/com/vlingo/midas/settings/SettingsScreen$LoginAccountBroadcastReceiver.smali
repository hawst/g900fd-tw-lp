.class Lcom/vlingo/midas/settings/SettingsScreen$LoginAccountBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/SettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoginAccountBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method private constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 2397
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$LoginAccountBroadcastReceiver;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;Lcom/vlingo/midas/settings/SettingsScreen$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vlingo/midas/settings/SettingsScreen;
    .param p2, "x1"    # Lcom/vlingo/midas/settings/SettingsScreen$1;

    .prologue
    .line 2397
    invoke-direct {p0, p1}, Lcom/vlingo/midas/settings/SettingsScreen$LoginAccountBroadcastReceiver;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 2400
    if-eqz p2, :cond_0

    .line 2401
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2402
    .local v0, "action":Ljava/lang/String;
    sget-object v1, Lcom/vlingo/midas/settings/SettingsScreen;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2403
    const-string/jumbo v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2404
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$LoginAccountBroadcastReceiver;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mhandler:Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$1100(Lcom/vlingo/midas/settings/SettingsScreen;)Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;->removeMessages(I)V

    .line 2405
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$LoginAccountBroadcastReceiver;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mhandler:Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$1100(Lcom/vlingo/midas/settings/SettingsScreen;)Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 2408
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    return-void
.end method
