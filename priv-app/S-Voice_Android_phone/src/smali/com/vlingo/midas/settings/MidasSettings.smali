.class public Lcom/vlingo/midas/settings/MidasSettings;
.super Lcom/vlingo/core/internal/settings/Settings;
.source "MidasSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/settings/MidasSettings$DeviceType;
    }
.end annotation


# static fields
.field public static final DEFAULT_CLIENT_VERSION:Ljava/lang/String; = "Q2"

.field public static final DEFAULT_DETAILED_TTS_FEEDBACK:Z = false

.field public static final DEFAULT_IS_EYES_FREE_MODE:Z = false

.field public static final DEFAULT_NETWORK_TIMEOUT_STRING:Ljava/lang/String; = "750"

.field public static final DEFAULT_SAFEREADER_DELAY:I = 0x1b58

.field public static final DEFAULT_USE_ECHO_CANCEL_FOR_SPOTTER:Z = true

.field public static final DEFAULT_WIDGET_DISPLAY_MAX:I = 0x0

.field public static FACEBOOK_APP_ID_DEFAULT:Ljava/lang/String; = null

.field private static final KEY_DATA_CHECK_MOBILE_ENABLED:Ljava/lang/String; = "data_check_mobile_enabled"

.field private static final KEY_DATA_CHECK_WLAN_ENABLED:Ljava/lang/String; = "data_check_wlan_enabled"

.field private static final KEY_DONT_ASK_BACKGROUND_DATA_CHECK_ENABLED:Ljava/lang/String; = "dont_ask_background_data_check_enabled"

.field private static final KEY_DONT_ASK_MOBILE_ENABLED:Ljava/lang/String; = "dont_ask_mobile_enabled"

.field private static final KEY_DONT_ASK_WLAN_ENABLED:Ljava/lang/String; = "dont_ask_wlan_enabled"

.field private static final KEY_EASY_MODE_SWITCH:Ljava/lang/String; = "easy_mode_switch"

.field private static final KEY_HANDS_FREE_INFORMATION_ACCEPTED:Ljava/lang/String; = "hands_free_information_accepted"

.field public static final KEY_HELP_VISIBLE:Ljava/lang/String; = "key_help_visible"

.field public static final KEY_IN_CAR_MODE:Ljava/lang/String; = "in_car_mode"

.field public static final KEY_IS_EYES_FREE_MODE:Ljava/lang/String; = "is_eyes_free_mode"

.field public static final KEY_LMTT_UPDATE_VERSION_CURRENT:Ljava/lang/String; = "lmtt.update.version"

.field public static final KEY_LMTT_UPDATE_VERSION_PREVIOUS:Ljava/lang/String; = ".previous"

.field public static final KEY_MANUALLY_TURNED_ON_PROMPT_IN_TALKBACK:Ljava/lang/String; = "manually_prompt_on_in_talkback"

.field private static final KEY_SAMSUNG_DISCLAIMER_ACCEPTED:Ljava/lang/String; = "samsung_disclaimer_accepted"

.field private static final KEY_SEAMLESS_WAKEUP_STARTED:Ljava/lang/String; = "is_seamless_wakeup_started"

.field public static final KEY_SET_WAKE_UP_COMMAND:Ljava/lang/String; = "key_set_wake_up_command"

.field public static final KEY_STREAM_VOLUME:Ljava/lang/String; = "stream_volume"

.field public static final KEY_SYSTEM_VOLUME:Ljava/lang/String; = "system_volume"

.field public static final KEY_USE_ECHO_CANCEL_FOR_SPOTTER:Ljava/lang/String; = "use_echo_cancel_for_spotter"

.field public static final MAIN_MENU:I = 0x1

.field private static final NOTIFICATION_PANEL_ACTIVE_APP_LIST:Ljava/lang/String; = "notification_panel_active_app_list"

.field public static final NO_RECEIVER_IN_CALL:Ljava/lang/String; = "no_receiver_in_call"

.field public static final SERVER_URL_KEYS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final TWITTER_CONSUMER_KEY_DEFAULT:Ljava/lang/String; = "AGv8Ps3AlFKrf2C1YoFkQ"

.field public static final TWITTER_CONSUMER_SECRET_DEFAULT:Ljava/lang/String; = "qeX5TCXa9HPDlpNmhPACOT7sUerHPmD91Oq9nYuw6Q"

.field public static final VOICE_WAKE_UP_TIME:Ljava/lang/String; = "preferences_voice_wake_up_time"

.field public static final WEIBO_APP_ID_DEFAULT:Ljava/lang/String; = "2291996457"

.field public static final WEIBO_REDIRECT_URL_DEFAULT:Ljava/lang/String; = "http://www.samsung.com/sec"

.field private static buildcarrier:Ljava/lang/String;

.field private static deviceName:Ljava/lang/String;

.field private static deviceType:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

.field static hasHWKey:Z

.field private static isASRMicOpened:Z

.field private static isNewAlarmConcept:Z

.field public static isShowingViewCoverUi:Z

.field static isSoftKeyEnabledKnown:Z

.field private static final locales:[Ljava/lang/String;

.field private static loggedPhoneLocales:Z

.field static mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private static mFeatureList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static mMusicLaunchedFromSvoice:Z

.field private static mSlook:Lcom/samsung/android/sdk/look/Slook;

.field private static mSvoiceLaunchedFromOutside:Z

.field static mWindowManager:Landroid/view/IWindowManager;

.field public static s_ReadMessage:Z

.field public static s_inCarMode:Z

.field public static s_nightTimer:Landroid/text/format/Time;

.field static supportServiceManager:Z

.field private static unsupportedLocales:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 81
    sput-boolean v3, Lcom/vlingo/midas/settings/MidasSettings;->isShowingViewCoverUi:Z

    .line 106
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->s_nightTimer:Landroid/text/format/Time;

    .line 108
    const-string/jumbo v0, "420500754634289"

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->FACEBOOK_APP_ID_DEFAULT:Ljava/lang/String;

    .line 119
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "SERVER_NAME"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string/jumbo v2, "SERVICES_HOST_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "EVENTLOG_HOST_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "HELLO_HOST_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "LMTT_HOST_NAME"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->SERVER_URL_KEYS:Ljava/util/List;

    .line 126
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getSystemSupportedLocales()[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->locales:[Ljava/lang/String;

    .line 128
    sput-boolean v3, Lcom/vlingo/midas/settings/MidasSettings;->loggedPhoneLocales:Z

    .line 130
    sput-boolean v3, Lcom/vlingo/midas/settings/MidasSettings;->isASRMicOpened:Z

    .line 136
    sput-boolean v3, Lcom/vlingo/midas/settings/MidasSettings;->mMusicLaunchedFromSvoice:Z

    .line 137
    sput-boolean v3, Lcom/vlingo/midas/settings/MidasSettings;->mSvoiceLaunchedFromOutside:Z

    .line 138
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v0

    const-string/jumbo v1, "ro.build.characteristics"

    invoke-virtual {v0, v1}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->deviceName:Ljava/lang/String;

    .line 139
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v0

    const-string/jumbo v1, "ro.product.name"

    invoke-virtual {v0, v1}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->buildcarrier:Ljava/lang/String;

    .line 140
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->mFeatureList:Ljava/util/HashMap;

    .line 142
    new-instance v0, Lcom/samsung/android/sdk/look/Slook;

    invoke-direct {v0}, Lcom/samsung/android/sdk/look/Slook;-><init>()V

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->mSlook:Lcom/samsung/android/sdk/look/Slook;

    .line 144
    sput-boolean v3, Lcom/vlingo/midas/settings/MidasSettings;->isNewAlarmConcept:Z

    .line 675
    sput-object v4, Lcom/vlingo/midas/settings/MidasSettings;->mWindowManager:Landroid/view/IWindowManager;

    .line 676
    sput-boolean v3, Lcom/vlingo/midas/settings/MidasSettings;->supportServiceManager:Z

    .line 677
    sput-object v4, Lcom/vlingo/midas/settings/MidasSettings;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/vlingo/core/internal/settings/Settings;-><init>()V

    .line 146
    return-void
.end method

.method public static SECRobotoLightFont()Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 1701
    const-string/jumbo v1, "system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v1}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 1703
    .local v0, "samsungSans_text":Landroid/graphics/Typeface;
    return-object v0
.end method

.method public static checkVoiceWakeupFeature()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1656
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1657
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v1, "wake_up_time"

    sget v2, Lcom/vlingo/midas/R$string;->only_is_s_voice:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->always_including_off:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1658
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->startAlwaysMicOnserviceIfNeeded()V

    .line 1659
    const-string/jumbo v1, "voice_wake_up"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1660
    const-string/jumbo v1, "preferences_voice_wake_up_time"

    invoke-static {v1, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1668
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.samsung.wakeupsettings.SVOICE_DSP_SETTING_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1669
    return-void

    .line 1662
    :cond_0
    const-string/jumbo v1, "voice_wake_up"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 1663
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1664
    const-string/jumbo v1, "preferences_voice_wake_up_time"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 1666
    :cond_1
    const-string/jumbo v1, "preferences_voice_wake_up_time"

    invoke-static {v1, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static convertToBargeinISOLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "vlingoLanguage"    # Ljava/lang/String;

    .prologue
    .line 745
    const-string/jumbo v0, "v-es-LA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 747
    const-string/jumbo p0, "es-US"

    .line 751
    .end local p0    # "vlingoLanguage":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 748
    .restart local p0    # "vlingoLanguage":Ljava/lang/String;
    :cond_1
    const-string/jumbo v0, "v-es-NA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    const-string/jumbo p0, "es-US"

    goto :goto_0
.end method

.method public static convertToISOLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "vlingoLanguage"    # Ljava/lang/String;

    .prologue
    .line 737
    const-string/jumbo v0, "v-es-LA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 738
    const-string/jumbo p0, "es-ES"

    .line 740
    .end local p0    # "vlingoLanguage":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method public static doNeedtoShowBackgroundData()Z
    .locals 1

    .prologue
    .line 581
    const/4 v0, 0x0

    return v0
.end method

.method public static forceContactEncoding(Landroid/content/Context;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 929
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 930
    .local v8, "rawContactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 931
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 933
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "raw_contact_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 942
    if-eqz v6, :cond_2

    .line 943
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    move v7, v9

    .line 944
    .local v7, "cursorEmpty":Z
    :goto_0
    if-nez v7, :cond_2

    .line 945
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 946
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    move v7, v9

    :goto_1
    goto :goto_0

    .end local v7    # "cursorEmpty":Z
    :cond_0
    move v7, v10

    .line 943
    goto :goto_0

    .restart local v7    # "cursorEmpty":Z
    :cond_1
    move v7, v10

    .line 946
    goto :goto_1

    .line 950
    .end local v7    # "cursorEmpty":Z
    :cond_2
    if-eqz v6, :cond_3

    .line 951
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 955
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/vlingo/core/internal/contacts/phoneticencoding/PhoneticEncodingUtils;->encodeContactData(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/util/List;)V

    .line 956
    return-void

    .line 950
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_4

    .line 951
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1
.end method

.method public static forceContactNormalization(Landroid/content/Context;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 897
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 898
    .local v8, "rawContactIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 899
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 901
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "raw_contact_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 910
    if-eqz v6, :cond_2

    .line 911
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    move v7, v9

    .line 912
    .local v7, "cursorEmpty":Z
    :goto_0
    if-nez v7, :cond_2

    .line 913
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 914
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    move v7, v9

    :goto_1
    goto :goto_0

    .end local v7    # "cursorEmpty":Z
    :cond_0
    move v7, v10

    .line 911
    goto :goto_0

    .restart local v7    # "cursorEmpty":Z
    :cond_1
    move v7, v10

    .line 914
    goto :goto_1

    .line 918
    .end local v7    # "cursorEmpty":Z
    :cond_2
    if-eqz v6, :cond_3

    .line 919
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 923
    :cond_3
    invoke-static {}, Lcom/vlingo/core/internal/contacts/contentprovider/DataTable;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/vlingo/core/internal/contacts/ContactDBNormalizeUtil;->normalizeContactData(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/util/List;)V

    .line 924
    return-void

    .line 918
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_4

    .line 919
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1
.end method

.method public static getDeviceIsNotUsModel()Z
    .locals 6

    .prologue
    .line 1405
    const/4 v1, 0x1

    .line 1407
    .local v1, "ret":Z
    const-string/jumbo v2, ""

    .line 1408
    .local v2, "sales_code":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1411
    .local v0, "get_sales_code":Ljava/lang/Process;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    const-string/jumbo v5, "getprop ro.csc.sales_code"

    invoke-virtual {v4, v5}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v0

    .line 1412
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1414
    .local v3, "sales_code_buffer_reader":Ljava/io/BufferedReader;
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 1415
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 1417
    if-eqz v2, :cond_1

    .line 1418
    const-string/jumbo v4, "SPR"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "VZW"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "ATT"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "USC"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "TMO"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "TMB"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "BST"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "VMU"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "XAS"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_1

    .line 1421
    :cond_0
    const/4 v1, 0x0

    .line 1426
    .end local v3    # "sales_code_buffer_reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    return v1

    .line 1423
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public static getDeviceIsNotVerizon()Z
    .locals 6

    .prologue
    .line 1383
    const/4 v1, 0x1

    .line 1385
    .local v1, "ret":Z
    const-string/jumbo v2, ""

    .line 1386
    .local v2, "sales_code":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1389
    .local v0, "get_sales_code":Ljava/lang/Process;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    const-string/jumbo v5, "getprop ro.csc.sales_code"

    invoke-virtual {v4, v5}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v0

    .line 1390
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1392
    .local v3, "sales_code_buffer_reader":Ljava/io/BufferedReader;
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 1393
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 1395
    if-eqz v2, :cond_0

    const-string/jumbo v4, "VZW"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    .line 1396
    const/4 v1, 0x0

    .line 1401
    .end local v3    # "sales_code_buffer_reader":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    return v1

    .line 1398
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public static getDeviceLanguage()Ljava/lang/String;
    .locals 10

    .prologue
    .line 861
    const-string/jumbo v7, ""

    .line 862
    .local v7, "popup_Langueage":Ljava/lang/String;
    const-string/jumbo v6, ""

    .line 863
    .local v6, "popup_Country":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 864
    .local v5, "mLocale":Ljava/lang/String;
    const/4 v4, 0x0

    .line 865
    .local v4, "mLanguage":Ljava/lang/Process;
    const/4 v3, 0x0

    .line 868
    .local v3, "mCountry":Ljava/lang/Process;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    const-string/jumbo v9, "getprop persist.sys.language"

    invoke-virtual {v8, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v4

    .line 870
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    const-string/jumbo v9, "getprop persist.sys.country"

    invoke-virtual {v8, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    .line 871
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-virtual {v4}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 873
    .local v1, "bLangueage":Ljava/io/BufferedReader;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-virtual {v3}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 875
    .local v0, "bCountry":Ljava/io/BufferedReader;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    .line 876
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 878
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 880
    invoke-virtual {v4}, Ljava/lang/Process;->destroy()V

    .line 881
    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    .line 882
    const/4 v4, 0x0

    .line 883
    const/4 v3, 0x0

    .line 884
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 885
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 892
    .end local v0    # "bCountry":Ljava/io/BufferedReader;
    .end local v1    # "bLangueage":Ljava/io/BufferedReader;
    :goto_0
    return-object v5

    .line 887
    :catch_0
    move-exception v2

    .line 889
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v8, "language"

    const-string/jumbo v9, "en-US"

    invoke-static {v8, v9}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public static getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;
    .locals 1

    .prologue
    .line 174
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->deviceType:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    return-object v0
.end method

.method private static getHasCarModeAppDefault()Z
    .locals 1

    .prologue
    .line 996
    sget-object v0, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->CAR_MODE:[Ljava/lang/String;

    invoke-static {v0}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasApp([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getHelpVisible()Z
    .locals 3

    .prologue
    .line 768
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "key_help_visible"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getHomeMainPrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 212
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->midas_greeting:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getInitiallyDisabledFeatures()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 190
    .local v0, "disabledFeatures":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;Ljava/lang/String;>;"
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasDialing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/samsungutils/SamsungPackageInfoProvider;->hasCallEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 191
    :cond_0
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->CALL:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v2, "Device"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasMemo()Z

    move-result v1

    if-nez v1, :cond_2

    .line 194
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MEMO:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v2, "Device"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MEMO_ADD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v2, "Device"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasMessaging()Z

    move-result v1

    if-nez v1, :cond_3

    .line 198
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->MESSAGING:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v2, "Device"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasVoiceRecorder()Z

    move-result v1

    if-nez v1, :cond_4

    .line 201
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->VOICERECORD:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v2, "Device"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasTimer()Z

    move-result v1

    if-nez v1, :cond_5

    .line 204
    sget-object v1, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->TIMER:Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;

    const-string/jumbo v2, "Device"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    :cond_5
    return-object v0
.end method

.method public static getModelName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 773
    const-string/jumbo v0, "midas_model_name"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMusicLaunchedFromSvoice()Z
    .locals 1

    .prologue
    .line 1232
    sget-boolean v0, Lcom/vlingo/midas/settings/MidasSettings;->mMusicLaunchedFromSvoice:Z

    return v0
.end method

.method private static getOfferCarModeDefault()Z
    .locals 3

    .prologue
    .line 969
    const/4 v1, 0x0

    .line 971
    .local v1, "toReturn":Z
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 972
    .local v0, "build_model":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 973
    const/4 v1, 0x0

    .line 988
    :goto_0
    if-eqz v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isDrivingModeSupported()Z

    move-result v2

    if-nez v2, :cond_0

    .line 989
    const/4 v1, 0x0

    .line 991
    :cond_0
    return v1

    .line 974
    :cond_1
    const-string/jumbo v2, "GT-I9505"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "GT-I9500"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SGH-I337"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SCH-I545"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SPH-L720"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SGH-M919"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SCH-R970"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SHV-E300"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SHV-E330"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SGH-N045"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "GT-I9502"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SCH-I959"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SC-04E"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SM-N900"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "ASH"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "Madrid"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "JS01"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "GT-I9506"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SM-G9105"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SC-01F"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SCL22"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SC-02F"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "SM-G910"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 982
    :cond_2
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 986
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static getSvoiceLaunchedFromOutside()Z
    .locals 1

    .prologue
    .line 1247
    sget-boolean v0, Lcom/vlingo/midas/settings/MidasSettings;->mSvoiceLaunchedFromOutside:Z

    return v0
.end method

.method private static getSystemSupportedLocales()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 812
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    move-result-object v2

    .line 813
    .local v2, "locales":[Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isLOSDevice()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 814
    array-length v3, v2

    new-array v0, v3, [Ljava/lang/String;

    .line 815
    .local v0, "convertedLocales":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 816
    aget-object v3, v2, v1

    const/16 v4, 0x2d

    const/16 v5, 0x5f

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    .line 815
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "convertedLocales":[Ljava/lang/String;
    .end local v1    # "i":I
    :cond_0
    move-object v0, v2

    .line 820
    :cond_1
    return-object v0
.end method

.method public static hasCarModeApp()Z
    .locals 2

    .prologue
    .line 661
    const-string/jumbo v0, "has_car_mode_app"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getHasCarModeAppDefault()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static hasFeature(Ljava/lang/String;)Z
    .locals 1
    .param p0, "feature"    # Ljava/lang/String;

    .prologue
    .line 1524
    if-eqz p0, :cond_0

    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->mFeatureList:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1525
    :cond_0
    const/4 v0, 0x0

    .line 1527
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->mFeatureList:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public static hasLSIChipset()Z
    .locals 2

    .prologue
    .line 1707
    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, "/system/wakeupdata/samsung/models_wa38.bin"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1708
    .local v0, "chipsetFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1709
    const/4 v1, 0x1

    .line 1711
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static hideActionBar(Landroid/app/ActionBar;)V
    .locals 2
    .param p0, "actionBar"    # Landroid/app/ActionBar;

    .prologue
    .line 1677
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1678
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, -0x1aff776e

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1679
    return-void
.end method

.method public static init(Landroid/content/SharedPreferences$Editor;)V
    .locals 3
    .param p0, "editor"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 178
    const-string/jumbo v1, "key_midas_first_run"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 179
    .local v0, "firstRun":Z
    if-eqz v0, :cond_0

    .line 180
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getInitiallyDisabledFeatures()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/settings/DynamicFeatureConfig;->initDisabledFeatures(Ljava/util/Map;)V

    .line 181
    const-string/jumbo v1, "key_midas_first_run"

    const/4 v2, 0x0

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 183
    :cond_0
    const-string/jumbo v1, "offer_app_car_mode"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getOfferCarModeDefault()Z

    move-result v2

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 184
    const-string/jumbo v1, "has_car_mode_app"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getHasCarModeAppDefault()Z

    move-result v2

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 185
    const-string/jumbo v1, "offer_phone_car_mode"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isDrivingModeSupported()Z

    move-result v2

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 186
    return-void
.end method

.method public static initializeUnsupportedLocales()V
    .locals 3

    .prologue
    .line 838
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    .line 839
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_AU"

    const-string/jumbo v2, "en_GB"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 840
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_BE"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 841
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_CA"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 842
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_HK"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_IE"

    const-string/jumbo v2, "en_GB"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 844
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_IN"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 845
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_NZ"

    const-string/jumbo v2, "en_GB"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 846
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_PH"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 847
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_SG"

    const-string/jumbo v2, "en_US"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 848
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "en_ZA"

    const-string/jumbo v2, "en_GB"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 850
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "fr_BE"

    const-string/jumbo v2, "fr_FR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 851
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "fr_CA"

    const-string/jumbo v2, "fr_FR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 852
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "fr_LU"

    const-string/jumbo v2, "fr_FR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 853
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "fr_CH"

    const-string/jumbo v2, "fr_FR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 855
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "de_AT"

    const-string/jumbo v2, "de_DE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 856
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "de_CH"

    const-string/jumbo v2, "de_DE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 857
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    const-string/jumbo v1, "de_LU"

    const-string/jumbo v2, "de_DE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 858
    return-void
.end method

.method public static isATTDevice()Z
    .locals 2

    .prologue
    .line 1552
    const-string/jumbo v0, "ATT"

    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isAllNotificationsAccepted()Z
    .locals 3

    .prologue
    .line 623
    const-string/jumbo v1, "all_notifications_accepted"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 626
    .local v0, "accepted":Z
    return v0
.end method

.method public static isAnyDSPWakeupEnabled()Z
    .locals 1

    .prologue
    .line 1624
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDSPWakeupEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isGoogleDSPWakeupEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCallActive()Z
    .locals 3

    .prologue
    .line 1587
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1588
    .local v0, "manager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1589
    const/4 v1, 0x1

    .line 1591
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isCarModeEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1596
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "car_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isCarModeSupported()Z
    .locals 7

    .prologue
    .line 1050
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x12

    if-ge v5, v6, :cond_1

    .line 1051
    const/4 v4, 0x1

    .line 1064
    .local v1, "active_app_list":Ljava/lang/String;
    .local v2, "context":Landroid/content/Context;
    .local v4, "isSupported":Z
    :cond_0
    return v4

    .line 1053
    .end local v1    # "active_app_list":Ljava/lang/String;
    .end local v2    # "context":Landroid/content/Context;
    .end local v4    # "isSupported":Z
    :cond_1
    const/4 v4, 0x0

    .line 1054
    .restart local v4    # "isSupported":Z
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1055
    .restart local v2    # "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "notification_panel_active_app_list"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1056
    .restart local v1    # "active_app_list":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1057
    const-string/jumbo v5, ";"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1058
    .local v0, "activeApps":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v0

    if-ge v3, v5, :cond_0

    .line 1059
    const-string/jumbo v5, "CarMode"

    aget-object v6, v0, v3

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1060
    const/4 v4, 0x1

    .line 1058
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static isCoverOpened()Z
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 680
    const/4 v1, 0x1

    .line 682
    .local v1, "covered":Z
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 683
    new-instance v7, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    sput-object v7, Lcom/vlingo/midas/settings/MidasSettings;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 684
    sget-object v7, Lcom/vlingo/midas/settings/MidasSettings;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v5

    .line 686
    .local v5, "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v7

    if-nez v7, :cond_0

    .line 687
    const/4 v1, 0x0

    .line 715
    .end local v5    # "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    :cond_0
    :goto_0
    return v1

    .line 689
    :cond_1
    sget-object v7, Lcom/vlingo/midas/settings/MidasSettings;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v7, :cond_3

    .line 691
    :try_start_0
    const-string/jumbo v7, "android.os.ServiceManager"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 693
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v7, "getService"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v0, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 694
    .local v3, "get":Ljava/lang/reflect/Method;
    const/4 v7, 0x0

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string/jumbo v10, "window"

    aput-object v10, v8, v9

    invoke-virtual {v3, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 696
    .local v6, "result":Ljava/lang/Object;
    instance-of v7, v6, Landroid/os/IBinder;

    if-eqz v7, :cond_2

    .line 697
    check-cast v6, Landroid/os/IBinder;

    .end local v6    # "result":Ljava/lang/Object;
    invoke-static {v6}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v7

    sput-object v7, Lcom/vlingo/midas/settings/MidasSettings;->mWindowManager:Landroid/view/IWindowManager;

    .line 698
    sget-object v7, Lcom/vlingo/midas/settings/MidasSettings;->mWindowManager:Landroid/view/IWindowManager;

    if-eqz v7, :cond_2

    .line 699
    sget-object v7, Lcom/vlingo/midas/settings/MidasSettings;->mWindowManager:Landroid/view/IWindowManager;

    invoke-interface {v7}, Landroid/view/IWindowManager;->isCoverOpen()Z

    move-result v1

    .line 702
    :cond_2
    const/4 v7, 0x1

    sput-boolean v7, Lcom/vlingo/midas/settings/MidasSettings;->supportServiceManager:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 704
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "get":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v4

    .line 705
    .local v4, "ignored":Ljava/lang/Throwable;
    sput-boolean v11, Lcom/vlingo/midas/settings/MidasSettings;->supportServiceManager:Z

    goto :goto_0

    .line 710
    .end local v4    # "ignored":Ljava/lang/Throwable;
    :cond_3
    :try_start_1
    sget-object v7, Lcom/vlingo/midas/settings/MidasSettings;->mWindowManager:Landroid/view/IWindowManager;

    invoke-interface {v7}, Landroid/view/IWindowManager;->isCoverOpen()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    goto :goto_0

    .line 711
    :catch_1
    move-exception v2

    .line 712
    .local v2, "e":Ljava/lang/Throwable;
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public static isDataCheckMobileEnabled()Z
    .locals 3

    .prologue
    .line 560
    const-string/jumbo v1, "data_check_mobile_enabled"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 563
    .local v0, "enabled":Z
    return v0
.end method

.method public static isDataCheckWlanEnabled()Z
    .locals 3

    .prologue
    .line 549
    const-string/jumbo v1, "data_check_wlan_enabled"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 552
    .local v0, "enabled":Z
    return v0
.end method

.method public static isDontAskMobileEnabled()Z
    .locals 3

    .prologue
    .line 592
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 593
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string/jumbo v1, "dont_ask_mobile_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isDontAskWlanEnabled()Z
    .locals 3

    .prologue
    .line 604
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 605
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string/jumbo v1, "dont_ask_wlan_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isDrivingModeSupported()Z
    .locals 7

    .prologue
    .line 1032
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x12

    if-ge v5, v6, :cond_1

    .line 1033
    const/4 v4, 0x1

    .line 1046
    .local v1, "active_app_list":Ljava/lang/String;
    .local v2, "context":Landroid/content/Context;
    .local v4, "isSupported":Z
    :cond_0
    return v4

    .line 1035
    .end local v1    # "active_app_list":Ljava/lang/String;
    .end local v2    # "context":Landroid/content/Context;
    .end local v4    # "isSupported":Z
    :cond_1
    const/4 v4, 0x0

    .line 1036
    .restart local v4    # "isSupported":Z
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1037
    .restart local v2    # "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "notification_panel_active_app_list"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1038
    .restart local v1    # "active_app_list":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1039
    const-string/jumbo v5, ";"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1040
    .local v0, "activeApps":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v0

    if-ge v3, v5, :cond_0

    .line 1041
    const-string/jumbo v5, "DrivingMode"

    aget-object v6, v0, v3

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1042
    const/4 v4, 0x1

    .line 1040
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static isEMailReadbackEnabled()Z
    .locals 2

    .prologue
    .line 756
    const-string/jumbo v0, "safereader_email_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isEasyModeOn()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1223
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "easy_mode_switch"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1225
    .local v0, "isOn":I
    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isEdgeEnabled()Z
    .locals 2

    .prologue
    .line 1687
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->mSlook:Lcom/samsung/android/sdk/look/Slook;

    if-eqz v0, :cond_0

    .line 1688
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->mSlook:Lcom/samsung/android/sdk/look/Slook;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/look/Slook;->isFeatureEnabled(I)Z

    move-result v0

    .line 1689
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isGoogleDSPWakeupEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1618
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string/jumbo v1, "gsa_alwayson_preference_enabled"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "gsa_alwayson_language_support"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static isH_Device()Z
    .locals 1

    .prologue
    .line 1190
    invoke-static {}, Lcom/samsung/model/DeviceModelManager;->isHDevice()Z

    move-result v0

    return v0
.end method

.method public static isHandsFreeInformationAccepted()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 483
    const-string/jumbo v2, "hands_free_information_accepted"

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 485
    .local v0, "accepted":Z
    if-nez v0, :cond_0

    .line 487
    const-string/jumbo v2, "hands_free_information_accepted"

    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->getData(Ljava/lang/String;)[B

    move-result-object v1

    .line 488
    .local v1, "val":[B
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    aget-byte v2, v1, v3

    if-ne v2, v4, :cond_0

    .line 489
    const/4 v0, 0x1

    .line 490
    const-string/jumbo v2, "hands_free_information_accepted"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 493
    .end local v1    # "val":[B
    :cond_0
    return v0
.end method

.method public static isHomeKeyEnabled()Z
    .locals 10

    .prologue
    .line 223
    sget-boolean v6, Lcom/vlingo/midas/settings/MidasSettings;->isSoftKeyEnabledKnown:Z

    if-nez v6, :cond_0

    .line 232
    const/4 v5, 0x0

    .line 234
    .local v5, "serviceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    const-string/jumbo v6, "android.os.ServiceManager"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 239
    :goto_0
    if-eqz v5, :cond_0

    .line 240
    const/4 v3, 0x0

    .line 242
    .local v3, "getService":Ljava/lang/reflect/Method;
    :try_start_1
    const-string/jumbo v6, "getService"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 247
    :goto_1
    if-eqz v3, :cond_0

    .line 248
    const/4 v1, 0x0

    .line 250
    .local v1, "binder":Landroid/os/IBinder;
    const/4 v6, 0x0

    const/4 v7, 0x1

    :try_start_2
    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v9, "window"

    aput-object v9, v7, v8

    invoke-virtual {v3, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Landroid/os/IBinder;

    move-object v1, v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4

    .line 259
    :goto_2
    if-eqz v1, :cond_0

    .line 260
    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v4

    .line 261
    .local v4, "iwm":Landroid/view/IWindowManager;
    if-eqz v4, :cond_0

    .line 263
    :try_start_3
    invoke-interface {v4}, Landroid/view/IWindowManager;->hasNavigationBar()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 264
    const/4 v6, 0x0

    sput-boolean v6, Lcom/vlingo/midas/settings/MidasSettings;->hasHWKey:Z

    .line 265
    const/4 v6, 0x1

    sput-boolean v6, Lcom/vlingo/midas/settings/MidasSettings;->isSoftKeyEnabledKnown:Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_5

    .line 279
    .end local v1    # "binder":Landroid/os/IBinder;
    .end local v3    # "getService":Ljava/lang/reflect/Method;
    .end local v4    # "iwm":Landroid/view/IWindowManager;
    :cond_0
    :goto_3
    sget-boolean v6, Lcom/vlingo/midas/settings/MidasSettings;->hasHWKey:Z

    return v6

    .line 235
    :catch_0
    move-exception v2

    .line 236
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 243
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v3    # "getService":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v2

    .line 244
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 251
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v1    # "binder":Landroid/os/IBinder;
    :catch_2
    move-exception v2

    .line 252
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 253
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v2

    .line 254
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 255
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v2

    .line 256
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_2

    .line 267
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v4    # "iwm":Landroid/view/IWindowManager;
    :cond_1
    const/4 v6, 0x1

    :try_start_4
    sput-boolean v6, Lcom/vlingo/midas/settings/MidasSettings;->hasHWKey:Z

    .line 268
    const/4 v6, 0x1

    sput-boolean v6, Lcom/vlingo/midas/settings/MidasSettings;->isSoftKeyEnabledKnown:Z
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_5

    goto :goto_3

    .line 270
    :catch_5
    move-exception v2

    .line 271
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3
.end method

.method public static isInCarFunctionSupported()Z
    .locals 1

    .prologue
    .line 1068
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->offerCarMode()Z

    move-result v0

    return v0
.end method

.method public static isInCarMode()Z
    .locals 1

    .prologue
    .line 641
    sget-boolean v0, Lcom/vlingo/midas/settings/MidasSettings;->s_inCarMode:Z

    return v0
.end method

.method public static isJDevice()Z
    .locals 1

    .prologue
    .line 1194
    invoke-static {}, Lcom/samsung/model/DeviceModelManager;->isJDevice()Z

    move-result v0

    return v0
.end method

.method public static isKDDIPhone()Z
    .locals 6

    .prologue
    .line 1434
    const/4 v1, 0x0

    .line 1436
    .local v1, "ret":Z
    const-string/jumbo v2, ""

    .line 1437
    .local v2, "sales_code":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1440
    .local v0, "get_sales_code":Ljava/lang/Process;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    const-string/jumbo v5, "getprop ro.csc.sales_code"

    invoke-virtual {v4, v5}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v0

    .line 1441
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1443
    .local v3, "sales_code_buffer_reader":Ljava/io/BufferedReader;
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 1444
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 1446
    if-eqz v2, :cond_0

    const-string/jumbo v4, "KDI"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    .line 1447
    const/4 v1, 0x1

    .line 1452
    .end local v3    # "sales_code_buffer_reader":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    return v1

    .line 1449
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public static isKitkatGUI()Z
    .locals 2

    .prologue
    .line 1283
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    .line 1284
    const/4 v0, 0x1

    .line 1286
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKitkatPhoneExtraHighGUI()Z
    .locals 2

    .prologue
    .line 1496
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_EXTRA_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_0

    .line 1498
    const/4 v0, 0x1

    .line 1500
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKitkatPhoneGUI()Z
    .locals 2

    .prologue
    .line 1260
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_EXTRA_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_1

    .line 1263
    :cond_0
    const/4 v0, 0x1

    .line 1265
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKitkatPhoneHighGUI()Z
    .locals 2

    .prologue
    .line 1270
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_0

    .line 1272
    const/4 v0, 0x1

    .line 1274
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKitkatPhoneLowGUI()Z
    .locals 2

    .prologue
    .line 1290
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_0

    .line 1292
    const/4 v0, 0x1

    .line 1294
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKitkatTabletGUI()Z
    .locals 3

    .prologue
    const/16 v2, 0x12

    .line 1312
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v0, v2, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v0, v2, :cond_2

    .line 1317
    :cond_1
    const/4 v0, 0x1

    .line 1319
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKitkatTabletHighGUI()Z
    .locals 2

    .prologue
    .line 1330
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_0

    .line 1332
    const/4 v0, 0x1

    .line 1334
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKitkatTabletLowGUI()Z
    .locals 2

    .prologue
    .line 1345
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    .line 1347
    const/4 v0, 0x1

    .line 1349
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLOSDevice()Z
    .locals 2

    .prologue
    .line 1693
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-le v0, v1, :cond_0

    .line 1694
    const/4 v0, 0x1

    .line 1696
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMobileDataConnected()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 609
    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasDialing()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSimInserted()Z

    move-result v2

    if-nez v2, :cond_1

    .line 618
    .local v0, "mobileDataValue":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 613
    .end local v0    # "mobileDataValue":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "mobile_data"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 615
    .restart local v0    # "mobileDataValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 616
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isMusicEchoCancellerSupported()Z
    .locals 1

    .prologue
    .line 1367
    const/4 v0, 0x1

    return v0
.end method

.method public static isNewAlarmConcept()Z
    .locals 1

    .prologue
    .line 1716
    sget-boolean v0, Lcom/vlingo/midas/settings/MidasSettings;->isNewAlarmConcept:Z

    return v0
.end method

.method public static isNightMode()Z
    .locals 2

    .prologue
    .line 730
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->s_nightTimer:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 731
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->s_nightTimer:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings;->s_nightTimer:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    .line 732
    const/4 v0, 0x0

    .line 733
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isNote4Device()Z
    .locals 1

    .prologue
    .line 1198
    invoke-static {}, Lcom/samsung/model/DeviceModelManager;->isNote4Device()Z

    move-result v0

    return v0
.end method

.method public static isSamsungDSPWakeupEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1612
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string/jumbo v1, "voice_wake_up"

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "preferences_voice_wake_up_time"

    invoke-static {v1, v0}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSamsungDisclaimerAccepted()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 504
    const-string/jumbo v2, "samsung_disclaimer_accepted"

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 506
    .local v0, "accepted":Z
    if-nez v0, :cond_0

    .line 508
    const-string/jumbo v2, "samsung_disclaimer_accepted"

    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->getData(Ljava/lang/String;)[B

    move-result-object v1

    .line 509
    .local v1, "val":[B
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    aget-byte v2, v1, v3

    if-ne v2, v4, :cond_0

    .line 510
    const/4 v0, 0x1

    .line 511
    const-string/jumbo v2, "samsung_disclaimer_accepted"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 516
    .end local v1    # "val":[B
    :cond_0
    return v0
.end method

.method public static isSeamlessWakeupStarted()Z
    .locals 3

    .prologue
    .line 1606
    const-string/jumbo v1, "is_seamless_wakeup_started"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1608
    .local v0, "wakeupStarted":Z
    return v0
.end method

.method public static isShannonAP()Z
    .locals 3

    .prologue
    .line 1000
    const/4 v1, 0x0

    .line 1001
    .local v1, "toReturn":Z
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 1002
    .local v0, "build_model":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1003
    const/4 v1, 0x0

    .line 1009
    :goto_0
    return v1

    .line 1004
    :cond_0
    const-string/jumbo v2, "SHV-E500"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1005
    const/4 v1, 0x1

    goto :goto_0

    .line 1007
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSimInserted()Z
    .locals 5

    .prologue
    .line 1371
    const/4 v0, 0x1

    .line 1372
    .local v0, "isSimInserted":Z
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 1373
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    .line 1374
    .local v1, "simState":I
    packed-switch v1, :pswitch_data_0

    .line 1379
    :goto_0
    return v0

    .line 1376
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1374
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static isSoundDetectorSettingOn()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1628
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1630
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "sound_detector"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "doorbell_detector"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    .line 1636
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isTOSAccepted()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 533
    const-string/jumbo v2, "tos_accepted"

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 535
    .local v0, "accepted":Z
    if-nez v0, :cond_0

    .line 537
    const-string/jumbo v2, "tos_accepted"

    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->getData(Ljava/lang/String;)[B

    move-result-object v1

    .line 538
    .local v1, "val":[B
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    aget-byte v2, v1, v3

    if-ne v2, v4, :cond_0

    .line 539
    const/4 v0, 0x1

    .line 540
    const-string/jumbo v2, "tos_accepted"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 545
    .end local v1    # "val":[B
    :cond_0
    return v0
.end method

.method public static isTPhoneGUI()Z
    .locals 3

    .prologue
    .line 1576
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "ro.build.product"

    invoke-virtual {v1, v2}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1577
    .local v0, "sales_code":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 1578
    const-string/jumbo v1, "tr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "tb"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1580
    :cond_0
    const/4 v1, 0x1

    .line 1583
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isTalkBackOn(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1481
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v0

    return v0
.end method

.method public static isUSEnglishEnabled()Z
    .locals 6

    .prologue
    .line 283
    const/4 v0, 0x1

    .line 285
    .local v0, "isEnglishUK":Z
    const-string/jumbo v1, ""

    .line 286
    .local v1, "isUS_MODEL":Ljava/lang/String;
    const/4 v3, 0x0

    .line 289
    .local v3, "isUS_PRODUCT_MODEL":Ljava/lang/Process;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    const-string/jumbo v5, "getprop ro.csc.sales_code"

    invoke-virtual {v4, v5}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    .line 290
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v3}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 292
    .local v2, "isUS_MODEL_BUFFER_READER":Ljava/io/BufferedReader;
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 293
    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    .line 294
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 295
    if-eqz v1, :cond_1

    .line 296
    const-string/jumbo v4, "VZW"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "ATT"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_1

    .line 298
    :cond_0
    const/4 v0, 0x0

    .line 304
    .end local v2    # "isUS_MODEL_BUFFER_READER":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    return v0

    .line 301
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public static isUS_Device()Z
    .locals 2

    .prologue
    .line 1555
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v0

    .line 1556
    .local v0, "vendor":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 1557
    const-string/jumbo v1, "ATT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "TMO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "VZW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "USC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "SPR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "TMB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1559
    :cond_0
    const/4 v1, 0x1

    .line 1562
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isUnderKitket()Z
    .locals 2

    .prologue
    .line 1299
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-gt v0, v1, :cond_0

    .line 1300
    const/4 v0, 0x1

    .line 1302
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUsaFeature()Z
    .locals 3

    .prologue
    .line 1488
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "ro.csc.country_code"

    invoke-virtual {v1, v2}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1489
    .local v0, "countryCode":Ljava/lang/String;
    const-string/jumbo v1, "USA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private static isWBSDevice()Z
    .locals 3

    .prologue
    .line 1013
    const/4 v1, 0x0

    .line 1014
    .local v1, "toReturn":Z
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 1015
    .local v0, "build_model":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1016
    const/4 v1, 0x0

    .line 1022
    :goto_0
    return v1

    .line 1017
    :cond_0
    const-string/jumbo v2, "SHV-E210"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "SHW-M440S"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1018
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1020
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isWifiOnly(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1531
    const-string/jumbo v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1534
    .local v0, "cm":Landroid/net/ConnectivityManager;
    const/4 v2, 0x0

    .line 1536
    .local v2, "isNetworkSupported":Z
    :try_start_0
    const-class v4, Landroid/net/ConnectivityManager;

    const-string/jumbo v7, "isNetworkSupported"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    sget-object v10, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v10, v8, v9

    invoke-virtual {v4, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 1537
    .local v3, "networksupport":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v7

    invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v2

    .line 1548
    .end local v3    # "networksupport":Ljava/lang/reflect/Method;
    :goto_0
    if-nez v2, :cond_0

    move v4, v5

    :goto_1
    return v4

    .line 1538
    :catch_0
    move-exception v1

    .line 1539
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 1540
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 1541
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 1542
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 1543
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1544
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 1545
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    :cond_0
    move v4, v6

    .line 1548
    goto :goto_1
.end method

.method public static isZeroDevice()Z
    .locals 3

    .prologue
    .line 1566
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "ro.build.product"

    invoke-virtual {v1, v2}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1567
    .local v0, "sales_code":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1568
    const-string/jumbo v1, "zero"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1569
    const/4 v1, 0x1

    .line 1572
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static languageSupportListCheck()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    .line 332
    const/4 v10, 0x0

    .line 333
    .local v10, "supportedInLangSet":Z
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/vlingo/midas/settings/MidasSettings;->getISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 334
    .local v4, "isoLanguage":Ljava/lang/String;
    const-string/jumbo v12, "language"

    const-string/jumbo v13, "en-US"

    invoke-static {v12, v13}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 336
    .local v6, "language":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguages()[Ljava/lang/CharSequence;

    move-result-object v11

    .line 337
    .local v11, "supportedLanguages":[Ljava/lang/CharSequence;
    move-object v0, v11

    .local v0, "arr$":[Ljava/lang/CharSequence;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v7, :cond_0

    aget-object v5, v0, v3

    .line 338
    .local v5, "lang":Ljava/lang/CharSequence;
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-ne v12, v14, :cond_2

    invoke-static {v6}, Lcom/vlingo/midas/settings/MidasSettings;->phoneSupportsLanguage(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 340
    const/4 v10, 0x1

    .line 344
    .end local v5    # "lang":Ljava/lang/CharSequence;
    :cond_0
    const-string/jumbo v12, "show_all_languages"

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 347
    .local v9, "showAllLanguages":Z
    if-eq v10, v14, :cond_1

    if-nez v9, :cond_1

    .line 349
    const-string/jumbo v4, "en-US"

    .line 350
    const-string/jumbo v12, "language"

    const-string/jumbo v13, "en-US"

    invoke-static {v12, v13}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v12

    iget v2, v12, Landroid/content/res/Configuration;->fontScale:F

    .line 357
    .local v2, "font_scale":F
    invoke-static {v4}, Lcom/vlingo/midas/settings/MidasSettings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v8

    .line 358
    .local v8, "locale":Ljava/util/Locale;
    invoke-static {v8}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 359
    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V

    .line 360
    .local v1, "config":Landroid/content/res/Configuration;
    iput-object v8, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 361
    iput v2, v1, Landroid/content/res/Configuration;->fontScale:F

    .line 362
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v1, v13}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 365
    const-string/jumbo v12, "language"

    const-string/jumbo v13, "en-US"

    invoke-static {v12, v13}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 370
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string/jumbo v13, "voicetalk_language"

    invoke-static {v6}, Lcom/vlingo/midas/settings/MidasSettings;->convertToBargeinISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 371
    return-void

    .line 337
    .end local v1    # "config":Landroid/content/res/Configuration;
    .end local v2    # "font_scale":F
    .end local v8    # "locale":Ljava/util/Locale;
    .end local v9    # "showAllLanguages":Z
    .restart local v5    # "lang":Ljava/lang/CharSequence;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static makeFeatureForTablet()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1506
    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings;->deviceName:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings;->deviceName:Ljava/lang/String;

    const-string/jumbo v4, "tablet"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1507
    .local v0, "isTablet":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1521
    :goto_1
    return-void

    .end local v0    # "isTablet":Ljava/lang/Boolean;
    :cond_0
    move v1, v3

    .line 1506
    goto :goto_0

    .line 1512
    .restart local v0    # "isTablet":Ljava/lang/Boolean;
    :cond_1
    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings;->mFeatureList:Ljava/util/HashMap;

    const-string/jumbo v4, "no_receiver_in_call"

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings;->buildcarrier:Ljava/lang/String;

    const-string/jumbo v6, "lt03lte"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings;->buildcarrier:Ljava/lang/String;

    const-string/jumbo v6, "lt033g"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings;->buildcarrier:Ljava/lang/String;

    const-string/jumbo v6, "viennalte"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings;->buildcarrier:Ljava/lang/String;

    const-string/jumbo v6, "v1a3g"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings;->buildcarrier:Ljava/lang/String;

    const-string/jumbo v6, "chagalllte"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings;->buildcarrier:Ljava/lang/String;

    const-string/jumbo v6, "chagallwifi"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings;->buildcarrier:Ljava/lang/String;

    const-string/jumbo v6, "matisse3gzc"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings;->buildcarrier:Ljava/lang/String;

    const-string/jumbo v6, "matisselte"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/vlingo/midas/settings/MidasSettings;->buildcarrier:Ljava/lang/String;

    const-string/jumbo v6, "milletwifi"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    move v3, v2

    :cond_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static offerCarMode()Z
    .locals 2

    .prologue
    .line 651
    const-string/jumbo v0, "offer_app_car_mode"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getOfferCarModeDefault()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static offerPhoneCarMode()Z
    .locals 2

    .prologue
    .line 672
    const-string/jumbo v0, "offer_phone_car_mode"

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isDrivingModeSupported()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static phoneSupportsLanguage(Ljava/lang/String;)Z
    .locals 5
    .param p0, "vlingoLocale"    # Ljava/lang/String;

    .prologue
    .line 790
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->initializeUnsupportedLocales()V

    .line 791
    const-string/jumbo v3, "v-"

    invoke-virtual {p0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 792
    invoke-static {p0}, Lcom/vlingo/midas/settings/MidasSettings;->phoneSupportsVlingoSpecificLanguage(Ljava/lang/String;)Z

    move-result v3

    .line 808
    :goto_0
    return v3

    .line 795
    :cond_0
    invoke-static {p0}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 797
    .local v2, "isoLocale":Ljava/lang/String;
    const/16 v3, 0x2d

    const/16 v4, 0x5f

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    .line 799
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v3, Lcom/vlingo/midas/settings/MidasSettings;->locales:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 800
    sget-object v3, Lcom/vlingo/midas/settings/MidasSettings;->locales:[Ljava/lang/String;

    aget-object v0, v3, v1

    .line 801
    .local v0, "foundLocale":Ljava/lang/String;
    sget-object v3, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 802
    sget-object v3, Lcom/vlingo/midas/settings/MidasSettings;->unsupportedLocales:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "foundLocale":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 804
    .restart local v0    # "foundLocale":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 805
    const/4 v3, 0x1

    goto :goto_0

    .line 799
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 808
    .end local v0    # "foundLocale":Ljava/lang/String;
    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static phoneSupportsVlingoSpecificLanguage(Ljava/lang/String;)Z
    .locals 7
    .param p0, "vlingoLocale"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x0

    .line 825
    const/4 v4, 0x4

    invoke-virtual {p0, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 826
    .local v2, "language":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v4, Lcom/vlingo/midas/settings/MidasSettings;->locales:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 827
    sget-object v4, Lcom/vlingo/midas/settings/MidasSettings;->locales:[Ljava/lang/String;

    aget-object v0, v4, v1

    .line 828
    .local v0, "foundLocale":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    .line 829
    invoke-virtual {v0, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 830
    const/4 v3, 0x1

    .line 834
    .end local v0    # "foundLocale":Ljava/lang/String;
    :cond_0
    return v3

    .line 826
    .restart local v0    # "foundLocale":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static playCoverUnsupportedTTS()V
    .locals 2

    .prologue
    .line 1642
    invoke-static {}, Lcom/vlingo/midas/util/SViewCoverUtils;->isSViewCoverOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1643
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/vlingo/midas/settings/MidasSettings$1;

    invoke-direct {v1}, Lcom/vlingo/midas/settings/MidasSettings$1;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1653
    :cond_0
    return-void
.end method

.method public static resetVolumeNormal(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1185
    invoke-static {p0}, Lcom/vlingo/midas/settings/MidasSettings;->setStreamVolumeNormal(Landroid/content/Context;)V

    .line 1186
    invoke-static {p0}, Lcom/vlingo/midas/settings/MidasSettings;->setSystemVolumeNormal(Landroid/content/Context;)V

    .line 1187
    return-void
.end method

.method public static saveStreamVloume(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1472
    const-string/jumbo v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1473
    .local v0, "audioManager":Landroid/media/AudioManager;
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 1474
    .local v1, "value":I
    const-string/jumbo v2, "stream_volume"

    invoke-static {v2, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1475
    return-void
.end method

.method private static saveString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "saveText"    # Ljava/lang/String;

    .prologue
    .line 309
    const/4 v2, 0x0

    .line 312
    .local v2, "mFileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, p0, v4}, Lcom/vlingo/midas/VlingoApplication;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    .line 313
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    if-eqz v2, :cond_0

    .line 319
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 320
    :catch_0
    move-exception v1

    .line 321
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 314
    .end local v1    # "e1":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 315
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 318
    if-eqz v2, :cond_0

    .line 319
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 320
    :catch_2
    move-exception v1

    .line 321
    .restart local v1    # "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 317
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 318
    if-eqz v2, :cond_1

    .line 319
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 322
    :cond_1
    :goto_1
    throw v3

    .line 320
    :catch_3
    move-exception v1

    .line 321
    .restart local v1    # "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static setActionBarTransparent(Landroid/app/ActionBar;)V
    .locals 2
    .param p0, "actionBar"    # Landroid/app/ActionBar;

    .prologue
    .line 1682
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1683
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$drawable;->transparent:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1684
    return-void
.end method

.method public static setDataCheckMobileEnabled(Z)V
    .locals 1
    .param p0, "b"    # Z

    .prologue
    .line 567
    const-string/jumbo v0, "data_check_mobile_enabled"

    invoke-static {v0, p0}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 568
    return-void
.end method

.method public static setDataCheckWlanEnabled(Z)V
    .locals 1
    .param p0, "b"    # Z

    .prologue
    .line 556
    const-string/jumbo v0, "data_check_wlan_enabled"

    invoke-static {v0, p0}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 557
    return-void
.end method

.method public static setDeviceType(I)V
    .locals 1
    .param p0, "device"    # I

    .prologue
    .line 151
    packed-switch p0, :pswitch_data_0

    .line 168
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->deviceType:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    .line 171
    :goto_0
    return-void

    .line 153
    :pswitch_0
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->deviceType:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    goto :goto_0

    .line 156
    :pswitch_1
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->deviceType:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    goto :goto_0

    .line 159
    :pswitch_2
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->deviceType:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    goto :goto_0

    .line 162
    :pswitch_3
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->deviceType:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    goto :goto_0

    .line 165
    :pswitch_4
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_EXTRA_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings;->deviceType:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    goto :goto_0

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static setDontAskMobileEnabled(Z)V
    .locals 3
    .param p0, "b"    # Z

    .prologue
    .line 585
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 586
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 587
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "dont_ask_mobile_enabled"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 588
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 589
    return-void
.end method

.method public static setDontAskWlanEnabled(Z)V
    .locals 3
    .param p0, "b"    # Z

    .prologue
    .line 597
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 598
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 599
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "dont_ask_wlan_enabled"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 600
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 601
    return-void
.end method

.method public static setForegroundState(Z)V
    .locals 5
    .param p0, "isForeground"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1456
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "svoice_msg_popup_disable"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1458
    .local v0, "isSet":I
    if-eqz p0, :cond_1

    .line 1459
    if-eq v0, v4, :cond_0

    .line 1460
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "svoice_msg_popup_disable"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1469
    :cond_0
    :goto_0
    return-void

    .line 1464
    :cond_1
    if-eqz v0, :cond_0

    .line 1465
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "svoice_msg_popup_disable"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public static setHandsFreeInformationAccepted(Z)V
    .locals 4
    .param p0, "b"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 498
    const-string/jumbo v0, "hands_free_information_accepted"

    invoke-static {v0, p0}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 499
    const-string/jumbo v1, "hands_free_information_accepted"

    if-eqz p0, :cond_0

    new-array v0, v3, [B

    aput-byte v3, v0, v2

    :goto_0
    invoke-static {v1, v0}, Lcom/vlingo/midas/settings/MidasSettings;->setData(Ljava/lang/String;[B)V

    .line 500
    return-void

    .line 499
    :cond_0
    new-array v0, v3, [B

    aput-byte v2, v0, v2

    goto :goto_0
.end method

.method public static setHelpVisible(Z)V
    .locals 3
    .param p0, "visible"    # Z

    .prologue
    .line 761
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 762
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 763
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "key_help_visible"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 764
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 765
    return-void
.end method

.method public static setInCarMode(Z)V
    .locals 1
    .param p0, "inCarMode"    # Z

    .prologue
    .line 720
    sget-boolean v0, Lcom/vlingo/midas/settings/MidasSettings;->s_inCarMode:Z

    if-ne p0, v0, :cond_0

    .line 727
    :goto_0
    return-void

    .line 723
    :cond_0
    sput-boolean p0, Lcom/vlingo/midas/settings/MidasSettings;->s_inCarMode:Z

    .line 725
    const-string/jumbo v0, "in_car_mode"

    invoke-static {v0, p0}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static setModelName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "model"    # Ljava/lang/String;

    .prologue
    .line 777
    const-string/jumbo v0, "midas_model_name"

    invoke-static {v0, p0}, Lcom/vlingo/midas/settings/MidasSettings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static setMusicLaunchedFromSvoice(Z)V
    .locals 0
    .param p0, "musicLaunchedFromSvoice"    # Z

    .prologue
    .line 1236
    sput-boolean p0, Lcom/vlingo/midas/settings/MidasSettings;->mMusicLaunchedFromSvoice:Z

    .line 1237
    return-void
.end method

.method public static setNewAlarmConcept(Z)V
    .locals 0
    .param p0, "_isNewAlarmConcept"    # Z

    .prologue
    .line 1720
    sput-boolean p0, Lcom/vlingo/midas/settings/MidasSettings;->isNewAlarmConcept:Z

    .line 1721
    return-void
.end method

.method public static setNotificationAccepted(Ljava/lang/String;Z)V
    .locals 3
    .param p0, "notificationVersion"    # Ljava/lang/String;
    .param p1, "accepted"    # Z

    .prologue
    .line 632
    const-string/jumbo v1, "accepted_notifications"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 633
    .local v0, "acceptedNotifications":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 634
    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 638
    :goto_0
    return-void

    .line 636
    :cond_0
    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setSamsungDisclaimerAccepted(Z)V
    .locals 6
    .param p0, "b"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 522
    const-string/jumbo v2, "samsung_disclaimer_accepted"

    invoke-static {v2, p0}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 523
    const-string/jumbo v3, "samsung_disclaimer_accepted"

    if-eqz p0, :cond_0

    new-array v2, v5, [B

    aput-byte v5, v2, v4

    :goto_0
    invoke-static {v3, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setData(Ljava/lang/String;[B)V

    .line 524
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 525
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 526
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "samsung_disclaimer_accepted"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 527
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 528
    return-void

    .line 523
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "sharedPreferences":Landroid/content/SharedPreferences;
    :cond_0
    new-array v2, v5, [B

    aput-byte v4, v2, v4

    goto :goto_0
.end method

.method public static setSeamlessWakeupStarted(Z)V
    .locals 1
    .param p0, "isWakeupStarted"    # Z

    .prologue
    .line 1602
    const-string/jumbo v0, "is_seamless_wakeup_started"

    invoke-static {v0, p0}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 1603
    return-void
.end method

.method public static setShowAgainBackgroundData(Z)V
    .locals 3
    .param p0, "b"    # Z

    .prologue
    .line 572
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 573
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 574
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "dont_ask_background_data_check_enabled"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 575
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 576
    return-void
.end method

.method public static setStreamVolumeMaximum(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 1159
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v1

    .line 1160
    .local v1, "talkback":Z
    if-eqz p0, :cond_0

    if-nez v1, :cond_0

    .line 1161
    const-string/jumbo v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1162
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1163
    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    invoke-virtual {v0, v5, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1168
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_0
    :goto_0
    return-void

    .line 1165
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_1
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    invoke-virtual {v0, v4, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public static setStreamVolumeMute(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 1114
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v2

    .line 1115
    .local v2, "talkback":Z
    if-eqz p0, :cond_0

    if-nez v2, :cond_0

    .line 1116
    const-string/jumbo v3, "audio"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1117
    .local v0, "audioManager":Landroid/media/AudioManager;
    if-eqz v0, :cond_0

    .line 1118
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1119
    const/4 v1, -0x1

    .line 1120
    .local v1, "streamVolume":I
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1121
    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 1128
    :goto_0
    const-string/jumbo v3, "stream_volume"

    invoke-static {v3, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1130
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1131
    invoke-virtual {v0, v6, v4, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1141
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    .end local v1    # "streamVolume":I
    :cond_0
    :goto_1
    return-void

    .line 1123
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    .restart local v1    # "streamVolume":I
    :cond_1
    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    goto :goto_0

    .line 1133
    :cond_2
    invoke-virtual {v0, v5, v4, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_1
.end method

.method public static setStreamVolumeNormal(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 1144
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v2

    .line 1145
    .local v2, "talkback":Z
    if-eqz p0, :cond_0

    if-nez v2, :cond_0

    .line 1146
    const-string/jumbo v3, "audio"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1147
    .local v0, "audioManager":Landroid/media/AudioManager;
    const-string/jumbo v3, "stream_volume"

    invoke-static {v3, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1148
    .local v1, "streamVolume":I
    if-eq v1, v4, :cond_0

    .line 1149
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1150
    const/4 v3, 0x6

    invoke-virtual {v0, v3, v1, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1154
    :goto_0
    const-string/jumbo v3, "stream_volume"

    invoke-static {v3, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1157
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    .end local v1    # "streamVolume":I
    :cond_0
    return-void

    .line 1152
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    .restart local v1    # "streamVolume":I
    :cond_1
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public static setSvoiceLaunchedFromOutside(Z)V
    .locals 0
    .param p0, "outside"    # Z

    .prologue
    .line 1251
    sput-boolean p0, Lcom/vlingo/midas/settings/MidasSettings;->mSvoiceLaunchedFromOutside:Z

    .line 1252
    return-void
.end method

.method public static setSystemVolumeMute(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1097
    const/4 v0, 0x0

    .line 1098
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v2

    .line 1099
    .local v2, "talkback":Z
    if-nez v2, :cond_1

    .line 1100
    if-eqz p0, :cond_0

    .line 1101
    const-string/jumbo v3, "audio"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "audioManager":Landroid/media/AudioManager;
    check-cast v0, Landroid/media/AudioManager;

    .line 1103
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1104
    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 1105
    .local v1, "systemVolume":I
    const-string/jumbo v3, "system_volume"

    invoke-static {v3, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1106
    invoke-virtual {v0, v5, v4, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1111
    .end local v1    # "systemVolume":I
    :cond_1
    return-void
.end method

.method public static setSystemVolumeNormal(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, -0x1

    .line 1078
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v2

    .line 1079
    .local v2, "talkback":Z
    if-eqz p0, :cond_0

    if-nez v2, :cond_0

    .line 1080
    const-string/jumbo v3, "audio"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1081
    .local v0, "audioManager":Landroid/media/AudioManager;
    const-string/jumbo v3, "system_volume"

    invoke-static {v3, v5}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1082
    .local v1, "systemVolume":I
    if-eq v1, v5, :cond_0

    .line 1083
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1084
    const-string/jumbo v3, "system_volume"

    invoke-static {v3, v5}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1089
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    .end local v1    # "systemVolume":I
    :cond_0
    return-void
.end method

.method public static setVolumeMaximum(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1181
    invoke-static {p0}, Lcom/vlingo/midas/settings/MidasSettings;->setStreamVolumeMaximum(Landroid/content/Context;)V

    .line 1182
    return-void
.end method

.method public static setVolumeMute(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1173
    return-void
.end method

.method public static setVolumeNormal(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1178
    return-void
.end method

.method public static showActionBar(Landroid/app/ActionBar;)V
    .locals 2
    .param p0, "actionBar"    # Landroid/app/ActionBar;

    .prologue
    .line 1672
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1673
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$drawable;->transparent:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1674
    return-void
.end method

.method public static unbindDrawables(Landroid/view/View;)V
    .locals 5
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 1203
    :try_start_0
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1204
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1206
    :cond_0
    instance-of v3, p0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    .line 1207
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v3, v0

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1208
    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v3, v0

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->unbindDrawables(Landroid/view/View;)V

    .line 1207
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1210
    :cond_1
    instance-of v3, p0, Landroid/widget/AdapterView;

    if-nez v3, :cond_2

    .line 1211
    check-cast p0, Landroid/view/ViewGroup;

    .end local p0    # "view":Landroid/view/View;
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1218
    .end local v2    # "i":I
    .local v1, "e":Ljava/lang/Exception;
    :cond_2
    :goto_1
    return-void

    .line 1214
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v1

    .line 1216
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static updateCurrentLocale()V
    .locals 1

    .prologue
    .line 375
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 376
    return-void
.end method

.method public static updateCurrentLocale(Landroid/content/res/Resources;)V
    .locals 24
    .param p0, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 381
    sget-boolean v21, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialFromSettings:Z

    if-eqz v21, :cond_1

    .line 382
    const-string/jumbo v21, "language"

    const-string/jumbo v22, "en-US"

    invoke-static/range {v21 .. v22}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 383
    .local v13, "language":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    const-string/jumbo v22, "voicetalk_language"

    invoke-static {v13}, Lcom/vlingo/midas/settings/MidasSettings;->convertToBargeinISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v23}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 477
    .end local v13    # "language":Ljava/lang/String;
    :cond_0
    :goto_0
    const-string/jumbo v21, "strangeLang"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v23

    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    invoke-static/range {p0 .. p0}, Lcom/vlingo/core/internal/settings/Settings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 479
    return-void

    .line 385
    :cond_1
    invoke-static {}, Lcom/vlingo/core/facade/lmtt/MdsoUtils;->isSlave()Z

    move-result v12

    .line 387
    .local v12, "isSlaveApp":Z
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v21

    if-nez v21, :cond_2

    sget-boolean v21, Lcom/vlingo/midas/settings/SettingsScreen;->isLaunchBySettings:Z

    if-eqz v21, :cond_3

    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isIUXComplete()Z

    move-result v21

    if-nez v21, :cond_c

    :cond_3
    if-nez v12, :cond_c

    .line 394
    :try_start_0
    const-string/jumbo v21, "android.app.ActivityManagerNative"

    invoke-static/range {v21 .. v21}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 395
    .local v4, "amnClass":Ljava/lang/Class;
    const/4 v3, 0x0

    .line 396
    .local v3, "amn":Ljava/lang/Object;
    const/4 v6, 0x0

    .line 399
    .local v6, "config":Landroid/content/res/Configuration;
    const-string/jumbo v21, "getDefault"

    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v4, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v16

    .line 400
    .local v16, "methodGetDefault":Ljava/lang/reflect/Method;
    const/16 v21, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 401
    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v4, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 404
    const-string/jumbo v21, "getConfiguration"

    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v4, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v15

    .line 405
    .local v15, "methodGetConfiguration":Ljava/lang/reflect/Method;
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 406
    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v15, v3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "config":Landroid/content/res/Configuration;
    check-cast v6, Landroid/content/res/Configuration;

    .line 407
    .restart local v6    # "config":Landroid/content/res/Configuration;
    iget-object v7, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    .end local v3    # "amn":Ljava/lang/Object;
    .end local v4    # "amnClass":Ljava/lang/Class;
    .end local v6    # "config":Landroid/content/res/Configuration;
    .end local v15    # "methodGetConfiguration":Ljava/lang/reflect/Method;
    .end local v16    # "methodGetDefault":Ljava/lang/reflect/Method;
    .local v7, "currentLocale":Ljava/util/Locale;
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSupportedLanguages()[Ljava/lang/CharSequence;

    move-result-object v20

    .line 418
    .local v20, "supportedLanaguageList":[Ljava/lang/CharSequence;
    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x5f

    const/16 v23, 0x2d

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v17

    .line 419
    .local v17, "rightArgs":Ljava/lang/String;
    const/16 v18, 0x0

    .line 420
    .local v18, "selectedLang":Ljava/lang/String;
    const-string/jumbo v21, "svoicedebugging"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "current locale : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getLanguageApplication()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/settings/MidasSettings;->getISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_0

    .line 424
    move-object/from16 v5, v20

    .local v5, "arr$":[Ljava/lang/CharSequence;
    array-length v14, v5

    .local v14, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_2
    if-ge v11, v14, :cond_5

    aget-object v19, v5, v11

    .line 425
    .local v19, "sss":Ljava/lang/CharSequence;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/settings/MidasSettings;->convertToISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 426
    move-object/from16 v18, v17

    .line 424
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 409
    .end local v5    # "arr$":[Ljava/lang/CharSequence;
    .end local v7    # "currentLocale":Ljava/util/Locale;
    .end local v11    # "i$":I
    .end local v14    # "len$":I
    .end local v17    # "rightArgs":Ljava/lang/String;
    .end local v18    # "selectedLang":Ljava/lang/String;
    .end local v19    # "sss":Ljava/lang/CharSequence;
    .end local v20    # "supportedLanaguageList":[Ljava/lang/CharSequence;
    :catch_0
    move-exception v9

    .line 411
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 412
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    .restart local v7    # "currentLocale":Ljava/util/Locale;
    goto :goto_1

    .line 430
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v5    # "arr$":[Ljava/lang/CharSequence;
    .restart local v11    # "i$":I
    .restart local v14    # "len$":I
    .restart local v17    # "rightArgs":Ljava/lang/String;
    .restart local v18    # "selectedLang":Ljava/lang/String;
    .restart local v20    # "supportedLanaguageList":[Ljava/lang/CharSequence;
    :cond_5
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 431
    move-object/from16 v5, v20

    array-length v14, v5

    const/4 v11, 0x0

    :goto_3
    if-ge v11, v14, :cond_6

    aget-object v19, v5, v11

    .line 433
    .restart local v19    # "sss":Ljava/lang/CharSequence;
    const-string/jumbo v21, "zh-TW"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_6

    const-string/jumbo v21, "zh-HK"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 444
    .end local v19    # "sss":Ljava/lang/CharSequence;
    :cond_6
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->startBatchEdit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    .line 445
    .local v10, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v8, 0x0

    .line 446
    .local v8, "defaultLang":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/vlingo/core/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 448
    const-string/jumbo v21, "kk-KZ"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_7

    const-string/jumbo v21, "uz-UZ"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_7

    const-string/jumbo v21, "tg-TJ"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_7

    const-string/jumbo v21, "tk-TM"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_7

    const-string/jumbo v21, "az-AZ"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_7

    const-string/jumbo v21, "hy-AM"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_7

    const-string/jumbo v21, "ky-KG"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 452
    :cond_7
    const-string/jumbo v21, "en-US"

    const-string/jumbo v22, "ru-RU"

    invoke-static/range {v21 .. v22}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 456
    :goto_4
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-static {v8, v0, v10}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;Landroid/content/SharedPreferences$Editor;)V

    .line 460
    :goto_5
    const-string/jumbo v21, "svoicedebugging"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "default language : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    invoke-static {v10}, Lcom/vlingo/midas/settings/MidasSettings;->commitBatchEdit(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_0

    .line 436
    .end local v8    # "defaultLang":Ljava/lang/String;
    .end local v10    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v19    # "sss":Ljava/lang/CharSequence;
    :cond_8
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/midas/settings/MidasSettings;->convertToISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_9

    const-string/jumbo v21, "pt-BR"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_9

    .line 438
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    .line 439
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_9

    const-string/jumbo v21, "en-GB"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_9

    const/16 v21, 0x3

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/vlingo/core/internal/settings/LanguageDefaulter;->checkForCommonwealthCountry(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_6

    .line 431
    :cond_9
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 454
    .end local v19    # "sss":Ljava/lang/CharSequence;
    .restart local v8    # "defaultLang":Ljava/lang/String;
    .restart local v10    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_a
    const-string/jumbo v21, "en-US"

    const-string/jumbo v22, "en-US"

    invoke-static/range {v21 .. v22}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_4

    .line 458
    :cond_b
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-static {v0, v1, v10}, Lcom/vlingo/core/internal/settings/Settings;->setLanguageApplication(Ljava/lang/String;Landroid/content/Context;Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_5

    .line 470
    .end local v5    # "arr$":[Ljava/lang/CharSequence;
    .end local v7    # "currentLocale":Ljava/util/Locale;
    .end local v8    # "defaultLang":Ljava/lang/String;
    .end local v10    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v11    # "i$":I
    .end local v14    # "len$":I
    .end local v17    # "rightArgs":Ljava/lang/String;
    .end local v18    # "selectedLang":Ljava/lang/String;
    .end local v20    # "supportedLanaguageList":[Ljava/lang/CharSequence;
    :cond_c
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v21

    if-nez v21, :cond_d

    if-eqz v12, :cond_0

    .line 471
    :cond_d
    const-string/jumbo v21, "language"

    const-string/jumbo v22, "en-US"

    invoke-static/range {v21 .. v22}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 472
    .restart local v13    # "language":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    const-string/jumbo v22, "voicetalk_language"

    invoke-static {v13}, Lcom/vlingo/midas/settings/MidasSettings;->convertToBargeinISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v23}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0
.end method
