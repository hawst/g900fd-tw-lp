.class Lcom/vlingo/midas/settings/SettingsScreen$35;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->showWakeupInSecuredLockDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 2461
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$35;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x0

    .line 2464
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$35;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mWakeup:Landroid/preference/CheckBoxPreference;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$1200(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 2471
    const/4 v0, 0x0

    .line 2472
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$35;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const-string/jumbo v2, "preferences_voice_wake_up"

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2473
    const-string/jumbo v1, "VOICE_WAKE_UP_IN_SECURED_POP"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2474
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2475
    return-void
.end method
