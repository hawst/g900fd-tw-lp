.class Lcom/vlingo/midas/settings/SettingsScreen$34;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->showWakeupInSecuredLockDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 2447
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$34;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    iput-object p2, p0, Lcom/vlingo/midas/settings/SettingsScreen$34;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    .line 2450
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$34;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mWakeup:Landroid/preference/CheckBoxPreference;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$1200(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 2451
    const-string/jumbo v1, "secure_voice_wake_up"

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 2453
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$34;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2454
    const/4 v0, 0x0

    .line 2455
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$34;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const-string/jumbo v2, "preferences_voice_wake_up"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2456
    const-string/jumbo v1, "VOICE_WAKE_UP_IN_SECURED_POP"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2457
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2459
    .end local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method
