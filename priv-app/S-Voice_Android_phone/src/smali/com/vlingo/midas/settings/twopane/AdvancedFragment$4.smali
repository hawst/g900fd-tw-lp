.class Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;
.super Ljava/lang/Object;
.source "AdvancedFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->initFragments()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 9
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 172
    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 173
    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    .line 174
    .local v2, "dialog_editbox":Landroid/widget/EditText;
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 175
    new-array v3, v7, [Landroid/text/InputFilter;

    new-instance v4, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LengthToastFilter;

    iget-object v5, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    const/16 v6, 0x12c

    invoke-direct {v4, v5, v6}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LengthToastFilter;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;I)V

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 176
    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 177
    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 178
    sget v3, Lcom/vlingo/midas/R$string;->settings_configure_home_address:I

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(I)V

    .line 179
    invoke-virtual {v2}, Landroid/widget/EditText;->selectAll()V

    .line 180
    new-instance v3, Landroid/widget/Scroller;

    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    invoke-virtual {v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setScroller(Landroid/widget/Scroller;)V

    .line 181
    new-instance v3, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$InputTextWatcher;

    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    iget-object v5, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v5

    invoke-virtual {v5}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$InputTextWatcher;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;Landroid/app/Dialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 182
    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 183
    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    const v4, 0x1020019

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 184
    .local v0, "button":Landroid/widget/Button;
    new-instance v3, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$1;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    .end local v0    # "button":Landroid/widget/Button;
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 217
    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    const v4, 0x102001a

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 218
    .local v1, "button2":Landroid/widget/Button;
    new-instance v3, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$2;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$2;-><init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    .end local v1    # "button2":Landroid/widget/Button;
    .end local v2    # "dialog_editbox":Landroid/widget/EditText;
    :cond_1
    return v8
.end method
