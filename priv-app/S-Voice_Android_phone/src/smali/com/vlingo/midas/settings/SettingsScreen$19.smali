.class Lcom/vlingo/midas/settings/SettingsScreen$19;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->showWakeupLockScreenDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 1943
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$19;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x0

    .line 1947
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen$19;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1948
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1949
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "VOICE_WAKE_UP_POP_SETTINGSSCREEN"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1950
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1951
    return-void
.end method
