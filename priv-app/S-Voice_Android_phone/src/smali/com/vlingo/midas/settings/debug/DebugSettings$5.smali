.class Lcom/vlingo/midas/settings/debug/DebugSettings$5;
.super Ljava/lang/Object;
.source "DebugSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/debug/DebugSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$5;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v6, 0x1

    .line 208
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$5;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    # getter for: Lcom/vlingo/midas/settings/debug/DebugSettings;->activity:Landroid/preference/PreferenceActivity;
    invoke-static {v5}, Lcom/vlingo/midas/settings/debug/DebugSettings;->access$100(Lcom/vlingo/midas/settings/debug/DebugSettings;)Landroid/preference/PreferenceActivity;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 209
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    new-instance v2, Landroid/widget/EditText;

    iget-object v5, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$5;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    # getter for: Lcom/vlingo/midas/settings/debug/DebugSettings;->activity:Landroid/preference/PreferenceActivity;
    invoke-static {v5}, Lcom/vlingo/midas/settings/debug/DebugSettings;->access$100(Lcom/vlingo/midas/settings/debug/DebugSettings;)Landroid/preference/PreferenceActivity;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 210
    .local v2, "inputName":Landroid/widget/EditText;
    new-instance v3, Landroid/widget/EditText;

    iget-object v5, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$5;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    # getter for: Lcom/vlingo/midas/settings/debug/DebugSettings;->activity:Landroid/preference/PreferenceActivity;
    invoke-static {v5}, Lcom/vlingo/midas/settings/debug/DebugSettings;->access$100(Lcom/vlingo/midas/settings/debug/DebugSettings;)Landroid/preference/PreferenceActivity;

    move-result-object v5

    invoke-direct {v3, v5}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 212
    .local v3, "inputTimeTest":Landroid/widget/EditText;
    const-string/jumbo v5, "Alex"

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 213
    const-string/jumbo v5, "10"

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 215
    new-instance v4, Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$5;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    # getter for: Lcom/vlingo/midas/settings/debug/DebugSettings;->activity:Landroid/preference/PreferenceActivity;
    invoke-static {v5}, Lcom/vlingo/midas/settings/debug/DebugSettings;->access$100(Lcom/vlingo/midas/settings/debug/DebugSettings;)Landroid/preference/PreferenceActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 216
    .local v4, "ll":Landroid/widget/LinearLayout;
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 217
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 218
    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 219
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 221
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 222
    const-string/jumbo v5, "Test"

    new-instance v6, Lcom/vlingo/midas/settings/debug/DebugSettings$5$1;

    invoke-direct {v6, p0, v2, v3}, Lcom/vlingo/midas/settings/debug/DebugSettings$5$1;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings$5;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 229
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 230
    .local v1, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 232
    const/4 v5, 0x0

    return v5
.end method
