.class public Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;
.super Landroid/app/Fragment;
.source "SettingEraseServerDataDetailFragment.java"

# interfaces
.implements Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;
.implements Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;
    }
.end annotation


# static fields
.field private static final DELETE:I = 0x2

.field private static final FAILURE:I = 0x0

.field private static final SUCCESS:I = 0x1


# instance fields
.field private callback:Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;

.field private context:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

.field private mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

.field private myhandler:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;

.field private progress:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->context:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    .line 39
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->myhandler:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;

    .line 40
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V
    .locals 1
    .param p1, "onFragmentLoadListener"    # Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->context:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    .line 39
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->myhandler:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;

    .line 40
    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;

    .line 47
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .line 48
    return-void
.end method

.method static synthetic access$100(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->context:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->myhandler:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->callback:Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->onItemSelected(I)V

    .line 125
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x400

    .line 56
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 57
    iput-object p0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->context:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    .line 58
    new-instance v3, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;

    invoke-direct {v3, p0, v5}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;-><init>(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;)V

    iput-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->myhandler:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;

    .line 59
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 60
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->settings_path_view:I

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 62
    .local v2, "view":Landroid/view/View;
    sget v3, Lcom/vlingo/midas/R$id;->fragment_description:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 63
    .local v1, "textView":Landroid/widget/TextView;
    sget v3, Lcom/vlingo/midas/R$id;->icon:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 64
    .local v0, "iconView":Landroid/widget/ImageView;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 65
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->erase_server_data_body:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    .end local v0    # "iconView":Landroid/widget/ImageView;
    .end local v1    # "textView":Landroid/widget/TextView;
    .end local v2    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    sget v3, Lcom/vlingo/midas/R$layout;->erase_server_data_detail_layout:I

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 74
    .local v1, "rootView":Landroid/view/View;
    sget v3, Lcom/vlingo/midas/R$id;->erase_server_data_detail_body:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 78
    .local v2, "txtView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->all_data_on_the_server_erase_without_car_mode:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    sget v3, Lcom/vlingo/midas/R$id;->erase_server_data_detail_btn:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 81
    .local v0, "btn":Landroid/widget/Button;
    new-instance v3, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;-><init>(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    return-object v1
.end method

.method public onFailure()V
    .locals 5

    .prologue
    .line 132
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 134
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 135
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->failed_to_erase_data:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->myhandler:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;

    const/4 v2, 0x0

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public onSuccess(Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;)V
    .locals 5
    .param p1, "callback"    # Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->callback:Lcom/vlingo/core/internal/userdata/UserDataManager$ServerDataDeletionListener;

    .line 150
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 151
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 152
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 153
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->success_to_erase_data:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->myhandler:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;

    const/4 v2, 0x1

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
