.class Lcom/vlingo/midas/settings/SettingsScreen$13;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 1846
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$13;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 1851
    const/4 v0, 0x0

    .line 1852
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$13;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const-string/jumbo v2, "checkBoxPrefForAlert"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1853
    const-string/jumbo v1, "isCheckBoxChecked"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1854
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1855
    return-void
.end method
