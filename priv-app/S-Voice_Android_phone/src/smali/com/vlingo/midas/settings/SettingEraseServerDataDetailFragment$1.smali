.class Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;
.super Ljava/lang/Object;
.source "SettingEraseServerDataDetailFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x1

    const/4 v8, 0x1

    .line 83
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    invoke-virtual {v4}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    invoke-virtual {v5}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$string;->erase_server_data_body:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    invoke-virtual {v6}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->erasing_data:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6, v8}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v4

    # setter for: Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;
    invoke-static {v3, v4}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->access$102(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 85
    invoke-static {}, Lcom/vlingo/core/facade/lmtt/MdsoUtils;->isMasterWithoutActiveSlave()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 86
    new-instance v3, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    sget v6, Lcom/vlingo/midas/R$string;->user_data_deletion_confirm_server_failed:I

    invoke-virtual {v5, v6}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->context:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;
    invoke-static {v6}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->access$200(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/vlingo/midas/datadeletion/DeleteUserDataCallback$DataDeletionListener;)V

    invoke-static {v8, v3}, Lcom/vlingo/core/internal/VlingoAndroidCore;->deleteAllPersonalData(ZLcom/vlingo/core/facade/userdata/IDeleteUserDataCallback;)V

    .line 96
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getInstance()Lcom/vlingo/midas/samsungutils/SalesCodeProperties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/samsungutils/SalesCodeProperties;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "salesCode":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v3

    const-string/jumbo v4, "ro.product.name"

    invoke-virtual {v3, v4}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "product_name":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string/jumbo v3, "VZW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    if-eqz v1, :cond_5

    const-string/jumbo v3, "degaswifibmw"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 100
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "double_tab_launch"

    invoke-static {v3, v4, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v8, :cond_2

    .line 101
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "double_tab_launch"

    invoke-static {v3, v4, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 110
    :cond_2
    :goto_1
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "audio"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 111
    .local v0, "audioManager":Landroid/media/AudioManager;
    if-eqz v0, :cond_3

    .line 112
    const-string/jumbo v3, "voice_wakeup_mic=off"

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 113
    const-string/jumbo v3, "Always"

    const-string/jumbo v4, "setParameters, voice_wakeup_mic=off"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_3
    return-void

    .line 88
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    .end local v1    # "product_name":Ljava/lang/String;
    .end local v2    # "salesCode":Ljava/lang/String;
    :cond_4
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->access$100(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 89
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->access$100(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 90
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->progress:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->access$100(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Landroid/app/ProgressDialog;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    invoke-virtual {v4}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$string;->success_to_erase_data:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->myhandler:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;->access$300(Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;)Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;

    move-result-object v3

    const/4 v4, 0x2

    const-wide/16 v5, 0x9c4

    invoke-virtual {v3, v4, v5, v6}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment$DataDeletionHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 105
    .restart local v1    # "product_name":Ljava/lang/String;
    .restart local v2    # "salesCode":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "double_tab_launch"

    invoke-static {v3, v4, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_2

    .line 106
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "double_tab_launch"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1
.end method
