.class public Lcom/vlingo/midas/settings/debug/DebugSettings;
.super Landroid/preference/PreferenceActivity;
.source "DebugSettings.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# static fields
.field public static final DEBUG_MENU:I = 0x3e8

.field public static final MENU_FORCE_FORCE_CLOSE:I = 0x3ea

.field public static final MENU_RUN_SCHEDULE_TESTS:I = 0x3e9

.field public static final MENU_START_AUTOMATION_PLAYBACK:I = 0x3eb

.field public static final MENU_START_AUTOMATION_RECORDING:I = 0x3ed

.field public static final MENU_STOP_AUTOMATION_PLAYBACK:I = 0x3ec

.field public static final MENU_STOP_AUTOMATION_RECORDING:I = 0x3ee

.field public static SHOW_DEBUG:Z


# instance fields
.field private activity:Landroid/preference/PreferenceActivity;

.field private changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

.field private mFakedDeviceModelValue:Landroid/preference/EditTextPreference;

.field private mFakedSalesCodeValue:Landroid/preference/EditTextPreference;

.field private m_Carrier:Landroid/preference/EditTextPreference;

.field private m_CarrierCountry:Landroid/preference/EditTextPreference;

.field private m_KillProcessOnDestroy:Z

.field private m_NothingRecognizedRepromptMax:Landroid/preference/EditTextPreference;

.field private m_TTS_Network_Timeout:Landroid/preference/EditTextPreference;

.field private m_fake_Autonavi:Landroid/preference/CheckBoxPreference;

.field private m_fake_BaiduMaps:Landroid/preference/CheckBoxPreference;

.field private m_fake_VideoCall_value:Landroid/preference/CheckBoxPreference;

.field private m_fake_eNavi:Landroid/preference/CheckBoxPreference;

.field private m_fake_lat:Landroid/preference/EditTextPreference;

.field private m_fake_long:Landroid/preference/EditTextPreference;

.field private m_server_response_file:Landroid/preference/EditTextPreference;

.field private m_server_response_logging:Landroid/preference/ListPreference;

.field private m_useHiddenCalendars:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->SHOW_DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_KillProcessOnDestroy:Z

    .line 97
    new-instance v0, Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-direct {v0}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    return-void
.end method

.method static synthetic access$002(Lcom/vlingo/midas/settings/debug/DebugSettings;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/settings/debug/DebugSettings;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_KillProcessOnDestroy:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vlingo/midas/settings/debug/DebugSettings;)Landroid/preference/PreferenceActivity;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/debug/DebugSettings;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->activity:Landroid/preference/PreferenceActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vlingo/midas/settings/debug/DebugSettings;)Landroid/preference/EditTextPreference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/debug/DebugSettings;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->mFakedDeviceModelValue:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vlingo/midas/settings/debug/DebugSettings;)Landroid/preference/EditTextPreference;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/debug/DebugSettings;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->mFakedSalesCodeValue:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method private updateMenuDescriptions()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 689
    const-string/jumbo v0, "lmtt.sync_status_contact"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 690
    const-string/jumbo v0, "LMTT_CONTACTS_SENDLMTT"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const-string/jumbo v1, "Initiate contact sync"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 695
    :goto_0
    const-string/jumbo v0, "lmtt.sync_status_music"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 696
    const-string/jumbo v0, "LMTT_MUSIC_SENDLMTT"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const-string/jumbo v1, "Initiate music sync"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 700
    :goto_1
    return-void

    .line 692
    :cond_0
    const-string/jumbo v0, "LMTT_CONTACTS_SENDLMTT"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const-string/jumbo v1, "Initiates a contact sync"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 698
    :cond_1
    const-string/jumbo v0, "LMTT_MUSIC_SENDLMTT"

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const-string/jumbo v1, "Initiates a music sync"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method enableFakeLatLong(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 639
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_CarrierCountry:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 640
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_Carrier:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 641
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_lat:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 642
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_long:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p1}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 643
    return-void
.end method

.method enableFakeNavigationApp(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 646
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_Autonavi:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 647
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_BaiduMaps:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 648
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_eNavi:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 649
    return-void
.end method

.method enableFakeVideoCalling(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 652
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_VideoCall_value:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 653
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 29
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 101
    invoke-super/range {p0 .. p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 102
    sget v26, Lcom/vlingo/midas/R$xml;->debug_settings:I

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->addPreferencesFromResource(I)V

    .line 104
    const/16 v21, 0x0

    .line 105
    .local v21, "showDebugSettings":Z
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/debug/DebugSettings;->getIntent()Landroid/content/Intent;

    move-result-object v14

    .line 106
    .local v14, "i":Landroid/content/Intent;
    invoke-virtual {v14}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 107
    .local v5, "extras":Landroid/os/Bundle;
    if-eqz v5, :cond_0

    .line 108
    const-string/jumbo v26, "key"

    const-string/jumbo v27, "gottajibboo"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 109
    .local v15, "key":Ljava/lang/String;
    const-string/jumbo v26, ""

    move-object/from16 v0, v26

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_0

    .line 110
    const/16 v21, 0x1

    .line 113
    .end local v15    # "key":Ljava/lang/String;
    :cond_0
    if-nez v21, :cond_1

    .line 114
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/debug/DebugSettings;->finish()V

    .line 116
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->activity:Landroid/preference/PreferenceActivity;

    .line 119
    new-instance v4, Lcom/vlingo/midas/settings/debug/DebugSettings$1;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/vlingo/midas/settings/debug/DebugSettings$1;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    .line 124
    .local v4, "cueKillProcess":Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;
    new-instance v17, Lcom/vlingo/midas/settings/debug/DebugSettings$2;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$2;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    .line 130
    .local v17, "logUpdate":Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;
    invoke-static/range {v17 .. v17}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->onAllUpdatedPreferences(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V

    .line 134
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    invoke-direct/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>()V

    const-string/jumbo v27, "tts_network_timeout"

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->onSetting(Ljava/lang/String;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$3;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$3;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->onPreferenceChange(Landroid/preference/Preference$OnPreferenceChangeListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_TTS_Network_Timeout:Landroid/preference/EditTextPreference;

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_TTS_Network_Timeout:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v26

    if-nez v26, :cond_2

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_TTS_Network_Timeout:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    const-string/jumbo v27, "750"

    invoke-virtual/range {v26 .. v27}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 154
    :cond_2
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    invoke-direct/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>()V

    const-string/jumbo v27, "nothing_recognized_reprompt.max_value"

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->onSetting(Ljava/lang/String;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$4;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$4;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->onPreferenceChange(Landroid/preference/Preference$OnPreferenceChangeListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_NothingRecognizedRepromptMax:Landroid/preference/EditTextPreference;

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_NothingRecognizedRepromptMax:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v26

    if-nez v26, :cond_3

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_NothingRecognizedRepromptMax:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    const-string/jumbo v27, "nothing_recognized_reprompt.max_value"

    invoke-virtual/range {v26 .. v27}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 175
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/util/ServerDetails;->getInstance()Lcom/vlingo/midas/util/ServerDetails;

    move-result-object v20

    .line 177
    .local v20, "servers":Lcom/vlingo/midas/util/ServerDetails;
    new-instance v26, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;

    const-string/jumbo v27, "SERVER_NAME"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/midas/util/ServerDetails;->getASRHost()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 178
    new-instance v26, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;

    const-string/jumbo v27, "SERVICES_HOST_NAME"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/midas/util/ServerDetails;->getVCSHost()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 179
    new-instance v26, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;

    const-string/jumbo v27, "EVENTLOG_HOST_NAME"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/midas/util/ServerDetails;->getLogHost()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 180
    new-instance v26, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;

    const-string/jumbo v27, "HELLO_HOST_NAME"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/midas/util/ServerDetails;->getHelloHost()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 181
    new-instance v26, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;

    const-string/jumbo v27, "LMTT_HOST_NAME"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Lcom/vlingo/midas/util/ServerDetails;->getLMTTHost()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/ServerPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 183
    invoke-static {}, Lcom/vlingo/sdk/internal/util/Screen;->getInstance()Lcom/vlingo/sdk/internal/util/Screen;

    move-result-object v19

    .line 185
    .local v19, "screen":Lcom/vlingo/sdk/internal/util/Screen;
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "screen.width"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Lcom/vlingo/sdk/internal/util/Screen;->getWidth()I

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 186
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "screen.mag"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/vlingo/sdk/internal/util/Screen;->getMagnification()F

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 187
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "plot.width"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 188
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "max.width"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 189
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "wa.image.preloads"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const/16 v27, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 190
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "wa.image.overlays"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const/16 v27, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 191
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "wa.download.timeout"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, "30"

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 192
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "wa.download.delay"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, "0"

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 194
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "asr.manager"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v27, Lcom/vlingo/core/internal/settings/Settings;->DEFAULT_ASR_MANAGER:Z

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 195
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "FIELD_ID"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    new-instance v27, Lcom/vlingo/midas/MidasValues;

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/midas/settings/debug/DebugSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Lcom/vlingo/midas/MidasValues;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v27 .. v27}, Lcom/vlingo/midas/MidasValues;->getDefaultFieldId()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 197
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "SMS_Simulation"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, "1"

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->withValue(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 198
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "EMail_Simulation"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, "1"

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->withValue(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 199
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "TTS_Simulation"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, "1"

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->withValue(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 201
    const-string/jumbo v26, "test.server.contact"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    .line 202
    .local v24, "testResolveContact":Landroid/preference/Preference;
    new-instance v26, Lcom/vlingo/midas/settings/debug/DebugSettings$5;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$5;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 236
    const-string/jumbo v26, "test.analyze.contact"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v23

    .line 237
    .local v23, "testAnalyzeContact":Landroid/preference/Preference;
    new-instance v26, Lcom/vlingo/midas/settings/debug/DebugSettings$6;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$6;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 246
    const-string/jumbo v26, "SPEAKER_ID"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    .line 247
    .local v22, "speakerId":Landroid/preference/Preference;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSpeakerID()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 248
    new-instance v26, Lcom/vlingo/midas/settings/debug/DebugSettings$7;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$7;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 259
    const-string/jumbo v26, "FAKE_DEVICE_ID"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    .line 260
    .local v7, "fakeId":Landroid/preference/CheckBoxPreference;
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getUUIDDeviceID()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 261
    new-instance v26, Lcom/vlingo/midas/settings/debug/DebugSettings$8;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$8;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 277
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "trial_tts_string"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$9;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$9;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->onPreferenceChange(Landroid/preference/Preference$OnPreferenceChangeListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 348
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "vcs.timeout.ms.str"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$10;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$10;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->onPreferenceChange(Landroid/preference/Preference$OnPreferenceChangeListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 364
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "barge_in_enabled"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 369
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "dynamic_config_disabled"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 372
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "USE_EDM"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const/16 v27, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 375
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "SEND_WAKEUP_WORD_HEADERS"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, "SEND_WAKEUP_WORD_HEADERS"

    const/16 v28, 0x1

    invoke-static/range {v27 .. v28}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 378
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "asr_event_logging"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 381
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "nlu_event_logging"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 384
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "show_all_languages"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 387
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "ru-RU_enable"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 391
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "CARRIER_COUNTRY"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, ""

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_CarrierCountry:Landroid/preference/EditTextPreference;

    .line 392
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "CARRIER"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, ""

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_Carrier:Landroid/preference/EditTextPreference;

    .line 393
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "FAKE_LAT"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, ""

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_lat:Landroid/preference/EditTextPreference;

    .line 394
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "FAKE_LONG"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, ""

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_long:Landroid/preference/EditTextPreference;

    .line 396
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "FAKE_LAT_LONG"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$11;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$11;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Landroid/preference/CheckBoxPreference;

    .line 403
    .local v8, "fakeLocation":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v8}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v26

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->enableFakeLatLong(Z)V

    .line 405
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "CHINESE_NAVIGATION_AUTONAVI"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_Autonavi:Landroid/preference/CheckBoxPreference;

    .line 406
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "CHINESE_NAVIGATION_BAIDU"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_BaiduMaps:Landroid/preference/CheckBoxPreference;

    .line 407
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "CHINESE_NAVIGATION_ENAVI"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_eNavi:Landroid/preference/CheckBoxPreference;

    .line 408
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "FAKE_CHINESE_NAVIGATION_APP"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$12;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$12;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/CheckBoxPreference;

    .line 414
    .local v9, "fakeNavigationApp":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v9}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v26

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->enableFakeNavigationApp(Z)V

    .line 416
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "video_calling_supported_value"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_fake_VideoCall_value:Landroid/preference/CheckBoxPreference;

    .line 417
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "fake_video_calling"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$13;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$13;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Landroid/preference/CheckBoxPreference;

    .line 423
    .local v11, "fakeVideoCalling":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v11}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v26

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->enableFakeVideoCalling(Z)V

    .line 425
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "use_hidden_calendars"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_useHiddenCalendars:Landroid/preference/CheckBoxPreference;

    .line 427
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "DEVICE_MODEL"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, ""

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->mFakedDeviceModelValue:Landroid/preference/EditTextPreference;

    .line 428
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "FAKE_DEVICE_MODEL"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$14;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$14;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/CheckBoxPreference;

    .line 434
    .local v6, "fakeDeviceModelCheckboxChecked":Landroid/preference/CheckBoxPreference;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->mFakedDeviceModelValue:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 436
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "faked_sales_code_value"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, ""

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->mFakedSalesCodeValue:Landroid/preference/EditTextPreference;

    .line 437
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "fake_sales_code"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$15;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$15;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v10

    check-cast v10, Landroid/preference/CheckBoxPreference;

    .line 443
    .local v10, "fakeSalesCodeCheckboxChecked":Landroid/preference/CheckBoxPreference;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->mFakedSalesCodeValue:Landroid/preference/EditTextPreference;

    move-object/from16 v26, v0

    invoke-virtual {v10}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v27

    invoke-virtual/range {v26 .. v27}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 445
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "SERVER_RESONSE_FILE"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, "serverReponseFile"

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/EditTextPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_server_response_file:Landroid/preference/EditTextPreference;

    .line 448
    invoke-static {}, Lcom/vlingo/sdk/util/SDKDebugSettings$ServerResponseLoggingState;->getStringValues()[Ljava/lang/String;

    move-result-object v25

    .line 450
    .local v25, "values":[Ljava/lang/String;
    new-instance v26, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;

    const-string/jumbo v27, "SERVER_RESONSE_LOGGGING"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v27, "None"

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->defaultTo(Ljava/lang/String;)Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->showAsSummary()Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->setEntries([Ljava/lang/String;)Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->setEntryValues([Ljava/lang/String;)Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;

    move-result-object v26

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$16;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$16;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->onPreferenceUpdated(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    move-result-object v26

    check-cast v26, Landroid/preference/ListPreference;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_server_response_logging:Landroid/preference/ListPreference;

    .line 461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_server_response_logging:Landroid/preference/ListPreference;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->updateServerReponseHandling(Ljava/lang/String;)V

    .line 463
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "seamless_wakeup"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    sget-object v27, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    .line 473
    new-instance v26, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;

    const-string/jumbo v27, "call_log_name_matching_value"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;-><init>(Ljava/lang/String;)V

    new-instance v27, Lcom/vlingo/midas/settings/debug/DebugSettings$17;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$17;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/EditTextPreferenceBuilder;->onPreferenceChange(Landroid/preference/Preference$OnPreferenceChangeListener;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->register(Landroid/preference/PreferenceActivity;Lcom/vlingo/core/internal/debug/PreferencesUpdater;)Landroid/preference/Preference;

    .line 488
    new-instance v26, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;

    const-string/jumbo v27, "driving_mode_audio_files"

    invoke-direct/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;-><init>(Ljava/lang/String;)V

    sget-object v27, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual/range {v26 .. v27}, Lcom/vlingo/core/internal/debug/CheckBoxPreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    .line 490
    new-instance v16, Lcom/vlingo/midas/settings/debug/DebugSettings$18;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$18;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    .line 522
    .local v16, "lmttPrefListener":Landroid/preference/Preference$OnPreferenceClickListener;
    new-instance v13, Lcom/vlingo/midas/settings/debug/DebugSettings$19;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/vlingo/midas/settings/debug/DebugSettings$19;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    .line 530
    .local v13, "forceContactNormaliztionListener":Landroid/preference/Preference$OnPreferenceClickListener;
    new-instance v12, Lcom/vlingo/midas/settings/debug/DebugSettings$20;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/vlingo/midas/settings/debug/DebugSettings$20;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    .line 538
    .local v12, "forceContactEncodingListener":Landroid/preference/Preference$OnPreferenceClickListener;
    new-instance v3, Lcom/vlingo/midas/settings/debug/DebugSettings$21;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/vlingo/midas/settings/debug/DebugSettings$21;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    .line 553
    .local v3, "cleanAcceptedTOSInfoListener":Landroid/preference/Preference$OnPreferenceClickListener;
    new-instance v18, Lcom/vlingo/midas/settings/debug/DebugSettings$22;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings$22;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V

    .line 577
    .local v18, "offerCarModeListener":Landroid/preference/Preference$OnPreferenceClickListener;
    const-string/jumbo v26, "LMTT_CONTACTS_FORCE_FULL"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 578
    const-string/jumbo v26, "LMTT_CONTACTS_CLEAR_CLIENTDB"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 579
    const-string/jumbo v26, "LMTT_CONTACTS_SENDLMTT"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 580
    const-string/jumbo v26, "LMTT_MUSIC_FORCE_FULL"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 581
    const-string/jumbo v26, "LMTT_MUSIC_CLEAR_CLIENTDB"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 582
    const-string/jumbo v26, "LMTT_MUSIC_SENDLMTT"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 584
    const-string/jumbo v26, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->registerLMTTUpdateVersionListener(Ljava/lang/String;)V

    .line 585
    const-string/jumbo v26, ".pim"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->registerLMTTUpdateVersionListener(Ljava/lang/String;)V

    .line 586
    const-string/jumbo v26, ".music"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->registerLMTTUpdateVersionListener(Ljava/lang/String;)V

    .line 608
    const-string/jumbo v26, "force_contact_normalization"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 609
    const-string/jumbo v26, "force_contact_encoding"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 610
    const-string/jumbo v26, "clean_accepted_notifications_info"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 611
    const-string/jumbo v26, "offer_app_car_mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 613
    const-string/jumbo v26, "Debug Settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/debug/DebugSettings;->setTitle(Ljava/lang/CharSequence;)V

    .line 616
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 657
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 660
    iget-boolean v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_KillProcessOnDestroy:Z

    if-eqz v0, :cond_0

    .line 661
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 663
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 675
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 676
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 677
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 667
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 669
    invoke-direct {p0}, Lcom/vlingo/midas/settings/debug/DebugSettings;->updateMenuDescriptions()V

    .line 670
    invoke-static {}, Lcom/vlingo/core/internal/settings/Settings;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 671
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 682
    if-nez p2, :cond_1

    .line 686
    :cond_0
    :goto_0
    return-void

    .line 683
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->changeActions:Lcom/vlingo/core/internal/debug/PreferencesUpdater;

    invoke-virtual {v0, p2}, Lcom/vlingo/core/internal/debug/PreferencesUpdater;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;

    invoke-interface {v0}, Lcom/vlingo/core/internal/debug/PreferenceUpdateAction;->onPreferenceUpdated()V

    goto :goto_0
.end method

.method protected registerLMTTUpdateVersionListener(Ljava/lang/String;)V
    .locals 2
    .param p1, "suffix"    # Ljava/lang/String;

    .prologue
    .line 620
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "debug.lmtt.update.version"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/debug/DebugSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Lcom/vlingo/midas/settings/debug/DebugSettings$23;

    invoke-direct {v1, p0, p1}, Lcom/vlingo/midas/settings/debug/DebugSettings$23;-><init>(Lcom/vlingo/midas/settings/debug/DebugSettings;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 630
    return-void
.end method

.method updateServerReponseHandling(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 634
    iget-object v1, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_server_response_file:Landroid/preference/EditTextPreference;

    const-string/jumbo v0, "None"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 635
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings;->m_server_response_logging:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 636
    return-void

    .line 634
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
