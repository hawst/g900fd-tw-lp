.class Lcom/vlingo/midas/settings/SettingsScreen$7$3;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen$7;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen$7;)V
    .locals 0

    .prologue
    .line 967
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 970
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v1, v1, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/settings/SettingsScreen;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 971
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v1, v1, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 974
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v1, v1, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v1, v1, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 975
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$7$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$7;

    iget-object v1, v1, Lcom/vlingo/midas/settings/SettingsScreen$7;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeOfficeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$700(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->cancel()V

    .line 977
    :cond_0
    return-void
.end method
