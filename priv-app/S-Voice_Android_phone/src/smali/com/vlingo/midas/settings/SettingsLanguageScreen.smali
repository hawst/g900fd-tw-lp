.class public Lcom/vlingo/midas/settings/SettingsLanguageScreen;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "SettingsLanguageScreen.java"


# static fields
.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"

.field public static isLaunchBySettings:Z

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field private fragment:Landroid/app/Fragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->isLaunchBySettings:Z

    .line 27
    const-class v0, Lcom/vlingo/midas/settings/SettingsLanguageScreen;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    const/16 v13, 0x400

    .line 33
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    sget v11, Lcom/vlingo/midas/R$layout;->fragment_container:I

    invoke-virtual {p0, v11}, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->setContentView(I)V

    .line 35
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 36
    .local v3, "intent":Landroid/content/Intent;
    const-string/jumbo v11, "is_from_svoice"

    const/4 v12, 0x0

    invoke-virtual {v3, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 39
    .local v4, "isFromSvoice":Z
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 40
    const/4 v11, 0x1

    invoke-virtual {p0, v11}, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->setRequestedOrientation(I)V

    .line 41
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11, v13, v13}, Landroid/view/Window;->setFlags(II)V

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    .line 47
    .local v8, "titlebar":Landroid/app/ActionBar;
    if-eqz v8, :cond_1

    .line 48
    sget v11, Lcom/vlingo/midas/R$mipmap;->svoice:I

    invoke-virtual {v8, v11}, Landroid/app/ActionBar;->setIcon(I)V

    .line 49
    sget v11, Lcom/vlingo/midas/R$string;->settings_language_dialogtitle:I

    invoke-virtual {p0, v11}, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 55
    const/16 v11, 0xc

    invoke-virtual {v8, v11}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 60
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 61
    .local v5, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string/jumbo v12, "privateFlags"

    invoke-virtual {v11, v12}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 62
    .local v6, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string/jumbo v12, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v11, v12}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    .line 63
    .local v7, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string/jumbo v12, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v11, v12}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 64
    .local v1, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v0, 0x0

    .local v0, "currentPrivateFlags":I
    const/4 v10, 0x0

    .local v10, "valueofFlagsEnableStatusBar":I
    const/4 v9, 0x0

    .line 65
    .local v9, "valueofFlagsDisableTray":I
    invoke-virtual {v6, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 66
    invoke-virtual {v7, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v10

    .line 67
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v9

    .line 68
    or-int/2addr v0, v10

    .line 69
    or-int/2addr v0, v9

    .line 70
    invoke-virtual {v6, v5, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 71
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 79
    .end local v0    # "currentPrivateFlags":I
    .end local v1    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v5    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v6    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v7    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v9    # "valueofFlagsDisableTray":I
    .end local v10    # "valueofFlagsEnableStatusBar":I
    :goto_0
    new-instance v11, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    invoke-direct {v11}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;-><init>()V

    iput-object v11, p0, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->fragment:Landroid/app/Fragment;

    .line 80
    iget-object v11, p0, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->fragment:Landroid/app/Fragment;

    if-eqz v11, :cond_2

    .line 81
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v11

    sget v12, Lcom/vlingo/midas/R$id;->fragment_placeholder:I

    iget-object v13, p0, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->fragment:Landroid/app/Fragment;

    invoke-virtual {v11, v12, v13}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/FragmentTransaction;->commit()I

    .line 84
    :cond_2
    return-void

    .line 72
    :catch_0
    move-exception v2

    .line 73
    .local v2, "e":Ljava/lang/NoSuchFieldException;
    const-string/jumbo v11, "SettingsLanguageScreen"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "NoSuchFieldException "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/lang/NoSuchFieldException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ", continuing"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 74
    .end local v2    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v2

    .line 75
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v11, "SettingsLanguageScreen"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Exception "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ", continuing"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 88
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 92
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 90
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingsLanguageScreen;->finish()V

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
