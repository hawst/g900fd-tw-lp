.class Lcom/vlingo/midas/settings/debug/DebugSettings$4;
.super Ljava/lang/Object;
.source "DebugSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/debug/DebugSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$4;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 157
    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    .line 159
    .local v1, "stringValue":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 160
    .local v2, "x":I
    if-lez v2, :cond_0

    .line 161
    const-string/jumbo v3, "nothing_recognized_reprompt.max_value"

    invoke-static {v3, v1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    .end local v2    # "x":I
    :cond_0
    :goto_0
    return v5

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "ex":Ljava/lang/NumberFormatException;
    iget-object v3, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$4;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    const-string/jumbo v4, "NumberFormatException"

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
