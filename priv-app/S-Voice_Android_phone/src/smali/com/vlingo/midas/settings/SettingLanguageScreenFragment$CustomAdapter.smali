.class Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SettingLanguageScreenFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CustomAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field context:Landroid/app/Activity;

.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;Landroid/app/Activity;)V
    .locals 2
    .param p2, "context"    # Landroid/app/Activity;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;->this$0:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    .line 200
    sget v0, Lcom/vlingo/midas/R$layout;->widget_language_list_view:I

    # getter for: Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguageDescriptions:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->access$200(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 201
    iput-object p2, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;->context:Landroid/app/Activity;

    .line 202
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 205
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;->context:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 206
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v3, Lcom/vlingo/midas/R$layout;->widget_language_list_view:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 207
    .local v2, "row":Landroid/view/View;
    sget v3, Lcom/vlingo/midas/R$id;->text1:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    .line 208
    .local v1, "label":Landroid/widget/CheckedTextView;
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment$CustomAdapter;->this$0:Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->settableLanguageDescriptions:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;->access$200(Lcom/vlingo/midas/settings/SettingLanguageScreenFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    return-object v2
.end method
