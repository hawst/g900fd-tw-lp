.class public Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;
.super Landroid/preference/DialogPreference;
.source "AsrTimeoutPreference.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# static fields
.field private static final androidns:Ljava/lang/String; = "http://schemas.android.com/apk/res/android"


# instance fields
.field private mDefaultValue:I

.field private mKey:Ljava/lang/String;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const-string/jumbo v1, "http://schemas.android.com/apk/res/android"

    const-string/jumbo v2, "defaultValue"

    const/4 v3, 0x0

    invoke-interface {p2, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mDefaultValue:I

    .line 28
    sget v1, Lcom/vlingo/midas/R$layout;->asr_http_timeout_layout:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->setDialogLayoutResource(I)V

    .line 29
    const-string/jumbo v1, "http://schemas.android.com/apk/res/android"

    const-string/jumbo v2, "title"

    invoke-interface {p2, v1, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "title":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "connect"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    const-string/jumbo v1, "asr.http.timeout.connect_ms"

    iput-object v1, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mKey:Ljava/lang/String;

    .line 38
    :goto_0
    return-void

    .line 36
    :cond_0
    const-string/jumbo v1, "asr.http.timeout.read_ms"

    iput-object v1, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mKey:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 42
    sget v2, Lcom/vlingo/midas/R$id;->asr_http_timeout_layout:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mSeekBar:Landroid/widget/SeekBar;

    .line 43
    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 44
    sget v2, Lcom/vlingo/midas/R$id;->asr_http_timeout_preference_text:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mTextView:Landroid/widget/TextView;

    .line 45
    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mSeekBar:Landroid/widget/SeekBar;

    const v3, 0xea60

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 47
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 49
    .local v0, "pref":Landroid/content/SharedPreferences;
    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mKey:Ljava/lang/String;

    iget v3, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mDefaultValue:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 50
    .local v1, "value":I
    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 51
    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mTextView:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 54
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 3
    .param p1, "positiveResult"    # Z

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->getEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 60
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 62
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 65
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 66
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/AsrTimeoutPreference;->mTextView:Landroid/widget/TextView;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 79
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 85
    return-void
.end method
