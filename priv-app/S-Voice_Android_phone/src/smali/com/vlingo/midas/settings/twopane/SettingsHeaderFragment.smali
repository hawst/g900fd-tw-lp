.class public Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;
.super Landroid/app/ListFragment;
.source "SettingsHeaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;
    }
.end annotation


# instance fields
.field mHeadlineSelectedListener:Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;

.field mHeadlinesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mListAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private selectedIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mHeadlinesList:Ljava/util/List;

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->selectedIndex:I

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mHeadlineSelectedListener:Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;

    .line 24
    return-void
.end method


# virtual methods
.method public loadCategory()V
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mHeadlinesList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 56
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mHeadlinesList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->settings_general_lower:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mHeadlinesList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->settings_incar_wakeup_lower:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mHeadlinesList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->advaced_lower:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 60
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 29
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$layout;->headline_item:I

    iget-object v3, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mHeadlinesList:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mListAdapter:Landroid/widget/ArrayAdapter;

    .line 30
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 31
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 1
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 43
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 44
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mHeadlineSelectedListener:Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mHeadlineSelectedListener:Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;

    invoke-interface {v0, p3}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;->onHeadlineSelected(I)V

    .line 46
    iput p3, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->selectedIndex:I

    .line 48
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 35
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 36
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 37
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->selectedIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 38
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->loadCategory()V

    .line 39
    return-void
.end method

.method public setOnHeadlineSelectedListener(Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment;->mHeadlineSelectedListener:Lcom/vlingo/midas/settings/twopane/SettingsHeaderFragment$OnHeadlineSelectedListener;

    .line 79
    return-void
.end method
