.class public Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;
.super Landroid/app/Fragment;
.source "SettingEraseServerDataScreenFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

.field private mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->context:Landroid/content/Context;

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V
    .locals 1
    .param p1, "onFragmentLoadListener"    # Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->context:Landroid/content/Context;

    .line 36
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;)Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 119
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 120
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->setOnSettingsContentClickListener(Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;)V

    .line 123
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->onHeadlineSelected(I)V

    .line 139
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "paramView"    # Landroid/view/View;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->context:Landroid/content/Context;

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;->onItemSelected(I)V

    .line 132
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 104
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->context:Landroid/content/Context;

    .line 105
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$layout;->settings_path_view:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 107
    .local v2, "view":Landroid/view/View;
    sget v3, Lcom/vlingo/midas/R$id;->fragment_description:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 108
    .local v1, "textView":Landroid/widget/TextView;
    sget v3, Lcom/vlingo/midas/R$id;->icon:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 109
    .local v0, "iconView":Landroid/widget/ImageView;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 110
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->erase_server_data_body:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    if-eqz v3, :cond_0

    .line 113
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    const/4 v4, 0x1

    invoke-interface {v3, v2, v4}, Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;->updatePathHeader(Landroid/view/View;I)V

    .line 115
    .end local v0    # "iconView":Landroid/widget/ImageView;
    .end local v1    # "textView":Landroid/widget/TextView;
    .end local v2    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x400

    .line 46
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 47
    sget v3, Lcom/vlingo/midas/R$layout;->erase_server_data_layout:I

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 48
    .local v2, "rootView":Landroid/view/View;
    sget v3, Lcom/vlingo/midas/R$id;->erase_server_data_body:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 52
    .local v1, "dataBodyTextview":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->erase_server_data_title_without_car_mode:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    sget v3, Lcom/vlingo/midas/R$id;->erase_server_data_btn:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 55
    .local v0, "btn":Landroid/widget/Button;
    new-instance v3, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;-><init>(Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    return-object v2
.end method

.method setOnSettingsContentClickListener(Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;)V
    .locals 0
    .param p1, "mClickListener"    # Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    .line 127
    return-void
.end method
