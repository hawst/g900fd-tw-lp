.class Lcom/vlingo/midas/settings/debug/ServerPreference$1;
.super Ljava/lang/Object;
.source "ServerPreference.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/debug/ServerPreference;->onBindDialogView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/debug/ServerPreference;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/debug/ServerPreference;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/vlingo/midas/settings/debug/ServerPreference$1;->this$0:Lcom/vlingo/midas/settings/debug/ServerPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-eqz p3, :cond_0

    .line 81
    iget-object v1, p0, Lcom/vlingo/midas/settings/debug/ServerPreference$1;->this$0:Lcom/vlingo/midas/settings/debug/ServerPreference;

    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/ServerPreference$1;->this$0:Lcom/vlingo/midas/settings/debug/ServerPreference;

    iget-object v0, v0, Lcom/vlingo/midas/settings/debug/ServerPreference;->servers:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # setter for: Lcom/vlingo/midas/settings/debug/ServerPreference;->value:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/vlingo/midas/settings/debug/ServerPreference;->access$002(Lcom/vlingo/midas/settings/debug/ServerPreference;Ljava/lang/String;)Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/ServerPreference$1;->this$0:Lcom/vlingo/midas/settings/debug/ServerPreference;

    # getter for: Lcom/vlingo/midas/settings/debug/ServerPreference;->editText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/vlingo/midas/settings/debug/ServerPreference;->access$100(Lcom/vlingo/midas/settings/debug/ServerPreference;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/settings/debug/ServerPreference$1;->this$0:Lcom/vlingo/midas/settings/debug/ServerPreference;

    # getter for: Lcom/vlingo/midas/settings/debug/ServerPreference;->value:Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/midas/settings/debug/ServerPreference;->access$000(Lcom/vlingo/midas/settings/debug/ServerPreference;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 84
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
