.class public final enum Lcom/vlingo/midas/settings/MidasSettings$DeviceType;
.super Ljava/lang/Enum;
.source "MidasSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/MidasSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeviceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/midas/settings/MidasSettings$DeviceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

.field public static final enum PHONE_EXTRA_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

.field public static final enum PHONE_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

.field public static final enum PHONE_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

.field public static final enum TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

.field public static final enum TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 147
    new-instance v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    const-string/jumbo v1, "PHONE_HIGH_RESOLUTION"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    new-instance v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    const-string/jumbo v1, "PHONE_LOW_RESOLUTION"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    new-instance v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    const-string/jumbo v1, "PHONE_EXTRA_HIGH_RESOLUTION"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_EXTRA_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    new-instance v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    const-string/jumbo v1, "TABLET_HIGH_RESOLUTION"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    new-instance v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    const-string/jumbo v1, "TABLET_LOW_RESOLUTION"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    .line 146
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->PHONE_EXTRA_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->$VALUES:[Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/midas/settings/MidasSettings$DeviceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 146
    const-class v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/midas/settings/MidasSettings$DeviceType;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->$VALUES:[Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    invoke-virtual {v0}, [Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    return-object v0
.end method
