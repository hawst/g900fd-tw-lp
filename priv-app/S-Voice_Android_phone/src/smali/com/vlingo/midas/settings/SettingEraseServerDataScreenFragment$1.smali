.class Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;
.super Ljava/lang/Object;
.source "SettingEraseServerDataScreenFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 57
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v7, "connectivity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 58
    .local v1, "cManager":Landroid/net/ConnectivityManager;
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 59
    .local v4, "mobile":Landroid/net/NetworkInfo;
    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v5

    .line 60
    .local v5, "wifi":Landroid/net/NetworkInfo;
    const/4 v3, 0x1

    .line 61
    .local v3, "isDataConnected":Z
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    invoke-virtual {v6}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/vlingo/midas/settings/MidasSettings;->isWifiOnly(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-nez v6, :cond_2

    .line 62
    const/4 v3, 0x0

    .line 67
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 68
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 69
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    sget v7, Lcom/vlingo/midas/R$string;->unable_to_connect_to_server:I

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 70
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    sget v7, Lcom/vlingo/midas/R$string;->unable_to_connect_network:I

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 72
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    sget v7, Lcom/vlingo/midas/R$string;->ok_uc:I

    invoke-virtual {v6, v7}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1$1;

    invoke-direct {v7, p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1$1;-><init>(Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 78
    new-instance v6, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1$2;

    invoke-direct {v6, p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1$2;-><init>(Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 96
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_1
    :goto_1
    return-void

    .line 63
    :cond_2
    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-nez v6, :cond_0

    .line 64
    const/4 v3, 0x0

    goto :goto_0

    .line 87
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 88
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    # getter for: Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->mSettingsContentClickListener:Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;
    invoke-static {v6}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->access$000(Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;)Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;

    move-result-object v6

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnSettingsItemSelectedListener;->onItemSelected(I)V

    goto :goto_1

    .line 92
    :cond_4
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 93
    new-instance v2, Landroid/content/Intent;

    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    invoke-virtual {v6}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 94
    .local v2, "intent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment$1;->this$0:Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;

    invoke-virtual {v6, v2}, Lcom/vlingo/midas/settings/SettingEraseServerDataScreenFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
