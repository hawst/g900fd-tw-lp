.class public abstract Lcom/vlingo/midas/settings/debug/PreferenceUpdateListenerAdapter;
.super Ljava/lang/Object;
.source "PreferenceUpdateListenerAdapter.java"

# interfaces
.implements Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceUpdated(Landroid/preference/Preference;)V
    .locals 0
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 18
    return-void
.end method

.method protected valueOf(Landroid/preference/Preference;)Ljava/lang/Object;
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 22
    const/4 v0, 0x0

    .line 24
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, p1, Landroid/preference/EditTextPreference;

    if-eqz v1, :cond_1

    .line 25
    check-cast p1, Landroid/preference/EditTextPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v0

    .line 33
    .end local v0    # "value":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v0

    .line 26
    .restart local v0    # "value":Ljava/lang/Object;
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_1
    instance-of v1, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_2

    .line 27
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .local v0, "value":Ljava/lang/Boolean;
    goto :goto_0

    .line 28
    .local v0, "value":Ljava/lang/Object;
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_2
    instance-of v1, p1, Landroid/preference/ListPreference;

    if-eqz v1, :cond_3

    .line 29
    check-cast p1, Landroid/preference/ListPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .local v0, "value":Ljava/lang/String;
    goto :goto_0

    .line 30
    .local v0, "value":Ljava/lang/Object;
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_3
    instance-of v1, p1, Lcom/vlingo/midas/settings/debug/ServerPreference;

    if-eqz v1, :cond_0

    .line 31
    check-cast p1, Lcom/vlingo/midas/settings/debug/ServerPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Lcom/vlingo/midas/settings/debug/ServerPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .local v0, "value":Ljava/lang/String;
    goto :goto_0
.end method
