.class public Lcom/vlingo/midas/settings/ConfigBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ConfigBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 16
    if-eqz p2, :cond_0

    .line 17
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 18
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v1, "com.vlingo.midas.hello_update"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 19
    invoke-static {p2}, Lcom/vlingo/core/internal/dialogmanager/vvs/VLConfigImporter;->handleIntent(Landroid/content/Intent;)V

    .line 22
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    return-void
.end method
