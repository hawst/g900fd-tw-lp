.class Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$1;
.super Ljava/lang/Object;
.source "AdvancedFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$1;->this$1:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 188
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$1;->this$1:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;

    iget-object v4, v4, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, "homeAddress":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 190
    .local v2, "homeAddressCheck":Ljava/lang/String;
    const-string/jumbo v4, "\n"

    const-string/jumbo v5, " "

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 192
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    if-gtz v4, :cond_1

    .line 193
    const-string/jumbo v4, "car_nav_home_address"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :goto_0
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$1;->this$1:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;

    iget-object v4, v4, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    invoke-virtual {v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string/jumbo v5, "input_method"

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .line 203
    .local v3, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$1;->this$1:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;

    iget-object v4, v4, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 204
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$1;->this$1:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;

    iget-object v4, v4, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$1;->this$1:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;

    iget-object v4, v4, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 205
    iget-object v4, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4$1;->this$1:Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;

    iget-object v4, v4, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$4;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    # getter for: Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->access$200(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;)Landroid/preference/EditTextPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Dialog;->cancel()V

    .line 208
    :cond_0
    const-wide/16 v4, 0x96

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :goto_1
    return-void

    .line 195
    .end local v3    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    const-string/jumbo v4, "*#DEBUG#*"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 196
    const-string/jumbo v2, ""

    .line 197
    const/4 v4, 0x1

    sput-boolean v4, Lcom/vlingo/midas/settings/debug/DebugSettings;->SHOW_DEBUG:Z

    .line 199
    :cond_2
    const-string/jumbo v4, "car_nav_home_address"

    invoke-static {v4, v2}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 209
    .restart local v3    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
