.class Lcom/vlingo/midas/settings/debug/DebugSettings$21;
.super Ljava/lang/Object;
.source "DebugSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/debug/DebugSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/debug/DebugSettings;)V
    .locals 0

    .prologue
    .line 538
    iput-object p1, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$21;->this$0:Lcom/vlingo/midas/settings/debug/DebugSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v2, 0x0

    .line 543
    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->setTOSAccepted(Z)V

    .line 544
    const-string/jumbo v0, "tos_notification_counter_local"

    invoke-static {v0, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 545
    const-string/jumbo v0, "notification_counter_local"

    invoke-static {v0, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 546
    const-string/jumbo v0, "tos_accepted_version"

    invoke-static {v0, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 547
    const-string/jumbo v0, "accepted_notifications"

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setStringSet(Ljava/lang/String;Ljava/util/Set;)V

    .line 548
    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->setAllNotificationsAccepted(Z)V

    .line 549
    const/4 v0, 0x1

    return v0
.end method
