.class Lcom/vlingo/midas/settings/debug/DebugSettings$5$1;
.super Ljava/lang/Object;
.source "DebugSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/debug/DebugSettings$5;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/settings/debug/DebugSettings$5;

.field final synthetic val$inputName:Landroid/widget/EditText;

.field final synthetic val$inputTimeTest:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/debug/DebugSettings$5;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$5$1;->this$1:Lcom/vlingo/midas/settings/debug/DebugSettings$5;

    iput-object p2, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$5$1;->val$inputName:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$5$1;->val$inputTimeTest:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 225
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$5$1;->val$inputName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/settings/debug/DebugSettings$5$1;->val$inputTimeTest:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/SettingsTestResolveContactHelper;->startTestResolveContact(Ljava/lang/String;I)V

    .line 226
    return-void
.end method
