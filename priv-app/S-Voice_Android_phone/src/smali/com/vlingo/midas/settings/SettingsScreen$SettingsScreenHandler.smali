.class Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;
.super Landroid/os/Handler;
.source "SettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/SettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SettingsScreenHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/vlingo/midas/settings/SettingsScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 1
    .param p1, "out"    # Lcom/vlingo/midas/settings/SettingsScreen;

    .prologue
    .line 2297
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2298
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 2299
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2302
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$SettingsScreenHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/SettingsScreen;

    .line 2303
    .local v0, "o":Lcom/vlingo/midas/settings/SettingsScreen;
    if-eqz v0, :cond_0

    .line 2305
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 2306
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 2312
    :cond_0
    :goto_0
    return-void

    .line 2308
    :pswitch_0
    invoke-virtual {v0}, Lcom/vlingo/midas/settings/SettingsScreen;->updateSocialUI()V

    goto :goto_0

    .line 2306
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
