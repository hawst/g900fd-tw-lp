.class public Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;
.super Landroid/preference/DialogPreference;
.source "HttpsRolloutPreference.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# static fields
.field private static final androidns:Ljava/lang/String; = "http://schemas.android.com/apk/res/android"


# instance fields
.field private mDefaultValue:I

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const-string/jumbo v0, "http://schemas.android.com/apk/res/android"

    const-string/jumbo v1, "defaultValue"

    const/4 v2, 0x0

    invoke-interface {p2, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->mDefaultValue:I

    .line 32
    sget v0, Lcom/vlingo/midas/R$layout;->httpsrollout_preference_layout:I

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->setDialogLayoutResource(I)V

    .line 33
    return-void
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 36
    sget v2, Lcom/vlingo/midas/R$id;->httpsrollout_preference_seekbar:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->mSeekBar:Landroid/widget/SeekBar;

    .line 37
    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 38
    sget v2, Lcom/vlingo/midas/R$id;->httpsrollout_preference_text:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->mTextView:Landroid/widget/TextView;

    .line 40
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 42
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string/jumbo v2, "https.rollout_percentage"

    iget v3, p0, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->mDefaultValue:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 43
    .local v1, "value":I
    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 44
    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->mTextView:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 47
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 3
    .param p1, "positiveResult"    # Z

    .prologue
    .line 51
    if-eqz p1, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->getEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 53
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 55
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 58
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 59
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 63
    iget-object v0, p0, Lcom/vlingo/midas/settings/debug/HttpsRolloutPreference;->mTextView:Landroid/widget/TextView;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 68
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 72
    return-void
.end method
