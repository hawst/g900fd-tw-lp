.class public Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LengthToastFilter;
.super Landroid/text/InputFilter$LengthFilter;
.source "AdvancedFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/twopane/AdvancedFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LengthToastFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/settings/twopane/AdvancedFragment;I)V
    .locals 0
    .param p2, "max"    # I

    .prologue
    .line 546
    iput-object p1, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LengthToastFilter;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    .line 547
    invoke-direct {p0, p2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    .line 548
    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/16 v1, 0x12c

    .line 553
    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v1, :cond_0

    if-lt p5, v1, :cond_0

    if-lt p6, v1, :cond_0

    .line 554
    iget-object v0, p0, Lcom/vlingo/midas/settings/twopane/AdvancedFragment$LengthToastFilter;->this$0:Lcom/vlingo/midas/settings/twopane/AdvancedFragment;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/twopane/AdvancedFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->character_max_length_reached:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 556
    :cond_0
    invoke-super/range {p0 .. p6}, Landroid/text/InputFilter$LengthFilter;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
