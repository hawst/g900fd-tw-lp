.class Lcom/vlingo/midas/settings/SettingsScreen$8;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->initPreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 1046
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$8;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1052
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 1054
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1055
    .local v1, "rVal":Ljava/lang/Boolean;
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$8;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const-string/jumbo v5, "preferences_voice_wake_up"

    invoke-virtual {v4, v5, v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1057
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string/jumbo v4, "VOICE_WAKE_UP_POP"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$8;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1058
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen$8;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v2, p1}, Lcom/vlingo/midas/settings/SettingsScreen;->wakeup_lockscreen_dialog(Landroid/preference/Preference;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1077
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    return v2

    .line 1060
    :cond_0
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$8;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1061
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$8;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1065
    :goto_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isH_Device()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1066
    :cond_1
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$8;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1067
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->startAlwaysMicOnserviceIfNeeded()V

    .line 1068
    const-string/jumbo v3, "voice_wake_up"

    invoke-static {v3, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1063
    :cond_2
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$8;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;
    invoke-static {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    .line 1070
    :cond_3
    const-string/jumbo v2, "voice_wake_up"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1073
    :cond_4
    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$8;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "wake_up_lock_screen"

    iget-object v6, p0, Lcom/vlingo/midas/settings/SettingsScreen$8;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_wake_up_lock_screen:Landroid/preference/CheckBoxPreference;
    invoke-static {v6}, Lcom/vlingo/midas/settings/SettingsScreen;->access$800(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v6

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_5

    :goto_2
    invoke-static {v4, v5, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_5
    move v2, v3

    goto :goto_2
.end method
