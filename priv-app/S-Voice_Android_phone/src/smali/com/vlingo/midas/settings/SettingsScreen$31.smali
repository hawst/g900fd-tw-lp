.class Lcom/vlingo/midas/settings/SettingsScreen$31;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/SettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field str:Ljava/lang/String;

.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 2359
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$31;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 2374
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x12c

    if-le v0, v1, :cond_0

    .line 2375
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$31;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x12b

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2376
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$31;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$31;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 2377
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$31;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->maxLengToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingsScreen;->access$1000(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2379
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 2369
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$31;->str:Ljava/lang/String;

    .line 2370
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 2365
    return-void
.end method
