.class Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/SettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InputTextWatcher"
.end annotation


# instance fields
.field private addressDialog:Landroid/app/Dialog;

.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/app/Dialog;)V
    .locals 0
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 2321
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2322
    iput-object p2, p0, Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;->addressDialog:Landroid/app/Dialog;

    .line 2323
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 2349
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 2356
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v3, 0x1

    .line 2328
    const/4 v0, 0x0

    .line 2330
    .local v0, "positiveButton":Landroid/widget/Button;
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;->addressDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 2331
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;->addressDialog:Landroid/app/Dialog;

    const v2, 0x1020019

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "positiveButton":Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 2333
    .restart local v0    # "positiveButton":Landroid/widget/Button;
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;->addressDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 2334
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 2335
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2343
    :cond_1
    :goto_0
    return-void

    .line 2336
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/vlingo/midas/settings/SettingsScreen;->containsNoAlphaNumeric(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/SettingsScreen;->access$900(Lcom/vlingo/midas/settings/SettingsScreen;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2338
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 2340
    :cond_4
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
