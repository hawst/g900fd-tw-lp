.class Lcom/vlingo/midas/settings/SettingsScreen$6;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->initPreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 802
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 9
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 807
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 809
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    .line 810
    .local v2, "dialog_editbox":Landroid/widget/EditText;
    invoke-virtual {v2}, Landroid/widget/EditText;->selectAll()V

    .line 812
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 813
    new-array v3, v7, [Landroid/text/InputFilter;

    new-instance v4, Lcom/vlingo/midas/settings/SettingsScreen$LengthToastFilter;

    iget-object v5, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const/16 v6, 0x12c

    invoke-direct {v4, v5, v6}, Lcom/vlingo/midas/settings/SettingsScreen$LengthToastFilter;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;I)V

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 817
    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 818
    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 819
    sget v3, Lcom/vlingo/midas/R$string;->settings_configure_home_address:I

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(I)V

    .line 820
    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v7, v3}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 824
    new-instance v3, Landroid/widget/Scroller;

    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v4}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setScroller(Landroid/widget/Scroller;)V

    .line 825
    new-instance v3, Lcom/vlingo/midas/settings/SettingsScreen$6$1;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/SettingsScreen$6$1;-><init>(Lcom/vlingo/midas/settings/SettingsScreen$6;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 832
    new-instance v3, Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;

    iget-object v4, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    iget-object v5, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v5}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v5

    invoke-virtual {v5}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/vlingo/midas/settings/SettingsScreen$InputTextWatcher;-><init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/app/Dialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 833
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 834
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    const v4, 0x1020019

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 837
    .local v0, "button":Landroid/widget/Button;
    new-instance v3, Lcom/vlingo/midas/settings/SettingsScreen$6$2;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/SettingsScreen$6$2;-><init>(Lcom/vlingo/midas/settings/SettingsScreen$6;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 874
    .end local v0    # "button":Landroid/widget/Button;
    :cond_0
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 875
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    const v4, 0x102001a

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 878
    .local v1, "button2":Landroid/widget/Button;
    new-instance v3, Lcom/vlingo/midas/settings/SettingsScreen$6$3;

    invoke-direct {v3, p0}, Lcom/vlingo/midas/settings/SettingsScreen$6$3;-><init>(Lcom/vlingo/midas/settings/SettingsScreen$6;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 901
    .end local v1    # "button2":Landroid/widget/Button;
    .end local v2    # "dialog_editbox":Landroid/widget/EditText;
    :cond_1
    return v8
.end method
