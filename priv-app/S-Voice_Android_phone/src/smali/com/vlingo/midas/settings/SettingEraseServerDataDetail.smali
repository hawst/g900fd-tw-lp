.class public Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "SettingEraseServerDataDetail.java"


# instance fields
.field private fragment:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0xc

    .line 18
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 20
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;->setRequestedOrientation(I)V

    .line 22
    :cond_0
    sget v1, Lcom/vlingo/midas/R$layout;->fragment_container:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;->setContentView(I)V

    .line 23
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 24
    .local v0, "titlebar":Landroid/app/ActionBar;
    if-eqz v0, :cond_1

    .line 25
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 26
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 37
    :cond_1
    :goto_0
    new-instance v1, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    invoke-direct {v1}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;-><init>()V

    iput-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;->fragment:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    .line 38
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;->fragment:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    if-eqz v1, :cond_2

    .line 39
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->fragment_placeholder:I

    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;->fragment:Lcom/vlingo/midas/settings/SettingEraseServerDataDetailFragment;

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 41
    :cond_2
    return-void

    .line 28
    :cond_3
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 30
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 31
    sget v1, Lcom/vlingo/midas/R$string;->erase_server_data_body:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 33
    :cond_4
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 34
    sget v1, Lcom/vlingo/midas/R$string;->erase_server_data_body:I

    invoke-virtual {p0, v1}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 45
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 49
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 47
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/SettingEraseServerDataDetail;->finish()V

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
