.class Lcom/vlingo/midas/settings/SettingsScreen$3;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->initPreference()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 648
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$3;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 652
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 653
    .local v0, "currentTime":J
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$3;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mSettingTimestampForDebug:J
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$400(Lcom/vlingo/midas/settings/SettingsScreen;)J

    move-result-wide v3

    sub-long v3, v0, v3

    const-wide/16 v5, 0x1388

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    .line 654
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$3;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const/4 v4, 0x1

    # setter for: Lcom/vlingo/midas/settings/SettingsScreen;->mSettingClicksForDebug:I
    invoke-static {v3, v4}, Lcom/vlingo/midas/settings/SettingsScreen;->access$502(Lcom/vlingo/midas/settings/SettingsScreen;I)I

    .line 658
    :goto_0
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$3;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # setter for: Lcom/vlingo/midas/settings/SettingsScreen;->mSettingTimestampForDebug:J
    invoke-static {v3, v0, v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$402(Lcom/vlingo/midas/settings/SettingsScreen;J)J

    .line 659
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$3;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mSettingClicksForDebug:I
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$500(Lcom/vlingo/midas/settings/SettingsScreen;)I

    move-result v3

    const/4 v4, 0x4

    if-le v3, v4, :cond_0

    .line 660
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$3;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/vlingo/midas/settings/debug/DebugSettings;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 661
    .local v2, "i":Landroid/content/Intent;
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$3;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v3, v2}, Lcom/vlingo/midas/settings/SettingsScreen;->startActivity(Landroid/content/Intent;)V

    .line 663
    .end local v2    # "i":Landroid/content/Intent;
    :cond_0
    const/4 v3, 0x0

    return v3

    .line 656
    :cond_1
    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$3;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # operator++ for: Lcom/vlingo/midas/settings/SettingsScreen;->mSettingClicksForDebug:I
    invoke-static {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->access$508(Lcom/vlingo/midas/settings/SettingsScreen;)I

    goto :goto_0
.end method
