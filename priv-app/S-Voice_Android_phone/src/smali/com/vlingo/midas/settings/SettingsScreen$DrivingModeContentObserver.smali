.class Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;
.super Landroid/database/ContentObserver;
.source "SettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/midas/settings/SettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrivingModeContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method public constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 256
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    .line 257
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 258
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 263
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 265
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingsScreen;->access$000(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/SwitchPreference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mDriveMode:Landroid/preference/SwitchPreference;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingsScreen;->access$000(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/SwitchPreference;

    move-result-object v3

    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "driving_mode_on"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mVoiceInputControl:Landroid/preference/SwitchPreference;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingsScreen;->access$100(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/SwitchPreference;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 270
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mVoiceInputControl:Landroid/preference/SwitchPreference;
    invoke-static {v0}, Lcom/vlingo/midas/settings/SettingsScreen;->access$100(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/SwitchPreference;

    move-result-object v0

    iget-object v3, p0, Lcom/vlingo/midas/settings/SettingsScreen$DrivingModeContentObserver;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "voice_input_control"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 273
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 266
    goto :goto_0

    :cond_3
    move v1, v2

    .line 270
    goto :goto_1
.end method
