.class public Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;
.super Lcom/vlingo/core/internal/debug/PreferenceBuilder;
.source "ListPreferenceBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vlingo/core/internal/debug/PreferenceBuilder",
        "<",
        "Landroid/preference/ListPreference;",
        ">;"
    }
.end annotation


# static fields
.field private static showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;


# instance fields
.field private entryValues:[Ljava/lang/String;

.field private values:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder$1;

    invoke-direct {v0}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder$1;-><init>()V

    sput-object v0, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;-><init>()V

    .line 19
    sget-object v0, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->setShowAsSummary(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "setting"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;-><init>(Ljava/lang/String;)V

    .line 24
    sget-object v0, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->showAsSummaryAction:Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->setShowAsSummary(Lcom/vlingo/core/internal/debug/PreferenceUpdateListener;)V

    .line 25
    return-void
.end method


# virtual methods
.method public defaultTo(Ljava/lang/String;)Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->defaultTo(Ljava/lang/Object;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    .line 34
    return-object p0
.end method

.method public bridge synthetic onSetting(Ljava/lang/String;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->onSetting(Ljava/lang/String;)Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;

    move-result-object v0

    return-object v0
.end method

.method public onSetting(Ljava/lang/String;)Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;
    .locals 0
    .param p1, "setting"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->onSetting(Ljava/lang/String;)Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    .line 29
    return-object p0
.end method

.method public register(Landroid/preference/PreferenceActivity;)Landroid/preference/ListPreference;
    .locals 2
    .param p1, "activity"    # Landroid/preference/PreferenceActivity;

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->attach(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 55
    .local v0, "list":Landroid/preference/ListPreference;
    iget-object v1, p0, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->values:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 56
    iget-object v1, p0, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->entryValues:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 57
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->isShowAsSummary()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 60
    :cond_0
    return-object v0
.end method

.method public bridge synthetic register(Landroid/preference/PreferenceActivity;)Landroid/preference/Preference;
    .locals 1
    .param p1, "x0"    # Landroid/preference/PreferenceActivity;

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->register(Landroid/preference/PreferenceActivity;)Landroid/preference/ListPreference;

    move-result-object v0

    return-object v0
.end method

.method public setEntries([Ljava/lang/String;)Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;
    .locals 0
    .param p1, "values"    # [Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->values:[Ljava/lang/String;

    .line 44
    return-object p0
.end method

.method public setEntryValues([Ljava/lang/String;)Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;
    .locals 0
    .param p1, "values"    # [Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->entryValues:[Ljava/lang/String;

    .line 49
    return-object p0
.end method

.method public bridge synthetic showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;->showAsSummary()Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;

    move-result-object v0

    return-object v0
.end method

.method public showAsSummary()Lcom/vlingo/midas/settings/debug/ListPreferenceBuilder;
    .locals 0

    .prologue
    .line 38
    invoke-super {p0}, Lcom/vlingo/core/internal/debug/PreferenceBuilder;->showAsSummary()Lcom/vlingo/core/internal/debug/PreferenceBuilder;

    .line 39
    return-object p0
.end method
