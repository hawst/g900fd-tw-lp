.class public Lcom/vlingo/midas/settings/DrivingModeSettings;
.super Lcom/vlingo/midas/settings/VLPreferenceActivity;
.source "DrivingModeSettings.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;
    }
.end annotation


# static fields
.field private static final ALARM_NOTIFICATION:Ljava/lang/String; = "alarm_notification"

.field private static final CHATONV_NOTIFICATION:Ljava/lang/String; = "chaton_call_notification"

.field public static final DB_KEY_DRIVING_MODE_ON:Ljava/lang/String; = "driving_mode_on"

.field public static final DRIVING_MODE_ALARM_NOTIFICATION:Ljava/lang/String; = "driving_mode_alarm_notification"

.field private static final DRIVING_MODE_CHATON_CALL_NOTIFICATION:Ljava/lang/String; = "driving_mode_chaton_call_notification"

.field public static final DRIVING_MODE_INCOMING_CALL_NOTIFICATION:Ljava/lang/String; = "driving_mode_incoming_call_notification"

.field public static final DRIVING_MODE_MESSAGE_CONTENTS:Ljava/lang/String; = "driving_mode_message_contents"

.field public static final DRIVING_MODE_MESSAGE_NOTIFICATION:Ljava/lang/String; = "driving_mode_message_notification"

.field public static final DRIVING_MODE_SCHEDULE_NOTIFICATION:Ljava/lang/String; = "driving_mode_schedule_notification"

.field public static final DRIVING_MODE_VOICE_MAIL_NOTIFICATION:Ljava/lang/String; = "driving_mode_voice_mail_notification"

.field private static final INCOMING_CALL_NOTIFICATION:Ljava/lang/String; = "incoming_call_notification"

.field private static final MESSAGE_NOTIFICATION:Ljava/lang/String; = "message_notification"

.field private static final SCHEDULE_NOTIFICATION:Ljava/lang/String; = "schedule_notification"

.field private static final TAG:Ljava/lang/String; = "DrivingModeSettings"

.field private static final VOICE_MAIL_NOTIFICATION:Ljava/lang/String; = "voice_mail_notification"


# instance fields
.field private mActionBarLayout:Landroid/view/View;

.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mAlarmNotification:Landroid/preference/CheckBoxPreference;

.field private mChatonV:Landroid/preference/CheckBoxPreference;

.field private mIncomingCallNotification:Landroid/preference/CheckBoxPreference;

.field private mMessageNotification:Landroid/preference/CheckBoxPreference;

.field private mScheduleNotification:Landroid/preference/CheckBoxPreference;

.field private mSettingsObserver:Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;

.field private mVoiceMailNotification:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;-><init>()V

    .line 421
    return-void
.end method

.method static synthetic access$000(Lcom/vlingo/midas/settings/DrivingModeSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/vlingo/midas/settings/DrivingModeSettings;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->updateState()V

    return-void
.end method

.method public static areAllDrivingModeOptionsDisabled(Landroid/content/ContentResolver;)Z
    .locals 4
    .param p0, "cr"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 389
    const-string/jumbo v2, "driving_mode_on"

    invoke-static {p0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_1

    .line 393
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string/jumbo v2, "driving_mode_incoming_call_notification"

    invoke-static {p0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v3, "driving_mode_message_notification"

    invoke-static {p0, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    add-int/2addr v2, v3

    const-string/jumbo v3, "driving_mode_voice_mail_notification"

    invoke-static {p0, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    add-int/2addr v2, v3

    const-string/jumbo v3, "driving_mode_alarm_notification"

    invoke-static {p0, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    add-int/2addr v2, v3

    const-string/jumbo v3, "driving_mode_schedule_notification"

    invoke-static {p0, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    add-int/2addr v2, v3

    const-string/jumbo v3, "driving_mode_chaton_call_notification"

    invoke-static {p0, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    add-int/2addr v2, v3

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private isChatOnVInstalled()Z
    .locals 4

    .prologue
    .line 405
    const-string/jumbo v1, "com.sec.chaton"

    .line 406
    .local v1, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 410
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    const/4 v3, 0x1

    :goto_0
    return v3

    .line 412
    :catch_0
    move-exception v0

    .line 414
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 415
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private updateState()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 305
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "driving_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 307
    .local v0, "savedValue":Z
    :cond_0
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarSwitch:Landroid/widget/Switch;

    if-eqz v1, :cond_1

    .line 308
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 310
    :cond_1
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mIncomingCallNotification:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_2

    .line 311
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mIncomingCallNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 312
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mMessageNotification:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_3

    .line 313
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mMessageNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 314
    :cond_3
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mAlarmNotification:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_4

    .line 315
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mAlarmNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 316
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mScheduleNotification:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_5

    .line 317
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mScheduleNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 318
    :cond_5
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_6

    .line 319
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 320
    :cond_6
    return-void
.end method


# virtual methods
.method public onActivityCreated()V
    .locals 6

    .prologue
    const/4 v5, -0x2

    const/4 v3, 0x0

    .line 211
    new-instance v1, Landroid/widget/Switch;

    invoke-direct {v1, p0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarSwitch:Landroid/widget/Switch;

    .line 213
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$dimen;->action_bar_switch_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 215
    .local v0, "padding":I
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 216
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 219
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/16 v4, 0x15

    invoke-direct {v3, v5, v5, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 224
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarLayout:Landroid/view/View;

    .line 226
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 227
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "desiredState"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 231
    const-string/jumbo v1, "DrivingModeSettings"

    const-string/jumbo v2, "onCheckChanged : Driving mode changed broadcast"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.DRIVING_MODE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 234
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 235
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "driving_mode_on"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 237
    const-string/jumbo v1, "DrivingMode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 243
    :goto_0
    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->sendBroadcast(Landroid/content/Intent;)V

    .line 244
    return-void

    .line 239
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "driving_mode_on"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 241
    const-string/jumbo v1, "DrivingMode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 116
    invoke-super {p0, p1}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 118
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChatONVPhone()Z

    move-result v9

    if-nez v9, :cond_3

    move v0, v10

    .line 119
    .local v0, "chatONVHide1":Z
    :goto_0
    sget v9, Lcom/vlingo/midas/R$xml;->driving_mode_settings:I

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/DrivingModeSettings;->addPreferencesFromResource(I)V

    .line 120
    const-string/jumbo v9, "incoming_call_notification"

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/DrivingModeSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/CheckBoxPreference;

    iput-object v9, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mIncomingCallNotification:Landroid/preference/CheckBoxPreference;

    .line 121
    const-string/jumbo v9, "message_notification"

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/DrivingModeSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/CheckBoxPreference;

    iput-object v9, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mMessageNotification:Landroid/preference/CheckBoxPreference;

    .line 123
    const-string/jumbo v9, "voice_mail_notification"

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/DrivingModeSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/CheckBoxPreference;

    iput-object v9, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mVoiceMailNotification:Landroid/preference/CheckBoxPreference;

    .line 124
    const-string/jumbo v9, "alarm_notification"

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/DrivingModeSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/CheckBoxPreference;

    iput-object v9, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mAlarmNotification:Landroid/preference/CheckBoxPreference;

    .line 125
    const-string/jumbo v9, "schedule_notification"

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/DrivingModeSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/CheckBoxPreference;

    iput-object v9, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mScheduleNotification:Landroid/preference/CheckBoxPreference;

    .line 127
    const-string/jumbo v9, "chaton_call_notification"

    invoke-virtual {p0, v9}, Lcom/vlingo/midas/settings/DrivingModeSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/CheckBoxPreference;

    iput-object v9, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    .line 146
    if-eq v0, v10, :cond_0

    invoke-direct {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->isChatOnVInstalled()Z

    move-result v9

    if-nez v9, :cond_1

    .line 147
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v9

    iget-object v10, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 148
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "driving_mode_chaton_call_notification"

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 151
    :cond_1
    iget-object v9, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mVoiceMailNotification:Landroid/preference/CheckBoxPreference;

    if-eqz v9, :cond_2

    .line 154
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v9

    iget-object v10, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mVoiceMailNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 155
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "driving_mode_voice_mail_notification"

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 161
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string/jumbo v10, "com.sec.android.app.clockpackage"

    const/4 v12, 0x0

    invoke-virtual {v9, v10, v12}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :goto_1
    :try_start_1
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 174
    .local v4, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "privateFlags"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 175
    .local v5, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 176
    .local v6, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 178
    .local v2, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v1, 0x0

    .local v1, "currentPrivateFlags":I
    const/4 v8, 0x0

    .local v8, "valueofFlagsEnableStatusBar":I
    const/4 v7, 0x0

    .line 179
    .local v7, "valueofFlagsDisableTray":I
    invoke-virtual {v5, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    .line 180
    invoke-virtual {v6, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 181
    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v7

    .line 183
    or-int/2addr v1, v8

    .line 184
    or-int/2addr v1, v7

    .line 186
    invoke-virtual {v5, v4, v1}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 187
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 197
    .end local v1    # "currentPrivateFlags":I
    .end local v2    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v4    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v5    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v6    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v7    # "valueofFlagsDisableTray":I
    .end local v8    # "valueofFlagsEnableStatusBar":I
    :goto_2
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->onActivityCreated()V

    .line 198
    return-void

    .end local v0    # "chatONVHide1":Z
    :cond_3
    move v0, v11

    .line 118
    goto/16 :goto_0

    .line 163
    .restart local v0    # "chatONVHide1":Z
    :catch_0
    move-exception v3

    .line 164
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string/jumbo v9, "DrivingModeSettings"

    const-string/jumbo v10, "Samsung Alarm Clock is not found"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v9

    iget-object v10, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mAlarmNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 167
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "driving_mode_alarm_notification"

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    .line 191
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v3

    .line 194
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 188
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v9

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 202
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 206
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 204
    :pswitch_0
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->finish()V

    goto :goto_0

    .line 202
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mSettingsObserver:Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mSettingsObserver:Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;->stopObserving()V

    .line 332
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mSettingsObserver:Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;

    .line 335
    :cond_0
    invoke-super {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onPause()V

    .line 336
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 340
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 342
    .local v0, "cr":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mIncomingCallNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 343
    const-string/jumbo v4, "driving_mode_incoming_call_notification"

    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mIncomingCallNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 372
    :cond_0
    :goto_1
    invoke-static {v0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->areAllDrivingModeOptionsDisabled(Landroid/content/ContentResolver;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 373
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 378
    :goto_2
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v1

    return v1

    :cond_1
    move v1, v3

    .line 343
    goto :goto_0

    .line 345
    :cond_2
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mMessageNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 346
    const-string/jumbo v4, "driving_mode_message_notification"

    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mMessageNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_3

    .line 353
    :cond_4
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mVoiceMailNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 354
    const-string/jumbo v4, "driving_mode_voice_mail_notification"

    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mVoiceMailNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_4
    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_4

    .line 356
    :cond_6
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mAlarmNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 357
    const-string/jumbo v4, "driving_mode_alarm_notification"

    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mAlarmNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    :goto_5
    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_7
    move v1, v3

    goto :goto_5

    .line 359
    :cond_8
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mScheduleNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 360
    const-string/jumbo v4, "driving_mode_schedule_notification"

    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mScheduleNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_9

    move v1, v2

    :goto_6
    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_9
    move v1, v3

    goto :goto_6

    .line 367
    :cond_a
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 368
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "driving_mode_chaton_call_notification"

    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_b

    move v1, v2

    :goto_7
    invoke-static {v4, v5, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    :cond_b
    move v1, v3

    goto :goto_7

    .line 375
    :cond_c
    iget-object v1, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setEnabled(Z)V

    goto/16 :goto_2
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 248
    invoke-super {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onResume()V

    .line 250
    invoke-direct {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->updateState()V

    .line 252
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mIncomingCallNotification:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    .line 256
    iget-object v3, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mIncomingCallNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "driving_mode_incoming_call_notification"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mMessageNotification:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    .line 261
    iget-object v3, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mMessageNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "driving_mode_message_notification"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 268
    :cond_2
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mVoiceMailNotification:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    .line 269
    iget-object v3, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mVoiceMailNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "driving_mode_voice_mail_notification"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 273
    :cond_3
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mAlarmNotification:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    .line 274
    iget-object v3, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mAlarmNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "driving_mode_alarm_notification"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 278
    :cond_4
    iget-object v3, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mScheduleNotification:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "driving_mode_schedule_notification"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 284
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_5

    .line 285
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "driving_mode_chaton_call_notification"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_c

    :goto_5
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 289
    :cond_5
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mSettingsObserver:Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;

    if-nez v0, :cond_6

    .line 290
    new-instance v0, Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-virtual {p0}, Lcom/vlingo/midas/settings/DrivingModeSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;-><init>(Lcom/vlingo/midas/settings/DrivingModeSettings;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mSettingsObserver:Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;

    .line 291
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mSettingsObserver:Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/DrivingModeSettings$SettingsObserver;->startObserving()V

    .line 294
    :cond_6
    return-void

    :cond_7
    move v0, v2

    .line 256
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 261
    goto :goto_1

    :cond_9
    move v0, v2

    .line 269
    goto :goto_2

    :cond_a
    move v0, v2

    .line 274
    goto :goto_3

    :cond_b
    move v0, v2

    .line 278
    goto :goto_4

    :cond_c
    move v1, v2

    .line 285
    goto :goto_5
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 298
    invoke-super {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onStop()V

    .line 300
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/vlingo/midas/settings/DrivingModeSettings;->mActionBarLayout:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 302
    :cond_0
    return-void
.end method
