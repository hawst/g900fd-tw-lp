.class Lcom/vlingo/midas/settings/SettingsScreen$36;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->showWakeupInSecuredLockDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 2477
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$36;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    .line 2480
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 2481
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 2482
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$36;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mWakeup:Landroid/preference/CheckBoxPreference;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$1200(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 2489
    const/4 v0, 0x1

    .line 2491
    :cond_0
    return v0
.end method
