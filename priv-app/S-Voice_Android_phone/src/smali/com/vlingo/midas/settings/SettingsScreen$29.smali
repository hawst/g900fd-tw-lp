.class Lcom/vlingo/midas/settings/SettingsScreen$29;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->showGuideDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;)V
    .locals 0

    .prologue
    .line 2172
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$29;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    .line 2175
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 2176
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 2177
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$29;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->mVoiceInputControl:Landroid/preference/SwitchPreference;
    invoke-static {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->access$100(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/SwitchPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 2178
    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$29;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "voice_input_control"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2179
    const-string/jumbo v1, "temp_input_voice_control"

    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen$29;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v2}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "voice_input_control"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 2180
    const/4 v0, 0x1

    .line 2182
    :cond_0
    return v0
.end method
