.class Lcom/vlingo/midas/settings/SettingsScreen$28;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen;->showGuideDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vlingo/midas/settings/SettingsScreen;

.field final synthetic val$check:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 2153
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$28;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    iput-object p2, p0, Lcom/vlingo/midas/settings/SettingsScreen$28;->val$check:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x1

    .line 2156
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$28;->val$check:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2157
    iget-object v0, p0, Lcom/vlingo/midas/settings/SettingsScreen$28;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v0}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "voiceinputcontrol_showNeverAgain"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2159
    :cond_0
    const-string/jumbo v0, "temp_input_voice_control"

    iget-object v1, p0, Lcom/vlingo/midas/settings/SettingsScreen$28;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    invoke-virtual {v1}, Lcom/vlingo/midas/settings/SettingsScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "voice_input_control"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 2160
    return-void
.end method
