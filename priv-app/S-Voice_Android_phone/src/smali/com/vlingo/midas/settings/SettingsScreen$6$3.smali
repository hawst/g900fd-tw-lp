.class Lcom/vlingo/midas/settings/SettingsScreen$6$3;
.super Ljava/lang/Object;
.source "SettingsScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vlingo/midas/settings/SettingsScreen$6;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;


# direct methods
.method constructor <init>(Lcom/vlingo/midas/settings/SettingsScreen$6;)V
    .locals 0

    .prologue
    .line 878
    iput-object p1, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 881
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v2, v2, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    const-string/jumbo v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/settings/SettingsScreen;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 883
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v2, v2, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v2}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 887
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v2, v2, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v2}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v2, v2, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v2}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 888
    iget-object v2, p0, Lcom/vlingo/midas/settings/SettingsScreen$6$3;->this$1:Lcom/vlingo/midas/settings/SettingsScreen$6;

    iget-object v2, v2, Lcom/vlingo/midas/settings/SettingsScreen$6;->this$0:Lcom/vlingo/midas/settings/SettingsScreen;

    # getter for: Lcom/vlingo/midas/settings/SettingsScreen;->m_carModeHomeAddress:Landroid/preference/EditTextPreference;
    invoke-static {v2}, Lcom/vlingo/midas/settings/SettingsScreen;->access$600(Lcom/vlingo/midas/settings/SettingsScreen;)Landroid/preference/EditTextPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->cancel()V

    .line 892
    :cond_0
    const-wide/16 v2, 0x96

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 897
    :goto_0
    return-void

    .line 893
    :catch_0
    move-exception v0

    .line 895
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
