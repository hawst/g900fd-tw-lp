.class public Lcom/vlingo/midas/settings/VLPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "VLPreferenceActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 17
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->setVolumeControlStream(I)V

    .line 19
    return-void
.end method
